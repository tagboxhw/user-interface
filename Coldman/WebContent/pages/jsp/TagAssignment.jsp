<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ page import="com.coldman.dao.ClientLocationMapDao"%>
<%@ page import="com.coldman.dao.TransitVehicleMapDao"%>
<%@ page import="com.coldman.dao.TransitDriverMapDao"%>
<%@ page import="com.coldman.bean.TransitDriverMapBean"%>
<%@ page import="com.coldman.bean.TransitVehicleMapBean"%>
<%@ page import="com.coldman.bean.ClientLocationMapBean"%>
<%@ page import="com.coldman.util.Utils"%>
<%@ page import="java.util.TreeMap"%>
<%@ page import="java.util.Set"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.util.Collection"%>
<%@ page import="java.util.Set"%>
<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>
<%
response.setHeader("Cache-Control","no-cache"); 
response.setHeader("Pragma","no-cache"); 
response.setDateHeader ("Expires", -1); 
%>
<%!
static final Logger logger = LoggerFactory.getLogger("TagAssignment");
%>
<%
String sUsername = (String) session.getAttribute("username");
if(sUsername == null || sUsername.equals("")) {
	session.setAttribute("callingPage", "TagAssignment.jsp");
	response.sendRedirect("../../index.html");
}
String sVehicleIdParam = "", sVehicleTypeParam = "", sSourceParam = "", sDestParam = "", sCurrentLocationParam = "", sDriverIDParam = "", sVehicleStatusParam = "", sListView = "";
sVehicleIdParam = request.getParameter("VehicleID");
if(sVehicleIdParam == null || sVehicleIdParam.equals("")) sVehicleIdParam = "ALL";
sSourceParam = request.getParameter("Source");
if(sSourceParam == null || sSourceParam.equals("")) sSourceParam = "ALL";
sDestParam = request.getParameter("Destination");
if(sDestParam == null || sDestParam.equals("")) sDestParam = "ALL";
sCurrentLocationParam = request.getParameter("CurrentLocation");
if(sCurrentLocationParam == null || sCurrentLocationParam.equals("")) sCurrentLocationParam = "ALL";
sDriverIDParam = request.getParameter("DriverID");
if(sDriverIDParam == null || sDriverIDParam.equals("")) sDriverIDParam = "ALL";
sVehicleStatusParam = request.getParameter("VehicleStatus");
if(sVehicleStatusParam == null || sVehicleStatusParam.equals("")) sVehicleStatusParam = "ALL";
sListView = request.getParameter("showView1");

java.util.Date dt = new java.util.Date();
java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd-MM-yyyy HH:mm");
sdf.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
String currentTime = sdf.format(dt);
ClientLocationMapDao clmDao = new ClientLocationMapDao();
TransitVehicleMapDao tvmDao = new TransitVehicleMapDao();
TransitDriverMapDao tdmDao = new TransitDriverMapDao();
TransitVehicleMapBean tvmBean = new TransitVehicleMapBean();
ClientLocationMapBean clmBean = new ClientLocationMapBean();

Collection<TransitVehicleMapBean> col = tvmDao.selectVehicles();
TreeMap tmLocations = clmDao.selectLocations();

Iterator iter = col.iterator();
String sVehicleId = "", sVehicleType = "", sSource = "", sDest = "", sCurrentLocation = "", sDriverID = "", sVehicleStatus  = "";
int iRunning = 0, iAssigned = 0, iAvailable = 0, iUnavailable = 0;
int tmp = 0;
while(iter.hasNext()) {
	tvmBean = (TransitVehicleMapBean) iter.next();
	if(tmLocations.containsKey(tvmBean.getSource())) {
		clmBean = (ClientLocationMapBean) tmLocations.get(tvmBean.getSource());
		tvmBean.setSource(clmBean.getLocationName());
	}
	if(tmLocations.containsKey(tvmBean.getDestination())) {
		clmBean = (ClientLocationMapBean) tmLocations.get(tvmBean.getDestination());
		tvmBean.setDestination(clmBean.getLocationName());
	}
	
	if(tvmBean.getLatitude() == null || tvmBean.getLatitude().equals("")) tvmBean.setLatitude("12.993709");
	if(tvmBean.getLongitude() == null || tvmBean.getLongitude().equals("")) tvmBean.setLongitude("77.681804");
	if(!sVehicleId.contains(tvmBean.getVehicle_ID())){ sVehicleId += tvmBean.getVehicle_ID() + ";";}
	//if(!sVehicleType.contains(tvmBean.getVehicle_Type())){ sVehicleType += tvmBean.getVehicle_Type() + ";";}
	if(!sSource.contains(tvmBean.getSource())){ sSource += tvmBean.getSource() + ";";}
	if(!sDest.contains(tvmBean.getDestination())){ sDest += tvmBean.getDestination() + ";";}
	if(!sCurrentLocation.contains(tvmBean.getSource())){ sCurrentLocation += tvmBean.getSource() + ";";}//This should be changed to Current Location
	//if(!sDriverID.contains(tvmBean.getDriver_DL_ID())){ sDriverID += tvmBean.getDriver_DL_ID() + ";";}
	if(!sVehicleStatus.contains(tvmBean.getVehicle_Status())){ sVehicleStatus += tvmBean.getVehicle_Status() + ";";}
	if(tvmBean.getVehicle_Status().equalsIgnoreCase("Assigned")) iAssigned++;
	else if(tvmBean.getVehicle_Status().equalsIgnoreCase("Running")) iRunning++;
	else if(tvmBean.getVehicle_Status().equalsIgnoreCase("Available")) iAvailable++;
	else if(tvmBean.getVehicle_Status().equalsIgnoreCase("Unavailable")) iUnavailable++;
	tmp++;
}
String[] sVehicleIdArray = sVehicleId.split(";");

String[] sSourceArray = sSource.split(";");
String[] sDestArray = sDest.split(";");
String[] sCurrentLocationArray = sCurrentLocation.split(";");
String[] sDriverIDArray = sDriverID.split(";");
String[] sVehicleStatusArray = sVehicleStatus.split(";");

TreeMap tmDrivers = tdmDao.selectDrivers();
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Tag Assignment</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Add these below two lines for autocomplete and the javascript function at the end  -->
<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="../../bootstrap/css/bootstrap.css">
   
<!-- Font Awesome -->
<script src="https://use.fontawesome.com/37ba1872d2.js"></script>
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../../dist/css/tagbox.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
<link href="https://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet">
<link href="../../dist/css/select2.css" rel="stylesheet" />
<script src="../../dist/js/select2.js"></script>
<script src="../../dist/js/vehicleManagement.js"></script>
<script src="../../dist/js/inventory_mgmt.js"></script>
<link rel="stylesheet" type="text/css" href="https://js.cit.api.here.com/v3/3.0/mapsjs-ui.css" />
<script src="http://js.api.here.com/v3/3.0/mapsjs-core.js" type="text/javascript" charset="utf-8"></script>
<script src="http://js.api.here.com/v3/3.0/mapsjs-service.js" type="text/javascript" charset="utf-8"></script>

<script type="text/javascript" src="https://js.cit.api.here.com/v3/3.0/mapsjs-ui.js"></script>
<script type="text/javascript" src="https://js.cit.api.here.com/v3/3.0/mapsjs-mapevents.js"></script>

<script  type="text/javascript" charset="UTF-8" >
   
/**
 * Boilerplate map initialization code starts below:
 */

//Step 1: initialize communication with the platform
var platform = new H.service.Platform({
	  app_id: 'DdeJ8sqdrenYc1k7eTH6',
	  app_code: 'DBU7IBF4dzhGgtLRCx6Sgg',
  useCIT: true,
  useHTTPS: true
});
var defaultLayers = platform.createDefaultLayers();
var platform1 = new H.service.Platform({
	  app_id: 'DdeJ8sqdrenYc1k7eTH6',
	  app_code: 'DBU7IBF4dzhGgtLRCx6Sgg',
useCIT: true,
useHTTPS: true
});
var defaultLayers1 = platform1.createDefaultLayers();

  </script>
<script>
var CurrentLoc = "";
	$(document).ready(function() {
		$("#VehicleID").select2();
		$("#Source").select2();
		$("#Destination").select2();
		$("#CurrentLocation").select2();
		//$("#DriverID").select2();
		$("#VehicleStatus").select2();
		$('#paramFilter').select2();
	});
	function blinker() {
		$('.blinking').fadeOut(500);
		$('.blinking').fadeIn(500);

	}
	setInterval(blinker, 1000);
</script>

<link rel="stylesheet" href="../../dist/css/bootstrap-switch.css">
<script src="../../dist/js/bootstrap-switch.js"></script>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
  body {
	font-family:  Lato, sans-serif;
}
</style>
<style>
.save
{
	display:none;
}
.no-border {
	border: none !important;
	border: 0 !important;
}
</style>
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
	<div class="wrapper">

		<header class="main-header">
			<!-- Logo -->
			<div>
				<a href="#" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
					<span class="logo-mini"><img src="../../dist/img/cipla_logo.jpeg" style="margin-top:14px"
						alt="User Image"></span> <!-- logo for regular state and mobile devices -->
					<span class="logo-lg"><img src="../../dist/img/cipla_logo.jpeg" 
						alt="User Image"></span>
				</a>
			</div>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">

				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
					role="button"> <span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
				</a>
				<label class="navbar-form pull-left" role="search">
					<div class="form-group">
						<b style="padding-left: 15px; font-size: 20px;color:#fff;" class="pull-right">Tag Assignment</b><br> <b style="padding-left: 15px;color:#fff;"><%= currentTime %></b>
					</div>
				</label>

				<div class="navbar-custom-menu">
					<img src="../../dist/img/Logo.png" class="logo-class pull-right"
						alt="User Image">
				</div>
			</nav>

		</header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->


				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<br>
				<ul class="sidebar-menu">
					<li class="header">MAIN NAVIGATION</li>
					<li class="treeview"><a href="#"> <i
							class="fa fa-dashboard"></i> <span>Dashboard</span>
					</a>
						<ul class="treeview-menu">
							<li class=""><a href="CMCDashboard.jsp"><i
									class="fa fa-circle-o"></i> CMC</a></li>
							<!-- <li class=""><a href="NewDashboard.jsp"><i
									class="fa fa-circle-o"></i> Vehicle Dashboard</a></li> -->
							<li class=""><a href="TodayColdChainHealth.jsp"><i
									class="fa fa-circle-o"></i> Cold Rooms Summary</a></li>
							<!--  <li><a href="VehiclePlannedRouteMap.jsp"><i class="fa fa-circle-o"></i> Vehicle Planned Route</a></li> -->
							<li class=""><a href="TodayColdChainHealthVehicle.jsp"><i
									class="fa fa-circle-o"></i> Vehicles Summary</a></li>
						</ul></li>
					<li class="treeview"><a href="#"> <i
							class="fa fa-files-o"></i> <span>Alerts</span> 
					</a>
						<ul class="treeview-menu">
							<li><a href="LiveVehicleTracking.jsp"><i
									class="fa fa-circle-o"></i> Live Vehicle Tracking</a></li>
						</ul></li>
					
					<li class="treeview active"><a href="#"> <i
							class="fa fa-database"></i> <span>Admin</span>
					</a>
						<ul class="treeview-menu">
							<li class="active"><a href="VehicleManagement.jsp"><i class="fa fa-circle-o"></i>
									Vehicle Management</a></li>
							<li><a href="VendorScorecard.jsp"><i class="fa fa-circle-o"></i>
									Vehicle Score Card</a></li>
						</ul></li>
						<li class="treeview"><a href="#"> <i
							class="fa fa-bar-chart"></i> <span>Reports</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="ParameterSummary.jsp"><i class="fa fa-circle-o"></i>
									Parameter Summary</a></li>
							<li><a href="AlertsSummary.jsp"><i class="fa fa-circle-o"></i>
									Alerts Summary</a></li>
							<li><a href="AlertsReport.jsp"><i class="fa fa-circle-o"></i>
									Alerts Report</a></li>		
							
						</ul></li>
						<li class="treeview"><a href="#"> <i
							class="fa fa-history"></i> <span>Historical Data</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="HistoricalData.jsp"><i class="fa fa-circle-o"></i>
									Download <br>Temperature Data</a></li>
						</ul></li>
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			
		<section id="mainSection" class="content">
			<form id="mainForm" method="post" action="VehicleManagement.jsp">
			
							<div class="row">
							
				<div class="col-md-offset-2 col-md-2 text-center">
				<label><b># Underway&nbsp;&nbsp;</b></label><br><button class="btn btn-warning"><%=iRunning %></button>
				</div>
				<div class="col-md-2 text-center">
				<label><b># Upcoming&nbsp;&nbsp;</b></label><br><button class="btn btn-warning"><%=iAssigned %></button>
				</div>
				<div class="col-md-2 text-center">
				<label><b># Unassigned&nbsp;&nbsp;</b></label><br><button class="btn btn-warning"><%=iAvailable %></button>
				</div>
							
							</div>
							<br><br>
		
		<div id="listView">		
									<table id="data_table_stationary" class="table table-bordered table-striped table-condensed">
									<thead class="success">
									<th width="150">Tag ID</th>
									<th width="100">Date of Ship</th>
									<th width="150">Inv Id</th>
									<th width="100">Source</th>
									<th width="120">Destination</th>
									<th width="100">Last Known Location</th>
									<th width="130">Flight Id</th>
									<th width="130">Trip Status</th>
									<th width="130">Load Criticality</th>
									<th width="60">Change Assignment</th>
									</thead>
									<%
									int iCount = 0;
									 int j=0; 
									TransitDriverMapBean tdmBean = new TransitDriverMapBean();
									iter = col.iterator();
									while(iter.hasNext()) {
										tvmBean = (TransitVehicleMapBean) iter.next();
										if((sVehicleIdParam.equals("ALL") || sVehicleIdParam.equals(tvmBean.getVehicle_ID())) &&
												(sSourceParam.equals("ALL") || sSourceParam.equals(tvmBean.getSource())) &&
												(sDestParam.equals("ALL") || sDestParam.equals(tvmBean.getDestination())) &&
												//(sCurrentLocationParam.equals("ALL") || sCurrentLocationParam.equals(tvmBean.getCurrentLocation())) &&
												//(sDriverIDParam.equals("ALL") || sDriverIDParam.equals(tvmBean.getDriver_DL_ID())) &&
												(sVehicleStatusParam.equals("ALL") || sVehicleStatusParam.equals(tvmBean.getVehicle_Status()))) {
										if(tvmBean.getTrip_Status().equals("Completed")) {
									%>
										<tr id="row_stationary<%= j%>" class="blinking">
									<%} else { %>
										<tr id="row_stationary<%= iCount%>" class="danger">
									<%} %>
									<td id="<%=iCount %>_Vehicle_Id">Tag-<%=iCount %></td>
									<td id="<%=iCount %>_Assigned_Date"><%=tvmBean.getAssigned_Date() %></td>
									<td>
									2287645</td>
									<td id="<%=iCount %>_Source"><%=tvmBean.getSource() %></td>
									<td id="<%=iCount %>_Destination"><%=tvmBean.getDestination() %></td>
									<td id="<%=iCount %>_Current_Location"><div id="<%=iCount %>_CR" name="<%=iCount %>_CR"></div></td>
									<script type="text/javascript">
									
									$.get(
											"https://reverse.geocoder.cit.api.here.com/6.2/reversegeocode.json",
											{
												app_id: 'DdeJ8sqdrenYc1k7eTH6',
												app_code: 'DBU7IBF4dzhGgtLRCx6Sgg',
												gen:'9',
												prox:'<%=tvmBean.getLatitude()%>,<%=tvmBean.getLongitude()%>,100',
												mode:'retrieveAddresses'
											},
											function(data) {
												//console.log(data);
												$.each(data.Response, function(i, item){
													var a = (data.Response.View[0].Result[0].Location.Address.City);
													$('#<%=iCount%>_CR').html(a);
													if(CurrentLoc.indexOf(a) < 0) {
														CurrentLoc = CurrentLoc + (data.Response.View[0].Result[0].Location.Address.City) + ";"
													}
													
												});
											}
										);
									</script>
									
									
									
									<td id="<%=iCount %>_Vehicle_Status">
									IC080</td>
									<%if(iCount % 3 == 0 && tvmBean.getVehicle_Status().equals("Running")){ %>
									<td id="<%=iCount %>_Trip_Status"><div class="progress">
  										<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
    										60%
 										 </div>
										</div></td>
									<%} else if(iCount % 2 == 0 && tvmBean.getVehicle_Status().equals("Running")) { %>
									<td id="<%=iCount %>_Trip_Status"><div class="progress">
  										<div class="progress-bar" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%;">
    										40%
 										 </div>
										</div></td>
									<%} else if(!tvmBean.getVehicle_Status().equals("Running")){ %>
									<td id="<%=iCount %>_Trip_Status"><div class="progress">
  										<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
    										0%
 										 </div>
										</div></td>
									<%} else if(tvmBean.getVehicle_Status().equals("Running")){ %>
									<td id="<%=iCount %>_Trip_Status"><div class="progress">
										<div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%;">
										80%
										 </div>
									</div></td>
									<%} %> 
									<%if(iCount % 3 == 0){ %>
									<td class="text-center"><span class="badge bg-red">HIGH</span></td>
									<%} else if(iCount % 2 == 0){%>
									<td class="text-center"><span class="badge bg-orange">MEDIUM</span></td>
									<%} else {%>
									<td class="text-center"><span class="badge bg-green">LOW</span></td>
									<%} %>
									
									<td class="text-center">
									<table width="100%" class="no-border"><tr class="no-border"><td class="no-border"><i id="edit_button<%=iCount %>" class="fa fa-pencil edit fa-color-blue" onclick="edit_row('<%= iCount%>')"></i></td>
									<td class="no-border"><i id="save_button<%=iCount %>" class="fa fa-floppy-o save fa-color-blue" onclick="save_row('<%=iCount%>')"></i></td>
									</tr>
									</table>
									</td>
									</tr>
									<%}
									iCount++;	
									} %>
									</table>	
									
								<br>
						<input type="hidden" id="lastValue_stationary" name="lastValue_stationary" value="<%= iCount%>">
		</div>
		</form>
		</section>
	
    
    
    
				</div>
		<!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0
			</div>
			<strong>Copyright &copy; 2017 <a href="http://tagbox.in">TagBox</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Control Sidebar -->
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->
	
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>

	<!-- Bootstrap 3.3.6 -->
	<script src="../../bootstrap/js/bootstrap.min.js"></script>
	<!-- ChartJS 1.0.1 -->
	<!-- AdminLTE App -->
	<script src="../../dist/js/app.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="../../dist/js/demo.js"></script>


	</body>
	<script type="text/javascript">
	
	$.get(
			"https://reverse.geocoder.cit.api.here.com/6.2/reversegeocode.json",
			{
				app_id: 'DdeJ8sqdrenYc1k7eTH6',
				app_code: 'DBU7IBF4dzhGgtLRCx6Sgg',
				gen:'9',
				prox:'12.993709,77.681804,100',
				mode:'retrieveAddresses'
			},
			function(data) {
				console.log(data);
				$.each(data.Response, function(i, item){
					//console.log(data.Response.View[0].Result[0].Location.Address.City);
					
					//getVideos(playlistId, localStorage.getItem('maxResults'));
				});
			}
		);
	
	
</script>
</html>
