<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
 
<title>Vehicle Planned Route Map Alerts</title>
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>


<!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="../../dist/css/tagbox.css">
  <!-- jQuery 2.2.3 -->


<!-- AdminLTE App -->
<script src="../../dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>

  <script src="../../dist/js/tagbox.js"></script>
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
   <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD53TZnxuAsBNWiVjlw8VbXewqG58QXVnE" type="text/javascript"></script>
<script src="../../dist/js/mapPlanData.js"></script>
<link rel="stylesheet" href="../../dist/css/map.css">

</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">

		<header class="main-header">
			<!-- Logo -->
			<div>
				<a href="#" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
					<span class="logo-mini"><img src="../../dist/img/cipla_logo.jpeg" style="margin-top:14px"
						alt="User Image"></span> <!-- logo for regular state and mobile devices -->
					<span class="logo-lg"><img src="../../dist/img/cipla_logo.jpeg" 
						alt="User Image"></span>
				</a>
			</div>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">

				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
					role="button"> <span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
				</a>
				<form class="navbar-form pull-left" role="search">
					<div class="form-group">
						<b style="padding-left: 15px; font-size: 20px;color:#fff;" class="pull-right">Adarsh Kumar's Central Console</b><br> <b style="padding-left: 15px;color:#fff;">&nbsp;</b>
					</div>
				</form>

				<div class="navbar-custom-menu">
					<img src="../../dist/img/Logo.png" class="logo-class pull-right"
						alt="User Image">
				</div>
			</nav>

		</header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->


				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<ul class="sidebar-menu">
					<li class="header">MAIN NAVIGATION</li>
					<li class="treeview"><a href="#"> <i
							class="fa fa-dashboard"></i> <span>Alert</span> <span
							class="pull-right-container"> <span
								class="label bg-red pull-right">4</span>
						</span>
					</a>
						<ul class="treeview-menu">
							<li><a> Cold Rooms</a></li>
							<li><a href="ColdRoomPendingAlertWorkflows.jsp"><i
									class="fa fa-circle-o"></i> Today's Alerts</a></li>
							<li><a href="#"><i class="fa fa-circle-o"></i> Pending
									Alert Workflows</a></li>
							<li><a href="#"><i class="fa fa-circle-o"></i> Alerts
									With No Action</a></li>
							<li><a>Vehicles</a></li>
							<li><a href="VehicleTodayAlerts.jsp"><i class="fa fa-circle-o"></i> Today's Alerts</a></li>
							<li><a href="VehicleStoppageAlerts.jsp"><i class="fa fa-circle-o"></i> Vehicle Stoppage</a></li>
							<li><a href="VehicleRouteAlerts.jsp"><i class="fa fa-circle-o"></i> Vehicle Route</a></li>
							<li><a href="VehicleDoorAlerts.jsp"><i class="fa fa-circle-o"></i> Vehicle Door Activity</a></li>
							<li><a href="VehicleTemperatureAlerts.jsp"><i class="fa fa-circle-o"></i> Vehicle Temperature</a></li>
						</ul></li>
					<li class="treeview active"><a href="#"> <i class="fa fa-files-o"></i>
							<span>Dashboard</span> <span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
						<ul class="treeview-menu">
						<li><a href="Dashboard.html"><i class="fa fa-circle-o"></i> My Dashboard</a></li>
						<!-- <li class="active"><a href="VehiclePlannedRouteMap.jsp"><i class="fa fa-circle-o"></i> Vehicle Planned Route</a></li> -->
							<li><a href="VehicleStopRouteMap.jsp"><i class="fa fa-circle-o"></i> Vehicle Stoppage And Route</a></li>
            <li><a href="VehicleDoorMapAlerts.jsp"><i class="fa fa-circle-o"></i> Vehicle Door Activity</a></li>
            <li><a href="VehicleTempMapAlerts.jsp"><i class="fa fa-circle-o"></i> Vehicle Temperature</a></li>
						</ul></li>
					<li><a href="#"> <i class="fa fa-th"></i> <span>Analytics</span>
							<span class="pull-right-container"> <small
								class="label pull-right bg-green">new</small>
						</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="DemandForecasting.jsp"><i class="fa fa-circle-o"></i> Demand Forecasting</li>
						</ul></li>
					<li class="treeview"><a href="#"> <i
							class="fa fa-pie-chart"></i> <span>Admin Console</span> <span
							class="pull-right-container"> <i
								class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="#"><i class="fa fa-circle-o"></i>
									Device Environment</a></li>
							<li><a href="#"><i class="fa fa-circle-o"></i> User
									Management</a></li>
							<li><a href="#"><i class="fa fa-circle-o"></i> Central
									Console</a></li>
						</ul></li>
					<li class="treeview"><a href="#"> <i class="fa fa-laptop"></i>
							<span>Support</span> <span class="pull-right-container"> <i
								class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="#"><i class="fa fa-circle-o"></i> Raise
									Service Ticket</a></li>
							<li><a href="#"><i class="fa fa-circle-o"></i> Email
									Your Questions</a></li>
						</ul></li>

				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Dashboard <small>Vehicle Stoppage and Route</small>
				</h1>
				
				<ol class="breadcrumb">
					<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
					<li><a href="#">Dashboard</a></li>
					<li class="active">Vehicle Stoppage and Route</li>
				</ol>
			</section>
			<br>


			<!-- Main content -->
			
			
			<section id="content_door">
				<div class="row">
					<div class="col-md-12" style="padding-left:25px">
						 <div id="map" style="border: 2px solid #3872ac;"></div>
						<div id="directions_panel"></div>
					</div>
				</div>
				<!-- /.row -->

			</section>
			
			
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0
			</div>
			<strong>Copyright &copy; 2017 <a href="http://tagbox.in">TagBox</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Control Sidebar -->
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<script type="text/javascript">google.maps.event.addDomListener(window, 'load', initialize);</script>
</body>
</html>
