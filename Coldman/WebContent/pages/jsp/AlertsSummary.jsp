<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>
<%@ page import="com.coldman.util.Constants"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Set"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.util.Vector"%>
<%@ page import="java.util.Collection"%>
<%@ page import="com.coldman.util.Utils"%>
<%@ page import="com.coldman.dao.ClientLocationMapDao"%>
<%@ page import="com.coldman.dao.AlertsWorkflowDao"%>
<%@ page import="java.util.TreeMap"%>
<%@ page import="com.coldman.util.Constants"%>
<%@ page import="com.coldman.bean.AllLocationDataBean"%>
<%@ page import="java.util.Set"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="com.coldman.bean.AlertSummaryBean"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@ page import="java.util.concurrent.TimeUnit"%>

<%
response.setHeader("Cache-Control","no-cache"); 
response.setHeader("Pragma","no-cache"); 
response.setDateHeader ("Expires", -1); 
%>
<%!
static final Logger logger = LoggerFactory.getLogger("AlertsSummary");
%>
<%
String sUsername = (String) session.getAttribute("username");
if(sUsername == null || sUsername.equals("")) {
	session.setAttribute("callingPage", "AlertsSummary.jsp");
	response.sendRedirect("../../index.html");
}
sUsername = (String) session.getAttribute("username");
String sFromDate = (String) request.getParameter("from_date");
String sToDate = (String) request.getParameter("to_date");

Date dt = new Date();
SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy");
SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy/M/d");
Date d = null;
sdf.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
String currentTime = sdf.format(dt);
String sParameter = request.getParameter("Parameter");
if(sParameter == null || sParameter.equals("")) sParameter = Constants.TEMPERATURE;
String sLocation = request.getParameter("Location");
String sZoneId = request.getParameter("Zone");
if(sZoneId == null || sZoneId.equals("")) sZoneId = "All";
TreeMap<String, String> tmLocation = new TreeMap<String, String>();
ClientLocationMapDao clmDao = new ClientLocationMapDao();
String sValue = "";
if(clmDao.colAllLocations == null || clmDao.colAllLocations.size() == 0) clmDao.getAllLocationsData();
String sTempNodeId = "";
sValue = "";
for(AllLocationDataBean alBean: clmDao.colAllLocations){
	if(sLocation == null || sLocation.equals("")) sLocation = alBean.getLocation_Name();
	if(tmLocation.containsKey(alBean.getLocation_Name())) {
		sValue = tmLocation.get(alBean.getLocation_Name());
		if(sValue.indexOf(alBean.getZone_Name()) >= 0) {
			
		} else {
			sValue = sValue + ";" + alBean.getZone_Name();
			tmLocation.put(alBean.getLocation_Name(), sValue);
		}
	} else {
		tmLocation.put(alBean.getLocation_Name(), alBean.getZone_Name());
	}
}
AlertsWorkflowDao ab = new AlertsWorkflowDao();
Date d2 = null;
Date dFromDateUI = null;
Date dToDateUI = null;
Date dFromDate = null;
Date dToDate = null;
String sFromDate1 = "", sToDate1 = "", sFromDate2 = "", sToDate2 = "";;
if(sFromDate != null && !sFromDate.equals("")) {
	d = sdf2.parse(sFromDate);
	sFromDate1 = df.format(d);
	sFromDate2 = sdf3.format(d);
	dFromDate = sdf3.parse(sFromDate2);
	dFromDateUI = sdf2.parse(sFromDate);
} else {
	d = new Date(dt.getTime() - (7 * Constants.DAY_IN_MS));
	sFromDate1 = df.format(d);
	sFromDate = sdf2.format(d);
	sFromDate2 = sdf3.format(d);
	dFromDate = sdf3.parse(sFromDate2);
	dFromDateUI = sdf2.parse(sFromDate);
}
if(sToDate != null && !sToDate.equals("")) {
	d = sdf2.parse(sToDate);
	sToDate1 = df.format(d);
	sToDate2 = sdf3.format(d);
	dToDate = sdf3.parse(sToDate2);
	dToDateUI = sdf2.parse(sToDate);
} else {
	sToDate1 = df.format(dt);
	sToDate = sdf2.format(dt);
	sToDate2 = sdf3.format(dt);
	dToDate = sdf3.parse(sToDate2);
	dToDateUI = sdf2.parse(sToDate);
}
Long newTime = null;
Collection<AlertSummaryBean> col = ab.getAlertsSummary(sFromDate1, sToDate1);
int[][] alertSum = new int[5][5];
for(AlertSummaryBean as: col) {
	if(sZoneId.equalsIgnoreCase("All") || sZoneId.equals(as.getsZoneName())) {
		alertSum[as.getsAlertSeverity()-1][as.getResolution()-1] += as.getsCount();
	}
}
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Alerts Summary</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<!-- Add these below two lines for autocomplete and the javascript function at the end  -->
<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="../../bootstrap/css/bootstrap.css">
   
<!-- Font Awesome -->
<script src="https://use.fontawesome.com/37ba1872d2.js"></script>
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../../dist/css/tagbox.css">
<link href="../../dist/css/select2.css" rel="stylesheet" />
<script src="../../dist/js/select2.js"></script>
<script src="../../dist/js/tagbox.js"></script>
<script src="../../dist/js/table2download.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#Parameter").select2();
	$("#Location").select2();
	$("#Zone").select2();
});
</script>
<STYLE TYPE="text/css">
			table.sortable span.sortarrow {
   				color: blue;
   				text-decoration: none;
			}
					
		</style>
<script src="../../dist/js/sorttable.js"></script>
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
<link href="https://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet">

<link rel="stylesheet" href="../../dist/css/bootstrap-switch.css">
<script src="../../dist/js/bootstrap-switch1.js"></script>
<script src="../../dist/js/location.js"></script>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
  body {
	font-family:  Lato, sans-serif;
	font-weight: 600;
}
</style>
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
	<div class="wrapper">

		<header class="main-header">
			<!-- Logo -->
			<div>
				<a href="#" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
					<span class="logo-mini"><img src="../../dist/img/cipla_logo.jpeg" style="margin-top:14px"
						alt="User Image"></span> <!-- logo for regular state and mobile devices -->
					<span class="logo-lg"><img src="../../dist/img/cipla_logo.jpeg" 
						alt="User Image"></span>
				</a>
			</div>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">

				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
					role="button"> <span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
				</a>
				<form class="navbar-form pull-left" role="search">
					<div class="form-group">
						<b style="padding-left: 15px; font-size: 20px;color:#fff;" class="pull-right">Alerts Summary</b><br> <b style="padding-left: 15px;color:#fff;"><%= currentTime %></b>
					</div>
				</form>

				<div class="navbar-custom-menu">
					<img src="../../dist/img/Logo.png" class="logo-class pull-right"
						alt="User Image">
				</div>
			</nav>

		</header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->


				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<br>
				<ul class="sidebar-menu">
					<li class="header">MAIN NAVIGATION</li>
					<li class="treeview"><a href="#"> <i
							class="fa fa-dashboard"></i> <span>Dashboard</span>
					</a>
						<ul class="treeview-menu">
							<li class="">
							
							<%if(sUsername != null && sUsername.equals("demo2")) { %>
							<a href="CMCDashboard.jsp"><i
									class="fa fa-circle-o"></i> CMC</a>
							<%} else { %>
							<a href="CMCDashboard.jsp" role="button" class="btn btn-disabled disabled"><i
									class="fa fa-circle-o"></i> CMC</a>
							<%} %></li>
							<!-- <li class=""><a href="NewDashboard.jsp"><i
									class="fa fa-circle-o"></i> Vehicle Dashboard</a></li> -->
							<li class=""><a href="TodayColdChainHealth.jsp"><i
									class="fa fa-circle-o"></i> Cold Rooms Summary</a></li>
							<!--  <li><a href="VehiclePlannedRouteMap.jsp"><i class="fa fa-circle-o"></i> Vehicle Planned Route</a></li> -->
							<li class=""><a href="TodayColdChainHealthVehicle.jsp" role="button" class="btn btn-disabled disabled"><i
									class="fa fa-circle-o"></i> Vehicles Summary</a></li>
						</ul></li>
					<li class="treeview"><a href="#"> <i
							class="fa fa-files-o"></i> <span>Alerts</span> 
					</a>
						<ul class="treeview-menu">
							<li><a href="LiveVehicleTracking.jsp" role="button" class="btn btn-disabled disabled"><i
									class="fa fa-circle-o"></i> Live Vehicle Tracking</a></li>
						</ul></li>
					
					<li class="treeview"><a href="#"> <i
							class="fa fa-database"></i> <span>Admin</span>
					</a>
						<ul class="treeview-menu">
							<li class=""><a href="VehicleManagement.jsp" role="button" class="btn btn-disabled disabled"><i class="fa fa-circle-o"></i>
									Vehicle Management</a></li>
							<li><a href="VendorScorecard.jsp" role="button" class="btn btn-disabled disabled"><i class="fa fa-circle-o"></i>
									Vehicle Score Card</a></li>
						</ul></li>
						<li class="treeview active"><a href="#"> <i
							class="fa fa-bar-chart"></i> <span>Reports</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="ParameterSummary.jsp"><i class="fa fa-circle-o"></i>
									Parameter Summary</a></li>
							<li class="active"><a href="AlertsSummary.jsp"><i class="fa fa-circle-o"></i>
									Alerts Summary</a></li>
							<li><a href="AlertsReport.jsp"><i class="fa fa-circle-o"></i>
									Alerts Report</a></li>		
							
						</ul></li>
						<li class="treeview"><a href="#"> <i
							class="fa fa-history"></i> <span>Historical Data</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="HistoricalData.jsp"><i class="fa fa-circle-o"></i>
									Download <br>Temperature Data</a></li>
						</ul></li>
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			
		<section id="mainSection" class="content">
		<div class="well well-sm white-background" style="height:550px">
							
						<br>
						<form id="myForm2" name="myForm2" method="post"
											action="AlertsSummary.jsp">
											<div class="row">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-offset-1 col-md-2 text-center">
						<label><b>Parameter</b></label>&nbsp;&nbsp;<br><br>
					<select id="Parameter" name="Parameter" class="form-control-no-background pull-center">
					<%if(sParameter.equals(Constants.HUMIDITY)) { %>
					<option value="Temperature">Temperature</option>
					<option value="Humidity" selected>Humidity</option>
					<%} else { %>
					<option value="Temperature" selected>Temperature</option>
					<option value="Humidity">Humidity</option>
					<%} %>
					</select>
					</div>
					<div class="col-md-2 text-center">
					<label><b>Location</b></label><br><br>
					<select id="Location" name="Location" class="form-control-no-background pull-center" onchange="reloadZone();">
					<%Set set = (Set) tmLocation.keySet();
						Iterator iter = set.iterator();
						while(iter.hasNext()){
							sValue = (String) iter.next();
							if(sLocation.equals(sValue)) {
						%>
							<option value="<%= sValue %>" selected><%= sValue %></option>
						<% } else {%>
							<option value="<%= sValue %>"><%= sValue %></option>
						<%}
						}
					%>
					</select>
					</div>
					<div class="col-md-2 text-center">
					<label><b>Zone</b></label><br><br>
					<select id="Zone" name="Zone" class="form-control-no-background pull-center">
					<option value="All" selected>All</option>
					<%
						sValue = tmLocation.get(sLocation);
						String sZone[] = sValue.split(";");
						for(int i =0; i<sZone.length; i++) {
							if(sZoneId.equals(sZone[i])) {	
						%>
							<option value="<%= sZone[i] %>" selected><%= sZone[i] %></option>
						<%} else {%>
						<option value="<%= sZone[i] %>"><%= sZone[i] %></option>
						<%}
						}
					%>
					</select>
					</div>
					<div class="col-md-2 text-center" id="time-range">
    <label><b>Time Range</b></label><br><label class="slider-time"></label> - <label class="slider-time2"></label>
    
    <div class="sliders_step1">
        <div id="slider-range"></div>
    </div>
    <input type="hidden" name="from_date" id="from_date" value="">
    <input type="hidden" name="to_date" id="to_date" value="">
</div>
					<div class="col-md-3"><br><br>
								<button type="submit" class="btn btn-success">GO</button>&nbsp;&nbsp;
								</div>
					
					</div>
					<br><br>
					<%if(col.size() == 0){ %>
					<div class="text-center">
						No Data!
						</div>
					<%} else { %>
					<div class="row">
					<div class="col-md-offset-2 col-md-8 text-right">
					<span id="csvLink1"></span>&nbsp;&nbsp;&nbsp;&nbsp;<span id="xlsLink1"></span><br><br>
						<table id="data_table_stationary1" class="table table-bordered table-condensed" style="width:80%">
									<tr class="info-background text-center"><td colspan="7">Severity&nbsp;<button type="button" class="btn btn-sm" data-toggle="modal" data-target="#myModal1" style="background:#d9edf7"><i class="fa fa-info-circle fa-lg fa-color-blue"></i></button></td></tr>
									<tr class="info text-center">
									<th width="10">&nbsp;</th>
									<th width="10">&nbsp;</th>
									<th width="10" class="text-center">1</th>
									<th width="10" class="text-center">2</th>
									<th width="10" class="text-center">3</th>
									<th width="10" class="text-center">4</th>
									<th width="10" class="text-center">5</th>
									</tr>
									<tr><td width="10" rowspan="15" class="info text-center"><br>Resolution&nbsp;<button type="button" class="btn btn-sm" data-toggle="modal" data-target="#myModal2" style="background:#d9edf7"><i class="fa fa-info-circle fa-lg fa-color-blue"></i></button></td></tr>
									<%
									String sTempDate = "";
									for(int i =0; i<5; i++) {
										int j = 0;
									%>
									<tr><td class="info" class="text-center"><%=(i+1) %></td>	<td class="text-center"><%=alertSum[j][i] %></td>	<td class="text-center"><%=alertSum[j+1][i] %></td>	<td class="text-center"><%= alertSum[j+2][i] %></td>	<td class="text-center"><%= alertSum[j+3][i] %></td><td class="text-center"><%= alertSum[j+4][i] %></td></tr>
									<%//}
										}%>
									</table>
						
									</div>
									</div>
									<%} %>
				</div>
						</div>	
										</form>
						
						
						
						
						
						
						
								</div>
						
		</section>
		</div>
							</div>
		<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center btn-info">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Definitions</h4>
      </div>
      <div class="modal-body">
       <h3>Severity </h3>Scale metric to quantify breach criticality relative to the upper/lower limit set for a monitoring point. Scale is defined based on limit to current parameter ratio.<br>
      </div>
      <div class="modal-footer text-center" style="text-align:center">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center btn-info">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Definitions</h4>
      </div>
      <div class="modal-body">
       <h3>Resolution </h3> Scale metric for the time taken to resolve an alert by assigned personnel. Default interval at each scale level is 15 minutes.<br>
      </div>
      <div class="modal-footer text-center" style="text-align:center">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
		<!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0
			</div>
			<strong>Copyright &copy; 2017 <a href="http://tagbox.in">TagBox</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Control Sidebar -->
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->
<!-- ChartJS 1.0.1 -->
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>


	<!-- Bootstrap 3.3.6 -->
	<script src="../../bootstrap/js/bootstrap.min.js"></script>
	<!-- ChartJS 1.0.1 -->
	<!-- AdminLTE App -->
	<script src="../../dist/js/app.min.js"></script>
	<script src="../../plugins/chartjs/Chart.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="../../dist/js/demo.js"></script>
	
	<script type="text/javascript">
	jQuery( document ).ready(function() {
	    
	    jQuery( "#data_table_stationary1" ).table_download({
	        format: "xls",
	        separator: ",",
	        filename: "download",
	        jspname: "AlertsSummary",
	        linkname: "Export To XLS",
	        quotes: "\"",
	        linkid: "xlsLink1"
	    });
	    
	    jQuery( "#data_table_stationary1" ).table_download({
	        format: "csv",
	        separator: "-",
	        filename: "download",
	        jspname: "AlertsSummary",
	        linkname: "Export To CSV",
	        quotes: "\"",
	        linkid: "csvLink1"
	    });  
	    
	});
	var currentTime = new Date();
	var currentMonth = currentTime.getMonth() + 1;
	var currentDay = currentTime.getDate();
	var currentYear = currentTime.getFullYear();
	var dt_to = currentYear + "/" + currentMonth + "/" + currentDay;
	currentTime.setDate(currentTime.getDate() - 7);
	var dt_from= currentTime.getFullYear() + "/" + (currentTime.getMonth() + 1) + "/" + (currentTime.getDate());
	currentTime = new Date(<%= dToDate.getTime()%>);
	currentMonth = currentTime.getMonth() + 1;
	currentDay = currentTime.getDate();
	currentYear = currentTime.getFullYear();
	var dt_to1 = zeroPad(currentDay, 2) + "/" + zeroPad(currentMonth, 2) + "/" + currentYear;
	var dt_to2 = currentTime.getFullYear() + "/" + (currentTime.getMonth() + 1) + "/" + (currentTime.getDate());;
	currentTime = new Date(<%=dFromDate.getTime()%>);
	var dt_from1= zeroPad(currentTime.getDate(), 2) + "/" + zeroPad((currentTime.getMonth() + 1), 2) + "/" + currentTime.getFullYear();
	var dt_from2= currentTime.getFullYear() + "/" + (currentTime.getMonth() + 1) + "/" + (currentTime.getDate());;
	$('.slider-time').html(dt_from1);
	$('.slider-time2').html(dt_to1);
	$('#from_date').val(dt_from1);
	$('#to_date').val(dt_to1);
	var min_val = Date.parse(dt_from)/1000;
	var max_val = Date.parse(dt_to)/1000;
	var min_val1 = Date.parse(dt_from2)/1000;
	var max_val1 = Date.parse(dt_to2)/1000;
	function zeroPad(num, places) {
	  var zero = places - num.toString().length + 1;
	  return Array(+(zero > 0 && zero)).join("0") + num;
	}
	function formatDT(__dt) {
	    var year = __dt.getFullYear();
	    var month = zeroPad(__dt.getMonth()+1, 2);
	    var date = zeroPad(__dt.getDate(), 2);
	    return date + '/' + month + '/' + year;
	};

	$("#slider-range").slider({
	    range: true,
	    min: min_val,
	    max: max_val,
	    step: 10,
	    values: [min_val1, max_val1],
	    slide: function (e, ui) {
	        var dt_cur_from = new Date(ui.values[0]*1000); //.format("yyyy-mm-dd hh:ii:ss");
	        $('.slider-time').html(formatDT(dt_cur_from));

	        var dt_cur_to = new Date(ui.values[1]*1000); //.format("yyyy-mm-dd hh:ii:ss");                
	        $('.slider-time2').html(formatDT(dt_cur_to));
	        $('#from_date').val(formatDT(dt_cur_from));
	        $('#to_date').val(formatDT(dt_cur_to));
	    }
	});


</script>
	</body>
</html>
