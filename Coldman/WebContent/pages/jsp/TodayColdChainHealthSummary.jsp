<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ page import="com.coldman.dao.ClientLocationMapDao"%>
<%@ page import="com.coldman.dao.AlertsWorkflowDao"%>
<%@ page import="java.util.Collection"%>
<%@ page import="com.coldman.util.Constants"%>
<%@ page import="com.coldman.util.Utils"%>
<%@ page import="com.coldman.bean.AllLocationDataBean"%>
<%@ page import="com.coldman.bean.ImageCorSelectBean"%>
<%@ page import="com.coldman.bean.TodaysAlertWorkflowBean"%>
<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>
<%@ page import="java.util.Set"%>
<%@ page import="java.util.TreeMap"%>
<%@ page import="java.util.Iterator"%>
<%
response.setHeader("Cache-Control","no-cache"); 
response.setHeader("Pragma","no-cache"); 
response.setDateHeader ("Expires", -1); 
%>
<%!
static final Logger logger = LoggerFactory.getLogger("TodayColdChainHealthSummary");
ClientLocationMapDao clmDao = new ClientLocationMapDao();
%>
<%
String sUsername = (String) session.getAttribute("username");
if(sUsername == null || sUsername.equals("")) {
	session.setAttribute("callingPage", "TodayColdChainHealthSummary.jsp");
	response.sendRedirect("../../index.html");
}
sUsername = (String) session.getAttribute("username");
java.util.Date dt = new java.util.Date();
java.text.SimpleDateFormat sdf = 
     new java.text.SimpleDateFormat("dd-MM-yyyy HH:mm");
sdf.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
String currentTime = sdf.format(dt);
String sZoneId = request.getParameter("ZoneId");
if(sZoneId == null || sZoneId.equals("")) sZoneId = "ZN-SY-DMO-000001";
String sLocationName = "", sZoneName = "";
TreeMap<String, String> tmLocID = new TreeMap<String, String>();
TreeMap<String, String> tmZoneID = new TreeMap<String, String>();
if(clmDao.colAllLocations == null || clmDao.colAllLocations.size() == 0) clmDao.getAllLocationsData();
for(AllLocationDataBean alBean: clmDao.colAllLocations){
	if(alBean.getZone_Id().equals(sZoneId)) {
		sLocationName = alBean.getLocation_Name();
		sZoneName = alBean.getZone_Name();
	}
}
for(AllLocationDataBean alBean: clmDao.colAllLocations){
	tmLocID.put(alBean.getLocation_Name(), "1");
	if(alBean.getLocation_Name().equals(sLocationName)) {
		if(!alBean.getZone_Id().equals("")) tmZoneID.put(alBean.getZone_Id(), alBean.getZone_Name());
	}
}
String sValue = "";
AlertsWorkflowDao awd = new AlertsWorkflowDao();
TodaysAlertWorkflowBean tawBean = new TodaysAlertWorkflowBean();
Collection<TodaysAlertWorkflowBean> colTodayAlerts = awd.getTodaysAlerts();
Collection<TodaysAlertWorkflowBean> colAlertsComp = awd.getAlertsComp();
float iTemperatureCRCount = 0, iHumidityCRCount = 0, iDoorOpenCRCount = 0;
float iCompTemperatureCRCount = 0, iCompHumidityCRCount = 0, iCompDoorOpenCRCount = 0;
for(TodaysAlertWorkflowBean taw: colTodayAlerts) {
	if(!taw.getZone_ID().equals(sZoneId)) continue;
	
	if(taw.getAlert_Type().equals(Constants.TEMPERATURE)){ iTemperatureCRCount += taw.getCount_Of_Alerts(); }
	else if(taw.getAlert_Type().equals(Constants.HUMIDITY)) { iHumidityCRCount += taw.getCount_Of_Alerts(); }
	else if(taw.getAlert_Type().equals(Constants.DOOR_ACTIVITY)) { iDoorOpenCRCount += taw.getCount_Of_Alerts(); }
}
for(TodaysAlertWorkflowBean taw: colAlertsComp) {
	if(!taw.getZone_ID().equals(sZoneId)) continue;
	if(taw.getAlert_Type().equals(Constants.TEMPERATURE)) { iCompTemperatureCRCount += taw.getCount_Of_Alerts(); }
	else if(taw.getAlert_Type().equals(Constants.HUMIDITY)) { iCompHumidityCRCount += taw.getCount_Of_Alerts(); }
	else if(taw.getAlert_Type().equals(Constants.DOOR_ACTIVITY)) { iCompDoorOpenCRCount += taw.getCount_Of_Alerts(); }
}
float iDiffTemperatureCRCount = 0, iDiffHumidityCRCount = 0, iDiffDoorOpenCRCount = 0;
if(iCompTemperatureCRCount > 0) iDiffTemperatureCRCount = ((iTemperatureCRCount-iCompTemperatureCRCount)/iCompTemperatureCRCount)*100;
if(iCompHumidityCRCount > 0) iDiffHumidityCRCount = ((iHumidityCRCount-iCompHumidityCRCount)/iCompHumidityCRCount)*100;
if(iCompDoorOpenCRCount > 0) iDiffDoorOpenCRCount = ((iDoorOpenCRCount-iCompDoorOpenCRCount)/iCompDoorOpenCRCount)*100;
TreeMap tmImageCorSel = awd.getImageCorSelect();
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Today Cold Chain Health Summary</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Add these below two lines for autocomplete and the javascript function at the end  -->
<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="../../bootstrap/css/bootstrap.css">
   
<!-- Font Awesome -->
<script src="https://use.fontawesome.com/37ba1872d2.js"></script>
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../../dist/css/tagbox.css">
<script src="../../dist/js/tagbox.js"></script>
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
<link href="https://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
  body {
	font-family:  Lato, sans-serif;
}
</style>
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
	<div class="wrapper">

		<header class="main-header">
			<!-- Logo -->
			<div>
				<a href="#" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
					<span class="logo-mini"><img src="../../dist/img/cipla_logo.jpeg" style="margin-top:14px"
						alt="User Image"></span> <!-- logo for regular state and mobile devices -->
					<span class="logo-lg"><img src="../../dist/img/cipla_logo.jpeg" 
						alt="User Image"></span>
				</a>
			</div>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">

				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
					role="button"> <span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
				</a>
				<form class="navbar-form pull-left" role="search">
					<div class="form-group">
						<b style="padding-left: 15px; font-size: 20px;color:#fff;" class="pull-right">Today's Cold Chain Health Summary</b><br> <b style="padding-left: 15px;color:#fff;"><%= currentTime %></b>
					</div>
				</form>

				<div class="navbar-custom-menu">
					<img src="../../dist/img/Logo.png" class="logo-class pull-right"
						alt="User Image">
				</div>
			</nav>

		</header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->


				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<br>
				<ul class="sidebar-menu">
					<li class="header">MAIN NAVIGATION</li>
					<li class="treeview active"><a href="#"> <i
							class="fa fa-dashboard"></i> <span>Dashboard</span>
					</a>
						<ul class="treeview-menu">
							<li>
							<%if(sUsername != null && sUsername.equals("demo2")) { %>
							<a href="CMCDashboard.jsp"><i
									class="fa fa-circle-o"></i> CMC</a>
							<%} else { %>
							<a href="CMCDashboard.jsp" role="button" class="btn btn-disabled disabled"><i
									class="fa fa-circle-o"></i> CMC</a>
							<%} %>
									</li>
							<!-- <li class=""><a href="NewDashboard.jsp"><i
									class="fa fa-circle-o"></i> Vehicle Dashboard</a></li> -->
							<li class="active"><a href="TodayColdChainHealth.jsp"><i
									class="fa fa-circle-o"></i> Cold Rooms Summary</a></li>
							<!--  <li><a href="VehiclePlannedRouteMap.jsp"><i class="fa fa-circle-o"></i> Vehicle Planned Route</a></li> -->
							<li class=""><a href="TodayColdChainHealthVehicle.jsp" role="button" class="btn btn-disabled disabled"><i
									class="fa fa-circle-o"></i> Vehicles Summary</a></li>
						</ul></li>
					<li class="treeview"><a href="#"> <i
							class="fa fa-files-o"></i> <span>Alerts</span> 
					</a>
						<ul class="treeview-menu">
							<li><a href="LiveVehicleTracking.jsp" role="button" class="btn btn-disabled disabled"><i
									class="fa fa-circle-o"></i> Live Vehicle Tracking</a></li>
						</ul></li>
					
					<li class="treeview"><a href="#"> <i
							class="fa fa-database"></i> <span>Admin</span>
					</a>
						<ul class="treeview-menu">
							<li class=""><a href="VehicleManagement.jsp" role="button" class="btn btn-disabled disabled"><i class="fa fa-circle-o"></i>
									Vehicle Management</a></li>
							<li><a href="VendorScorecard.jsp" role="button" class="btn btn-disabled disabled"><i class="fa fa-circle-o"></i>
									Vehicle Score Card</a></li>
						</ul></li>
						<li class="treeview"><a href="#"> <i
							class="fa fa-bar-chart"></i> <span>Reports</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="ParameterSummary.jsp"><i class="fa fa-circle-o"></i>
									Parameter Summary</a></li>
							<li><a href="AlertsSummary.jsp"><i class="fa fa-circle-o"></i>
									Alerts Summary</a></li>
							<li><a href="AlertsReport.jsp"><i class="fa fa-circle-o"></i>
									Alerts Report</a></li>		
							
						</ul></li>
						<li class="treeview"><a href="#"> <i
							class="fa fa-history"></i> <span>Historical Data</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="HistoricalData.jsp"><i class="fa fa-circle-o"></i>
									Download <br>Temperature Data</a></li>
						</ul></li>
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			
		<section id="mainSection" class="content">
			<div class="row">
				<div class="col-md-12">
				<b>
				<%if(sUsername != null && sUsername.equals("demo2")) { %>
				<a href="CMCDashboard.jsp">CMC</a>  &nbsp;<i class="fa fa-caret-right"></i> &nbsp;
				<%} %>
				<a href="TodayColdChainHealth.jsp?LocationId=<%= sLocationName%>"><%= sLocationName %></a>&nbsp; <i class="fa fa-caret-right"></i> &nbsp;<%= sZoneName %></b><br> <br>
					<div class="row">
						
							
										<div class="col-md-8">
											<div class="well well-sm">
												<div class="text-center">
												<div id="map_1">
<%
String sImgTempND1 = "", sImgTempND2 = "", sImgTempND3 = "", sImgTempND4 = "";
String sImgHumND1 = "", sImgHumND2 = "", sImgHumND3 = "", sImgHumND4 = "";
ImageCorSelectBean imgCorSel = new ImageCorSelectBean();
if(sZoneId.equals("ZN-SY-DMO-000001")) {%>
<img src="../../dist/img/1.png" alt="YourImage" usemap="#YourMap1" id="map_2">
<map name="YourMap1">
	<area alt="Node1" title="" href="TodayColdChainHealthDetails.jsp?Node=SZ-SY-DMO-000001" shape="rect" coords="13,81,57,113" />
</map>
<%if(tmImageCorSel.containsKey("SZ-SY-DMO-000001")) {
		imgCorSel = (ImageCorSelectBean) tmImageCorSel.get("SZ-SY-DMO-000001");
		sImgTempND1 = imgCorSel.getsTemperature();
		sImgHumND1 = imgCorSel.getsHumidity();
	} else {
		sImgTempND1 = "NA";
		sImgHumND1 = "NA";
	}
if(sImgTempND1.equals("NA")) {%>
<h5 class="h5_6"><span>T: <%=sImgTempND1 %><br>H: <%=sImgHumND1 %></span></h5>
<%} else {%>
<h5 class="h5_6"><span>T: <%=sImgTempND1 %>&deg;C<br>H: <%=sImgHumND1 %></span></h5>
<%}
%>

<%} else if(sZoneId.equals("ZN-SY-DMO-000002")) { %>
<img src="../../dist/img/2.png" alt="YourImage" usemap="#YourMap1" id="map_2">
<map name="YourMap1">
	<area alt="Node2" title="" href="TodayColdChainHealthDetails.jsp?Node=SZ-SY-DMO-000002" shape="rect" coords="13,81,57,113" />   
</map>
<%if(tmImageCorSel.containsKey("SZ-SY-DMO-000002")) {
		imgCorSel = (ImageCorSelectBean) tmImageCorSel.get("SZ-SY-DMO-000002");
		sImgTempND1 = imgCorSel.getsTemperature();
		sImgHumND1 = imgCorSel.getsHumidity();
	} else {
		sImgTempND1 = "NA";
		sImgHumND1 = "NA";
	}
if(sImgTempND1.equals("NA")) {%>
<h5 class="h5_6"><span>T: <%=sImgTempND1 %><br>H: <%=sImgHumND1 %></span></h5>
<%} else {%>
<h5 class="h5_6"><span>T: <%=sImgTempND1 %>&deg;C<br>H: <%=sImgHumND1 %></span></h5>
<%}
%>
	
<%}  else if(sZoneId.equals("ZN-SY-DMO-000003")) { %>
<img src="../../dist/img/3.png" alt="YourImage" usemap="#YourMap1" id="map_2">
<map name="YourMap1">
  	<area alt="Node1" title="" href="TodayColdChainHealthDetails.jsp?Node=SZ-SY-DMO-000004" shape="rect" coords="5,126,117,170" />
  	<area alt="Node2" title="" href="TodayColdChainHealthDetails.jsp?Node=SZ-SY-DMO-000003" shape="rect" coords="215,14,255,42" />
	<area alt="Node4" title="" href="TodayColdChainHealthDetails.jsp?Node=SZ-SY-DMO-000006" shape="rect" coords="215,263,255,288" />
	<area alt="Node3" title="" href="TodayColdChainHealthDetails.jsp?Node=SZ-SY-DMO-000005" shape="rect" coords="433,138,541,164" />
</map>	
<%if(tmImageCorSel.containsKey("SZ-SY-DMO-000003")) {
		imgCorSel = (ImageCorSelectBean) tmImageCorSel.get("SZ-SY-DMO-000003");
		sImgTempND1 = imgCorSel.getsTemperature();
		sImgHumND1 = imgCorSel.getsHumidity();
	} else {
		sImgTempND1 = "NA";
		sImgHumND1 = "NA";
	}
if(tmImageCorSel.containsKey("SZ-SY-DMO-000004")) {
	imgCorSel = (ImageCorSelectBean) tmImageCorSel.get("SZ-SY-DMO-000004");
	sImgTempND2 = imgCorSel.getsTemperature();
	sImgHumND2 = imgCorSel.getsHumidity();
} else {
	sImgTempND2 = "NA";
	sImgHumND2 = "NA";
}
if(tmImageCorSel.containsKey("SZ-SY-DMO-000005")) {
	imgCorSel = (ImageCorSelectBean) tmImageCorSel.get("SZ-SY-DMO-000005");
	sImgTempND4 = imgCorSel.getsTemperature();
	sImgHumND4 = imgCorSel.getsHumidity();
} else {
	sImgTempND4 = "NA";
	sImgHumND4 = "NA";
}
if(tmImageCorSel.containsKey("SZ-SY-DMO-000006")) {
	imgCorSel = (ImageCorSelectBean) tmImageCorSel.get("SZ-SY-DMO-000006");
	sImgTempND3 = imgCorSel.getsTemperature();
	sImgHumND3 = imgCorSel.getsHumidity();
} else {
	sImgTempND3 = "NA";
	sImgHumND3 = "NA";
}


if(sImgTempND1.equals("NA")) {%>
<h5 class="h5_1"><span>T: <%=sImgTempND1 %><br>H: <%=sImgHumND1 %></span></h5>
<%} else {%>
<h5 class="h5_1"><span>T: <%=sImgTempND1 %>&deg;C<br>H: <%=sImgHumND1 %></span></h5>
<%}
if(sImgTempND2.equals("NA")) {%>
<h5 class="h5_2"><span>T: <%=sImgTempND2 %><br>H: <%=sImgHumND2 %></span></h5>
<%} else {%>
<h5 class="h5_2"><span>T: <%=sImgTempND2 %>&deg;C<br>H: <%=sImgHumND2 %></span></h5>
<%}
if(sImgTempND3.equals("NA")) {%>
<h5 class="h5_3"><span>T: <%=sImgTempND3 %><br>H: <%=sImgHumND3 %></span></h5>
<%} else {%>
<h5 class="h5_3"><span>T: <%=sImgTempND3 %>&deg;C<br>H: <%=sImgHumND3 %></span></h5>
<%}
if(sImgTempND4.equals("NA")) {%>
<h5 class="h5_4"><span>T: <%=sImgTempND4 %><br>H: <%=sImgHumND4 %></span></h5>
<%} else {%>
<h5 class="h5_4"><span>T: <%=sImgTempND4 %>&deg;C<br>H: <%=sImgHumND4 %></span></h5>
<%}
}
%>
	</div>
									</div>
											</div>
										</div>
									<div class="col-md-offset-2 col-md-2">
							<div class="text-left">
							<label>GO TO:</label>
				<div class="scroll-area2 white-background">
				<%Set set = tmLocID.keySet();
				Set set1 = tmZoneID.keySet();
				Iterator iter = set.iterator();
				Iterator iter1 = set1.iterator();
				String sValue1 = "";
				while(iter.hasNext()) {
					sValue = (String) iter.next();
					if(sValue.equals(sLocationName)){%>
					<a href="TodayColdChainHealth.jsp?LocationId=<%=sValue%>"><%= sValue %></a><br>
					<%
							while(iter1.hasNext()) {
								sValue1 = (String) iter1.next();
								if(sValue1.equals(sZoneId)) {%>
									&nbsp;&nbsp;<b><a href="TodayColdChainHealthSummary.jsp?ZoneId=<%=sValue1 %>"><%=tmZoneID.get(sValue1).toUpperCase() %></a></b><br>
								<%} else { %>
									&nbsp;&nbsp;<a href="TodayColdChainHealthSummary.jsp?ZoneId=<%=sValue1 %>"><%=tmZoneID.get(sValue1) %></a><br>
								<%}
							}
					} else { %>
					<a href="TodayColdChainHealth.jsp?LocationId=<%=sValue%>"><%= sValue %></a><br>
					<%}
					
				}%>
				</div>
												</div>
											</div>
										
									</div>
									
									<div class="row">
				<div class="col-md-8 text-center">
				<div class="well well-sm white-background">
				<div class="text-center">
					<b>TOTAL NUMBER OF ALERTS TODAY</b>
					</div><br>
					<div class="row">
						<div class="col-md-12">
							TOTAL COLD ROOM ALERTS: <B><%= Utils.zeroPad(Math.round((iTemperatureCRCount+iHumidityCRCount+iDoorOpenCRCount)), 2) %></B>
							<div class="well well-sm white-background">
					<div class="row">
								<div class="col-md-3">
								TEMPERATURE
								<div class="row"><br>
									<div class="col-md-offset-1 col-md-3">
									<img src="../../dist/img/temperature.png">
									</div>
									<div class="col-md-offset-1 col-md-7 text-left">
								<b class="font-new"><%= Utils.zeroPad(Math.round(iTemperatureCRCount), 2) %></b>
									</div>
								</div>
								<%if(iDiffTemperatureCRCount == 0) { %>
									<b><span class="description-percentage">NA</span></b>
								<%} else if(iDiffTemperatureCRCount > 0) { %>
									<b><span class="description-percentage text-red"><i class="fa fa-caret-up"></i>&nbsp;<%=Utils.zeroPad(Math.round(iDiffTemperatureCRCount), 2) %>%</span></b>
								<%} else { %>
									<b><span class="description-percentage text-green"><i class="fa fa-caret-down"></i>&nbsp;<%=Utils.zeroPad(Math.round(Math.abs(iDiffTemperatureCRCount)), 2) %>%</span></b>
								<%} %>
								</div>
								<div class="col-md-3">
								HUMIDITY
								<div class="row"><br>
									<div class="col-md-offset-1 col-md-3">
									<img src="../../dist/img/humidity.png">
									</div>
									<div class="col-md-offset-1 col-md-7 text-left">
								<b class="font-new text-left"><%=Utils.zeroPad(Math.round(iHumidityCRCount), 2)%></b>
									</div>
								</div>
								<%if(iDiffHumidityCRCount == 0) { %>
									<b><span class="description-percentage">NA</span></b>
								<%} else if(iDiffHumidityCRCount > 0) { %>
									<b><span class="description-percentage text-red"><i class="fa fa-caret-up"></i>&nbsp;<%=Utils.zeroPad(Math.round(iDiffHumidityCRCount), 2) %>%</span></b>
								<%} else { %>
									<b><span class="description-percentage text-green"><i class="fa fa-caret-down"></i>&nbsp;<%=Utils.zeroPad(Math.round(Math.abs(iDiffHumidityCRCount)), 2) %>%</span></b>
								<%} %>
								</div>
								<div class="col-md-3">
								DOOR OPEN
								<div class="row"><br>
									<div class="col-md-offset-1 col-md-3">
									<img src="../../dist/img/dooropen.png">
									</div>
									<div class="col-md-offset-1 col-md-7 text-left">
								<b class="font-new text-left">NA</b>
									</div>
								</div>
								
								</div>
								<div class="col-md-3">
								POWER
								<div class="row">
									<br>
									<div class="col-md-12 text-center">
								<b class="font-new text-left">NA</b>
									</div>
								</div>
								</div>
								</div>
							</div>
						</div>
						
					
							
						</div>
					</div>
					</div>
				</div>
									
									
								</div>
							</div>
						
		</section>
		





		</div>
		<!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0
			</div>
			<strong>Copyright &copy; 2017 <a href="http://tagbox.in">TagBox</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Control Sidebar -->
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>


	<!-- Bootstrap 3.3.6 -->
	<script src="../../bootstrap/js/bootstrap.min.js"></script>
	<!-- ChartJS 1.0.1 -->
	<!-- AdminLTE App -->
	<script src="../../dist/js/app.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="../../dist/js/demo.js"></script>
	
<script>
$('<p>').appendTo('#map_2').html('<b>T: 5&deg;C<br>H: 85</b>').css({position:'absolute',
    top:'50px',
    left:'59px'});
//$('<p>').appendTo('#map_2').html('<b>T: 7&deg;C<br>H: 87</b>').css({position:'absolute',
//    top:'129px',
//    left:'509px'});    
</script>
	</body>
</html>
