<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ page import="java.util.TreeMap"%>
<%@ page import="java.util.LinkedHashMap"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.util.Collection"%>
<%@ page import="com.coldman.util.Utils"%>
<%@ page import="com.coldman.util.Constants"%>
<%@ page import="java.util.Set"%>
<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>
<%@ page import="com.coldman.dao.AlertsWorkflowDao"%>
<%@ page import="com.coldman.dao.TransitDao"%>
<%@ page import="com.coldman.dao.ClientLocationMapDao"%>
<%@ page import="com.coldman.bean.TodaysAlertWorkflowBean"%>
<%@ page import="com.coldman.bean.LocationVehicleAlertsDataBean"%>
<%@ page import="com.coldman.bean.AllLocationDataBean"%>
<%@ page import="com.coldman.bean.AllVehicleDataBean"%>
<%
response.setHeader("Cache-Control","no-cache"); 
response.setHeader("Pragma","no-cache"); 
response.setDateHeader ("Expires", -1); 
%>
<%!
static final Logger logger = LoggerFactory.getLogger("CMCDashboard");
ClientLocationMapDao clmDao = new ClientLocationMapDao();
TransitDao trDao = new TransitDao();
%>
<%
String sUsername = (String) session.getAttribute("username");
if(sUsername == null || sUsername.equals("")) {
	session.setAttribute("callingPage", "CMCDashboard.jsp");
	response.sendRedirect("../../index.html");
}
TreeMap<String, String> tmMULocs = new TreeMap<String, String>();
TreeMap<String, String> tmDCLocs = new TreeMap<String, String>();
TreeMap<String, String> tmMUZoneID = new TreeMap<String, String>();
TreeMap<String, String> tmDCZoneID = new TreeMap<String, String>();
String sValue = "";
float iMUDCCount = 0, iDCDCCount = 0, iDCEPCount = 0;
if(clmDao.colAllLocations == null || clmDao.colAllLocations.size() == 0) clmDao.getAllLocationsData();
if(trDao.colAllVehicles == null || trDao.colAllVehicles.size() == 0) trDao.getAllVehiclesData();
for(AllLocationDataBean alBean: clmDao.colAllLocations){
	if(alBean.getLocation_Type().equals(Constants.MANUFACTURING_LOCATION_TYPE)){
		if(!alBean.getZone_Id().equals("")) tmMUZoneID.put(alBean.getZone_Id(), "1");
		if(tmMULocs.containsKey(alBean.getLocation_Name())) {
			sValue = tmMULocs.get(alBean.getLocation_Name());
			if(sValue.indexOf(alBean.getZone_Id()) >= 0) {
				
			} else {
				sValue = sValue + ";" + alBean.getZone_Id();
				tmMULocs.put(alBean.getLocation_Name(), sValue);
			}
		} else {
			sValue = alBean.getZone_Id();
			if(!alBean.getZone_Id().equals("")) tmMULocs.put(alBean.getLocation_Name(), sValue);
		}
	} else if(alBean.getLocation_Type().equals(Constants.DC_LOCATION_TYPE)){
		if(!alBean.getZone_Id().equals("")) tmDCZoneID.put(alBean.getZone_Id(), "1");
		if(tmDCLocs.containsKey(alBean.getLocation_Name())) {
			sValue = tmDCLocs.get(alBean.getLocation_Name());
			if(sValue.indexOf(alBean.getZone_Id()) >= 0) {
				
			} else {
				sValue = sValue + ";" + alBean.getZone_Id();
				tmDCLocs.put(alBean.getLocation_Name(), sValue);
			}
		} else {
			sValue = alBean.getZone_Id();
			if(!alBean.getZone_Id().equals("")) tmDCLocs.put(alBean.getLocation_Name(), sValue);
		}
	}
}
for(AllVehicleDataBean alBean: trDao.colAllVehicles){
	if(alBean.getRoute_Type().equals(Constants.ROUTE_TYPE_MU_DC)){
		iMUDCCount++;
	} else if(alBean.getRoute_Type().equals(Constants.ROUTE_TYPE_DC_DC)){
		iDCDCCount++;
	} else if(alBean.getRoute_Type().equals(Constants.ROUTE_TYPE_DC_EP)){
		iDCEPCount++;
	} else {
		
	}
}
float iTodayRedMan = 0, iTodayOrangeMan = 0, iTodayGreenMan = 0, iTodayRedDC = 0, iTodayOrangeDC = 0, iTodayGreenDC = 0;
float iTodayRedMUDC = 0, iTodayOrangeMUDC = 0, iTodayGreenMUDC = 0;
float iTodayRedDCDC = 0, iTodayOrangeDCDC = 0, iTodayGreenDCDC = 0,iTodayRedDCEP = 0, iTodayOrangeDCEP = 0, iTodayGreenDCEP = 0;
float iCompRedMan = 0, iCompOrangeMan = 0, iCompGreenMan = 0, iCompRedDC = 0, iCompOrangeDC = 0, iCompGreenDC = 0;
float iCompRedMUDC = 0, iCompOrangeMUDC = 0, iCompGreenMUDC = 0;
float iCompRedDCDC = 0, iCompOrangeDCDC = 0, iCompGreenDCDC = 0,iCompRedDCEP = 0, iCompOrangeDCEP = 0, iCompGreenDCEP = 0;
float iTemperatureCRCount = 0, iHumidityCRCount = 0, iDoorOpenCRCount = 0, iTemperatureVehCount = 0, iHumidityVehCount = 0, iDoorOpenVehCount = 0, iStoppageVehCount = 0; 
String sPower = "ON";
float iCompTemperatureCRCount = 0, iCompHumidityCRCount = 0, iCompDoorOpenCRCount = 0, iCompTemperatureVehCount = 0,
	iCompHumidityVehCount = 0, iCompDoorOpenVehCount = 0, iCompStoppageVehCount = 0;
AlertsWorkflowDao awd = new AlertsWorkflowDao();
TodaysAlertWorkflowBean tawBean = new TodaysAlertWorkflowBean();
Collection<TodaysAlertWorkflowBean> colTodayAlerts = awd.getTodaysAlerts();
Collection<TodaysAlertWorkflowBean> colAlertsComp = awd.getAlertsComp();
TreeMap<String, String> tmMULocs1 = new TreeMap<String, String>();
TreeMap<String, String> tmDCLocs1 = new TreeMap<String, String>();

for(TodaysAlertWorkflowBean taw: colTodayAlerts) {
	if(taw.getLocation_Type().equals(Constants.MANUFACTURING_LOCATION_TYPE)) {
		if(taw.getCount_Of_Alerts() > 3) iTodayRedMan++;
		else if(taw.getCount_Of_Alerts() <= 3 && taw.getCount_Of_Alerts() > 0) iTodayOrangeMan++;
		else if(taw.getCount_Of_Alerts() == 0) iTodayGreenMan++;
		if(taw.getCount_Of_Alerts() > 0){
			if(tmMULocs1.containsKey(taw.getLocation_Name())) {
				sValue = tmMULocs1.get(taw.getLocation_Name());
				if(sValue.indexOf(taw.getZone_ID()) >= 0) {
					
				} else {
					sValue = sValue + ";" + taw.getZone_ID();
					tmMULocs1.put(taw.getLocation_Name(), sValue);
				}
			} else {
				sValue = taw.getZone_ID();
				tmMULocs1.put(taw.getLocation_Name(), sValue);
			}
		}
	} else if(taw.getLocation_Type().equals(Constants.DC_LOCATION_TYPE)) {
		if(taw.getCount_Of_Alerts() > 3) iTodayRedDC++;
		else if(taw.getCount_Of_Alerts() <= 3 && taw.getCount_Of_Alerts() > 0) iTodayOrangeDC++;
		else if(taw.getCount_Of_Alerts() == 0) iTodayGreenDC++;
		if(taw.getCount_Of_Alerts() > 0){
			if(tmDCLocs1.containsKey(taw.getLocation_Name())) {
				sValue = tmDCLocs1.get(taw.getLocation_Name());
				if(sValue.indexOf(taw.getZone_ID()) >= 0) {
					
				} else {
					sValue = sValue + ";" + taw.getZone_ID();
					tmDCLocs1.put(taw.getLocation_Name(), sValue);
				}
			} else {
				sValue = taw.getZone_ID();
				tmDCLocs1.put(taw.getLocation_Name(), sValue);
			}
		}
	}
	if(taw.getRoute_Type().equals(Constants.ROUTE_TYPE_MU_DC)) {
		if(taw.getCount_Of_Alerts() > 3) iTodayRedMUDC++;
		else if(taw.getCount_Of_Alerts() <= 3 && taw.getCount_Of_Alerts() > 0) iTodayOrangeMUDC++;
		else if(taw.getCount_Of_Alerts() == 0) iTodayGreenMUDC++;
	} else if(taw.getRoute_Type().equals(Constants.ROUTE_TYPE_DC_DC)) {
		if(taw.getCount_Of_Alerts() > 3) iTodayRedDCDC++;
		else if(taw.getCount_Of_Alerts() <= 3 && taw.getCount_Of_Alerts() > 0) iTodayOrangeDCDC++;
		else if(taw.getCount_Of_Alerts() == 0) iTodayGreenDCDC++;
	} else if(taw.getRoute_Type().equals(Constants.ROUTE_TYPE_DC_EP)) {
		if(taw.getCount_Of_Alerts() > 3) iTodayRedDCEP++;
		else if(taw.getCount_Of_Alerts() <= 3 && taw.getCount_Of_Alerts() > 0) iTodayOrangeDCEP++;
		else if(taw.getCount_Of_Alerts() == 0) iTodayGreenDCEP++;
	}
	if(taw.getClassType().equals(Constants.STATIONARY)) {
		if(taw.getAlert_Type().equals(Constants.TEMPERATURE)){ iTemperatureCRCount += taw.getCount_Of_Alerts(); }
		else if(taw.getAlert_Type().equals(Constants.HUMIDITY)) { iHumidityCRCount += taw.getCount_Of_Alerts(); }
		else if(taw.getAlert_Type().equals(Constants.DOOR_ACTIVITY)) { iDoorOpenCRCount += taw.getCount_Of_Alerts(); }
	} else if(taw.getClassType().equals(Constants.TRANSIT)) {
		if(taw.getAlert_Type().equals(Constants.TEMPERATURE)) { iTemperatureVehCount += taw.getCount_Of_Alerts(); }
		else if(taw.getAlert_Type().equals(Constants.HUMIDITY)) { iHumidityVehCount += taw.getCount_Of_Alerts(); }
		else if(taw.getAlert_Type().equals(Constants.DOOR_ACTIVITY)) { iDoorOpenVehCount += taw.getCount_Of_Alerts(); }
		else if(taw.getAlert_Type().equals(Constants.STOPPAGE)) { iStoppageVehCount += taw.getCount_Of_Alerts(); }
	}
}
for(TodaysAlertWorkflowBean taw: colAlertsComp) {
	if(taw.getLocation_Type().equals(Constants.MANUFACTURING_LOCATION_TYPE)) {
		if(taw.getCount_Of_Alerts() > 3) iCompRedMan++;
		else if(taw.getCount_Of_Alerts() <= 3 && taw.getCount_Of_Alerts() > 0) iCompOrangeMan++;
		else if(taw.getCount_Of_Alerts() == 0) iCompGreenMan++;
	} else if(taw.getLocation_Type().equals(Constants.DC_LOCATION_TYPE)) {
		if(taw.getCount_Of_Alerts() > 3) iCompRedDC++;
		else if(taw.getCount_Of_Alerts() <= 3 && taw.getCount_Of_Alerts() > 0) iCompOrangeDC++;
		else if(taw.getCount_Of_Alerts() == 0) iCompGreenDC++;
	}
	if(taw.getRoute_Type().equals(Constants.ROUTE_TYPE_MU_DC)) {
		if(taw.getCount_Of_Alerts() > 3) iCompRedMUDC++;
		else if(taw.getCount_Of_Alerts() <= 3 && taw.getCount_Of_Alerts() > 0) iCompOrangeMUDC++;
		else if(taw.getCount_Of_Alerts() == 0) iCompGreenMUDC++;
	} else if(taw.getRoute_Type().equals(Constants.ROUTE_TYPE_DC_DC)) {
		if(taw.getCount_Of_Alerts() > 3) iCompRedDCDC++;
		else if(taw.getCount_Of_Alerts() <= 3 && taw.getCount_Of_Alerts() > 0) iCompOrangeDCDC++;
		else if(taw.getCount_Of_Alerts() == 0) iCompGreenDCDC++;
	} else if(taw.getRoute_Type().equals(Constants.ROUTE_TYPE_DC_EP)) {
		if(taw.getCount_Of_Alerts() > 3) iCompRedDCEP++;
		else if(taw.getCount_Of_Alerts() <= 3 && taw.getCount_Of_Alerts() > 0) iCompOrangeDCEP++;
		else if(taw.getCount_Of_Alerts() == 0) iCompGreenDCEP++;
	}
	if(taw.getClassType().equals(Constants.STATIONARY)) {
		if(taw.getAlert_Type().equals(Constants.TEMPERATURE)) { iCompTemperatureCRCount += taw.getCount_Of_Alerts(); }
		else if(taw.getAlert_Type().equals(Constants.HUMIDITY)) { iCompHumidityCRCount += taw.getCount_Of_Alerts(); }
		else if(taw.getAlert_Type().equals(Constants.DOOR_ACTIVITY)) { iCompDoorOpenCRCount += taw.getCount_Of_Alerts(); }
	} else if(taw.getClassType().equals(Constants.TRANSIT)) {
		if(taw.getAlert_Type().equals(Constants.TEMPERATURE)) { iCompTemperatureVehCount += taw.getCount_Of_Alerts(); }
		else if(taw.getAlert_Type().equals(Constants.HUMIDITY)) { iCompHumidityVehCount += taw.getCount_Of_Alerts(); }
		else if(taw.getAlert_Type().equals(Constants.DOOR_ACTIVITY)) { iCompDoorOpenVehCount += taw.getCount_Of_Alerts(); }
		else if(taw.getAlert_Type().equals(Constants.STOPPAGE)) { iCompStoppageVehCount += taw.getCount_Of_Alerts(); }
	}
}
float iRedMan = 0, iOrangeMan = 0, iRedDC = 0, iOrangeDC = 0, iRedMUDC = 0, iOrangeMUDC = 0, iRedDCDC = 0, iOrangeDCDC = 0, iRedDCEP = 0, iOrangeDCEP = 0;
if(iCompRedMan > 0) iRedMan = ((iTodayRedMan-iCompRedMan)/iCompRedMan)*100;

if(iCompOrangeMan > 0){
	iOrangeMan = ((iTodayOrangeMan-iCompOrangeMan)/iCompOrangeMan)*100;
}

if(iCompRedDC > 0) iRedDC = ((iTodayRedDC-iCompRedDC)/iCompRedDC)*100;
if(iCompOrangeDC > 0) iOrangeDC = ((iTodayOrangeDC-iCompOrangeDC)/iCompOrangeDC)*100;
if(iCompRedMUDC > 0) iRedMUDC = ((iTodayRedMUDC-iCompRedMUDC)/iCompRedMUDC)*100;
if(iCompOrangeMUDC > 0) iOrangeMUDC = ((iTodayOrangeMUDC-iCompOrangeMUDC)/iCompOrangeMUDC)*100;
if(iCompRedDCDC > 0) iRedDCDC = ((iTodayRedDCDC-iCompRedDCDC)/iCompRedDCDC)*100;
if(iCompOrangeDCDC > 0) iOrangeDCDC = ((iTodayOrangeDCDC-iCompOrangeDCDC)/iCompOrangeDCDC)*100;
if(iCompRedDCEP > 0) iRedDCEP = ((iTodayRedDCEP-iCompRedDCEP)/iCompRedDCEP)*100;
if(iCompOrangeDCEP > 0) iOrangeDCEP = ((iTodayOrangeDCEP-iCompOrangeDCEP)/iCompOrangeDCEP)*100;

float iDiffTemperatureCRCount = 0, iDiffHumidityCRCount = 0, iDiffDoorOpenCRCount = 0, iDiffTemperatureVehCount = 0, iDiffHumidityVehCount = 0, 
	iDiffDoorOpenVehCount = 0, iDiffStoppageVehCount = 0;

if(iCompTemperatureCRCount > 0) iDiffTemperatureCRCount = ((iTemperatureCRCount-iCompTemperatureCRCount)/iCompTemperatureCRCount)*100;
if(iCompHumidityCRCount > 0) iDiffHumidityCRCount = ((iHumidityCRCount-iCompHumidityCRCount)/iCompHumidityCRCount)*100;
if(iCompDoorOpenCRCount > 0) iDiffDoorOpenCRCount = ((iDoorOpenCRCount-iCompDoorOpenCRCount)/iCompDoorOpenCRCount)*100;
if(iCompTemperatureVehCount > 0) iDiffTemperatureVehCount = ((iTemperatureVehCount-iCompTemperatureVehCount)/iCompTemperatureVehCount)*100;
if(iCompHumidityVehCount > 0) iDiffHumidityVehCount = ((iHumidityVehCount-iCompHumidityVehCount)/iCompHumidityVehCount)*100;
if(iCompDoorOpenVehCount > 0) iDiffDoorOpenVehCount = ((iDoorOpenVehCount-iCompDoorOpenVehCount)/iCompDoorOpenVehCount)*100;
if(iCompStoppageVehCount > 0) iDiffStoppageVehCount = ((iStoppageVehCount-iCompStoppageVehCount)/iCompStoppageVehCount)*100;


java.util.Date dt = new java.util.Date();
java.text.SimpleDateFormat sdf = 
     new java.text.SimpleDateFormat("dd-MM-yyyy HH:mm");
sdf.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
String currentTime = sdf.format(dt);
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Dashboard</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Add these below two lines for autocomplete and the javascript function at the end  -->
<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="../../bootstrap/css/bootstrap.css">
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
   
<!-- Font Awesome -->
<script src="https://use.fontawesome.com/37ba1872d2.js"></script>
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../../dist/css/tagbox.css">
<script src="../../dist/js/tagbox.js"></script>
<!-- <script src="../../dist/js/google_charts.js"></script> -->
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
<link href="https://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
   body {
	font-family:  Lato, sans-serif;
	font-weight: 600;
}
</style>
</head>
<!-- To collapse the menu, add the following class to body tag: sidebar-collapse  -->
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
	<div class="wrapper">

		<header class="main-header">
			<!-- Logo -->
			<div>
				<a href="#" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
					<span class="logo-mini"><img src="../../dist/img/cipla_logo.jpeg" style="margin-top:14px"
						alt="User Image"></span> <!-- logo for regular state and mobile devices -->
					<span class="logo-lg"><img src="../../dist/img/cipla_logo.jpeg" 
						alt="User Image"></span>
				</a>
			</div>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">

				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
					role="button"> <span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
				</a>
				<form class="navbar-form pull-left" role="search">
					<div class="form-group">
						<b style="padding-left: 15px; font-size: 20px;color:#fff;" class="pull-right;color:#fff;">Today's Cold Chain Health Summary</b><br> <b style="padding-left: 15px;color:#fff;"><%= currentTime %></b>
					</div>
				</form>

				<div class="navbar-custom-menu">
					<img src="../../dist/img/Logo.png" class="logo-class pull-right"
						alt="User Image">
				</div>
			</nav>

		</header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->


				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<br>
				<ul class="sidebar-menu">
					<li class="header">MAIN NAVIGATION</li>
					<li class="treeview active"><a href="#"> <i
							class="fa fa-dashboard"></i> <span>Dashboard</span>
					</a>
						<ul class="treeview-menu">
							<li class="active"><a href="CMCDashboard.jsp"><i
									class="fa fa-circle-o"></i> CMC</a></li>
							<!-- <li class=""><a href="NewDashboard.jsp"><i
									class="fa fa-circle-o"></i> Vehicle Dashboard</a></li> -->
							<li class=""><a href="TodayColdChainHealth.jsp"><i
									class="fa fa-circle-o"></i> Cold Rooms Summary</a></li>
							<!--  <li><a href="VehiclePlannedRouteMap.jsp"><i class="fa fa-circle-o"></i> Vehicle Planned Route</a></li> -->
							<li class=""><a href="TodayColdChainHealthVehicle.jsp" role="button" class="btn btn-disabled disabled"><i
									class="fa fa-circle-o"></i> Vehicles Summary</a></li>
						</ul></li>
					<li class="treeview"><a href="#"> <i
							class="fa fa-files-o"></i> <span>Alerts</span> 
					</a>
						<ul class="treeview-menu">
							<li><a href="LiveVehicleTracking.jsp" role="button" class="btn btn-disabled disabled"><i
									class="fa fa-circle-o"></i> Live Vehicle Tracking</a></li>
						</ul></li>
					
					<li class="treeview"><a href="#"> <i
							class="fa fa-database"></i> <span>Admin</span>
					</a>
						<ul class="treeview-menu">
							<li class=""><a href="VehicleManagement.jsp" role="button" class="btn btn-disabled disabled"><i class="fa fa-circle-o"></i>
									Vehicle Management</a></li>
							<li><a href="VendorScorecard.jsp" role="button" class="btn btn-disabled disabled"><i class="fa fa-circle-o"></i>
									Vehicle Score Card</a></li>
						</ul></li>
						<li class="treeview"><a href="#"> <i
							class="fa fa-bar-chart"></i> <span>Reports</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="ParameterSummary.jsp"><i class="fa fa-circle-o"></i>
									Parameter Summary</a></li>
							<li><a href="AlertsSummary.jsp"><i class="fa fa-circle-o"></i>
									Alerts Summary</a></li>
							<li><a href="AlertsReport.jsp"><i class="fa fa-circle-o"></i>
									Alerts Report</a></li>		
							
						</ul></li>
						<li class="treeview"><a href="#"> <i
							class="fa fa-history"></i> <span>Historical Data</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="HistoricalData.jsp"><i class="fa fa-circle-o"></i>
									Download <br>Temperature Data</a></li>
						</ul></li>
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			
		<section id="mainSection" class="content white-background">
		
			<div class="row">
				<div class="col-md-6">
					<div class="row">
						<div class="col-md-12">
						<div class="text-center"><i>Cold Rooms with:&nbsp;&nbsp;<i class="fa fa-square alert-text alert-font"></i> >3 Alerts&nbsp;&nbsp;
				<i class="fa fa-square warning-text alert-font"></i> 1-3 Alerts&nbsp;&nbsp;
				<i class="fa fa-square success-text alert-font"></i> No Alerts</i>
				</div>
							<div class="panel-coldroom text-center">
								<b>&nbsp;COLD ROOMS</b>
							</div>
							<div class="well well-sm white-background" style="height:450px">
								<div class="text-center">&nbsp;Total Cold Rooms: <b><%=tmMUZoneID.size() %></b></div><br>
								<div class="row">
									<div class="col-md-6 text-center">
										Cold Room Alert Status<br>
										<div class="row">
										
											<div class="col-md-offset-1 col-md-10"><br>
											
												<div class="graph_container">
													<canvas id="Chart1" class="chart1" height="40" width="250"></canvas>
													<div class="row text-center">
														<div class="col-md-4"><span data-toggle="tooltip" class="badge badge-big bg-red"><%= Utils.zeroPad(Math.round(iTodayRedMan), 2) %></span><br>
														<%if(iRedMan == 0) { %>
														<b><span class="description-percentage">NA</span></b>
														<%} else if(iRedMan > 0){ %>
														<b><span class="description-percentage text-red"><i class="fa fa-caret-up"></i>&nbsp;<%=Utils.zeroPad(Math.round(iRedMan), 2) %>%</span></b>
														<%} else { %>
														<b><span class="description-percentage text-green"><i class="fa fa-caret-down"></i>&nbsp;<%=Utils.zeroPad(Math.round(Math.abs(iRedMan)), 2) %>%</span></b>
														<%} %>
														</div>
														<div class="col-md-4"><span data-toggle="tooltip" class="badge badge-big orange"><%= Utils.zeroPad(Math.round(iTodayOrangeMan), 2) %></span><br>
														<%if(iOrangeMan == 0) { %>
														<b><span class="description-percentage">NA</span></b>
														<%} else if(iOrangeMan > 0) { %>
														<b><span class="description-percentage text-red"><i class="fa fa-caret-up"></i>&nbsp;<%=Utils.zeroPad(Math.round(iOrangeMan), 2) %>%</span></b>
														<%} else { %>
														<b><span class="description-percentage text-green"><i class="fa fa-caret-down"></i>&nbsp;<%=Utils.zeroPad(Math.round(Math.abs(iOrangeMan)), 2) %>%</span></b>
														<%} %>
														</div>
														<div class="col-md-4"><span data-toggle="tooltip" class="badge badge-big bg-green"><%= Utils.zeroPad(Math.round(iTodayGreenMan), 2) %></span></div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="well well-sm white-background div-height-small text-center">
											Warehouse w/ Alerts<br><br>
											<div class="scroll-area">
											<%Set set = tmMULocs.keySet();
												Iterator iter = set.iterator();
												String stemp = "";
												String[] sT = null;
												String[] sT1 = null;
												String sValue1 = "", stemp1 = "";
												int iSize = 0;
												while(iter.hasNext()) {
													sValue = (String)iter.next();
													stemp = tmMULocs.get(sValue);
													sT = stemp.split(";");
													if(tmMULocs1.containsKey(sValue)) {
														stemp1 = tmMULocs1.get(sValue);
														sT1 = stemp1.split(";");
														iSize = sT1.length;
													} else {
														iSize = 0;
													}
												%>
												<span class="pull-left"><a href="TodayColdChainHealth.jsp?LocationId=<%=sValue%>" style="color:#3c8dbc;text-decoration:underline;"><%= sValue%></a></span><b style="color:red" class="pull-right"><%=iSize %>/<%=sT.length %>&nbsp;&nbsp;</b><br><hr class="hr-no-padding">
												<%} %>
											</div>
										</div>
									</div>
								</div>
								<div class="row panel-coldroom  light-gray-filter">
									<div class="text-center">&nbsp;DISTRIBUTION CENTERS (DC): <b><%=tmDCZoneID.size() %></b> Cold Rooms</div><br>
										<div class="col-md-6 text-center">
										DC Cold Room Alert Status<br>
											<div class="row">
												<div class="col-md-offset-1 col-md-10"><br>
													<div class="graph_container">
														<canvas id="Chart2" class="chart1" height="40" width="250"></canvas>
														<div class="row text-center">
															<div class="col-md-4"><span data-toggle="tooltip" class="badge badge-big bg-red"><%= Utils.zeroPad(Math.round(iTodayRedDC), 2) %></span><br>
															<%if(iRedDC == 0) { %>
														<b><span class="description-percentage">NA</span></b>
														<%} else if(iRedDC > 0) { %>
														<b><span class="description-percentage text-red"><i class="fa fa-caret-up"></i>&nbsp;<%=Utils.zeroPad(Math.round(iRedDC), 2) %>%</span></b>
														<%} else { %>
														<b><span class="description-percentage text-green"><i class="fa fa-caret-down"></i>&nbsp;<%=Utils.zeroPad(Math.round(Math.abs(iRedDC)), 2) %>%</span></b>
														<%} %>
														</div>
															<div class="col-md-4"><span data-toggle="tooltip" class="badge badge-big orange"><%= Utils.zeroPad(Math.round(iTodayOrangeDC), 2) %></span><br>
															<%if(iOrangeDC == 0) { %>
														<b><span class="description-percentage">NA</span></b>
														<%} else if(iOrangeDC > 0) { %>
														<b><span class="description-percentage text-red"><i class="fa fa-caret-up"></i>&nbsp;<%=Utils.zeroPad(Math.round(iOrangeDC), 2) %>%</span></b>
														<%} else { %>
														<b><span class="description-percentage text-green"><i class="fa fa-caret-down"></i>&nbsp;<%=Utils.zeroPad(Math.round(Math.abs(iOrangeDC)), 2) %>%</span></b>
														<%} %>
														</div>
															<div class="col-md-4"><span data-toggle="tooltip" class="badge badge-big bg-green"><%= Utils.zeroPad(Math.round(iTodayGreenDC), 2) %></span></div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="well well-sm white-background div-height-small text-center">
												DC Cold Room w/ Alerts<br><br>
												<div class="scroll-area">
												<%set = tmDCLocs.keySet();
												iter = set.iterator();
												stemp = "";
												sT = null;
												while(iter.hasNext()) {
													sValue = (String)iter.next();
													stemp = tmDCLocs.get(sValue);
													sT = stemp.split(";");
													if(tmDCLocs1.containsKey(sValue)) {
														stemp1 = tmDCLocs1.get(sValue);
														sT1 = stemp1.split(";");
														iSize = sT1.length;
													} else {
														iSize = 0;
													}
													
													
												%>
													<span class="pull-left"><a href="TodayColdChainHealth.jsp?LocationId=<%=sValue%>"><%=sValue %></a></span><b style="color:red" class="pull-right"><%=iSize %>/<%=sT.length %>&nbsp;&nbsp;</b><br><hr class="hr-no-padding">
													<%} %>
												</div>
											</div>
										</div>
									</div>	
							</div>
						</div>
					</div>
				</div>	
				<div class="col-md-6">
					<div class="row">
						<div class="col-md-12">
						<div class="text-center"><i>Vehicles with:&nbsp;&nbsp;<i class="fa fa-square alert-text alert-font"></i> >3 Alerts&nbsp;&nbsp;
				<i class="fa fa-square warning-text alert-font"></i>&nbsp; 1-3 Alerts&nbsp;&nbsp;
				<i class="fa fa-square success-text alert-font"></i>&nbsp; No Alerts</i>
				</div>
							<div class="panel-coldroom text-center">
								<b>&nbsp;VEHICLES</b>
							</div>
							<div class="well well-sm panel-coldroom light-gray-filter" style="height:450px">
								<div class="text-center">
									<div class="row">
										<div class="col-md-12">
											<div class="row">
												<div class="col-md-offset-2 col-md-7">
													MU <i class="fa fa-long-arrow-right"></i> DC (# Of Vehicles: <b><%=iMUDCCount %></b>)
													<div class="graph_container">
														<div>
														<canvas id="Chart3" class="chart1" height="30" width="250"></canvas>
														<div class="row text-center">
															<div class="col-md-4"><span data-toggle="tooltip" class="badge badge-big bg-red"><%= Utils.zeroPad(Math.round(iTodayRedMUDC), 2) %></span><br>
															<%if(iRedMUDC == 0) { %>
														<b><span class="description-percentage">NA</span></b>
														<%} else if(iRedMUDC > 0) { %>
														<b><span class="description-percentage text-red"><i class="fa fa-caret-up"></i>&nbsp;<%=Utils.zeroPad(Math.round(iRedMUDC), 2) %>%</span></b>
														<%} else { %>
														<b><span class="description-percentage text-green"><i class="fa fa-caret-down"></i>&nbsp;<%=Utils.zeroPad(Math.round(Math.abs(iRedMUDC)), 2) %>%</span></b>
														<%} %>
														</div>
															<div class="col-md-4"><span data-toggle="tooltip" class="badge badge-big orange"><%= Utils.zeroPad(Math.round(iTodayOrangeMUDC), 2) %></span><br>
															<%if(iOrangeMUDC == 0) { %>
														<b><span class="description-percentage">NA</span></b>
														<%} else if(iOrangeMUDC > 0) { %>
														<b><span class="description-percentage text-red"><i class="fa fa-caret-up"></i>&nbsp;<%=Utils.zeroPad(Math.round(iOrangeMUDC), 2) %>%</span></b>
														<%} else { %>
														<b><span class="description-percentage text-green"><i class="fa fa-caret-down"></i>&nbsp;<%=Utils.zeroPad(Math.round(Math.abs(iOrangeMUDC)), 2) %>%</span></b>
														<%} %>
														</div>
															<div class="col-md-4"><span data-toggle="tooltip" class="badge badge-big bg-green"><%= Utils.zeroPad(Math.round(iTodayGreenMUDC), 2) %></span></div>
														</div>
														</div>
													</div>
												</div>
											</div>
											<br>
											<div class="row">
												<div class="col-md-offset-2 col-md-7">
													DC <i class="fa fa-long-arrow-right"></i> DC (# Of Vehicles: <b><%=iDCDCCount %></b>)
													<div class="graph_container">
													<div>
														<canvas id="Chart4" class="chart1" height="30" width="250"></canvas>
														<div class="row text-center">
															<div class="col-md-4"><span data-toggle="tooltip" class="badge badge-big bg-red"><%= Utils.zeroPad(Math.round(iTodayRedDCDC), 2) %></span><br>
															<%if(iRedDCDC == 0) { %>
														<b><span class="description-percentage">NA</span></b>
														<%} else if(iRedDCDC > 0) { %>
														<b><span class="description-percentage text-red"><i class="fa fa-caret-up"></i>&nbsp;<%=Utils.zeroPad(Math.round(iRedDCDC), 2) %>%</span></b>
														<%} else { %>
														<b><span class="description-percentage text-green"><i class="fa fa-caret-down"></i>&nbsp;<%=Utils.zeroPad(Math.round(Math.abs(iRedDCDC)), 2) %>%</span></b>
														<%} %>
														</div>
															<div class="col-md-4"><span data-toggle="tooltip" class="badge badge-big orange"><%= Utils.zeroPad(Math.round(iTodayOrangeDCDC), 2) %></span><br>
															<%if(iOrangeDCDC == 0) { %>
														<b><span class="description-percentage">NA</span></b>
														<%} else if(iOrangeDCDC > 0) { %>
														<b><span class="description-percentage text-red"><i class="fa fa-caret-up"></i>&nbsp;<%=Utils.zeroPad(Math.round(iOrangeDCDC), 2) %>%</span></b>
														<%} else { %>
														<b><span class="description-percentage text-green"><i class="fa fa-caret-down"></i>&nbsp;<%=Utils.zeroPad(Math.round(Math.abs(iOrangeDCDC)), 2) %>%</span></b>
														<%} %>
														</div>
															<div class="col-md-4"><span data-toggle="tooltip" class="badge badge-big bg-green"><%= Utils.zeroPad(Math.round(iTodayGreenDCDC), 2) %></span></div>
														</div>
														</div>
													</div>
												</div>
											</div>
											<br>
											<div class="row">
												<div class="col-md-offset-2 col-md-7">
													DC <i class="fa fa-long-arrow-right"></i> End Point (# Of Vehicles: <b><%=iDCEPCount %></b>)
													<div class="graph_container">
														<canvas id="Chart5" class="chart1" height="30" width="250"></canvas>
														<div class="row text-center">
															<div class="col-md-4"><span data-toggle="tooltip" class="badge badge-big bg-red"><%= Utils.zeroPad(Math.round(iTodayRedDCEP), 2) %></span><br>
															<%if(iRedDCEP == 0) { %>
														<b><span class="description-percentage">NA</span></b>
														<%} else if(iRedDCEP > 0) { %>
														<b><span class="description-percentage text-red"><i class="fa fa-caret-up"></i>&nbsp;<%=Utils.zeroPad(Math.round(iRedDCEP), 2) %>%</span></b>
														<%} else { %>
														<b><span class="description-percentage text-green"><i class="fa fa-caret-down"></i>&nbsp;<%=Utils.zeroPad(Math.round(Math.abs(iRedDCEP)), 2) %>%</span></b>
														<%} %>
														</div>
															<div class="col-md-4"><span data-toggle="tooltip" class="badge badge-big orange"><%= Utils.zeroPad(Math.round(iTodayOrangeDCEP), 2) %></span><br>
															<%if(iOrangeDCEP == 0) { %>
														<b><span class="description-percentage">NA</span></b>
														<%} else if(iOrangeDCEP > 0) { %>
														<b><span class="description-percentage text-red"><i class="fa fa-caret-up"></i>&nbsp;<%=Utils.zeroPad(Math.round(iOrangeDCEP), 2) %>%</span></b>
														<%} else { %>
														<b><span class="description-percentage text-green"><i class="fa fa-caret-down"></i>&nbsp;<%=Utils.zeroPad(Math.round(Math.abs(iOrangeDCEP)), 2) %>%</span></b>
														<%} %>
														</div>
															<div class="col-md-4"><span data-toggle="tooltip" class="badge badge-big bg-green"><%= Utils.zeroPad(Math.round(iTodayGreenDCEP), 2) %></span></div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 text-center">
				<div class="well well-sm white-background">
				<div class="text-center panel-coldroom">
					<b>TOTAL NUMBER OF ALERTS TODAY</b>
					</div><br>
					<div class="row">
						<div class="col-md-6">
							TOTAL COLD ROOM ALERTS: <B class="font-big" style="color:black"><%= Utils.zeroPad(Math.round((iTemperatureCRCount+iHumidityCRCount+iDoorOpenCRCount)), 2) %></B>
							<div class="well well-sm white-background">
					<div class="row">
								<div class="col-md-3">
								TEMPERATURE
								<div class="row"><br>
									<div class="col-md-offset-1 col-md-3">
									<img src="../../dist/img/temperature.png">
									</div>
									<div class="col-md-offset-1 col-md-7 text-left">
								<b class="font-new"><%= Utils.zeroPad(Math.round(iTemperatureCRCount), 2) %></b>
									</div>
								</div>
								<%if(iDiffTemperatureCRCount == 0) { %>
									<b><span class="description-percentage">NA</span></b>
								<%} else if(iDiffTemperatureCRCount > 0) { %>
									<b><span class="description-percentage text-red"><i class="fa fa-caret-up"></i>&nbsp;<%=Utils.zeroPad(Math.round(iDiffTemperatureCRCount), 2) %>%</span></b>
								<%} else { %>
									<b><span class="description-percentage text-green"><i class="fa fa-caret-down"></i>&nbsp;<%=Utils.zeroPad(Math.round(Math.abs(iDiffTemperatureCRCount)), 2) %>%</span></b>
								<%} %>
								</div>
								<div class="col-md-3">
								HUMIDITY
								<div class="row"><br>
									<div class="col-md-offset-1 col-md-3">
									<img src="../../dist/img/humidity.png">
									</div>
									<div class="col-md-offset-1 col-md-7 text-left">
								<b class="font-new text-left"><%=Utils.zeroPad(Math.round(iHumidityCRCount), 2)%></b>
									</div>
								</div>
								<%if(iDiffHumidityCRCount == 0) { %>
									<b><span class="description-percentage">NA</span></b>
								<%} else if(iDiffHumidityCRCount > 0) { %>
									<b><span class="description-percentage text-red"><i class="fa fa-caret-up"></i>&nbsp;<%=Utils.zeroPad(Math.round(iDiffHumidityCRCount), 2) %>%</span></b>
								<%} else { %>
									<b><span class="description-percentage text-green"><i class="fa fa-caret-down"></i>&nbsp;<%=Utils.zeroPad(Math.round(Math.abs(iDiffHumidityCRCount)), 2) %>%</span></b>
								<%} %>
								</div>
								<div class="col-md-3">
								DOOR OPEN
								<div class="row"><br>
									<div class="col-md-offset-1 col-md-3">
									<img src="../../dist/img/dooropen.png">
									</div>
									<div class="col-md-offset-1 col-md-7 text-left">
								<b class="font-new text-left">NA</b>
									</div>
								</div>
								
								</div>
								<div class="col-md-3">
								POWER
								<div class="row">
									<br>
									<div class="col-md-12 text-center">
								<b class="font-new text-left">NA</b>
									</div>
								</div>
								</div>
								</div>
							</div>
						</div>
						<div class="col-md-6 panel-coldroom light-gray-filter">
							TOTAL VEHICLE ALERTS: <B><%= Utils.zeroPad(Math.round((iTemperatureVehCount+iHumidityVehCount+iDoorOpenVehCount+iStoppageVehCount)), 2) %></B>
							<div class="well well-sm  panel-coldroom  light-gray-filter">
								<div class="row">
								<div class="col-md-3">
								TEMPERATURE
								<div class="row"><br>
									<div class="col-md-offset-1 col-md-3">
									<img src="../../dist/img/temperature.png">
									</div>
									<div class="col-md-offset-1 col-md-7 text-left">
								<b class="font-new"><%=Utils.zeroPad(Math.round(iTemperatureVehCount), 2) %></b>
									</div>
								</div>
								<%if(iDiffTemperatureVehCount == 0) { %>
									<b><span class="description-percentage">NA</span></b>
								<%} else if(iDiffTemperatureVehCount > 0) { %>
									<b><span class="description-percentage text-red"><i class="fa fa-caret-up"></i>&nbsp;<%=Utils.zeroPad(Math.round(iDiffTemperatureVehCount), 2) %>%</span></b>
								<%} else { %>
									<b><span class="description-percentage text-green"><i class="fa fa-caret-down"></i>&nbsp;<%=Utils.zeroPad(Math.round(Math.abs(iDiffTemperatureVehCount)), 2) %>%</span></b>
								<%} %>
								</div>
								<div class="col-md-3">
								HUMIDITY
								<div class="row"><br>
									<div class="col-md-offset-1 col-md-3">
									<img src="../../dist/img/humidity.png">
									</div>
									<div class="col-md-offset-1 col-md-7 text-left">
								<b class="font-new text-left"><%=Utils.zeroPad(Math.round(iHumidityVehCount), 2) %></b>
									</div>
								</div>
								<%if(iDiffHumidityVehCount == 0) { %>
									<b><span class="description-percentage">NA</span></b>
								<%} else if(iDiffHumidityVehCount > 0) { %>
									<b><span class="description-percentage text-red"><i class="fa fa-caret-up"></i>&nbsp;<%=Utils.zeroPad(Math.round(iDiffHumidityVehCount), 2) %>%</span></b>
								<%} else { %>
									<b><span class="description-percentage text-green"><i class="fa fa-caret-down"></i>&nbsp;<%=Utils.zeroPad(Math.round(Math.abs(iDiffHumidityVehCount)), 2) %>%</span></b>
								<%} %>
								</div>
							<div class="col-md-3">
								VEHICLE DOOR OPEN
								<%iDoorOpenVehCount = 0;
								iDiffDoorOpenVehCount = 0;%>
								<div class="row">
									<div class="col-md-3">
									<img src="../../dist/img/vehdooropen.png">
									</div>
									<div class="col-md-offset-2 col-md-7 text-left">
								<b class="font-new text-left"><%=Utils.zeroPad(Math.round(iDoorOpenVehCount), 2)%></b>
									</div>
									
								</div>
								<%if(iDiffDoorOpenVehCount == 0) { %>
									<b><span class="description-percentage">NA</span></b>
								<%} else if(iDiffDoorOpenVehCount > 0) { %>
									<b><span class="description-percentage text-red"><i class="fa fa-caret-up"></i>&nbsp;<%=Utils.zeroPad(Math.round(iDiffDoorOpenVehCount), 2) %>%</span></b>
								<%} else { %>
									<b><span class="description-percentage text-green"><i class="fa fa-caret-down"></i>&nbsp;<%=Utils.zeroPad(Math.round(Math.abs(iDiffDoorOpenVehCount)), 2) %>%</span></b>
								<%} %>
								</div>
								<div class="col-md-3">
								VEHICLE STOPPAGE
								<div class="row">
									<div class="col-md-3">
									<img src="../../dist/img/vehiclestop.png">
									</div>
									<div class="col-md-offset-3 col-md-6 text-left">
								<b class="font-new text-left"><%=Utils.zeroPad(Math.round(iStoppageVehCount), 2) %></b>
									</div>
								
								</div>
								<%if(iDiffStoppageVehCount == 0) { %>
									<b><span class="description-percentage">NA</span></b>
								<%} else if(iDiffStoppageVehCount > 0) { %>
									<b><span class="description-percentage text-red"><i class="fa fa-caret-up"></i>&nbsp;<%=Utils.zeroPad(Math.round(iDiffStoppageVehCount), 2) %>%</span></b>
								<%} else { %>
									<b><span class="description-percentage text-green"><i class="fa fa-caret-down"></i>&nbsp;<%=Utils.zeroPad(Math.round(Math.abs(iDiffStoppageVehCount)), 2) %>%</span></b>
								<%} %>
								</div>
								</div>
						</div>
					</div>
					
							
						</div>
					</div>
				</div>
				</div>
			<div class="row">
				<div class="col-md-12 text-center">
					<div class="well well-sm white-background" style="height:125px">
					
					<div class="text-center panel-coldroom">
						<div class="row">
						<div class="col-md-offset-4 col-md-4">
						<b>ALERTS RESOLUTION</b>
						</div>
						<div class="col-md-4">
						<span class="pull-right"><i>
				<i class="fa fa-square grey-text-color alert-font"></i>:&nbsp;% Alerts Unresolved&nbsp;&nbsp;
				<i class="fa fa-square info-text-color alert-font"></i>:&nbsp;% Alerts Resolved</i>
				</span>
						</div>
						</div>
					</div>
					<br>
						<div class="row">
							<div class="col-md-2">
								TOTAL MU ALERTS: <b>6</b><br>
								<div class="graph_container text-center">
									<canvas id="Chart6" class="chart1" width="100"></canvas>
								</div>
							</div>
							<div class="col-md-2">
								TOTAL DC ALERTS: <b>6</b><br>
								<div class="graph_container">
									<canvas id="Chart7" class="chart1" width="100"></canvas>
								</div>
							</div>
							<div class="col-md-2">
								&nbsp;
							</div>
							<div class="col-md-2 col-no-padding panel-coldroom  light-gray-filter">
								TOTAL MU <i class="fa fa-long-arrow-right"></i> DC ALERTS: <b>7</b><br>
								<div class="graph_container" style="position: relative;">
									<canvas id="Chart8" class="chart1" width="100"></canvas>
								</div>
							</div>
							<div class="col-md-2 col-no-padding panel-coldroom  light-gray-filter">
								TOTAL DC <i class="fa fa-long-arrow-right"></i> DC ALERTS: <b>2</b><br>
								<div class="graph_container" style="position: relative;">
									<canvas id="Chart9" class="chart1" width="100"></canvas>
								</div>
							</div>
							<div class="col-md-2 panel-coldroom  light-gray-filter">
								TOTAL DC <i class="fa fa-long-arrow-right"></i> EP ALERTS: <b>6</b><br>
								<div class="graph_container" style="position: relative;">
									<canvas id="Chart10" class="chart1" width="100"></canvas>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			
				 <div class="row">
				<div class="col-md-12 text-center">
				<div class="well well-sm panel-coldroom light-gray-filter">
				<div class="text-center panel-coldroom">
					<b>COLD CHAIN EFFICIENCY SUMMARY</b>
					</div><br>
					<div class="row">
						<div class="col-md-6 text-center">
						<div class="well well-sm  panel-coldroom light-gray-filter">
							<div class="row">
								<div class="col-md-3"><br><br>
								AVG ENERGY EFFICIENCY(%)
								</div>
								<div class="col-md-4">
								DCs
								<div class="row">
									<div class="col-md-12">
								<b class="font-new">03</b>
									</div>
								</div>
								<b><span class="description-percentage text-red"><i class="fa fa-caret-up"></i>&nbsp;12%</span></b>
								</div>
								<div class="col-md-4">
								MUs
								<div class="row">
									<div class="col-md-12">
								<b class="font-new text-left">01</b>
									</div>
								</div>
								<b><span class="description-percentage text-green"><i class="fa fa-caret-down"></i>&nbsp;10%</span></b>
								</div>
							</div>	
							<hr>
							<div class="row">
								<div class="col-md-3"><br>
								COLD ROOM UTILIZATION(%)
								</div>
								<div class="col-md-4">
								<div class="row">
									<div class="col-md-12">
								<b class="font-new">07</b>
									</div>
								</div>
								<b><span class="description-percentage text-green"><i class="fa fa-caret-down"></i>&nbsp;25%</span></b>
								</div>
								<div class="col-md-4">
								<div class="row">
									<div class="col-md-12">
								<b class="font-new text-left">02</b>
									</div>
								</div>
								<b><span class="description-percentage text-green"><i class="fa fa-caret-down"></i>&nbsp;02%</span></b>
								</div>
							</div>	
							</div>	
						</div>
					<div class="col-md-6 text-center">
				<div class="well well-sm  panel-coldroom light-gray-filter">
						
							<div class="row">
								<div class="col-md-3"><br><br>
								% ON TIME
								</div>
								<div class="col-md-3">
								MU <i class="fa fa-long-arrow-right"></i> DC
								<div class="row">
									<div class="col-md-12">
								<b class="font-new">85%</b>
									</div>
								</div>
								<b><span class="description-percentage text-red"><i class="fa fa-caret-down"></i>&nbsp;05%</span></b>
								</div>
								<div class="col-md-3">
								DC <i class="fa fa-long-arrow-right"></i> DC
								<div class="row">
									<div class="col-md-12">
								<b class="font-new text-left">83%</b>
									</div>
								</div>
								<b><span class="description-percentage text-green"><i class="fa fa-caret-up"></i>&nbsp;10%</span></b>
								</div>
								<div class="col-md-3">
								DC <i class="fa fa-long-arrow-right"></i> EP
								<div class="row">
									<div class="col-md-12">
								<b class="font-new text-left">82%</b>
									</div>
								</div>
								<b><span class="description-percentage text-green"><i class="fa fa-caret-up"></i>&nbsp;08%</span></b>
								</div>
							</div>	
						</div>
					</div>
					</div>
					</div>
					</div>
				</div>
				</section>
				</div>
		<!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0
			</div>
			<strong>Copyright &copy; 2017 <a href="http://tagbox.in">TagBox</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Control Sidebar -->
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->
<!-- ChartJS 1.0.1 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>

	<!-- Bootstrap 3.3.6 -->
	<script src="../../bootstrap/js/bootstrap.min.js"></script>
	<!-- ChartJS 1.0.1 -->
	<!-- AdminLTE App -->
	<script src="../../dist/js/app.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="../../dist/js/demo.js"></script>
<script>

Chart.defaults.global.legend.display = false;

var barOptions_stacked = {
	    tooltips: {
	        enabled: false
	    },
	    hover :{
	        animationDuration:0
	    },
	    scales: {
	        xAxes: [{
	        	display:false,
	            ticks: {
	                beginAtZero:true,
	                fontFamily: "'Open Sans Bold', sans-serif",
	                fontSize:11
	            },
	            
	            stacked: true
	        }],
	        yAxes: [{
	        	display:false,
	            ticks: {
	                fontFamily: "'Open Sans Bold', sans-serif",
	                fontSize:11
	            },
	            stacked: true
	        }]
	    },
	    legend:{
	        display:false
	    },
	    pointLabelFontFamily : "Quadon Extra Bold",
	    scaleFontFamily : "Quadon Extra Bold"
	};
	
var horizontalbarOptions_stacked = {
		responsive: true,
		maintainAspectRatio: false,
		tooltips: {
	        enabled: false
	    },
	    hover :{
	        animationDuration:0
	    },
	    scales: {
	        xAxes: [{
	        	display: false,
	            ticks: {
	                beginAtZero:true,
	                fontFamily: "'Open Sans Bold', sans-serif",
	                fontSize:11
	            },
	            scaleLabel:{
	                display:false
	            },
	            gridLines: {
	            }, 
	            stacked: true,
	            barPercentage: 0.5,
		        categoryPercentage: 0.6
	        }],
	        yAxes: [{
	        	display: false,
	            gridLines: {
	                display:false,
	                color: "#fff",
	                zeroLineColor: "#fff",
	                zeroLineWidth: 0
	            },
	            scaleLabel:{
	                display:false
	            },
	            ticks: {
	                fontFamily: "'Open Sans Bold', sans-serif",
	                fontSize:11
	            },
	            stacked: true
	        }]
	    },
	    legend:{
	        display:false
	    },
	    
	    animation: {
	        onComplete: function () {
	            var chartInstance = this.chart;
	            var ctx = chartInstance.ctx;
	            ctx.textAlign = "center";
	            ctx.font = "20px Open Sans";
	            ctx.fillStyle = "#fff";

	            Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
	                var meta = chartInstance.controller.getDatasetMeta(i);
	                Chart.helpers.each(meta.data.forEach(function (bar, index) {
	                    data = dataset.data[index];
	                   // alert(index + ":" + data + ":" +  bar._model.x + ":" + bar._model.y);
	                        ctx.fillText(data, bar._model.x-10, bar._model.y);
	                    
	                }),this)
	            }),this);
				
			}
	    },
	    pointLabelFontFamily : "Quadon Extra Bold",
	    scaleFontFamily : "Quadon Extra Bold"
	};
	
	var ctx = document.getElementById("Chart1");
	var myChart = new Chart(ctx, {
	    type: 'horizontalBar',
	    data: {
	        labels: [""],
	        
	        datasets: [{
	        	label: ">3 Alerts",
	            data: [<%=iTodayRedMan%>],
	            backgroundColor: "rgba(212,19,20,1)"
	        },{
	        	label: "<3 Alerts",
	            data: [<%=iTodayOrangeMan%>],
	            backgroundColor: "rgba(255,169,34,1)"
	        },{
	        	label: "No Alerts",
	            data: [<%=iTodayGreenMan%>],
	            backgroundColor: "rgba(49,150,8,1)"
	        }]
	    },

	    options: barOptions_stacked
	});
	var ctx1 = document.getElementById("Chart2");
	var myChart2 = new Chart(ctx1, {
	    type: 'horizontalBar',
	    data: {
	        labels: [""],
	        
	        datasets: [{
	        	label: ">3 Alerts",
	            data: [<%=iTodayRedDC%>],
	            backgroundColor: "rgba(212,19,20,1)"
	        },{
	        	label: "<3 Alerts",
	            data: [<%=iTodayOrangeDC%>],
	            backgroundColor: "rgba(255,169,34,1)"
	        },{
	        	label: "No Alerts",
	            data: [<%=iTodayGreenDC%>],
	            backgroundColor: "rgba(49,150,8,1)"
	        }]
	    },

	    options: barOptions_stacked
	});
	var ctx2 = document.getElementById("Chart3");
	var myChart2 = new Chart(ctx2, {
	    type: 'horizontalBar',
	    data: {
	        labels: [""],
	        
	        datasets: [{
	        	label: ">3 Alerts",
	            data: [<%=iTodayRedMUDC %>],
	            backgroundColor: "rgba(212,19,20,1)"
	        },{
	        	label: "<3 Alerts",
	            data: [<%=iTodayOrangeMUDC %>],
	            backgroundColor: "rgba(255,169,34,1)"
	        },{
	        	label: "No Alerts",
	            data: [<%=iTodayGreenMUDC %>],
	            backgroundColor: "rgba(49,150,8,1)"
	        }]
	    },

	    options: barOptions_stacked
	});
	var ctx3 = document.getElementById("Chart4");
	var myChart3 = new Chart(ctx3, {
	    type: 'horizontalBar',
	    data: {
	        labels: [""],
	        
	        datasets: [{
	        	label: ">3 Alerts",
	            data: [<%=iTodayRedDCDC %>],
	            backgroundColor: "rgba(212,19,20,1)"
	        },{
	        	label: "<3 Alerts",
	            data: [<%=iTodayOrangeDCDC %>],
	            backgroundColor: "rgba(255,169,34,1)"
	        },{
	        	label: "No Alerts",
	            data: [<%=iTodayGreenDCDC %>],
	            backgroundColor: "rgba(49,150,8,1)"
	        }]
	    },

	    options: barOptions_stacked
	});
	var ctx4 = document.getElementById("Chart5");
	var myChart4 = new Chart(ctx4, {
	    type: 'horizontalBar',
	    data: {
	        labels: [""],
	        
	        datasets: [{
	        	label: ">3 Alerts",
	            data: [<%=iTodayRedDCEP %>],
	            backgroundColor: "rgba(212,19,20,1)"
	        },{
	        	label: "<3 Alerts",
	            data: [<%=iTodayOrangeDCEP %>],
	            backgroundColor: "rgba(255,169,34,1)"
	        },{
	        	label: "No Alerts",
	            data: [<%=iTodayGreenDCEP %>],
	            backgroundColor: "rgba(49,150,8,1)"
	        }]
	    },

	    options: barOptions_stacked
	});
	
	
	
	// For a pie chart
	var ctx5 = document.getElementById("Chart6");
	var myPieChart = new Chart(ctx5,{
	    type: 'horizontalBar',
	    data: {
	        labels: [""],
	        
	        datasets: [{
	        	label: "Resolved",
	            data: [4],
	            backgroundColor: "rgb(66, 133, 244)"
	        },{
	        	label: "Unresolved",
	            data: [2],
	            backgroundColor: "rgba(66, 66, 66, 0.5)"
	        }]
	    },
	    options: horizontalbarOptions_stacked
	});
	
	// For a pie chart
	var ctx6 = document.getElementById("Chart7");
	var myPieChart1 = new Chart(ctx6,{
	    type: 'horizontalBar',
	    data: {
	        labels: [""],
	        
	        datasets: [{
	        	label: "Resolved",
	            data: [3],
	            backgroundColor: "rgb(66, 133, 244)"
	        },{
	        	label: "Unresolved",
	            data: [3],
	            backgroundColor: "rgba(66, 66, 66, 0.5)"
	        }]
	    },
	    options: horizontalbarOptions_stacked
	});
	
	// For a pie chart
	var ctx7 = document.getElementById("Chart8");
	var myPieChart2 = new Chart(ctx7,{
	    type: 'horizontalBar',
	    data: {
	        labels: [""],
	        
	        datasets: [{
	        	label: "Resolved",
	            data: [5],
	            backgroundColor: "rgb(66, 133, 244)"
	        },{
	        	label: "Unresolved",
	            data: [2],
	            backgroundColor: "rgba(66, 66, 66, 0.5)"
	        }]
	    },
	    options: horizontalbarOptions_stacked
	});
	var ctx8 = document.getElementById("Chart9");
	var myPieChart3 = new Chart(ctx8,{
	    type: 'horizontalBar',
	    data: {
	        labels: [""],
	        
	        datasets: [{
	        	label: "Resolved",
	            data: [1],
	            backgroundColor: "rgb(66, 133, 244)"
	        },{
	        	label: "Unresolved",
	            data: [1],
	            backgroundColor: "rgba(66, 66, 66, 0.5)"
	        }]
	    },
	    options: horizontalbarOptions_stacked
	});
	var ctx9 = document.getElementById("Chart10");
	var myPieChart4 = new Chart(ctx9,{
	    type: 'horizontalBar',
	    data: {
	        labels: [""],
	        
	        datasets: [{
	        	label: "Resolved",
	            data: [2],
	            backgroundColor: "rgb(66, 133, 244)"
	        },{
	        	label: "Unresolved",
	            data: [4],
	            backgroundColor: "rgba(66, 66, 66, 0.5)"
	        }]
	    },
	    options: horizontalbarOptions_stacked
	});
	
	</script>


	</body>
</html>
