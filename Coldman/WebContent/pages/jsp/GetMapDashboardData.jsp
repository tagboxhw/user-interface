
<%@ page import="com.coldman.dao.TransitDao"%>
<%@ page import="com.coldman.dao.AlertsWorkflowDao"%>
<%@ page import="com.coldman.bean.TransitVehicleMapBean"%>
<%@ page import="com.coldman.bean.AlertWorkflowBean"%>
<%@ page import="com.coldman.util.Constants"%>
<%@ page import="java.util.TreeMap"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.util.Collection"%>
<%@ page import="java.util.LinkedHashMap"%>
<%@ page import="java.util.Vector"%>
<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>

<!-- <?xml version="1.0" encoding="ISO-8859-1"?> -->

<%!
    static final Logger logger = LoggerFactory.getLogger("GetMapDashboardData");
	TransitDao transitDao = new TransitDao();
    AlertsWorkflowDao alDao = new AlertsWorkflowDao();
	String buf = "";
	String sString = "";
	String sErrorMessage = null;
	String sId = null;
	String sRowId = null;
	TransitVehicleMapBean tv = new TransitVehicleMapBean();
	TreeMap tAll = new TreeMap();
	Collection tLocalRun = new Vector();
	Collection tShortHaul = new Vector();
	Collection tLongHaul = new Vector();
	LinkedHashMap lhm;
    Iterator iter = null;
    String sAlertType = "";
    String sTime = "";
    String sKey = "";
	%>
<% 
	try {
		sAlertType = request.getParameter("alert_type");
		if(sAlertType == null) sAlertType = "ALL";
		sTime = request.getParameter("time");
		if(sTime == null || sTime.equals("")){
			sTime = "60";
		}
		sString = request.getParameter("type");
		
		if(sString.equals("Dashboard")){
			buf="";
			//response.setContentType("text/xml");
			//buf += ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			
			tAll = alDao.getLatLong(sAlertType, sTime);
			tLocalRun = (Collection) tAll.get(Constants.LOCAL_RUN);
			tShortHaul = (Collection) tAll.get(Constants.SHORT_HAUL);
			tLongHaul = (Collection) tAll.get(Constants.LONG_HAUL);
			int iCount = 0;
			//logger.info("<rows total_count='100' pos='0'>");
			buf = "<rows>";
			iter = tLongHaul.iterator();
			while(iter.hasNext()) {
				tv = (TransitVehicleMapBean) iter.next();
				buf += ("<row id=\"" + iCount + "\">");
				buf += ("<RouteType>" + tv.getRoute_Type() + "</RouteType>");
				buf += ("<VehicleId>" + tv.getVehicle_ID() + "</VehicleId>");
				buf += ("<Status>" + tv.getVehicle_Status() + "</Status>");
				buf += ("<Source>" + tv.getSource() + "</Source>");
				buf += ("<Destination>" + tv.getDestination() + "</Destination>");
				buf += ("</row>");
				iCount++;
			}
			iter = tShortHaul.iterator();
			while(iter.hasNext()) {
				tv = (TransitVehicleMapBean) iter.next();
				buf += ("<row id=\"" + iCount + "\">");
				buf += ("<RouteType>" + tv.getRoute_Type() + "</RouteType>");
				buf += ("<VehicleId>" + tv.getVehicle_ID() + "</VehicleId>");
				buf += ("<Status>" + tv.getVehicle_Status() + "</Status>");
				buf += ("<Source>" + tv.getSource() + "</Source>");
				buf += ("<Destination>" + tv.getDestination() + "</Destination>");
				buf += ("</row>");
				iCount++;
			}
			iter = tLocalRun.iterator();
			while(iter.hasNext()) {
				tv = (TransitVehicleMapBean) iter.next();
				buf += ("<row id=\"" + iCount + "\">");
				buf += ("<RouteType>" + tv.getRoute_Type() + "</RouteType>");
				buf += ("<VehicleId>" + tv.getVehicle_ID() + "</VehicleId>");
				buf += ("<Status>" + tv.getVehicle_Status() + "</Status>");
				buf += ("<Source>" + tv.getSource() + "</Source>");
				buf += ("<Destination>" + tv.getDestination() + "</Destination>");
				buf += ("</row>");
				iCount++;
			}
			buf += "</rows>";
			buf = buf.trim();
			//logger.info(buf);
			out.clear();
			out.println(buf);
		} else if(sString.equals("LocalRunDetails")){
			try {
			String sRouteType = request.getParameter("route");
			String sVehicleId = request.getParameter("vehicleId");
			if(sRouteType == null || sRouteType.equals("")){
				sRouteType = Constants.LONG_HAUL;
			}
			buf="";
			buf = "<rows>";
			//response.setContentType("text/xml");
			//buf += ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			logger.info(sRouteType);
// 			tAll = alDao.getLatLong(sAlertType, sTime);
// 			tLocalRun = (Collection) tAll.get(sRouteType);
 			int iCount = 0;
// 			//logger.info("<rows total_count='100' pos='0'>");
// 			buf = "<rows>";
// 			iter = tLocalRun.iterator();
// 			while(iter.hasNext()) {
// 				tv = (TransitVehicleMapBean) iter.next();
// 				buf += ("<row id=\"" + iCount + "\">");
// 				buf += ("<RouteType>" + tv.getRoute_Type() + "</RouteType>");
// 				buf += ("<VehicleId>" + tv.getVehicle_ID() + "</VehicleId>");
// 				buf += ("<Status>" + tv.getVehicle_Status() + "</Status>");
// 				buf += ("<Source>" + tv.getSource() + "</Source>");
// 				buf += ("<Destination>" + tv.getDestination() + "</Destination>");
// 				buf += ("</row>");
// 				iCount++;
// 			}
			lhm = transitDao.getMapDetailsVehicleId(sVehicleId, sAlertType, sTime);
			iter = lhm.keySet().iterator();
			AlertWorkflowBean ab = new AlertWorkflowBean();
			while(iter.hasNext()){
				sKey = (String) iter.next();
				ab = (AlertWorkflowBean) lhm.get(sKey);
				buf += ("<row id=\"" + iCount + "\">");
				if(ab.getRoute_Type() == null || ab.getRoute_Type().equals("null")){
					buf += ("<RouteType></RouteType>");
				} else {
					buf += ("<RouteType>" + ab.getRoute_Type() + "</RouteType>");
				}
				if(ab.getAlert_Location() == null || ab.getAlert_Location().equals("null")){
					buf += ("<VehicleId></VehicleId>");
				} else {
					buf += ("<VehicleId>" + ab.getAlert_Location() + "</VehicleId>");
				}
				if(ab.getAlert_Type() == null || ab.getAlert_Type().equals("null")){
					buf += ("<AlertType></AlertType>");
				} else {
					buf += ("<AlertType>" + ab.getAlert_Type() + "</AlertType>");
				}
				if(ab.getCurrent_Location_Lat() == null || ab.getCurrent_Location_Lat().equals("null")){
					buf += ("<Source></Source>");
				} else {
					buf += ("<Source>" + ab.getCurrent_Location_Lat() + "</Source>");
				}
				if(ab.getCurrent_Location_Long() == null || ab.getCurrent_Location_Long().equals("null")){
					buf += ("<Destination></Destination>");
				} else {
					buf += ("<Destination>" + ab.getCurrent_Location_Long() + "</Destination>");
				}
				if(ab.getAlert_Timestamp_From() == null || ab.getAlert_Timestamp_From().equals("null")){
					buf += ("<TimestampFrom></TimestampFrom>");
				} else {
					buf += ("<TimestampFrom>" + ab.getAlert_Timestamp_From() + "</TimestampFrom>");
				}
				if(ab.getAlert_Timestamp_To() == null || ab.getAlert_Timestamp_To().equals("null")){
					buf += ("<TimestampTo></TimestampTo>");
				} else {
					buf += ("<TimestampTo>" + ab.getAlert_Timestamp_To() + "</TimestampTo>");
				}
				if(ab.getAlert_Duration() == null || ab.getAlert_Duration().equals("null")){
					buf += ("<Duration></Duration>");
				} else {
					buf += ("<Duration>" + ab.getAlert_Duration() + "</Duration>");
				}
				if(ab.getCurrent_Location_Name() == null || ab.getCurrent_Location_Name().equals("null")){
					buf += ("<CurrentLocName></CurrentLocName>");
				} else {
					buf += ("<CurrentLocName>" + ab.getCurrent_Location_Name() + "</CurrentLocName>");
				}
				if(ab.getBreach_Type() == null || ab.getBreach_Type().equals("null")){
					buf += ("<Breach></Breach>");
				} else {
					buf += ("<Breach>" + ab.getBreach_Type() + "</Breach>");
				}
				buf += ("</row>");
				iCount++;
			}
			buf += "</rows>";
			buf = buf.trim();
			//logger.info(buf);
			out.clear();
			out.println(buf);
			} catch(Exception e) {
				e.printStackTrace();
			}
		}  else if(sString.equals("AnasuyaRun")){
			try {
			String sRouteType = request.getParameter("route");
			String sVehicleId = request.getParameter("vehicleId");
			if(sRouteType == null || sRouteType.equals("")){
				sRouteType = Constants.LONG_HAUL;
			}
			buf="";
			//response.setContentType("text/xml");
			//buf += ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			logger.info(sRouteType);
			tAll = alDao.getLatLongForAnasuya(sAlertType, sTime);
			tLocalRun = (Collection) tAll.get(Constants.LONG_HAUL);
			int iCount = 0;
			//logger.info("<rows total_count='100' pos='0'>");
			buf = "<rows>";
			iter = tLocalRun.iterator();
			while(iter.hasNext()) {
				tv = (TransitVehicleMapBean) iter.next();
				buf += ("<row id=\"" + iCount + "\">");
				buf += ("<Source>" + tv.getSource() + "</Source>");
				buf += ("<Destination>" + tv.getDestination() + "</Destination>");
				buf += ("</row>");
				iCount++;
			}
			
			buf += "</rows>";
			buf = buf.trim();
			//logger.info(buf);
			out.clear();
			out.println(buf);
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	} catch (Exception e) {
		logger.error("Exception: " + e.getLocalizedMessage());
		e.printStackTrace();
	}
			%>