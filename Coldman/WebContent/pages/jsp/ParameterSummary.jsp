<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>
<%@ page import="com.coldman.dao.ParameterSummaryDao"%>
<%@ page import="com.coldman.bean.ParameterSummaryBean"%>
<%@ page import="com.coldman.util.Constants"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Set"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.util.Collection"%>
<%@ page import="com.coldman.util.Utils"%>
<%@ page import="com.coldman.dao.ClientLocationMapDao"%>
<%@ page import="java.util.TreeMap"%>
<%@ page import="com.coldman.bean.AllLocationDataBean"%>
<%@ page import="com.coldman.dao.HumidityDao"%>
<%@ page import="com.coldman.dao.TemperatureDao"%>
<%@ page import="com.coldman.bean.HumidityBean"%>
<%@ page import="com.coldman.bean.TemperatureBean"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@ page import="java.util.concurrent.TimeUnit"%>

<%
response.setHeader("Cache-Control","no-cache"); 
response.setHeader("Pragma","no-cache"); 
response.setDateHeader ("Expires", -1); 
%>

<%!
static final Logger logger = LoggerFactory.getLogger("ParameterSummary");
%>
<%
String sUsername = (String) session.getAttribute("username");
if(sUsername == null || sUsername.equals("")) {
	session.setAttribute("callingPage", "ParameterSummary.jsp");
	response.sendRedirect("../../index.html");
}
String sFromDate = (String) request.getParameter("from_date");
String sToDate = (String) request.getParameter("to_date");
sUsername = (String) session.getAttribute("username");
java.util.Date dt = new java.util.Date();
java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd-MM-yyyy HH:mm");
sdf.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
String currentTime = sdf.format(dt);
ParameterSummaryDao psDao = new ParameterSummaryDao();
ParameterSummaryBean psBean = new ParameterSummaryBean();
Collection<ParameterSummaryBean> col = new ArrayList<ParameterSummaryBean>();
String sParameter = request.getParameter("Parameter");
String sListView = request.getParameter("showView1");
String sNodeId = request.getParameter("SubZone");
if(sNodeId == null || sNodeId.equals("")) sNodeId = "All";
String sLocation = request.getParameter("Location");
String sZoneId = request.getParameter("Zone");
TreeMap<String, String> tmLocation = new TreeMap<String, String>();
TreeMap<String, String> tmZone = new TreeMap<String, String>();
ClientLocationMapDao clmDao = new ClientLocationMapDao();
String sValue = "";
if(clmDao.colAllLocations == null || clmDao.colAllLocations.size() == 0) clmDao.getAllLocationsData();
String sTempLocationId = "", sTempZoneId = "", sTempNodeId = "";
for(AllLocationDataBean alBean: clmDao.colAllLocations){
	if(sLocation == null || sLocation.equals("")) sLocation = alBean.getLocation_Name();
	if(alBean.getLocation_Name().equals(sLocation)) sTempLocationId = alBean.getLocation_Id();
	if(sZoneId == null || sZoneId.equals("")) sZoneId = alBean.getZone_Name();
	if(alBean.getZone_Name().equals(sZoneId)) sTempZoneId = alBean.getZone_Id();
	if(alBean.getSubzone_Name().equals(sNodeId)) sTempNodeId = alBean.getSubzone_Id();
	sValue = "";
	if(tmLocation.containsKey(alBean.getLocation_Name())) {
		sValue = tmLocation.get(alBean.getLocation_Name());
		if(sValue.indexOf(alBean.getZone_Name()) >= 0) {
			
		} else {
			sValue = sValue + ";" + alBean.getZone_Name();
			tmLocation.put(alBean.getLocation_Name(), sValue);
		}
	} else {
		tmLocation.put(alBean.getLocation_Name(), alBean.getZone_Name());
	}
	sValue = "";
	if(tmZone.containsKey(alBean.getZone_Name())) {
		sValue = tmZone.get(alBean.getZone_Name());
		if(sValue.indexOf(alBean.getSubzone_Name()) >= 0) {
			
		} else {
			sValue = sValue + ";" + alBean.getSubzone_Name();
			tmZone.put(alBean.getZone_Name(), sValue);
		}
	} else {
		tmZone.put(alBean.getZone_Name(), alBean.getSubzone_Name());
	}
}
if(sTempNodeId.equals("")) sTempNodeId = sNodeId;
String temperature = "", temperature1 = "",  temperature2 = "";
String time = "", time1 = "", time2 = "";	
HumidityDao hd = new HumidityDao();
HumidityBean hdBean = new HumidityBean();
TemperatureDao td = new TemperatureDao();
TemperatureBean tdBean = new TemperatureBean();
SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MMM HH:mm");
SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy");
SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy/M/d");
df.setTimeZone(java.util.TimeZone.getTimeZone("UTC"));
sdf1.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
sdf2.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
Date d = null;
Date d2 = null;
Date dFromDateUI = null;
Date dToDateUI = null;
Date dFromDate = null;
Date dToDate = null;
String sFromDate1 = "", sToDate1 = "", sFromDate2 = "", sToDate2 = "";;
if(sFromDate != null && !sFromDate.equals("")) {
	d = sdf2.parse(sFromDate);
	sFromDate1 = df.format(d);
	sFromDate2 = sdf3.format(d);
	dFromDate = sdf3.parse(sFromDate2);
	dFromDateUI = sdf2.parse(sFromDate);
} else {
	d = new Date(dt.getTime() - (7 * Constants.DAY_IN_MS));
	sFromDate1 = df.format(d);
	sFromDate = sdf2.format(d);
	sFromDate2 = sdf3.format(d);
	dFromDate = sdf3.parse(sFromDate2);
	dFromDateUI = sdf2.parse(sFromDate);
}
if(sToDate != null && !sToDate.equals("")) {
	d = sdf2.parse(sToDate);
	sToDate1 = df.format(d);
	sToDate2 = sdf3.format(d);
	dToDate = sdf3.parse(sToDate2);
	dToDateUI = sdf2.parse(sToDate);
} else {
	sToDate1 = df.format(dt);
	sToDate = sdf2.format(dt);
	sToDate2 = sdf3.format(dt);
	dToDate = sdf3.parse(sToDate2);
	dToDateUI = sdf2.parse(sToDate);
}
if(sParameter == null || sParameter.equals("")) sParameter = Constants.TEMPERATURE;
if(sParameter.equals(Constants.HUMIDITY)) {
	col = psDao.getParameterSummaryHumidityData(sFromDate1, sToDate1);
} else {
	col = psDao.getParameterSummaryTemperatureData(sFromDate1, sToDate1);
}
Long newTime = null;
Collection col1 = null;
int iColSize = 0;
int iSize = 0;
Collection tempCol = new ArrayList();
String sTempDate = "";
Iterator iterSt = null;
if(sParameter.equals(Constants.TEMPERATURE)) {
	if(sNodeId.equals("All")) {
		col1 = td.getTemperatureDataForAllSubzones(sFromDate1, sToDate1);
		iterSt = col1.iterator();
		while(iterSt.hasNext()) {
			tdBean = (TemperatureBean) iterSt.next();
				if(tdBean.getZone_Id().equals(sTempZoneId)) {
					d = df.parse(tdBean.getTimestamp());
					newTime = d.getTime();
					newTime +=(330*60*1000);
					d2 = new Date(newTime);
					//d = df.parse(tdBean.getTimestamp());
					sTempDate = sdf2.format(d2);
					if(sdf2.parse(sTempDate).getTime() >= dFromDateUI.getTime() && sdf2.parse(sTempDate).getTime() <= dToDateUI.getTime()) {
						tempCol.add(tdBean);
					}
				}
			}
		
	} else {
		col1 = td.getTemperatureDataForWeek();
		iterSt = col1.iterator();
		while(iterSt.hasNext()) {
			tdBean = (TemperatureBean) iterSt.next();
			if(tdBean.getLocation_id().equals(sTempLocationId)) {
				if(tdBean.getZone_Id().equals(sTempZoneId)) {
					if(sTempNodeId.equals(tdBean.getNode_Id())) {
						d = df.parse(tdBean.getTimestamp());
						newTime = d.getTime();
						newTime +=(330*60*1000);
						d2 = new Date(newTime);
						//d = df.parse(tdBean.getTimestamp());
						sTempDate = sdf2.format(d2);
						if(sdf2.parse(sTempDate).getTime() >= dFromDateUI.getTime() && sdf2.parse(sTempDate).getTime() <= dToDateUI.getTime()) {
							tempCol.add(tdBean);
						}
					}
				}
			}
		}
	}
	iterSt = tempCol.iterator();
	iColSize = tempCol.size()/10;
	iSize = 0;
	
	while(iterSt.hasNext()) {
		tdBean = (TemperatureBean) iterSt.next();
		
		if(iColSize != 0 && iSize%iColSize == 0){
			d = df.parse(tdBean.getTimestamp());
			temperature += "\"" + tdBean.getTemperature() + "\", ";
			time += "\"" + sdf1.format(d) + "\", ";
		} else {
			if(tempCol.size() != 0 && tempCol.size() <= 10){
				d = df.parse(tdBean.getTimestamp());
				temperature += "\"" + tdBean.getTemperature() + "\", ";
				time += "\"" + sdf1.format(d) + "\", ";
			} else{
				temperature += "\"" + tdBean.getTemperature() + "\", ";
				time += "\"\", ";
			}
		}
		iSize++;
	}
	if(temperature.equals("") || time.equals("")){
		temperature1 = "00";
		time1 = "00";
	} else {
		if(temperature.length() >= 2)	{
			temperature1 = temperature.substring(0, (temperature.length() - 2));
		}
			if(time.length() >= 2) time1 = time.substring(0, (time.length() - 2));
	}
} else if(sParameter.equals(Constants.HUMIDITY)) {
	temperature = ""; time = "";
	if(sNodeId.equals("All")) {
		col1 = hd.getHumidityDataForAllSubzones(sFromDate1, sToDate1);
		iterSt = col1.iterator();
		while(iterSt.hasNext()) {
			hdBean = (HumidityBean) iterSt.next();
			if(hdBean.getZone_Id().equals(sTempZoneId)) {
				d = df.parse(hdBean.getTimestamp());
				sTempDate = sdf2.format(d);
				//System.out.println("Timestamp: " + sTempDate + ";" + sFromDate1 + ";" + sToDate1);
				if(sdf2.parse(sTempDate).getTime() >= dFromDateUI.getTime() && sdf2.parse(sTempDate).getTime() <= dToDateUI.getTime()) {
					//System.out.println("Added");
					tempCol.add(hdBean);
				}
			}
		}
	} else {
		col1 = hd.getHumidityDataForWeek();
		iterSt = col1.iterator();
		while(iterSt.hasNext()) {
			hdBean = (HumidityBean) iterSt.next();
			if(hdBean.getLocation_id().equals(sTempLocationId)) {
				if(hdBean.getZone_Id().equals(sTempZoneId)) {
					if(sTempNodeId.equals(hdBean.getNode_Id())) {
						d = df.parse(hdBean.getTimestamp());
						sTempDate = sdf2.format(d);
						//System.out.println("Timestamp: " + sTempDate + ";" + sFromDate1 + ";" + sToDate1);
						if(sdf2.parse(sTempDate).getTime() >= dFromDateUI.getTime() && sdf2.parse(sTempDate).getTime() <= dToDateUI.getTime()) {
							//System.out.println("Added");
							tempCol.add(hdBean);
						}
					}
				}
			}
		}
	}
	iterSt = tempCol.iterator();
	iColSize = tempCol.size()/10;
	iSize = 0;
	Iterator iterSh = tempCol.iterator();
	while(iterSh.hasNext()) {
		hdBean = (HumidityBean) iterSh.next();
		if(iColSize != 0 && iSize%iColSize == 0){
			d = df.parse(hdBean.getTimestamp());
			temperature += "\"" + hdBean.getHumidity() + "\", ";
			time += "\"" + sdf1.format(d) + "\", ";
		} else {
			 if(tempCol.size() != 0 && tempCol.size() <= 10){
				d = df.parse(hdBean.getTimestamp());
				temperature += "\"" + hdBean.getHumidity() + "\", ";
				time += "\"" + sdf1.format(d) + "\", ";
			} else{
				temperature += "\"" + hdBean.getHumidity() + "\", ";
				time += "\"\", ";
			}
		}
		iSize++;
	}
	if(temperature.equals("") || time.equals("")){
		temperature2 = "00";
		time2 = "00";
	} else {
		if(temperature.length() >= 2)	temperature2 = temperature.substring(0, (temperature.length() - 2));
			if(time.length() >= 2) time2 = time.substring(0, (time.length() - 2));
	}
}
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Parameter Summary</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<!-- Add these below two lines for autocomplete and the javascript function at the end  -->
<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="../../bootstrap/css/bootstrap.css">
   
<!-- Font Awesome -->
<script src="https://use.fontawesome.com/37ba1872d2.js"></script>
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../../dist/css/tagbox.css">
<link href="../../dist/css/select2.css" rel="stylesheet" />
<script src="../../dist/js/select2.js"></script>
<script src="../../dist/js/tagbox.js"></script>
<script src="../../dist/js/location.js"></script>
<script src="../../dist/js/table2download.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#Parameter").select2();
	$("#Location").select2();
	$("#Zone").select2();
	$("#SubZone").select2();
});
</script>
<STYLE TYPE="text/css">
			table.sortable span.sortarrow {
   				color: blue;
   				text-decoration: none;
			}
					
		</style>
<script src="../../dist/js/sorttable.js"></script>
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
<link href="https://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet">

<link rel="stylesheet" href="../../dist/css/bootstrap-switch.css">
<script src="../../dist/js/bootstrap-switch1.js"></script>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
  body {
	font-family:  Lato, sans-serif;
	font-weight: 600;
}
</style>
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
	<div class="wrapper">

		<header class="main-header">
			<!-- Logo -->
			<div>
				<a href="#" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
					<span class="logo-mini"><img src="../../dist/img/cipla_logo.jpeg" style="margin-top:14px"
						alt="User Image"></span> <!-- logo for regular state and mobile devices -->
					<span class="logo-lg"><img src="../../dist/img/cipla_logo.jpeg" 
						alt="User Image"></span>
				</a>
			</div>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">

				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
					role="button"> <span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
				</a>
				<form class="navbar-form pull-left" role="search">
					<div class="form-group">
						<b style="color:#fff;padding-left: 15px; font-size: 20px;" class="pull-right">Parameter Summary</b><br> <b style="padding-left: 15px;color:#fff;"><%= currentTime %></b>
					</div>
				</form>

				<div class="navbar-custom-menu">
					<img src="../../dist/img/Logo.png" class="logo-class pull-right"
						alt="User Image">
				</div>
			</nav>

		</header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->


				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<br>
				<ul class="sidebar-menu">
					<li class="header">MAIN NAVIGATION</li>
					<li class="treeview"><a href="#"> <i
							class="fa fa-dashboard"></i> <span>Dashboard</span>
					</a>
						<ul class="treeview-menu">
							<li class="">
							<%if(sUsername != null && sUsername.equals("demo2")) { %>
							<a href="CMCDashboard.jsp"><i
									class="fa fa-circle-o"></i> CMC</a>
							<%} else { %>
							<a href="CMCDashboard.jsp" role="button" class="btn btn-disabled disabled"><i
									class="fa fa-circle-o"></i> CMC</a>
							<%} %>
									</li>
							<!-- <li class=""><a href="NewDashboard.jsp"><i
									class="fa fa-circle-o"></i> Vehicle Dashboard</a></li> -->
							<li class=""><a href="TodayColdChainHealth.jsp"><i
									class="fa fa-circle-o"></i> Cold Rooms Summary</a></li>
							<!--  <li><a href="VehiclePlannedRouteMap.jsp"><i class="fa fa-circle-o"></i> Vehicle Planned Route</a></li> -->
							<li class=""><a href="TodayColdChainHealthVehicle.jsp" role="button" class="btn btn-disabled disabled"><i
									class="fa fa-circle-o"></i> Vehicles Summary</a></li>
						</ul></li>
					<li class="treeview"><a href="#"> <i
							class="fa fa-files-o"></i> <span>Alerts</span> 
					</a>
						<ul class="treeview-menu">
							<li><a href="LiveVehicleTracking.jsp" role="button" class="btn btn-disabled disabled"><i
									class="fa fa-circle-o"></i> Live Vehicle Tracking</a></li>
						</ul></li>
					
					<li class="treeview"><a href="#"> <i
							class="fa fa-database"></i> <span>Admin</span>
					</a>
						<ul class="treeview-menu">
							<li class=""><a href="VehicleManagement.jsp" role="button" class="btn btn-disabled disabled"><i class="fa fa-circle-o"></i>
									Vehicle Management</a></li>
							<li><a href="VendorScorecard.jsp" role="button" class="btn btn-disabled disabled"><i class="fa fa-circle-o"></i>
									Vehicle Score Card</a></li>
						</ul></li>
						<li class="treeview active"><a href="#"> <i
							class="fa fa-bar-chart"></i> <span>Reports</span>
					</a>
						<ul class="treeview-menu">
							<li class="active"><a href="ParameterSummary.jsp"><i class="fa fa-circle-o"></i>
									Parameter Summary</a></li>
							<li><a href="AlertsSummary.jsp"><i class="fa fa-circle-o"></i>
									Alerts Summary</a></li>
							<li><a href="AlertsReport.jsp"><i class="fa fa-circle-o"></i>
									Alerts Report</a></li>		
							
						</ul></li>
						<li class="treeview"><a href="#"> <i
							class="fa fa-history"></i> <span>Historical Data</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="HistoricalData.jsp"><i class="fa fa-circle-o"></i>
									Download <br>Temperature Data</a></li>
						</ul></li>
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			
		<section id="mainSection" class="content">
		<div class="well well-sm white-background">
							
						<br>
						<form id="myForm1" name="myForm1" method="post"
											action="ParameterSummary.jsp">
						<div class="row">
						<div class="col-md-offset-3 col-md-6">
						<div class="row well well-sm">
					<div class="col-md-3 text-center">
					<label><b>View As</b></label>&nbsp;&nbsp;<br><br><input name="my-checkbox" id="my-checkbox" type="checkbox" data-size="mini" onchange="ViewChange();" checked>
					<input type="hidden" id="showView1" name="showView1" value="<%= sListView%>">
					</div>
						<div class="col-md-3 text-center">
							<label><b>Parameter</b></label>&nbsp;&nbsp;<br><br>
					<select id="Parameter" name="Parameter" class="form-control-no-background pull-center">
					<%if(sParameter.equals(Constants.HUMIDITY)) { %>
					<option value="Temperature">Temperature</option>
					<option value="Humidity" selected>Humidity</option>
					<%} else { %>
					<option value="Temperature" selected>Temperature</option>
					<option value="Humidity">Humidity</option>
					<%} %>
					</select>
						</div>
						<div class="col-md-4 text-center" id="time-range">
    <label><b>Time Range</b></label><br><label class="slider-time"></label> - <label class="slider-time2"></label>
    
    <div class="sliders_step1">
        <div id="slider-range"></div>
    </div>
    <input type="hidden" name="from_date" id="from_date" value="">
    <input type="hidden" name="to_date" id="to_date" value="">
</div>
						
						<div class="col-md-2 text-center"><br>
								<button type="submit" class="btn btn-success">GO</button>&nbsp;&nbsp;
								</div>
						
					</div>
					</div></div>
					<div id="listView" style="display:none;" class="text-center">
					
					<div class="row">
					<div class="col-md-offset-9 col-md-3">
						<span id="csvLink"></span>&nbsp;&nbsp;&nbsp;&nbsp;<span id="xlsLink"></span>&nbsp;&nbsp;<a type="button" class="btn btn-sm" data-toggle="modal" data-target="#myModal"><i class="fa fa-info-circle fa-lg fa-color-blue"></i></a>
					</div>
					
					</div>
					<br>
					<div class="scroll-area-table">
					<table id="data_table_stationary" class="table table-fixed sortable table-bordered table-striped table-condensed">
									<thead>
									<tr class="info text-center">
									<th class="text-center">Location</th>
									<th class="text-center">Zone</th>
									<th class="text-center">SubZone</th>
									<th class="text-center">Max</th>
									<th class="text-center">Min</th>
									<th class="text-center">Average</th>
									<th class="text-center">Volatility</th>
									<th class="text-center">Excursion</th>
									</tr>
									</thead>
									<tbody>
									<%for(ParameterSummaryBean ps: col) {
										
											
									%>
									<tr><td><%=ps.getLocation_ID() %></td>	<td><%= ps.getZone_ID() %></td><td><%= ps.getSub_Zone_ID() %></td>	<td><%= ps.getMax_Value() %></td>	<td><%= ps.getMin_Value() %></td>	<td><%= Utils.roundIt(ps.getMean_Value()) %></td>	
									<%if(ps.getVol_Ap().equals("")) ps.setVol_Ap("0");
										if(ps.getExcursion().equals("")) ps.setExcursion("0");%>
											<td><%= Utils.roundToOneDecimal(ps.getVol_Ap()) %>%</td> <td><%= Utils.roundToOneDecimal(ps.getExcursion()) %>%</td></tr>
										<%
									}%>
									</tbody></table>
									</div>
									</div>
									<br>
									<div id="chartView" class="text-center">
									
						
						<div class="row"><div class="col-md-offset-3 col-md-5">
						
						<%
						if(sParameter.equals(Constants.TEMPERATURE)) { %>
						<u class="pull-right"><a id="link2" download="ChartJpg.jpg">Save As JPG</a></u><br><br>
						<div class="box box-danger">
						<%} else { %>
						<u class="pull-right" style="display:none"><a id="link2" download="ChartJpg.jpg">Save As JPG</a></u>
						<div class="box box-danger" style="display:none;">
						<%} %>
							<div class="box-header with-border">
														<h3 class="box-title">&nbsp;Temperature</h3>
														<div class="box-tools pull-right">
															<button type="button" class="btn btn-box-tool"
																data-widget="collapse">
																<i class="fa fa-minus"></i>
															</button>
															<button type="button" class="btn btn-box-tool"
																data-widget="remove">
																<i class="fa fa-times"></i>
															</button>
														</div>
													</div>
													<div class="box-body">
														<%
															if(temperature1.equals("00")){
														%>
														No Data!
														<%
															} else {
														%>
														<div class="chart">
															<canvas id="lineChart2"
																style="height: 270px; width: 100px"></canvas>
														</div>
														<%} %>
													</div>
												</div>
												<%if(sParameter.equals(Constants.HUMIDITY)) { %>
												<u class="pull-right"><a id="link3" download="ChartJpg.jpg">Save As JPG</a></u><br><br>
						<div class="box box-danger">
						<%} else { %>
						<u class="pull-right" style="display:none"><a id="link3" download="ChartJpg.jpg">Save As JPG</a></u>
						<div class="box box-danger" style="display:none;">
						<%} %>
							<div class="box-header with-border">
														<h3 class="box-title">&nbsp;Relative Humidity</h3>
														<div class="box-tools pull-right">
															<button type="button" class="btn btn-box-tool"
																data-widget="collapse">
																<i class="fa fa-minus"></i>
															</button>
															<button type="button" class="btn btn-box-tool"
																data-widget="remove">
																<i class="fa fa-times"></i>
															</button>
														</div>
													</div>
													<div class="box-body">
														<%
															if(temperature2.equals("00")){
														%>
														No Data!
														<%
															} else {
														%>
														<div class="chart">
															<canvas id="lineChart3"
																style="height: 270px; width: 100px"></canvas>
														</div>
														<%} %>
													</div>
												</div>
												</div>
												<div class="col-md-offset-1 col-md-2"><br><br>
												<div class="row well well-sm">
					<div class="col-md-12">
					<label><b>Location</b></label>&nbsp;&nbsp;<br>
					<select id="Location" name="Location" class="form-control-no-background pull-center" onchange="reloadZone();">
					<%Set set = (Set) tmLocation.keySet();
						Iterator iter = set.iterator();
						while(iter.hasNext()){
							sValue = (String) iter.next();
							if(sLocation.equals(sValue)) {
						%>
							<option value="<%= sValue %>" selected><%= sValue %></option>
						<% } else {%>
							<option value="<%= sValue %>"><%= sValue %></option>
						<%}
						}
					%>
					</select>
					<br><br>
							<label><b>Zone</b></label>&nbsp;&nbsp;<br>
					<select id="Zone" name="Zone" class="form-control-no-background pull-center" onchange="reloadNodes();">
					<%
						sValue = tmLocation.get(sLocation);
						String sZone[] = sValue.split(";");
						for(int i =0; i<sZone.length; i++) {
							if(sZoneId.equals(sZone[i])) {	
						%>
							<option value="<%= sZone[i] %>" selected><%= sZone[i] %></option>
						<%} else {%>
						<option value="<%= sZone[i] %>"><%= sZone[i] %></option>
						<%}
						}
					%>
					</select>
						<br><br>
							<label><b>Sub Zone</b></label>&nbsp;&nbsp;<br>
					<select id="SubZone" name="SubZone" class="form-control-no-background pull-center">
					<option value="All">All</option>
					<%
						sValue = tmZone.get(sZoneId);
						sZone = sValue.split(";");
						for(int i =0; i<sZone.length; i++) {
							if(sNodeId.equals(sZone[i])) {	
						
						%>
							<option value="<%= sZone[i] %>" selected><%= sZone[i] %></option>
						<%} else {%>
						<option value="<%= sZone[i] %>"><%= sZone[i] %></option>
						<%} 
						}
					%>
					</select>
					<br><br>
					
								<button type="submit" class="btn btn-success">GO</button>&nbsp;&nbsp;
								
								</div>
						
					</div>
												</div>
												</div>
						</div>	
						
						</form>
								</div>
								<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center btn-info">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Definitions</h4>
      </div>
      <div class="modal-body">
       <h3>Volatility: </h3>This indiacates the percentage of cases when the temperature moves across the mid point of threshold. Eg. if the expected range is 
0-6, 3 is the mid point. Volatility is the number of data points, that move from below 3 to above 3 or vice versa, as a percentage of all data points.<br>
       <h3>Excursion: </h3>Number of data points that breach the lower or upper threshold but do not lead to alerts, as a percentage of all data points.<br>
      </div>
      <div class="modal-footer text-center" style="text-align:center">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
						
		</section>
		</div>
							</div>
		
		<!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0
			</div>
			<strong>Copyright &copy; 2017 <a href="http://tagbox.in">TagBox</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Control Sidebar -->
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->
<!-- ChartJS 1.0.1 -->
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>


	<!-- Bootstrap 3.3.6 -->
	<script src="../../bootstrap/js/bootstrap.min.js"></script>
	<!-- ChartJS 1.0.1 -->
	<!-- AdminLTE App -->
	<script src="../../dist/js/app.min.js"></script>
	<script src="../../plugins/chartjs/Chart.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="../../dist/js/demo.js"></script>
	
	<script type="text/javascript">
	Chart.types.Line.extend({
	    name: "LineAlt",
	    draw: function(){
	        Chart.types.Line.prototype.draw.apply(this, arguments);

	        this.chart.ctx.textAlign = "center";
	        this.chart.ctx.fillText("BANGALORE", this.scale.calculateX(423.5), this.scale.calculateY(6.75));
	        this.chart.ctx.fillText("BIAL", this.scale.calculateX(923.5), this.scale.calculateY(6.75));
	        this.chart.ctx.fillText("MUMBAI", this.scale.calculateX(1723.5), this.scale.calculateY(6.75));
	        this.chart.ctx.fillText("MADRID", this.scale.calculateX(2223.5), this.scale.calculateY(6.75));
	    }
	});
	
	jQuery( document ).ready(function() {
	    jQuery( "#data_table_stationary" ).table_download({
	        format: "xls",
	        separator: ",",
	        filename: "download",
	        jspname: "ParameterSummary",
	        linkname: "Export To XLS",
	        quotes: "\"",
	        linkid: "xlsLink"
	    });
	    
	    jQuery( "#data_table_stationary" ).table_download({
	        format: "csv",
	        separator: "-",
	        filename: "download",
	        jspname: "ParameterSummary",
	        linkname: "Export To CSV",
	        quotes: "\"",
	        linkid: "csvLink"
	    });  
	     
	    
	});



	function ViewChange() {
		var a = (document.getElementById("my-checkbox").checked);
	  	if(a == false){
	  		$('#listView').show();
	  		$('#mapView').hide();
	  	} else {
	  		$('#listView').hide();
	  		$('#mapView').show();
	  		
	  	}
	}
	
function done() {
		
        var url_base64jp = document.getElementById("lineChart2").toDataURL("image/jpg");

       // link1.href = url_base64;
        link2.href=url_base64jp;

       // var url = link1.href.replace(/^data:image\/[^;]/, 'data:application/octet-stream');

    }
    
function done1() {
	
    var url_base64jp = document.getElementById("lineChart3").toDataURL("image/jpg");

   // link1.href = url_base64;
    link3.href=url_base64jp;

   // var url = link1.href.replace(/^data:image\/[^;]/, 'data:application/octet-stream');

}
var areaChartData2 = {
		labels : [<%= time1%>],
	datasets : [ 
	             {
		label : "Temperature",
		fillColor : "rgba(210, 214, 222, 1)",
		strokeColor : "rgba(210, 214, 222, 1)",
		pointColor : "rgba(210, 214, 222, 1)",
		pointStrokeColor : "#c1c7d1",
		pointHighlightFill : "#fff",
		pointHighlightStroke : "rgba(220,220,220,1)",
		data : [<%= temperature1%>]
	}
	
	]
};

var areaChartData3 = {
		labels : [<%= time2%>],
	datasets : [ 
	             {
		label : "Humidity",
		fillColor : "rgba(210, 214, 222, 1)",
		strokeColor : "rgba(210, 214, 222, 1)",
		pointColor : "rgba(210, 214, 222, 1)",
		pointStrokeColor : "#c1c7d1",
		pointHighlightFill : "#fff",
		pointHighlightStroke : "rgba(220,220,220,1)",
		data : [<%= temperature2%>]
	}
	]
};
var areaChartOptions = {
	scales : {
		xAxes : [ {
			ticks : {
				maxRotation : 90,
				minRotation : 90
			}
		} ]
	},
	scaleOverride: false,
    animation: false,
	//Boolean - If we should show the scale at all
	showScale : true,
	//Boolean - Whether grid lines are shown across the chart
	scaleShowGridLines : false,
	//String - Colour of the grid lines
	scaleGridLineColor : "rgba(0,0,0,.05)",
	//Number - Width of the grid lines
	scaleGridLineWidth : 1,
	//Boolean - Whether to show horizontal lines (except X axis)
	scaleShowHorizontalLines : true,
	//Boolean - Whether to show vertical lines (except Y axis)
	scaleShowVerticalLines : true,
	//Boolean - Whether the line is curved between points
	bezierCurve : true,
	//Number - Tension of the bezier curve between points
	bezierCurveTension : 0.3,
	//Boolean - Whether to show a dot for each point
	pointDot : false,
//Number - Radius of each point dot in pixels
pointDotRadius : 1,
//Number - Pixel width of point dot stroke
pointDotStrokeWidth : 1,
//Number - amount extra to add to the radius to cater for hit detection outside the drawn point !!!important to display only one point
pointHitDetectionRadius : 0,
//Boolean - Whether to show a stroke for datasets
datasetStroke : true,
//Number - Pixel width of dataset stroke
datasetStrokeWidth : 1,
	//Boolean - Whether to fill the dataset with a color
	datasetFill : true,
	//String - A legend template
	//Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
	maintainAspectRatio : true,
	//Boolean - whether to make the chart responsive to window resizing
	responsive : true,
	onAnimationComplete: done
};

var areaChartOptions1 = {
		scales : {
			xAxes : [ {
				ticks : {
					maxRotation : 90,
					minRotation : 90
				}
			} ]
		},

		//Boolean - If we should show the scale at all
		showScale : true,
		//Boolean - Whether grid lines are shown across the chart
		scaleShowGridLines : false,
		//String - Colour of the grid lines
		scaleGridLineColor : "rgba(0,0,0,.05)",
		//Number - Width of the grid lines
		scaleGridLineWidth : 1,
		//Boolean - Whether to show horizontal lines (except X axis)
		scaleShowHorizontalLines : true,
		//Boolean - Whether to show vertical lines (except Y axis)
		scaleShowVerticalLines : true,
		//Boolean - Whether the line is curved between points
		bezierCurve : true,
		//Number - Tension of the bezier curve between points
		bezierCurveTension : 0.3,
		//Boolean - Whether to show a dot for each point
		pointDot : false,
	//Number - Radius of each point dot in pixels
	pointDotRadius : 1,
	//Number - Pixel width of point dot stroke
	pointDotStrokeWidth : 1,
	//Number - amount extra to add to the radius to cater for hit detection outside the drawn point !!!important to display only one point
	pointHitDetectionRadius : 0,
	//Boolean - Whether to show a stroke for datasets
	datasetStroke : true,
	//Number - Pixel width of dataset stroke
	datasetStrokeWidth : 1,
		//Boolean - Whether to fill the dataset with a color
		datasetFill : true,
		//String - A legend template
		//Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
		maintainAspectRatio : true,
		//Boolean - whether to make the chart responsive to window resizing
		responsive : true,
		onAnimationComplete: done1

	};

//-------------
//- LINE CHART -
//--------------
areaChartOptions.datasetFill = false;
areaChartOptions1.datasetFill = false;
//$('a[data-toggle=tab').on('shown.bs.tab', function(e) {
//	window.dispatchEvent(new Event('resize'));
<%if(!temperature1.equals("00")){%>
var lineChartCanvas2 = $("#lineChart2").get(0).getContext("2d");
	var lineChart2 = new Chart(lineChartCanvas2);
	lineChart2.Line(areaChartData2, areaChartOptions);
	
<%}
if(!temperature2.equals("00")){%>	
	var lineChartCanvas3 = $("#lineChart3").get(0).getContext("2d");
	var lineChart3 = new Chart(lineChartCanvas3);
	lineChart3.Line(areaChartData3, areaChartOptions1);
<%}%>

//System.out.println(";"+temperature4+";");
//if(!temperature4.equals("00")){%>
//var lineChartCanvas4 = $("#lineChart4").get(0).getContext("2d");
//var lineChart4 = new Chart(lineChartCanvas4);
//lineChart4.Line(areaChartData4, areaChartOptions);
<%//}
%>

//});
$("[name='my-checkbox']").bootstrapSwitch();
if($("#showView1").val() == null || $("#showView1").val() == "null" || $("#showView1").val() == "chart"){
	$("[name='my-checkbox']").bootstrapSwitch();
	$('#listView').hide();
	$('#chartView').show();
} else {
	$("[name='my-checkbox']").bootstrapSwitch('toggleState');
	$('#listView').show();
	$('#chartView').hide();
}

function ViewChange(){
  	var a = (document.getElementById("my-checkbox").checked);
  	if(a == false){
  		$('#listView').show();
  		$('#chartView').hide();
  		$('#showView1').val("list");
  	} else {
  		$('#listView').hide();
  		$('#chartView').show();
  		$('#showView1').val("chart");
  	}
  	//alert(a);
  }
  
var currentTime = new Date();
var currentMonth = currentTime.getMonth() + 1;
var currentDay = currentTime.getDate();
var currentYear = currentTime.getFullYear();
var dt_to = currentYear + "/" + currentMonth + "/" + currentDay;
currentTime.setDate(currentTime.getDate() - 7);
var dt_from= currentTime.getFullYear() + "/" + (currentTime.getMonth() + 1) + "/" + (currentTime.getDate());
currentTime = new Date(<%= dToDate.getTime()%>);
currentMonth = currentTime.getMonth() + 1;
currentDay = currentTime.getDate();
currentYear = currentTime.getFullYear();
var dt_to1 = zeroPad(currentDay, 2) + "/" + zeroPad(currentMonth, 2) + "/" + currentYear;
var dt_to2 = currentTime.getFullYear() + "/" + (currentTime.getMonth() + 1) + "/" + (currentTime.getDate());;
currentTime = new Date(<%=dFromDate.getTime()%>);
var dt_from1= zeroPad(currentTime.getDate(), 2) + "/" + zeroPad((currentTime.getMonth() + 1), 2) + "/" + currentTime.getFullYear();
var dt_from2= currentTime.getFullYear() + "/" + (currentTime.getMonth() + 1) + "/" + (currentTime.getDate());;
$('.slider-time').html(dt_from1);
$('.slider-time2').html(dt_to1);
$('#from_date').val(dt_from1);
$('#to_date').val(dt_to1);
var min_val = Date.parse(dt_from)/1000;
var max_val = Date.parse(dt_to)/1000;
var min_val1 = Date.parse(dt_from2)/1000;
var max_val1 = Date.parse(dt_to2)/1000;
function zeroPad(num, places) {
  var zero = places - num.toString().length + 1;
  return Array(+(zero > 0 && zero)).join("0") + num;
}
function formatDT(__dt) {
    var year = __dt.getFullYear();
    var month = zeroPad(__dt.getMonth()+1, 2);
    var date = zeroPad(__dt.getDate(), 2);
    return date + '/' + month + '/' + year;
};

$("#slider-range").slider({
    range: true,
    min: min_val,
    max: max_val,
    step: 10,
    values: [min_val1, max_val1],
    slide: function (e, ui) {
        var dt_cur_from = new Date(ui.values[0]*1000); //.format("yyyy-mm-dd hh:ii:ss");
        $('.slider-time').html(formatDT(dt_cur_from));

        var dt_cur_to = new Date(ui.values[1]*1000); //.format("yyyy-mm-dd hh:ii:ss");                
        $('.slider-time2').html(formatDT(dt_cur_to));
        $('#from_date').val(formatDT(dt_cur_from));
        $('#to_date').val(formatDT(dt_cur_to));
    }
});

</script>
	</body>
</html>
