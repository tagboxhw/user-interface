package com.coldman.bean;

import java.io.Serializable;

public class MapsBean implements Serializable{

	/**
	 * Generated serial version ID
	 */
	private static final long serialVersionUID = -1874474658549948076L;
	
	private String gateway_id;
	private String device_id;
	private String device_time;
	private int temp;
	private int humidity;
	
	private boolean door_open;
	private float latitude;
	private float longitude;
	private int halt_duration;
	private int open_duration;
	private boolean temp_alert;
	
	private boolean humidity_alert;
	private boolean halt_alert;
	private boolean detour_alert;
	private boolean door_alert;
	
	/**
	 * @return the gateway_id
	 */
	public String getGateway_id() {
		return gateway_id;
	}

	/**
	 * @param gateway_id the gateway_id to set
	 */
	public void setGateway_id(String gateway_id) {
		this.gateway_id = gateway_id;
	}

	/**
	 * @return the device_id
	 */
	public String getDevice_id() {
		return device_id;
	}

	/**
	 * @param device_id the device_id to set
	 */
	public void setDevice_id(String device_id) {
		this.device_id = device_id;
	}

	/**
	 * @return the device_time
	 */
	public String getDevice_time() {
		return device_time;
	}

	/**
	 * @param device_time the device_time to set
	 */
	public void setDevice_time(String device_time) {
		this.device_time = device_time;
	}

	/**
	 * @return the temp
	 */
	public int getTemp() {
		return temp;
	}

	/**
	 * @param temp the temp to set
	 */
	public void setTemp(int temp) {
		this.temp = temp;
	}

	/**
	 * @return the humidity
	 */
	public int getHumidity() {
		return humidity;
	}

	/**
	 * @param humidity the humidity to set
	 */
	public void setHumidity(int humidity) {
		this.humidity = humidity;
	}

	/**
	 * @return the door_open
	 */
	public boolean isDoor_open() {
		return door_open;
	}

	/**
	 * @param door_open the door_open to set
	 */
	public void setDoor_open(boolean door_open) {
		this.door_open = door_open;
	}

	/**
	 * @return the latitude
	 */
	public float getLatitude() {
		return latitude;
	}

	/**
	 * @param latitude the latitude to set
	 */
	public void setLatitude(float latitude) {
		this.latitude = latitude;
	}

	/**
	 * @return the longitude
	 */
	public float getLongitude() {
		return longitude;
	}

	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(float longitude) {
		this.longitude = longitude;
	}

	/**
	 * @return the halt_duration
	 */
	public int getHalt_duration() {
		return halt_duration;
	}

	/**
	 * @param halt_duration the halt_duration to set
	 */
	public void setHalt_duration(int halt_duration) {
		this.halt_duration = halt_duration;
	}

	/**
	 * @return the open_duration
	 */
	public int getOpen_duration() {
		return open_duration;
	}

	/**
	 * @param open_duration the open_duration to set
	 */
	public void setOpen_duration(int open_duration) {
		this.open_duration = open_duration;
	}

	/**
	 * @return the temp_alert
	 */
	public boolean isTemp_alert() {
		return temp_alert;
	}

	/**
	 * @param temp_alert the temp_alert to set
	 */
	public void setTemp_alert(boolean temp_alert) {
		this.temp_alert = temp_alert;
	}

	/**
	 * @return the humidity_alert
	 */
	public boolean isHumidity_alert() {
		return humidity_alert;
	}

	/**
	 * @param humidity_alert the humidity_alert to set
	 */
	public void setHumidity_alert(boolean humidity_alert) {
		this.humidity_alert = humidity_alert;
	}

	/**
	 * @return the halt_alert
	 */
	public boolean isHalt_alert() {
		return halt_alert;
	}

	/**
	 * @param halt_alert the halt_alert to set
	 */
	public void setHalt_alert(boolean halt_alert) {
		this.halt_alert = halt_alert;
	}

	/**
	 * @return the detour_alert
	 */
	public boolean isDetour_alert() {
		return detour_alert;
	}

	/**
	 * @param detour_alert the detour_alert to set
	 */
	public void setDetour_alert(boolean detour_alert) {
		this.detour_alert = detour_alert;
	}

	/**
	 * @return the door_alert
	 */
	public boolean isDoor_alert() {
		return door_alert;
	}

	/**
	 * @param door_alert the door_alert to set
	 */
	public void setDoor_alert(boolean door_alert) {
		this.door_alert = door_alert;
	}

	
	
	public MapsBean(){
		super();
	}
	
	public void init() {
		this.gateway_id = "";
		this.device_id = "";
		this.device_time = "";
		this.temp = 0;
		this.humidity = 0;
		this.door_open = false;
		this.latitude = 0;
		this.longitude = 0;
		this.halt_duration = 0;
		this.open_duration = 0;
		this.temp_alert = false;
		this.humidity_alert = false;
		this.halt_alert = false;
		this.detour_alert = false;
		this.door_alert = false;
	}
	
	public String toString(){
		String buf = null;
		buf = "Maps object: \n";
		buf = buf + "gateway_id: " + gateway_id + "\n";
		buf = buf + "device_id: " + device_id + "\n";
		buf = buf + "device_time: " + device_time + "\n";
		buf = buf + "temp: " + temp + "\n";
		buf = buf + "humidity: " + humidity + "\n";
		buf = buf + "door_open: " + door_open + "\n";
		buf = buf + "latitude: " + latitude + "\n";
		buf = buf + "longitude: " + longitude + "\n";
		buf = buf + "halt_duration: " + halt_duration + "\n";
		buf = buf + "open_duration: " + open_duration + "\n";
		buf = buf + "temp_alert: " + temp_alert + "\n";
		buf = buf + "humidity_alert: " + humidity_alert + "\n";
		buf = buf + "halt_alert: " + halt_alert + "\n";
		buf = buf + "detour_alert: " + detour_alert + "\n";
		buf = buf + "door_alert: " + door_alert + "\n";
		return buf;
	}
	

}
