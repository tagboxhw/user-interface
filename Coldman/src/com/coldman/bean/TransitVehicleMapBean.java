package com.coldman.bean;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement (name="transitVehicleMapBean")
public class TransitVehicleMapBean implements Serializable{

	/**
	 * Generated serial version ID
	 */
	private static final long serialVersionUID = -1874474658549948076L;
	
	private String Vehicle_ID;
	private String Vehicle_Status;
	private String Assigned_Date;
	private String Vehicle_Type;
	private String Driver_DL_ID;
	private String Route_Type;
	private String Source;
	private String Destination;
	private String Planned_Stops;
	private String GW_Status;
	private String Trip_Status;
	private String Handover_To;
	private String Driver_Change_To;
	private String Latitude;
	public String getLatitude() {
		return Latitude;
	}

	@XmlElement
	public void setLatitude(String latitude) {
		Latitude = latitude;
	}

	public String getLongitude() {
		return Longitude;
	}

	@XmlElement
	public void setLongitude(String longitude) {
		Longitude = longitude;
	}

	private String Longitude;
	
	public TransitVehicleMapBean(){
		super();
	}
	
	public void init() {
		this.Vehicle_ID = "";
		this.Vehicle_Status = "";
		this.Assigned_Date = "";
		this.Driver_DL_ID = "";
		this.Route_Type = "";
		this.Source = "";
		this.Destination ="";
		this.Planned_Stops = "";
		this.Vehicle_Type = "";
		this.GW_Status = "";
		this.Trip_Status = "";
		this.Handover_To = "";
		this.Driver_Change_To = "";
		this.Latitude = "";
		this.Longitude = "";
	}
	
	public String toString(){
		String buf = null;
		buf = "Transit Vehicle Map object: \n";
		buf = buf + "Vehicle_Status: " + Vehicle_Status + "\n";
		buf = buf + "Vehicle_ID: " + Vehicle_ID + "\n";
		buf = buf + "Vehicle_Type: " + Vehicle_Type + "\n";
		buf = buf + "Assigned_Date: " + Assigned_Date + "\n";
		buf = buf + "Driver_DL_ID: " + Driver_DL_ID + "\n";
		buf = buf + "Route_Type: " + Route_Type + "\n";
		buf = buf + "Source: " + Source + "\n";
		buf = buf + "Destination: " + Destination + "\n";
		buf = buf + "Planned_Stops: " + Planned_Stops + "\n";
		buf = buf + "GW_Status: " + GW_Status + "\n";
		buf = buf + "Trip_Status: " + Trip_Status + "\n";
		buf = buf + "Handover_To: " + Handover_To + "\n";
		buf = buf + "Driver_Change_To: " + Driver_Change_To + "\n";
		buf = buf + "Latitude: " + Latitude + "\n";
		buf = buf + "Longitude: " + Longitude + "\n";
		return buf;
	}

	public String getVehicle_Type() {
		return Vehicle_Type;
	}

	@XmlElement
	public void setVehicle_Type(String vehicle_Type) {
		this.Vehicle_Type = vehicle_Type;
	}

	public String getGW_Status() {
		return GW_Status;
	}

	@XmlElement
	public void setGW_Status(String gW_Status) {
		this.GW_Status = gW_Status;
	}

	public String getTrip_Status() {
		return Trip_Status;
	}

	@XmlElement
	public void setTrip_Status(String trip_Status) {
		this.Trip_Status = trip_Status;
	}

	public String getHandover_To() {
		return Handover_To;
	}
	
	@XmlElement
	public void setHandover_To(String handover_To) {
		this.Handover_To = handover_To;
	}

	public String getDriver_Change_To() {
		return Driver_Change_To;
	}

	@XmlElement
	public void setDriver_Change_To(String driver_Change_To) {
		this.Driver_Change_To = driver_Change_To;
	}

	/**
	 * @return the vehicle_ID
	 */
	public String getVehicle_ID() {
		return Vehicle_ID;
	}

	/**
	 * @param vehicle_ID the vehicle_ID to set
	 */
	@XmlElement
	public void setVehicle_ID(String vehicle_ID) {
		this.Vehicle_ID = vehicle_ID;
	}

	/**
	 * @return the vehicle_Status
	 */
	public String getVehicle_Status() {
		return Vehicle_Status;
	}

	/**
	 * @param vehicle_Status the vehicle_Status to set
	 */
	@XmlElement
	public void setVehicle_Status(String vehicle_Status) {
		this.Vehicle_Status = vehicle_Status;
	}

	

	/**
	 * @return the driver_DL_ID
	 */
	public String getDriver_DL_ID() {
		return Driver_DL_ID;
	}

	/**
	 * @param driver_DL_ID the driver_DL_ID to set
	 */
	@XmlElement
	public void setDriver_DL_ID(String driver_DL_ID) {
		this.Driver_DL_ID = driver_DL_ID;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the assigned_Date
	 */
	public String getAssigned_Date() {
		return Assigned_Date;
	}

	/**
	 * @param assigned_Date the assigned_Date to set
	 */
	@XmlElement
	public void setAssigned_Date(String assigned_Date) {
		this.Assigned_Date = assigned_Date;
	}

	/**
	 * @return the route_Type
	 */
	public String getRoute_Type() {
		return Route_Type;
	}

	/**
	 * @param route_Type the route_Type to set
	 */
	@XmlElement
	public void setRoute_Type(String route_Type) {
		this.Route_Type = route_Type;
	}

	/**
	 * @return the source
	 */
	public String getSource() {
		return Source;
	}

	/**
	 * @param source the source to set
	 */
	@XmlElement
	public void setSource(String source) {
		this.Source = source;
	}

	/**
	 * @return the destination
	 */
	public String getDestination() {
		return Destination;
	}

	/**
	 * @param destination the destination to set
	 */
	@XmlElement
	public void setDestination(String destination) {
		this.Destination = destination;
	}

	/**
	 * @return the planned_Stops
	 */
	public String getPlanned_Stops() {
		return Planned_Stops;
	}

	/**
	 * @param planned_Stops the planned_Stops to set
	 */
	@XmlElement
	public void setPlanned_Stops(String planned_Stops) {
		this.Planned_Stops = planned_Stops;
	}
	

}
