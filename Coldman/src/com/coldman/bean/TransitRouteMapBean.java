package com.coldman.bean;

import java.io.Serializable;
import java.util.Vector;

public class TransitRouteMapBean implements Serializable{

	/**
	 * Generated serial version ID
	 */
	private static final long serialVersionUID = -1874474658549948076L;
	
	private String Vehicle_ID;
	private String Date;
	private String Route_Type;
	private String Source;
	private String Destination;
	private Vector<PlanMapsBean> Planned_Stops;
	
	public TransitRouteMapBean(){
		super();
	}
	
	public void init() {
		this.Vehicle_ID = "";
		this.Date = "";
		this.Route_Type = "";
		this.Source = "";
		this.Destination = "";
		Planned_Stops = new Vector<PlanMapsBean>(10);
	}
	
	public void resetPlannedStops() {
        Planned_Stops = new Vector<PlanMapsBean>(10);
    }
	
	public Vector<PlanMapsBean> getPlannedStops() {
        return Planned_Stops;
    }
	
	public void setPlannedStops(Vector<PlanMapsBean> plannedStops) {
        this.Planned_Stops = plannedStops;
    }

    public PlanMapsBean getPlannedStop(int index) {
        return ((PlanMapsBean) Planned_Stops.elementAt(index));
    }
	
	public String toString(){
		String buf = null;
		buf = "Transit Route Map object: \n";
		buf = buf + "Vehicle_ID: " + Vehicle_ID + "\n";
		buf = buf + "Date: " + Date + "\n";
		buf = buf + "Route_Type: " + Route_Type + "\n";
		buf = buf + "Source: " + Source + "\n";
		PlanMapsBean planMap = new PlanMapsBean();
        if (Planned_Stops != null) {
            for (int i = 0; i < Planned_Stops.size(); i++) {
                planMap = Planned_Stops.elementAt(i);
                buf = buf + planMap.toString();
            }
        }
		return buf;
	}

	/**
	 * @return the vehicle_ID
	 */
	public String getVehicle_ID() {
		return Vehicle_ID;
	}

	/**
	 * @param vehicle_ID the vehicle_ID to set
	 */
	public void setVehicle_ID(String vehicle_ID) {
		Vehicle_ID = vehicle_ID;
	}

	/**
	 * @return the date
	 */
	public String getDate() {
		return Date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		Date = date;
	}

	/**
	 * @return the route_Type
	 */
	public String getRoute_Type() {
		return Route_Type;
	}

	/**
	 * @param route_Type the route_Type to set
	 */
	public void setRoute_Type(String route_Type) {
		Route_Type = route_Type;
	}

	/**
	 * @return the source
	 */
	public String getSource() {
		return Source;
	}

	/**
	 * @param source the source to set
	 */
	public void setSource(String source) {
		Source = source;
	}

	/**
	 * @return the destination
	 */
	public String getDestination() {
		return Destination;
	}

	/**
	 * @param destination the destination to set
	 */
	public void setDestination(String destination) {
		Destination = destination;
	}

	/**
	 * @return the planned_Stops
	 */
	public Vector<PlanMapsBean> getPlanned_Stops() {
		return Planned_Stops;
	}

	/**
	 * @param planned_Stops the planned_Stops to set
	 */
	public void setPlanned_Stops(Vector<PlanMapsBean> planned_Stops) {
		Planned_Stops = planned_Stops;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
