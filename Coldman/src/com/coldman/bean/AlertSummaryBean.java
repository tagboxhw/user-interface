package com.coldman.bean;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "alertSummaryBean")
public class AlertSummaryBean implements Serializable{

	/**
	 * Generated serial version ID
	 */
	private static final long serialVersionUID = -1874474658549948076L;
	private int Resolution;
	private String sLocationName;
	private String sZoneName;
	private int sAlertSeverity;
	private int sCount;
	
	public AlertSummaryBean(){
		super();
	}
	
	public void init() {
		this.Resolution = 0;
		this.sLocationName = "";
		this.sZoneName = "";
		this.sAlertSeverity = 0;
		this.sCount = 0;
		
	}
	
	public String toString(){
		String buf = null;
		buf = "Alert Summary object: \n";
		buf = buf + "Resolution: " + Resolution + "\n";
		buf = buf + "sLocationName: " + sLocationName + "\n";
		buf = buf + "sZoneName: " + sZoneName + "\n";
		buf = buf + "sAlertSeverity: " + sAlertSeverity + "\n";
		buf = buf + "sCount: " + sCount + "\n";
		
		return buf;
	}

	public String getsLocationName() {
		return sLocationName;
	}

	public void setsLocationName(String sLocationName) {
		sLocationName = sLocationName;
	}

	public int getResolution() {
		return Resolution;
	}

	@XmlElement
	public void setResolution(int Resolution) {
		this.Resolution = Resolution;
	}

	public String getsZoneName() {
		return sZoneName;
	}
	
	@XmlElement
	public void setsZoneName(String sZoneName) {
		this.sZoneName = sZoneName;
	}

	public int getsAlertSeverity() {
		return sAlertSeverity;
	}
	
	@XmlElement
	public void setsAlertSeverity(int sAlertSeverity) {
		this.sAlertSeverity = sAlertSeverity;
	}

	public int getsCount() {
		return sCount;
	}

	@XmlElement
	public void setsCount(int sCount) {
		this.sCount = sCount;
	}


}
