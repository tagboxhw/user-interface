package com.coldman.bean;

import java.io.Serializable;

public class TransitHistoryMapBean implements Serializable{

	/**
	 * Generated serial version ID
	 */
	private static final long serialVersionUID = -1874474658549948076L;
	
	private String Vehicle_ID;
	private String Assigned_Date;
	private String Driver_DL_ID;
	private String Route_Type;
	private String Source;
	private String Destination;
	private String Planned_Stops;
	
	public TransitHistoryMapBean(){
		super();
	}
	
	public void init() {
		this.Vehicle_ID = "";
		this.Assigned_Date = "";
		this.Driver_DL_ID = "";
		this.Route_Type = "";
		this.Source = "";
		this.Destination ="";
		this.Planned_Stops = "";
	}
	
	public String toString(){
		String buf = null;
		buf = "Transit History Map object: \n";
		buf = buf + "Vehicle_ID: " + Vehicle_ID + "\n";
		buf = buf + "Assigned_Date: " + Assigned_Date + "\n";
		buf = buf + "Driver_DL_ID: " + Driver_DL_ID + "\n";
		buf = buf + "Route_Type: " + Route_Type + "\n";
		buf = buf + "Source: " + Source + "\n";
		buf = buf + "Destination: " + Destination + "\n";
		buf = buf + "Planned_Stops: " + Planned_Stops + "\n";
		return buf;
	}

	/**
	 * @return the vehicle_ID
	 */
	public String getVehicle_ID() {
		return Vehicle_ID;
	}

	/**
	 * @param vehicle_ID the vehicle_ID to set
	 */
	public void setVehicle_ID(String vehicle_ID) {
		Vehicle_ID = vehicle_ID;
	}


	/**
	 * @return the date
	 */
	public String getAssignedDate() {
		return Assigned_Date;
	}

	/**
	 * @param date the date to set
	 */
	public void setAssignedDate(String date) {
		Assigned_Date = date;
	}

	/**
	 * @return the driver_DL_ID
	 */
	public String getDriver_DL_ID() {
		return Driver_DL_ID;
	}

	/**
	 * @param driver_DL_ID the driver_DL_ID to set
	 */
	public void setDriver_DL_ID(String driver_DL_ID) {
		Driver_DL_ID = driver_DL_ID;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the assigned_Date
	 */
	public String getAssigned_Date() {
		return Assigned_Date;
	}

	/**
	 * @param assigned_Date the assigned_Date to set
	 */
	public void setAssigned_Date(String assigned_Date) {
		Assigned_Date = assigned_Date;
	}

	/**
	 * @return the route_Type
	 */
	public String getRoute_Type() {
		return Route_Type;
	}

	/**
	 * @param route_Type the route_Type to set
	 */
	public void setRoute_Type(String route_Type) {
		Route_Type = route_Type;
	}

	/**
	 * @return the source
	 */
	public String getSource() {
		return Source;
	}

	/**
	 * @param source the source to set
	 */
	public void setSource(String source) {
		Source = source;
	}

	/**
	 * @return the destination
	 */
	public String getDestination() {
		return Destination;
	}

	/**
	 * @param destination the destination to set
	 */
	public void setDestination(String destination) {
		Destination = destination;
	}

	/**
	 * @return the planned_Stops
	 */
	public String getPlanned_Stops() {
		return Planned_Stops;
	}

	/**
	 * @param planned_Stops the planned_Stops to set
	 */
	public void setPlanned_Stops(String planned_Stops) {
		Planned_Stops = planned_Stops;
	}
	

}
