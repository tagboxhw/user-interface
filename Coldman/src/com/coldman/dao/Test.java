package com.coldman.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;
import java.sql.Types;

class Test {
	public static void main(String args[]) {
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			System.err.println(e);
			System.exit(-1);
		}
		try {
			// open connection to database
			Connection connection = DriverManager.getConnection(
					// "jdbc:postgresql://dbhost:port/dbname", "user",
					// "dbpass");
					"jdbc:postgresql://104.215.248.40:5432/somesh", "somesh",
					"Tagbox_123456");

			if (connection != null) {
				System.out.println("You made it!");
				connection.setAutoCommit(false);
			} else {
				System.out.println("Failed to make connection!");
			}

			// build query, here we get info about all databases"
			// String query = "select * from eng.gateway_log limit 10";

			// execute query
			// Statement statement = connection.createStatement ();
			// ResultSet rs = statement.executeQuery (query);

			String call = "{? = call tel.alert_today() }";
			CallableStatement pstmt = connection.prepareCall(call);
			pstmt.registerOutParameter(1, Types.OTHER);
			// pstmt.setString(2,"Unassigned");
			// pstmt.setString(3,"3/17/2017 21:55");
			// pstmt.setString(4,"LO-DMO-000005");
			// pstmt.setString(5,"LO-DMO-000001");
			// pstmt.setString(6,"LO-DMO-000001");
			// pstmt.setString(7,"KA03-KH5282");
			// ResultSet rs = pstmt.executeQuery();
			pstmt.execute();
			ResultSet rs = (ResultSet) pstmt.getObject(1);
			if (rs.next()) {
				// first result set returned
				Object o = rs.getObject(1);
				while(rs.next()){
					System.out.println("PostgreSQL Query result: "
						+ rs.getString(1));
				}
			}
		} catch (Exception e) {
			System.err.println(e);
			e.printStackTrace();
			// System.exit (-1);

		}
	}

}