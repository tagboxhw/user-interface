package com.coldman.dao;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.coldman.bean.AlertWorkflowBean;
import com.coldman.bean.ParameterSummaryBean;
import com.coldman.bean.TemperatureBean;
import com.coldman.util.Constants;

public class AlertsReportDao {
	private Client client; 
	static final Logger logger = LoggerFactory.getLogger(AlertsReportDao.class);
	
	private void init(){ 
		this.client = ClientBuilder.newClient(); 
	}  
	
	public Collection<AlertWorkflowBean> getAlertsReport() {
		init();
		GenericType<Collection<AlertWorkflowBean>> clmBean1 = new GenericType<Collection<AlertWorkflowBean>>(){};
		Collection<AlertWorkflowBean> col = client 
		         .target(Constants.REST_SERVICE_URL + "AlertsWorkflowService/alertsreport") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(clmBean1);
		return col;
	}
	
	public Collection<TemperatureBean> getHistoricalTemperatureData(String sStartTime, String sEndTime, String sSubZoneId){
		Collection<TemperatureBean>  tvmBean = null;
		try {
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		df.setTimeZone(java.util.TimeZone.getTimeZone("UTC"));
		sdf.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
		Date dateObj = sdf.parse(sStartTime);
		sStartTime = df.format(dateObj);
		dateObj = sdf.parse(sEndTime);
		sEndTime = df.format(dateObj);
		init();
		GenericType<Collection<TemperatureBean>> tvmBean1 = new GenericType<Collection<TemperatureBean>>(){};
		WebTarget target = client.target(Constants.REST_SERVICE_URL + "AlertsWorkflowService/history").path(sStartTime).path(sEndTime).path(sSubZoneId);
		tvmBean  = target.request().get(tvmBean1);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return tvmBean; 
	   } 
}
