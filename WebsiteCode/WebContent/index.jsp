<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
	<html lang="en">
		<head>
			<link rel="shortcut icon" type="image/x-icon" href="logo.ico" />
			<!--Start of Zendesk Chat Script-->
			<script type="text/javascript">
			window.$zopim||(function(d,s){var z=$zopim=function(c){
			z._.push(c)},$=z.s=
			d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
			_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
			$.src='https://v2.zopim.com/?4tpmEWQxvEs45A3mrIzz2OBEDY608KZH';z.t=+new Date;$.
			type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
			</script>
			<!--End of Zendesk Chat Script-->
			<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<meta name="description" content="">
			<meta name="author" content="">
			<title>TagBox - Making Cold Chains Reliable</title>
			<!-- Bootstrap Core CSS -->
			<link href="css/bootstrap.min.css" rel="stylesheet">
			<!-- Custom CSS -->
			<link href="css/agency.css" rel="stylesheet">
			<!-- Custom Fonts -->
			<link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
			<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
			<link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
			<link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
			<link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>
			<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
			<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
			<!--[if lt IE 9]>
		        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		    <![endif]-->
		    <script src="js/jquery-2.2.3.min.js"></script>
		    <script src="js/tagbox.js"></script>
		</head>
		<body id="page-top" class="index">
		
		<!-- Navigation -->
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="container">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header page-scroll">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span> 
						<span class="icon-bar"></span> <span class="icon-bar"></span> 
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand page-scroll" href="#page-top">
						<div class="valign">
							<img src="img/Logo1.png" class="logo-class">&nbsp;&nbsp;TAGBOX SOLUTIONS
						</div>
					</a>
				</div>
				
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-right">
						<li class="hidden"><a href="#page-top"></a></li>
						<li><a class="page-scroll" href="#page-top">Home</a></li>
						<li><a class="page-scroll" href="#product">Solution</a></li>
						<li><a class="page-scroll" href="#portfolio">Applications</a>
						</li>
						<!--
		    			<li><a class="page-scroll" href="#about">About</a></li>
		    			-->
						<li><a class="page-scroll" href="#team">Team</a></li>
						<li><a class="page-scroll" href="#contact">Contact</a></li>
					</ul>
				</div>
				<!-- /.navbar-collapse -->
			</div>
			<!-- /.container-fluid -->
		</nav>
					
		<!-- Header -->
		<header class="background-img">
        		<div class="container">
    		<div class="row">
    			<div class="col-md-12">
    			<div class="intro-text-heading">
    			<br>
                <div class="intro-lead-in"><i>Making Cold Chains Reliable</i></div>
                <h4 class="service-heading">Monitor, analyze and control your Cold Chain</h4>
            </div>
            
					<div id="carousel-example-generic" class="carousel slide"
						data-ride="carousel" data-interval="6000">
						<!-- Indicators -->
						<ol class="carousel-indicators">
							<li data-target="#carousel-example-generic" data-slide-to="0"
								class="active"></li>
							<li data-target="#carousel-example-generic" data-slide-to="1"></li>
							<li data-target="#carousel-example-generic" data-slide-to="2"></li>
							<li data-target="#carousel-example-generic" data-slide-to="3"></li>
						</ol>

						<!-- Wrapper for slides -->
						<div class="carousel-inner" role="listbox">
						
						<div class="item active">
							<div class="row">
        						<div class="col-md-offset-2 col-xs-offset-2 col-md-3 col-sm-6 col-xs-12 row-eq-height">
								<div class="well well-sm well-dashboard3 well-one-line">
									<div class="transbox row-eq-height">
										<h4>&nbsp;&nbsp;End to end visibility</h4>
									</div>
									</div>
								</div>
        						<div class="col-md-offset-2 col-xs-offset-2 col-md-3 col-sm-6 col-xs-8 row-eq-height">
								<div class="well well-sm well-dashboard3 well-two-line">
									<div class="transbox row-eq-height">
										 <h4>Reduce product spoilage</h4>
									</div>
								</div>
								
              </div>
              </div>
              </div>
              <div class="item">
							<div class="row">
								<div class="col-md-offset-2 col-xs-offset-2 col-md-3 col-sm-12 col-xs-12 row-eq-height">
								<div class="well well-sm well-dashboard1 well-two-line">
									<div class="transbox row-eq-height">
										<h4>Seamless ERP integration</h4>
                
									</div>
									</div>
								</div>
								<div class="col-md-offset-2 col-xs-offset-2 col-md-3 col-sm-12 col-xs-8 row-eq-height">
								<div class="well well-sm well-dashboard1 well-two-line">
									<div class="transbox row-eq-height">
										 <h4>Meet compliance requirements</h4>
									</div>
									</div>
								</div>
								
              </div>
              </div>
					<div class="item">
							<div class="row">
								<div class="col-md-offset-2 col-xs-offset-2 col-md-3 col-sm-12 col-xs-12 row-eq-height">
								<div class="well well-dashboard2 well-two-line">
									<div class="transbox row-eq-height text-center">
										<h4>Prevent refrigeration failure</h4>
									</div>
									</div>
								</div>
								<div class="col-md-offset-2 col-xs-offset-2 col-md-3 col-sm-12 col-xs-8 row-eq-height">
								<div class="well well-sm well-dashboard2 well-one-line">
									<div class="transbox row-eq-height text-center">
										 <h4>&nbsp;&nbsp;&nbsp;&nbsp;Cut energy costs</h4>
									</div>
									</div>
								</div>
								
              </div>
              </div>
              <div class="item">
							<div class="row">
								<div class="col-md-offset-2 col-xs-offset-2 col-md-3 col-sm-12 col-xs-12 row-eq-height">
								<div class="well well-sm well-dashboard4 well-two-line">
									<div class="transbox row-eq-height">
										<h4>Prevent theft and pilferage</h4>
                
									</div>
									</div>
								</div>
								<div class="col-md-offset-2 col-xs-offset-2 col-md-3 col-sm-12 col-xs-12 row-eq-height">
								<div class="well well-sm well-dashboard4 well-two-line">
									<div class="transbox row-eq-height">
										 <h4>Decrease logistics and insurance costs</h4>
									</div>
									</div>
								</div>
								
              </div>
              </div>		
<br><br>
						<!-- Controls -->
						<a class="left carousel-control" href="#carousel-example-generic"
							role="button" data-slide="prev"> <span
							class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						</a> <a class="right carousel-control"
							href="#carousel-example-generic" role="button" data-slide="next">
							<span class="glyphicon glyphicon-chevron-right"
							aria-hidden="true"></span> <span class="sr-only">Next</span>
						</a>
					</div>
				</div>
    		</div>
    	</div>
        		</header>
        		
        		
		
		<!-- Solution Section -->
		<section id="product" class="bg-light-gray">
			<div class="container">
			<div class="row">
				<div class="col-lg-12 text-center">
					<h2 class="section-heading add-padding">Solution</h2>
					<br>
				</div>
			</div>
				<div class="row">
					<div class="col-md-12 text-center">
						<div class="text-center">
						<img src="img/Intersection.jpg" class="image-resize">
					</div>
					</div>
				</div>
				<br><br>
				<div class="row">
					<div class="col-md-4">
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-2 col-sm-12 col-xs-12 col-small">
									<span class="fa-stack fa-2x"> <i class="fa fa-circle fa-stack-2x text-primary"></i> <i class="fa fa-tag fa-stack-1x fa-inverse"></i></span>
								</div>
								<div class="col-md-9">
									<br><b class="title-no-text-transform">IoT BASED MONITORING</b>
								</div>
								<ul class="ul-class text-muted">
   						<li class="li-class">-&nbsp;Monitor Cold Chain with Tag360 sensors</li>
   						<li class="li-class">-&nbsp;Central Monitoring Console</li>
   						<li class="li-class">-&nbsp;Smart Notifications for breaches and events</li>
   					</ul>
							</div>
								<a class="btn btn-link btn-sm pull-right" href="iotColdChainMonitoring.jsp">Learn More... <i class="fa fa-angle-right"></i></a>
						</div>
					</div>
					</div>
					<div class="col-md-4">
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-2 col-sm-12 col-xs-12 col-small">
									<span class="fa-stack fa-2x"> <i class="fa fa-circle fa-stack-2x text-primary"></i> <i class="fa fa-line-chart fa-stack-1x fa-inverse"></i></span>
								</div>
								<div class="col-md-9">
									<br> <b class="title">Advanced Analytics</b>
								</div>
								<ul class="ul-class text-muted">
   						<li class="li-class">-&nbsp;Predict and prevent unfavorable events</li>
   						<li class="li-class">-&nbsp;Optimize reefer fleet and routes</li>
   						<li class="li-class">-&nbsp;Manage inventory</li>
   					</ul>
							</div>
							
								<a class="btn btn-link btn-sm pull-right" href="AdvancedAnalytics.jsp">Learn More... <i class="fa fa-angle-right"></i></a>
						</div>
					</div>
					</div>
					<div class="col-md-4">
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-2 col-sm-12 col-xs-12 col-small">
									<span class="fa-stack fa-2x"> <i class="fa fa-circle fa-stack-2x text-primary"></i> <i class="fa fa-cogs fa-stack-1x fa-inverse"></i></span>
								</div>
								<div class="col-md-9">
									<Br><b class="title">Automation and Control</b>
								</div>
								<ul class="ul-class text-muted">
   						<li class="li-class">-&nbsp;Automate operational processes</li>
   						<li class="li-class">-&nbsp;Control refrigeration units</li>
   						<li class="li-class">-&nbsp;Augment facility, fleet and field personnel</li>
   						
   					</ul>
							</div>
							
								<a class="btn btn-link btn-sm pull-right" href="MachineAndProcess.jsp">Learn More... <i class="fa fa-angle-right"></i></a>
							<br>
							<br>
							<br>
							<br>
						</div>
					</div>
					</div>
				
				
			</div>
		</div>
	</section>

	<!-- Portfolio Grid Section -->
	<section id="portfolio" class="bg-gray">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 text-center">
					<h2 class="section-heading add-padding">Applications</h2><br>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3 col-sm-6 portfolio-item">
					<div class="portfolio-caption">
					<img src="img/portfolio/Pharma.jpg" class="img-responsive height-not-auto" alt="">
					</div>
						
					<div class="portfolio-caption div-height box">
						<b class="title">Pharmaceutical & Biotech</b><br>
						<p class="text-muted">Manufacturers can retain efficacy of drugs and vaccines by maintaining appropriate ambient or box level temperature environments across the cold chain</p>
						<!-- <a class="btn btn-link btn-sm pull-right">Learn More... <i class="fa fa-angle-right"></i></a> -->
					</div>
				</div>
				<div class="col-md-3 col-sm-6 portfolio-item">
				<div class="portfolio-caption">
						<img src="img/P1-2.jpg" class="img-responsive height-not-auto" alt="">
						</div>
					<div class="portfolio-caption div-height box">
						<b class="title">Perishable and Processed Food</b><br>
						<p class="text-muted">Horticulture, Meats and Packaged food companies can prevent significant spoilage losses by monitoring and controlling temperature, humidity and gas</p>
						<!-- <a class="btn btn-link btn-sm pull-right">Learn More... <i class="fa fa-angle-right"></i></a> -->
					</div>
				</div>
				<div class="col-md-3 col-sm-6 portfolio-item">
				<div class="portfolio-caption">
						<img src="img/P1-3.jpg" class="img-responsive height-not-auto" alt="">
						</div>
					<div class="portfolio-caption div-height box">
						<b class="title">Milk and Dairy Products</b><br>
						<p class="text-muted">Complex dairy cold chains can benefit significantly from temperature monitoring and FEFO based inventory management</p>
						<!-- <a class="btn btn-link btn-sm pull-right">Learn More... <i class="fa fa-angle-right"></i></a> -->
					</div>
				</div>
				<div class="col-md-3 col-sm-6 portfolio-item">
				<div class="portfolio-caption">
						<img src="img/P1-4.jpg" class="img-responsive height-not-auto" alt="">
						</div>
					<div class="portfolio-caption div-height box">
						<b class="title">End Point Refrigeration</b><br>
						<p class="text-muted">Retail stores, Hospitals and Restaurant kitchens can achieve substantial energy savings by modulating refrigeration units' power through operational inputs</p>
						<!-- <a class="btn btn-link btn-sm pull-right">Learn More... <i class="fa fa-angle-right"></i></a> -->
					</div>
				</div>
				<!--
		<div class="col-md-4 col-sm-6 portfolio-item">
                    <a href="#portfolioModal4" class="portfolio-link" data-toggle="modal">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="img/portfolio/golden.png" class="img-responsive" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4>Golden</h4>
                        <p class="text-muted">Website Design</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 portfolio-item">
                    <a href="#portfolioModal5" class="portfolio-link" data-toggle="modal">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="img/portfolio/escape.png" class="img-responsive" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4>Escape</h4>
                        <p class="text-muted">Website Design</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 portfolio-item">
                    <a href="#portfolioModal6" class="portfolio-link" data-toggle="modal">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="img/portfolio/dreams.png" class="img-responsive" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4>Dreams</h4>
                        <p class="text-muted">Website Design</p>
                    </div>
                </div>
		-->

			</div>
		</div>
	</section>

	<!-- About Section 
    <section id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">About</h2>
                    <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <ul class="timeline">
                        <li>
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/about/1.jpg" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4>2009-2011</h4>
                                    <h4 class="subheading">Our Humble Beginnings</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!</p>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/about/2.jpg" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4>March 2011</h4>
                                    <h4 class="subheading">An Agency is Born</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/about/3.jpg" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4>December 2012</h4>
                                    <h4 class="subheading">Transition to Full Service</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!</p>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/about/4.jpg" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4>July 2014</h4>
                                    <h4 class="subheading">Phase Two Expansion</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!</p>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <div class="timeline-image">
                                <h4>Be Part
                                    <br>Of Our
                                    <br>Story!</h4>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

	-->

	<!-- Team Section -->
	<section id="team" class="bg-light-gray">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 text-center">
					<h2 class="section-heading add-padding">Team Tagbox</h2>
				</div>
			</div>

			<!-- 
            <div class="row">
                <div class="col-sm-4">
                    <div class="team-member">
                        <img src="img/Adarsh.jpg" class="img-responsive img-circle" alt="">
                        <h4>Kay Garland</h4>
                        <p class="text-muted">Lead Designer</p>
                        <ul class="list-inline social-buttons">
                            <li><a href="#"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="team-member">
                        <img src="img/Somesh.jpg" class="img-responsive img-circle" alt="">
                        <h4>Larry Parker</h4>
                        <p class="text-muted">Lead Marketer</p>
                        <ul class="list-inline social-buttons">
                            <li><a href="#"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="team-member">
                        <img src="img/Saumitra.jpg" class="img-responsive img-circle" alt="">
                        <h4>Diana Pertersen</h4>
                        <p class="text-muted">Lead Developer</p>
                        <ul class="list-inline social-buttons">
                            <li><a href="#"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
-->
			<div class="row">
				<div class="col-md-4">
				<div class="team-member box">
          <img class="img-responsive img-thumbnail img-size-team" src="img/Adarsh.jpg">
            <h3>Adarsh Kumar <small><br>Chief Executive Officer</small></h3><a href="https://www.linkedin.com/in/adarsh-kumar-4759baa/" data-toggle="tooltip" data-placement="top" title="LinkedIn"><i class="fa fa-linkedin-square fa-2x"></i></a>
            <p class="text-muted">Adarsh has over 12 years of experience in providing Advanced Analytics solutions to Fortune 500 companies and business development. A MICA alumnus, he was a part of the core leadership team at Mu Sigma before starting TagBox.</p>
            
            </div>
        </div>
        <div class="col-md-4">
        <div class="team-member box">
          <img class="img-responsive img-thumbnail img-size-team" src="img/Saumitra.jpg">
            <h3>Saumitra Singh <small><br>Chief Product Officer</small></h3> <a href="https://www.linkedin.com/in/saumitra-singh-3139585/" data-toggle="tooltip" data-placement="top" title="LinkedIn"><i class="fa fa-linkedin-square fa-2x"></i></a>
            <p class="text-muted">Saumitra, an IIT Bombay alumnus, is a Product leader with focus on VLSI and Energy IoT. Over the last 12 years, he has held leadership positions in startups including Cosmic Circuits and Innorel Systems. Saumitra holds 9 US patents.</p>
          
            </div>
        </div>
        <div class="col-md-4">
        <div class="team-member box">
          <img class="img-responsive img-thumbnail img-size-team" src="img/Sameer.jpg">
            <h3>Sameer Singh <small><br>VP, Engineering</small></h3><a href="https://www.linkedin.com/in/sameer-singh-840125106/" data-toggle="tooltip" data-placement="top" title="LinkedIn"><i class="fa fa-linkedin-square fa-2x"></i></a>
            <p class="text-muted">Sameer is passionate about research in Communication and Microcontroller AFEs. An IIT Madras alumnus, he has led R&D teams in Texas Instruments, Cadence and Cosmic Circuits. Sameer has also authored 2 IEEE publications.</p>
           </div>
        </div>
			</div>
			<div class="row">
        <div class="col-md-4">
        <div class="team-member box">
          <img class="img-responsive img-thumbnail img-size-team" src="img/Suhas.jpg">
            <h3>Suhas Keshavamurthy <small><br>Cross-Platform Development</small></h3> <a href="https://www.linkedin.com/in/suhas-keshavamurthy-00737044/" data-toggle="tooltip" data-placement="top" title="LinkedIn"><i class="fa fa-linkedin-square fa-2x"></i></a>
            <p class="text-muted">Suhas is a NIT-K alumnus and started his own e-bike venture called Wizark under NIT-K incubation. He has 4 years of experience in cross platform software development, with the last stint being at Sandisk.</p>
          
           </div>
        </div>
        <div class="col-md-4">
        <div class="team-member box">
          <img class="img-responsive img-thumbnail img-size-team" src="img/Somesh.jpg" style="width:120px;height:150px">
            <h3>Somesh Rai <small><br>Analytics</small></h3><a href=" https://in.linkedin.com/in/somesh-rai-18495624" data-toggle="tooltip" data-placement="top" title="LinkedIn"><i class="fa fa-linkedin-square fa-2x"></i></a>
            <p class="text-muted">Somesh is a Certified Decision Scientist and loves exploring new machine learning algorithms to solve Supply Chain problems. He has spent the last 3 years with Flipkart and Mu Sigma and worked on various supply chain initiatives</p>
            
            </div>
        </div>
        <div class="col-md-4">
        <div class="team-member box">
          <img class="img-responsive img-thumbnail img-size-team" src="img/KK5.JPG">
            <h3>Krishnaveni Kumbaji <small><br>App Development</small></h3><a href="http://www.linkedin.com/in/krishnaveni-kumbaji-30a41032"
								target="_blank" data-toggle="tooltip" data-placement="top" title="LinkedIn"><i class="fa fa-linkedin-square fa-2x"></i></a>
            <p class="text-muted">Krishnaveni is a seasoned web and mobile application expert. With over 12 years of experience with the likes of United Airlines and IBM, she is the backbone of TagBox's web and mobile platforms.</p>
            
            </div>
        </div>
      </div>
		</div>
	</section>


	<!-- Clients Aside -->
	<!--
    <aside class="clients">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <a href="#">
                        <img src="img/logos/envato.jpg" class="img-responsive img-centered" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-sm-6">
                    <a href="#">
                        <img src="img/logos/designmodo.jpg" class="img-responsive img-centered" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-sm-6">
                    <a href="#">
                        <img src="img/logos/themeforest.jpg" class="img-responsive img-centered" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-sm-6">
                    <a href="#">
                        <img src="img/logos/creative-market.jpg" class="img-responsive img-centered" alt="">
                    </a>
                </div>
            </div>
        </div>
    </aside>
    -->

	<!-- Contact Section -->
	<section id="contact">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 text-center">
					<h2 class="section-heading add-padding">Contact Us</h2><br>
				</div>
			</div>
			<div class="row">
			<div class="col-md-6">
			<div class="row">
				<div class="col-md-12">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3887.883939849356!2d77.71344971430442!3d12.979274318222878!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMTLCsDU4JzQ1LjQiTiA3N8KwNDInNTYuMyJF!5e0!3m2!1sen!2sin!4v1491237507910" width="100%" height="320" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
			</div>
			<div class="row">
			<div class="col-md-12" style="color:#3c8dbc">
                <p><i class="fa fa-map-marker"></i> Address:<br>
                    Startup Studio, Building 2 SAP
					138 EPIP Zone, Whitefield, Bangalore 560066<br>

                </p>
                <p>
                
                <i class="fa fa-phone"></i> Phone: <br>
                    +91 95359 64140, +91 98456 60136, +91 96206 47577
                    </p>
                <p><i class="fa fa-envelope-o"></i> Email: <a href="mailto:contactus@tagbox.in">contactus@tagbox.in</a>
                </p>
                
        </div>
			</div>
		</div>
		<div class="col-md-6">
				<form name="sentMessage" id="contactForm" novalidate>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<input type="text" class="form-control"
										placeholder="Your Name *" id="name" required
										data-validation-required-message="Please enter your name.">
									<div id="name_error" class="help-block text-danger"></div>
								</div>
								</div>
								</div>
								<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<input type="email" class="form-control"
										placeholder="Your Email *" id="email" required
										data-validation-required-message="Please enter your email address.">
									<div id="email_error" class="help-block text-danger"></div>
								</div>
								</div>
								</div>
								<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<input type="tel" class="form-control" 
										placeholder="Your Phone *" id="phone" required
										data-validation-required-message="Please enter your phone number.">
									<div id="phone_error" class="help-block text-danger"></div>
								</div>
							</div>
							</div>
							<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<textarea class="form-control" placeholder="Your Message *"
										id="message" required
										data-validation-required-message="Please enter a message."></textarea>
									<div id="message_error" class="help-block text-danger"></div>
								</div>
							</div>
							</div>
							<div class="clearfix"></div>
							<div class="row">
							<div class="col-lg-12 text-center">
								<div id="success"></div>
								<button type="submit" class="btn btn-xl">Send Message</button><br>
								<div id="errormessage" class="text-muted-lesser alert"></div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>

	<footer>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<span class="copyright pull-left">Copyright &copy; TagBox 2017</span><a class="copyright pull-right" href="#" data-toggle="modal" data-target="#myModal">&nbsp;&nbsp;Privacy Policy</a>&nbsp;&nbsp;<a class="copyright pull-right" href="#" data-toggle="modal" data-target="#myModal1">Terms of Use&nbsp;&nbsp;</a>
				</div>
				<!-- <div class="col-md-4">
					<ul class="list-inline social-buttons">
						<li><a href="#"><i class="fa fa-twitter"></i></a></li>
						<li><a href="#"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
					</ul>
				</div>
				<div class="col-md-4">
					<ul class="list-inline quicklinks">
						<li><a href="#">Privacy Policy</a></li>
						<li><a href="#">Terms of Use</a></li>
					</ul>
				</div> -->
			</div>
		</div>
	</footer>
<div class="modal fade" tabindex="-1" role="dialog" id="myModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">TagBox Privacy Policy</h4>
      </div>
      <div class="modal-body">
        <p><%@include file="PrivacyPolicy.jsp" %></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" tabindex="-1" role="dialog" id="myModal1">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">TagBox - Terms of Use</h4>
      </div>
      <div class="modal-body">
        <p><%@include file="TermsOfUsage.jsp" %></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


	<!-- jQuery -->
	<script src="js/jquery.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="js/bootstrap.min.js"></script>

	<!-- Plugin JavaScript -->
	<script
		src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
	<script src="js/classie.js"></script>
	<script src="js/cbpAnimatedHeader.js"></script>

	<!-- Contact Form JavaScript -->
	<script src="js/jqBootstrapValidation.js"></script>
	<script src="js/contact_me.js"></script>

	<!-- Custom Theme JavaScript -->
	<script src="js/agency.js"></script>
<script src="js/lightbox.js"></script>
</body>

</html>
