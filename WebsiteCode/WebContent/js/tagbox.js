function insertMessage(){
	var b = false;
	$("#name_error").hide();
	$('#errormessage').hide();
	if($('#name').val() == null || $('#name').val() == ""){
		$("#name_error").html("Please enter a valid Name").show();
		return;
	}
	var posting = $.post("jsp/ContactDetails.jsp",
				{
					Name:$('#name').val(),
					Email:$('#email').val(),
					Phone:$('#phone').val(),
					Message:$('#message').val(),
					query:"insertMessage"
				},
				function(data,status){
					//alert(data);
					xmlDoc = loadXMLString(data);
					if($(xmlDoc).find("rows").length == 0) {
						
						return;
					}
					
					$(xmlDoc).find("row").each(function(){
						$("#errormessage").html("Successfully sent the message.").show();
						$('.alert').fadeIn(500);
				           setTimeout( "$('.alert').fadeOut(1500);",3000 );
					});
					
				});
	
}


function loadXMLString(txt) 
{
	if (window.DOMParser)
	{
		parser=new DOMParser();
		xmlDoc=parser.parseFromString(txt,"text/xml");
	}
	else // Internet Explorer
	{
		xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
		xmlDoc.async=false;
		xmlDoc.loadXML(txt); 
	}
	return xmlDoc;
}