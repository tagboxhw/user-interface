<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>TagBox - Terms of Usage</title>
</head>
<body>


<p>Welcome to our website. If you continue to browse and use this website, you are agreeing to comply with and be bound by the following terms and conditions of use, which together with our privacy policy govern Tagbox Solution Private Limited's relationship with you in relation to this website. These terms applies to our website 'tagbox.in' and any subdomain associated with it. <b>If you disagree with any part of these terms and conditions, please do not use our website.</b></p>
<p>The term <b>'TagBox' or 'us' or 'we'</b> refers to the owner of the website whose registered office is C404 Nagarjuna Maple Heights, Sy 182-185 B Narayanpura, DV Nagar Bangalore North 560048 KA. Our company registration number is U72900KA2016PTC097344 registered in Bangalore, Karnataka. The term <b>'you'</b> refers to the user or viewer of our website.</p>
<br><br>

<h3>The use of this website is subject to the following terms of use:</h3>

<br><br>1.	The content of the pages of <b>'tagbox.in'</b>, hereafter referred to as <b>'website' </b>is for your general information and use only. It is subject to change without notice.
<br><br>2.	The content of the pages of <b>'portal.tagbox.in/[company name]'</b>, hereafter referred to as <b>'portal'</b>, is a specifically designed page for the purpose of delivering services
<br><br>3.	This website and portal use cookies to monitor browsing preferences and store data necessary for better operation. If you do allow cookies to be used, the following personal information may be stored in the browser.
<br><br>4.	Neither of our website or portal will ever ask for payment related information, please check the source in a case this happens.
<br><br>5.	Neither we nor any third parties provide any guarantee as to the accuracy, timeliness, performance, completeness or suitability of the information if technical specification needed and mentioned in the manuals are not met. You acknowledge that such information and materials may contain inaccuracies or errors and we expressly exclude liability for any such inaccuracies or errors to the fullest extent permitted by law.
<br><br>6.	Your use of any information or materials on the website is entirely at your own risk, for which we shall not be liable. It shall be our responsibility to ensure that any products, services or information available through the portal meet your specific requirements and ensure highest delivery standards.
<br><br>7.	The website contains material which is owned by or licensed to us. This material includes, but is not limited to, the design, layout, look, appearance and graphics. Reproduction is prohibited other than in accordance with the copyright notice, which forms part of these terms and conditions.
<br><br>8.	All trademarks reproduced in this website which are not the property of, or licensed to, the operator is acknowledged on the website.
<br><br>9.	Unauthorised use of this website may give rise to a claim for damages and/or be a criminal offence.
<br><br>10.	From time to time this website may also include links to other websites. These links are provided for your convenience to provide further information. They do not signify that we endorse the website(s). We have no responsibility for the content of the linked website(s).
<br><br>11.	Your use of this website and any dispute arising out of such use of the website is subject to the Bangalore Jurisdiction only
<br><br>
For any additional queries or information please write to us at <b>contactus@tagbox.in</b>


</body>
</html>