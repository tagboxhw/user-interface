package com.tagbox.website.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Vector;

public class ContactBean implements Serializable{

	/**
	 * Generated serial version ID
	 */
	private static final long serialVersionUID = -1874474658549948076L;
	
	private String sEmail;
	private String sName;
	private String sPhone;
	private String sMessage;
	
	/**
	 * @return the location_type
	 */
	public String getName() {
		return sName;
	}

	/**
	 * @param location_type the location_type to set
	 */
	public void setName(String location_name) {
		this.sName = location_name;
	}

	/**
	 * @return the sEmail
	 */
	public String getEmail() {
		return sEmail;
	}

	/**
	 * @param sEmail the sEmai set
	 */
	public void setEmail(String email) {
		this.sEmail = email;
	}
	
	/**
	 * @return the sPhone
	 */
	public String getPhone() {
		return sPhone;
	}

	/**
	 * @param sPhone the sPhone to set
	 */
	public void setPhone(String sPhone) {
		this.sPhone = sPhone;
	}
	
	/**
	 * @param sPhone the sMessage to set
	 */
	public void setMessage(String sMessage) {
		this.sMessage = sMessage;
	}
	
	/**
	 * @return the sMessage
	 */
	public String getMessage() {
		return sMessage;
	}

		
	public ContactBean(){
		super();
	}
	
	public void init() {
		this.sEmail = "";
		this.sName = "";
		this.sPhone = "";
		this.sMessage = "";
		
	}
	
	public String toString(){
		String buf = null;
		buf = "Contact Bean object: \n";
		buf = buf + "Name: " + sName + "\n";
		buf = buf + "Email: " + sEmail + "\n";
		buf = buf + "Phone: " + sPhone + "\n";
		buf = buf + "Message: " + sMessage + "\n";
		
		return buf;
	}
	

}
