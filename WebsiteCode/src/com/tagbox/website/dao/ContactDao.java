package com.tagbox.website.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.TreeMap;
import java.util.Vector;

import com.tagbox.website.bean.ContactBean;
import com.tagbox.website.util.DbConnection;

public class ContactDao {
	private Connection conn = null;
	
		public void insertMessage(String sName, String sPhone, String sEmail, String sMessage) {
			Connection conn = null;
			ResultSet rs = null;
			Statement statement = null;
			PreparedStatement pSelect = null;
			String updateSql = "";
			String sKey = "";
			try {
				java.util.Date dt = new java.util.Date();
				Timestamp tstamp = new Timestamp(dt.getTime()); 
				conn = DbConnection.getConnection();
				System.out.println("action: " + sName + " id: " + sPhone);
				 //There is no route_type in transit_gateway_map. Should that be transit_vehicle_map. If yes, then there is no client_id in transit_vehicle_map
				 updateSql = "insert into web_info(name, email, phone, message, created_on) values (?,?,?,?,?)";
				 pSelect = conn.prepareStatement(updateSql);  
				 pSelect.setString(1, sName);
				 pSelect.setString(2, sEmail);
				 pSelect.setString(3, sPhone);
				 pSelect.setString(4, sMessage);
				 pSelect.setTimestamp(5, tstamp);
				 int iReturn = pSelect.executeUpdate();  
		     } catch (Exception e){
		       	 e.printStackTrace();
		     } finally {
				try {
					if(rs != null) rs.close();
					DbConnection.closeConnection();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
}
