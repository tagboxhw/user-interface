<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="com.licious.dao.AlertsReportDao"%>
<%@ page import="com.licious.util.Constants"%>
<%@ page import="com.licious.bean.AlertWorkflowBean"%>
<%@ page import="java.util.Collection"%>
<%@ page import="java.util.Vector"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.util.TimeZone"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>
<%@ page import="java.util.concurrent.TimeUnit"%>

<%
response.setHeader("Cache-Control","no-cache"); 
response.setHeader("Pragma","no-cache"); 
response.setDateHeader ("Expires", -1); 
%>
<%!
static final Logger logger = LoggerFactory.getLogger("AlertsReport");
AlertsReportDao alertsReportDao = new AlertsReportDao();
AlertWorkflowBean alertWorkflowBean = new AlertWorkflowBean();
Collection<AlertWorkflowBean> col = new Vector<AlertWorkflowBean>();
%>
<%
String sUsername = (String) session.getAttribute("username");
if(sUsername == null || sUsername.equals("")) {
	session.setAttribute("callingPage", "AlertsReport.jsp");
	response.sendRedirect("../../index.html");
}
sUsername = (String) session.getAttribute("username");
String sFromDate = (String) request.getParameter("from_date");
String sToDate = (String) request.getParameter("to_date");

java.util.Date dt = new java.util.Date();
SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
Date d = null;
java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm");
SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy");
SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy/M/d");
df.setTimeZone(java.util.TimeZone.getTimeZone("UTC"));
sdf2.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
sdf3.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
Date d2 = null;
Date dFromDateUI = null;
Date dToDateUI = null;
Date dFromDate = null;
Date dToDate = null;
String sFromDate1 = "", sToDate1 = "", sFromDate2 = "", sToDate2 = "";;
if(sFromDate != null && !sFromDate.equals("")) {
	d = sdf2.parse(sFromDate);
	sFromDate1 = df.format(d);
	sFromDate2 = sdf3.format(d);
	dFromDate = sdf3.parse(sFromDate2);
	dFromDateUI = sdf2.parse(sFromDate);
} else {
	d = new Date(dt.getTime() - (7 * Constants.DAY_IN_MS));
	sFromDate1 = df.format(d);
	sFromDate = sdf2.format(d);
	sFromDate2 = sdf3.format(d);
	dFromDate = sdf3.parse(sFromDate2);
	dFromDateUI = sdf2.parse(sFromDate);
}
if(sToDate != null && !sToDate.equals("")) {
	d = sdf2.parse(sToDate);
	sToDate1 = df.format(d);
	sToDate2 = sdf3.format(d);
	dToDate = sdf3.parse(sToDate2);
	dToDateUI = sdf2.parse(sToDate);
} else {
	sToDate1 = df.format(dt);
	sToDate = sdf2.format(dt);
	sToDate2 = sdf3.format(dt);
	dToDate = sdf3.parse(sToDate2);
	dToDateUI = sdf2.parse(sToDate);
}
Long newTime = null;
sdf.setTimeZone(TimeZone.getTimeZone("IST"));
String currentTime = sdf.format(dt);
col = alertsReportDao.getAlertsReport();
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Alerts Reports</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<!-- Add these below two lines for autocomplete and the javascript function at the end  -->
<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="../../bootstrap/css/bootstrap.css">
   
<!-- Font Awesome -->
<script src="https://use.fontawesome.com/37ba1872d2.js"></script>
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../../dist/css/tagbox.css">
<link href="../../dist/css/select2.css" rel="stylesheet" />
<script src="../../dist/js/select2.js"></script>
<script src="../../dist/js/tagbox.js"></script>
<script src="../../dist/js/table2download.js"></script>
<STYLE TYPE="text/css">
			table.sortable span.sortarrow {
   				color: blue;
   				text-decoration: none;
			}
					
		</style>
		<style>
        .ev_material{
         	background-color:#f9f9f9;
      	}
      	.odd_material{
         	background-color:#F0F8FF;
      	}
        </style>
<script src="../../dist/js/sorttable.js"></script>
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
<link href="https://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet">

<link rel="stylesheet" href="../../dist/css/bootstrap-switch.css">
<script src="../../dist/js/bootstrap-switch1.js"></script>
<link rel="stylesheet" type="text/css" href="../../dist/css/dhtmlx.css">
<script src="../../dist/js/dhtmlx.js"></script>
<script src="../../dist/js/table2download.js"></script>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
  body {
	font-family:  Lato, sans-serif;
	font-weight: 600;
}
</style>
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
	<div class="wrapper">

		<header class="main-header">
			<!-- Logo -->
			<div>
				<a href="#" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
					<span class="logo-mini"><img src="../../dist/img/licious.png" style="margin-top:14px"
						alt="User Image"></span> <!-- logo for regular state and mobile devices -->
					<span class="logo-lg"><img src="../../dist/img/licious.png" 
						alt="User Image"></span>
				</a>
			</div>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">

				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
					role="button"> <span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
				</a>
				<form class="navbar-form pull-left" role="search">
					<div class="form-group">
						<b style="padding-left: 15px; font-size: 20px;color:#fff;" class="pull-right">Alerts Reports</b><br> <b style="padding-left: 15px;color:#fff;"><%= currentTime %></b>
					</div>
				</form>

				<div class="navbar-custom-menu">
					<img src="../../dist/img/Logo.png" class="logo-class pull-right"
						alt="User Image">
				</div>
			</nav>

		</header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->


				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<br>
				<ul class="sidebar-menu">
					<li class="header">MAIN NAVIGATION</li>
					<li class="treeview"><a href="#"> <i
							class="fa fa-dashboard"></i> <span>Dashboard</span>
					</a>
						<ul class="treeview-menu">
							<li class="">
							<%if(sUsername != null && sUsername.equals("demo2")) { %>
							<a href="CMCDashboard.jsp"><i
									class="fa fa-circle-o"></i> CMC</a>
							<%} else { %>
							<a href="CMCDashboard.jsp" role="button" class="btn btn-disabled disabled"><i
									class="fa fa-circle-o"></i> CMC</a>
							<%} %>
							</li>
							<!-- <li class=""><a href="NewDashboard.jsp"><i
									class="fa fa-circle-o"></i> Vehicle Dashboard</a></li> -->
							<li class=""><a href="TodayColdChainHealth.jsp"><i
									class="fa fa-circle-o"></i> Cold Rooms Summary</a></li>
							<!--  <li><a href="VehiclePlannedRouteMap.jsp"><i class="fa fa-circle-o"></i> Vehicle Planned Route</a></li> -->
							<li class=""><a href="TodayColdChainHealthVehicle.jsp" role="button" class="btn btn-disabled disabled"><i
									class="fa fa-circle-o"></i> Vehicles Summary</a></li>
						</ul></li>
					<li class="treeview"><a href="#"> <i
							class="fa fa-files-o"></i> <span>Alerts</span> 
					</a>
						<ul class="treeview-menu">
							<li><a href="LiveVehicleTracking.jsp" role="button" class="btn btn-disabled disabled"><i
									class="fa fa-circle-o"></i> Live Vehicle Tracking</a></li>
						</ul></li>
					
					<li class="treeview"><a href="#"> <i
							class="fa fa-database"></i> <span>Admin</span>
					</a>
						<ul class="treeview-menu">
							<li class=""><a href="VehicleManagement.jsp" role="button" class="btn btn-disabled disabled"><i class="fa fa-circle-o"></i>
									Vehicle Management</a></li>
							<li><a href="VendorScorecard.jsp" role="button" class="btn btn-disabled disabled"><i class="fa fa-circle-o"></i>
									Vehicle Score Card</a></li>
						</ul></li>
						<li class="treeview active"><a href="#"> <i
							class="fa fa-bar-chart"></i> <span>Reports</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="ParameterSummary.jsp"><i class="fa fa-circle-o"></i>
									Parameter Summary</a></li>
							<li><a href="AlertsSummary.jsp"><i class="fa fa-circle-o"></i>
									Alerts Summary</a></li>
							<li class="active"><a href="AlertsReport.jsp"><i class="fa fa-circle-o"></i>
									Alerts Report</a></li>		
							
						</ul></li>
						<li class="treeview"><a href="#"> <i
							class="fa fa-history"></i> <span>Historical Data</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="HistoricalData.jsp"><i class="fa fa-circle-o"></i>
									Download <br>Temperature Data</a></li>
						</ul></li>
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			
		<section id="mainSection" class="content">
		<div class="well well-sm white-background">
							
						<br>
						<form id="myForm3" name="myForm3" method="post"
											action="AlertsReport.jsp">
											<div class="row">
				<div class="col-md-12">
				<div class="row">
					<div class="col-md-offset-3 col-md-5 text-center well well-sm" id="time-range">
					<div class="row">
						<div class="col-md-offset-1 col-md-6">
						<label><b>Time Range</b></label><br><label class="slider-time"></label> - <label class="slider-time2"></label>
    <div class="sliders_step1">
        <div id="slider-range"></div>
    </div>
    <input type="hidden" name="from_date" id="from_date" value="">
    <input type="hidden" name="to_date" id="to_date" value="">
						</div>
						<div class="col-md-4 text-center"><br>
						<button type="submit" class="btn btn-success">GO</button>
						</div>
					</div>
    
					</div>
					</div>
				<div class="row">
						<div class="col-md-offset-1 col-md-2 text-center">
					<label><b>Location</b></label><br><br>
					<select id="Location" name="Location" class="form-control-no-height">
					<option>Location</option>
					<option>Bengaluru</option>
					<option>Chennai</option>
					</select>
					</div>
					<div class="col-md-2 text-center">
					<label><b>Zone</b></label><br><br>
					<select id="Zone" name="Zone" class="form-control-no-height">
					<option>Cold Room</option>
					<option>First Floor CR</option>
					<option>Second Floor CR</option>
					</select>
					</div>
					
					<div class="col-md-2 text-center">
						<label><b>Alert Type</b></label><br><br>
					<select id="AlertType" name="AlertType" class="form-control-no-height">
					<option>Alert Type</option>
					<!-- <option>Door Open</option> -->
					<option>Temp Breach</option>
					</select>
					</div>
					<div class="col-md-2 text-center">
						<label><b>Assigned To</b></label><br><br>
					<select id="AssignedTo" name="AssignedTo" class="form-control-no-height">
					<option>Assigned To</option>
					<option>Prajesh</option>
					<option>Santhosh</option>
					</select>
					</div>
					<div class="col-md-2 text-center">
						<label><b>Status</b></label><br><br>
					<select id="Status" name="Status" class="form-control-no-height">
					<option>Status</option>
					<option>Closed</option>
					<option>Open</option>
					</select>
					</div>
					</div>
					<br><br>
					<div class="row">
					<div class="col-md-offset-9 col-md-3">
						<span id="csvLink"></span>&nbsp;&nbsp;&nbsp;&nbsp;<span id="xlsLink"></span>
					</div>
					<br><br>
					</div>
					<div class="row">
						<div class="col-md-offset-1 col-md-10">
						<div class="scroll-area-table">
						<table id="data_table_stationary2" class="table sortable dhtmlxGrid">
									
									<tr class="info text-center">
									<th width="150" class="text-center">Location</th>
									<th width="100" class="text-center">Zone</th>
									<th width="200" class="text-center">Time</th>
									<th width="100" class="text-center">Alert Type</th>
									<th width="120" class="text-center">Alert Duration</th>
									<th width="120" class="text-center">Breach</th>
									<th width="100" class="text-center">Assigned To</th>
									<th width="150" class="text-center">Status</th>
									</tr>
									<%
									String sTempDate = "";
									for(AlertWorkflowBean alertWorkflowBean: col){ 
										d = df.parse(alertWorkflowBean.getAlert_Timestamp_From());
										sTempDate = sdf2.format(d);
										if(sdf2.parse(sTempDate).getTime() >= dFromDateUI.getTime() && sdf2.parse(sTempDate).getTime() <= dToDateUI.getTime()) {
										
									%>
									<tr>
										<td><%= alertWorkflowBean.getGW_Client_ID()%></td>
										<td><%=alertWorkflowBean.getND_Client_ID() %></td>
										<td><%=sdf.format(df1.parse(alertWorkflowBean.getAlert_Timestamp_From())) %></td>
										<td><%=alertWorkflowBean.getAlert_Type() %></td>
										<%if(Integer.parseInt(alertWorkflowBean.getAlert_Duration()) == 1) { %>
										<td><%=alertWorkflowBean.getAlert_Duration() %> min</td>
										<%} else { %>
										<td><%=alertWorkflowBean.getAlert_Duration() %> mins</td>
										<%} %>
										<td><%=alertWorkflowBean.getBreach_Type() %></td>
										<td><%=alertWorkflowBean.getAssigned_To() %></td>
										<td><%=alertWorkflowBean.getAction_Status() %></td></tr>
									<%} 
									}%>
									</table>
									</div>
						</div>
					</div>
					
									<script>
    // here you will place your JavaScript code
    var mygrid = dhtmlXGridFromTable("data_table_stationary2");
    mygrid.enableAutoWidth(true);
    //mygrid.enableAlterCss("even","uneven");
   	//mygrid.setSkin("gray"); 
    //mygrid.setSizes();
    mygrid.setColSorting("str,str,str,str,str,str,str,str"); 
    mygrid.makeFilter("Location",0);
    mygrid.makeFilter("Zone",1);
    mygrid.makeFilter("AlertType",3);
    mygrid.makeFilter("AssignedTo",6);
    mygrid.makeFilter("Status",7);
    //mygrid.attachHeader("#text_filter,#text_filter,#numeric_filter");
    //mygrid.init();
    </script>
									
						</div>
						
																
								</form>
					
					
					
						
								</div>
						
		</section>
		</div>
							</div>
		
		<!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0
			</div>
			<strong>Copyright &copy; 2017 <a href="http://tagbox.in">TagBox</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Control Sidebar -->
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->
<!-- ChartJS 1.0.1 -->
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>


	<!-- Bootstrap 3.3.6 -->
	<script src="../../bootstrap/js/bootstrap.min.js"></script>
	<!-- ChartJS 1.0.1 -->
	<!-- AdminLTE App -->
	<script src="../../dist/js/app.min.js"></script>
	<script src="../../plugins/chartjs/Chart.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="../../dist/js/demo.js"></script>
	
	<script type="text/javascript">
	jQuery( document ).ready(function() {
		 $("[rel=tooltip]").tooltip();
	    jQuery( "#data_table_stationary2" ).table_download({
	        format: "xls",
	        separator: ",",
	        filename: "download",
	        jspname: "AlertsReport",
	        linkname: "Export To XLS",
	        quotes: "\"",
	        linkid: "xlsLink"
	    });
	    
	    jQuery( "#data_table_stationary2" ).table_download({
	        format: "csv",
	        separator: "-",
	        filename: "download",
	        jspname: "AlertsReport",
	        linkname: "Export To CSV",
	        quotes: "\"",
	        linkid: "csvLink"
	    });  
	    
	});
	var currentTime = new Date();
	var currentMonth = currentTime.getMonth() + 1;
	var currentDay = currentTime.getDate();
	var currentYear = currentTime.getFullYear();
	var dt_to = currentYear + "/" + currentMonth + "/" + currentDay;
	currentTime.setDate(currentTime.getDate() - 7);
	var dt_from= currentTime.getFullYear() + "/" + (currentTime.getMonth() + 1) + "/" + (currentTime.getDate());
	currentTime = new Date(<%= dToDate.getTime()%>);
	currentMonth = currentTime.getMonth() + 1;
	currentDay = currentTime.getDate();
	currentYear = currentTime.getFullYear();
	var dt_to1 = zeroPad(currentDay, 2) + "/" + zeroPad(currentMonth, 2) + "/" + currentYear;
	var dt_to2 = currentTime.getFullYear() + "/" + (currentTime.getMonth() + 1) + "/" + (currentTime.getDate());;
	currentTime = new Date(<%=dFromDate.getTime()%>);
	var dt_from1= zeroPad(currentTime.getDate(), 2) + "/" + zeroPad((currentTime.getMonth() + 1), 2) + "/" + currentTime.getFullYear();
	var dt_from2= currentTime.getFullYear() + "/" + (currentTime.getMonth() + 1) + "/" + (currentTime.getDate());;
	$('.slider-time').html(dt_from1);
	$('.slider-time2').html(dt_to1);
	$('#from_date').val(dt_from1);
	$('#to_date').val(dt_to1);
	var min_val = Date.parse(dt_from)/1000;
	var max_val = Date.parse(dt_to)/1000;
	var min_val1 = Date.parse(dt_from2)/1000;
	var max_val1 = Date.parse(dt_to2)/1000;
	function zeroPad(num, places) {
	  var zero = places - num.toString().length + 1;
	  return Array(+(zero > 0 && zero)).join("0") + num;
	}
	function formatDT(__dt) {
	    var year = __dt.getFullYear();
	    var month = zeroPad(__dt.getMonth()+1, 2);
	    var date = zeroPad(__dt.getDate(), 2);
	    return date + '/' + month + '/' + year;
	};

	$("#slider-range").slider({
	    range: true,
	    min: min_val,
	    max: max_val,
	    step: 10,
	    values: [min_val1, max_val1],
	    slide: function (e, ui) {
	        var dt_cur_from = new Date(ui.values[0]*1000); //.format("yyyy-mm-dd hh:ii:ss");
	        $('.slider-time').html(formatDT(dt_cur_from));

	        var dt_cur_to = new Date(ui.values[1]*1000); //.format("yyyy-mm-dd hh:ii:ss");                
	        $('.slider-time2').html(formatDT(dt_cur_to));
	        $('#from_date').val(formatDT(dt_cur_from));
	        $('#to_date').val(formatDT(dt_cur_to));
	    }
	});

</script>
	</body>
</html>
