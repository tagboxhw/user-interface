
<%@ page import="com.licious.action.MapAction"%>
<%@ page import="com.licious.bean.MapsBean"%>
<%@ page import="java.util.Collection"%>
<%@ page import="java.util.Vector"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>

<!-- <?xml version="1.0" encoding="ISO-8859-1"?> -->

<%!
    static final Logger logger = LoggerFactory.getLogger("GetMapData");
	MapAction mapAction = new MapAction();
	String buf = "";
	String sString = "";
	String sErrorMessage = null;
	String sId = null;
	String sRowId = null;
	MapsBean mapbean = new MapsBean();
	%>
<% 
	try {
			buf="";
			//response.setContentType("text/xml");
			//buf += ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			Collection col = mapAction.getAllRows();
			Iterator<MapsBean> i = col.iterator();
			int iCount = 0;
			//logger.info("<rows total_count='100' pos='0'>");
			buf = "<rows>";
			while(i.hasNext()) {
				mapbean = (MapsBean) i.next();
				buf += ("<row id=\"" + iCount + "\">");
				buf += ("<gatewayId>" + mapbean.getGateway_id() + "</gatewayId>");
				buf += ("<deviceId>" + mapbean.getDevice_id() + "</deviceId>");
				buf += ("<deviceTime>" + mapbean.getDevice_time() + "</deviceTime>");
				buf += ("<temp>" + mapbean.getTemp() + "</temp>");
				buf += ("<humidity>" + mapbean.getHumidity() + "</humidity>");
				buf += ("<doorOpen>" + mapbean.isDoor_open() + "</doorOpen>");
				buf += ("<latitude>" + mapbean.getLatitude() + "</latitude>");
				buf += ("<longitude>" + mapbean.getLongitude() + "</longitude>");
				buf += ("<haltDuration>" + mapbean.getHalt_duration() + "</haltDuration>");
				buf += ("<openDuration>" + mapbean.getOpen_duration() + "</openDuration>");
				buf += ("<tempAlert>" + mapbean.isTemp_alert() + "</tempAlert>");
				buf += ("<humidityAlert>" + mapbean.isHumidity_alert() + "</humidityAlert>");
				buf += ("<haltAlert>" + mapbean.isHalt_alert() + "</haltAlert>");
				buf += ("<detourAlert>" + mapbean.isDetour_alert() + "</detourAlert>");
				buf += ("<doorAlert>" + mapbean.isDoor_alert() + "</doorAlert>");
				buf += ("</row>");
				iCount++;
			}
			buf += "</rows>";
			buf = buf.trim();
			//logger.info(buf);
			out.clear();
			out.println(buf);
	} catch (Exception e) {
		logger.error("Exception: " + e.getLocalizedMessage());
		e.printStackTrace();
	}
			%>