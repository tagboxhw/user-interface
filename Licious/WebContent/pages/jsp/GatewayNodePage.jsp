<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ page import="com.licious.dao.ClientLocationMapDao"%>
<%@ page import="com.licious.dao.TransitDao"%>
<%@ page import="com.licious.bean.StationaryNodeMapBean"%>
<%@ page import="com.licious.bean.TransitNodeMapBean"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.util.Collection"%>
<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>

<%!static final Logger logger = LoggerFactory.getLogger("GatewayNodePage");
	String sErrorMessage = null;
	Collection colTransit;
	Collection colStationary;
	StationaryNodeMapBean stationaryNodeMapBean = new StationaryNodeMapBean();
	TransitNodeMapBean transitNodeMapBean = new TransitNodeMapBean();
	String sTempKey = "";
	TransitDao transitDao = new TransitDao();
	ClientLocationMapDao clientDao = new ClientLocationMapDao();%>
<%
	sErrorMessage = null;
	clientDao = new ClientLocationMapDao();
	transitDao = new TransitDao();
	colStationary = clientDao.selectAllStationaryNodeMapRows();
	colTransit = transitDao.selectAllTransitNodeMapRows();
	Iterator iterStationary = colStationary.iterator();
	Iterator iterTransit = colTransit.iterator();
	stationaryNodeMapBean = new StationaryNodeMapBean();
	transitNodeMapBean = new TransitNodeMapBean();
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Gateway Nodes Page</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Add these below two lines for autocomplete and the javascript function at the end  -->
<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="../../bootstrap/css/bootstrap.css">

<!-- Font Awesome -->
<script src="https://use.fontawesome.com/37ba1872d2.js"></script>


<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">

<!-- Theme style -->
<link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../../dist/css/tagbox.css">

<script src="../../dist/js/table_script.js"></script>
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">


<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
<script type="text/javascript">
	function SubmitPage() {
		//var Time = $("#alertsFilter").val();
		//var Loc = $("#locationsFilter").val();
		//var Zone = $("#zonesFilter").val();
		//var Node = $("#nodeFilter").val();
		//var Thres = $("#ex2").val();
		//var Phone = $("#PhoneNum").val();
		//window.location = "AlertsDemoCopy.jsp?Time=" + Time + "&Loc="+ Loc +"&Zone="+ Zone +"&Node="+ Node +"&Threshold="+ Thres +"&Phone=" + Phone;
		document.forms["myForm"].submit();
	}
</script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.5.4/bootstrap-slider.min.js"></script>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.5.4/css/bootstrap-slider.min.css">
<style>
.save {
	display: none;
}

.no-border {
	border: none !important;
	border: 0 !important;
}
</style>
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
	<div class="wrapper">
		<header class="main-header">
			<!-- Logo -->
			<div>
				<a href="#" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
					<span class="logo-mini"><img src="../../dist/img/licious.png" style="margin-top:14px"
						alt="User Image"></span> <!-- logo for regular state and mobile devices -->
					<span class="logo-lg"><img src="../../dist/img/licious.png" 
						alt="User Image"></span>
				</a>
			</div>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">

				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
					role="button"> <span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
				</a>
				<form class="navbar-form pull-left" role="search">
					<div class="form-group">
						<b style="padding-left: 15px; font-size: 20px;color:#fff;" class="pull-right">Adarsh
							Kumar's Central Console</b><br> <b style="padding-left: 15px;color:#fff;">Gateway
							Node Updates</b>
					</div>
				</form>

				<div class="navbar-custom-menu">
					<img src="../../dist/img/Logo.png" class="logo-class pull-right"
						alt="User Image">
				</div>
			</nav>

		</header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->


				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<br>
				<ul class="sidebar-menu">
					<li class="header">MAIN NAVIGATION</li>
					<li class="treeview"><a href="#"> <i
							class="fa fa-dashboard"></i> <span>Alert</span> <span
							class="pull-right-container"> <span
								class="label bg-red pull-right">4</span>
						</span>
					</a>
						<ul class="treeview-menu">
							<li><a> Cold Rooms</a></li>
							<li><a href="ColdRoomPendingAlertWorkflows.jsp"><i
									class="fa fa-circle-o"></i> Today's Alerts</a></li>
							<li><a href="#"><i class="fa fa-circle-o"></i> Pending
									Alert Workflows</a></li>
							<li><a href="#"><i class="fa fa-circle-o"></i> Alerts
									With No Action</a></li>
							<li><a>Vehicles</a></li>
							<li><a href="VehicleTodayAlerts.jsp"><i
									class="fa fa-circle-o"></i> Today's Alerts</a></li>
							<li><a href="VehicleStoppageAlerts.jsp"><i
									class="fa fa-circle-o"></i> Vehicle Stoppage</a></li>
							<li><a href="VehicleRouteAlerts.jsp"><i
									class="fa fa-circle-o"></i> Vehicle Route</a></li>
							<li><a href="VehicleDoorAlerts.jsp"><i
									class="fa fa-circle-o"></i> Vehicle Door Activity</a></li>
							<li><a href="VehicleTemperatureAlerts.jsp"><i
									class="fa fa-circle-o"></i> Vehicle Temperature</a></li>
						</ul></li>
					<li class="treeview active"><a href="#"> <i
							class="fa fa-files-o"></i> <span>Dashboard</span> <span
							class="pull-right-container"> <i
								class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="Dashboard.jsp"><i class="fa fa-circle-o"></i>
									CMC</a></li>
							<!--  <li><a href="VehiclePlannedRouteMap.jsp"><i class="fa fa-circle-o"></i> Vehicle Planned Route</a></li> -->
							<li class="active"><a href="AlertsDemo.jsp"><i
									class="fa fa-circle-o"></i> Live Alerts</a></li>
							<li><a href="LiveVehicleTracking.jsp"><i
									class="fa fa-circle-o"></i> Live Vehicle Tracking</a></li>
						</ul></li>
					<li><a href="#"> <i class="fa fa-th"></i> <span>Analytics</span>
							<span class="pull-right-container"> <small
								class="label pull-right bg-green">new</small>
						</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="DemandForecasting.jsp"><i
									class="fa fa-circle-o"></i> Demand Forecasting</a></li>
						</ul></li>
					<li class="treeview"><a href="#"> <i
							class="fa fa-pie-chart"></i> <span>Admin Console</span> <span
							class="pull-right-container"> <i
								class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
						<ul class="treeview-menu">
							<li class=""><a href="#"><i class="fa fa-circle-o"></i>
									Device Environment</a></li>
							<li><a href="#"><i class="fa fa-circle-o"></i> User
									Management</a></li>
							<li><a href="#"><i class="fa fa-circle-o"></i> Central
									Console</a></li>
						</ul></li>
					<li class="treeview"><a href="#"> <i class="fa fa-laptop"></i>
							<span>Support</span> <span class="pull-right-container"> <i
								class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="#"><i class="fa fa-circle-o"></i> Raise
									Service Ticket</a></li>
							<li><a href="#"><i class="fa fa-circle-o"></i> Email
									Your Questions</a></li>
						</ul></li>

				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Main content -->
			<section class="content">
				<div class="row">
					<div class="col-md-12">
						<div class="panel-group" id="accordion" role="tablist"
							aria-multiselectable="true">
							<div class="panel panel-info">
								<div class="panel-heading" role="tab" id="headingOne">
									<h4 class="panel-title">
										<div class="collapsed" role="button" data-toggle="collapse"
											data-parent="#accordion" href="#collapseOne"
											aria-expanded="true" aria-controls="collapseOne">
											Stationary Node Map</div>
									</h4>
								</div>
								<div id="collapseOne" class="panel-collapse collapse in"
									role="tabpanel" aria-labelledby="headingOne">
									<div class="panel-body">
										<form id="myForm1" name="myForm1" method="post"
											action="GatewayNodePage.jsp">
											<table id="data_table_stationary"
												class="table table-bordered table-striped table-condensed"
												width="100%">
												<tr>
													<th width="10%">Zone ID</th>
													<th width="10%">GW_Client_ID</th>
													<th width="10%">ND_Client_ID</th>
													<th width="10%">ND_Device_ID</th>
													<th width="10%">GW_ND_Pairing</th>
													<th width="10%">ND_Subzone_Type</th>
													<th width="10%">ND_Subzone_Name</th>
													<th width="5%">ND_Type</th>
													<th width="8%">ND_Status</th>
													<th width="10%">Action</th>
												</tr>
												<%
													int j = 0;
													while (iterStationary.hasNext()) {
														stationaryNodeMapBean = new StationaryNodeMapBean();
														stationaryNodeMapBean = (StationaryNodeMapBean) iterStationary
																.next();
												%>
												<tr id="row_stationary<%=j%>">
													<td id="<%=j%>_Zone_id_stationary"><%=stationaryNodeMapBean.getGW_Zone_id()%></td>
													<td id="<%=j%>_GW_client_id_stationary"><%=stationaryNodeMapBean.getGW_client_id()%></td>
													<td id="<%=j%>_ND_client_id_stationary"><%=stationaryNodeMapBean.getND_client_id()%></td>
													<td id="<%=j%>_ND_device_id_stationary"><%=stationaryNodeMapBean.getND_device_id()%></td>
													<td id="<%=j%>_GW_ND_Pairing_stationary"><%=stationaryNodeMapBean.getGW_ND_Pairing()%></td>
													<td id="<%=j%>_ND_subzone_type_stationary"><%=stationaryNodeMapBean.getND_subzone_type()%></td>
													<td id="<%=j%>_ND_subzone_name_stationary"><%=stationaryNodeMapBean.getND_subzone_name()%></td>
													<td id="<%=j%>_ND_type_stationary"><%=stationaryNodeMapBean.getND_type()%></td>
													<td id="<%=j%>_ND_status_stationary"><%=stationaryNodeMapBean.getND_status()%></td>
													<td>
														<table width="100%" class="no-border">
															<tr class="no-border">
																<td class="no-border"><i
																	id="edit_button_stationary<%=j%>"
																	class="fa fa-pencil edit fa-color-blue"
																	onclick="edit_row_stationary('<%=j%>')"></i></td>
																<td class="no-border"><i
																	id="save_button_stationary<%=j%>"
																	class="fa fa-floppy-o save fa-color-blue"
																	onclick="save_row_stationary('<%=j%>')"></i></td>
																<td class="no-border">
																	<!-- <i id="cancel_button<%=j%>" class="fa fa-ban save fa-color-blue" onclick="cancel_row('<%=j%>')"></i> -->
																</td>
																<td class="no-border"><i
																	class="fa fa-trash delete alert-text"
																	onclick="delete_row_stationary('<%=j%>')"></i></td>
															</tr>
														</table>
													</td>
												</tr>
												<%
													j++;
													}
												%>
												<tr id="row_stationary<%=j%>">
													<td><input id="new_zone_id_stationary" type="text"
														class="form-control-no-height"></td>
													<td><input id="new_GW_client_id_stationary"
														type="text" class="form-control-no-height"></td>
													<td><input id="new_ND_client_id_stationary"
														type="text" class="form-control-no-height"></td>
													<td><input id="new_ND_device_id_stationary"
														type="text" class="form-control-no-height"></td>
													<td><input id="new_GW_ND_Pairing_stationary"
														type="text" class="form-control-no-height"></td>
													<td><input id="new_ND_subzone_type_stationary"
														type="text" class="form-control-no-height"></td>
													<td><input id="new_ND_subzone_name_stationary"
														type="text" class="form-control-no-height"></td>
													<td><input id="new_ND_type_stationary" type="text"
														class="form-control-no-height"></td>
													<td><input id="new_ND_status_stationary" type="text"
														class="form-control-no-height"></td>
													<td><button type="button"
															class="add btn btn-sm btn-success"
															onclick="add_row_stationary();" value="Add Row">Add
															Row</button></td>
												</tr>
												<%
													j++;
												%>

											</table>

											<br> <input type="hidden" id="lastValue_stationary"
												name="lastValue_stationary" value="<%=j%>">
										</form>
									</div>
								</div>
							</div>
							<div class="panel panel-info">
								<div class="panel-heading" role="tab" id="headingTwo">
									<h4 class="panel-title">
										<div role="button" data-toggle="collapse"
											data-parent="#accordion" href="#collapseTwo"
											aria-expanded="false" aria-controls="collapseTwo">
											Transit Node Map</div>
									</h4>
								</div>
								<div id="collapseTwo" class="panel-collapse collapse"
									role="tabpanel" aria-labelledby="headingTwo">
									<div class="panel-body">
										<form id="myForm" name="myForm" method="post"
											action="GatewayNodePage.jsp">
											<table id="data_table"
												class="table table-bordered table-striped table-condensed"
												width="100%">
												<tr>
													<th width="10%">Vehicle ID</th>
													<th width="10%">GW_Client_ID</th>
													<th width="10%">ND_Client_ID</th>
													<th width="10%">ND_Device_ID</th>
													<th width="10%">GW_ND_Pairing</th>
													<th width="10%">ND_Subzone_Type</th>
													<th width="10%">ND_Subzone_Name</th>
													<th width="5%">ND_Type</th>
													<th width="8%">ND_Status</th>
													<th width="10%">Action</th>
												</tr>
												<%
													int i = 0;
													while (iterTransit.hasNext()) {
														transitNodeMapBean = new TransitNodeMapBean();
														transitNodeMapBean = (TransitNodeMapBean) iterTransit.next();
												%>
												<tr id="row_<%=i%>">
													<td id="<%=i%>_Vehicle_id"><%=transitNodeMapBean.getVehicle_id()%></td>
													<td id="<%=i%>_GW_client_id"><%=transitNodeMapBean.getGW_client_id()%></td>
													<td id="<%=i%>_ND_client_id"><%=transitNodeMapBean.getND_client_id()%></td>
													<td id="<%=i%>_ND_device_id"><%=transitNodeMapBean.getND_device_id()%></td>
													<td id="<%=i%>_GW_ND_Pairing"><%=transitNodeMapBean.getGW_ND_Pairing()%></td>
													<td id="<%=i%>_ND_subzone_type"><%=transitNodeMapBean.getND_subzone_type()%></td>
													<td id="<%=i%>_ND_subzone_name"><%=transitNodeMapBean.getND_subzone_name()%></td>
													<td id="<%=i%>_ND_type"><%=transitNodeMapBean.getND_type()%></td>
													<td id="<%=i%>_ND_status"><%=transitNodeMapBean.getND_status()%></td>
													<td>
														<table width="100%" class="no-border">
															<tr class="no-border">
																<td class="no-border"><i id="edit_button<%=i%>"
																	class="fa fa-pencil edit fa-color-blue"
																	onclick="edit_row('<%=i%>')"></i></td>
																<td class="no-border"><i id="save_button<%=i%>"
																	class="fa fa-floppy-o save fa-color-blue"
																	onclick="save_row('<%=i%>')"></i></td>
																<td class="no-border">
																	<!-- <i id="cancel_button<%=i%>" class="fa fa-ban save fa-color-blue" onclick="cancel_row('<%=i%>')"></i> -->
																</td>
																<td class="no-border"><i
																	class="fa fa-trash delete alert-text"
																	onclick="delete_row('<%=i%>')"></i></td>
															</tr>
														</table> <!-- <button type="button" id="edit_button<%=i%>" value="Edit" class="edit btn btn-sm btn-success" onclick="edit_row('<%=i%>')"><i class="fa fa-pencil"></i></button>
									<button type="button" id="save_button<%=i%>" value="Save" class="save btn btn-sm btn-success" onclick="save_row('<%=i%>')"><i class="fa fa-floppy-o"></i></button>
									<button type="button" value="Delete" class="delete btn btn-sm btn-danger" onclick="delete_row('<%=i%>')"><i class="fa fa-trash"></i></button>
									 -->
													</td>
												</tr>
												<%
													i++;
													}
												%>
												<tr id="row_<%=i%>">
													<td><input id="new_vehicle_id" type="text"
														class="form-control-no-height"></td>
													<td><input id="new_GW_client_id" type="text"
														class="form-control-no-height"></td>
													<td><input id="new_ND_client_id" type="text"
														class="form-control-no-height"></td>
													<td><input id="new_ND_device_id" type="text"
														class="form-control-no-height"></td>
													<td><input id="new_GW_ND_Pairing" type="text"
														class="form-control-no-height"></td>
													<td><input id="new_ND_subzone_type" type="text"
														class="form-control-no-height"></td>
													<td><input id="new_ND_subzone_name" type="text"
														class="form-control-no-height"></td>
													<td><input id="new_ND_type" type="text"
														class="form-control-no-height"></td>
													<td><input id="new_ND_status" type="text"
														class="form-control-no-height"></td>
													<td><button type="button"
															class="add btn btn-sm btn-success" onclick="add_row();"
															value="Add Row">Add Row</button></td>
												</tr>
												<%i++; %>

											</table>

											<br> <input type="hidden" id="lastValue"
												name="lastValue" value="<%=i%>">
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<br>
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0
			</div>
			<strong>Copyright &copy; 2017 <a href="http://tagbox.in">TagBox</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Control Sidebar -->
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>

	<!-- Bootstrap 3.3.6 -->
	<script src="../../bootstrap/js/bootstrap.min.js"></script>
	<!-- AdminLTE App -->
	<script src="../../dist/js/app.min.js"></script>

	<!-- ./wrapper -->
</body>
</html>
