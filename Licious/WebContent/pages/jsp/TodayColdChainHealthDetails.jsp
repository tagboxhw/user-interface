<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ page import="com.licious.dao.AlertsWorkflowDao"%>
<%@ page import="com.licious.dao.HumidityDao"%>
<%@ page import="com.licious.dao.TemperatureDao"%>
<%@ page import="com.licious.dao.ClientLocationMapDao"%>
<%@ page import="com.licious.dao.TransitDao"%>
<%@ page import="com.licious.bean.LocationBean"%>
<%@ page import="com.licious.dao.DoorActivityDao"%>
<%@ page import="com.licious.bean.DoorActivityBean"%>
<%@ page import="com.licious.bean.HumidityBean"%>
<%@ page import="com.licious.bean.TemperatureBean"%>
<%@ page import="com.licious.bean.HumidityTriggersBean"%>
<%@ page import="com.licious.bean.TemperatureTriggersBean"%>
<%@ page import="com.licious.bean.SubLocationBean"%>
<%@ page import="com.licious.util.Utils"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.util.TreeMap"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.util.Collection"%>
<%@ page import="java.util.LinkedHashMap"%>
<%@ page import="java.util.Set"%>
<%@ page import="java.util.Date"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Calendar"%>
<%@ page import="java.util.GregorianCalendar"%>
<%@ page import="com.licious.bean.AlertWorkflowBean"%>
<%@ page import="com.licious.bean.AllLocationDataBean"%>
<%@ page import="com.licious.util.Constants"%>
<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>
<%
response.setHeader("Cache-Control","no-cache"); 
response.setHeader("Pragma","no-cache"); 
response.setDateHeader ("Expires", -1); 
%>
<%!
static final Logger logger = LoggerFactory.getLogger("TodayColdChainHealthDetails");
SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MMM HH:mm");
SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm");
String sNodeId = "";
%>
<%
String sUsername = (String) session.getAttribute("username");
if(sUsername == null || sUsername.equals("")) {
	session.setAttribute("callingPage", "TodayColdChainHealthDetails.jsp");
	response.sendRedirect("../../index.html");
}
sUsername = (String) session.getAttribute("username");
AlertsWorkflowDao alertsWorkflowDao = new AlertsWorkflowDao();
Date d = null;
Calendar gc = new GregorianCalendar();
Date d2 = null;
Long newTime = null;
sNodeId = request.getParameter("Node");
if(sNodeId == null || sNodeId.equals("")) sNodeId = "SZ-SY-DMO-000001";
java.util.Date dt = new java.util.Date();
java.text.SimpleDateFormat sdf = 
     new java.text.SimpleDateFormat("dd-MM-yyyy HH:mm");
sdf.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
String currentTime = sdf.format(dt);
String sAlertType = request.getParameter("CatID");
if(sAlertType == null) sAlertType = "ALL";
String sTimeValue = request.getParameter("alertsFilter");
if(sTimeValue == null || sTimeValue.equals("")){
	sTimeValue = "60";
}
df.setTimeZone(java.util.TimeZone.getTimeZone("UTC"));
sdf1.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
String sValue = "";
ClientLocationMapDao clmDao = new ClientLocationMapDao();
String sLocationName = "", sZoneName = "", sZoneId = "", sSubZoneName = "", sZoneType = "";
if(clmDao.colAllLocations == null || clmDao.colAllLocations.size() == 0) clmDao.getAllLocationsData();
for(AllLocationDataBean alBean: clmDao.colAllLocations){
	if(alBean.getSubzone_Id().equals(sNodeId)) {
		sLocationName = alBean.getLocation_Name();
		sZoneName = alBean.getZone_Name();
		sZoneId = alBean.getZone_Id();
		sSubZoneName = alBean.getSubzone_Name();
		sZoneType = alBean.getSubzone_Type();
		break;
	}
}
TreeMap<String, String> tmLocID = new TreeMap<String, String>();
TreeMap<String, String> tmZoneID = new TreeMap<String, String>();
TreeMap<String, String> tmSubZoneID = new TreeMap<String, String>();
for(AllLocationDataBean alBean: clmDao.colAllLocations){
	tmLocID.put(alBean.getLocation_Name(), "1");
	if(alBean.getLocation_Name().equals(sLocationName)) {
		if(!alBean.getZone_Id().equals("")) tmZoneID.put(alBean.getZone_Id(), alBean.getZone_Name());
		if(alBean.getZone_Id().equals(sZoneId)) {
			if(!alBean.getSubzone_Id().equals("")) tmSubZoneID.put(alBean.getSubzone_Id(), alBean.getSubzone_Name());
			
	}
}
}
TemperatureDao tDao = new TemperatureDao();
HumidityDao hDao = new HumidityDao();
if(tDao.tempThresholds == null || tDao.tempThresholds.size() == 0) tDao.getTemperatureThresholds();
if(hDao.humThresholds == null || hDao.humThresholds.size() == 0) hDao.getHumidityThresholds();
int iTempUpperLimit = 0, iTempLowerLimit = 0, iHumUpperLimit = 0, iHumLowerLimit = 0;
for(TemperatureTriggersBean alBean: tDao.tempThresholds) {
	if(sNodeId.equals(alBean.getND_client_id())) {
		iTempUpperLimit = alBean.getThreshold_UL();
		iTempLowerLimit = alBean.getThreshold_LL();
	}
}
for(HumidityTriggersBean alBean: hDao.humThresholds) {
	if(sNodeId.equals(alBean.getND_client_id())) {
		iHumUpperLimit = alBean.getThreshold_UL();
		iHumLowerLimit = alBean.getThreshold_LL();
	}
}
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Today Cold Chain Health Details</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Add these below two lines for autocomplete and the javascript function at the end  -->
<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="../../bootstrap/css/bootstrap.css">
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
   
<!-- Font Awesome -->
<script src="https://use.fontawesome.com/37ba1872d2.js"></script>
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../../dist/css/tagbox.css">
<script src="../../dist/js/tagbox.js"></script>
<script src="../../dist/js/location.js"></script>
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
<link href="https://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
  body {
	font-family:  Lato, sans-serif;
}
</style>
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
	<div class="wrapper">

		<header class="main-header">
			<!-- Logo -->
			<div>
				<a href="#" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
					<span class="logo-mini"><img src="../../dist/img/licious.png" style="margin-top:14px"
						alt="User Image"></span> <!-- logo for regular state and mobile devices -->
					<span class="logo-lg"><img src="../../dist/img/licious.png" 
						alt="User Image"></span>
				</a>
			</div>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">

				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
					role="button"> <span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
				</a>
				<form class="navbar-form pull-left" role="search">
					<div class="form-group">
						<b style="padding-left: 15px; font-size: 20px;color:#fff;" class="pull-right">Today's Cold Chain Health Details</b><br> <b style="padding-left: 15px;color:#fff;"><%= currentTime %></b>
					</div>
				</form>

				<div class="navbar-custom-menu">
					<img src="../../dist/img/Logo.png" class="logo-class pull-right"
						alt="User Image">
				</div>
			</nav>

		</header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->


				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<br>
				<ul class="sidebar-menu">
					<li class="header">MAIN NAVIGATION</li>
					<li class="treeview active"><a href="#"> <i
							class="fa fa-dashboard"></i> <span>Dashboard</span>
					</a>
						<ul class="treeview-menu">
							<li class="">
							<%if(sUsername != null && sUsername.equals("demo2")) { %>
							<a href="CMCDashboard.jsp"><i
									class="fa fa-circle-o"></i> CMC</a>
									<%} else { %>
									<a href="CMCDashboard.jsp" role="button" class="btn btn-disabled disabled"><i
									class="fa fa-circle-o"></i> CMC</a>
									<%} %>
									</li>
							<!-- <li class=""><a href="NewDashboard.jsp"><i
									class="fa fa-circle-o"></i> Vehicle Dashboard</a></li> -->
							<li class="active"><a href="TodayColdChainHealth.jsp"><i
									class="fa fa-circle-o"></i> Cold Rooms Summary</a></li>
							<!--  <li><a href="VehiclePlannedRouteMap.jsp"><i class="fa fa-circle-o"></i> Vehicle Planned Route</a></li> -->
							<li class=""><a href="TodayColdChainHealthVehicle.jsp" role="button" class="btn btn-disabled disabled"><i
									class="fa fa-circle-o"></i> Vehicles Summary</a></li>
						</ul></li>
					<li class="treeview"><a href="#"> <i
							class="fa fa-files-o"></i> <span>Alerts</span> 
					</a>
						<ul class="treeview-menu">
							<li><a href="LiveVehicleTracking.jsp" role="button" class="btn btn-disabled disabled"><i
									class="fa fa-circle-o"></i> Live Vehicle Tracking</a></li>
						</ul></li>
					
					<li class="treeview"><a href="#"> <i
							class="fa fa-database"></i> <span>Admin</span>
					</a>
						<ul class="treeview-menu">
							<li class=""><a href="VehicleManagement.jsp" role="button" class="btn btn-disabled disabled"><i class="fa fa-circle-o"></i>
									Vehicle Management</a></li>
							<li><a href="VendorScorecard.jsp" role="button" class="btn btn-disabled disabled"><i class="fa fa-circle-o"></i>
									Vehicle Score Card</a></li>
						</ul></li>
						<li class="treeview"><a href="#"> <i
							class="fa fa-bar-chart"></i> <span>Reports</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="ParameterSummary.jsp"><i class="fa fa-circle-o"></i>
									Parameter Summary</a></li>
							<li><a href="AlertsSummary.jsp"><i class="fa fa-circle-o"></i>
									Alerts Summary</a></li>
							<li><a href="AlertsReport.jsp"><i class="fa fa-circle-o"></i>
									Alerts Report</a></li>		
							
						</ul></li>
						<li class="treeview"><a href="#"> <i
							class="fa fa-history"></i> <span>Historical Data</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="HistoricalData.jsp"><i class="fa fa-circle-o"></i>
									Download <br>Temperature Data</a></li>
						</ul></li>
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			
		<section id="mainSection" class="content">
			<div class="row">
				<div class="col-md-12">
				<b>
				<%if(sUsername != null && sUsername.equals("demo2")) { %>
				<a href="CMCDashboard.jsp">CMC</a> &nbsp; <i class="fa fa-caret-right"></i> &nbsp;
				<%} %>
				<a href="TodayColdChainHealth.jsp?LocationId=<%=sLocationName%>"><%= sLocationName %></a> &nbsp;<i class="fa fa-caret-right"></i>&nbsp; 
				<a href="TodayColdChainHealthSummary.jsp?ZoneId=<%=sZoneId%>"><%=sZoneName %></a> &nbsp;<i class="fa fa-caret-right"></i> &nbsp;
				<%= sSubZoneName %>
				</b><br> <br>
					<div class="row">
						
							
										<div class="col-md-8">
											<div class="well well-sm">
												<div class="text-center">
													<%if(sNodeId.equals("SZ-SY-DMO-000001")){%>
														<img src="../../dist/img/Picture6.png">
													<%} else if(sNodeId.equals("SZ-SY-DMO-000002")) {%>
													<img src="../../dist/img/Picture5.png">
													<%} else if(sNodeId.equals("SZ-SY-DMO-000003")) {  %>
													<img src="../../dist/img/Picture4.png">
													<%} else if(sNodeId.equals("SZ-SY-DMO-000004")) { %>
														<img src="../../dist/img/Picture3.png">
													<%} else if(sNodeId.equals("SZ-SY-DMO-000005")) {%>
														<img src="../../dist/img/Picture2.png">
													<%} else if(sNodeId.equals("SZ-SY-DMO-000006")) {%>
														<img src="../../dist/img/Picture7.png">
													<%} else if(sNodeId.equals("SZ-SY-DMO-000007")) {%>
														<img src="../../dist/img/Picture7.png">
													<%} else { %>
														<img src="../../dist/img/Picture6.png">
													<%} %>
												</div>
											</div>
										</div>
									<div class="col-md-offset-2 col-md-2">
							<div class="text-left">
							<label>GO TO:</label>
				<div class="scroll-area2 white-background">
				<%Set set = tmLocID.keySet();
				Set set1 = tmZoneID.keySet();
				Set set2 = tmSubZoneID.keySet();
				Iterator iter = set.iterator();
				Iterator iter1 = set1.iterator();
				Iterator iter2 = set2.iterator();
				String sValue1 = "";
				String sValue2 = "";
				while(iter.hasNext()) {
					sValue = (String) iter.next();
					if(sValue.equals(sLocationName)){%>
					<a href="TodayColdChainHealth.jsp?LocationId=<%=sValue%>"><%= sValue %></a><br>
					<%
							while(iter1.hasNext()) {
								sValue1 = (String) iter1.next();
								if(sValue1.equals(sZoneId)) {%>
									&nbsp;&nbsp;&nbsp;&nbsp;<b><a href="TodayColdChainHealthSummary.jsp?ZoneId=<%=sValue1 %>"><%=tmZoneID.get(sValue1).toUpperCase() %></a></b><br>
									<%while(iter2.hasNext()) {
										sValue2 = (String) iter2.next();
										if(sValue2.equals(sNodeId)){%>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="TodayColdChainHealthDetails.jsp?Node=<%=sValue2%>"><b><%=tmSubZoneID.get(sValue2).toUpperCase() %></b></a><br>
										<%} else {%>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="TodayColdChainHealthDetails.jsp?Node=<%=sValue2%>"><%=tmSubZoneID.get(sValue2) %></a><br>
										<%}
									}
								} else { %>
									&nbsp;&nbsp;&nbsp;&nbsp;<a href="TodayColdChainHealthSummary.jsp?ZoneId=<%=sValue1 %>"><%=tmZoneID.get(sValue1) %></a><br>
								<%}
							}
					} else { %>
					<a href="TodayColdChainHealth.jsp?LocationId=<%=sValue%>"><%= sValue %></a><br>
					<%}
					
				}%>
				</div>
								
												</div>
											</div>
									</div>
									<br><br>
									<div class="row">
									
				<div class="col-md-7 text-center">
				<div class="well well-sm">
				<b>TRENDS</b><br><br>
				<i><%= sSubZoneName %></i>
					<div class="row">
						
							<%
							String temperature = "", temperature1 = "", temperature2 = "", temperature3 = "", temperature5 = "", sMin1="", sMax1="", sMin2="", sMax2="";;
							String time = "", time1 = "", time2 = "", time3 = "", time5 = "";	
							HumidityDao hd = new HumidityDao();
							HumidityBean hdBean = new HumidityBean();
							DoorActivityDao daDao = new DoorActivityDao();
							DoorActivityBean daBean = new DoorActivityBean();
							TemperatureDao td = new TemperatureDao();
							TemperatureBean tdBean = new TemperatureBean();
							Collection col = null;
							if(sZoneType.equals(Constants.AMBIENT_NODE)) {
							col = td.getTemperatureData(sNodeId);
							Iterator iterSt = col.iterator();
							while(iterSt.hasNext()) {
								tdBean = (TemperatureBean) iterSt.next();
									d = df.parse(tdBean.getTimestamp());
									temperature += "\"" + tdBean.getTemperature() + "\", ";
									time += "\"" + sdf1.format(d) + "\", ";
									sMin1 += "\"" + iTempLowerLimit + "\", ";
									sMax1 += "\"" + iTempUpperLimit + "\", ";
							}
							if(temperature.equals("") || time.equals("")){
								temperature1 = "00";
								time1 = "00";
							} else {
								if(temperature.length() >= 2){
									temperature1 = temperature.substring(0, (temperature.length() - 2));
									sMin1 = sMin1.substring(0, (sMin1.length() - 2));
									sMax1 = sMax1.substring(0, (sMax1.length() - 2));
								}
									if(time.length() >= 2) time1 = time.substring(0, (time.length() - 2));
									
							}
								//logger.info("temperature1: " + temperature1);
								//logger.info("time1: " + time1);
								temperature = ""; time = "";
								//Enter GW_ClientId and ND_Client Id for Chart 2
								col = hd.getHumidityData(sNodeId);
								Iterator iterSh = col.iterator();
								while(iterSh.hasNext()) {
									hdBean = (HumidityBean) iterSh.next();
										d = df.parse(hdBean.getTimestamp());
										temperature += "\"" + hdBean.getHumidity() + "\", ";
										time += "\"" + sdf1.format(d) + "\", ";
										sMin2 += "\"" + iHumLowerLimit + "\", ";
										sMax2 += "\"" + iHumUpperLimit + "\", ";
								}
								if(temperature.equals("") || time.equals("")){
									temperature2 = "00";
									time2 = "00";
								} else {
									if(temperature.length() >= 2){
										temperature2 = temperature.substring(0, (temperature.length() - 2));
										sMin2 = sMin2.substring(0, (sMin2.length() - 2));
										sMax2 = sMax2.substring(0, (sMax2.length() - 2));
									}
										if(time.length() >= 2) time2 = time.substring(0, (time.length() - 2));
								}
							} else if(sZoneType.equals(Constants.DOOR_NODE)) {
								temperature = ""; time = "";
								//Enter GW_ClientId and ND_Client Id for Chart 3
							 	col = daDao.getDoorActivityData(sNodeId);
								Iterator<DoorActivityBean> iterDa = col.iterator();
								while(iterDa.hasNext()) {
									daBean = (DoorActivityBean) iterDa.next();
										d = df.parse(daBean.getTimestamp());
										temperature += "\"" + daBean.getDoorActivity() + "\", ";
										time += "\"" + sdf1.format(d) + "\", ";
									
								};
								
								if(temperature.equals("") || time.equals("")){
									temperature3 = "00";
									time3 = "00";
								} else {
									 if(temperature.length() >= 2)	temperature3 = temperature.substring(0, (temperature.length() - 2));
									   if(time.length() >= 2) time3 = time.substring(0, (time.length() - 2));
								}
							}%>
								
								<div class="col-md-6">
													<br><br>
													<%if(sZoneType.equals(Constants.AMBIENT_NODE)) {%>
								<div class="box box-danger">
								<%} else { %>
								<div class="box box-danger" style="display:none;">
								<%} %>
							<div class="box-header with-border">
														<h3 class="box-title">&nbsp;Temperature (&deg; C)</h3>
														<div class="box-tools pull-right">
															<button type="button" class="btn btn-box-tool"
																data-widget="collapse">
																<i class="fa fa-minus"></i>
															</button>
															<button type="button" class="btn btn-box-tool"
																data-widget="remove">
																<i class="fa fa-times"></i>
															</button>
														</div>
													</div>
													<div class="box-body">
														<%
															if(temperature1.equals("00")){
														%>
														No Data!
														<%
															} else {
														%>
														<div class="chart">
															<canvas id="lineChart1"
																style="height: 270px; width: 100px"></canvas>
														</div>
														<%
															}
														%>

													</div>
												</div>
												<%
												
												if(sZoneType.equals(Constants.DOOR_NODE)) {%>
								<div class="box box-danger">
								<%} else { %>
								<div class="box box-danger" style="display:none;">
								<%} %>
							<div class="box-header with-border">
														<h3 class="box-title">&nbsp;Door Activity</h3>
														<div class="box-tools pull-right">
															<button type="button" class="btn btn-box-tool"
																data-widget="collapse">
																<i class="fa fa-minus"></i>
															</button>
															<button type="button" class="btn btn-box-tool"
																data-widget="remove">
																<i class="fa fa-times"></i>
															</button>
														</div>
													</div>
													<div class="box-body">
														<%
															if(temperature3.equals("00")){
														%>
														No Data!
														<%
															} else {
														%>
														<div class="chart">
															<canvas id="lineChart3"
																style="height: 270px; width: 100px"></canvas>
														</div>
														<%
															}
														%>

													</div>
												</div>
						
												
							
						</div>
						<div class="col-md-6">
						 <br><br>
						 <%if(sZoneType.equals(Constants.AMBIENT_NODE)) {%>
								<div class="box box-danger">
								<%} else { %>
								<div class="box box-danger" style="display:none;">
								<%} %>
							<div class="box-header with-border">
														<h3 class="box-title">&nbsp;Relative Humidity (%)</h3>
														<div class="box-tools pull-right">
															<button type="button" class="btn btn-box-tool"
																data-widget="collapse">
																<i class="fa fa-minus"></i>
															</button>
															<button type="button" class="btn btn-box-tool"
																data-widget="remove">
																<i class="fa fa-times"></i>
															</button>
														</div>
													</div>
													<div class="box-body">
														<%
															if(temperature2.equals("00")){
														%>
														No Data!
														<%
															} else {
														%>
														<div class="chart">
															<canvas id="lineChart2"
																style="height: 270px; width: 100px"></canvas>
														</div>
														<%
															}
														%>

													</div>
												</div>
							
							
						</div>
						
																		</div>
					</div>
					</div>
					<div class="col-md-5 text-center">
				<div class="well well-sm">
				<b>ALERTS</b><br><br>
				
					<i><%= sSubZoneName %></i>
				
				<br>
				
													<br><br> 
													<div class="row">
									<div class="col-md-12">
										<div class="panel-group" id="accordion" role="tablist"
											aria-multiselectable="true">
											<%
											LinkedHashMap lhm = alertsWorkflowDao.getAllAlertsForSubzoneId(sNodeId, sAlertType, sTimeValue);
											if(lhm.size() == 0){%>
												No Alerts!
											<%}
												iter = lhm.keySet().iterator();
												AlertWorkflowBean ab = new AlertWorkflowBean();
												String sKey = "";
												while(iter.hasNext()){
													sKey = (String) iter.next();
													ab = (AlertWorkflowBean) lhm.get(sKey);
													if(ab.getAlert_Type().equals(Constants.TEMPERATURE)){
											%>
											<div class="panel col-no-padding3">
												<div class="panel-heading" role="tab"
													id="<%=ab.getAlert_ID()%>">
													<h4 class="panel-title">
														<div class="row" class="collapsed" role="button"
															data-toggle="collapse" data-parent="#accordion"
															href="#header<%=ab.getAlert_ID()%>" aria-expanded="false"
															aria-controls="header<%=ab.getAlert_ID()%>">
															<%
																if(ab.getAlert_Severity().equals("4") || ab.getAlert_Severity().equals("3")){
															%>
															<div class="col-md-1 alert-text text-center">
																<i class="fa fa-thermometer-three-quarters"></i>
															</div>
															<div class="col-md-1 alert-text"><span data-toggle="tooltip" data-placement="top" title="Alert Id"><%=ab.getAlert_ID()%></span></div>
															<div class="col-md-4 alert-text">
																Temperature has breached
																<%=ab.getBreach_Type()%>
																limit for over
																<%=ab.getAlert_Duration()%>
																minutes
															</div>
															<div class="col-md-2 alert-text"><span data-toggle="tooltip" data-placement="top" title="Assigned To"><%= ab.getAssigned_To() %></span></div>
															<%
																} else {
															%>
															<div class="col-md-1 text-center">
																<i class="fa fa-thermometer-three-quarters"></i>
															</div>
															<div class="col-md-1"><span data-toggle="tooltip" data-placement="top" title="Alert Id"><%=ab.getAlert_ID()%></span></div>
															<div class="col-md-4">
																Temperature 
																has breached
																<%=ab.getBreach_Type()%>
																limit for over
																<%=ab.getAlert_Duration()%>
																minutes
															</div>
															<div class="col-md-2"><span data-toggle="tooltip" data-placement="top" title="Assigned To"><%= ab.getAssigned_To() %></span></div>
															<%
																}
															%>
															<div class="col-md-2 text-right">
															<%d = df.parse(ab.getAlert_Timestamp_From()); 
															%>
															<span data-toggle="tooltip" data-placement="top" title="Alert Start Time"><%=sdf2.format(d) %></span>
															</div>
															<div class="col-md-1">
																<input id="btn<%=ab.getAlert_ID()%>" type="button"
																	class="btn btn-default btn-sm"
																	value="<%=ab.getAction_Status()%>">
															</div>
															
														</div>
													</h4>
												</div>
											</div>
											<%
												} else if(ab.getAlert_Type().equals(Constants.HUMIDITY)) {
											%>
											<div class="panel col-no-padding3">
												<div class="panel-heading" role="tab"
													id="<%=ab.getAlert_ID()%>">
													<h4 class="panel-title">
														<div class="row" class="collapsed" role="button"
															data-toggle="collapse" data-parent="#accordion"
															href="#header<%=ab.getAlert_ID()%>" aria-expanded="false"
															aria-controls="header<%=ab.getAlert_ID()%>">
															<%
																if(ab.getAlert_Severity().equals("4") || ab.getAlert_Severity().equals("3")){
															%>
															<div class="col-md-1 alert-text text-center">
																<span class="glyphicon glyphicon glyphicon-tint"></span>
															</div>
															<div class="col-md-1 alert-text"><span data-toggle="tooltip" data-placement="top" title="Alert Id"><%=ab.getAlert_ID()%></span></div>
															<div class="col-md-4 alert-text">
																Humidity 
																has breached
																<%=ab.getBreach_Type()%>
																limit for over
																<%=ab.getAlert_Duration()%>
																minutes
															</div>
															<div class="col-md-2 alert-text"><span data-toggle="tooltip" data-placement="top" title="Assigned To"><%= ab.getAssigned_To() %></span></div>
															<%
																} else {
															%>
															<div class="col-md-1 text-center">
																<span class="glyphicon glyphicon glyphicon-tint"></span>
															</div>
															<div class="col-md-1"><span data-toggle="tooltip" data-placement="top" title="Alert Id"><%=ab.getAlert_ID()%></span></div>
															<div class="col-md-4">
																Humidity 
																has breached
																<%=ab.getBreach_Type()%>
																limit for over
																<%=ab.getAlert_Duration()%>
																minutes
															</div>
															<div class="col-md-2"><span data-toggle="tooltip" data-placement="top" title="Assigned To"><%= ab.getAssigned_To() %></span></div>
															<%
																}
															%>
															<div class="col-md-2 text-right">
															<%d = df.parse(ab.getAlert_Timestamp_From()); 
															%>
															<span data-toggle="tooltip" data-placement="top" title="Alert Start Time"><%=sdf2.format(d) %></span>
															</div>
															<div class="col-md-1">
																<input id="btn<%=ab.getAlert_ID()%>" type="button"
																	class="btn btn-default btn-sm"
																	value="<%=ab.getAction_Status()%>">
															</div>
														</div>
													</h4>
												</div>
											</div>
											<%
												} else if(ab.getAlert_Type().equals(Constants.DOOR_ACTIVITY)) {
											%>
											<div class="panel col-no-padding3">
												<div class="panel-heading" role="tab"
													id="<%=ab.getAlert_ID()%>">
													<h4 class="panel-title">
														<div class="row" class="collapsed" role="button"
															data-toggle="collapse" data-parent="#accordion"
															href="#header<%=ab.getAlert_ID()%>" aria-expanded="false"
															aria-controls="header<%=ab.getAlert_ID()%>">
															<%
																if(ab.getAlert_Severity().equals("4") || ab.getAlert_Severity().equals("3")){
															%>
															<div class="col-md-1 alert-text text-center">
																<i class="fa fa-columns"></i>
															</div>
															<div class="col-md-1 alert-text"><span data-toggle="tooltip" data-placement="top" title="Alert Id"><%=ab.getAlert_ID()%></span></div>
															<div class="col-md-4 alert-text">
																Door 
																has been opened for over
																<%=ab.getAlert_Duration()%>
																minutes
															</div>
															<div class="col-md-2 alert-text"><span data-toggle="tooltip" data-placement="top" title="Assigned To"><%= ab.getAssigned_To() %></span></div>
															<%
																} else {
															%>
															<div class="col-md-1 text-center">
																<i class="fa fa-columns"></i>
															</div>
															<div class="col-md-1"><span data-toggle="tooltip" data-placement="top" title="Alert Id"><%=ab.getAlert_ID()%></span></div>
															<div class="col-md-4">
																Door 
																has been opened for over
																<%=ab.getAlert_Duration()%>
																minutes
															</div>
															<div class="col-md-2"><span data-toggle="tooltip" data-placement="top" title="Assigned To"><%= ab.getAssigned_To() %></span></div>
															<%
																}
															%>
															<div class="col-md-2 text-right">
															<%d = df.parse(ab.getAlert_Timestamp_From()); 
															%>
															<span data-toggle="tooltip" data-placement="top" title="Alert Start Time"><%=sdf2.format(d) %></span>
															</div>
															<div class="col-md-1">
																<input id="btn<%=ab.getAlert_ID()%>" type="button"
																	class="btn btn-default btn-sm"
																	value="<%=ab.getAction_Status()%>">
															</div>
														</div>
													</h4>
												</div>
											</div>
											<%
												}
											%>
											<br>
											<%
												if(ab.getAlert_Type().equals(Constants.TEMPERATURE)) {
											%>
											<div id="header<%=ab.getAlert_ID()%>"
												class="panel-collapse collapse" role="tabpanel"
												aria-labelledby="<%=ab.getAlert_ID()%>">
												<div class="panel-body">
													<div class="well">
														<div class="container">
															<div class="row">
																<div class="col-md-4">
																<div class="row">
																		<div class="col-md-6">
																	<div class="form-group">
																		<label>Average Event Temperature</label><br> <input
																			class="btn btn-default"
																			class="form-control-no-height" type="button"
																			value="<%=Utils.roundIt(ab.getAlert_Parameter_Avg()) %>&deg;C">
																	</div>
																		</div>
																		<div class="col-md-6">
																		<div class="form-group">

																		<label>Event Duration</label><br><br> <input
																			class="btn btn-default" type="button"
																			value="<%=ab.getAlert_Duration()%> minutes">
																	</div>
																		</div>
																	</div>
																	<div class="row">
																		<div class="col-md-6">
																	<div class="form-group">
																		<label>Assigned To</label><br> <input type="text" id="assigned_to_<%=ab.getAlert_ID()%>"
																			class="form-control-no-height" value="<%= ab.getAssigned_To() %>">
																	</div>
																	</div>
																	<div class="col-md-6">
																	<div class="form-group">
																		<label>Supervisor</label><br> <input type="text"
																			class="form-control-no-height" value="Nayan Patil">
																	</div>
																	</div>
																	</div>
																	
																	<input class="btn btn-success" type="button"
																		value="In Progress"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'In Progress')">
																	<input class="btn btn-success" type="button"
																		value="On Hold"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'On Hold')">
																	<input class="btn btn-success" type="button"
																		value="Close"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'Closed')">
																	<br> <br>
																	
																	<input class="btn btn-success" type="button"
																		value="Escalate"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'Escalated')">
																	<input class="btn btn-success" type="button"
																		value="Reassign"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'Reassigned')">
																	<input class="btn btn-success" type="button"
																		value="Reopen"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'Reopened')">
																</div>
																</div>
																<div class="row">
																<div class="col-md-4">
																	<br>
																	<div class="row">
																		<div class="col-md-6">
																		<div class="form-group">
																		<label>Alert Root Cause</label><br>
																		<textarea class="form-control" rows="3"
																			id="txtRoot<%=ab.getAlert_ID()%>"><%=ab.getRoot_Cause()%></textarea>
																	</div>
																		</div>
																		<div class="col-md-6">
																		<div class="form-group">
																		<label>Preventive Actions</label><br>
																		<textarea class="form-control" rows="3"
																			id="txtPre<%=ab.getAlert_ID()%>"><%=ab.getPreventive_Actions()%></textarea>
																	</div>
																		</div>
																	</div>
																	<div class="form-group text-center">
																		<input class="btn btn-success" type="button"
																			value="Save"
																			onclick="updateRoot('<%=ab.getAlert_ID()%>')"
																			data-toggle="modal" data-target="#myModal">
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<%
												}  else if(ab.getAlert_Type().equals(Constants.HUMIDITY)) {
											%>
											<div id="header<%=ab.getAlert_ID()%>"
												class="panel-collapse collapse" role="tabpanel"
												aria-labelledby="<%=ab.getAlert_ID()%>">
												<div class="panel-body">
													<div class="well">
														<div class="container">
															<div class="row">
																<div class="col-md-4">
																	<div class="row">
																		<div class="col-md-6">
																	<div class="form-group">
																		<label>Average Humidity</label><br> <input
																			class="btn btn-default"
																			class="form-control-no-height" type="button"
																			value="<%= Utils.roundIt(ab.getAlert_Parameter_Avg()) %>%">
																	</div>	
																		</div>
																		<div class="col-md-6">
																		<div class="form-group">

																		<label>Event Duration</label><br> <input
																			class="btn btn-default" type="button"
																			value="<%=ab.getAlert_Duration()%> minutes">
																	</div>
																		</div>
																	</div>
																	<div class="row">
																		<div class="col-md-6">
																	<div class="form-group">
																		<label>Assigned To</label><br> <input type="text" id="assigned_to_<%=ab.getAlert_ID()%>"
																			class="form-control-no-height" value="<%= ab.getAssigned_To() %>">
																	</div>
																	</div>
																	<div class="col-md-6">
																	<div class="form-group">
																		<label>Supervisor</label><br> <input type="text"
																			class="form-control-no-height" value="Nayan Patil">
																	</div>
																	</div>
																	</div>
																	<input class="btn btn-success" type="button"
																		value="In Progress"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'In Progress')">
																	<input class="btn btn-success" type="button"
																		value="On Hold"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'On Hold')">
																	<input class="btn btn-success" type="button"
																		value="Close"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'Closed')">
																	<br> <br>
																	
																	<input class="btn btn-success" type="button"
																		value="Escalate"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'Escalated')">
																	<input class="btn btn-success" type="button"
																		value="Reassign"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'Reassigned')">
																	<input class="btn btn-success" type="button"
																		value="Reopen"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'Reopened')">
																</div>
																</div>
																<div class="row">
																<div class="col-md-4">
																<br>
																	<div class="row">
																		<div class="col-md-6">
																		<div class="form-group">
																		<label>Alert Root Cause</label><br>
																		<textarea class="form-control" rows="3"
																			id="txtRoot<%=ab.getAlert_ID()%>"><%=ab.getRoot_Cause()%></textarea>
																	</div>
																		</div>
																		<div class="col-md-6">
																		<div class="form-group">
																		<label>Preventive Actions</label><br>
																		<textarea class="form-control" rows="3"
																			id="txtPre<%=ab.getAlert_ID()%>"><%=ab.getPreventive_Actions()%></textarea>
																	</div>
																		</div>
																	</div>
																	
																	
																	<div class="form-group text-center">
																		<input class="btn btn-success" type="button"
																			value="Save"
																			onclick="updateRoot('<%=ab.getAlert_ID()%>')"
																			data-toggle="modal" data-target="#myModal">
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<%
												} else if(ab.getAlert_Type().equals(Constants.DOOR_ACTIVITY)) {
											%>
											<div id="header<%=ab.getAlert_ID()%>"
												class="panel-collapse collapse" role="tabpanel"
												aria-labelledby="<%=ab.getAlert_ID()%>">
												<div class="panel-body">
													<div class="well">
														<div class="container">
															<div class="row">
																<div class="col-md-4">
																<div class="row">
																		<div class="col-md-12">
																	<div class="form-group">
																		<label>Door Open Duration</label><br> <input
																			class="btn btn-default" type="button"
																			value="<%=ab.getAlert_Duration()%> minutes">
																	</div>
																		</div>
																		
																	</div>
																	<div class="row">
																		<div class="col-md-6">
																	<div class="form-group">
																		<label>Assigned To</label><br> <input type="text" id="assigned_to_<%=ab.getAlert_ID()%>"
																			class="form-control-no-height" value="<%= ab.getAssigned_To() %>">
																	</div>
																	</div>
																	<div class="col-md-6">
																	<div class="form-group">
																		<label>Supervisor</label><br> <input type="text"
																			class="form-control-no-height" value="Nayan Patil">
																	</div>
																	</div>
																	</div>
																	
																	<input class="btn btn-success" type="button"
																		value="In Progress"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'In Progress')">
																	<input class="btn btn-success" type="button"
																		value="On Hold"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'On Hold')">
																	<input class="btn btn-success" type="button"
																		value="Close"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'Closed')">
																	<br> <br>
																	
																	<input class="btn btn-success" type="button"
																		value="Escalate"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'Escalated')">
																	<input class="btn btn-success" type="button"
																		value="Reassign"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'Reassigned')">
																	<input class="btn btn-success" type="button"
																		value="Reopen"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'Reopened')">
																</div>
																</div>
																<div class="row">
																<div class="col-md-4">
																	<div class="row">
																		<div class="col-md-6">
																		<div class="form-group">
																		<label>Alert Root Cause</label><br>
																		<textarea class="form-control" rows="3"
																			id="txtRoot<%=ab.getAlert_ID()%>"><%=ab.getRoot_Cause()%></textarea>
																	</div>
																		</div>
																		<div class="col-md-6">
																		<div class="form-group">
																		<label>Preventive Actions</label><br>
																		<textarea class="form-control" rows="3"
																			id="txtPre<%=ab.getAlert_ID()%>"><%=ab.getPreventive_Actions()%></textarea>
																	</div>
																		</div>
																	</div>
																	
																	
																	<div class="form-group text-center">
																		<input class="btn btn-success" type="button"
																			value="Save"
																			onclick="updateRoot('<%=ab.getAlert_ID()%>')"
																			data-toggle="modal" data-target="#myModal">
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<%
												}
											}
											%>
										</div>
									</div>
								</div>
					
					</div>
					</div>
				</div>
									
									
								</div>
							</div>
						
		</section>
		

<div class="modal fade" tabindex="-1" role="dialog" id="myModal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header alert-info">Updated Data!</div>
					<div class="modal-body">Updated Alert Root Cause and
						Preventive Actions Successfully!</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>

				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		</div>
		<!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0
			</div>
			<strong>Copyright &copy; 2017 <a href="http://tagbox.in">TagBox</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Control Sidebar -->
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
	

	<!-- Bootstrap 3.3.6 -->
	<script src="../../bootstrap/js/bootstrap.min.js"></script>
	<!-- ChartJS 1.0.1 -->
	<!-- AdminLTE App -->
	<script src="../../dist/js/app.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="../../dist/js/demo.js"></script>
<script>
Chart.defaults.global.legend.display = false;


var barOptions_stacked_temp = {
		 responsive: true,
    	 maintainAspectRatio: false,
	    tooltips: {
	        enabled: true
	    },
	    legend:{
	        display:false
	    },
	    elements: {
            point:{
                radius: 1
            }
        },
	    scales: {
	        xAxes: [{
	                    gridLines: {
	                        display:false
	                    },
	                    ticks: {
	                        autoSkip: true,
	                        maxTicksLimit: 20
	                    }
	                }],
	        yAxes: [{
	                    gridLines: {
	                        display:false
	                    },
	        			ticks: {
	        				stepSize: 10,
	        				suggestedMin: <%= (iTempLowerLimit - 10)%>,
	        				suggestedMax: <%= (iTempUpperLimit + 10)%>
	        			}
	                }]
	        },
	        fill: false,
            lineTension: 0,
            backgroundColor: "rgba(75,192,192,0.4)",
            borderColor: "rgba(75,192,192,1)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            spanGaps: false,
	};
	
var barOptions_stacked_hum = {
		 responsive: true,
    	 maintainAspectRatio: false,
	    tooltips: {
	        enabled: true
	    },
	    legend:{
	        display:false
	    },
	    elements: {
            point:{
                radius: 1
            }
        },
	    scales: {
	        xAxes: [{
	                    gridLines: {
	                        display:false
	                    },
	                    ticks: {
	                        autoSkip: true,
	                        maxTicksLimit: 20
	                    }
	                }],
	        yAxes: [{
	                    gridLines: {
	                        display:false
	                    },
	        			ticks: {
	        				stepSize: 10,
	        				suggestedMin: <%= (iHumLowerLimit - 10)%>,
	        				suggestedMax: <%= (iHumUpperLimit + 10)%>
	        			}
	                }]
	        },
	        fill: false,
            lineTension: 0,
            backgroundColor: "rgba(75,192,192,0.4)",
            borderColor: "rgba(75,192,192,1)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            spanGaps: false,
	};
	
var ctx2 = document.getElementById("lineChart1");
var myChart2 = new Chart(ctx2, {
    type: 'line',
    data: {
    	labels : [<%=time1%>],
		datasets : [ {
			label : ["Temperature"],
			fill: false,
			borderColor: "black",
			data : [<%=temperature1%>]
		}, 
		{
			label : ["Min Threshold"],
			fill: false,
			borderColor: "orange",
			data : [<%= sMin1 %>]
		}, {
			label : ["Max Threshold"],
			fill: false,
			borderColor: "orange",
			data : [<%= sMax1 %>]
		}]
    },
    options: barOptions_stacked_temp
});

var ctx2 = document.getElementById("lineChart2");
var myChart2 = new Chart(ctx2, {
    type: 'line',
    data: {
    	labels : [<%=time2%>],
		datasets : [ {
			label : ["Humidity"],
			fill: false,
			borderColor: "black",
			data : [<%=temperature2%>]
		}, 
		{
			label : ["Min Threshold"],
			fill: false,
			borderColor: "orange",
			data : [<%= sMin2 %>]
		}, {
			label : ["Max Threshold"],
			fill: false,
			borderColor: "orange",
			data : [<%= sMax2 %>]
		}]
    },
    options: barOptions_stacked_hum
});

</script>
	</body>
</html>
