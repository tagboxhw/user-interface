<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="com.licious.dao.AlertsReportDao"%>
<%@ page import="com.licious.dao.ClientLocationMapDao"%>
<%@ page import="com.licious.util.Constants"%>
<%@ page import="com.licious.bean.TemperatureTriggersBean"%>
<%@ page import="com.licious.bean.TemperatureBean"%>
<%@ page import="com.licious.dao.TemperatureDao"%>
<%@ page import="com.licious.bean.ClientSubLocationMapBean"%>
<%@ page import="java.util.Collection"%>
<%@ page import="java.util.Vector"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.util.TimeZone"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>
<%@ page import="java.util.TreeMap"%>
<%@ page import="java.util.Set"%>
<%@ page import="java.util.concurrent.TimeUnit"%>

<%
response.setHeader("Cache-Control","no-cache"); 
response.setHeader("Pragma","no-cache"); 
response.setDateHeader ("Expires", -1); 
%>
<%!
static final Logger logger = LoggerFactory.getLogger("HistoricalData");
AlertsReportDao alertsReportDao = new AlertsReportDao();
ClientLocationMapDao clientLocationMapDao = new ClientLocationMapDao();
TemperatureBean temperatureBean = new TemperatureBean();
Collection<TemperatureBean> col = new Vector<TemperatureBean>();
ClientSubLocationMapBean clientSubLocationMapBean = new ClientSubLocationMapBean();
Collection<ClientSubLocationMapBean> col1 = new Vector<ClientSubLocationMapBean>();
%>
<%
String sUsername = (String) session.getAttribute("username");
if(sUsername == null || sUsername.equals("")) {
	session.setAttribute("callingPage", "HistoricalData.jsp");
	response.sendRedirect("../../index.html");
}
Date d2 = null;
Long newTime = null;
java.util.Date dt = new java.util.Date();
java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm");
sdf.setTimeZone(TimeZone.getTimeZone("IST"));
String currentTime = sdf.format(dt);
sUsername = (String) session.getAttribute("username");
String sFromDate = (String) request.getParameter("StartDate");
if(sFromDate == null || sFromDate.equals("")) sFromDate = "07/31/2017";
String sZoneId = (String) request.getParameter("Zone");
String sNodeId = (String) request.getParameter("SubZone");
String sToDate = (String) request.getParameter("EndDate");
if(sToDate == null || sToDate.equals("")) sToDate = currentTime.substring(0, 10);
col1 = clientLocationMapDao.selectSubZones();
String sTempLocationId = "", sTempZoneId = "", sTempNodeId = "", sValue = "";
TreeMap<String, String> tmZone = new TreeMap<String, String>();
String sAllNodeId = "";
for(ClientSubLocationMapBean alBean: col1){
	if(sZoneId == null || sZoneId.equals("")) sZoneId = alBean.getLocationId();
	if(sNodeId == null || sNodeId.equals("")) sNodeId = alBean.getSubLocationId();
	
	if(alBean.getLocationId().equals(sZoneId)) { 
		sTempZoneId = alBean.getLocationId();
		if(sNodeId.equals("All")) {
			sAllNodeId = sAllNodeId + ",'" + alBean.getSubLocationId() + "'";
		}
		if(alBean.getSubLocationId().equals(sNodeId)) sTempNodeId = alBean.getSubLocationId();
	}
	sValue = "";
	if(tmZone.containsKey(alBean.getLocationId())) {
		sValue = tmZone.get(alBean.getLocationId());
		if(sValue.indexOf(alBean.getSubLocationId()) >= 0) {
			
		} else {
			sValue = sValue + ";" + alBean.getSubLocationId();
			tmZone.put(alBean.getLocationId(), sValue);
		}
	} else {
		tmZone.put(alBean.getLocationId(), alBean.getSubLocationId());
	}
}
if(sTempNodeId.equals("")) sTempNodeId = sNodeId;
Date d = null;
SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy");
df.setTimeZone(java.util.TimeZone.getTimeZone("UTC"));
sdf2.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
if(sZoneId.equals("All")) {
	col = alertsReportDao.getHistoricalTemperatureData(sFromDate, sToDate, sAllNodeId.substring(1));
} else {
	col = alertsReportDao.getHistoricalTemperatureData(sFromDate, sToDate, sZoneId);
}
TemperatureDao tDao = new TemperatureDao();
if(tDao.tempThresholds == null || tDao.tempThresholds.size() == 0) tDao.getTemperatureThresholds();
int iTempUpperLimit = 0, iTempLowerLimit = 0;

%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Historical Data</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- Add these below two lines for autocomplete and the javascript function at the end  -->
 <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> 
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="../../bootstrap/css/bootstrap.css">
   
<!-- Font Awesome -->
<script src="https://use.fontawesome.com/37ba1872d2.js"></script>
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../../dist/css/tagbox.css">
<link href="../../dist/css/select2.css" rel="stylesheet" />
<script src="../../dist/js/select2.js"></script>
<script src="../../dist/js/tagbox.js"></script>
<link rel="stylesheet" href="../../dist/css/timepicker1.css">
<script src="../../dist/js/table2download.js"></script>
<script type="text/javascript" src="../../plugins/tableExport/tableExport.js"></script>
<script type="text/javascript" src="../../plugins/tableExport/jquery.base64.js"></script>
<script type="text/javascript" src="../../plugins/tableExport/jspdf/libs/sprintf.js"></script>
<script type="text/javascript" src="../../plugins/tableExport/jspdf/jspdf.js"></script>
<script type="text/javascript" src="../../plugins/tableExport/jspdf/libs/base64.js"></script>
<script type="text/javascript" src="../../plugins/tableExport/html2canvas.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.3/jspdf.debug.js"></script>
<script src="../../dist/js/autoTable.js"></script>

<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
<link href="https://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet">
<link rel="stylesheet" href="../../dist/css/timepicker1.css">
<script src="../../dist/js/location.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#Zone").select2();
});
</script>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
  body {
	font-family:  Lato, sans-serif;
	font-weight: 600;
}
</style>
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
	<div class="wrapper">

		<header class="main-header">
			<!-- Logo -->
			<div>
				<a href="#" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
					<span class="logo-mini"><img src="../../dist/img/licious.png" style="margin-top:14px"
						alt="User Image"></span> <!-- logo for regular state and mobile devices -->
					<span class="logo-lg"><img src="../../dist/img/licious.png" 
						alt="User Image"></span>
				</a>
			</div>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">

				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
					role="button"> <span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
				</a>
				<form class="navbar-form pull-left" role="search">
					<div class="form-group">
						<b style="padding-left: 15px; font-size: 20px;color:#fff;" class="pull-right">Historical Data</b><br> <b style="padding-left: 15px;color:#fff;"><%= currentTime %></b>
					</div>
				</form>

				<div class="navbar-custom-menu">
					<img src="../../dist/img/Logo.png" class="logo-class pull-right"
						alt="User Image">
				</div>
			</nav>

		</header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->


				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<br>
				<ul class="sidebar-menu">
					<li class="header">MAIN NAVIGATION</li>
					<li class="treeview"><a href="#"> <i
							class="fa fa-dashboard"></i> <span>Dashboard</span>
					</a>
						<ul class="treeview-menu">
							<li class="">
							<%if(sUsername != null && sUsername.equals("demo2")) { %>
							<a href="CMCDashboard.jsp"><i
									class="fa fa-circle-o"></i> CMC</a>
							<%} else { %>
							<a href="CMCDashboard.jsp" role="button" class="btn btn-disabled disabled"><i
									class="fa fa-circle-o"></i> CMC</a>
							<%} %>
							</li>
							<!-- <li class=""><a href="NewDashboard.jsp"><i
									class="fa fa-circle-o"></i> Vehicle Dashboard</a></li> -->
							<li class=""><a href="TodayColdChainHealth.jsp"><i
									class="fa fa-circle-o"></i> Cold Rooms Summary</a></li>
							<!--  <li><a href="VehiclePlannedRouteMap.jsp"><i class="fa fa-circle-o"></i> Vehicle Planned Route</a></li> -->
							<li class=""><a href="TodayColdChainHealthVehicle.jsp" role="button" class="btn btn-disabled disabled"><i
									class="fa fa-circle-o"></i> Vehicles Summary</a></li>
						</ul></li>
					<li class="treeview"><a href="#"> <i
							class="fa fa-files-o"></i> <span>Alerts</span> 
					</a>
						<ul class="treeview-menu">
							<li><a href="LiveVehicleTracking.jsp" role="button" class="btn btn-disabled disabled"><i
									class="fa fa-circle-o"></i> Live Vehicle Tracking</a></li>
						</ul></li>
					
					<li class="treeview"><a href="#"> <i
							class="fa fa-database"></i> <span>Admin</span>
					</a>
						<ul class="treeview-menu">
							<li class=""><a href="VehicleManagement.jsp" role="button" class="btn btn-disabled disabled"><i class="fa fa-circle-o"></i>
									Vehicle Management</a></li>
							<li><a href="VendorScorecard.jsp" role="button" class="btn btn-disabled disabled"><i class="fa fa-circle-o"></i>
									Vehicle Score Card</a></li>
						</ul></li>
						<li class="treeview"><a href="#"> <i
							class="fa fa-bar-chart"></i> <span>Reports</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="ParameterSummary.jsp"><i class="fa fa-circle-o"></i>
									Parameter Summary</a></li>
							<li><a href="AlertsSummary.jsp"><i class="fa fa-circle-o"></i>
									Alerts Summary</a></li>
							<li><a href="AlertsReport.jsp"><i class="fa fa-circle-o"></i>
									Alerts Report</a></li>		
							
						</ul></li>
						<li class="treeview active"><a href="#"> <i
							class="fa fa-history"></i> <span>Historical Data</span>
					</a>
						<ul class="treeview-menu">
							<li class="active"><a href="HistoricalData.jsp"><i class="fa fa-circle-o"></i>
									Download <br>Temperature Data</a></li>
						</ul></li>
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			
		<section id="mainSection" class="content">
		<div class="row">
			<div class="col-md-12">
		<div class="well well-sm white-background">
							
						<br>
						<form id="myForm3" name="myForm3" method="post"
											action="HistoricalData.jsp">
						<%
						for(TemperatureTriggersBean alBean: tDao.tempThresholds) {%>
						<input type="hidden" name="<%= alBean.getGW_client_id()%>_upper" id="<%= alBean.getGW_client_id()%>_upper" value="<%=alBean.getThreshold_UL() %>">
						<input type="hidden" name="<%= alBean.getGW_client_id()%>_lower" id="<%= alBean.getGW_client_id()%>_lower" value="<%=alBean.getThreshold_LL() %>">
						<%}

						%>
											<div class="row">
				<div class="col-md-12">
				<div class="row">
					<div class="col-md-offset-3 col-md-6 text-center" id="time-range">
					<div class="well well-sm" id="time-range">
					<div class="row">
					<div class="col-md-3 text-center">
					<label><b>Zone</b></label>&nbsp;&nbsp;<br>
					<select id="Zone" name="Zone" class="form-control-no-background pull-center">
					<%
					Set set = tmZone.keySet();
					Iterator iter = set.iterator();
					while(iter.hasNext()) {
						sValue = (String) iter.next();
					
						if(sValue.equals(sZoneId)) {
						%>
							<option value="<%= sValue %>" selected><%= sValue %></option>
						<%} else {%>
						<option value="<%= sValue %>"><%= sValue %></option>
						<%}
						}
					%>
					</select>
					</div>
					<!-- <div class="col-md-2 text-center">
					<label><b>Sub Zone</b></label>&nbsp;&nbsp;<br>
					<select id="SubZone" name="SubZone" class="form-control-no-background pull-center">
					<%
					sValue = (String) tmZone.get(sZoneId);
					String[] sZone = sValue.split(";");
						for(int i =0; i<sZone.length; i++) {
							if(sNodeId.equals(sZone[i])) {	
						
						%>
							<option value="<%= sZone[i] %>" selected><%= sZone[i] %></option>
						<%} else {%>
						<option value="<%= sZone[i] %>"><%= sZone[i] %></option>
						<%} 
						}
					
					%>
					</select>
					</div> -->
						<div class="col-md-3">
						<label><b>Start Date: </b></label><input class='form-control-no-height' placeholder="Start Date" type="text" id="StartDate" name="StartDate" value="<%=sFromDate%>">
						</div>
						<div class="col-md-3">
						<label><b>End Date: </b></label><input class='form-control-no-height' placeholder="End Date" type="text" id="EndDate" name="EndDate" value="<%=sToDate%>">
    
						</div>
						<div class="col-md-3 text-center"><br>
						<button type="button" class="btn btn-success" onclick="checkDate()">GO</button>
						</div>
					</div>
    				</div>
					</div>
					</div>
				<div class="alert alert-danger text-center" role="alert" id="alertFailure" style="display:none;"></div>
					<br><br>
					
					<div class="row">
						<div class="col-md-offset-1 col-md-10">
						<div>
						<!-- <span id="csvLink1"></span>&nbsp;&nbsp;&nbsp;&nbsp;<span id="xlsLink1"></span>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" onClick ="$('#temp1_table').tableExport({type:'pdf',escape:'false'});" style="text-decoration:underline">Export to PDF</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" style="text-decoration:underline">Email</a> -->
						<div class="btn-group pull-right">
							<button class="btn btn-warning btn-sm dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Export Data</button>
							<ul class="dropdown-menu " role="menu">
								<!-- <li><a href="#" onClick ="$('#temp1_table').tableExport({type:'json',escape:'false'});"> <img src='../../dist/img/json.png' width='24px'> JSON</a></li>
								<li><a href="#" onClick ="$('#temp1_table').tableExport({type:'sql',escape:'false'});"> <img src='../../dist/img/sql.png' width='24px'> SQL</a></li>
								<li class="divider"></li>
								<li><a href="#" onClick ="$('#temp1_table').tableExport({type:'xml',escape:'false'});"> <img src='../../dist/img/xml.png' width='24px'> XML</a></li> -->
								<li><a href="#" onClick ="$('#temp1_table').tableExport({type:'csv',escape:'false'});"> <img src='../../dist/img/csv.png' width='24px'> CSV</a></li>
								<li><a href="#" onClick ="$('#temp1_table').tableExport({type:'excel',escape:'false'});"> <img src='../../dist/img/xls.png' width='24px'> XLS</a></li>
								<li class="divider"></li>				
								<li><a href="#" onClick ="$('#temp1_table').tableExport({type:'txt',escape:'false'});"> <img src='../../dist/img/txt.png' width='24px'> TXT</a></li>
								<!-- <li><a href="#" onClick ="$('#temp1_table').tableExport({type:'doc',escape:'false'});"> <img src='../../dist/img/word.png' width='24px'> Word</a></li>
								<li class="divider"></li> -->
								<!-- <li><a href="#" onClick ="$('#temp1_table').tableExport({type:'pdf',pdfFontSize:'7',escape:'false'});"> <img src='../../dist/img/pdf.png' width='15px'> PDF</a></li> -->
								<li><a href="#" onClick ="exportData()"> <img src='../../dist/img/pdf.png' width='15px'> PDF</a></li>
							</ul>
						</div>		
					</div><br><br>
						<table id="temp1_table" class="table table-condensed table-bordered table-striped">
									<thead>
									<tr class="blue text-center">
									<th width="150" class="text-center">Monitoring Point</th>
									<th width="100" class="text-center">Time</th>
									<th width="200" class="text-center">Temperature</th>
																		</tr></thead>
																		<tbody>
									<%
									String sTempDate = "";
									for(TemperatureBean temperatureBean: col){ 
										d = df.parse(temperatureBean.getTimestamp());
										sTempDate = sdf2.format(d);
										for(TemperatureTriggersBean alBean: tDao.tempThresholds) {
											if(temperatureBean.getNode_Id().equals(alBean.getGW_client_id())) {
												iTempUpperLimit = alBean.getThreshold_UL();
												iTempLowerLimit = alBean.getThreshold_LL();
												break;
											}
										}
										if(Float.parseFloat(temperatureBean.getTemperature()) < iTempLowerLimit || Float.parseFloat(temperatureBean.getTemperature()) > iTempUpperLimit) {
									%>
									<tr class="text-center danger">
									<%} else { %>
									<tr class="text-center">
									<%} %>
										<td><%= temperatureBean.getNode_Id()%></td>
										<td><%=sdf.format(df.parse(temperatureBean.getTimestamp())) %></td>
										<td><%=temperatureBean.getTemperature() %></td>
										</tr>
									<%
									}%>
									</tbody>
									</table>
						</div>
					</div>
					
									
						</div>
						
									</div>							
								</form>
					
					
					
						
								</div>
								</div>
								</div>
						
		</section>
		</div>
							</div>
		
		<!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0
			</div>
			<strong>Copyright &copy; 2017 <a href="http://tagbox.in">TagBox</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Control Sidebar -->
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->
<!-- ChartJS 1.0.1 -->
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
<script src="../../dist/js/timepicker1.js"></script>

	<!-- Bootstrap 3.3.6 -->
	<script src="../../bootstrap/js/bootstrap.min.js"></script>
	<!-- ChartJS 1.0.1 -->
	<!-- AdminLTE App -->
	<script src="../../dist/js/app.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="../../dist/js/demo.js"></script>
	
	<script type="text/javascript">
	
	$('#StartDate').datetimepicker({
		oneLine: true,
		controlType: 'select'
	}); 
	
	$('#EndDate').datetimepicker({
		oneLine: true,
		maxDate:0,
		controlType: 'select'
	});
	function exportData(){
	    var pdf = new jsPDF('p', 'pt', 'letter');
	  
		//creates PDF from img
		pdf.setFontSize(20);
		pdf.text(20, 20, "Temperature");
	    canvas = null;
	     var elem = document.getElementById("temp1_table");
	     var res = pdf.autoTableHtmlToJson(elem, true);
	     pdf.autoTable(res.columns, res.data, {startY: 100,createdCell: function(cell, data) {
	    	 //data.row will give the row details. data.row.index is the row number. Or we can print to console data json object to check further details
	    	 //console.log(JSON.stringify(data.row))
	    	 
	    	 if (data.column.dataKey == "2") { 
	    		 var a = data.row.cells[0].text;
	    		 b = $("#"+a+"_upper").val();
	    		 c = $("#"+a+"_lower").val();
	    		 if(parseInt(data.row.cells[2].text) < parseInt(c) || parseInt(data.row.cells[2].text) > parseInt(b)) {
	    			 data.row.cells[0].styles.fillColor = [242,222,222];
	    			 data.row.cells[1].styles.fillColor = [242,222,222];
	    			 data.row.cells[2].styles.fillColor = [242,222,222];
	    		 }
	    	 }
	     }
	     });
	     pdf.save('Export.pdf');
		
	    //Adding the html table to the same pdf file 
//	     source = $('#table_div')[0];
//	     specialElementHandlers = {
//	         '#bypassme': function (element, renderer) {
//	             return true;
//	         }
//	     };
//	     margins = {
//	         top: 300, // Providing high top margin value to make the table below to the image
//	         bottom: 60,
//	         left: 80,
//	         width: 400
//	     };
//	     pdf.fromHTML(
//	     source, 
//	     margins.left, 
//	     margins.top, { 
//	         'width': margins.width, 
//	         'elementHandlers': specialElementHandlers
//	     },

//	     function (dispose) {
//	        pdf.save('Export.pdf');
//	    }, margins);
	}

	function ApplyCellStyle(cell, x, y) {
		
	    var styles = tableCellsStyles[x + "-" + y]; // Retrieve the cell's style
	    if (styles === undefined)
	        return;
	   // Apply the cell's style.
	    cell.styles.cellPadding = styles.cellPadding
	    cell.styles.fillColor = styles.fillColor;
	    cell.styles.textColor = styles.textColor;
	    cell.styles.font = styles.font;
	    cell.styles.color = styles.color;
	    cell.styles.fontStyle = styles.fontStyle;
	}
</script>
	</body>
</html>
