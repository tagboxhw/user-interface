<html>
    <head>
        <meta charset='UTF-8'>
<script type="text/javascript">
     // Converts canvas to an image
        function convertCanvasToImage(canvasId) {
        	var image = new Image();
            var canvas=document.getElementById(canvasId);

            image.src = canvas.toDataURL("image/png");
            //return image;

            document.getElementById("wrap1").appendChild(image);
        }

        function drawCanvas() {
            var c = document.getElementById("canvasresult");

            h=parseInt(document.getElementById("canvasresult").getAttribute("height"));
            w=parseInt(document.getElementById("canvasresult").getAttribute("width"));

            //get context
            var ctx = c.getContext("2d");

            //Fill the path
            ctx.fillStyle = "#ffffff";
            ctx.fillRect(0,0,w,h);

            var img=document.getElementById("persphoto");
            ih=parseInt(document.getElementById("persphoto").getAttribute("height"));
            iw=parseInt(document.getElementById("persphoto").getAttribute("width"));

            var maxw = 150;
            var maxh = 150;

            if (ih > iw) {
                var newh = 150;
                var neww = Math.round(150 * (iw / ih));
            } else if (ih < iw) {
                var newh = Math.round(150 * (ih / iw));
                var neww = 150;
            } else {
                var newh = 150;
                var neww = 150;
            }

            var newx = Math.round((296 - neww) / 2);
            var newy = 60 + Math.round((150 - newh) / 2);

            img.onload = function() {
                ctx.drawImage(img,newx,newy,neww,newh);
            };


            ctx.fillStyle = "#000000";
            ctx.font = '10pt Verdana';
            ctx.textAlign = 'center';
            ctx.fillText(document.getElementById("persname").value, w/2, h*0.65);
            ctx.fillText(document.getElementById("adress").value, w/2, h*0.69);
            ctx.fillText(document.getElementById("postaddr").value, w/2, h*0.73);
            ctx.fillText(document.getElementById("homephone").value, w/2, h*0.77);
            ctx.fillText(document.getElementById("cellphone").value, w/2, h*0.81);


            var canvasData = c.toDataURL("image/png");
            /*
            document.getElementById("canvasdata").value = canvasData;
            document.getElementById("hidepersphoto").value = document.getElementById("persphoto").value;
            document.getElementById("hidepersname").value = document.getElementById("persname").value;
            document.getElementById("hideadress").value = document.getElementById("adress").value;
            document.getElementById("hidepostaddr").value = document.getElementById("postaddr").value;
            document.getElementById("hidehomephone").value = document.getElementById("homephone").value;
            document.getElementById("hidecellphone").value = document.getElementById("cellphone").value;
            */
        }
        </script>
    </head>

    <body>
        <div class="wrapper" id="wrap1">

            <img id="persphoto" src="../../dist/img/temperature.png">

            <input type="hidden" id="persname" value="Test Testinen">
            <input type="hidden" id="adress" value="Testgatan 1">
            <input type="hidden" id="postaddr" value="12345 Teststad">
            <input type="hidden" id="homephone" value="0123456789">
            <input type="hidden" id="cellphone" value="0712345678">

            <canvas src="" id="canvasresult" width="296px" height="420px" />
            <br>
        </div>
        <script type="text/javascript"> drawCanvas(); </script>
        <script type="text/javascript"> convertCanvasToImage("canvasresult"); </script>
        
    </body>
</html>