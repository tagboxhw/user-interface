<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ page import="com.licious.action.LocationAction"%>
<%@ page import="com.licious.dao.ClientLocationMapDao"%>
<%@ page import="com.licious.dao.AlertsWorkflowDao"%>
<%@ page import="com.licious.dao.TransitDao"%>
<%@ page import="com.licious.bean.LocationBean"%>
<%@ page import="com.licious.bean.SubLocationBean"%>
<%@ page import="com.licious.util.Utils"%>
<%@ page import="java.util.TreeMap"%>
<%@ page import="java.util.LinkedHashMap"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.util.Collection"%>
<%@ page import="java.util.Set"%>
<%!LocationAction locAction = new LocationAction();
	String buf = "";
	int iColSize = 0;
	String sErrorMessage = null;
	String sId = null;
	String sRowId = null;
	String sKey = "";
	String sLocation = "";
	String sSubLocation = "";
	String sZone = "";
	String sColor = "";
	Collection colLocation;
	Collection colSubLocation;
	Collection colZone;
	String[] sZones;
	String[] sLoc = null;
	TreeMap hmLocation;
	TreeMap tmAllLocations;
	TreeMap tmAllSubLocations;
	TreeMap tmRedAlerts;
	TreeMap tmGreenAlerts;
	TreeMap tmZoneColor;
	TreeMap tmCurrentAlerts;
	TreeMap tmTransitAlerts;
	LocationBean locBean = new LocationBean();
	LocationBean locBean1 = new LocationBean();
	LocationBean locBean2 = new LocationBean();
	String sTempKey = "";
	SubLocationBean subLocBean = new SubLocationBean();
	TransitDao transitDao = new TransitDao();
	ClientLocationMapDao clientDao = new ClientLocationMapDao();
	AlertsWorkflowDao alertsWorkflowDao = new AlertsWorkflowDao();
	String sTimeValue = "";
	String sAlertType = "";
	int iRed = 0;
	int iGreen = 0;
	String sOptions = "";
	String[] sAlerts = null;%>
<%

java.util.Date dt = new java.util.Date();
java.text.SimpleDateFormat sdf = 
     new java.text.SimpleDateFormat("dd-MM-yyyy HH:mm");
sdf.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
String currentTime = sdf.format(dt);
/*
	buf = "";
	iColSize = 0;
	sErrorMessage = null;
	sId = null;
	sRowId = null;
	sKey = "";
	sLocation = "";
	sSubLocation = "";
	sZone = "";
	sColor = "";
	colLocation = null;
	colSubLocation = null;
	colZone = null;
	sZones = null;
	sLoc = null;
	hmLocation = null; 
	tmAllLocations = null;
	tmAllSubLocations = null;
	tmRedAlerts = null;
	tmGreenAlerts = null;
	tmZoneColor = null;
	tmCurrentAlerts = null;
	tmTransitAlerts = null;
	locBean = new LocationBean();
	locBean1 = new LocationBean();
	locBean2 = new LocationBean();
	sTempKey = "";
	subLocBean = new SubLocationBean();
	clientDao = new ClientLocationMapDao();
	alertsWorkflowDao = new AlertsWorkflowDao();
	transitDao = new TransitDao();
	sTimeValue = "";
	sAlertType = "";
	iRed = 0;
	iGreen = 0;
	sOptions = "";
	sAlerts = null;
	//sAlerts = request.getParameterValues("all_btn");
	sAlertType = request.getParameter("CatID");	
	//System.out.println("new: " + sAlertType);
	if(sAlertType == null) sAlertType = "ALL";
	sTimeValue = request.getParameter("alertsFilter");
	System.out.println(" a: " + sTimeValue + " AlertysType: " + sAlertType);
	
	LinkedHashMap hm = ClientLocationMapDao.lhmAllDashboardData;
	System.out.println(hm.size());
	if(hm == null || hm.size() == 0) {
		System.out.println("Dashboard.jsp: getting all locations data..");
		clientDao.AllDashboardData();
		hm = ClientLocationMapDao.lhmAllDashboardData;
	}
	tmAllLocations = ClientLocationMapDao.tmAllLocations;
	tmAllSubLocations = ClientLocationMapDao.tmAllSubLocations;
	if(tmAllLocations == null || tmAllLocations.size() == 0){
		clientDao.selectLocationTypes();
		tmAllLocations = ClientLocationMapDao.tmAllLocations;
	}
	if(tmAllSubLocations == null || tmAllSubLocations.size() == 0){
		clientDao.selectSubLocationTypes();
		tmAllSubLocations = ClientLocationMapDao.tmAllSubLocations;
	}
	
	Set getHmSet = hm.keySet();
	Iterator<String> i = getHmSet.iterator();
	iColSize = 12/(hm.size() + 1);
	if(sTimeValue == null || sTimeValue.equals("")){
		sTimeValue = "60";
	}
	tmRedAlerts = alertsWorkflowDao.getRedAlertsForLocations(sAlertType, sTimeValue);
	tmGreenAlerts = alertsWorkflowDao.getGreenAlertsForLocations(sAlertType, sTimeValue);
	tmZoneColor = alertsWorkflowDao.getZoneColor(sAlertType, sTimeValue);
	tmCurrentAlerts = alertsWorkflowDao.getCurrentAlerts();
	tmTransitAlerts = transitDao.getAlertsForTransit(sAlertType, sTimeValue);
	*/
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Dashboard</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Add these below two lines for autocomplete and the javascript function at the end  -->
<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="../../bootstrap/css/bootstrap.css">
   
<!-- Font Awesome -->
<script src="https://use.fontawesome.com/37ba1872d2.js"></script>
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../../dist/css/tagbox.css">
<script src="../../dist/js/tagbox.js"></script>
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
<link href="https://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
  body {
	font-family:  Lato, sans-serif;
}
</style>
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
	<div class="wrapper">

		<header class="main-header">
			<!-- Logo -->
			<div>
				<a href="#" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
					<span class="logo-mini"><img src="../../dist/img/licious.png" style="margin-top:14px"
						alt="User Image"></span> <!-- logo for regular state and mobile devices -->
					<span class="logo-lg"><img src="../../dist/img/licious.png" 
						alt="User Image"></span>
				</a>
			</div>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">

				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
					role="button"> <span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
				</a>
				<form class="navbar-form pull-left" role="search">
					<div class="form-group">
						<b style="padding-left: 15px; font-size: 20px;color:#fff;" class="pull-right">Today's Cold Chain Health Summary</b><br> <b style="padding-left: 15px;color:#fff;"><%= currentTime %></b>
					</div>
				</form>

				<div class="navbar-custom-menu">
					<img src="../../dist/img/Logo.png" class="logo-class pull-right"
						alt="User Image">
				</div>
			</nav>

		</header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->


				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<br>
				<ul class="sidebar-menu">
					<li class="header">MAIN NAVIGATION</li>
					<li class="treeview"><a href="#"> <i
							class="fa fa-dashboard"></i> <span>Alert</span> <span
							class="pull-right-container"> <span
								class="label bg-red pull-right">4</span>
						</span>
					</a>
						<ul class="treeview-menu">
							<li><a> Cold Rooms</a></li>
							<li><a href="ColdRoomPendingAlertWorkflows.jsp"><i
									class="fa fa-circle-o"></i> Today's Alerts</a></li>
							<li><a href="#"><i class="fa fa-circle-o"></i> Pending
									Alert Workflows</a></li>
							<li><a href="#"><i class="fa fa-circle-o"></i> Alerts
									With No Action</a></li>
							<li><a>Vehicles</a></li>
							<li><a href="VehicleTodayAlerts.jsp"><i
									class="fa fa-circle-o"></i> Today's Alerts</a></li>
							<li><a href="VehicleStoppageAlerts.jsp"><i
									class="fa fa-circle-o"></i> Vehicle Stoppage</a></li>
							<li><a href="VehicleRouteAlerts.jsp"><i
									class="fa fa-circle-o"></i> Vehicle Route</a></li>
							<li><a href="VehicleDoorAlerts.jsp"><i
									class="fa fa-circle-o"></i> Vehicle Door Activity</a></li>
							<li><a href="VehicleTemperatureAlerts.jsp"><i
									class="fa fa-circle-o"></i> Vehicle Temperature</a></li>
						</ul></li>
					<li class="treeview active"><a href="#"> <i
							class="fa fa-files-o"></i> <span>Dashboard</span> <span
							class="pull-right-container"> <i
								class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="Dashboard_new_with_all_bars.jsp"><i
									class="fa fa-circle-o"></i> CMC</a></li>
							<!--  <li><a href="VehiclePlannedRouteMap.jsp"><i class="fa fa-circle-o"></i> Vehicle Planned Route</a></li> -->
							<li><a href="TodayColdChainHealth.jsp"><i
									class="fa fa-circle-o"></i> Location Health</a></li>
							<li><a href="TodayColdChainHealthSummary.jsp"><i
									class="fa fa-circle-o"></i> Zone Health</a></li>
							<li><a href="TodayColdChainHealthDetails.jsp"><i
									class="fa fa-circle-o"></i> Node Health</a></li>
							<li><a href="TodayColdChainHealthVehicle.jsp"><i
									class="fa fa-circle-o"></i> Vehicles By Route</a></li>
							<li class="active"><a href="TodayColdChainHealthVehicleDetails.jsp"><i
									class="fa fa-circle-o"></i> Vehicle Health</a></li>
						</ul></li>
					<li><a href="#"> <i class="fa fa-th"></i> <span>Analytics</span>
							<span class="pull-right-container"> <small
								class="label pull-right bg-green">new</small>
						</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="DemandForecasting.jsp"><i
									class="fa fa-circle-o"></i> Demand Forecasting</a></li>
						</ul></li>
					<li class="treeview"><a href="#"> <i
							class="fa fa-pie-chart"></i> <span>Admin Console</span> <span
							class="pull-right-container"> <i
								class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
						<ul class="treeview-menu">
							<li class=""><a href="#"><i class="fa fa-circle-o"></i>
									Device Environment</a></li>
							<li><a href="#"><i class="fa fa-circle-o"></i> User
									Management</a></li>
							<li><a href="#"><i class="fa fa-circle-o"></i> Central
									Console</a></li>
						</ul></li>
					<li class="treeview"><a href="#"> <i class="fa fa-laptop"></i>
							<span>Support</span> <span class="pull-right-container"> <i
								class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="#"><i class="fa fa-circle-o"></i> Raise
									Service Ticket</a></li>
							<li><a href="#"><i class="fa fa-circle-o"></i> Email
									Your Questions</a></li>
						</ul></li>

				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			
		<section id="mainSection" class="content">
			<div class="row">
				<div class="col-md-12">
				<b><a href="Dashboard_new_with_all_bars.jsp">CMC</a></b> &nbsp;<i class="fa fa-caret-right"></i> &nbsp;<b><a href="TodayColdChainHealthVehicle.jsp">MU <i class="fa fa-long-arrow-right"></i> DC</a></b> &nbsp;<i class="fa fa-caret-right"></i> &nbsp;<b>Vehicle Details</b><br> <br>
					<div class="row">
						<div class="col-md-4">
						<div class="well well-sm panel-coldroom" style="height:150px">
												<div class="text-center">
													MU <i class="fa fa-long-arrow-right"></i> DC (# Of Vehicles: <b>120</b>)
													<div class="graph_container">
														<canvas id="Chart3" class="chart1" height="30" width="250"></canvas>
														<div class="row text-center">
															<div class="col-md-4"><span data-toggle="tooltip" class="badge badge-big bg-red">02</span><br><b><span class="description-percentage text-green"><i class="fa fa-caret-down"></i>&nbsp;12%</span></b></div>
															<div class="col-md-4"><span data-toggle="tooltip" class="badge badge-big orange">02</span><br><b><span class="description-percentage text-red"><i class="fa fa-caret-up"></i>&nbsp;09%</span></b></div>
															<div class="col-md-4"><span data-toggle="tooltip" class="badge badge-big bg-green">05</span></div>
														</div>
													</div>
												</div>
											</div>
											</div>
							
										<div class="col-md-4">
											<div class="well well-sm white-background" style="height:150px">
												<div class="text-center">
													DC <i class="fa fa-long-arrow-right"></i> DC (# Of Vehicles: <b>70</b>)
													<div class="graph_container">
														<canvas id="Chart4" class="chart1" height="30" width="250"></canvas>
														<div class="row text-center">
															<div class="col-md-4"><span data-toggle="tooltip" class="badge badge-big bg-red">09</span><br><b><span class="description-percentage text-green"><i class="fa fa-caret-down"></i>&nbsp;02%</span></b></div>
															<div class="col-md-4"><span data-toggle="tooltip" class="badge badge-big orange">02</span><br><b><span class="description-percentage text-green"><i class="fa fa-caret-down"></i>&nbsp;04%</span></b></div>
															<div class="col-md-4"><span data-toggle="tooltip" class="badge badge-big bg-green">05</span></div>
														</div>
													</div>
												</div>
											</div>
										</div>
									
										<div class="col-md-4">
											<div class="well well-sm white-background" style="height:150px">
												<div class="text-center">
													DC <i class="fa fa-long-arrow-right"></i> End Point (# Of Vehicles: <b>80</b>)
													<div class="graph_container">
														<canvas id="Chart5" class="chart1" height="30" width="250"></canvas>
														<div class="row text-center">
															<div class="col-md-4"><span data-toggle="tooltip" class="badge badge-big bg-red">08</span><br><b><span class="description-percentage text-red"><i class="fa fa-caret-up"></i>&nbsp;03%</span></b></div>
															<div class="col-md-4"><span data-toggle="tooltip" class="badge badge-big orange">04</span><br><b><span class="description-percentage text-red"><i class="fa fa-caret-up"></i>&nbsp;07%</span></b></div>
															<div class="col-md-4"><span data-toggle="tooltip" class="badge badge-big bg-green">02</span></div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-10 text-center">
										<div class="well well-sm white-background">
											<b>MANUFACTURING <i class="fa fa-long-arrow-right"></i> DC</b><br><br>
											
									<div class="row">
										<div class="col-md-4">
										<img src="../../dist/img/map1.png" class="map-class">
										</div>
										<div class="col-md-8">
										<img src="../../dist/img/map2.png" class="map-class">	
										</div>
									</div>
										</div>
										
									</div>
									<div class="col-md-2 text-center">
											<div class="well well-sm white-background">
											<br>Filter By:<br>
											<div class="row">
												<div class="col-md-12">
													<select class="form-control-no-height pull-center">
  														<option>Source</option>
													</select> 
												</div>
												</div>
												<br>
												<div class="row">
												<div class="col-md-12">
													<select class="form-control-no-height pull-center">
  														<option>Destination</option>
													</select> 
												</div>
												</div>
												<br>
												<div class="row">
												<div class="col-md-12">
													<select class="form-control-no-height pull-center">
  														<option>Trip Status</option>
													</select> 
												</div>
												</div>
												<br>
												<div class="row">
												<div class="col-md-12">
													<select class="form-control-no-height pull-center">
  														<option>Vehicle#</option>
													</select> 
												</div>
												</div>
												<br>
												<div class="row">
												<div class="col-md-12">
													<select class="form-control-no-height pull-center">
  														<option>Alerts</option>
													</select> 
												</div>
												</div>
												<br>
												<div class="row">
												<div class="col-md-12">
												<button type="button"
														class="btn btn-success">GO</button>&nbsp;&nbsp;
												<button type="button"
														class="btn btn-default">RESET</button>
													
												</div>
											</div>
											<br>
											</div>
											
											 
										</div>
									</div>
									<div class="row">
									
				<div class="col-md-7 text-center">
				<div class="well well-sm">
				<b>TRENDS</b><br><br>
				<i>AMBIENT ABOVE DOOR</i>
					<div class="row">
						<div class="col-md-6">
						<select class="form-control-no-height pull-center" style="width:50%">
  														<option>Duration</option>
													</select> 
													<br><br>
							<b>Temperature (Today)</b>
							<img src="../../dist/img/graph.png" class="map-class">
							
							
						</div>
						<div class="col-md-6">
						<select class="form-control-no-height pull-center" style="width:50%">
  														<option>Duration</option>
													</select> <br><br>
							<b>Humidity (Today)</b>
							<img src="../../dist/img/graph.png" class="map-class">
							
							
						</div>
					</div>
					</div>
					</div>
					<div class="col-md-5 text-center">
				<div class="well well-sm">
				<b>ALERTS</b><br><br>
				<i>AMBIENT ABOVE DOOR</i>
				<br>
				<select class="form-control-no-height pull-center" style="width:25%">
  														<option>Alert Type</option>
													</select> &nbsp;
													<select class="form-control-no-height pull-center" style="width:25%">
  														<option>Duration</option>
													</select> &nbsp;
													<select class="form-control-no-height pull-center" style="width:25%">
  														<option>Status</option>
													</select>
													<br><br> 
					<div class="row">
						<div class="col-md-12 text-center">
					
							<table class="table table-condensed" width="100%">
							<tr class="panel-stop"><td>Alert Workflow 1</td></tr>
							<tr class="panel-door"><td>Alert Workflow 2</td></tr>
							<tr class="panel-temperature"><td>Alert Workflow 3</td></tr>
							
						</table>
							<br><br>
							
						</div>
						
					</div>
					</div>
					</div>
				</div>
									
									
								</div>
							</div>
						
		</section>
		</div>
		<!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0
			</div>
			<strong>Copyright &copy; 2017 <a href="http://tagbox.in">TagBox</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Control Sidebar -->
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->
<!-- ChartJS 1.0.1 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>


	<!-- Bootstrap 3.3.6 -->
	<script src="../../bootstrap/js/bootstrap.min.js"></script>
	<!-- ChartJS 1.0.1 -->
	<!-- AdminLTE App -->
	<script src="../../dist/js/app.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="../../dist/js/demo.js"></script>
<script>
Chart.defaults.global.legend.display = false;

var barOptions_stacked = {
	    tooltips: {
	        enabled: false
	    },
	    hover :{
	        animationDuration:0
	    },
	    scales: {
	        xAxes: [{
	        	display:false,
	            ticks: {
	                beginAtZero:true,
	                fontFamily: "'Open Sans Bold', sans-serif",
	                fontSize:11
	            },
	            
	            stacked: true
	        }],
	        yAxes: [{
	        	display:false,
	            ticks: {
	                fontFamily: "'Open Sans Bold', sans-serif",
	                fontSize:11
	            },
	            stacked: true
	        }]
	    },
	    legend:{
	        display:false
	    },
	    pointLabelFontFamily : "Quadon Extra Bold",
	    scaleFontFamily : "Quadon Extra Bold"
	};
	

var ctx2 = document.getElementById("Chart3");
var myChart2 = new Chart(ctx2, {
    type: 'horizontalBar',
    data: {
        labels: [""],
        
        datasets: [{
        	label: ">3 Alerts",
            data: [2],
            backgroundColor: "rgba(212,19,20,1)"
        },{
        	label: "<3 Alerts",
            data: [2],
            backgroundColor: "rgba(255,169,34,1)"
        },{
        	label: "No Alerts",
            data: [5],
            backgroundColor: "rgba(49,150,8,1)"
        }]
    },

    options: barOptions_stacked
});
var ctx3 = document.getElementById("Chart4");
var myChart3 = new Chart(ctx3, {
    type: 'horizontalBar',
    data: {
        labels: [""],
        
        datasets: [{
        	label: ">3 Alerts",
            data: [9],
            backgroundColor: "rgba(212,19,20,1)"
        },{
        	label: "<3 Alerts",
            data: [2],
            backgroundColor: "rgba(255,169,34,1)"
        },{
        	label: "No Alerts",
            data: [5],
            backgroundColor: "rgba(49,150,8,1)"
        }]
    },

    options: barOptions_stacked
});
var ctx4 = document.getElementById("Chart5");
var myChart4 = new Chart(ctx4, {
    type: 'horizontalBar',
    data: {
        labels: [""],
        
        datasets: [{
        	label: ">3 Alerts",
            data: [8],
            backgroundColor: "rgba(212,19,20,1)"
        },{
        	label: "<3 Alerts",
            data: [4],
            backgroundColor: "rgba(255,169,34,1)"
        },{
        	label: "No Alerts",
            data: [2],
            backgroundColor: "rgba(49,150,8,1)"
        }]
    },

    options: barOptions_stacked
});

</script>
	</body>
</html>
