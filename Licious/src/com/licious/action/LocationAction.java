package com.licious.action;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.TreeMap;

import com.licious.bean.PlanMapsBean;
import com.licious.bean.SubLocationBean;
import com.licious.bean.ZoneBean;
import com.licious.dao.ClientLocationMapDao;
import com.licious.dao.ProcessMapDataDao;
import com.licious.dao.TransitDao;


public class LocationAction {

	public TreeMap<String, SubLocationBean> getLocationSubTypes(String sLocId) {
		ClientLocationMapDao ps = new ClientLocationMapDao();
		TreeMap<String, SubLocationBean> col = new TreeMap<String, SubLocationBean>();
		try {
			col = ps.selectSubLocation(sLocId);
		} catch(Exception e) {
			System.out.println(this.getClass().getName() + "Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		}
		return col;
	}
	
	public TreeMap<String, ZoneBean> getZones(String sLocId, String sSubLocId) {
		ClientLocationMapDao ps = new ClientLocationMapDao();
		TreeMap<String, ZoneBean> col = new TreeMap<String, ZoneBean>();
		try {
			col = ps.selectZones(sLocId, sSubLocId);
		} catch(Exception e) {
			System.out.println(this.getClass().getName() + "Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		}
		return col;
	}
	
	public Collection<PlanMapsBean> getPlanAllRows() {
		ProcessMapDataDao ps = new ProcessMapDataDao();
		Collection<PlanMapsBean> col = new ArrayList<PlanMapsBean>();
		try {
			col = ps.selectPlanAllRecords();
		} catch(Exception e) {
			System.out.println(this.getClass().getName() + "Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		}
		return col;
	}
	
	public List<String> selectSourceVehiclesLocationCity() {
		TransitDao ps = new TransitDao();
		List<String> al=new ArrayList<String>();
		try {
			al = ps.selectSourceVehiclesLocationCity();
		} catch(Exception e) {
			System.out.println(this.getClass().getName() + "Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		}
		return al;
	}
	
	public TreeMap<String, String> selectSourceVehicleLocationNameId(String sSourceCityName) {
		TransitDao ps = new TransitDao();
		TreeMap<String, String> tv = new TreeMap<String, String>();
		try {
			tv = ps.selectSourceVehicleLocationNameId(sSourceCityName);
		} catch(Exception e) {
			System.out.println(this.getClass().getName() + "Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		}
		return tv;
	}
	
	public List<String> selectDestinationVehiclesLocationCity(String sSourceLocationId) {
		TransitDao ps = new TransitDao();
		List<String> al=new ArrayList<String>();
		try {
			al = ps.selectDestinationVehiclesLocationCity(sSourceLocationId);
		} catch(Exception e) {
			System.out.println(this.getClass().getName() + "Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		}
		return al;
	}
	
	public TreeMap<String, String> selectDestinationVehicleLocationNameId(String sDestinationCityName) {
		TransitDao ps = new TransitDao();
		TreeMap<String, String> tv = new TreeMap<String, String>();
		try {
			tv = ps.selectDestinationVehicleLocationNameId(sDestinationCityName);
		} catch(Exception e) {
			System.out.println(this.getClass().getName() + "Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		}
		return tv;
	}
	
	public List<String> selectVehicleIdForSourceDestination(String sSourceId, String sDestinationId) {
		TransitDao ps = new TransitDao();
		List<String> al=new ArrayList<String>();
		try {
			al = ps.selectVehicleIdForSourceDestination(sSourceId, sDestinationId);
		} catch(Exception e) {
			System.out.println(this.getClass().getName() + "Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		}
		return al;
	}
}
