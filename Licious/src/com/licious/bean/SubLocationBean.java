package com.licious.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Vector;

public class SubLocationBean implements Serializable{

	/**
	 * Generated serial version ID
	 */
	private static final long serialVersionUID = -1874474658549948076L;
	
	private String sSubLocId;
	private String sName;
	private String sLocId;
	private int iRed_alert;
	private int iGreen_alert;
	private Collection colLocationNames = new ArrayList();
	/**
	 * @return the iRed_alert
	 */
	public int getRed_alert() {
		return iRed_alert;
	}

	/**
	 * @param iRed_alert the iRed_alert to set
	 */
	public void setRed_alert(int iRed_alert) {
		this.iRed_alert = iRed_alert;
	}
	
	/**
	 * @return the iRed_alert
	 */
	public String getLocationId() {
		return sLocId;
	}

	/**
	 * @param iRed_alert the iRed_alert to set
	 */
	public void setLocationId(String sId) {
		this.sLocId = sId;
	}
	

	/**
	 * @return the location_type
	 */
	public String getName() {
		return sName;
	}

	/**
	 * @param location_type the location_type to set
	 */
	public void setName(String location_name) {
		this.sName = location_name;
	}

	/**
	 * @return the location_id
	 */
	public String getSubLocationId() {
		return sSubLocId;
	}

	/**
	 * @param location_id the location_ to set
	 */
	public void setSubLocationId(String location_id) {
		this.sSubLocId = location_id;
	}
	
	/**
	 * @return the iGreen_alert
	 */
	public int getGreen_alert() {
		return iGreen_alert;
	}

	/**
	 * @param iGreen_alert the iGreen_alert to set
	 */
	public void setGreen_alert(int iGreen_alert) {
		this.iGreen_alert = iGreen_alert;
	}

	/**
	 * @return the colLocationNames
	 */
	public Collection getColLocationNames() {
		return colLocationNames;
	}

	/**
	 * @param colLocationNames the colLocationNames to set
	 */
	public void setLocationNames(Collection colLocationNames) {
		this.colLocationNames = colLocationNames;
	}
	
	public SubLocationBean(){
		super();
	}
	
	public void init() {
		this.sLocId = "";
		this.sName = "";
		this.sSubLocId = "";
		this.iRed_alert = 0;
		this.iGreen_alert = 0;
		this.colLocationNames = new ArrayList();
	}
	
	public String toString(){
		String buf = null;
		buf = "Location Bean object: \n";
		buf = buf + "sub_location_id: " + sSubLocId + "\n";
		buf = buf + "location_name: " + sName + "\n";
		buf = buf + "location_type: " + sLocId + "\n";
		buf = buf + "red_alert: " + iRed_alert + "\n";
		buf = buf + "green_alert: " + iGreen_alert + "\n";
		
		return buf;
	}
	

}
