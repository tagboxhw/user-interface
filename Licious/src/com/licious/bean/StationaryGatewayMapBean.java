package com.licious.bean;

import java.io.Serializable;

public class StationaryGatewayMapBean implements Serializable{

	/**
	 * Generated serial version ID
	 */
	private static final long serialVersionUID = -1874474658549948076L;
	
	private String GW_client_id;
	private String GW_device_id;
	private String location_id;
	private String sub_location_id;
	private String GW_zone_id;
	private String GW_zone_type;
	private String GW_zone_name;
	private String GW_type;
	private String GW_status;
	private String Alert_WF_User_ID;
	private String Serial_Number;
	private String Alias;
	
	public StationaryGatewayMapBean(){
		super();
	}
	
	public void init() {
		this.GW_client_id = "";
		this.GW_device_id = "";
		this.location_id = "";
		this.sub_location_id = "";
		this.GW_zone_id = "";
		this.GW_zone_type = "";
		this.GW_zone_name = "";
		this.GW_type = "";
		this.GW_status = "";
		this.Alert_WF_User_ID = "";
		this.Serial_Number = "";
		this.Alias = "";
	}
	
	public String toString(){
		String buf = null;
		buf = "Stationary GW Map object: \n";
		buf = buf + "GW_client_id: " + GW_client_id + "\n";
		buf = buf + "GW_device_id: " + GW_device_id + "\n";
		buf = buf + "location_id: " + location_id + "\n";
		buf = buf + "sub_location_id: " + sub_location_id + "\n";
		buf = buf + "GW_zone_id: " + GW_zone_id + "\n";
		buf = buf + "GW_zone_type: " + GW_zone_type + "\n";
		buf = buf + "GW_zone_name: " + GW_zone_name + "\n";
		buf = buf + "GW_type: " + GW_type + "\n";
		buf = buf + "GW_status: " + GW_status + "\n";
		buf = buf + "Alert_WF_User_ID: " + Alert_WF_User_ID + "\n";
		buf = buf + "Serial_Number: " + Serial_Number + "\n";
		buf = buf + "Alias: " + Alias + "\n";
		return buf;
	}

	/**
	 * @return the GW_client_id
	 */
	public String getGWClientId() {
		return GW_client_id;
	}

	/**
	 * @param GW_client_id the GW_client_id to set
	 */
	public void setGWClientId(String GW_client_id) {
		this.GW_client_id = GW_client_id;
	}

	/**
	 * @return the GW_device_id
	 */
	public String getGWDeviceId() {
		return GW_device_id;
	}

	/**
	 * @param GW_device_id the GW_device_id to set
	 */
	public void setGWDeviceId(String GW_device_id) {
		this.GW_device_id = GW_device_id;
	}

	/**
	 * @return the location_id
	 */
	public String getLocationId() {
		return location_id;
	}

	/**
	 * @param location_id the location_id to set
	 */
	public void setLocationId(String location_id) {
		this.location_id = location_id;
	}

	/**
	 * @return the sub_location_id
	 */
	public String getSubLocationId() {
		return sub_location_id;
	}

	/**
	 * @param sub_location_id the sub_location_id to set
	 */
	public void setSubLocationId(String sub_location_id) {
		this.sub_location_id = sub_location_id;
	}

	/**
	 * @return the GW_zone_type
	 */
	public String getGWZoneType() {
		return GW_zone_type;
	}

	/**
	 * @param GW_zone_type the GW_zone_type to set
	 */
	public void setGWZoneType(String GW_zone_type) {
		this.GW_zone_type = GW_zone_type;
	}
	
	/**
	 * @return the GW_zone_id
	 */
	public String getGWZoneId() {
		return GW_zone_id;
	}

	/**
	 * @param GW_zone_type the GW_zone_type to set
	 */
	public void setGWZoneId(String GW_zone_id) {
		this.GW_zone_id = GW_zone_id;
	}

	/**
	 * @return the GW_zone_name
	 */
	public String getGWZoneName() {
		return GW_zone_name;
	}

	/**
	 * @param GW_zone_name the GW_zone_name to set
	 */
	public void setGWZoneName(String GW_zone_name) {
		this.GW_zone_name = GW_zone_name;
	}

	/**
	 * @return the GW_type
	 */
	public String getGWType() {
		return GW_type;
	}

	/**
	 * @param GW_type the GW_type to set
	 */
	public void setGWType(String GW_type) {
		this.GW_type = GW_type;
	}

	/**
	 * @return the GW_status
	 */
	public String getGWStatus() {
		return GW_status;
	}

	/**
	 * @param GW_status the GW_status to set
	 */
	public void setGWStatus(String GW_status) {
		this.GW_status = GW_status;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the alert_WF_User_ID
	 */
	public String getAlert_WF_User_ID() {
		return Alert_WF_User_ID;
	}

	/**
	 * @param alert_WF_User_ID the alert_WF_User_ID to set
	 */
	public void setAlert_WF_User_ID(String alert_WF_User_ID) {
		Alert_WF_User_ID = alert_WF_User_ID;
	}

	/**
	 * @return the serial_Number
	 */
	public String getSerial_Number() {
		return Serial_Number;
	}

	/**
	 * @param serial_Number the serial_Number to set
	 */
	public void setSerial_Number(String serial_Number) {
		Serial_Number = serial_Number;
	}

	/**
	 * @return the alias
	 */
	public String getAlias() {
		return Alias;
	}

	/**
	 * @param alias the alias to set
	 */
	public void setAlias(String alias) {
		Alias = alias;
	}
	

}
