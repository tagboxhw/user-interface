package com.licious.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeMap;
import java.util.Vector;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.licious.bean.DoorActivityBean;
import com.licious.util.Constants;

public class DoorActivityDao {
	private Client client; 
	static final Logger logger = LoggerFactory
			.getLogger(DoorActivityDao.class);
	
	private void init(){ 
	      this.client = ClientBuilder.newClient(); 
	   }  
	
	public Collection<DoorActivityBean> getDoorActivityData() {
		init();
		GenericType<Collection<DoorActivityBean>> tvmBean1 = new GenericType<Collection<DoorActivityBean>>(){};
		Collection<DoorActivityBean> tvmBean = client 
		         .target(Constants.REST_SERVICE_URL + "DoorActivityService/dooractivity") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(tvmBean1); 
		return tvmBean;
	}
	
	public Collection<DoorActivityBean> getDoorActivityData(String sNodeId) {
		init();
		Collection<DoorActivityBean> col1 = new Vector();
		GenericType<Collection<DoorActivityBean>> tvmBean1 = new GenericType<Collection<DoorActivityBean>>(){};
		Collection<DoorActivityBean> tvmBean = client 
		         .target(Constants.REST_SERVICE_URL + "DoorActivityService/dooractivity") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(tvmBean1); 
		for(DoorActivityBean tb: tvmBean) {
			if(tb.getND_client_id().equals(sNodeId)) {
				col1.add(tb);
			}
		}
		return col1;
	}
}
