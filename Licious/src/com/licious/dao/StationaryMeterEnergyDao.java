package com.licious.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.Collection;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.licious.bean.PlanMapsBean;
import com.licious.bean.StationaryMeterEnergyBean;
import com.licious.bean.StationaryTemperatureBean;
import com.licious.util.DbConnection;
import com.licious.util.SqlConstants;
import com.licious.util.Utils;

public class StationaryMeterEnergyDao {

	static final Logger logger = LoggerFactory
			.getLogger(StationaryMeterEnergyDao.class);

	public Collection getChartData(String sNodeId, String sTime) {
		Collection col = new Vector();
		Connection conn = null;
		ResultSet rs = null;
		StationaryMeterEnergyBean stBean = new StationaryMeterEnergyBean();
		try {
			double fTime = Utils.getAlertTime(sTime);
			// logger.info("fTime: " + fTime);
			conn = DbConnection.getConnection();
			PreparedStatement pSelect = conn
					.prepareStatement(SqlConstants.GetChartData_Energy);
			pSelect.setString(1, sNodeId);
			pSelect.setDouble(2, fTime);
			// logger.info("1: " + System.currentTimeMillis());
			rs = pSelect.executeQuery();
			while (rs.next()) {
				// logger.info(rs.getString(1) + " " + rs.getString(2) + " "
				// +rs.getString(3) + " " + rs.getString(4));
				stBean = new StationaryMeterEnergyBean();
				stBean.setGW_client_id(rs.getString(1));
				stBean.setND_client_id(sNodeId);
				stBean.setTimestamp(rs.getString(3));
				stBean.setPowerFactor(rs.getString(4));
				col.add(stBean);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return col;
	}

	public Collection selectAllStationaryTemperatureRecords() {
		Collection col = new Vector();
		Connection conn = null;
		ResultSet rs = null;
		StationaryTemperatureBean stBean = new StationaryTemperatureBean();
		Statement statement = null;
		try {
			conn = DbConnection.getConnection();

			try {
				statement = conn.createStatement();
				rs = statement
						.executeQuery(SqlConstants.SelectAllStationaryTemperatureRecords);
			} catch (Exception e) {
				e.printStackTrace();
			}
			// logger.info("start");
			while (rs.next()) {
				// logger.info(rs.getString(1) + " " + rs.getString(2) + " "
				// +rs.getString(3) + " " + rs.getString(4));
				stBean = new StationaryTemperatureBean();
				stBean.setGW_client_id(rs.getString(1));
				stBean.setND_client_id(rs.getString(2));
				stBean.setTimestamp(rs.getString(3));
				stBean.setTemperature(rs.getString(4));
				col.add(stBean);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return col;
	}

	public Collection getChartData(String sGWClientId, String sNDClientId,
			String sTime) {
		Collection col = new Vector();
		Connection conn = null;
		ResultSet rs = null;
		StationaryTemperatureBean stBean = new StationaryTemperatureBean();
		try {
			double fTime = Utils.getAlertTime(sTime);
			// logger.info("fTime: " + fTime);
			conn = DbConnection.getConnection();
			PreparedStatement pSelect = conn
					.prepareStatement(SqlConstants.GetChartData_Temperature_WithClient);
			pSelect.setString(1, sGWClientId);
			pSelect.setString(2, sNDClientId);
			pSelect.setDouble(3, fTime);
			// logger.info("1: " + System.currentTimeMillis());
			rs = pSelect.executeQuery();
			while (rs.next()) {
				// logger.info(rs.getString(1) + " " + rs.getString(2) + " "
				// +rs.getString(3) + " " + rs.getString(4));
				stBean = new StationaryTemperatureBean();
				stBean.setGW_client_id(sGWClientId);
				stBean.setND_client_id(sNDClientId);
				stBean.setTimestamp(rs.getString(3));
				stBean.setTemperature(rs.getString(4));
				col.add(stBean);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return col;
	}

	public Collection selectPlanAllRecords() {
		Collection col = new Vector();
		Calendar rightNow = Calendar.getInstance();
		Connection conn = null;
		PreparedStatement psSelect = null;
		ResultSet rs = null;
		PlanMapsBean mapsBean = new PlanMapsBean();
		Statement statement = null;
		try {
			conn = DbConnection.getConnection();

			// Create and execute a SELECT SQL statement.
			// Create and execute a SELECT SQL statement.
			try {
				statement = conn.createStatement();
			} catch (Exception e) {
				e.printStackTrace();
			}
			rs = statement.executeQuery(SqlConstants.SelectPlanAllRecords);

			while (rs.next()) {
				mapsBean = new PlanMapsBean();
				mapsBean.setGateway_id(rs.getString(1));
				mapsBean.setLatitude(rs.getFloat(2));
				mapsBean.setLongitude(rs.getFloat(3));
				mapsBean.setDCName(rs.getString(4));
				col.add(mapsBean);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (psSelect != null)
					psSelect.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return col;
	}

}
