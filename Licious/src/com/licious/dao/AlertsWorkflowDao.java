package com.licious.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.TreeMap;
import java.util.Vector;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.licious.bean.AlertSummaryBean;
import com.licious.bean.AlertWorkflowBean;
import com.licious.bean.ImageCorSelectBean;
import com.licious.bean.LocationVehicleAlertsDataBean;
import com.licious.bean.ParameterSummaryBean;
import com.licious.bean.TodaysAlertWorkflowBean;
import com.licious.bean.TransitVehicleMapBean;
import com.licious.util.Constants;
import com.licious.util.DbConnection;
import com.licious.util.SqlConstants;
import com.licious.util.Utils;

public class AlertsWorkflowDao {
	static final Logger logger = LoggerFactory
			.getLogger(AlertsWorkflowDao.class);
	String[] sAlerts = null;
	private Client client; 
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	private void init(){ 
	      this.client = ClientBuilder.newClient(); 
	   }  
	
	public Collection<TodaysAlertWorkflowBean> getTodaysAlerts() {
		init();
		GenericType<Collection<TodaysAlertWorkflowBean>> tvmBean1 = new GenericType<Collection<TodaysAlertWorkflowBean>>(){};
		Collection<TodaysAlertWorkflowBean> tvmBean = client 
		         .target(Constants.REST_SERVICE_URL + "AlertsWorkflowService/todayalerts") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(tvmBean1); 
		return tvmBean;
	}

	public Collection<TodaysAlertWorkflowBean> getAlertsComp() {
		init();
		GenericType<Collection<TodaysAlertWorkflowBean>> tvmBean1 = new GenericType<Collection<TodaysAlertWorkflowBean>>(){};
		Collection<TodaysAlertWorkflowBean> tvmBean = client 
		         .target(Constants.REST_SERVICE_URL + "AlertsWorkflowService/alertscomp") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(tvmBean1); 
		return tvmBean;
	}
	
	public Collection<AlertSummaryBean> getAlertsSummary(String sStartTime, String sEndTime){ 
		init();
		GenericType<Collection<AlertSummaryBean>> tvmBean1 = new GenericType<Collection<AlertSummaryBean>>(){};
		WebTarget target = client.target(Constants.REST_SERVICE_URL + "AlertSummaryService/alertssummary").path(sStartTime).path(sEndTime);
		Collection<AlertSummaryBean> tvmBean  = target.request().get(tvmBean1);
		return tvmBean; 
	   } 
	
	
	
	public TreeMap<String, ImageCorSelectBean> getImageCorSelect() {
		Connection conn = null;
		ResultSet rs = null;
		Statement statement = null;
		TreeMap<String, ImageCorSelectBean> tmMap = new TreeMap<String, ImageCorSelectBean>();
		ImageCorSelectBean imgCorSel = new ImageCorSelectBean();
		String sKey = "";
		try {
			conn = DbConnection.getConnection();

			try {
				statement = conn.createStatement();

				rs = statement.executeQuery(SqlConstants.GetImageCorSelect);
			} catch (Exception e) {
				e.printStackTrace();
			}
			while (rs.next()) {
				sKey = rs.getString(1);
				imgCorSel = new ImageCorSelectBean();
				imgCorSel.setsSubZoneId(rs.getString(1));
				imgCorSel.setsTimestamp(rs.getString(2));
				imgCorSel.setsTemperature(rs.getString(3));
				imgCorSel.setsHumidity(rs.getString(4));
				tmMap.put(sKey, imgCorSel);
			}

		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return tmMap;
	}

	public TreeMap<String, Integer> getRedAlertsForLocations(String sAlertType,
			String sTime) {
		Connection conn = null;
		ResultSet rs = null;
		TreeMap<String, Integer> tmMap = new TreeMap<String, Integer>();
		PreparedStatement pSelect = null;
		String sKey = "";
		try {
			conn = DbConnection.getConnection();
			double fTime = Utils.getAlertTime(sTime);
			logger.info("AlertFilter: " + sAlertType + " Time: " + fTime);
			if (sAlertType.equals("ALL")) {
				pSelect = conn
						.prepareStatement(SqlConstants.GetRedAlertsForLocations_All);
				pSelect.setDouble(1, fTime);
			} else {
				pSelect = conn
						.prepareStatement(SqlConstants.GetRedAlertsForLocations);
				sAlerts = sAlertType.split(",");
				sAlertType = "";
				int i = 0;
				for (int in = 0; in < sAlerts.length; in++) {
					i++;
					pSelect.setString(i, sAlerts[in]);
				}
				i++;
				while (i <= 6) {
					pSelect.setString(i, "NULL");
					i++;
				}
				pSelect.setDouble(7, fTime);
			}
			try {
				rs = pSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}
			int total = 0;
			while (rs.next()) {
				logger.info("getRedAlertsForLocations(): " + rs.getString(1)
						+ " " + rs.getString(2) + " " + rs.getInt(3));
				sKey = rs.getString(1);
				if (tmMap.containsKey(sKey)) {
					logger.info("3: " + rs.getInt(3));
					total = rs.getInt(3) + (Integer) tmMap.get(sKey);
					tmMap.put(sKey, total);
					logger.info("5: " + total);
				} else {
					logger.info("4: " + rs.getInt(3));
					tmMap.put(sKey, rs.getInt(3));
				}
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return tmMap;
	}

	public TreeMap<String, Integer> getGreenAlertsForLocations(
			String sAlertType, String sTime) {
		Connection conn = null;
		ResultSet rs = null;
		TreeMap<String, Integer> tmMap = new TreeMap<String, Integer>();
		PreparedStatement pSelect = null;
		String sKey = "";
		try {
			conn = DbConnection.getConnection();
			double fTime = Utils.getAlertTime(sTime);
			if (sAlertType.equals("ALL")) {
				pSelect = conn
						.prepareStatement(SqlConstants.GetGreenAlertsForLocations_All);
				pSelect.setDouble(1, fTime);
			} else {
				pSelect = conn
						.prepareStatement(SqlConstants.GetGreenAlertsForLocations);
				sAlerts = sAlertType.split(",");
				sAlertType = "";
				int i = 0;
				for (int in = 0; in < sAlerts.length; in++) {
					i++;
					pSelect.setString(i, sAlerts[in]);
				}
				i++;
				while (i <= 6) {
					pSelect.setString(i, "NULL");
					i++;
				}
				pSelect.setDouble(7, fTime);
			}
			try {
				rs = pSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}
			int total = 0;
			while (rs.next()) {
				sKey = rs.getString(1);
				if (tmMap.containsKey(sKey)) {
					total = rs.getInt(3) + (Integer) tmMap.get(sKey);
					tmMap.put(sKey, total);
				} else {
					tmMap.put(sKey, rs.getInt(3));
				}
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return tmMap;
	}

	public TreeMap<String, Comparable> getZoneColor(String sAlertType,
			String sTime) {
		Connection conn = null;
		ResultSet rs = null;
		TreeMap<String, Comparable> tmMap = new TreeMap<String, Comparable>();
		PreparedStatement pSelect = null;
		String sKey = "";
		try {
			conn = DbConnection.getConnection();
			double fTime = Utils.getAlertTime(sTime);
			logger.info("AlertFilter: " + sAlertType + " Time: " + fTime);
			if (sAlertType.equals("ALL")) {
				pSelect = conn.prepareStatement(SqlConstants.GetZoneColor_All);
				pSelect.setDouble(1, fTime);
			} else {
				pSelect = conn.prepareStatement(SqlConstants.GetZoneColor);
				sAlerts = sAlertType.split(",");
				sAlertType = "";
				int i = 0;
				for (int in = 0; in < sAlerts.length; in++) {
					i++;
					pSelect.setString(i, sAlerts[in]);
				}
				i++;
				while (i <= 6) {
					pSelect.setString(i, "NULL");
					i++;
				}
				pSelect.setDouble(7, fTime);
			}
			try {
				rs = pSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}
			int total = 0;
			while (rs.next()) {
				logger.info("zoneId: " + rs.getString(1) + " loc_type: "
						+ rs.getString(2) + " loc_id: " + rs.getString(3)
						+ " color: " + rs.getString(4) + " alerts: "
						+ rs.getInt(5));
				if (rs.getString(1) != null) {
					sKey = rs.getString(1);
					tmMap.put(sKey, rs.getString(4));
				}
				if (rs.getString(2) != null) {
					sKey = rs.getString(2).toUpperCase() + rs.getString(4);
					if (tmMap.containsKey(sKey)) {
						total = 1 + (Integer) tmMap.get(sKey);
						tmMap.put(sKey, total);
					} else {

						tmMap.put(sKey, 1);
					}
				}
				if (rs.getString(3) != null) {
					sKey = rs.getString(3);
					if (tmMap.containsKey(sKey)) {
						total = rs.getInt(5) + (Integer) tmMap.get(sKey);
						tmMap.put(sKey, total);
					} else {
						tmMap.put(sKey, rs.getInt(5));
					}
					sKey = rs.getString(3) + rs.getString(4);
					if (tmMap.containsKey(sKey)) {
						total = 1 + (Integer) tmMap.get(sKey);
						tmMap.put(sKey, total);
					} else {

						tmMap.put(sKey, 1);
					}

				}
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return tmMap;
	}

	public TreeMap<String, String> getCurrentAlerts() {
		Connection conn = null;
		ResultSet rs = null;
		Statement statement = null;
		TreeMap<String, String> tmMap = new TreeMap<String, String>();
		String sKey = "";
		try {
			conn = DbConnection.getConnection();

			try {
				statement = conn.createStatement();

				rs = statement.executeQuery(SqlConstants.GetCurrentAlerts);
			} catch (Exception e) {
				e.printStackTrace();
			}
			while (rs.next()) {
				sKey = rs.getString(1);
				tmMap.put(sKey, sKey);
			}

		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return tmMap;
	}

	public TreeMap<String, Integer> getRedAlertsForTransit(String sAlertType,
			String sTime) {
		Connection conn = null;
		ResultSet rs = null;
		TreeMap<String, Integer> tmMap = new TreeMap<String, Integer>();
		PreparedStatement pSelect = null;
		String sKey = "";
		try {
			conn = DbConnection.getConnection();
			double fTime = Utils.getAlertTime(sTime);
			logger.info("AlertFilter: " + sAlertType + " Time: " + fTime);
			if (sAlertType.equals("ALL")) {
				// There is no route_type in transit_gateway_map. Should that be
				// transit_vehicle_map. If yes, then there is no client_id in
				// transit_vehicle_map
				pSelect = conn
						.prepareStatement(SqlConstants.GetRedAlertsForTransit_All);
				pSelect.setDouble(1, fTime);
			} else {
				pSelect = conn
						.prepareStatement(SqlConstants.GetRedAlertsForTransit);
				sAlerts = sAlertType.split(",");
				sAlertType = "";
				int i = 0;
				for (int in = 0; in < sAlerts.length; in++) {
					i++;
					pSelect.setString(i, sAlerts[in]);
				}
				i++;
				while (i <= 6) {
					pSelect.setString(i, "NULL");
					i++;
				}
				pSelect.setDouble(7, fTime);
			}
			try {
				rs = pSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}

			while (rs.next()) {
				logger.info(rs.getString(1) + " " + rs.getString(2));
				sKey = rs.getString(1);
				tmMap.put(sKey, rs.getInt(2));
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return tmMap;
	}

	public TreeMap<String, Integer> getAllAlertsForTransit(String sAlertType,
			String sTime) {
		Connection conn = null;
		ResultSet rs = null;
		TreeMap<String, Integer> tmMap = new TreeMap<String, Integer>();
		PreparedStatement pSelect = null;
		String sKey = "";
		try {
			conn = DbConnection.getConnection();
			double fTime = Utils.getAlertTime(sTime);
			logger.info("AlertFilter: " + sAlertType + " Time: " + fTime);
			if (sAlertType.equals("ALL")) {
				// There is no route_type in transit_gateway_map. Should that be
				// transit_vehicle_map. If yes, then there is no client_id in
				// transit_vehicle_map
				pSelect = conn
						.prepareStatement(SqlConstants.GetAllAlertsForTransit_All);
				pSelect.setDouble(1, fTime);
			} else {
				pSelect = conn
						.prepareStatement(SqlConstants.GetAllAlertsForTransit);
				sAlerts = sAlertType.split(",");
				sAlertType = "";
				pSelect.setDouble(1, fTime);
				int i = 1;
				for (int in = 0; in < sAlerts.length; in++) {
					i++;
					pSelect.setString(i, sAlerts[in]);
				}
				i++;
				while (i <= 7) {
					pSelect.setString(i, "NULL");
					i++;
				}
			}
			try {
				rs = pSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}
			int total = 0;

			while (rs.next()) {
				total = 0;
				logger.info(rs.getString(1) + " " + rs.getString(2) + " "
						+ rs.getString(3) + " " + rs.getString(4));
				sKey = rs.getString(2) + rs.getString(5);
				if (tmMap.containsKey(sKey)) {
					total = 1 + (Integer) tmMap.get(sKey);
					tmMap.put(sKey, total);
				} else {
					tmMap.put(sKey, 1);
				}
				total = 0;
				if (rs.getInt(6) > 0) {
					if (tmMap.containsKey(rs.getString(2))) {
						total = rs.getInt(6)
								+ (Integer) tmMap.get(rs.getString(2));
						tmMap.put(rs.getString(2), total);
					} else {
						tmMap.put(rs.getString(2), rs.getInt(6));
					}
				}
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return tmMap;
	}

	public TreeMap<String, Collection<TransitVehicleMapBean>> getLatLong(
			String sAlertType, String sTime) {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement pSelect = null;
		TreeMap<String, Collection<TransitVehicleMapBean>> tAll = new TreeMap<String, Collection<TransitVehicleMapBean>>();
		try {
			conn = DbConnection.getConnection();
			double fTime = Utils.getAlertTime(sTime);
			logger.info("AlertFilter: " + sAlertType + " Time: " + fTime);
				// There is no route_type in transit_gateway_map. Should that be
				// transit_vehicle_map. If yes, then there is no client_id in
				// transit_vehicle_map
				pSelect = conn.prepareStatement(SqlConstants.GetLatLong_All);
				pSelect.setDouble(1, fTime);
			try {
				rs = pSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}
			Collection<TransitVehicleMapBean> col1 = new Vector<TransitVehicleMapBean>();
			Collection<TransitVehicleMapBean> col2 = new Vector<TransitVehicleMapBean>();
			Collection<TransitVehicleMapBean> col3 = new Vector<TransitVehicleMapBean>();
			TransitVehicleMapBean tv = new TransitVehicleMapBean();
			while (rs.next()) {
				logger.info(rs.getString(1) + " " + rs.getString(2) + " "
						+ rs.getString(3) + " " + rs.getString(4) + " "
						+ rs.getString(5));
				tv = new TransitVehicleMapBean();
				tv.setVehicle_ID(rs.getString(1));
				tv.setRoute_Type(rs.getString(2));
				tv.setSource(rs.getString(3));
				tv.setDestination(rs.getString(4));
				tv.setVehicle_Status(rs.getString(5));
				if (rs.getString(2) == null)
					col1.add(tv);
				else if (rs.getString(2).equals(Constants.LONG_HAUL)) {
					col1.add(tv);
				} else if (rs.getString(2).equals(Constants.SHORT_HAUL)) {
					col2.add(tv);
				} else if (rs.getString(2).equals(Constants.LOCAL_RUN)) {
					col3.add(tv);
				}
			}
			tAll.put(Constants.LONG_HAUL, col1);
			tAll.put(Constants.SHORT_HAUL, col2);
			tAll.put(Constants.LOCAL_RUN, col3);
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return tAll;
	}

	public TreeMap<String, Collection<TransitVehicleMapBean>> getLatLongForAnasuya(
			String sAlertType, String sTime) {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement pSelect = null;
		TreeMap<String, Collection<TransitVehicleMapBean>> tAll = new TreeMap<String, Collection<TransitVehicleMapBean>>();
		try {
			conn = DbConnection.getConnection();
			double fTime = Utils.getAlertTime(sTime);
			logger.info("AlertFilter: " + sAlertType + " Time: " + fTime);
			
				pSelect = conn.prepareStatement("select * from DEV.transit_lat_long where vehicle_id=? order by timestamp asc");
				pSelect.setString(1, "KA03-KH5282");
			try {
				rs = pSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}
			Collection<TransitVehicleMapBean> col1 = new Vector<TransitVehicleMapBean>();
			TransitVehicleMapBean tv = new TransitVehicleMapBean();
			String sSource = "", sDest = "";
			while (rs.next()) {
				logger.info(rs.getString(1) + " " + rs.getString(2) + " "
						+ rs.getString(3) + " " + rs.getString(4));
				tv = new TransitVehicleMapBean();
				tv.setVehicle_ID(rs.getString(1));
				tv.setSource(rs.getString(3));
				tv.setDestination(rs.getString(4));
				if(sSource.equals(rs.getString(3)) && sDest.equals(rs.getString(4))) {
					continue;
				}
				sSource = rs.getString(3);
				sDest = rs.getString(4);
				col1.add(tv);
			}
			System.out.println("Size of Anasuya: " + col1.size());
			tAll.put(Constants.LONG_HAUL, col1);
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return tAll;
	}

	
	public LinkedHashMap<String, AlertWorkflowBean> getAllAlertsForZoneId(
			String sZoneId, String sAlertType, String sTime) {
		Connection conn = null;
		ResultSet rs = null;
		LinkedHashMap<String, AlertWorkflowBean> tmMap = new LinkedHashMap<String, AlertWorkflowBean>();
		PreparedStatement pSelect = null;
		String sKey = "";
		try {
			conn = DbConnection.getConnection();
			double fTime = Utils.getAlertTime(sTime);
			if (sAlertType.equals("ALL")) {
				// There is no route_type in transit_gateway_map. Should that be
				// transit_vehicle_map. If yes, then there is no client_id in
				// transit_vehicle_map
				pSelect = conn
						.prepareStatement(SqlConstants.GetAllAlertsForZoneId_All);
				pSelect.setString(1, sZoneId);
				pSelect.setDouble(2, fTime);
			} else {
				pSelect = conn
						.prepareStatement(SqlConstants.GetAllAlertsForZoneId);
				pSelect.setString(1, sZoneId);
				sAlerts = sAlertType.split(",");
				sAlertType = "";
				int i = 1;
				for (int in = 0; in < sAlerts.length; in++) {
					i++;
					pSelect.setString(i, sAlerts[in]);
				}
				i++;
				while (i <= 7) {
					pSelect.setString(i, "NULL");
					i++;
				}
				pSelect.setDouble(8, fTime);
			}
			try {
				rs = pSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}

			while (rs.next()) {
				sKey = rs.getString(1);
				AlertWorkflowBean ab = new AlertWorkflowBean();
				ab.setAlert_ID(rs.getString(1));
				ab.setAlert_Location(rs.getString(2));
				ab.setAlert_Type(rs.getString(3));
				ab.setBreach_Type(rs.getString(4));
				ab.setAlert_Parameter_Avg(rs.getFloat(5));
				ab.setAlert_Timestamp_From(rs.getString(6));
				ab.setAlert_Timestamp_To(rs.getString(7));
				if (rs.getString(8) == null || rs.getString(8).equals("")) {
					ab.setAlert_Duration("");
				} else {
					ab.setAlert_Duration(rs.getString(8));
				}
				if (rs.getString(9) == null || rs.getString(9).equals("")) {
					ab.setAlert_Severity("");
				} else {
					ab.setAlert_Severity(rs.getString(9));
				}
				if (rs.getString(10) == null || rs.getString(10).equals("")) {
					ab.setCurrent_Location_Name("");
				} else {
					ab.setCurrent_Location_Name(rs.getString(10));
				}
				if (rs.getString(11) == null || rs.getString(11).equals("")) {
					ab.setAction_Status("Open");
				} else {
					ab.setAction_Status(rs.getString(11));
				}
				if (rs.getString(12) == null || rs.getString(12).equals("")) {
					ab.setRoot_Cause("");
				} else {
					ab.setRoot_Cause(rs.getString(12));
				}
				if (rs.getString(13) == null || rs.getString(13).equals("")) {
					ab.setPreventive_Actions("");
				} else {
					ab.setPreventive_Actions(rs.getString(13));
				}
				
				tmMap.put(sKey, ab);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return tmMap;
	}
	
	public LinkedHashMap<String, AlertWorkflowBean> getAllAlertsForSubzoneId(
			String sSubzoneId, String sAlertType, String sTime) {
		Connection conn = null;
		ResultSet rs = null;
		LinkedHashMap<String, AlertWorkflowBean> tmMap = new LinkedHashMap<String, AlertWorkflowBean>();
		PreparedStatement pSelect = null;
		String sKey = "";
		try {
			conn = DbConnection.getConnection();
			double fTime = Utils.getAlertTime(sTime);
				// There is no route_type in transit_gateway_map. Should that be
				// transit_vehicle_map. If yes, then there is no client_id in
				// transit_vehicle_map
				pSelect = conn
						.prepareStatement(SqlConstants.AllAlertsForSubzone);
				pSelect.setString(1, sSubzoneId);
			try {
				rs = pSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}

			while (rs.next()) {
				//logger.info(rs.getString(1) + " " + rs.getString(2));
				sKey = rs.getString(3);
				AlertWorkflowBean ab = new AlertWorkflowBean();
				if (rs.getString(1) == null || rs.getString(1).equals("")) {
					ab.setND_Client_ID(rs.getString(1));
				} else {
					ab.setND_Client_ID(rs.getString(1));
				}
				if (rs.getString(2) == null || rs.getString(2).equals("")) {
					ab.setClassType(rs.getString(2));
				} else {
					ab.setClassType(rs.getString(2));
				}
				if (rs.getString(3) == null || rs.getString(3).equals("")) {
					ab.setAlert_ID(rs.getString(3));
				} else {
					ab.setAlert_ID(rs.getString(3));
				}
				if (rs.getString(4) == null || rs.getString(4).equals("")) {
					ab.setAlert_Location(rs.getString(4));
				} else {
					ab.setAlert_Location(rs.getString(4));
				}
				if (rs.getString(5) == null || rs.getString(5).equals("")) {
					ab.setAlert_Type(rs.getString(5));
				} else {
					ab.setAlert_Type(rs.getString(5));
				}
				if (rs.getString(6) == null || rs.getString(6).equals("")) {
					ab.setBreach_Type("");
				} else {
					ab.setBreach_Type(rs.getString(6));
				}
				if (rs.getString(7) == null) {
					ab.setAlert_Parameter_Avg(0);
				} else {
					ab.setAlert_Parameter_Avg(rs.getFloat(7));
				}
				if (rs.getString(8) == null || rs.getString(8).equals("")) {
					ab.setAlert_Timestamp_From("");
				} else {
					ab.setAlert_Timestamp_From(rs.getString(8));
				}
				if (rs.getString(9) == null || rs.getString(9).equals("")) {
					ab.setAlert_Timestamp_To("");
				} else {
					ab.setAlert_Timestamp_To(rs.getString(9));
				}
				
				if (rs.getString(10) == null || rs.getString(10).equals("")) {
					ab.setAlert_Duration("");
				} else {
					ab.setAlert_Duration(rs.getString(10));
				}
				if (rs.getString(11) == null || rs.getString(11).equals("")) {
					ab.setAlert_Severity("");
				} else {
					ab.setAlert_Severity(rs.getString(11));
				}
				if (rs.getString(13) == null || rs.getString(13).equals("")) {
					ab.setCurrent_Location_Name("");
				} else {
					ab.setCurrent_Location_Name(rs.getString(13));
				}
				if (rs.getString(14) == null || rs.getString(14).equals("")) {
					ab.setRoute_Type("");
				} else {
					ab.setRoute_Type(rs.getString(14));
				}
				if (rs.getString(15) == null || rs.getString(15).equals("")) {
					ab.setAction_Status("Open");
				} else {
					ab.setAction_Status(rs.getString(15));
				}
				if (rs.getString(18) == null || rs.getString(1).equals("")) {
					ab.setAssigned_To("Yatish Bangera");
				} else {
					ab.setAssigned_To(rs.getString(18));
				}
				if (rs.getString(22) == null || rs.getString(22).equals("")) {
					ab.setRoot_Cause("");
				} else {
					ab.setRoot_Cause(rs.getString(22));
				}
				if (rs.getString(23) == null || rs.getString(23).equals("")) {
					ab.setPreventive_Actions("");
				} else {
					ab.setPreventive_Actions(rs.getString(23));
				}
				if (rs.getString(28) == null || rs.getString(28).equals("")) {
					ab.setCurrent_Location_Lat("");
				} else {
					ab.setCurrent_Location_Lat(rs.getString(28));
				}
				if (rs.getString(29) == null || rs.getString(29).equals("")) {
					ab.setCurrent_Location_Long("");
				} else {
					ab.setCurrent_Location_Long(rs.getString(29));
				}
				tmMap.put(sKey, ab);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return tmMap;
	}


	public LinkedHashMap<String, AlertWorkflowBean> getAllAlertsForVehicleId(
			String sVehicleId, String sAlertType, String sTime) {
		Connection conn = null;
		ResultSet rs = null;
		LinkedHashMap<String, AlertWorkflowBean> tmMap = new LinkedHashMap<String, AlertWorkflowBean>();
		PreparedStatement pSelect = null;
		String sKey = "";
		try {
			conn = DbConnection.getConnection();
			double fTime = Utils.getAlertTime(sTime);
				// There is no route_type in transit_gateway_map. Should that be
				// transit_vehicle_map. If yes, then there is no client_id in
				// transit_vehicle_map
				pSelect = conn
						.prepareStatement(SqlConstants.AllAlerts);
				pSelect.setString(1, sVehicleId);
			try {
				rs = pSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}

			while (rs.next()) {
				//logger.info(rs.getString(1) + " " + rs.getString(2));
				sKey = rs.getString(3);
				AlertWorkflowBean ab = new AlertWorkflowBean();
				ab.setND_Client_ID(rs.getString(1));
				ab.setClassType(rs.getString(2));
				ab.setAlert_ID(rs.getString(3));
				ab.setAlert_Location(rs.getString(4));
				ab.setAlert_Type(rs.getString(5));
				if (rs.getString(6) == null || rs.getString(6).equals("")) {
					ab.setBreach_Type("");
				} else {
					ab.setBreach_Type(rs.getString(6));
				}
				ab.setAlert_Parameter_Avg(rs.getFloat(7));
				ab.setAlert_Timestamp_From(rs.getString(8));
				ab.setAlert_Timestamp_To(rs.getString(9));
				if (rs.getString(10) == null || rs.getString(10).equals("")) {
					ab.setAlert_Duration("");
				} else {
					ab.setAlert_Duration(rs.getString(10));
				}
				if (rs.getString(11) == null || rs.getString(11).equals("")) {
					ab.setAlert_Severity("");
				} else {
					ab.setAlert_Severity(rs.getString(11));
				}
				if (rs.getString(13) == null || rs.getString(13).equals("")) {
					ab.setCurrent_Location_Name("");
				} else {
					ab.setCurrent_Location_Name(rs.getString(13));
				}
				if (rs.getString(14) == null || rs.getString(14).equals("")) {
					ab.setRoute_Type("");
				} else {
					ab.setRoute_Type(rs.getString(14));
				}
				if (rs.getString(15) == null || rs.getString(15).equals("")) {
					ab.setAction_Status("Open");
				} else {
					ab.setAction_Status(rs.getString(15));
				}
				ab.setAssigned_To(rs.getString(18));
				if (rs.getString(22) == null || rs.getString(22).equals("")) {
					ab.setRoot_Cause("");
				} else {
					ab.setRoot_Cause(rs.getString(22));
				}
				if (rs.getString(23) == null || rs.getString(23).equals("")) {
					ab.setPreventive_Actions("");
				} else {
					ab.setPreventive_Actions(rs.getString(23));
				}
				ab.setCurrent_Location_Lat(rs.getString(28));
				ab.setCurrent_Location_Long(rs.getString(29));
				tmMap.put(sKey, ab);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return tmMap;
	}

	public LinkedHashMap<String, AlertWorkflowBean> getAllAlertsForStationary(
			String sAlertType, String sTime) {
		Connection conn = null;
		ResultSet rs = null;
		LinkedHashMap<String, AlertWorkflowBean> tmMap = new LinkedHashMap<String, AlertWorkflowBean>();
		PreparedStatement pSelect = null;
		String sKey = "";
		try {
			conn = DbConnection.getConnection();
			double fTime = Utils.getAlertTime(sTime);
			if (sAlertType.equals("ALL")) {
				// There is no route_type in transit_gateway_map. Should that be
				// transit_vehicle_map. If yes, then there is no client_id in
				// transit_vehicle_map
				pSelect = conn
						.prepareStatement(SqlConstants.GetAllAlertsForStationary_All);
				pSelect.setDouble(1, fTime);
			} else {
				pSelect = conn
						.prepareStatement(SqlConstants.GetAllAlertsForStationary);
				sAlerts = sAlertType.split(",");
				sAlertType = "";
				int i = 0;
				for (int in = 0; in < sAlerts.length; in++) {
					i++;
					pSelect.setString(i, sAlerts[in]);
				}
				i++;
				while (i <= 6) {
					pSelect.setString(i, "NULL");
					i++;
				}
				pSelect.setDouble(7, fTime);
			}
			try {
				rs = pSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}

			while (rs.next()) {
				sKey = rs.getString(1);
				AlertWorkflowBean ab = new AlertWorkflowBean();
				ab.setAlert_ID(rs.getString(1));
				ab.setAlert_Location(rs.getString(2));
				ab.setAlert_Type(rs.getString(3));
				ab.setBreach_Type(rs.getString(4));
				ab.setAlert_Parameter_Avg(rs.getFloat(5));
				ab.setAlert_Timestamp_From(rs.getString(6));
				ab.setAlert_Timestamp_To(rs.getString(7));
				if (rs.getString(8) == null || rs.getString(8).equals("")) {
					ab.setAlert_Duration("");
				} else {
					ab.setAlert_Duration(rs.getString(8));
				}
				if (rs.getString(9) == null || rs.getString(9).equals("")) {
					ab.setAlert_Severity("");
				} else {
					ab.setAlert_Severity(rs.getString(9));
				}
				if (rs.getString(10) == null || rs.getString(10).equals("")) {
					ab.setCurrent_Location_Name("");
				} else {
					ab.setCurrent_Location_Name(rs.getString(10));
				}
				if (rs.getString(11) == null || rs.getString(11).equals("")) {
					ab.setAction_Status("Open");
				} else {
					ab.setAction_Status(rs.getString(11));
				}
				if (rs.getString(12) == null || rs.getString(12).equals("")) {
					ab.setRoot_Cause("");
				} else {
					ab.setRoot_Cause(rs.getString(12));
				}
				if (rs.getString(13) == null || rs.getString(13).equals("")) {
					ab.setPreventive_Actions("");
				} else {
					ab.setPreventive_Actions(rs.getString(13));
				}
				tmMap.put(sKey, ab);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return tmMap;
	}

	public LinkedHashMap<String, AlertWorkflowBean> allAlertsForTransit(
			String sAlertType, String sTime) {
		Connection conn = null;
		ResultSet rs = null;
		LinkedHashMap<String, AlertWorkflowBean> tmMap = new LinkedHashMap<String, AlertWorkflowBean>();
		PreparedStatement pSelect = null;
		String sKey = "";
		try {
			conn = DbConnection.getConnection();
			double fTime = Utils.getAlertTime(sTime);
			if (sAlertType.equals("ALL")) {
				// There is no route_type in transit_gateway_map. Should that be
				// transit_vehicle_map. If yes, then there is no client_id in
				// transit_vehicle_map
				pSelect = conn
						.prepareStatement(SqlConstants.AllAlertsForTransit_All);
				pSelect.setDouble(1, fTime);
			} else {
				pSelect = conn
						.prepareStatement(SqlConstants.AllAlertsForTransit);
				sAlerts = sAlertType.split(",");
				sAlertType = "";
				int i = 0;
				for (int in = 0; in < sAlerts.length; in++) {
					i++;
					pSelect.setString(i, sAlerts[in]);
				}
				i++;
				while (i <= 6) {
					pSelect.setString(i, "NULL");
					i++;
				}
				pSelect.setDouble(7, fTime);
			}
			try {
				rs = pSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}

			while (rs.next()) {
				sKey = rs.getString(1);
				AlertWorkflowBean ab = new AlertWorkflowBean();
				ab.setAlert_ID(rs.getString(1));
				ab.setAlert_Location(rs.getString(2));
				ab.setAlert_Type(rs.getString(3));
				ab.setAlert_Timestamp_From(rs.getString(4));
				ab.setAlert_Timestamp_To(rs.getString(5));
				if (rs.getString(6) == null || rs.getString(6).equals("")) {
					ab.setAlert_Duration("");
				} else {
					ab.setAlert_Duration(rs.getString(6));
				}
				if (rs.getString(7) == null || rs.getString(7).equals("")) {
					ab.setAlert_Severity("");
				} else {
					ab.setAlert_Severity(rs.getString(7));
				}
				if (rs.getString(7) == null || rs.getString(7).equals("")) {
					ab.setAlert_Severity("");
				} else {
					ab.setAlert_Severity(rs.getString(7));
				}
				if (rs.getString(8) == null || rs.getString(8).equals("")) {
					ab.setCurrent_Location_Name("");
				} else {
					ab.setCurrent_Location_Name(rs.getString(8));
				}
				if (rs.getString(9) == null || rs.getString(9).equals("")) {
					ab.setAction_Status("Open");
				} else {
					ab.setAction_Status(rs.getString(9));
				}
				if (rs.getString(10) == null || rs.getString(10).equals("")) {
					ab.setRoot_Cause("");
				} else {
					ab.setRoot_Cause(rs.getString(10));
				}
				if (rs.getString(11) == null || rs.getString(11).equals("")) {
					ab.setPreventive_Actions("");
				} else {
					ab.setPreventive_Actions(rs.getString(11));
				}
				if (rs.getString(12) == null || rs.getString(12).equals("")) {
					ab.setRoute_Type("");
				} else {
					ab.setRoute_Type(rs.getString(12));
				}
				
				if (rs.getString(13) == null || rs.getString(13).equals("")) {
					ab.setBreach_Type("");
				} else {
					ab.setBreach_Type(rs.getString(13));
				}
				tmMap.put(sKey, ab);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return tmMap;
	}

	public TreeMap<String, Integer> getGreenAlertsForTransit(String sAlertType,
			String sTime) {
		Connection conn = null;
		ResultSet rs = null;
		TreeMap<String, Integer> tmMap = new TreeMap<String, Integer>();
		PreparedStatement pSelect = null;
		String sKey = "";
		try {
			conn = DbConnection.getConnection();
			double fTime = Utils.getAlertTime(sTime);
			logger.info("AlertFilter: " + sAlertType + " Time: " + fTime);
			if (sAlertType.equals("ALL")) {
				// There is no route_type in transit_gateway_map. Should that be
				// transit_vehicle_map. If yes, then there is no client_id in
				// transit_vehicle_map
				pSelect = conn
						.prepareStatement(SqlConstants.GetGreenAlertsForTransit_All);
				pSelect.setDouble(1, fTime);
			} else {
				pSelect = conn
						.prepareStatement(SqlConstants.GetGreenAlertsForTransit);
				sAlerts = sAlertType.split(",");
				sAlertType = "";
				int i = 0;
				for (int in = 0; in < sAlerts.length; in++) {
					i++;
					pSelect.setString(i, sAlerts[in]);
				}
				i++;
				while (i <= 6) {
					pSelect.setString(i, "NULL");
					i++;
				}
				pSelect.setDouble(7, fTime);
			}
			try {
				rs = pSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}

			while (rs.next()) {
				logger.info(rs.getString(1) + " " + rs.getString(2));
				sKey = rs.getString(1);
				tmMap.put(sKey, rs.getInt(2));
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return tmMap;
	}

	public void updateAction(String sAction, String sAlertId) {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement pSelect = null;
		try {
			conn = DbConnection.getConnection();
			logger.info("action: " + sAction + " id: " + sAlertId);
			// There is no route_type in transit_gateway_map. Should that be
			// transit_vehicle_map. If yes, then there is no client_id in
			// transit_vehicle_map
			pSelect = conn.prepareStatement(SqlConstants.NewUpateAction);
			pSelect.setString(1, sAction);
			pSelect.setString(2, sAlertId);
			int iReturn = pSelect.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void updatePreventiveActionRootCause(String sRootCause,
			String sPreventiveAction, String sAssigned, String sAlertId) {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement pSelect = null;
		try {
			conn = DbConnection.getConnection();
			logger.info("root: " + sRootCause + " pre: " + sPreventiveAction
					+ " alert: " + sAlertId);
			// There is no route_type in transit_gateway_map. Should that be
			// transit_vehicle_map. If yes, then there is no client_id in
			// transit_vehicle_map
			pSelect = conn
					.prepareStatement(SqlConstants.NewUpdateRootCause);
			pSelect.setString(1, sAssigned);
			pSelect.setString(2, sRootCause);
			pSelect.setString(3, sPreventiveAction);
			pSelect.setString(4, sAlertId);
			int iReturn = pSelect.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
