package com.stellar.action;

import java.util.ArrayList;
import java.util.Collection;
import java.util.TreeMap;

import com.stellar.bean.PlanMapsBean;
import com.stellar.bean.ZoneBean;
import com.stellar.dao.AlertsWorkflowDao;
import com.stellar.dao.ClientLocationMapDao;
import com.stellar.dao.ProcessMapDataDao;

public class AlertAction {

	public void updateAction(String sAction, String sAlertId) {
		AlertsWorkflowDao ps = new AlertsWorkflowDao();
		try {
			ps.updateAction(sAction, sAlertId);
		} catch(Exception e) {
			System.out.println(this.getClass().getName() + "Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		}
	}
	
	public void updateRoot(String sRoot, String sPreventive, String sAssigned, String sAlertId) {
		AlertsWorkflowDao ps = new AlertsWorkflowDao();
		try {
			ps.updatePreventiveActionRootCause(sRoot, sPreventive, sAssigned, sAlertId);
		} catch(Exception e) {
			System.out.println(this.getClass().getName() + "Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		}
	}
	
	public TreeMap<String, ZoneBean> getZones(String sLocId, String sSubLocId) {
		ClientLocationMapDao ps = new ClientLocationMapDao();
		TreeMap<String, ZoneBean> col = new TreeMap<String, ZoneBean>();
		try {
			col = ps.selectZones(sLocId, sSubLocId);
		} catch(Exception e) {
			System.out.println(this.getClass().getName() + "Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		}
		return col;
	}
	
	public Collection<PlanMapsBean> getPlanAllRows() {
		ProcessMapDataDao ps = new ProcessMapDataDao();
		Collection<PlanMapsBean> col = new ArrayList<PlanMapsBean>();
		try {
			col = ps.selectPlanAllRecords();
		} catch(Exception e) {
			System.out.println(this.getClass().getName() + "Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		}
		return col;
	}
}
