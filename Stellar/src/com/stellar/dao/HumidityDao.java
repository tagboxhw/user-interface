package com.stellar.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeMap;
import java.util.Vector;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.stellar.bean.ClientLocationMapBean;
import com.stellar.bean.HumidityBean;
import com.stellar.bean.ParameterSummaryBean;
import com.stellar.bean.TemperatureBean;
import com.stellar.util.Constants;
import com.stellar.util.Utils;

public class HumidityDao {
	private Client client; 
	static final Logger logger = LoggerFactory
			.getLogger(HumidityDao.class);
	
	private void init(){ 
	      this.client = ClientBuilder.newClient(); 
	   }  
	
	public Collection<HumidityBean> getHumidityData() {
		init();
		GenericType<Collection<HumidityBean>> tvmBean1 = new GenericType<Collection<HumidityBean>>(){};
		Collection<HumidityBean> tvmBean = client 
		         .target(Constants.REST_SERVICE_URL + "HumidityService/humidity") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(tvmBean1); 
		return tvmBean;
	}
	
	public Collection<HumidityBean> getHumidityDataForWeek() {
		init();
		GenericType<Collection<HumidityBean>> tvmBean1 = new GenericType<Collection<HumidityBean>>(){};
		Collection<HumidityBean> tvmBean = client 
		         .target(Constants.REST_SERVICE_URL + "HumidityService/humidityweek") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(tvmBean1); 
		return tvmBean;
	}
	
	
	public TreeMap<String, String> getHumidityAverageData() {
		init();
		GenericType<Collection<HumidityBean>> tvmBean1 = new GenericType<Collection<HumidityBean>>(){};
		Collection<HumidityBean> tvmBean = client 
		         .target(Constants.REST_SERVICE_URL + "HumidityService/averagehumidity") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(tvmBean1); 
		TreeMap<String, String> tm = new TreeMap<String, String>();
		Iterator iter = tvmBean.iterator();
		HumidityBean clmBean = new HumidityBean();
		while(iter.hasNext()){
			clmBean = (HumidityBean) iter.next();
			tm.put(clmBean.getZone_Id(), Utils.roundIt(clmBean.getHumidity()));
		}
		return tm;
	}
	
	public Collection<HumidityBean> getHumidityData(String sNodeId) {
		init();
		Collection<HumidityBean> col1 = new Vector();
		GenericType<Collection<HumidityBean>> tvmBean1 = new GenericType<Collection<HumidityBean>>(){};
		Collection<HumidityBean> tvmBean = client 
		         .target(Constants.REST_SERVICE_URL + "HumidityService/humidity") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(tvmBean1); 
		for(HumidityBean tb: tvmBean) {
			if(tb.getNode_Id().equals(sNodeId)) {
				col1.add(tb);
			}
		}
		return col1;
	}
	
	public Collection<HumidityBean> getHumidityDataForAllSubzones(String sStartTime, String sEndTime){ 
		init();
		GenericType<Collection<HumidityBean>> tvmBean1 = new GenericType<Collection<HumidityBean>>(){};
		WebTarget target = client.target(Constants.REST_SERVICE_URL + "HumidityService/zoneaveragehumidity").path(sStartTime).path(sEndTime);
		Collection<HumidityBean> tvmBean  = target.request().get(tvmBean1);
		return tvmBean; 
	   } 
	
}
