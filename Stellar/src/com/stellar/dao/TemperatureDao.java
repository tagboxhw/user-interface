package com.stellar.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeMap;
import java.util.Vector;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.stellar.bean.ClientLocationMapBean;
import com.stellar.bean.TemperatureBean;
import com.stellar.bean.TransitVehicleMapBean;
import com.stellar.util.Constants;
import com.stellar.util.Utils;

public class TemperatureDao {
	private Client client; 
	ClientLocationMapDao clmDao = new ClientLocationMapDao();
	ClientLocationMapBean clmBean = new ClientLocationMapBean();
	static final Logger logger = LoggerFactory
			.getLogger(TemperatureDao.class);
	
	private void init(){ 
	      this.client = ClientBuilder.newClient(); 
	   }  
	
	public Collection<TemperatureBean> getTemperatureData() {
		init();
		GenericType<Collection<TemperatureBean>> tvmBean1 = new GenericType<Collection<TemperatureBean>>(){};
		Collection<TemperatureBean> tvmBean = client 
		         .target(Constants.REST_SERVICE_URL + "TemperatureService/temperature") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(tvmBean1); 
		return tvmBean;
	}
	
	public TreeMap<String, String> getTemperatureAverageData() {
		init();
		GenericType<Collection<TemperatureBean>> tvmBean1 = new GenericType<Collection<TemperatureBean>>(){};
		Collection<TemperatureBean> tvmBean = client 
		         .target(Constants.REST_SERVICE_URL + "TemperatureService/averagetemperature") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(tvmBean1); 
		TreeMap<String, String> tm = new TreeMap<String, String>();
		Iterator iter = tvmBean.iterator();
		TemperatureBean clmBean = new TemperatureBean();
		while(iter.hasNext()){
			clmBean = (TemperatureBean) iter.next();
			tm.put(clmBean.getZone_Id(), Utils.roundIt(clmBean.getTemperature()));
		}
		return tm;
	}
	
	
	public Collection<TemperatureBean> getTemperatureData(String sNodeId) {
		init();
		Collection<TemperatureBean> col1 = new Vector();
		GenericType<Collection<TemperatureBean>> tvmBean1 = new GenericType<Collection<TemperatureBean>>(){};
		Collection<TemperatureBean> tvmBean = client 
		         .target(Constants.REST_SERVICE_URL + "TemperatureService/temperature") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(tvmBean1); 
		for(TemperatureBean tb: tvmBean) {
			if(tb.getNode_Id().equals(sNodeId)) {
				col1.add(tb);
			}
		}
		return col1;
	}
	
	public Collection<TemperatureBean> getTemperatureDataForWeek() {
		init();
		GenericType<Collection<TemperatureBean>> tvmBean1 = new GenericType<Collection<TemperatureBean>>(){};
		Collection<TemperatureBean> tvmBean = client 
		         .target(Constants.REST_SERVICE_URL + "TemperatureService/temperatureweek") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(tvmBean1); 
		return tvmBean;
	}
	
	public Collection<TemperatureBean> getTemperatureDataForAllSubzones(String sStartTime, String sEndTime){ 
		init();
		GenericType<Collection<TemperatureBean>> tvmBean1 = new GenericType<Collection<TemperatureBean>>(){};
		WebTarget target = client.target(Constants.REST_SERVICE_URL + "TemperatureService/zoneaveragetemperature").path(sStartTime).path(sEndTime);
		Collection<TemperatureBean> tvmBean  = target.request().get(tvmBean1);
		return tvmBean; 
	   } 
	
}
