package com.stellar.dao;

import java.util.Collection;
import java.util.Iterator;
import java.util.TreeMap;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.stellar.bean.ClientLocationMapBean;
import com.stellar.bean.TransitDriverMapBean;
import com.stellar.util.Constants;

public class TransitDriverMapDao {
	private Client client; 
	static final Logger logger = LoggerFactory
			.getLogger(TransitDriverMapDao.class);
	
	private void init(){ 
	      this.client = ClientBuilder.newClient(); 
	   }  
	
	public TreeMap<String, TransitDriverMapBean> selectDrivers() {
		init();
		GenericType<Collection<TransitDriverMapBean>> tdmBean1 = new GenericType<Collection<TransitDriverMapBean>>(){};
		Collection<TransitDriverMapBean> col = client 
		         .target(Constants.REST_SERVICE_URL + "TransitDriverMapService/drivermap") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(tdmBean1); 
		TreeMap<String, TransitDriverMapBean> tm = new TreeMap<String, TransitDriverMapBean>();
		Iterator iter = col.iterator();
		TransitDriverMapBean tdmBean = new TransitDriverMapBean();
		while(iter.hasNext()){
			tdmBean = (TransitDriverMapBean) iter.next();
			tm.put(tdmBean.getDriver_DL_ID(), tdmBean);
		}
		return tm;
	}
}
