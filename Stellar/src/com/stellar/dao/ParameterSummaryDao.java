package com.stellar.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeMap;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.SyncInvoker;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.stellar.bean.ParameterSummaryBean;
import com.stellar.util.Constants;

public class ParameterSummaryDao {
	private Client client; 
	static final Logger logger = LoggerFactory
			.getLogger(ParameterSummaryDao.class);
	
	private void init(){ 
	      this.client = ClientBuilder.newClient(); 
	   }  
	
	public Collection<ParameterSummaryBean> getParameterSummaryHumidityData() {
		init();
		GenericType<Collection<ParameterSummaryBean>> tvmBean1 = new GenericType<Collection<ParameterSummaryBean>>(){};
		Collection<ParameterSummaryBean> tvmBean = client 
		         .target(Constants.REST_SERVICE_URL + "ParameterSummaryService/parameterhumidity") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(tvmBean1); 
		return tvmBean;
	}
	
	public Collection<ParameterSummaryBean> getParameterSummaryTemperatureData() {
		init();
		GenericType<Collection<ParameterSummaryBean>> tvmBean1 = new GenericType<Collection<ParameterSummaryBean>>(){};
		Collection<ParameterSummaryBean> tvmBean = client 
		         .target(Constants.REST_SERVICE_URL + "ParameterSummaryService/parametertemperature") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(tvmBean1); 
		return tvmBean;
	}
	
	public Collection<ParameterSummaryBean> getParameterSummaryTemperatureData(String sStartTime, String sEndTime){ 
		init();
		GenericType<Collection<ParameterSummaryBean>> tvmBean1 = new GenericType<Collection<ParameterSummaryBean>>(){};
		WebTarget target = client.target(Constants.REST_SERVICE_URL + "ParameterSummaryService/parametertemperature").path(sStartTime).path(sEndTime);
		Collection<ParameterSummaryBean> tvmBean  = target.request().get(tvmBean1);
		return tvmBean; 
	   } 
	
	public Collection<ParameterSummaryBean> getParameterSummaryHumidityData(String sStartTime, String sEndTime){ 
		init();
		GenericType<Collection<ParameterSummaryBean>> tvmBean1 = new GenericType<Collection<ParameterSummaryBean>>(){};
		WebTarget target = client.target(Constants.REST_SERVICE_URL + "ParameterSummaryService/parameterhumidity").path(sStartTime).path(sEndTime);
		Collection<ParameterSummaryBean> tvmBean  = target.request().get(tvmBean1);
		
		return tvmBean; 
	   } 
}
