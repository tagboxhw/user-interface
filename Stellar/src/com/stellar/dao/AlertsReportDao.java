package com.stellar.dao;

import java.util.Collection;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.stellar.bean.AlertWorkflowBean;
import com.stellar.util.Constants;

public class AlertsReportDao {
	private Client client; 
	static final Logger logger = LoggerFactory.getLogger(AlertsReportDao.class);
	
	private void init(){ 
		this.client = ClientBuilder.newClient(); 
	}  
	
	public Collection<AlertWorkflowBean> getAlertsReport() {
		init();
		GenericType<Collection<AlertWorkflowBean>> clmBean1 = new GenericType<Collection<AlertWorkflowBean>>(){};
		Collection<AlertWorkflowBean> col = client 
		         .target(Constants.REST_SERVICE_URL + "AlertsWorkflowService/alertsreport") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(clmBean1);
		return col;
	}
}
