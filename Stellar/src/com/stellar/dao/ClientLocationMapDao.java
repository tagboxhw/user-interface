package com.stellar.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.TreeMap;
import java.util.Vector;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.stellar.bean.AllLocationDataBean;
import com.stellar.bean.ClientLocationMapBean;
import com.stellar.bean.GatewayLogBean;
import com.stellar.bean.LocationBean;
import com.stellar.bean.PlanMapsBean;
import com.stellar.bean.StationaryNodeMapBean;
import com.stellar.bean.SubLocationBean;
import com.stellar.bean.TransitVehicleMapBean;
import com.stellar.bean.ZoneBean;
import com.stellar.util.Constants;
import com.stellar.util.DbConnection;
import com.stellar.util.SqlConstants;
import com.stellar.util.Utils;

public class ClientLocationMapDao {
	private Client client; 
	static final Logger logger = LoggerFactory
			.getLogger(ClientLocationMapDao.class);
	public static Collection<AllLocationDataBean> colAllLocations = new Vector<AllLocationDataBean>();
	public static TreeMap<String, ClientLocationMapBean> tmAllLocationsOrderByName = new TreeMap<String, ClientLocationMapBean>();
	public static TreeMap<String, SubLocationBean> tmAllSubLocations = new TreeMap<String, SubLocationBean>();
	public static LinkedHashMap<String, Collection> lhmAllDashboardData = new LinkedHashMap<String, Collection>();

	private void init(){ 
	      this.client = ClientBuilder.newClient(); 
	   }  
	
	
	public TreeMap<String, ClientLocationMapBean> selectLocations() {
		init();
		GenericType<Collection<ClientLocationMapBean>> clmBean1 = new GenericType<Collection<ClientLocationMapBean>>(){};
		Collection<ClientLocationMapBean> col = client 
		         .target(Constants.REST_SERVICE_URL + "ClientLocationMapService/locationmap") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(clmBean1);
		TreeMap<String, ClientLocationMapBean> tm = new TreeMap<String, ClientLocationMapBean>();
		Iterator iter = col.iterator();
		ClientLocationMapBean clmBean = new ClientLocationMapBean();
		while(iter.hasNext()){
			clmBean = (ClientLocationMapBean) iter.next();
			tm.put(clmBean.getLocation_id(), clmBean);
		}
		return tm;
	}
	
	public Collection<AllLocationDataBean> getAllLocationsData() {
		init();
		GenericType<Collection<AllLocationDataBean>> clmBean1 = new GenericType<Collection<AllLocationDataBean>>(){};
		Collection<AllLocationDataBean> col = client 
		         .target(Constants.REST_SERVICE_URL + "ClientLocationMapService/alllocations") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(clmBean1);
		colAllLocations = col;
		return colAllLocations;
	}
	
//	public void AllDashboardData() {
//		Connection conn = null;
//		PreparedStatement psSelect = null;
//		ResultSet rs = null;
//		Collection colLocationBean = new ArrayList();
//		Collection colSubLocationBean = new ArrayList();
//		Collection colZoneLocationBean = new ArrayList();
//		Statement statement = null;
//		ClientLocationMapBean locBean = new ClientLocationMapBean();
//		SubLocationBean subLocBean = new SubLocationBean();
//		LocationBean zoneLocBean = new LocationBean();
//		TreeMap hmLocation = new TreeMap();
//		TreeMap hmSubLocation = new TreeMap();
//		try {
//			if (tmAllLocations == null || tmAllLocations.size() == 0) {
//				selectLocationTypes();
//			}
//			if (tmAllSubLocations == null || tmAllSubLocations.size() == 0) {
//				selectSubLocationTypes();
//			}
//			conn = DbConnection.getConnection();
//			try {
//				statement = conn.createStatement();
//			} catch (Exception e) {
//				if (colZoneLocationBean == null)
//					colZoneLocationBean = new ArrayList();
//				e.printStackTrace();
//			}
//			rs = statement.executeQuery(SqlConstants.AllDashboardData);
//			boolean bIsLocThere = false;
//			boolean bIsSubLocThere = false;
//			lhmAllDashboardData.clear();
//			while (rs.next()) {
//				bIsLocThere = false;
//				bIsSubLocThere = false;
//				locBean = (ClientLocationMapBean) tmAllLocations.get(rs
//						.getString(1));
//				if (lhmAllDashboardData.containsKey(locBean.getLocation_type())) {
//					colLocationBean = (Collection) lhmAllDashboardData
//							.get(locBean.getLocation_type());
//					Iterator iLocBean = colLocationBean.iterator();
//					while (iLocBean.hasNext()) {
//						hmLocation = (TreeMap) iLocBean.next();
//						// logger.info("1: Location Name: " +
//						// locBean.getName());
//						if (hmLocation.containsKey(locBean.getLocationName())) {
//							// logger.info("2: Location Id: " +
//							// locBean.getName());
//							colSubLocationBean = (Collection) hmLocation
//									.get(locBean.getLocationName());
//							Iterator iSubLoc = colSubLocationBean.iterator();
//							while (iSubLoc.hasNext()) {
//								hmSubLocation = (TreeMap) iSubLoc.next();
//								if (rs.getString(2) == null) {
//									subLocBean = new SubLocationBean();
//									subLocBean
//											.setSubLocationId(Constants.NO_SUB_LOCATION);
//									subLocBean.setLocationId(locBean
//											.getLocation_id());
//									subLocBean
//											.setName(Constants.NO_SUB_LOCATION);
//								} else {
//									subLocBean = (SubLocationBean) tmAllSubLocations
//											.get(rs.getString(2));
//									if (subLocBean == null) {
//										subLocBean = new SubLocationBean();
//										subLocBean.setSubLocationId(rs
//												.getString(2));
//										subLocBean.setLocationId(locBean
//												.getLocation_id());
//										subLocBean
//												.setName(Constants.NO_SUB_LOCATION);
//									}
//								}
//								// logger.info("5: Sub Location Name: " +
//								// subLocBean.getName());
//								if (hmSubLocation.containsKey(subLocBean
//										.getName())) {
//									colZoneLocationBean = (Collection) hmSubLocation
//											.get(subLocBean.getName());
//									zoneLocBean = new LocationBean();
//									// logger.info("6: Zone ID: " +
//									// rs.getString(3));
//									// logger.info("7: Zone Name: " +
//									// rs.getString(4));
//									zoneLocBean.setId(rs.getString(3));
//									zoneLocBean.setName(rs.getString(4));
//									if (rs.getString(5) == null
//											|| rs.getString(5).equals("")) {
//										zoneLocBean.setAlias("--");
//									} else {
//										zoneLocBean.setAlias(rs.getString(5));
//									}
//									colZoneLocationBean.add(zoneLocBean);
//									hmSubLocation.put(subLocBean.getName(),
//											colZoneLocationBean);
//									// logger.info("dd:" + locBean.getType() +
//									// "; " + locBean.getName() + "; " +
//									// subLocBean.getName() + "; " +
//									// zoneLocBean.getId());
//									bIsSubLocThere = true;
//									break;
//								}
//
//							}
//							if (!bIsSubLocThere) {
//								zoneLocBean = new LocationBean();
//								// logger.info("10: Zone ID: " +
//								// rs.getString(3));
//								// logger.info("11: Zone Name: " +
//								// rs.getString(4));
//								zoneLocBean.setId(rs.getString(3));
//								zoneLocBean.setName(rs.getString(4));
//								if (rs.getString(5) == null
//										|| rs.getString(5).equals("")) {
//									zoneLocBean.setAlias("--");
//								} else {
//									zoneLocBean.setAlias(rs.getString(5));
//								}
//								colZoneLocationBean = new ArrayList();
//								colZoneLocationBean.add(zoneLocBean);
//								hmSubLocation = new TreeMap();
//								hmSubLocation.put(subLocBean.getName(),
//										colZoneLocationBean);
//								colSubLocationBean.add(hmSubLocation);
//								hmLocation.put(locBean.getLocationName(),
//										colSubLocationBean);
//								// logger.info("cc:" + locBean.getType() + "; "
//								// + locBean.getName() + "; " +
//								// subLocBean.getName() + "; " +
//								// zoneLocBean.getId());
//
//							}
//							bIsLocThere = true;
//							break;
//
//						}
//
//					}
//					if (!bIsLocThere) {
//						// logger.info("9: Zone ID: " + rs.getString(3));
//						// logger.info("7: Zone Name: " + rs.getString(4));
//						if (rs.getString(2) == null) {
//							subLocBean = new SubLocationBean();
//							subLocBean
//									.setSubLocationId(Constants.NO_SUB_LOCATION);
//							subLocBean.setLocationId(locBean.getLocation_id());
//							subLocBean.setName(Constants.NO_SUB_LOCATION);
//						} else {
//							subLocBean = (SubLocationBean) tmAllSubLocations
//									.get(rs.getString(2));
//							if (subLocBean == null) {
//								subLocBean = new SubLocationBean();
//								subLocBean.setSubLocationId(rs.getString(2));
//								subLocBean.setLocationId(locBean
//										.getLocation_id());
//								subLocBean.setName(Constants.NO_SUB_LOCATION);
//							}
//						}
//						zoneLocBean = new LocationBean();
//						zoneLocBean.setId(rs.getString(3));
//						zoneLocBean.setName(rs.getString(4));
//						if (rs.getString(5) == null
//								|| rs.getString(5).equals("")) {
//							zoneLocBean.setAlias("--");
//						} else {
//							zoneLocBean.setAlias(rs.getString(5));
//						}
//						colZoneLocationBean = new ArrayList();
//						colZoneLocationBean.add(zoneLocBean);
//						hmSubLocation = new TreeMap();
//						hmSubLocation.put(subLocBean.getName(),
//								colZoneLocationBean);
//						colSubLocationBean = new ArrayList();
//						colSubLocationBean.add(hmSubLocation);
//						hmLocation.put(locBean.getLocationName(),
//								colSubLocationBean);
//						// logger.info("bb:" + locBean.getType() + "; " +
//						// locBean.getName() + "; " + subLocBean.getName() +
//						// "; " + zoneLocBean.getId());
//
//					}
//				} else {
//					// logger.info("8: Location Id: " + rs.getString(1));
//					locBean = new ClientLocationMapBean();
//					locBean = (ClientLocationMapBean) tmAllLocations.get(rs
//							.getString(1));
//
//					if (rs.getString(2) == null) {
//						subLocBean = new SubLocationBean();
//						subLocBean.setSubLocationId(Constants.NO_SUB_LOCATION);
//						subLocBean.setLocationId(locBean.getLocation_id());
//						subLocBean.setName(Constants.NO_SUB_LOCATION);
//					} else {
//						subLocBean = (SubLocationBean) tmAllSubLocations.get(rs
//								.getString(2));
//						if (subLocBean == null) {
//							subLocBean = new SubLocationBean();
//							subLocBean.setSubLocationId(rs.getString(2));
//							subLocBean.setLocationId(locBean.getLocation_id());
//							subLocBean.setName(Constants.NO_SUB_LOCATION);
//						}
//					}
//					zoneLocBean = new LocationBean();
//					zoneLocBean.setId(rs.getString(3));
//					zoneLocBean.setName(rs.getString(4));
//					if (rs.getString(5) == null || rs.getString(5).equals("")) {
//						zoneLocBean.setAlias("--");
//					} else {
//						zoneLocBean.setAlias(rs.getString(5));
//					}
//					colZoneLocationBean = new ArrayList();
//					colSubLocationBean = new ArrayList();
//					colLocationBean = new ArrayList();
//					colZoneLocationBean.add(zoneLocBean);
//					hmSubLocation = new TreeMap();
//					hmSubLocation
//							.put(subLocBean.getName(), colZoneLocationBean);
//					colSubLocationBean.add(hmSubLocation);
//					hmLocation = new TreeMap();
//					hmLocation.put(locBean.getLocationName(),
//							colSubLocationBean);
//					colLocationBean.add(hmLocation);
//					lhmAllDashboardData.put(locBean.getLocation_type(),
//							colLocationBean);
//					// logger.info("aa: " + locBean.getType() + "; " +
//					// locBean.getName() + "; " + subLocBean.getName() + "; " +
//					// zoneLocBean.getId());
//				}
//			}
//
//		} catch (Exception e) {
//			logger.error("Exception: " + e.getLocalizedMessage());
//			e.printStackTrace();
//		} finally {
//			try {
//				if (rs != null)
//					rs.close();
//				if (psSelect != null)
//					psSelect.close();
//				DbConnection.closeConnection();
//			} catch (SQLException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//
//		}
//	}

//	public void selectLocationTypes() {
//		Connection conn = null;
//		PreparedStatement psSelect = null;
//		ResultSet rs = null;
//		Statement statement = null;
//		ClientLocationMapBean locBean = new ClientLocationMapBean();
//
//		try {
//			tmAllLocations.clear();
//			tmAllLocationsOrderByName.clear();
//			conn = DbConnection.getConnection();
//
//			// Create and execute a SELECT SQL statement.
//			// Create and execute a SELECT SQL statement.
//			try {
//				statement = conn.createStatement();
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//			rs = statement.executeQuery(SqlConstants.NewLocationDetails);
//			while (rs.next()) {
//				locBean = new ClientLocationMapBean();
//				locBean.setLocation_id(rs.getString(1));
//				locBean.setLocation_type(rs.getString(2));
//				locBean.setLocationName(rs.getString(3));
//				locBean.setLocationCity(rs.getString(5));
//				locBean.setLatitude(rs.getString(6));
//				locBean.setLongitude(rs.getString(7));
//				
//				tmAllLocations.put(rs.getString(1), locBean);
//				tmAllLocationsOrderByName.put(rs.getString(3), locBean);
//			}
//		} catch (Exception e) {
//			logger.error("Exception: " + e.getLocalizedMessage());
//			e.printStackTrace();
//		} finally {
//			try {
//				if (rs != null)
//					rs.close();
//				if (psSelect != null)
//					psSelect.close();
//				DbConnection.closeConnection();
//			} catch (SQLException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//
//		}
//
//	}

	public void selectSubLocationTypes() {
		Connection conn = null;
		PreparedStatement psSelect = null;
		ResultSet rs = null;
		Statement statement = null;
		SubLocationBean locBean = new SubLocationBean();
		try {
			tmAllSubLocations.clear();
			conn = DbConnection.getConnection();

			// Create and execute a SELECT SQL statement.
			// Create and execute a SELECT SQL statement.
			try {
				statement = conn.createStatement();
			} catch (Exception e) {
				e.printStackTrace();
			}
			rs = statement.executeQuery(SqlConstants.SelectSubLocationTypes);
			while (rs.next()) {
				locBean = new SubLocationBean();
				locBean.setSubLocationId(rs.getString(1));
				locBean.setLocationId(rs.getString(2));
				locBean.setName(rs.getString(3));
				tmAllSubLocations.put(rs.getString(1), locBean);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (psSelect != null)
					psSelect.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	public TreeMap selectZones(String sLocId, String sSubLocId) {
		Connection conn = null;
		PreparedStatement psSelect = null;
		ResultSet rs = null;
		ZoneBean zoneBean = new ZoneBean();
		TreeMap tm = new TreeMap();
		try {
			conn = DbConnection.getConnection();

			if (!sSubLocId.equals("0")) {
				psSelect = conn
						.prepareStatement(SqlConstants.SelectZones_WithSubLoc);
				psSelect.setString(1, sLocId);
				psSelect.setString(2, sSubLocId);
			} else {
				psSelect = conn.prepareStatement(SqlConstants.SelectZones);
				psSelect.setString(1, sLocId);
			}
			try {
				rs = psSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}

			while (rs.next()) {
				zoneBean = new ZoneBean();
				zoneBean.setLocationId(rs.getString(1));
				zoneBean.setSubLocationId(rs.getString(2));
				zoneBean.setZoneId(rs.getString(3));
				zoneBean.setZoneName(rs.getString(4));
				tm.put(zoneBean.getZoneName(), zoneBean);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (psSelect != null)
					psSelect.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return tm;
	}

	public TreeMap selectNodesFromZoneID(String sZoneId, String sAlertType,
			String sTime) {
		Connection conn = null;
		PreparedStatement psSelect = null;
		ResultSet rs = null;
		StationaryNodeMapBean stationaryNodeMapBean = new StationaryNodeMapBean();
		TreeMap tm = new TreeMap();
		try {
			conn = DbConnection.getConnection();
			double fTime = Utils.getAlertTime(sTime);
			if (sAlertType.equals("ALL")) {
				psSelect = conn
						.prepareStatement(SqlConstants.SelectNodesFromZoneID_All);
				psSelect.setDouble(1, fTime);
				psSelect.setString(2, sZoneId);
			} else {
				psSelect = conn
						.prepareStatement(SqlConstants.SelectNodesFromZoneID);
				psSelect.setDouble(1, fTime);
				String[] sAlerts = sAlertType.split(",");
				sAlertType = "";
				int i = 1;
				for (int in = 0; in < sAlerts.length; in++) {
					i++;
					psSelect.setString(i, sAlerts[in]);
				}
				i++;
				while (i <= 7) {
					psSelect.setString(i, "NULL");
					i++;
				}
				psSelect.setString(8, sZoneId);
			}
			try {
				rs = psSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}
			while (rs.next()) {
				stationaryNodeMapBean = new StationaryNodeMapBean();
				stationaryNodeMapBean.setND_client_id(rs.getString(1));
				stationaryNodeMapBean.setND_subzone_name(rs.getString(2));
				stationaryNodeMapBean.setND_type(rs.getString(3));
				stationaryNodeMapBean.setND_subzone_type(rs.getString(5));
				stationaryNodeMapBean.setND_status(rs.getString(6));
				tm.put(stationaryNodeMapBean.getND_client_id(),
						stationaryNodeMapBean);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (psSelect != null)
					psSelect.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return tm;
	}

	public TreeMap selectSubLocation(String sLocId) {
		Connection conn = null;
		PreparedStatement psSelect = null;
		ResultSet rs = null;
		SubLocationBean subLocBean = new SubLocationBean();
		TreeMap tm = new TreeMap();
		try {
			conn = DbConnection.getConnection();
			psSelect = conn.prepareStatement(SqlConstants.SelectSubLocation);
			psSelect.setString(1, sLocId);
			try {
				rs = psSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}
			while (rs.next()) {

				subLocBean = new SubLocationBean();
				subLocBean.setSubLocationId(rs.getString(1));
				subLocBean.setName(rs.getString(2));
				tm.put(subLocBean.getName(), subLocBean);
			}
			psSelect = conn
					.prepareStatement(SqlConstants.SelectSubLocation_WithSubLocNull);
			psSelect.setString(1, sLocId);
			try {
				rs = psSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}
			while (rs.next()) {
				subLocBean = new SubLocationBean();
				subLocBean.setSubLocationId(rs.getString(1));
				subLocBean.setName("None");
				tm.put(subLocBean.getName(), subLocBean);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (psSelect != null)
					psSelect.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return tm;
	}

	public ZoneBean selectLocationFromZoneId(String sZoneId) {
		Connection conn = null;
		PreparedStatement psSelect = null;
		ResultSet rs = null;

		ZoneBean zoneBean = new ZoneBean();
		try {
			conn = DbConnection.getConnection();

			// Create and execute a SELECT SQL statement.
			// Create and execute a SELECT SQL statement.
			psSelect = conn
					.prepareStatement(SqlConstants.SelectLocationFromZoneId);
			psSelect.setString(1, sZoneId);
			try {
				rs = psSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}
			while (rs.next()) {
				zoneBean = new ZoneBean();
				zoneBean.setLocationId(rs.getString(1));
				zoneBean.setSubLocationId(rs.getString(2));
				zoneBean.setZoneId(rs.getString(3));
				zoneBean.setZoneName(rs.getString(4));
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (psSelect != null)
					psSelect.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return zoneBean;
	}

	public LocationBean getLocationDetails(String sLocationName) {
		Connection conn = null;
		PreparedStatement psSelect = null;
		ResultSet rs = null;
		LocationBean locBean = new LocationBean();
		try {
			conn = DbConnection.getConnection();

			// Create and execute a SELECT SQL statement.
			// Create and execute a SELECT SQL statement.
			psSelect = conn.prepareStatement(SqlConstants.GetLocationDetails);
			psSelect.setString(1, sLocationName);
			try {
				rs = psSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}
			while (rs.next()) {
				// logger.info("LocationID: " + rs.getString(1) +
				// " location name: " + sLocationName + " location_type: " +
				// rs.getString(2));
				locBean = new LocationBean();
				locBean.setId(rs.getString(1));
				locBean.setName(sLocationName);
				locBean.setType(rs.getString(2));
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (psSelect != null)
					psSelect.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return locBean;

	}

	public Collection selectPlanAllRecords() {
		Collection col = new ArrayList();
		Connection conn = null;
		PreparedStatement psSelect = null;
		ResultSet rs = null;
		PlanMapsBean mapsBean = new PlanMapsBean();
		Statement statement = null;
		try {
			conn = DbConnection.getConnection();

			// Create and execute a SELECT SQL statement.
			// Create and execute a SELECT SQL statement.
			try {
				statement = conn.createStatement();
			} catch (Exception e) {
				e.printStackTrace();
			}
			rs = statement.executeQuery(SqlConstants.SelectPlanAllRecords);

			while (rs.next()) {
				mapsBean = new PlanMapsBean();
				mapsBean.setGateway_id(rs.getString(1));
				mapsBean.setLatitude(rs.getFloat(2));
				mapsBean.setLongitude(rs.getFloat(3));
				mapsBean.setDCName(rs.getString(4));
				col.add(mapsBean);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (psSelect != null)
					psSelect.close();
				DbConnection.closeConnection();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return col;
	}

	public void updateThresholdAndPhone(String sLocId, String sNodeId,
			String sThreshold, String sPhoneNum) {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement pSelect = null;
		String updateSql = "";
		try {
			conn = DbConnection.getConnection();
			String[] s = sThreshold.split(",");
			pSelect = conn.prepareStatement(SqlConstants.UpdateThreshold);
			pSelect.setString(1, s[0]);
			pSelect.setString(2, s[1]);
			pSelect.setString(3, sNodeId);
			int iReturn = pSelect.executeUpdate();
			if (sLocId.equals("LO-DMO-000001")) {
				updateSql = SqlConstants.UpdatePhoneForLoc1;
			} else if (sLocId.equals("LO-DMO-000002")) {
				updateSql = SqlConstants.UpdatePhoneForLoc2;
			}
			pSelect = conn.prepareStatement(updateSql);
			pSelect.setString(1, sPhoneNum);
			iReturn = pSelect.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void resetThreshold(String sNodeId) {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement pSelect = null;
		try {
			conn = DbConnection.getConnection();
			pSelect = conn.prepareStatement(SqlConstants.ResetThreshold);
			pSelect.setString(1, Constants.THRESHOLD_TEMPERTURE_MIN);
			pSelect.setString(2, Constants.THRESHOLD_TEMPERTURE_MAX);
			pSelect.setString(3, sNodeId);
			int iReturn = pSelect.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public Collection selectAllStationaryNodeMapRows() {
		Connection conn = null;
		ResultSet rs = null;
		Statement statement = null;
		Collection<StationaryNodeMapBean> col = new ArrayList();
		StationaryNodeMapBean sd = new StationaryNodeMapBean();
		try {
			conn = DbConnection.getConnection();

			// Create and execute a SELECT SQL statement.
			// Create and execute a SELECT SQL statement.
			try {
				statement = conn.createStatement();
			} catch (Exception e) {
				e.printStackTrace();
			}
			rs = statement
					.executeQuery(SqlConstants.SelectAllStationaryNodeMapRows);
			while (rs.next()) {
				sd = new StationaryNodeMapBean();
				sd.setGW_Zone_id(rs.getString(1));
				sd.setGW_client_id(rs.getString(2));
				sd.setND_client_id(rs.getString(3));
				sd.setND_device_id(rs.getString(4));
				sd.setGW_ND_Pairing(rs.getString(5));
				sd.setND_subzone_type(rs.getString(6));
				sd.setND_subzone_name(rs.getString(7));
				sd.setND_type(rs.getString(8));
				sd.setND_status(rs.getString(9));
				col.add(sd);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return col;
	}

	public int insertStationaryNodeMap(StationaryNodeMapBean sn) {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement pSelect = null;
		int iReturn = 0;
		try {
			conn = DbConnection.getConnection();
			pSelect = conn
					.prepareStatement(SqlConstants.InsertStationaryNodeMap);
			pSelect.setString(1, sn.getGW_Zone_id());
			pSelect.setString(2, sn.getGW_client_id());
			pSelect.setString(3, sn.getND_client_id());
			pSelect.setString(4, sn.getND_device_id());
			pSelect.setString(5, sn.getGW_ND_Pairing());
			pSelect.setString(6, sn.getND_subzone_type());
			pSelect.setString(7, sn.getND_subzone_name());
			pSelect.setString(8, sn.getND_type());
			pSelect.setString(9, sn.getND_status());

			iReturn = pSelect.executeUpdate();
		} catch (SQLException e) {
			logger.error("localized message: " + e.getLocalizedMessage());
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return iReturn;
	}

	public int updateStationaryNodeMap(StationaryNodeMapBean sn) {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement pSelect = null;
		int iReturn = 0;
		try {
			conn = DbConnection.getConnection();
			pSelect = conn
					.prepareStatement(SqlConstants.UpdateStationaryNodeMap);
			pSelect.setString(1, sn.getGW_Zone_id());
			pSelect.setString(2, sn.getGW_client_id());
			pSelect.setString(3, sn.getND_device_id());
			pSelect.setString(4, sn.getGW_ND_Pairing());
			pSelect.setString(5, sn.getND_subzone_type());
			pSelect.setString(6, sn.getND_subzone_name());
			pSelect.setString(7, sn.getND_type());
			pSelect.setString(8, sn.getND_status());
			pSelect.setString(9, sn.getND_client_id());
			iReturn = pSelect.executeUpdate();
		} catch (SQLException e) {
			logger.error("localized message: " + e.getLocalizedMessage());
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return iReturn;
	}

	public int deleteStationaryNodeMap(String sNDClientID) {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement pSelect = null;
		int iReturn = 0;
		try {
			conn = DbConnection.getConnection();
			pSelect = conn
					.prepareStatement(SqlConstants.DeleteStationaryNodeMap);
			pSelect.setString(1, sNDClientID);
			iReturn = pSelect.executeUpdate();
		} catch (SQLException e) {
			logger.error("localized message: " + e.getLocalizedMessage());
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return iReturn;
	}
	
	public GatewayLogBean getBatteryAndPower(String sDeviceId) {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement pSelect = null;
		int iReturn = 0;
		GatewayLogBean glb = new GatewayLogBean();
		try {
			conn = DbConnection.getConnection();
			pSelect = conn.prepareStatement(SqlConstants.GetBatteryAndPower);
			pSelect.setString(1, sDeviceId);
			rs = pSelect.executeQuery();
			while (rs.next()) {
				glb = new GatewayLogBean();
				if(rs.getString(1) == null || rs.getString(1).equals("")) glb.setGW_Device_ID("");
				else glb.setGW_Device_ID(rs.getString(1));
				glb.setGW_IsPowered(rs.getInt(6));
				glb.setGW_Battery(rs.getInt(7));
			}
		} catch (SQLException e) {
			logger.error("localized message: " + e.getLocalizedMessage());
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return glb;
	}

}
