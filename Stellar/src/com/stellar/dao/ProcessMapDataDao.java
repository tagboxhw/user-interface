package com.stellar.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.Collection;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.stellar.bean.MapsBean;
import com.stellar.bean.PlanMapsBean;
import com.stellar.util.DbConnection;
import com.stellar.util.SqlConstants;

public class ProcessMapDataDao {
	static final Logger logger = LoggerFactory
			.getLogger(ProcessMapDataDao.class);

	public Collection selectAllRecords() {
		Collection col = new Vector();
		Connection conn = null;
		PreparedStatement psSelect = null;
		ResultSet rs = null;
		MapsBean mapsBean = new MapsBean();
		Statement statement = null;
		try {
			conn = DbConnection.getConnection();

			// Create and execute a SELECT SQL statement.
			// Create and execute a SELECT SQL statement.
			try {
				statement = conn.createStatement();
			} catch (Exception e) {
				e.printStackTrace();
			}
			rs = statement.executeQuery(SqlConstants.SelectAllRecords);

			while (rs.next()) {
				mapsBean = new MapsBean();
				mapsBean.setGateway_id(rs.getString(1));
				mapsBean.setDevice_id(rs.getString(2));
				mapsBean.setDevice_time(rs.getString(3));
				mapsBean.setTemp(rs.getInt(4));
				mapsBean.setHumidity(rs.getInt(5));
				if (rs.getString(6).equals("1")) {
					mapsBean.setDoor_open(true);
				} else {
					mapsBean.setDoor_open(false);
				}
				mapsBean.setLatitude(rs.getFloat(7));
				mapsBean.setLongitude(rs.getFloat(8));
				mapsBean.setHalt_duration(rs.getInt(9));
				mapsBean.setOpen_duration(rs.getInt(10));
				if (rs.getString(11).equals("1")) {
					mapsBean.setTemp_alert(true);
				} else {
					mapsBean.setTemp_alert(false);
				}
				if (rs.getString(12).equals("1")) {
					mapsBean.setHumidity_alert(true);
				} else {
					mapsBean.setHumidity_alert(false);
				}
				if (rs.getString(13).equals("1")) {
					mapsBean.setHalt_alert(true);
				} else {
					mapsBean.setHalt_alert(false);
				}
				if (rs.getString(14).equals("1")) {
					mapsBean.setDetour_alert(true);
				} else {
					mapsBean.setDetour_alert(false);
				}
				if (rs.getString(15).equals("1")) {
					mapsBean.setDoor_alert(true);
				} else {
					mapsBean.setDoor_alert(false);
				}
				col.add(mapsBean);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (psSelect != null)
					psSelect.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return col;
	}

	public Collection selectPlanAllRecords() {
		Collection col = new Vector();
		Connection conn = null;
		PreparedStatement psSelect = null;
		ResultSet rs = null;
		PlanMapsBean mapsBean = new PlanMapsBean();
		Statement statement = null;
		try {
			conn = DbConnection.getConnection();

			try {
				statement = conn.createStatement();
			} catch (Exception e) {
				e.printStackTrace();
			}
			rs = statement.executeQuery(SqlConstants.SelectPlanAllRecords);

			while (rs.next()) {
				mapsBean = new PlanMapsBean();
				mapsBean.setGateway_id(rs.getString(1));
				mapsBean.setLatitude(rs.getFloat(2));
				mapsBean.setLongitude(rs.getFloat(3));
				mapsBean.setDCName(rs.getString(4));
				col.add(mapsBean);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (psSelect != null)
					psSelect.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return col;
	}

}
