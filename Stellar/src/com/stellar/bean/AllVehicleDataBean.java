package com.stellar.bean;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="allVehicleDataBean")
public class AllVehicleDataBean implements Serializable{

	/**
	 * Generated serial version ID
	 */
	private static final long serialVersionUID = -1874474658549948076L;
	
	private String Vehicle_Id;
	private String Route_Type;
	private String Source_Location_Id;
	private String Dest_Location_Id;
	private String Source_Location_Name;
	private String Dest_Location_Name;
	
	public String getVehicle_Id() {
		return Vehicle_Id;
	}

	@XmlElement
	public void setVehicle_Id(String vehicle_Id) {
		Vehicle_Id = vehicle_Id;
	}

	public String getRoute_Type() {
		return Route_Type;
	}

	@XmlElement
	public void setRoute_Type(String route_Type) {
		Route_Type = route_Type;
	}

	public String getSource_Location_Id() {
		return Source_Location_Id;
	}

	@XmlElement
	public void setSource_Location_Id(String source_Location_Id) {
		Source_Location_Id = source_Location_Id;
	}

	public String getDest_Location_Id() {
		return Dest_Location_Id;
	}

	@XmlElement
	public void setDest_Location_Id(String dest_Location_Id) {
		Dest_Location_Id = dest_Location_Id;
	}

	public String getSource_Location_Name() {
		return Source_Location_Name;
	}

	@XmlElement
	public void setSource_Location_Name(String source_Location_Name) {
		Source_Location_Name = source_Location_Name;
	}

	public String getDest_Location_Name() {
		return Dest_Location_Name;
	}

	@XmlElement
	public void setDest_Location_Name(String dest_Location_Name) {
		Dest_Location_Name = dest_Location_Name;
	}
	
	public AllVehicleDataBean(){
		super();
	}
	
	public void init() {
		Vehicle_Id = "";
		Route_Type = "";
		Source_Location_Id = "";
		Dest_Location_Id = "";
		Source_Location_Name = "";
		Dest_Location_Name = "";
	}
	
	public String toString(){
		String buf = null;
		buf = "All Vehicles Data Bean object: \n";
		buf = buf + "Vehicle_Id: " + Vehicle_Id + "\n";
		buf = buf + "Route_Type: " + Route_Type + "\n";
		buf = buf + "Source_Location_Id: " + Source_Location_Id + "\n";
		buf = buf + "Dest_Location_Id: " + Dest_Location_Id + "\n";
		buf = buf + "Source_Location_Name: " + Source_Location_Name + "\n";
		buf = buf + "Dest_Location_Name: " + Dest_Location_Name + "\n";
		return buf;
	}

	
}
