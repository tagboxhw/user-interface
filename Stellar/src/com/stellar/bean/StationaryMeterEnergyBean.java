package com.stellar.bean;

import java.io.Serializable;

public class StationaryMeterEnergyBean implements Serializable{

	/**
	 * Generated serial version ID
	 */
	private static final long serialVersionUID = -1874474658549948076L;
	
	private String GW_client_id;
	private String ND_client_id;
	private String Timestamp;
	private String PowerFactor;
	
	public StationaryMeterEnergyBean(){
		super();
	}
	
	public void init() {
		this.GW_client_id = "";
		this.ND_client_id = "";
		this.Timestamp = "";
		this.PowerFactor = "";
	}
	
	public String toString(){
		String buf = null;
		buf = "Stationary Meter Energy object: \n";
		buf = buf + "GW_client_id: " + GW_client_id + "\n";
		buf = buf + "GW_client_id: " + GW_client_id + "\n";
		buf = buf + "Timestamp: " + Timestamp + "\n";
		buf = buf + "PowerFactor: " + PowerFactor + "\n";
		return buf;
	}

	/**
	 * @return the gW_client_id
	 */
	public String getGW_client_id() {
		return GW_client_id;
	}

	/**
	 * @param gW_client_id the gW_client_id to set
	 */
	public void setGW_client_id(String gW_client_id) {
		GW_client_id = gW_client_id;
	}

	/**
	 * @return the nD_client_id
	 */
	public String getND_client_id() {
		return ND_client_id;
	}

	/**
	 * @param nD_client_id the nD_client_id to set
	 */
	public void setND_client_id(String nD_client_id) {
		ND_client_id = nD_client_id;
	}

	/**
	 * @return the timestamp
	 */
	public String getTimestamp() {
		return Timestamp;
	}

	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(String timestamp) {
		Timestamp = timestamp;
	}

	/**
	 * @return the PowerFactor
	 */
	public String getPowerFactor() {
		return PowerFactor;
	}

	/**
	 * @param PowerFactor the PowerFactor to set
	 */
	public void setPowerFactor(String PowerFactor) {
		this.PowerFactor = PowerFactor;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	

}
