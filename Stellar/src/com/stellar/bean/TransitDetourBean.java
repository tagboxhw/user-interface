package com.stellar.bean;

import java.io.Serializable;

public class TransitDetourBean implements Serializable{

	/**
	 * Generated serial version ID
	 */
	private static final long serialVersionUID = -1874474658549948076L;
	private String GW_client_id;
	private String Timestamp;
	private String lat;
	private String llong;
	
	public TransitDetourBean(){
		super();
	}
	
	public void init() {
		this.GW_client_id = "";
		this.Timestamp = "";
		this.lat = "";
		this.llong = "";
	}
	
	public String toString(){
		String buf = null;
		buf = "Transit Door Activity object: \n";
		buf = buf + "GW_client_id: " + GW_client_id + "\n";
		buf = buf + "Timestamp: " + Timestamp + "\n";
		buf = buf + "Lat: " + lat + "\n";
		buf = buf + "Long: " + llong + "\n";
		return buf;
	}

	/**
	 * @return the gW_client_id
	 */
	public String getGW_client_id() {
		return GW_client_id;
	}

	/**
	 * @param gW_client_id the gW_client_id to set
	 */
	public void setGW_client_id(String gW_client_id) {
		GW_client_id = gW_client_id;
	}

	
	/**
	 * @return the timestamp
	 */
	public String getTimestamp() {
		return Timestamp;
	}

	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(String timestamp) {
		Timestamp = timestamp;
	}

	

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the lat
	 */
	public String getLat() {
		return lat;
	}

	/**
	 * @param lat the lat to set
	 */
	public void setLat(String lat) {
		this.lat = lat;
	}

	/**
	 * @return the llong
	 */
	public String getLlong() {
		return llong;
	}

	/**
	 * @param llong the llong to set
	 */
	public void setLlong(String llong) {
		this.llong = llong;
	}
	
	

}
