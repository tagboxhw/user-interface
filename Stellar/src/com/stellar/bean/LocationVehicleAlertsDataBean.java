package com.stellar.bean;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="locationVehicleAlertsDataBean")
public class LocationVehicleAlertsDataBean implements Serializable{

	/**
	 * Generated serial version ID
	 */
	private static final long serialVersionUID = -1874474658549948076L;
	
	private String Class_Type;
	private String Vehicle_Id;
	private String Route_Type;
	private String Zone_Id;
	private String Alert_Type;
	private String Location_Id;
	private String Location_Name;
	private String City_Name;
	private String Location_Type;
	private int Alert_Count;
	
	public LocationVehicleAlertsDataBean(){
		super();
	}
	
	public void init() {
		Class_Type = "";
		Vehicle_Id = "";
		Route_Type = "";
		Zone_Id = "";
		Alert_Type = "";
		Location_Id = "";
		Location_Name = "";
		City_Name = "";
		Location_Type = "";
		Alert_Count = 0;
	}
	
	public String toString(){
		String buf = null;
		buf = "Locations Vehicles Alerts Data Bean object: \n";
		buf = buf + "class_type: " + Class_Type + "\n";
		buf = buf + "Vehicle_Id: " + Vehicle_Id + "\n";
		buf = buf + "Route_Type: " + Route_Type + "\n";
		buf = buf + "Zone_Id: " + Zone_Id + "\n";
		buf = buf + "Alert_Type: " + Alert_Type + "\n";
		buf = buf + "Location_Id: " + Location_Id + "\n";
		buf = buf + "city_name: " + City_Name + "\n";
		buf = buf + "Location_Name: " + Location_Name + "\n";
		buf = buf + "Location_Type: " + Location_Type + "\n";
		buf = buf + "Alert_Count: " + Alert_Count + "\n";
		return buf;
	}

	public String getLocation_Type() {
		return this.Location_Type;
	}

	@XmlElement
	public void setLocation_Type(String location_Type) {
		this.Location_Type = location_Type;
	}

	public String getLocation_Id() {
		return this.Location_Id;
	}

	@XmlElement
	public void setLocation_Id(String location_Id) {
		this.Location_Id = location_Id;
	}

	public String getLocation_Name() {
		return this.Location_Name;
	}

	@XmlElement
	public void setLocation_Name(String location_Name) {
		this.Location_Name = location_Name;
	}

	public String getClass_Type() {
		return Class_Type;
	}

	@XmlElement
	public void setClass_Type(String class_Type) {
		Class_Type = class_Type;
	}

	public String getVehicle_Id() {
		return Vehicle_Id;
	}

	@XmlElement
	public void setVehicle_Id(String vehicle_Id) {
		Vehicle_Id = vehicle_Id;
	}

	public String getRoute_Type() {
		return Route_Type;
	}

	@XmlElement
	public void setRoute_Type(String route_Type) {
		Route_Type = route_Type;
	}

	public String getZone_Id() {
		return Zone_Id;
	}

	@XmlElement
	public void setZone_Id(String zone_Id) {
		Zone_Id = zone_Id;
	}

	public String getAlert_Type() {
		return Alert_Type;
	}

	@XmlElement
	public void setAlert_Type(String alert_Type) {
		Alert_Type = alert_Type;
	}

	public String getCity_Name() {
		return City_Name;
	}

	@XmlElement
	public void setCity_Name(String city_Name) {
		City_Name = city_Name;
	}

	public int getAlert_Count() {
		return Alert_Count;
	}

	@XmlElement
	public void setAlert_Count(int alert_Count) {
		Alert_Count = alert_Count;
	}

}
