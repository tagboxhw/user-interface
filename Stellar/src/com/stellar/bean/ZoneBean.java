package com.stellar.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Vector;

public class ZoneBean implements Serializable{

	/**
	 * Generated serial version ID
	 */
	private static final long serialVersionUID = -1874474658549948076L;
	
	private String sSubLocId;
	private String sZoneName;
	private String sLocId;
	private String sLocName;
	private String sZoneId;
	private String sSubLocName;
	private int iRed_alert;
	private int iGreen_alert;
	/**
	 * @return the iRed_alert
	 */
	public int getRed_alert() {
		return iRed_alert;
	}

	/**
	 * @param iRed_alert the iRed_alert to set
	 */
	public void setRed_alert(int iRed_alert) {
		this.iRed_alert = iRed_alert;
	}
	
	/**
	 * @return the iRed_alert
	 */
	public String getLocationId() {
		return sLocId;
	}

	/**
	 * @param iRed_alert the iRed_alert to set
	 */
	public void setLocationId(String sId) {
		this.sLocId = sId;
	}
	

	/**
	 * @return the location_type
	 */
	public String getLocationName() {
		return sLocName;
	}

	/**
	 * @param location_type the location_type to set
	 */
	public void setLocationName(String location_name) {
		this.sLocName = location_name;
	}

	/**
	 * @return the location_id
	 */
	public String getSubLocationId() {
		return sSubLocId;
	}

	/**
	 * @param location_id the location_ to set
	 */
	public void setSubLocationId(String location_id) {
		this.sSubLocId = location_id;
	}
	
	/**
	 * @return the iGreen_alert
	 */
	public int getGreen_alert() {
		return iGreen_alert;
	}

	/**
	 * @param iGreen_alert the iGreen_alert to set
	 */
	public void setGreen_alert(int iGreen_alert) {
		this.iGreen_alert = iGreen_alert;
	}

	
	public ZoneBean(){
		super();
	}
	
	public void init() {
		this.sLocId = "";
		this.sLocName = "";
		this.sZoneId = "";
		this.sZoneName = "";
		this.sSubLocId = "";
		this.sSubLocName = "";
		this.iRed_alert = 0;
		this.iGreen_alert = 0;
	}
	
	public String toString(){
		String buf = null;
		buf = "Zone Bean object: \n";
		buf = buf + "location_Id: " + sLocId + "\n";
		buf = buf + "location_name: " + sLocName + "\n";
		buf = buf + "sub_location_id: " + sSubLocId + "\n";
		buf = buf + "sub_location_name: " + sSubLocName + "\n";
		buf = buf + "zone_id: " + sZoneId + "\n";
		buf = buf + "zone_name: " + sZoneName + "\n";
		buf = buf + "red_alert: " + iRed_alert + "\n";
		buf = buf + "green_alert: " + iGreen_alert + "\n";
		
		return buf;
	}

	/**
	 * @return the sZoneName
	 */
	public String getZoneName() {
		return sZoneName;
	}

	/**
	 * @param sZoneName the sZoneName to set
	 */
	public void setZoneName(String sZoneName) {
		this.sZoneName = sZoneName;
	}

	/**
	 * @return the sLocName
	 */
	public String getLocName() {
		return sLocName;
	}

	/**
	 * @param sLocName the sLocName to set
	 */
	public void setLocName(String sLocName) {
		this.sLocName = sLocName;
	}

	/**
	 * @return the sZoneId
	 */
	public String getZoneId() {
		return sZoneId;
	}

	/**
	 * @param sZoneId the sZoneId to set
	 */
	public void setZoneId(String sZoneId) {
		this.sZoneId = sZoneId;
	}

	/**
	 * @return the sSubLocName
	 */
	public String getSubLocName() {
		return sSubLocName;
	}

	/**
	 * @param sSubLocName the sSubLocName to set
	 */
	public void setSubLocName(String sSubLocName) {
		this.sSubLocName = sSubLocName;
	}
	

}
