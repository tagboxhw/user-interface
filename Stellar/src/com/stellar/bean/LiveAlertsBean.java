package com.stellar.bean;

import java.io.Serializable;

public class LiveAlertsBean implements Serializable{

	/**
	 * Generated serial version ID
	 */
	private static final long serialVersionUID = -1874474658549948076L;
	
	private String GW_Client_ID;
	private String ND_Client_ID;
	private String ClassType;
	private String Alert_ID;
	private String Alert_Location;
	
	private String Alert_Type;
	private String Breach_Type;
	private float Alert_Parameter_Avg;
	private String Alert_Timestamp_From;
	private String Alert_Timestamp_To;
	private int Alert_Duration;
	private String Alert_Severity;
	
	private String Current_Location_Lat;
	private String Current_Location_Long;
	private String Current_Location_Name;
	private String Route_Type;
	private String SMS_Sent;
		
	public LiveAlertsBean(){
		super();
	}
	
	public void init() {
		this.GW_Client_ID = "";
		this.ND_Client_ID = "";
		this.ClassType = "";
		this.Alert_ID = "";
		this.Alert_Location = "";
		this.Alert_Type = "";
		this.Breach_Type = "";
		this.Alert_Parameter_Avg = 0;
		this.Alert_Timestamp_From = "";
		this.Alert_Timestamp_To = "";
		this.Alert_Duration = 0;
		this.Alert_Severity = "";
		this.Current_Location_Lat = "";
		this.Current_Location_Long = "";
		this.Current_Location_Name = "";
		this.SMS_Sent = "";
		this.Route_Type = "";
	}
	
	public String toString(){
		String buf = null;
		buf = "Live Alerts object: \n";
		buf = buf + "GW_Client_ID: " + GW_Client_ID + "\n";
		buf = buf + "ND_Client_ID: " + ND_Client_ID + "\n";
		buf = buf + "Class: " + ClassType + "\n";
		buf = buf + "Alert_ID: " + Alert_ID + "\n";
		buf = buf + "Alert_Location: " + Alert_Location + "\n";
		buf = buf + "Alert_Type: " + Alert_Type + "\n";
		buf = buf + "Alert_Parameter_Avg: " + Alert_Parameter_Avg + "\n";
		buf = buf + "Alert_Timestamp_From: " + Alert_Timestamp_From + "\n";
		buf = buf + "Alert_Timestamp_To: " + Alert_Timestamp_To + "\n";
		buf = buf + "Breach_Type: " + Breach_Type + "\n";
		buf = buf + "Current_Location_Lat: " + Current_Location_Lat + "\n";
		buf = buf + "Current_Location_Long: " + Current_Location_Long + "\n";
		buf = buf + "Current_Location_Name: " + Current_Location_Name + "\n";
		buf = buf + "Route_Type: " + Route_Type + "\n";
		buf = buf + "SMS_Sent: " + SMS_Sent + "\n";
		buf = buf + "Alert_Severity: " + Alert_Severity + "\n";
		return buf;
	}

	/**
	 * @return the gW_Client_ID
	 */
	public String getGW_Client_ID() {
		return GW_Client_ID;
	}

	/**
	 * @param gW_Client_ID the gW_Client_ID to set
	 */
	public void setGW_Client_ID(String gW_Client_ID) {
		GW_Client_ID = gW_Client_ID;
	}

	/**
	 * @return the nD_Client_ID
	 */
	public String getND_Client_ID() {
		return ND_Client_ID;
	}

	/**
	 * @param nD_Client_ID the nD_Client_ID to set
	 */
	public void setND_Client_ID(String nD_Client_ID) {
		ND_Client_ID = nD_Client_ID;
	}

	/**
	 * @return the ClassType
	 */
	public String getClassType() {
		return this.ClassType;
	}

	/**
	 * @param ClassType the ClassType to set
	 */
	public void setClassType(String ClassType) {
		this.ClassType = ClassType;
	}

	/**
	 * @return the alert_ID
	 */
	public String getAlert_ID() {
		return Alert_ID;
	}

	/**
	 * @param alert_ID the alert_ID to set
	 */
	public void setAlert_ID(String alert_ID) {
		Alert_ID = alert_ID;
	}

	/**
	 * @return the alert_Location
	 */
	public String getAlert_Location() {
		return Alert_Location;
	}

	/**
	 * @param alert_Location the alert_Location to set
	 */
	public void setAlert_Location(String alert_Location) {
		Alert_Location = alert_Location;
	}

	/**
	 * @return the alert_Type
	 */
	public String getAlert_Type() {
		return Alert_Type;
	}

	/**
	 * @param alert_Type the alert_Type to set
	 */
	public void setAlert_Type(String alert_Type) {
		Alert_Type = alert_Type;
	}

	/**
	 * @return the alert_Parameter_Avg
	 */
	public float getAlert_Parameter_Avg() {
		return Alert_Parameter_Avg;
	}

	/**
	 * @param alert_Parameter_Avg the alert_Parameter_Avg to set
	 */
	public void setAlert_Parameter_Avg(float alert_Parameter_Avg) {
		Alert_Parameter_Avg = alert_Parameter_Avg;
	}

	/**
	 * @return the alert_Timestamp_From
	 */
	public String getAlert_Timestamp_From() {
		return Alert_Timestamp_From;
	}

	/**
	 * @param alert_Timestamp_From the alert_Timestamp_From to set
	 */
	public void setAlert_Timestamp_From(String alert_Timestamp_From) {
		Alert_Timestamp_From = alert_Timestamp_From;
	}

	/**
	 * @return the alert_Timestamp_To
	 */
	public String getAlert_Timestamp_To() {
		return Alert_Timestamp_To;
	}

	/**
	 * @param alert_Timestamp_To the alert_Timestamp_To to set
	 */
	public void setAlert_Timestamp_To(String alert_Timestamp_To) {
		Alert_Timestamp_To = alert_Timestamp_To;
	}

	/**
	 * @return the alert_Duration
	 */
	public int getAlert_Duration() {
		return Alert_Duration;
	}

	/**
	 * @param alert_Duration the alert_Duration to set
	 */
	public void setAlert_Duration(int alert_Duration) {
		Alert_Duration = alert_Duration;
	}

	/**
	 * @return the alert_Severity
	 */
	public String getAlert_Severity() {
		return Alert_Severity;
	}

	/**
	 * @param alert_Severity the alert_Severity to set
	 */
	public void setAlert_Severity(String alert_Severity) {
		Alert_Severity = alert_Severity;
	}

	

	/**
	 * @return the route_Type
	 */
	public String getRoute_Type() {
		return Route_Type;
	}

	/**
	 * @param route_Type the route_Type to set
	 */
	public void setRoute_Type(String route_Type) {
		Route_Type = route_Type;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the breach_Type
	 */
	public String getBreach_Type() {
		return Breach_Type;
	}

	/**
	 * @param breach_Type the breach_Type to set
	 */
	public void setBreach_Type(String breach_Type) {
		Breach_Type = breach_Type;
	}

	/**
	 * @return the current_Location_Lat
	 */
	public String getCurrent_Location_Lat() {
		return Current_Location_Lat;
	}

	/**
	 * @param current_Location_Lat the current_Location_Lat to set
	 */
	public void setCurrent_Location_Lat(String current_Location_Lat) {
		Current_Location_Lat = current_Location_Lat;
	}

	/**
	 * @return the current_Location_Long
	 */
	public String getCurrent_Location_Long() {
		return Current_Location_Long;
	}

	/**
	 * @param current_Location_Long the current_Location_Long to set
	 */
	public void setCurrent_Location_Long(String current_Location_Long) {
		Current_Location_Long = current_Location_Long;
	}

	/**
	 * @return the current_Location_Name
	 */
	public String getCurrent_Location_Name() {
		return Current_Location_Name;
	}

	/**
	 * @param current_Location_Name the current_Location_Name to set
	 */
	public void setCurrent_Location_Name(String current_Location_Name) {
		Current_Location_Name = current_Location_Name;
	}

	/**
	 * @return the sMS_Sent
	 */
	public String getSMS_Sent() {
		return SMS_Sent;
	}

	/**
	 * @param sMS_Sent the sMS_Sent to set
	 */
	public void setSMS_Sent(String sMS_Sent) {
		SMS_Sent = sMS_Sent;
	}
	

}
