package com.stellar.bean;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="allLocationDataBean")
public class AllLocationDataBean implements Serializable{

	/**
	 * Generated serial version ID
	 */
	private static final long serialVersionUID = -1874474658549948076L;
	
	private String Location_Type;
	private String Location_Id;
	private String Location_Name;
	private String Location_Lat;
	private String Location_Long;
	private String City_Name;
	private String Zone_Type;
	private String Zone_Name;
	private String Zone_Id;
	private String Subzone_Id;
	private String Subzone_Name;
	private String Subzone_Type;
	
	public AllLocationDataBean(){
		super();
	}
	
	public void init() {
		Location_Type = "";
		Location_Id = "";
		Location_Name = "";
		Location_Lat = "";
		Location_Long = "";
		City_Name = "";
		Zone_Type = "";
		Zone_Name = "";
		Zone_Id = "";
		Subzone_Id = "";
		Subzone_Name = "";
		Subzone_Type = "";
	}
	
	public String toString(){
		String buf = null;
		buf = "All Locations Data Bean object: \n";
		buf = buf + "location_id: " + Location_Id + "\n";
		buf = buf + "location_type: " + Location_Type + "\n";
		buf = buf + "location_name: " + Location_Name + "\n";
		buf = buf + "location_lat: " + Location_Lat + "\n";
		buf = buf + "location_long: " + Location_Long + "\n";
		buf = buf + "city_name: " + City_Name + "\n";
		buf = buf + "zone_type: " + Zone_Type + "\n";
		buf = buf + "zone_name: " + Zone_Name + "\n";
		buf = buf + "zone_id: " + Zone_Id + "\n";
		buf = buf + "subzone_id: " + Subzone_Id + "\n";
		buf = buf + "subzone_name: " + Subzone_Name + "\n";
		buf = buf + "Subzone_Type: " + Subzone_Type + "\n";
		return buf;
	}

	public String getLocation_Type() {
		return this.Location_Type;
	}

	@XmlElement
	public void setLocation_Type(String location_Type) {
		this.Location_Type = location_Type;
	}

	public String getLocation_Id() {
		return this.Location_Id;
	}

	@XmlElement
	public void setLocation_Id(String location_Id) {
		this.Location_Id = location_Id;
	}

	public String getLocation_Name() {
		return this.Location_Name;
	}

	@XmlElement
	public void setLocation_Name(String location_Name) {
		this.Location_Name = location_Name;
	}

	public String getLocation_Lat() {
		return this.Location_Lat;
	}

	@XmlElement
	public void setLocation_Lat(String location_Lat) {
		this.Location_Lat = location_Lat;
	}

	public String getLocation_Long() {
		return this.Location_Long;
	}

	@XmlElement
	public void setLocation_Long(String location_Long) {
		this.Location_Long = location_Long;
	}

	public String getCity_Name() {
		return this.City_Name;
	}

	@XmlElement
	public void setCity_Name(String city_Name) {
		this.City_Name = city_Name;
	}

	public String getZone_Type() {
		return this.Zone_Type;
	}

	@XmlElement
	public void setZone_Type(String zone_Type) {
		this.Zone_Type = zone_Type;
	}

	public String getZone_Name() {
		return this.Zone_Name;
	}

	@XmlElement
	public void setZone_Name(String zone_Name) {
		this.Zone_Name = zone_Name;
	}

	public String getZone_Id() {
		return this.Zone_Id;
	}

	@XmlElement
	public void setZone_Id(String zone_Id) {
		this.Zone_Id = zone_Id;
	}

	public String getSubzone_Id() {
		return this.Subzone_Id;
	}

	@XmlElement
	public void setSubzone_Id(String subzone_Id) {
		this.Subzone_Id = subzone_Id;
	}

	public String getSubzone_Name() {
		return this.Subzone_Name;
	}

	@XmlElement
	public void setSubzone_Name(String subzone_Name) {
		this.Subzone_Name = subzone_Name;
	}
	
	public String getSubzone_Type() {
		return Subzone_Type;
	}

	@XmlElement
	public void setSubzone_Type(String subzone_Type) {
		Subzone_Type = subzone_Type;
	}
}
