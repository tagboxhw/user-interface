package com.stellar.bean;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "parameterSummaryBean")
public class ParameterSummaryBean implements Serializable{

	/**
	 * Generated serial version ID
	 */
	private static final long serialVersionUID = -1874474658549948076L;
	private String Location_ID;
	private String Zone_ID;
	private String Sub_Zone_ID;
	private String Max_Value;
	private String Min_Value;
	private String Mean_Value;
	private String Volatility;
	private String Totals;
	private String Excursion;
	private String Vol_Ap;
	private String Date_R;
	
	public ParameterSummaryBean(){
		super();
	}
	
	public void init() {
		this.Location_ID = "";
		this.Zone_ID = "";
		this.Sub_Zone_ID = "";
		this.Max_Value = "";
		this.Min_Value = "";
		this.Mean_Value = "";
		this.Volatility = "";
		this.Totals = "";
		this.Excursion = "";
		this.Vol_Ap = "";
		this.Date_R = "";
	}
	
	public String toString(){
		String buf = null;
		buf = "Parameter Summary object: \n";
		buf = buf + "Location_ID: " + Location_ID + "\n";
		buf = buf + "Zone_ID: " + Zone_ID + "\n";
		buf = buf + "Sub_Zone_ID: " + Sub_Zone_ID + "\n";
		buf = buf + "Max_Value: " + Max_Value + "\n";
		buf = buf + "Min_Value: " + Min_Value + "\n";
		buf = buf + "Mean_Value: " + Mean_Value + "\n";
		buf = buf + "Volatility: " + Volatility + "\n";
		buf = buf + "Totals: " + Totals + "\n";
		buf = buf + "Excursion: " + Excursion + "\n";
		buf = buf + "Vol_Ap: " + Vol_Ap + "\n";
		buf = buf + "Date_R: " + Date_R + "\n";
		return buf;
	}

	public String getDate_R() {
		return Date_R;
	}

	public void setDate_R(String date_R) {
		Date_R = date_R;
	}

	public String getLocation_ID() {
		return Location_ID;
	}

	@XmlElement
	public void setLocation_ID(String location_ID) {
		Location_ID = location_ID;
	}

	public String getZone_ID() {
		return Zone_ID;
	}
	
	@XmlElement
	public void setZone_ID(String zone_ID) {
		Zone_ID = zone_ID;
	}

	public String getMax_Value() {
		return Max_Value;
	}
	
	@XmlElement
	public void setMax_Value(String max_Value) {
		Max_Value = max_Value;
	}

	public String getMin_Value() {
		return Min_Value;
	}

	@XmlElement
	public void setMin_Value(String min_Value) {
		Min_Value = min_Value;
	}

	public String getMean_Value() {
		return Mean_Value;
	}

	@XmlElement
	public void setMean_Value(String mean_Value) {
		Mean_Value = mean_Value;
	}

	public String getVolatility() {
		return Volatility;
	}

	@XmlElement
	public void setVolatility(String volatility) {
		Volatility = volatility;
	}

	public String getTotals() {
		return Totals;
	}

	@XmlElement
	public void setTotals(String totals) {
		Totals = totals;
	}

	public String getExcursion() {
		return Excursion;
	}

	@XmlElement
	public void setExcursion(String excursion) {
		Excursion = excursion;
	}

	public String getVol_Ap() {
		return Vol_Ap;
	}

	@XmlElement
	public void setVol_Ap(String vol_Ap) {
		Vol_Ap = vol_Ap;
	}

	public String getSub_Zone_ID() {
		return Sub_Zone_ID;
	}

	public void setSub_Zone_ID(String sub_Zone_ID) {
		Sub_Zone_ID = sub_Zone_ID;
	}

}
