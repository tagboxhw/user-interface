package com.stellar.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Vector;

public class ImageCorSelectBean implements Serializable{

	/**
	 * Generated serial version ID
	 */
	private static final long serialVersionUID = -1874474658549948076L;
	
	private String sSubZoneId;
	private String sTimestamp;
	private String sHumidity;
	private String sTemperature;

	public String getsSubZoneId() {
		return sSubZoneId;
	}

	public void setsSubZoneId(String sSubZoneId) {
		this.sSubZoneId = sSubZoneId;
	}

	public String getsTimestamp() {
		return sTimestamp;
	}

	public void setsTimestamp(String sTimestamp) {
		this.sTimestamp = sTimestamp;
	}

	public String getsHumidity() {
		return sHumidity;
	}

	public void setsHumidity(String sHumidity) {
		this.sHumidity = sHumidity;
	}

	public String getsTemperature() {
		return sTemperature;
	}

	public void setsTemperature(String sTemperature) {
		this.sTemperature = sTemperature;
	}

	public ImageCorSelectBean(){
		super();
	}
	
	public void init() {
		this.sSubZoneId = "";
		this.sTimestamp = "";
		this.sHumidity = "";
		this.sTemperature = "";
	}
	
	public String toString(){
		String buf = null;
		buf = "ImageCorSelct Bean object: \n";
		buf = buf + "sSubZoneId: " + sSubZoneId + "\n";
		buf = buf + "sTimestamp: " + sTimestamp + "\n";
		buf = buf + "sHumidity: " + sHumidity + "\n";
		buf = buf + "sTemperature: " + sTemperature + "\n";
		
		return buf;
	}
	

}
