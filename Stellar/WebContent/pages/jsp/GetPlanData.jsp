
<%@ page import="com.stellar.action.MapAction"%>
<%@ page import="com.stellar.bean.PlanMapsBean"%>
<%@ page import="java.util.Collection"%>
<%@ page import="java.util.Vector"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>

<!-- <?xml version="1.0" encoding="ISO-8859-1"?> -->

<%!static final Logger logger = LoggerFactory.getLogger("GetPlanData");
	MapAction mapAction = new MapAction();
	String buf = "";
	String sString = "";
	String sErrorMessage = null;
	String sId = null;
	String sRowId = null;
	PlanMapsBean mapbean = new PlanMapsBean();%>
<%
	try {
		buf = "";
		//response.setContentType("text/xml");
		//buf += ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		Collection col = mapAction.getPlanAllRows();
		Iterator<PlanMapsBean> i = col.iterator();
		int iCount = 0;
		//logger.info("<rows total_count='100' pos='0'>");
		buf = "<rows>";
		while (i.hasNext()) {
			mapbean = (PlanMapsBean) i.next();
			buf += ("<row id=\"" + iCount + "\">");
			buf += ("<gatewayId>" + mapbean.getGateway_id() + "</gatewayId>");
			buf += ("<dcName>" + mapbean.getDCName() + "</dcName>");
			buf += ("<latitude>" + mapbean.getLatitude() + "</latitude>");
			buf += ("<longitude>" + mapbean.getLongitude() + "</longitude>");
			buf += ("</row>");
			iCount++;
		}
		buf += "</rows>";
		buf = buf.trim();
		//logger.info(buf);
		out.clear();
		out.println(buf);
	} catch (Exception e) {
		logger.error("Exception: " + e.getLocalizedMessage());
		e.printStackTrace();
	}
%>