<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ page import="com.stellar.dao.ClientLocationMapDao"%>
<%@ page import="com.stellar.dao.TransitVehicleMapDao"%>
<%@ page import="com.stellar.dao.TransitDriverMapDao"%>
<%@ page import="com.stellar.bean.TransitDriverMapBean"%>
<%@ page import="com.stellar.bean.TransitVehicleMapBean"%>
<%@ page import="com.stellar.bean.ClientLocationMapBean"%>
<%@ page import="com.stellar.dao.TransitDao"%>
<%@ page import="com.stellar.dao.AlertsWorkflowDao"%>
<%@ page import="com.stellar.util.Utils"%>
<%@ page import="com.stellar.util.Constants"%>
<%@ page import="java.util.TreeMap"%>
<%@ page import="java.util.Set"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.util.Collection"%>
<%@ page import="java.util.Set"%>
<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>
<%@ page import="com.stellar.bean.AllVehicleDataBean"%>
<%@ page import="com.stellar.bean.TodaysAlertWorkflowBean"%>

<%!
static final Logger logger = LoggerFactory.getLogger("TodayColdChainHealthVehicle");
%>
<%
String sUsername = (String) session.getAttribute("username");
if(sUsername == null || sUsername.equals("")) {
	session.setAttribute("callingPage", "TodayColdChainHealthVehicle.jsp");
	response.sendRedirect("../../index.html");
}
sUsername = (String) session.getAttribute("username");
String sVehicleIdParam = "", sVehicleTypeParam = "", sSourceParam = "", sDestParam = "", sCurrentLocationParam = "", sDriverIDParam = "";
sVehicleIdParam = request.getParameter("VehicleID");
if(sVehicleIdParam == null || sVehicleIdParam.equals("")) sVehicleIdParam = "ALL";
sVehicleTypeParam = request.getParameter("VehicleType");
if(sVehicleTypeParam == null || sVehicleTypeParam.equals("")) sVehicleTypeParam = "ALL";
sSourceParam = request.getParameter("Source");
if(sSourceParam == null || sSourceParam.equals("")) sSourceParam = "ALL";
sDestParam = request.getParameter("Destination");
if(sDestParam == null || sDestParam.equals("")) sDestParam = "ALL";
sCurrentLocationParam = request.getParameter("CurrentLocation");
if(sCurrentLocationParam == null || sCurrentLocationParam.equals("")) sCurrentLocationParam = "ALL";
sDriverIDParam = request.getParameter("DriverID");
if(sDriverIDParam == null || sDriverIDParam.equals("")) sDriverIDParam = "ALL";
String sListView = request.getParameter("showView");
String sType = request.getParameter("Type");
if(sType == null || sType.equals("")) sType = Constants.ROUTE_TYPE_MU_DC;
else if(sType.equals("MUDC")) sType = Constants.ROUTE_TYPE_MU_DC;
else if(sType.equals("DCDC")) sType = Constants.ROUTE_TYPE_DC_DC;
else if(sType.equals("DCEP")) sType = Constants.ROUTE_TYPE_DC_EP;
System.out.println(sListView);
TransitDao trDao = new TransitDao();
int iMUDCCount = 0, iDCDCCount = 0, iDCEPCount = 0;
int iTodayRedMUDC = 0, iTodayOrangeMUDC = 0, iTodayGreenMUDC = 0;
int iTodayRedDCDC = 0, iTodayOrangeDCDC = 0, iTodayGreenDCDC = 0,iTodayRedDCEP = 0, iTodayOrangeDCEP = 0, iTodayGreenDCEP = 0;
int iCompRedMUDC = 0, iCompOrangeMUDC = 0, iCompGreenMUDC = 0;
int iCompRedDCDC = 0, iCompOrangeDCDC = 0, iCompGreenDCDC = 0,iCompRedDCEP = 0, iCompOrangeDCEP = 0, iCompGreenDCEP = 0;
int iTemperatureVehCount = 0, iHumidityVehCount = 0, iDoorOpenVehCount = 0, iStoppageVehCount = 0;
int iCompTemperatureVehCount = 0, iCompHumidityVehCount = 0, iCompDoorOpenVehCount = 0, iCompStoppageVehCount = 0;
if(trDao.colAllVehicles == null || trDao.colAllVehicles.size() == 0) trDao.getAllVehiclesData();
for(AllVehicleDataBean alBean: trDao.colAllVehicles){
	if(alBean.getRoute_Type().equals(Constants.ROUTE_TYPE_MU_DC)){
		iMUDCCount++;
	} else if(alBean.getRoute_Type().equals(Constants.ROUTE_TYPE_DC_DC)){
		iDCDCCount++;
	} else if(alBean.getRoute_Type().equals(Constants.ROUTE_TYPE_DC_EP)){
		iDCEPCount++;
	} else {
		
	}
}
AlertsWorkflowDao awd = new AlertsWorkflowDao();
TodaysAlertWorkflowBean tawBean = new TodaysAlertWorkflowBean();
Collection<TodaysAlertWorkflowBean> colTodayAlerts = awd.getTodaysAlerts();
Collection<TodaysAlertWorkflowBean> colAlertsComp = awd.getAlertsComp();
TreeMap<String, Integer> tmAlerts = new TreeMap<String, Integer>();
int iCount = 0;
for(TodaysAlertWorkflowBean taw: colTodayAlerts) {
	if(taw.getRoute_Type().equals(Constants.ROUTE_TYPE_MU_DC)) {
		if(taw.getCount_Of_Alerts() > 3) iTodayRedMUDC++;
		else if(taw.getCount_Of_Alerts() <= 3 && taw.getCount_Of_Alerts() > 0) iTodayOrangeMUDC++;
		else if(taw.getCount_Of_Alerts() == 0) iTodayGreenMUDC++;
	} else if(taw.getRoute_Type().equals(Constants.ROUTE_TYPE_DC_DC)) {
		if(taw.getCount_Of_Alerts() > 3) iTodayRedDCDC++;
		else if(taw.getCount_Of_Alerts() <= 3 && taw.getCount_Of_Alerts() > 0) iTodayOrangeDCDC++;
		else if(taw.getCount_Of_Alerts() == 0) iTodayGreenDCDC++;
	} else if(taw.getRoute_Type().equals(Constants.ROUTE_TYPE_DC_EP)) {
		if(taw.getCount_Of_Alerts() > 3) iTodayRedDCEP++;
		else if(taw.getCount_Of_Alerts() <= 3 && taw.getCount_Of_Alerts() > 0) iTodayOrangeDCEP++;
		else if(taw.getCount_Of_Alerts() == 0) iTodayGreenDCEP++;
	}
	if(taw.getRoute_Type().equals(sType)) {	
		if(taw.getAlert_Type().equals(Constants.TEMPERATURE)) iTemperatureVehCount++;
		else if(taw.getAlert_Type().equals(Constants.HUMIDITY)) iHumidityVehCount++;
		else if(taw.getAlert_Type().equals(Constants.DOOR_ACTIVITY)) iDoorOpenVehCount++;
		else if(taw.getAlert_Type().equals(Constants.STOPPAGE)) iStoppageVehCount++;
		if(tmAlerts.containsKey(taw.getVehicle_ID())) {
			iCount = (Integer) tmAlerts.get(taw.getVehicle_ID());
			tmAlerts.put(taw.getVehicle_ID(), (iCount+1));
		} else {
			tmAlerts.put(taw.getVehicle_ID(), 1);
		}
	}
}
for(TodaysAlertWorkflowBean taw: colAlertsComp) {
	
	if(taw.getRoute_Type().equals(Constants.ROUTE_TYPE_MU_DC)) {
		if(taw.getCount_Of_Alerts() > 3) iCompRedMUDC++;
		else if(taw.getCount_Of_Alerts() <= 3 && taw.getCount_Of_Alerts() > 0) iCompOrangeMUDC++;
		else if(taw.getCount_Of_Alerts() == 0) iCompGreenMUDC++;
	} else if(taw.getRoute_Type().equals(Constants.ROUTE_TYPE_DC_DC)) {
		if(taw.getCount_Of_Alerts() > 3) iCompRedDCDC++;
		else if(taw.getCount_Of_Alerts() <= 3 && taw.getCount_Of_Alerts() > 0) iCompOrangeDCDC++;
		else if(taw.getCount_Of_Alerts() == 0) iCompGreenDCDC++;
	} else if(taw.getRoute_Type().equals(Constants.ROUTE_TYPE_DC_EP)) {
		if(taw.getCount_Of_Alerts() > 3) iCompRedDCEP++;
		else if(taw.getCount_Of_Alerts() <= 3 && taw.getCount_Of_Alerts() > 0) iCompOrangeDCEP++;
		else if(taw.getCount_Of_Alerts() == 0) iCompGreenDCEP++;
	}
	if(taw.getRoute_Type().equals(sType)) {	
		if(taw.getAlert_Type().equals(Constants.TEMPERATURE)) iCompTemperatureVehCount++;
		else if(taw.getAlert_Type().equals(Constants.HUMIDITY)) iCompHumidityVehCount++;
		else if(taw.getAlert_Type().equals(Constants.DOOR_ACTIVITY)) iCompDoorOpenVehCount++;
		else if(taw.getAlert_Type().equals(Constants.STOPPAGE)) iCompStoppageVehCount++;
	}
}
int iRedMan = 0, iOrangeMan = 0, iRedDC = 0, iOrangeDC = 0, iRedMUDC = 0, iOrangeMUDC = 0, iRedDCDC = 0, iOrangeDCDC = 0, iRedDCEP = 0, iOrangeDCEP = 0;
if(iCompRedMUDC > 0) iRedMUDC = ((iTodayRedMUDC-iCompRedMUDC)/iCompRedMUDC)*100;
if(iCompOrangeMUDC > 0) iOrangeMUDC = ((iTodayOrangeMUDC-iCompOrangeMUDC)/iCompOrangeMUDC)*100;
if(iCompRedDCDC > 0) iRedDCDC = ((iTodayRedDCDC-iCompRedDCDC)/iCompRedDCDC)*100;
if(iCompOrangeDCDC > 0) iOrangeDCDC = ((iTodayOrangeDCDC-iCompOrangeDCDC)/iCompOrangeDCDC)*100;
if(iCompRedDCEP > 0) iRedDCEP = ((iTodayRedDCEP-iCompRedDCEP)/iCompRedDCEP)*100;
if(iCompOrangeDCEP > 0) iOrangeDCEP = ((iTodayOrangeDCEP-iCompOrangeDCEP)/iCompOrangeDCEP)*100;

int iDiffTemperatureCRCount = 0, iDiffHumidityCRCount = 0, iDiffDoorOpenCRCount = 0, iDiffTemperatureVehCount = 0, iDiffHumidityVehCount = 0, 
	iDiffDoorOpenVehCount = 0, iDiffStoppageVehCount = 0;

if(iCompTemperatureVehCount > 0) iDiffTemperatureVehCount = ((iTemperatureVehCount-iCompTemperatureVehCount)/iCompTemperatureVehCount)*100;
if(iCompHumidityVehCount > 0) iDiffHumidityVehCount = ((iHumidityVehCount-iCompHumidityVehCount)/iCompHumidityVehCount)*100;
if(iCompDoorOpenVehCount > 0) iDiffDoorOpenVehCount = ((iDoorOpenVehCount-iCompDoorOpenVehCount)/iCompDoorOpenVehCount)*100;
if(iCompStoppageVehCount > 0) iDiffStoppageVehCount = ((iStoppageVehCount-iCompStoppageVehCount)/iCompStoppageVehCount)*100;


java.util.Date dt = new java.util.Date();
java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd-MM-yyyy HH:mm");
sdf.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
String currentTime = sdf.format(dt);
ClientLocationMapDao clmDao = new ClientLocationMapDao();
TransitVehicleMapDao tvmDao = new TransitVehicleMapDao();
TransitDriverMapDao tdmDao = new TransitDriverMapDao();
TransitVehicleMapBean tvmBean = new TransitVehicleMapBean();
ClientLocationMapBean clmBean = new ClientLocationMapBean();

Collection<TransitVehicleMapBean> col = tvmDao.selectVehicles();
TreeMap tmLocations = clmDao.selectLocations();

Iterator iter = col.iterator();
String sVehicleId = "", sVehicleType = "", sSource = "", sDest = "", sCurrentLocation = "", sDriverID = "";
int iRunning = 0, iAssigned = 0, iAvailable = 0, iUnavailable = 0;
while(iter.hasNext()) {
	tvmBean = (TransitVehicleMapBean) iter.next();
	if(tmLocations.containsKey(tvmBean.getSource())) {
		clmBean = (ClientLocationMapBean) tmLocations.get(tvmBean.getSource());
		tvmBean.setSource(clmBean.getLocationName());
	}
	if(tmLocations.containsKey(tvmBean.getDestination())) {
		clmBean = (ClientLocationMapBean) tmLocations.get(tvmBean.getDestination());
		tvmBean.setDestination(clmBean.getLocationName());
	}
	if(tvmBean.getLatitude() == null || tvmBean.getLatitude().equals("")) tvmBean.setLatitude("12.993709");
	if(tvmBean.getLongitude() == null || tvmBean.getLongitude().equals("")) tvmBean.setLongitude("77.681804");
	if(!sVehicleId.contains(tvmBean.getVehicle_ID())){ sVehicleId += tvmBean.getVehicle_ID() + ";";}
	if(!sVehicleType.contains(tvmBean.getVehicle_Type())){ sVehicleType += tvmBean.getVehicle_Type() + ";";}
	if(!sSource.contains(tvmBean.getSource())){ sSource += tvmBean.getSource() + ";";}
	if(!sDest.contains(tvmBean.getDestination())){ sDest += tvmBean.getDestination() + ";";}
	if(!sCurrentLocation.contains(tvmBean.getSource())){ sCurrentLocation += tvmBean.getSource() + ";";}//This should be changed to Current Location
	if(!sDriverID.contains(tvmBean.getDriver_DL_ID())){ sDriverID += tvmBean.getDriver_DL_ID() + ";";}
	if(tvmBean.getVehicle_Status().equalsIgnoreCase("Assigned")) iAssigned++;
	else if(tvmBean.getVehicle_Status().equalsIgnoreCase("Running")) iRunning++;
	else if(tvmBean.getVehicle_Status().equalsIgnoreCase("Available")) iAvailable++;
	else if(tvmBean.getVehicle_Status().equalsIgnoreCase("Unavailable")) iUnavailable++;
}
String[] sVehicleIdArray = sVehicleId.split(";");
String[] sVehicleTypeArray = sVehicleType.split(";");
String[] sSourceArray = sSource.split(";");
String[] sDestArray = sDest.split(";");
String[] sCurrentLocationArray = sCurrentLocation.split(";");
String[] sDriverIDArray = sDriverID.split(";");

TreeMap tmDrivers = tdmDao.selectDrivers();
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Dashboard</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Add these below two lines for autocomplete and the javascript function at the end  -->
<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="../../bootstrap/css/bootstrap.css">
   
<!-- Font Awesome -->
<script src="https://use.fontawesome.com/37ba1872d2.js"></script>
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../../dist/css/tagbox.css">
<link href="../../dist/css/select2.css" rel="stylesheet" />
<script src="../../dist/js/select2.js"></script>
<script src="../../dist/js/tagbox.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#VehicleID").select2();
	$("#Source").select2();
	$("#Destination").select2();
	$("#TripStatus").select2();
	$("#Alerts").select2();
	
});
</script>
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
<link href="https://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
  body {
	font-family:  Lato, sans-serif;
	font-weight: 600;
}
</style>
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
	<div class="wrapper">

		<header class="main-header">
			<!-- Logo -->
			<div>
				<a href="#" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
					<span class="logo-mini"><b>Demo</b></span> <!-- logo for regular state and mobile devices -->
					<span class="logo-lg"><b>Demo</b> </span>
				</a>
			</div>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">

				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
					role="button"> <span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
				</a>
				<form class="navbar-form pull-left" role="search">
					<div class="form-group">
						<b style="padding-left: 15px; font-size: 20px;" class="pull-right">Today's Vehicle Health Summary</b><br> <b style="padding-left: 15px"><%= currentTime %></b>
					</div>
				</form>

				<div class="navbar-custom-menu">
					<img src="../../dist/img/Logo.png" class="logo-class pull-right"
						alt="User Image">
				</div>
			</nav>

		</header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->


				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<br>
				<ul class="sidebar-menu">
					<li class="header">MAIN NAVIGATION</li>
					<li class="treeview active"><a href="#"> <i
							class="fa fa-dashboard"></i> <span>Dashboard</span>
					</a>
						<ul class="treeview-menu">
							<li class=""><a href="CMCDashboard.jsp"><i
									class="fa fa-circle-o"></i> CMC</a></li>
							<!-- <li class=""><a href="NewDashboard.jsp"><i
									class="fa fa-circle-o"></i> Vehicle Dashboard</a></li> -->
							<li class=""><a href="TodayColdChainHealth.jsp"><i
									class="fa fa-circle-o"></i> Cold Rooms Summary</a></li>
							<!--  <li><a href="VehiclePlannedRouteMap.jsp"><i class="fa fa-circle-o"></i> Vehicle Planned Route</a></li> -->
							<li class="active"><a href="TodayColdChainHealthVehicle.jsp"><i
									class="fa fa-circle-o"></i> Vehicles Summary</a></li>
						</ul></li>
					<li class="treeview"><a href="#"> <i
							class="fa fa-files-o"></i> <span>Alerts</span> 
					</a>
						<ul class="treeview-menu">
							<li><a href="LiveVehicleTracking.jsp"><i
									class="fa fa-circle-o"></i> Live Vehicle Tracking</a></li>
						</ul></li>
					
					<li class="treeview"><a href="#"> <i
							class="fa fa-database"></i> <span>Admin</span>
					</a>
						<ul class="treeview-menu">
							<li class=""><a href="VehicleManagement.jsp"><i class="fa fa-circle-o"></i>
									Vehicle Management</a></li>
							<li><a href="VendorScorecard.jsp"><i class="fa fa-circle-o"></i>
									Vehicle Score Card</a></li>
						</ul></li>
						<li class="treeview"><a href="#"> <i
							class="fa fa-bar-chart"></i> <span>Reports</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="ParameterSummary.jsp"><i class="fa fa-circle-o"></i>
									Parameter Summary</a></li>
							<li><a href="AlertsSummary.jsp"><i class="fa fa-circle-o"></i>
									Alerts Summary</a></li>
							<li><a href="AlertsReport.jsp"><i class="fa fa-circle-o"></i>
									Alerts Report</a></li>		
							
						</ul></li>
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			
		<section id="mainSection" class="content">
			<div class="row">
				<div class="col-md-12">
				<b><a href="CMCDashboard.jsp">CMC</a></b> &nbsp;<i class="fa fa-caret-right"></i> &nbsp;<b>MU <i class="fa fa-long-arrow-right"></i> DC</b>
				<span class="pull-right"><i>Vehicles with: &nbsp;&nbsp;
				<i class="fa fa-square alert-text alert-font"></i>&nbsp; >3 Alerts&nbsp;&nbsp;
				<i class="fa fa-square warning-text alert-font"></i>&nbsp; 1-3 Alerts&nbsp;&nbsp;
				<i class="fa fa-square success-text alert-font"></i>&nbsp; No Alerts</i>
				</span>
				<br> <br>
					<div class="row">
						<div class="col-md-4">
						<%if(sType.equals(Constants.ROUTE_TYPE_MU_DC)) { %>
						<div class="well well-sm white-background" style="height:150px">
						<%} else { %>
						<div class="well well-sm panel-coldroom light-gray-filter" style="height:150px">
						<%} %>
												<div class="text-center">
													<a href="TodayColdChainHealthVehicle.jsp?Type=MUDC">MU <i class="fa fa-long-arrow-right"></i> DC (# Of Vehicles: <b><%=iMUDCCount %></b>)</a>
													<div class="graph_container">
														<canvas id="Chart3" class="chart1" height="30" width="250"></canvas>
														<div class="row text-center">
															<div class="col-md-4"><span data-toggle="tooltip" class="badge badge-big bg-red"><%= Utils.zeroPad(iTodayRedMUDC, 2) %></span><br>
															<%if(iRedMUDC == 0) { %>
														<b><span class="description-percentage">NA</span></b>
														<%} else if(iRedMUDC > 0) { %>
														<b><span class="description-percentage text-red"><i class="fa fa-caret-up"></i>&nbsp;<%=Utils.zeroPad(iRedMUDC, 2) %>%</span></b>
														<%} else { %>
														<b><span class="description-percentage text-green"><i class="fa fa-caret-down"></i>&nbsp;<%=Utils.zeroPad(Math.abs(iRedMUDC), 2) %>%</span></b>
														<%} %>
														</div>
															<div class="col-md-4"><span data-toggle="tooltip" class="badge badge-big orange"><%= Utils.zeroPad(iTodayOrangeMUDC, 2) %></span><br>
																												<%if(iOrangeMUDC == 0) { %>
														<b><span class="description-percentage">NA</span></b>
														<%} else if(iOrangeMUDC > 0) { %>
														<b><span class="description-percentage text-red"><i class="fa fa-caret-up"></i>&nbsp;<%=Utils.zeroPad(iOrangeMUDC, 2) %>%</span></b>
														<%} else { %>
														<b><span class="description-percentage text-green"><i class="fa fa-caret-down"></i>&nbsp;<%=Utils.zeroPad(Math.abs(iOrangeMUDC), 2) %>%</span></b>
														<%} %>
														</div>
															<div class="col-md-4"><span data-toggle="tooltip" class="badge badge-big bg-green"><%= Utils.zeroPad(iTodayGreenMUDC, 2) %></span></div>
														</div>
													</div>
												</div>
											</div>
											</div>
							
										<div class="col-md-4">
											<%if(sType.equals(Constants.ROUTE_TYPE_DC_DC)) { %>
						<div class="well well-sm white-background" style="height:150px">
						<%} else { %>
						<div class="well well-sm panel-coldroom light-gray-filter" style="height:150px">
						<%} %>
												<div class="text-center">
													<a href="TodayColdChainHealthVehicle.jsp?Type=DCDC">DC <i class="fa fa-long-arrow-right"></i> DC (# Of Vehicles: <b><%=iDCDCCount %></b>)</a>
													<div class="graph_container">
														<canvas id="Chart4" class="chart1" height="30" width="250"></canvas>
														<div class="row text-center">
															<div class="col-md-4"><span data-toggle="tooltip" class="badge badge-big bg-red"><%= Utils.zeroPad(iTodayRedDCDC, 2) %></span><br>
															<%if(iRedDCDC == 0) { %>
														<b><span class="description-percentage">NA</span></b>
														<%} else if(iRedDCDC > 0) { %>
														<b><span class="description-percentage text-red"><i class="fa fa-caret-up"></i>&nbsp;<%=Utils.zeroPad(iRedDCDC, 2) %>%</span></b>
														<%} else { %>
														<b><span class="description-percentage text-green"><i class="fa fa-caret-down"></i>&nbsp;<%=Utils.zeroPad(Math.abs(iRedDCDC), 2) %>%</span></b>
														<%} %>
														</div>
															<div class="col-md-4"><span data-toggle="tooltip" class="badge badge-big orange"><%= Utils.zeroPad(iTodayOrangeDCDC, 2) %></span><br>
															<%if(iOrangeDCDC == 0) { %>
														<b><span class="description-percentage">NA</span></b>
														<%} else if(iOrangeDCDC > 0) { %>
														<b><span class="description-percentage text-red"><i class="fa fa-caret-up"></i>&nbsp;<%=Utils.zeroPad(iOrangeDCDC, 2) %>%</span></b>
														<%} else { %>
														<b><span class="description-percentage text-green"><i class="fa fa-caret-down"></i>&nbsp;<%=Utils.zeroPad(Math.abs(iOrangeDCDC), 2) %>%</span></b>
														<%} %>
														</div>
															<div class="col-md-4"><span data-toggle="tooltip" class="badge badge-big bg-green"><%= Utils.zeroPad(iTodayGreenDCDC, 2) %></span></div>
														</div>
													</div>
												</div>
											</div>
										</div>
									
										<div class="col-md-4">
											<%if(sType.equals(Constants.ROUTE_TYPE_DC_EP)) { %>
						<div class="well well-sm white-background" style="height:150px">
						<%} else { %>
						<div class="well well-sm panel-coldroom light-gray-filter" style="height:150px">
						<%} %>
												<div class="text-center">
													<a href="TodayColdChainHealthVehicle.jsp?Type=DCEP">DC <i class="fa fa-long-arrow-right"></i> End Point (# Of Vehicles: <b><%=iDCEPCount %></b>)</a>
													<div class="graph_container">
														<canvas id="Chart5" class="chart1" height="30" width="250"></canvas>
														<div class="row text-center">
															<div class="col-md-4"><span data-toggle="tooltip" class="badge badge-big bg-red"><%= Utils.zeroPad(iTodayRedDCEP, 2) %></span><br>
															<%if(iRedDCEP == 0) { %>
														<b><span class="description-percentage">NA</span></b>
														<%} else if(iRedDCEP > 0) { %>
														<b><span class="description-percentage text-red"><i class="fa fa-caret-up"></i>&nbsp;<%=Utils.zeroPad(iRedDCEP, 2) %>%</span></b>
														<%} else { %>
														<b><span class="description-percentage text-green"><i class="fa fa-caret-down"></i>&nbsp;<%=Utils.zeroPad(Math.abs(iRedDCEP), 2) %>%</span></b>
														<%} %>
														</div>
															<div class="col-md-4"><span data-toggle="tooltip" class="badge badge-big orange"><%= Utils.zeroPad(iTodayOrangeDCEP, 2) %></span><br>
															<%if(iOrangeDCEP == 0) { %>
														<b><span class="description-percentage">NA</span></b>
														<%} else if(iOrangeDCEP > 0) { %>
														<b><span class="description-percentage text-red"><i class="fa fa-caret-up"></i>&nbsp;<%=Utils.zeroPad(iOrangeDCEP, 2) %>%</span></b>
														<%} else { %>
														<b><span class="description-percentage text-green"><i class="fa fa-caret-down"></i>&nbsp;<%=Utils.zeroPad(Math.abs(iOrangeDCEP), 2) %>%</span></b>
														<%} %>
														</div>
															<div class="col-md-4"><span data-toggle="tooltip" class="badge badge-big bg-green"><%= Utils.zeroPad(iTodayGreenDCEP, 2) %></span></div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
				<div class="col-md-12 text-center">
				<div class="well well-sm white-background">
				<div class="text-center">
				<div class="panel-coldroom text-center">
					<b>TOTAL NUMBER OF ALERTS TODAY: <B class="font-big" style="color:black"><%= Utils.zeroPad((iTemperatureVehCount+iHumidityVehCount+iDoorOpenVehCount+iStoppageVehCount), 2) %></B></b>
					</div>
					</div><br>
					<div class="row">
						<div class="col-md-offset-1 col-md-10">
							
								<div class="row">
								<div class="col-md-3">
								TEMPERATURE
								<div class="row">
									<div class="col-md-offset-1 col-md-3">
									<img src="../../dist/img/temperature.png">
									</div>
									<div class="col-md-offset-1 col-md-7 text-left">
								<b class="font-new"><%=Utils.zeroPad(iTemperatureVehCount, 2) %></b>
									</div>
								</div>
								<b><span class="description-percentage text-red"><i class="fa fa-caret-up"></i>&nbsp;02%</span></b>
								</div>
								<div class="col-md-3">
								HUMIDITY
								<div class="row">
									<div class="col-md-offset-1 col-md-3">
									<img src="../../dist/img/humidity.png">
									</div>
									<div class="col-md-offset-1 col-md-7 text-left">
								<b class="font-new text-left"><%=Utils.zeroPad(iHumidityVehCount, 2) %></b>
									</div>
								</div>
								<b><span class="description-percentage text-green"><i class="fa fa-caret-down"></i>&nbsp;01%</span></b>
								</div>
							<div class="col-md-3">
								VEHICLE DOOR OPEN
								<div class="row">
									<div class="col-md-offset-1 col-md-3">
									<img src="../../dist/img/vehdooropen.png">
									</div>
									<div class="col-md-offset-1 col-md-7 text-left">
								<b class="font-new text-left"><%=Utils.zeroPad(iDoorOpenVehCount, 2) %></b>
									</div>
									
								</div>
								<b><span class="description-percentage text-green text-center"><i class="fa fa-caret-down"></i>&nbsp;02%</span></b>
								</div>
								<div class="col-md-3">
								VEHICLE STOPPAGE
								<div class="row">
									<div class="col-md-offset-1 col-md-3">
									<img src="../../dist/img/vehiclestop.png">
									</div>
									<div class="col-md-offset-1 col-md-6 text-left">
								<b class="font-new text-left"><%=Utils.zeroPad(iStoppageVehCount, 2) %></b>
									</div>
								
								</div>
								<b><span class="description-percentage text-red"><i class="fa fa-caret-up"></i>&nbsp;05%</span></b>
								</div>
								</div>
					</div>
					</div>
					</div>
							
						</div>
					</div>
									<div class="row">
									
										<div class="col-md-10">
											<div class="well well-sm white-background text-center">
											<%if(sType.equals(Constants.ROUTE_TYPE_MU_DC)){ %>
											<b>MU <i class="fa fa-long-arrow-right"></i> DC</b><br><br>
											<%} else if(sType.equals(Constants.ROUTE_TYPE_DC_DC)) { %>
											<b>DC <i class="fa fa-long-arrow-right"></i> DC</b><br><br>
											<%} else { %>
											<b>DC <i class="fa fa-long-arrow-right"></i> EP</b><br><br>
											<%} %>
				
							
							<br>
							
		
		<div id="listView">		
									<table id="data_table_stationary" class="table table-bordered table-striped table-condensed">
									
									<tr class="info text-center">
									<th width="150">Vehicle ID</th>
									<th width="100">Start Datetime</th>
									<th width="100">Source</th>
									<th width="120">Destination</th>
									<th width="100">Current Location</th>
									<th width="100">Trip Status</th>
									<th width="100"># Alerts</th>
									<th width="120">Shipment Risk</th>
									</tr>
									<%
									iCount = 0;
									 int j=0; 
									TransitDriverMapBean tdmBean = new TransitDriverMapBean();
									iter = col.iterator();
									while(iter.hasNext()) {
										tvmBean = (TransitVehicleMapBean) iter.next();
										if(!tvmBean.getRoute_Type().equals(sType)) continue;
										if((sVehicleIdParam.equals("ALL") || sVehicleIdParam.equals(tvmBean.getVehicle_ID())) &&
												(sVehicleTypeParam.equals("ALL") || sVehicleTypeParam.equals(tvmBean.getVehicle_Type())) &&
												(sSourceParam.equals("ALL") || sSourceParam.equals(tvmBean.getSource())) &&
												(sDestParam.equals("ALL") || sDestParam.equals(tvmBean.getDestination())) &&
												//(sCurrentLocationParam.equals("ALL") || sCurrentLocationParam.equals(tvmBean.getCurrentLocation())) &&
												(sDriverIDParam.equals("ALL") || sDriverIDParam.equals(tvmBean.getDriver_DL_ID()))) {
										if(tvmBean.getTrip_Status().equals("Completed")) {
									%>
										<tr id="row_stationary<%= j%>" class="blinking">
									<%} else { %>
										<tr id="row_stationary<%= iCount%>" class="active">
									<%} %>
									<td id="<%=iCount %>_Vehicle_Id"><%=tvmBean.getVehicle_ID() %><a href="LiveVehicleTracking.jsp?VehicleId=<%= tvmBean.getVehicle_ID()%>" style="color:red">Track Live</a></td>
									<td id="<%=iCount %>_Assigned_Date"><%=tvmBean.getAssigned_Date() %></td>
									<td id="<%=iCount %>_Source"><%=tvmBean.getSource() %></td>
									<td id="<%=iCount %>_Destination"><%=tvmBean.getDestination() %></td>
									<td id="<%=iCount %>_Current_Location"><div id="<%=iCount %>_CR" name="<%=iCount %>_CR"></div></td>
									<script type="text/javascript">
									
									$.get(
											"https://reverse.geocoder.cit.api.here.com/6.2/reversegeocode.json",
											{
												app_id: 'DdeJ8sqdrenYc1k7eTH6',
												app_code: 'DBU7IBF4dzhGgtLRCx6Sgg',
												gen:'9',
												prox:'<%=tvmBean.getLatitude()%>,<%=tvmBean.getLongitude()%>,100',
												mode:'retrieveAddresses'
											},
											function(data) {
												//console.log(data);
												$.each(data.Response, function(i, item){
													var a = (data.Response.View[0].Result[0].Location.Address.City);
													$('#<%=iCount%>_CR').html(a);
													
													
												});
											}
										);
									</script>
									<%if(iCount % 3 == 0){ %>
									<td><div class="progress">
  										<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
    										60%
 										 </div>
										</div></td>
									<%} else if(iCount % 2 == 0) { %>
									<td><div class="progress">
  										<div class="progress-bar" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%;">
    										40%
 										 </div>
										</div></td>
									<%} else { %>
									<td><div class="progress">
  										<div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%;">
    										80%
 										 </div>
										</div></td>
									<%} %>
									
									
									<%if(tmAlerts.containsKey(tvmBean.getVehicle_ID())) { %>
									<td><%=tmAlerts.get(tvmBean.getVehicle_ID()) %></td>
									<%} else { %>
									<td>0</td>
									<%} %>
									<%if(iCount % 3 == 0){ %>
									<td><span data-toggle="tooltip" class="badge bg-red">HIGH</span></td>
									<%} else if(iCount % 2 == 0){%>
									<td><span data-toggle="tooltip" class="badge bg-orange">MEDIUM</span></td>
									<%} else {%>
									<td><span data-toggle="tooltip" class="badge bg-green">LOW</span></td>
									<%} %>
									</tr>
									<%}
									iCount++;	
									} %>
									
									</table>
												</div>
												</div>
												</div>
										<div class="col-md-2 text-center">
											<div class="well well-sm white-background">
											<br>Filter By:<br><br>
											<div class="row">
												<div class="col-md-12">
													<select id="Source" name="Source" class="form-control-no-background pull-center">
  														<option value="ALL">Source</option>
  														<%for(int i=0; i<sSourceArray.length; i++){ 
  															if(sSourceParam.equals(sSourceArray[i])){
  														%>
  												<option value="<%=sSourceArray[i] %>" selected><%=sSourceArray[i] %></option>
  												<%} else { %>
  												<option value="<%=sSourceArray[i] %>"><%=sSourceArray[i] %></option>
  												<%} %>
  											<%}%>
													</select>
												</div>
												</div>
												<br>
												<div class="row">
												<div class="col-md-12">
													<select id="Destination" name="Destination" class="form-control-no-background pull-center">
  														<option value="ALL">Destination</option>
  														<%for(int i=0; i<sDestArray.length; i++){ 
  															if(sDestParam.equals(sDestArray[i])){
  														%>
  												<option value="<%=sDestArray[i] %>" selected><%=sDestArray[i] %></option>
  												<%} else { %>
  												<option value="<%=sDestArray[i] %>"><%=sDestArray[i] %></option>
  												<%} %>
  											<%}%>
													</select>
												</div>
												</div>
												<br>
												<!-- <div class="row">
												<div class="col-md-12">
													<select id="TripStatus" name="TripStatus" class="form-control-no-background pull-center">
  														<option>Vehicle Status</option>
  														<option>Completed</option>
  														<option>To Start</option>
  														<option>Running</option>
													</select> 
												</div>
												</div>
												<br> -->
												<div class="row">
												<div class="col-md-12">
													<select id="VehicleID" name="VehicleID" class="form-control-no-background pull-center">
											<option value="ALL">Vehicle ID</option>
											<%for(int i=0; i<sVehicleIdArray.length; i++){ 
												if(sVehicleIdParam.equals(sVehicleIdArray[i])){
												%>
  												<option value="<%=sVehicleIdArray[i] %>" selected><%=sVehicleIdArray[i] %></option>
  												<%} else { %>
  												<option value="<%=sVehicleIdArray[i] %>"><%=sVehicleIdArray[i] %></option>
  												<%} %>
  											<%}%>
											
														
										</select>
												</div>
												</div>
												<br>
												<div class="row">
												<div class="col-md-12">
													<select id="Alerts" name="Alerts" class="form-control-no-background pull-center">
  														<option>Alerts</option>
  														<option>>= 3 Alerts</option>
  														<option><3 Alerts</option>
  														<option>No Alerts</option>
													</select> 
												</div>
												</div>
												<br>
												<div class="row">
												<div class="col-md-12">
												<button type="button"
														class="btn btn-success">GO</button>&nbsp;&nbsp;
												<button type="button"
														class="btn btn-default">RESET</button>
													
												</div>
											</div>
											<br><br><br><br>
											</div>
											
											 
										</div>
									</div>
									
				</div>
				</div>
									
									
								</div>
							</div>
						
		</section>
		</div>
		<!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0
			</div>
			<strong>Copyright &copy; 2017 <a href="http://tagbox.in">TagBox</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Control Sidebar -->
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->
<!-- ChartJS 1.0.1 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>


	<!-- Bootstrap 3.3.6 -->
	<script src="../../bootstrap/js/bootstrap.min.js"></script>
	<!-- ChartJS 1.0.1 -->
	<!-- AdminLTE App -->
	<script src="../../dist/js/app.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="../../dist/js/demo.js"></script>
<script>
Chart.defaults.global.legend.display = false;

var barOptions_stacked = {
	    tooltips: {
	        enabled: false
	    },
	    hover :{
	        animationDuration:0
	    },
	    scales: {
	        xAxes: [{
	        	display:false,
	            ticks: {
	                beginAtZero:true,
	                fontFamily: "'Open Sans Bold', sans-serif",
	                fontSize:11
	            },
	            
	            stacked: true
	        }],
	        yAxes: [{
	        	display:false,
	            ticks: {
	                fontFamily: "'Open Sans Bold', sans-serif",
	                fontSize:11
	            },
	            stacked: true
	        }]
	    },
	    legend:{
	        display:false
	    },
	    pointLabelFontFamily : "Quadon Extra Bold",
	    scaleFontFamily : "Quadon Extra Bold"
	};
	
var ctx2 = document.getElementById("Chart3");
var myChart2 = new Chart(ctx2, {
    type: 'horizontalBar',
    data: {
        labels: [""],
        
        datasets: [{
        	label: ">3 Alerts",
            data: [<%=iTodayRedMUDC %>],
            backgroundColor: "rgba(212,19,20,1)"
        },{
        	label: "<3 Alerts",
            data: [<%=iTodayOrangeMUDC %>],
            backgroundColor: "rgba(255,169,34,1)"
        },{
        	label: "No Alerts",
            data: [<%=iTodayGreenMUDC %>],
            backgroundColor: "rgba(49,150,8,1)"
        }]
    },

    options: barOptions_stacked
});
var ctx3 = document.getElementById("Chart4");
var myChart3 = new Chart(ctx3, {
    type: 'horizontalBar',
    data: {
        labels: [""],
        
        datasets: [{
        	label: ">3 Alerts",
            data: [<%=iTodayRedDCDC %>],
            backgroundColor: "rgba(212,19,20,1)"
        },{
        	label: "<3 Alerts",
            data: [<%=iTodayOrangeDCDC %>],
            backgroundColor: "rgba(255,169,34,1)"
        },{
        	label: "No Alerts",
            data: [<%=iTodayGreenDCDC %>],
            backgroundColor: "rgba(49,150,8,1)"
        }]
    },

    options: barOptions_stacked
});
var ctx4 = document.getElementById("Chart5");
var myChart4 = new Chart(ctx4, {
    type: 'horizontalBar',
    data: {
        labels: [""],
        
        datasets: [{
        	label: ">3 Alerts",
            data: [<%=iTodayRedDCEP %>],
            backgroundColor: "rgba(212,19,20,1)"
        },{
        	label: "<3 Alerts",
            data: [<%=iTodayOrangeDCEP %>],
            backgroundColor: "rgba(255,169,34,1)"
        },{
        	label: "No Alerts",
            data: [<%=iTodayGreenDCEP %>],
            backgroundColor: "rgba(49,150,8,1)"
        }]
    },

    options: barOptions_stacked
});

</script>
	</body>
</html>
