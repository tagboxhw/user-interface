<%@ page import="com.stellar.dao.ClientLocationMapDao"%>
<%@ page import="com.stellar.dao.TransitDao"%>
<%@ page import="com.stellar.bean.StationaryNodeMapBean"%>
<%@ page import="com.stellar.bean.TransitNodeMapBean"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.util.Collection"%>
<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>

<!-- <?xml version="1.0" encoding="ISO-8859-1"?> -->

<%!
    static final Logger logger = LoggerFactory.getLogger("GetNodeData");
    String sErrorMessage = null;
	StationaryNodeMapBean stationaryNodeMapBean = new StationaryNodeMapBean();
	TransitNodeMapBean transitNodeMapBean = new TransitNodeMapBean();
	TransitDao transitDao = new TransitDao();
	ClientLocationMapDao clientDao = new ClientLocationMapDao();
	String buf = "";
	String sString = "";
%>
<%
	sErrorMessage = null;
	clientDao = new ClientLocationMapDao();
	transitDao = new TransitDao();
	stationaryNodeMapBean = new StationaryNodeMapBean();
	transitNodeMapBean = new TransitNodeMapBean();
	buf = "";
	sString = "";
	%>
<% 
	
	try {
		sString = request.getParameter("query");
		if(sString.equals("updateTransitNodeMap")){
			transitNodeMapBean.setVehicle_id(request.getParameter("Vehicle_Id"));
			transitNodeMapBean.setGW_client_id(request.getParameter("GW_client_Id"));
			transitNodeMapBean.setND_device_id(request.getParameter("ND_device_id_val"));
			transitNodeMapBean.setGW_ND_Pairing(request.getParameter("GW_nd_pairing_val"));
			transitNodeMapBean.setND_subzone_type(request.getParameter("ND_subzone_type_val"));
			transitNodeMapBean.setND_subzone_name(request.getParameter("ND_subzone_name_val"));
			transitNodeMapBean.setND_type(request.getParameter("ND_type_val"));
			transitNodeMapBean.setND_status(request.getParameter("ND_status_val"));
			transitNodeMapBean.setND_client_id(request.getParameter("ND_client_id_val"));
			
			buf="";
			//response.setContentType("text/xml");
			//buf += ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			int i = transitDao.updateTransitNodeMap(transitNodeMapBean);
			int iCount = 0;
			//logger.info("<rows total_count='100' pos='0'>");
			buf = "<rows>";
			buf += ("<row>");
			buf += ("<Return>" + i + "</Return>");
			buf += ("</row>");
			buf += "</rows>";
			buf = buf.trim();
			//logger.info(buf);
			out.clear();
			out.println(buf);
		} else if(sString.equals("insertTransitNodeMap")){
			transitNodeMapBean.setVehicle_id(request.getParameter("Vehicle_Id"));
			transitNodeMapBean.setGW_client_id(request.getParameter("GW_client_Id"));
			transitNodeMapBean.setND_device_id(request.getParameter("ND_device_id_val"));
			transitNodeMapBean.setGW_ND_Pairing(request.getParameter("GW_nd_pairing_val"));
			transitNodeMapBean.setND_subzone_type(request.getParameter("ND_subzone_type_val"));
			transitNodeMapBean.setND_subzone_name(request.getParameter("ND_subzone_name_val"));
			transitNodeMapBean.setND_type(request.getParameter("ND_type_val"));
			transitNodeMapBean.setND_status(request.getParameter("ND_status_val"));
			transitNodeMapBean.setND_client_id(request.getParameter("ND_client_id_val"));
			
			buf="";
			//response.setContentType("text/xml");
			//buf += ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			int i = transitDao.insertTransitNodeMap(transitNodeMapBean);
			int iCount = 0;
			//logger.info("<rows total_count='100' pos='0'>");
			buf = "<rows>";
			buf += ("<row>");
			buf += ("<Return>" + i + "</Return>");
			buf += ("</row>");
			buf += "</rows>";
			buf = buf.trim();
			//logger.info(buf);
			out.clear();
			out.println(buf);
		} else if(sString.equals("deleteTransitNodeMap")){
			buf="";
			//response.setContentType("text/xml");
			//buf += ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			int i = transitDao.deleteTransitNodeMap(request.getParameter("ND_client_id_val"));
			int iCount = 0;
			//logger.info("<rows total_count='100' pos='0'>");
			buf = "<rows>";
			buf += ("<row>");
			buf += ("<Return>" + i + "</Return>");
			buf += ("</row>");
			buf += "</rows>";
			buf = buf.trim();
			//logger.info(buf);
			out.clear();
			out.println(buf);
		} else if(sString.equals("updateStationaryNodeMap")){
			stationaryNodeMapBean.setGW_Zone_id(request.getParameter("Zone_Id"));
			stationaryNodeMapBean.setGW_client_id(request.getParameter("GW_client_Id"));
			stationaryNodeMapBean.setND_device_id(request.getParameter("ND_device_id_val"));
			stationaryNodeMapBean.setGW_ND_Pairing(request.getParameter("GW_nd_pairing_val"));
			stationaryNodeMapBean.setND_subzone_type(request.getParameter("ND_subzone_type_val"));
			stationaryNodeMapBean.setND_subzone_name(request.getParameter("ND_subzone_name_val"));
			stationaryNodeMapBean.setND_type(request.getParameter("ND_type_val"));
			stationaryNodeMapBean.setND_status(request.getParameter("ND_status_val"));
			stationaryNodeMapBean.setND_client_id(request.getParameter("ND_client_id_val"));
			
			buf="";
			//response.setContentType("text/xml");
			//buf += ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			int i = clientDao.updateStationaryNodeMap(stationaryNodeMapBean);
			int iCount = 0;
			//logger.info("<rows total_count='100' pos='0'>");
			buf = "<rows>";
			buf += ("<row>");
			buf += ("<Return>" + i + "</Return>");
			buf += ("</row>");
			buf += "</rows>";
			buf = buf.trim();
			//logger.info(buf);
			out.clear();
			out.println(buf);
		} else if(sString.equals("insertStationaryNodeMap")){
			stationaryNodeMapBean.setGW_Zone_id(request.getParameter("Zone_Id"));
			stationaryNodeMapBean.setGW_client_id(request.getParameter("GW_client_Id"));
			stationaryNodeMapBean.setND_device_id(request.getParameter("ND_device_id_val"));
			stationaryNodeMapBean.setGW_ND_Pairing(request.getParameter("GW_nd_pairing_val"));
			stationaryNodeMapBean.setND_subzone_type(request.getParameter("ND_subzone_type_val"));
			stationaryNodeMapBean.setND_subzone_name(request.getParameter("ND_subzone_name_val"));
			stationaryNodeMapBean.setND_type(request.getParameter("ND_type_val"));
			stationaryNodeMapBean.setND_status(request.getParameter("ND_status_val"));
			stationaryNodeMapBean.setND_client_id(request.getParameter("ND_client_id_val"));
			
			buf="";
			//response.setContentType("text/xml");
			//buf += ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			int i = clientDao.insertStationaryNodeMap(stationaryNodeMapBean);
			int iCount = 0;
			//logger.info("<rows total_count='100' pos='0'>");
			buf = "<rows>";
			buf += ("<row>");
			buf += ("<Return>" + i + "</Return>");
			buf += ("</row>");
			buf += "</rows>";
			buf = buf.trim();
			//logger.info(buf);
			out.clear();
			out.println(buf);
		} else if(sString.equals("deleteStationaryNodeMap")){
			buf="";
			//response.setContentType("text/xml");
			//buf += ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			int i = clientDao.deleteStationaryNodeMap(request.getParameter("ND_client_id_val"));
			int iCount = 0;
			//logger.info("<rows total_count='100' pos='0'>");
			buf = "<rows>";
			buf += ("<row>");
			buf += ("<Return>" + i + "</Return>");
			buf += ("</row>");
			buf += "</rows>";
			buf = buf.trim();
			//logger.info(buf);
			out.clear();
			out.println(buf);
		}
	} catch (Exception e) {
		logger.error("Exception: " + e.getLocalizedMessage());
		e.printStackTrace();
	}
			%>