<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ page import="com.stellar.dao.AlertsWorkflowDao"%>
<%@ page import="com.stellar.dao.HumidityDao"%>
<%@ page import="com.stellar.dao.TemperatureDao"%>
<%@ page import="com.stellar.dao.ClientLocationMapDao"%>
<%@ page import="com.stellar.dao.TransitDao"%>
<%@ page import="com.stellar.bean.LocationBean"%>
<%@ page import="com.stellar.dao.DoorActivityDao"%>
<%@ page import="com.stellar.bean.DoorActivityBean"%>
<%@ page import="com.stellar.bean.HumidityBean"%>
<%@ page import="com.stellar.bean.TemperatureBean"%>
<%@ page import="com.stellar.bean.SubLocationBean"%>
<%@ page import="com.stellar.util.Utils"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.util.TreeMap"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.util.Collection"%>
<%@ page import="java.util.LinkedHashMap"%>
<%@ page import="java.util.Set"%>
<%@ page import="java.util.Date"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Calendar"%>
<%@ page import="java.util.GregorianCalendar"%>
<%@ page import="com.stellar.bean.AlertWorkflowBean"%>
<%@ page import="com.stellar.bean.AllLocationDataBean"%>
<%@ page import="com.stellar.util.Constants"%>
<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>
<%
response.setHeader("Cache-Control","no-cache"); 
response.setHeader("Pragma","no-cache"); 
response.setDateHeader ("Expires", -1); 
%>
<%!
static final Logger logger = LoggerFactory.getLogger("TodayColdChainHealthDetails");
SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MMM HH:mm");
SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm");
String sNodeId = "";
%>
<%
String sUsername = (String) session.getAttribute("username");
if(sUsername == null || sUsername.equals("")) {
	session.setAttribute("callingPage", "TodayColdChainHealthDetails.jsp");
	response.sendRedirect("../../index.html");
}
sUsername = (String) session.getAttribute("username");
AlertsWorkflowDao alertsWorkflowDao = new AlertsWorkflowDao();
Date d = null;
Calendar gc = new GregorianCalendar();
Date d2 = null;
Long newTime = null;
sNodeId = request.getParameter("Node");
if(sNodeId == null || sNodeId.equals("")) sNodeId = "SZ-SY-DMO-000001";
java.util.Date dt = new java.util.Date();
java.text.SimpleDateFormat sdf = 
     new java.text.SimpleDateFormat("dd-MM-yyyy HH:mm");
sdf.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
String currentTime = sdf.format(dt);
String sAlertType = request.getParameter("CatID");
if(sAlertType == null) sAlertType = "ALL";
String sTimeValue = request.getParameter("alertsFilter");
if(sTimeValue == null || sTimeValue.equals("")){
	sTimeValue = "60";
}
String sValue = "";
ClientLocationMapDao clmDao = new ClientLocationMapDao();
String sLocationName = "", sZoneName = "", sZoneId = "", sSubZoneName = "", sZoneType = "";
if(clmDao.colAllLocations == null || clmDao.colAllLocations.size() == 0) clmDao.getAllLocationsData();
for(AllLocationDataBean alBean: clmDao.colAllLocations){
	if(alBean.getSubzone_Id().equals(sNodeId)) {
		sLocationName = alBean.getLocation_Name();
		sZoneName = alBean.getZone_Name();
		sZoneId = alBean.getZone_Id();
		sSubZoneName = alBean.getSubzone_Name();
		sZoneType = alBean.getSubzone_Type();
		break;
	}
}
System.out.println(sZoneType);
TreeMap<String, String> tmLocID = new TreeMap<String, String>();
TreeMap<String, String> tmZoneID = new TreeMap<String, String>();
TreeMap<String, String> tmSubZoneID = new TreeMap<String, String>();
for(AllLocationDataBean alBean: clmDao.colAllLocations){
	tmLocID.put(alBean.getLocation_Name(), "1");
	if(alBean.getLocation_Name().equals(sLocationName)) {
		if(!alBean.getZone_Id().equals("")) tmZoneID.put(alBean.getZone_Id(), alBean.getZone_Name());
		if(alBean.getZone_Id().equals(sZoneId)) {
			if(!alBean.getSubzone_Id().equals("")) tmSubZoneID.put(alBean.getSubzone_Id(), alBean.getSubzone_Name());
	}
}
}

%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Today Cold Chain Health Details</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Add these below two lines for autocomplete and the javascript function at the end  -->
<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="../../bootstrap/css/bootstrap.css">
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
   
<!-- Font Awesome -->
<script src="https://use.fontawesome.com/37ba1872d2.js"></script>
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../../dist/css/tagbox.css">
<script src="../../dist/js/tagbox.js"></script>
<script src="../../dist/js/location.js"></script>
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
<link href="https://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
  body {
	font-family:  Lato, sans-serif;
}
</style>
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
	<div class="wrapper">

		<header class="main-header">
			<!-- Logo -->
			<div>
				<a href="#" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
					<span class="logo-mini"><b>Demo</b></span> <!-- logo for regular state and mobile devices -->
					<span class="logo-lg"><b>Demo</b> </span>
				</a>
			</div>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">

				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
					role="button"> <span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
				</a>
				<form class="navbar-form pull-left" role="search">
					<div class="form-group">
						<b style="padding-left: 15px; font-size: 20px;" class="pull-right">Today's Cold Chain Health Details</b><br> <b style="padding-left: 15px"><%= currentTime %></b>
					</div>
				</form>

				<div class="navbar-custom-menu">
					<img src="../../dist/img/Logo.png" class="logo-class pull-right"
						alt="User Image">
				</div>
			</nav>

		</header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->


				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<br>
				<ul class="sidebar-menu">
					<li class="header">MAIN NAVIGATION</li>
					<li class="treeview active"><a href="#"> <i
							class="fa fa-dashboard"></i> <span>Dashboard</span>
					</a>
						<ul class="treeview-menu">
							<li class="">
							<%if(sUsername != null && sUsername.equals("demo1")) { %>
							<a href="CMCDashboard.jsp"><i
									class="fa fa-circle-o"></i> CMC</a>
									<%} else { %>
									<a href="CMCDashboard.jsp" role="button" class="btn btn-disabled disabled"><i
									class="fa fa-circle-o"></i> CMC</a>
									<%} %>
									</li>
							<!-- <li class=""><a href="NewDashboard.jsp"><i
									class="fa fa-circle-o"></i> Vehicle Dashboard</a></li> -->
							<li class="active"><a href="TodayColdChainHealth.jsp"><i
									class="fa fa-circle-o"></i> Cold Rooms Summary</a></li>
							<!--  <li><a href="VehiclePlannedRouteMap.jsp"><i class="fa fa-circle-o"></i> Vehicle Planned Route</a></li> -->
							<li class=""><a href="TodayColdChainHealthVehicle.jsp" role="button" class="btn btn-disabled disabled"><i
									class="fa fa-circle-o"></i> Vehicles Summary</a></li>
						</ul></li>
					<li class="treeview"><a href="#"> <i
							class="fa fa-files-o"></i> <span>Alerts</span> 
					</a>
						<ul class="treeview-menu">
							<li><a href="LiveVehicleTracking.jsp" role="button" class="btn btn-disabled disabled"><i
									class="fa fa-circle-o"></i> Live Vehicle Tracking</a></li>
						</ul></li>
					
					<li class="treeview"><a href="#"> <i
							class="fa fa-database"></i> <span>Admin</span>
					</a>
						<ul class="treeview-menu">
							<li class=""><a href="VehicleManagement.jsp" role="button" class="btn btn-disabled disabled"><i class="fa fa-circle-o"></i>
									Vehicle Management</a></li>
							<li><a href="VendorScorecard.jsp" role="button" class="btn btn-disabled disabled"><i class="fa fa-circle-o"></i>
									Vehicle Score Card</a></li>
						</ul></li>
						<li class="treeview"><a href="#"> <i
							class="fa fa-bar-chart"></i> <span>Reports</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="ParameterSummary.jsp"><i class="fa fa-circle-o"></i>
									Parameter Summary</a></li>
							<li><a href="AlertsSummary.jsp"><i class="fa fa-circle-o"></i>
									Alerts Summary</a></li>
							<li><a href="AlertsReport.jsp"><i class="fa fa-circle-o"></i>
									Alerts Report</a></li>		
							
						</ul></li>
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			
		<section id="mainSection" class="content">
			<div class="row">
				<div class="col-md-12">
				<b>
				<%if(sUsername != null && sUsername.equals("demo1")) { %>
				<a href="CMCDashboard.jsp">CMC</a> &nbsp; <i class="fa fa-caret-right"></i> &nbsp;
				<%} %>
				<a href="TodayColdChainHealth.jsp?LocationId=<%=sLocationName%>"><%= sLocationName %></a> &nbsp;<i class="fa fa-caret-right"></i>&nbsp; 
				<a href="TodayColdChainHealthSummary.jsp?ZoneId=<%=sZoneId%>"><%=sZoneName %></a> &nbsp;<i class="fa fa-caret-right"></i> &nbsp;
				<%= sSubZoneName %>
				</b><br> <br>
					<div class="row">
						
							
										<div class="col-md-8">
											<div class="well well-sm">
												<div class="text-center">
												<%if(sNodeId.equals("SZ-SY-DMO-000001")) { %>
													<img src="../../dist/img/node1.png">
												<%} else if(sNodeId.equals("SZ-SY-DMO-000002")) { %>
												<img src="../../dist/img/node2.png">
												<%} else if(sNodeId.equals("SZ-SY-DMO-000003")) { %>
												<img src="../../dist/img/node1.png">
												<%} else if(sNodeId.equals("SZ-SY-DMO-000004")) { %>
												<img src="../../dist/img/node2.png">
												<%} else if(sNodeId.equals("SZ-SY-DMO-000005")) { %>
												<img src="../../dist/img/node1.png">
												<%} else if(sNodeId.equals("SZ-SY-DMO-000006")) { %>
												<img src="../../dist/img/node2.png">
												<%} else if(sNodeId.equals("SZ-SY-DMO-000007")) { %>
												<img src="../../dist/img/node1.png">
												<%} else if(sNodeId.equals("SZ-SY-DMO-000008")) { %>
												<img src="../../dist/img/node2.png">
												<%} else {%>
													<img src="../../dist/img/node1.png">
												<%} %>
												</div>
											</div>
										</div>
									<div class="col-md-offset-2 col-md-2">
							<div class="text-left">
							<label>GO TO:</label>
				<div class="scroll-area2 white-background">
				<%Set set = tmLocID.keySet();
				Set set1 = tmZoneID.keySet();
				Set set2 = tmSubZoneID.keySet();
				Iterator iter = set.iterator();
				Iterator iter1 = set1.iterator();
				Iterator iter2 = set2.iterator();
				String sValue1 = "";
				String sValue2 = "";
				while(iter.hasNext()) {
					sValue = (String) iter.next();
					if(sValue.equals(sLocationName)){%>
					<a href="TodayColdChainHealth.jsp?LocationId=<%=sValue%>"><%= sValue %></a><br>
					<%
							while(iter1.hasNext()) {
								sValue1 = (String) iter1.next();
								if(sValue1.equals(sZoneId)) {%>
									&nbsp;&nbsp;<b><a href="TodayColdChainHealthSummary.jsp?ZoneId=<%=sValue1 %>"><%=tmZoneID.get(sValue1).toUpperCase() %></a></b><br>
									<%while(iter2.hasNext()) {
										sValue2 = (String) iter2.next();
										if(sValue2.equals(sNodeId)){%>
										&nbsp;&nbsp;&nbsp;&nbsp;<a href="TodayColdChainHealthDetails.jsp?Node=<%=sValue2%>"><b><%=tmSubZoneID.get(sValue2).toUpperCase() %></b></a><br>
										<%} else {%>
											&nbsp;&nbsp;&nbsp;&nbsp;<a href="TodayColdChainHealthDetails.jsp?Node=<%=sValue2%>"><%=tmSubZoneID.get(sValue2) %></a><br>
										<%}
									}
								} else { %>
									&nbsp;&nbsp;<a href="TodayColdChainHealthSummary.jsp?ZoneId=<%=sValue1 %>"><%=tmZoneID.get(sValue1) %></a><br>
								<%}
							}
					} else { %>
					<a href="TodayColdChainHealth.jsp?LocationId=<%=sValue%>"><%= sValue %></a><br>
					<%}
					
				}%>
				</div>
								
												</div>
											</div>
									</div>
									<br><br>
									<div class="row">
									
				<div class="col-md-7 text-center">
				<div class="well well-sm">
				<b>TRENDS</b><br><br>
				<i><%= sSubZoneName %></i>
					<div class="row">
						
							<%
							String temperature = "", temperature1 = "", temperature2 = "", temperature3 = "", temperature5 = "";
							String time = "", time1 = "", time2 = "", time3 = "", time5 = "";	
							HumidityDao hd = new HumidityDao();
							HumidityBean hdBean = new HumidityBean();
							DoorActivityDao daDao = new DoorActivityDao();
							DoorActivityBean daBean = new DoorActivityBean();
							TemperatureDao td = new TemperatureDao();
							TemperatureBean tdBean = new TemperatureBean();
							Collection col = null;
							int iColSize = 0;
							int iSize = 0;
							if(sZoneType.equals(Constants.AMBIENT_NODE)) {
							col = td.getTemperatureData(sNodeId);
							Iterator iterSt = col.iterator();
							iColSize = col.size()/10;
							iSize = 0;
							while(iterSt.hasNext()) {
								tdBean = (TemperatureBean) iterSt.next();
								
								if(iColSize != 0 && iSize%iColSize == 0){
									d = df.parse(tdBean.getTimestamp());
									newTime = d.getTime();
									newTime +=(330*60*1000);
									d2 = new Date(newTime);
									temperature += "\"" + tdBean.getTemperature() + "\", ";
									time += "\"" + sdf1.format(d2) + "\", ";
								} else {
									if(col.size() != 0 && col.size() <= 10){
										d = df.parse(tdBean.getTimestamp());
										newTime = d.getTime();
										newTime +=(330*60*1000);
										d2 = new Date(newTime);
										temperature += "\"" + tdBean.getTemperature() + "\", ";
										time += "\"" + sdf1.format(d2) + "\", ";
									} else{
										temperature += "\"" + tdBean.getTemperature() + "\", ";
										time += "\"\", ";
									}
								}
								iSize++;
							}
							if(temperature.equals("") || time.equals("")){
								temperature1 = "00";
								time1 = "00";
							} else {
								if(temperature.length() >= 2)	temperature1 = temperature.substring(0, (temperature.length() - 2));
									if(time.length() >= 2) time1 = time.substring(0, (time.length() - 2));
							}
								//logger.info("temperature1: " + temperature1);
								//logger.info("time1: " + time1);
								temperature = ""; time = "";
								//Enter GW_ClientId and ND_Client Id for Chart 2
								col = hd.getHumidityData(sNodeId);
								Iterator iterSh = col.iterator();
								iColSize = col.size()/10;
								iSize = 0;
								while(iterSh.hasNext()) {
									hdBean = (HumidityBean) iterSh.next();
									if(iColSize != 0 && iSize%iColSize == 0){
										d = df.parse(hdBean.getTimestamp());
										newTime = d.getTime();
										newTime +=(330*60*1000);
										d2 = new Date(newTime);
										temperature += "\"" + hdBean.getHumidity() + "\", ";
										time += "\"" + sdf1.format(d2) + "\", ";
									} else {
										 if(col.size() != 0 && col.size() <= 10){
											d = df.parse(hdBean.getTimestamp());
											newTime = d.getTime();
											newTime +=(330*60*1000);
											d2 = new Date(newTime);
											temperature += "\"" + hdBean.getHumidity() + "\", ";
											time += "\"" + sdf1.format(d2) + "\", ";
										} else{
											temperature += "\"" + hdBean.getHumidity() + "\", ";
											time += "\"\", ";
										}
									}
									iSize++;
								}
								if(temperature.equals("") || time.equals("")){
									temperature2 = "00";
									time2 = "00";
								} else {
									if(temperature.length() >= 2)	temperature2 = temperature.substring(0, (temperature.length() - 2));
										if(time.length() >= 2) time2 = time.substring(0, (time.length() - 2));
								}
							} else if(sZoneType.equals(Constants.DOOR_NODE)) {
								temperature = ""; time = "";
								//Enter GW_ClientId and ND_Client Id for Chart 3
							 	col = daDao.getDoorActivityData(sNodeId);
								Iterator<DoorActivityBean> iterDa = col.iterator();
								iColSize = col.size()/10;
								iSize = 0;
								while(iterDa.hasNext()) {
									daBean = (DoorActivityBean) iterDa.next();
									if(iColSize != 0 && iSize%iColSize == 0){
										d = df.parse(daBean.getTimestamp());
										newTime = d.getTime();
										newTime +=(330*60*1000);
										d2 = new Date(newTime);
										temperature += "\"" + daBean.getDoorActivity() + "\", ";
										time += "\"" + sdf1.format(d2) + "\", ";
									}
									 else {
										 if(col.size() != 0 && col.size() <= 10){
												d = df.parse(daBean.getTimestamp());
												newTime = d.getTime();
												newTime +=(330*60*1000);
												d2 = new Date(newTime);
												temperature += "\"" + daBean.getDoorActivity() + "\", ";
												time += "\"" + sdf1.format(d2) + "\", ";
											} else{
												temperature += "\"" + daBean.getDoorActivity() + "\", ";
												time += "\"\", ";
											}
										}
									iSize++;
								};
								
								if(temperature.equals("") || time.equals("")){
									temperature3 = "00";
									time3 = "00";
								} else {
									 if(temperature.length() >= 2)	temperature3 = temperature.substring(0, (temperature.length() - 2));
									   if(time.length() >= 2) time3 = time.substring(0, (time.length() - 2));
								}
							}%>
								
								<div class="col-md-6">
													<br><br>
													<%if(sZoneType.equals(Constants.AMBIENT_NODE)) {%>
								<div class="box box-danger">
								<%} else { %>
								<div class="box box-danger" style="display:none;">
								<%} %>
							<div class="box-header with-border">
														<h3 class="box-title">&nbsp;Temperature (&deg; C)</h3>
														<div class="box-tools pull-right">
															<button type="button" class="btn btn-box-tool"
																data-widget="collapse">
																<i class="fa fa-minus"></i>
															</button>
															<button type="button" class="btn btn-box-tool"
																data-widget="remove">
																<i class="fa fa-times"></i>
															</button>
														</div>
													</div>
													<div class="box-body">
														<%
															if(temperature1.equals("00")){
														%>
														No Data!
														<%
															} else {
														%>
														<div class="chart">
															<canvas id="lineChart1"
																style="height: 270px; width: 100px"></canvas>
														</div>
														<%
															}
														%>

													</div>
												</div>
												<%
												System.out.println(sZoneType + ";" + Constants.DOOR_NODE);
												if(sZoneType.equals(Constants.DOOR_NODE)) {%>
								<div class="box box-danger">
								<%} else { %>
								<div class="box box-danger" style="display:none;">
								<%} %>
							<div class="box-header with-border">
														<h3 class="box-title">&nbsp;Door Activity</h3>
														<div class="box-tools pull-right">
															<button type="button" class="btn btn-box-tool"
																data-widget="collapse">
																<i class="fa fa-minus"></i>
															</button>
															<button type="button" class="btn btn-box-tool"
																data-widget="remove">
																<i class="fa fa-times"></i>
															</button>
														</div>
													</div>
													<div class="box-body">
														<%
															if(temperature3.equals("00")){
														%>
														No Data!
														<%
															} else {
														%>
														<div class="chart">
															<canvas id="lineChart3"
																style="height: 270px; width: 100px"></canvas>
														</div>
														<%
															}
														%>

													</div>
												</div>
						
												
							
						</div>
						<div class="col-md-6">
						 <br><br>
						 <%if(sZoneType.equals(Constants.AMBIENT_NODE)) {%>
								<div class="box box-danger">
								<%} else { %>
								<div class="box box-danger" style="display:none;">
								<%} %>
							<div class="box-header with-border">
														<h3 class="box-title">&nbsp;Relative Humidity (%)</h3>
														<div class="box-tools pull-right">
															<button type="button" class="btn btn-box-tool"
																data-widget="collapse">
																<i class="fa fa-minus"></i>
															</button>
															<button type="button" class="btn btn-box-tool"
																data-widget="remove">
																<i class="fa fa-times"></i>
															</button>
														</div>
													</div>
													<div class="box-body">
														<%
															if(temperature2.equals("00")){
														%>
														No Data!
														<%
															} else {
														%>
														<div class="chart">
															<canvas id="lineChart2"
																style="height: 270px; width: 100px"></canvas>
														</div>
														<%
															}
														%>

													</div>
												</div>
							
							
						</div>
						
																		</div>
					</div>
					</div>
					<div class="col-md-5 text-center">
				<div class="well well-sm">
				<b>ALERTS</b><br><br>
				
					<i><%= sSubZoneName %></i>
				
				<br>
				
													<br><br> 
													<div class="row">
									<div class="col-md-12">
										<div class="panel-group" id="accordion" role="tablist"
											aria-multiselectable="true">
											<%
											LinkedHashMap lhm = alertsWorkflowDao.getAllAlertsForSubzoneId(sNodeId, sAlertType, sTimeValue);
											if(lhm.size() == 0){%>
												No Alerts!
											<%}
												iter = lhm.keySet().iterator();
												AlertWorkflowBean ab = new AlertWorkflowBean();
												String sKey = "";
												while(iter.hasNext()){
													sKey = (String) iter.next();
													ab = (AlertWorkflowBean) lhm.get(sKey);
													if(ab.getAlert_Type().equals(Constants.TEMPERATURE)){
											%>
											<div class="panel col-no-padding3">
												<div class="panel-heading" role="tab"
													id="<%=ab.getAlert_ID()%>">
													<h4 class="panel-title">
														<div class="row" class="collapsed" role="button"
															data-toggle="collapse" data-parent="#accordion"
															href="#header<%=ab.getAlert_ID()%>" aria-expanded="false"
															aria-controls="header<%=ab.getAlert_ID()%>">
															<%
																if(ab.getAlert_Severity().equals("4") || ab.getAlert_Severity().equals("3")){
															%>
															<div class="col-md-1 alert-text text-center">
																<i class="fa fa-thermometer-three-quarters"></i>
															</div>
															<div class="col-md-1 alert-text"><span data-toggle="tooltip" data-placement="top" title="Alert Id"><%=ab.getAlert_ID()%></span></div>
															<div class="col-md-4 alert-text">
																Temperature in
																<%=ab.getCurrent_Location_Name()%>
																has breached
																<%=ab.getBreach_Type()%>
																limit for over
																<%=ab.getAlert_Duration()%>
																minutes
															</div>
															<div class="col-md-2 alert-text"><span data-toggle="tooltip" data-placement="top" title="Assigned To"><%= ab.getAssigned_To() %></span></div>
															<%
																} else {
															%>
															<div class="col-md-1 text-center">
																<i class="fa fa-thermometer-three-quarters"></i>
															</div>
															<div class="col-md-1"><span data-toggle="tooltip" data-placement="top" title="Alert Id"><%=ab.getAlert_ID()%></span></div>
															<div class="col-md-4">
																Temperature in
																<%=ab.getCurrent_Location_Name()%>
																has breached
																<%=ab.getBreach_Type()%>
																limit for over
																<%=ab.getAlert_Duration()%>
																minutes
															</div>
															<div class="col-md-2"><span data-toggle="tooltip" data-placement="top" title="Assigned To"><%= ab.getAssigned_To() %></span></div>
															<%
																}
															%>
															<div class="col-md-2 text-right">
															<%d = df.parse(ab.getAlert_Timestamp_From()); 
															%>
															<span data-toggle="tooltip" data-placement="top" title="Alert Start Time"><%=sdf2.format(d) %></span>
															</div>
															<div class="col-md-1">
																<input id="btn<%=ab.getAlert_ID()%>" type="button"
																	class="btn btn-default btn-sm"
																	value="<%=ab.getAction_Status()%>">
															</div>
															
														</div>
													</h4>
												</div>
											</div>
											<%
												} else if(ab.getAlert_Type().equals(Constants.HUMIDITY)) {
											%>
											<div class="panel col-no-padding3">
												<div class="panel-heading" role="tab"
													id="<%=ab.getAlert_ID()%>">
													<h4 class="panel-title">
														<div class="row" class="collapsed" role="button"
															data-toggle="collapse" data-parent="#accordion"
															href="#header<%=ab.getAlert_ID()%>" aria-expanded="false"
															aria-controls="header<%=ab.getAlert_ID()%>">
															<%
																if(ab.getAlert_Severity().equals("4") || ab.getAlert_Severity().equals("3")){
															%>
															<div class="col-md-1 alert-text text-center">
																<span class="glyphicon glyphicon glyphicon-tint"></span>
															</div>
															<div class="col-md-1 alert-text"><span data-toggle="tooltip" data-placement="top" title="Alert Id"><%=ab.getAlert_ID()%></span></div>
															<div class="col-md-4 alert-text">
																Humidity in
																<%=ab.getCurrent_Location_Name()%>
																has breached
																<%=ab.getBreach_Type()%>
																limit for over
																<%=ab.getAlert_Duration()%>
																minutes
															</div>
															<div class="col-md-2 alert-text"><span data-toggle="tooltip" data-placement="top" title="Assigned To"><%= ab.getAssigned_To() %></span></div>
															<%
																} else {
															%>
															<div class="col-md-1 text-center">
																<span class="glyphicon glyphicon glyphicon-tint"></span>
															</div>
															<div class="col-md-1"><span data-toggle="tooltip" data-placement="top" title="Alert Id"><%=ab.getAlert_ID()%></span></div>
															<div class="col-md-4">
																Humidity in
																<%=ab.getCurrent_Location_Name()%>
																has breached
																<%=ab.getBreach_Type()%>
																limit for over
																<%=ab.getAlert_Duration()%>
																minutes
															</div>
															<div class="col-md-2"><span data-toggle="tooltip" data-placement="top" title="Assigned To"><%= ab.getAssigned_To() %></span></div>
															<%
																}
															%>
															<div class="col-md-2 text-right">
															<%d = df.parse(ab.getAlert_Timestamp_From()); 
															%>
															<span data-toggle="tooltip" data-placement="top" title="Alert Start Time"><%=sdf2.format(d) %></span>
															</div>
															<div class="col-md-1">
																<input id="btn<%=ab.getAlert_ID()%>" type="button"
																	class="btn btn-default btn-sm"
																	value="<%=ab.getAction_Status()%>">
															</div>
														</div>
													</h4>
												</div>
											</div>
											<%
												} else if(ab.getAlert_Type().equals(Constants.DOOR_ACTIVITY)) {
											%>
											<div class="panel col-no-padding3">
												<div class="panel-heading" role="tab"
													id="<%=ab.getAlert_ID()%>">
													<h4 class="panel-title">
														<div class="row" class="collapsed" role="button"
															data-toggle="collapse" data-parent="#accordion"
															href="#header<%=ab.getAlert_ID()%>" aria-expanded="false"
															aria-controls="header<%=ab.getAlert_ID()%>">
															<%
																if(ab.getAlert_Severity().equals("4") || ab.getAlert_Severity().equals("3")){
															%>
															<div class="col-md-1 alert-text text-center">
																<i class="fa fa-columns"></i>
															</div>
															<div class="col-md-1 alert-text"><span data-toggle="tooltip" data-placement="top" title="Alert Id"><%=ab.getAlert_ID()%></span></div>
															<div class="col-md-4 alert-text">
																Door of
																<%=ab.getCurrent_Location_Name()%>
																has been opened for over
																<%=ab.getAlert_Duration()%>
																minutes
															</div>
															<div class="col-md-2 alert-text"><span data-toggle="tooltip" data-placement="top" title="Assigned To"><%= ab.getAssigned_To() %></span></div>
															<%
																} else {
															%>
															<div class="col-md-1 text-center">
																<i class="fa fa-columns"></i>
															</div>
															<div class="col-md-1"><span data-toggle="tooltip" data-placement="top" title="Alert Id"><%=ab.getAlert_ID()%></span></div>
															<div class="col-md-4">
																Door of
																<%=ab.getCurrent_Location_Name()%>
																has been opened for over
																<%=ab.getAlert_Duration()%>
																minutes
															</div>
															<div class="col-md-2"><span data-toggle="tooltip" data-placement="top" title="Assigned To"><%= ab.getAssigned_To() %></span></div>
															<%
																}
															%>
															<div class="col-md-2 text-right">
															<%d = df.parse(ab.getAlert_Timestamp_From()); 
															%>
															<span data-toggle="tooltip" data-placement="top" title="Alert Start Time"><%=sdf2.format(d) %></span>
															</div>
															<div class="col-md-1">
																<input id="btn<%=ab.getAlert_ID()%>" type="button"
																	class="btn btn-default btn-sm"
																	value="<%=ab.getAction_Status()%>">
															</div>
														</div>
													</h4>
												</div>
											</div>
											<%
												}
											%>
											<br>
											<%
												if(ab.getAlert_Type().equals(Constants.TEMPERATURE)) {
											%>
											<div id="header<%=ab.getAlert_ID()%>"
												class="panel-collapse collapse" role="tabpanel"
												aria-labelledby="<%=ab.getAlert_ID()%>">
												<div class="panel-body">
													<div class="well">
														<div class="container">
															<div class="row">
																<div class="col-md-4">
																<div class="row">
																		<div class="col-md-6">
																	<div class="form-group">
																		<label>Average Event Temperature</label><br> <input
																			class="btn btn-default"
																			class="form-control-no-height" type="button"
																			value="<%=Utils.roundIt(ab.getAlert_Parameter_Avg()) %>&deg;C">
																	</div>
																		</div>
																		<div class="col-md-6">
																		<div class="form-group">

																		<label>Event Duration</label><br><br> <input
																			class="btn btn-default" type="button"
																			value="<%=ab.getAlert_Duration()%> minutes">
																	</div>
																		</div>
																	</div>
																	<div class="row">
																		<div class="col-md-6">
																	<div class="form-group">
																		<label>Assigned To</label><br> <input type="text" id="assigned_to_<%=ab.getAlert_ID()%>"
																			class="form-control-no-height" value="<%= ab.getAssigned_To() %>">
																	</div>
																	</div>
																	<div class="col-md-6">
																	<div class="form-group">
																		<label>Supervisor</label><br> <input type="text"
																			class="form-control-no-height" value="Saumitra Singh">
																	</div>
																	</div>
																	</div>
																	
																	<input class="btn btn-success" type="button"
																		value="In Progress"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'In Progress')">
																	<input class="btn btn-success" type="button"
																		value="On Hold"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'On Hold')">
																	<input class="btn btn-success" type="button"
																		value="Close"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'Closed')">
																	<br> <br>
																	
																	<input class="btn btn-success" type="button"
																		value="Escalate"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'Escalated')">
																	<input class="btn btn-success" type="button"
																		value="Reassign"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'Reassigned')">
																	<input class="btn btn-success" type="button"
																		value="Reopen"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'Reopened')">
																</div>
																</div>
																<div class="row">
																<div class="col-md-4">
																	<br>
																	<div class="row">
																		<div class="col-md-6">
																		<div class="form-group">
																		<label>Alert Root Cause</label><br>
																		<textarea class="form-control" rows="3"
																			id="txtRoot<%=ab.getAlert_ID()%>"><%=ab.getRoot_Cause()%></textarea>
																	</div>
																		</div>
																		<div class="col-md-6">
																		<div class="form-group">
																		<label>Preventive Actions</label><br>
																		<textarea class="form-control" rows="3"
																			id="txtPre<%=ab.getAlert_ID()%>"><%=ab.getPreventive_Actions()%></textarea>
																	</div>
																		</div>
																	</div>
																	<div class="form-group text-center">
																		<input class="btn btn-success" type="button"
																			value="Save"
																			onclick="updateRoot('<%=ab.getAlert_ID()%>')"
																			data-toggle="modal" data-target="#myModal">
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<%
												}  else if(ab.getAlert_Type().equals(Constants.HUMIDITY)) {
											%>
											<div id="header<%=ab.getAlert_ID()%>"
												class="panel-collapse collapse" role="tabpanel"
												aria-labelledby="<%=ab.getAlert_ID()%>">
												<div class="panel-body">
													<div class="well">
														<div class="container">
															<div class="row">
																<div class="col-md-4">
																	<div class="row">
																		<div class="col-md-6">
																	<div class="form-group">
																		<label>Average Humidity</label><br> <input
																			class="btn btn-default"
																			class="form-control-no-height" type="button"
																			value="<%= Utils.roundIt(ab.getAlert_Parameter_Avg()) %>%">
																	</div>	
																		</div>
																		<div class="col-md-6">
																		<div class="form-group">

																		<label>Event Duration</label><br> <input
																			class="btn btn-default" type="button"
																			value="<%=ab.getAlert_Duration()%> minutes">
																	</div>
																		</div>
																	</div>
																	<div class="row">
																		<div class="col-md-6">
																	<div class="form-group">
																		<label>Assigned To</label><br> <input type="text" id="assigned_to_<%=ab.getAlert_ID()%>"
																			class="form-control-no-height" value="<%= ab.getAssigned_To() %>">
																	</div>
																	</div>
																	<div class="col-md-6">
																	<div class="form-group">
																		<label>Supervisor</label><br> <input type="text"
																			class="form-control-no-height" value="Saumitra Singh">
																	</div>
																	</div>
																	</div>
																	<input class="btn btn-success" type="button"
																		value="In Progress"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'In Progress')">
																	<input class="btn btn-success" type="button"
																		value="On Hold"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'On Hold')">
																	<input class="btn btn-success" type="button"
																		value="Close"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'Closed')">
																	<br> <br>
																	
																	<input class="btn btn-success" type="button"
																		value="Escalate"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'Escalated')">
																	<input class="btn btn-success" type="button"
																		value="Reassign"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'Reassigned')">
																	<input class="btn btn-success" type="button"
																		value="Reopen"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'Reopened')">
																</div>
																</div>
																<div class="row">
																<div class="col-md-4">
																<br>
																	<div class="row">
																		<div class="col-md-6">
																		<div class="form-group">
																		<label>Alert Root Cause</label><br>
																		<textarea class="form-control" rows="3"
																			id="txtRoot<%=ab.getAlert_ID()%>"><%=ab.getRoot_Cause()%></textarea>
																	</div>
																		</div>
																		<div class="col-md-6">
																		<div class="form-group">
																		<label>Preventive Actions</label><br>
																		<textarea class="form-control" rows="3"
																			id="txtPre<%=ab.getAlert_ID()%>"><%=ab.getPreventive_Actions()%></textarea>
																	</div>
																		</div>
																	</div>
																	
																	
																	<div class="form-group text-center">
																		<input class="btn btn-success" type="button"
																			value="Save"
																			onclick="updateRoot('<%=ab.getAlert_ID()%>')"
																			data-toggle="modal" data-target="#myModal">
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<%
												} else if(ab.getAlert_Type().equals(Constants.DOOR_ACTIVITY)) {
											%>
											<div id="header<%=ab.getAlert_ID()%>"
												class="panel-collapse collapse" role="tabpanel"
												aria-labelledby="<%=ab.getAlert_ID()%>">
												<div class="panel-body">
													<div class="well">
														<div class="container">
															<div class="row">
																<div class="col-md-4">
																<div class="row">
																		<div class="col-md-12">
																	<div class="form-group">
																		<label>Door Open Duration</label><br> <input
																			class="btn btn-default" type="button"
																			value="<%=ab.getAlert_Duration()%> minutes">
																	</div>
																		</div>
																		
																	</div>
																	<div class="row">
																		<div class="col-md-6">
																	<div class="form-group">
																		<label>Assigned To</label><br> <input type="text" id="assigned_to_<%=ab.getAlert_ID()%>"
																			class="form-control-no-height" value="<%= ab.getAssigned_To() %>">
																	</div>
																	</div>
																	<div class="col-md-6">
																	<div class="form-group">
																		<label>Supervisor</label><br> <input type="text"
																			class="form-control-no-height" value="Saumitra Singh">
																	</div>
																	</div>
																	</div>
																	
																	<input class="btn btn-success" type="button"
																		value="In Progress"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'In Progress')">
																	<input class="btn btn-success" type="button"
																		value="On Hold"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'On Hold')">
																	<input class="btn btn-success" type="button"
																		value="Close"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'Closed')">
																	<br> <br>
																	
																	<input class="btn btn-success" type="button"
																		value="Escalate"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'Escalated')">
																	<input class="btn btn-success" type="button"
																		value="Reassign"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'Reassigned')">
																	<input class="btn btn-success" type="button"
																		value="Reopen"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'Reopened')">
																</div>
																</div>
																<div class="row">
																<div class="col-md-4">
																	<div class="row">
																		<div class="col-md-6">
																		<div class="form-group">
																		<label>Alert Root Cause</label><br>
																		<textarea class="form-control" rows="3"
																			id="txtRoot<%=ab.getAlert_ID()%>"><%=ab.getRoot_Cause()%></textarea>
																	</div>
																		</div>
																		<div class="col-md-6">
																		<div class="form-group">
																		<label>Preventive Actions</label><br>
																		<textarea class="form-control" rows="3"
																			id="txtPre<%=ab.getAlert_ID()%>"><%=ab.getPreventive_Actions()%></textarea>
																	</div>
																		</div>
																	</div>
																	
																	
																	<div class="form-group text-center">
																		<input class="btn btn-success" type="button"
																			value="Save"
																			onclick="updateRoot('<%=ab.getAlert_ID()%>')"
																			data-toggle="modal" data-target="#myModal">
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<%
												}
											}
											%>
										</div>
									</div>
								</div>
					
					</div>
					</div>
				</div>
									
									
								</div>
							</div>
						
		</section>
		

<div class="modal fade" tabindex="-1" role="dialog" id="myModal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header alert-info">Updated Data!</div>
					<div class="modal-body">Updated Alert Root Cause and
						Preventive Actions Successfully!</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>

				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		</div>
		<!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0
			</div>
			<strong>Copyright &copy; 2017 <a href="http://tagbox.in">TagBox</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Control Sidebar -->
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
<script src="../../plugins/chartjs/Chart.min.js"></script>
	

	<!-- Bootstrap 3.3.6 -->
	<script src="../../bootstrap/js/bootstrap.min.js"></script>
	<!-- ChartJS 1.0.1 -->
	<!-- AdminLTE App -->
	<script src="../../dist/js/app.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="../../dist/js/demo.js"></script>
<script>
var areaChartData1 = {
	labels : [
<%=time1%>
],
	datasets : [ {
		label : "Temperature",
		fillColor : "rgba(210, 214, 222, 1)",
		strokeColor : "rgba(210, 214, 222, 1)",
		pointColor : "rgba(210, 214, 222, 1)",
		pointStrokeColor : "#c1c7d1",
		pointHighlightFill : "#fff",
		pointHighlightStroke : "rgba(220,220,220,1)",
		data : [
<%=temperature1%>
]
	} ]
};
var areaChartData2 = {
	labels : [
<%=time2%>
],
	datasets : [ {
		label : "Humidity",
		fillColor : "rgba(210, 214, 222, 1)",
		strokeColor : "rgba(210, 214, 222, 1)",
		pointColor : "rgba(210, 214, 222, 1)",
		pointStrokeColor : "#c1c7d1",
		pointHighlightFill : "#fff",
		pointHighlightStroke : "rgba(220,220,220,1)",
		data : [
<%=temperature2%>
]
	} ]
};

var areaChartData3 = {
		labels : [
	<%=time3%>
	],
		datasets : [ {
			label : "Door Activity",
			fillColor : "rgba(210, 214, 222, 1)",
			strokeColor : "rgba(210, 214, 222, 1)",
			pointColor : "rgba(210, 214, 222, 1)",
			pointStrokeColor : "#c1c7d1",
			pointHighlightFill : "#fff",
			pointHighlightStroke : "rgba(220,220,220,1)",
			data : [
	<%=temperature3%>
	]
		} ]
	};
	
	
var areaChartOptions = {
		scales : {
			xAxes : [ {
				ticks : {
					maxRotation : 90,
					minRotation : 90
				}
			} ]
		},

		//Boolean - If we should show the scale at all
		showScale : true,
		//Boolean - Whether grid lines are shown across the chart
		scaleShowGridLines : false,
		//String - Colour of the grid lines
		scaleGridLineColor : "rgba(0,0,0,.05)",
		//Number - Width of the grid lines
		scaleGridLineWidth : 1,
		//Boolean - Whether to show horizontal lines (except X axis)
		scaleShowHorizontalLines : true,
		//Boolean - Whether to show vertical lines (except Y axis)
		scaleShowVerticalLines : true,
		//Boolean - Whether the line is curved between points
		bezierCurve : true,
		//Number - Tension of the bezier curve between points
		bezierCurveTension : 0.3,
		//Boolean - Whether to show a dot for each point
		pointDot : false,
		//Number - Radius of each point dot in pixels
		pointDotRadius : 1,
		//Number - Pixel width of point dot stroke
		pointDotStrokeWidth : 1,
		//Number - amount extra to add to the radius to cater for hit detection outside the drawn point !!!important to display only one point
		pointHitDetectionRadius : 0,
		//Boolean - Whether to show a stroke for datasets
		datasetStroke : true,
		//Number - Pixel width of dataset stroke - !!!!!the thickness of the lines on the chart
		datasetStrokeWidth : 2,
		//Boolean - Whether to fill the dataset with a color
		datasetFill : true,
		//String - A legend template
		//Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
		maintainAspectRatio : true,
		//Boolean - whether to make the chart responsive to window resizing
		responsive : true

	};

	//-------------
	//- LINE CHART -
	//--------------
	areaChartOptions.datasetFill = false;
	//$('a[data-toggle=tab').on('shown.bs.tab', function (e) {
	//	window.dispatchEvent(new Event('resize'));
<%if(!temperature1.equals("00")){%>
var lineChartCanvas1 = $("#lineChart1").get(0).getContext("2d");
var lineChart1 = new Chart(lineChartCanvas1);
lineChart1.Line(areaChartData1, areaChartOptions);
<%}%>

<%if(!temperature2.equals("00")){%>
var lineChartCanvas2 = $("#lineChart2").get(0).getContext("2d");
var lineChart2 = new Chart(lineChartCanvas2);
lineChart2.Line(areaChartData2, areaChartOptions);
<%}%>

<%if(!temperature3.equals("00")){%>
var lineChartCanvas3 = $("#lineChart3").get(0).getContext("2d");
var lineChart3 = new Chart(lineChartCanvas3);
lineChart3.Line(areaChartData3, areaChartOptions);
<%}%>

</script>
	</body>
</html>
