package com.bigbasket.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "orderBean")
public class OrderBean implements Serializable{

	/**
	 * Generated serial version ID
	 */
	private static final long serialVersionUID = -1874474658549948076L;
	private String sOrderId;
	private String sOrderItemId;
	private String sDC;
	private String sHub;
	private String sBarCode;
	private String sSKU;
	private String sCategory;
	private String sStatus;
	private String sStatusTimestamp;
	
	public OrderBean(){
		super();
	}
	
	public void init() {
		this.sOrderId = "";
		this.sOrderItemId = "";
		this.sDC = "";
		this.sHub = "";
		this.sBarCode = "";
		this.sSKU = "";
		this.sCategory = "";
		this.sStatus = "";
		this.sStatusTimestamp = "";
	}
	
	public String toString(){
		String buf = null;
		buf = "Order object: \n";
		buf = buf + "Order Id: " + sOrderId + "\n";
		buf = buf + "Order Item Id: " + sOrderItemId + "\n";
		buf = buf + "DC: " + sDC + "\n";
		buf = buf + "Hub: " + sHub + "\n";
		buf = buf + "Bar Code: " + sBarCode + "\n";
		buf = buf + "SKU: " + sSKU + "\n";
		buf = buf + "Category: " + sCategory + "\n";
		buf = buf + "Status: " + sStatus + "\n";
		buf = buf + "Status Timestamp: " + sStatusTimestamp + "\n";
		return buf;
	}

	public String getOrderId() {
		return sOrderId;
	}

	@XmlElement
	public void setOrderId(String sOrderId) {
		this.sOrderId = sOrderId;
	}

	public String getOrderItemId() {
		return sOrderItemId;
	}

	@XmlElement
	public void setOrderItemId(String sOrderItemId) {
		this.sOrderItemId = sOrderItemId;
	}

	public String getDC() {
		return sDC;
	}

	@XmlElement
	public void setDC(String sDC) {
		this.sDC = sDC;
	}

	public String getHub() {
		return sHub;
	}

	@XmlElement
	public void setHub(String sHub) {
		this.sHub = sHub;
	}

	public String getBarCode() {
		return sBarCode;
	}

	@XmlElement
	public void setBarCode(String sBarCode) {
		this.sBarCode = sBarCode;
	}

	public String getSKU() {
		return sSKU;
	}

	@XmlElement
	public void setSKU(String sSKU) {
		this.sSKU = sSKU;
	}

	public String getCategory() {
		return sCategory;
	}

	@XmlElement
	public void setCategory(String sCategory) {
		this.sCategory = sCategory;
	}

	public String getStatus() {
		return sStatus;
	}

	@XmlElement
	public void setStatus(String sStatus) {
		this.sStatus = sStatus;
	}

	public String getStatusTimestamp() {
		return sStatusTimestamp;
	}

	@XmlElement
	public void setStatusTimestamp(String sStatusTimestamp) {
		this.sStatusTimestamp = sStatusTimestamp;
	}
}
