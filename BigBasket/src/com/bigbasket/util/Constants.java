package com.bigbasket.util;

/**
 * @author kkumbaji
 * @version 1.0
 */
public class Constants {
	    
    public static final String THRESHOLD_TEMPERTURE_MIN = "-23";
    public static final String THRESHOLD_TEMPERTURE_MAX = "40";
    
    //public static String REST_SERVICE_URL = "http://portal.tagbox.in/demorest/";
    public static String REST_SERVICE_URL = "http://localhost:8080/TagBoxRestServices/";
    public static final String SUCCESS_RESULT = "<result>success</result>"; 
    public static final String ERROR = "ERROR";
    public static final String SUCCESS = "SUCCESS"; 
}
