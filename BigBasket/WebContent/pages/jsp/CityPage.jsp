<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>

<%
response.setHeader("Cache-Control","no-cache"); 
response.setHeader("Pragma","no-cache"); 
response.setDateHeader ("Expires", -1); 
%>
<%!
static final Logger logger = LoggerFactory.getLogger("CityPage");
%>
<%
String sUsername = (String) session.getAttribute("username");
if(sUsername == null || sUsername.equals("")) {
	session.setAttribute("callingPage", "CityPage.jsp");
	response.sendRedirect("../../index.html");
}
	java.util.Date dt = new java.util.Date();
	java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd-MM-yyyy HH:mm");
sdf.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
String currentTime = sdf.format(dt);
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>City Performance</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- Add these below two lines for autocomplete and the javascript function at the end  -->
<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="../../bootstrap/css/bootstrap.css">
   <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<!-- Font Awesome -->
<script src="https://use.fontawesome.com/37ba1872d2.js"></script>
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../../dist/css/tagbox.css">
<link href="../../dist/css/select2.css" rel="stylesheet" />
<script src="../../dist/js/select2.js"></script>
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
<link href="https://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet">
<link rel="stylesheet" href="../../dist/css/animate.css">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
  body {
	font-family:  Lato, sans-serif;
	font-weight: 600;
}
</style>
<style>/*please add styles after .js files, so it won't be overwritten*/
    .google-visualization-orgchart-connrow-medium {
        height: 30px !important;/*changed to a higher value so I could put some text in between nodes*/
    }
    .label{
        color:red; font-style:italic;font-size: 12px !important;
        position: absolute;
        margin-top:-60px !important;
    }
  </style>
<script>
$(document).ready(function() {
	$("#City").select2();
});
</script>
<script src="../../dist/js/wow.js"></script>
              <script>
              new WOW().init();
              </script>
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
	<div class="wrapper">

		<header class="main-header">
			<!-- Logo -->
			<div>
				<a href="#" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
					<span class="logo-mini"><img src="../../dist/img/BigBasketLogo.png" style="margin-top:14px"
						alt="User Image"></span> <!-- logo for regular state and mobile devices -->
					<span class="logo-lg"><img src="../../dist/img/BigBasketLogo.png" 
						alt="User Image"></span>
				</a>
			</div>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">

				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
					role="button"> <span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
				</a>
				<label class="navbar-form pull-left" role="search">
					<div class="form-group">
						<b style="padding-right: 115px; font-size: 20px;color:#fff;" class="pull-right">City Performance</b><br> <b style="padding-right: 15px;color:#fff;"><%= currentTime %></b>
					</div>
				</label>

				<div class="navbar-custom-menu">
					<img src="../../dist/img/Logo.png" class="logo-class pull-right"
						alt="User Image">
				</div>
			</nav>

		</header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->


				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<br>
				<ul class="sidebar-menu">
					<li class="header">MAIN NAVIGATION</li>
						<li class="treeview active"><a href="#"> <i
							class="fa fa-home"></i> <span>Home</span>
					</a>
					<ul class="treeview-menu">
							<li><a href="HomePage.jsp"><i class="fa fa-circle-o"></i>
									Cold Chain<br> Dashboard</a></li>
							<li class="active"><a href="CityPage.jsp"><i class="fa fa-circle-o"></i>
									City Performance</a></li>
							<li><a href="RunningShipmentInfo.jsp"><i class="fa fa-circle-o"></i>
									Shipment Detail</a></li>
							<li><a href="MissingBoxesDC.jsp"><i class="fa fa-circle-o"></i>
									Missing Boxes <br>Dashboard</a></li>
							
						
									</ul>
									</li>
						<li class="treeview"><a href="#"> <i
							class="fa fa-bar-chart"></i> <span>Reports</span>
					</a>
					<ul class="treeview-menu">
							<li><a href="Reports.jsp"><i class="fa fa-circle-o"></i>
									Shipment Report</a></li>
									</ul>
									</li>
							
						
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper" style="background:white">
			<!-- Content Header (Page header) -->
			
		<section id="mainSection" class="content"><div class="row">
		<div class="col-md-offset-3 col-md-6">
		
		<div class="well">
		<div class="row">
					<div class="col-md-6 text-center">
					City&nbsp;&nbsp;&nbsp;
					<select id="City" name="City" class="form-control-no-background pull-center">
					<option>All</option>
					<option selected>Bangalore</option>
					<option>Chennai</option>
					<option>Goa</option>
					<option>Mumbai</option>
					<option>Pune</option>
					</select>
					
					</div>
					
					
					<div class="col-md-6 text-center">
					<button type="button" class="btn bb-color">GO</button>
					</div>
					</div>
					</div></div></div>
		<div class="row">
						<div class="col-md-12 alert-text text-center">
								<ol class="breadcrumb">
		  <li class="alert-font-small">BANGALORE CITY PERFORMANCE</li>
		</ol>
									
							</div>
						</div>
			<div class="well">
		<div class="row">
			<div class="col-md-4 text-center">
				<h4>TOTAL COLD CHAIN ORDERS <br>TODAY<br><br><br><span data-toggle="tooltip" class="badge badge-big bb-color">1050</span></h4><i class="fa fa-caret-up"></i>&nbsp;5% THAN DAILY AVERAGE
			</div>
			<div class="col-md-4 text-center">
			<h4>% ORDERS DELIVERED AT RIGHT TEMPERATURE TODAY<br><br><span data-toggle="tooltip" class="badge badge-big bg-green">96%</span></h4><span class="description-percentage text-green"><i class="fa fa-caret-up"></i>&nbsp;2% THAN DAILY AVERAGE</span>
			</div>
			<div class="col-md-4 text-center">
			<h4>% ORDERS WITHOUT TEMPERATURE EXCURSIONS TODAY<br><br><span data-toggle="tooltip" class="badge badge-big bg-green">97%</span></h4><span class="description-percentage text-green"><i class="fa fa-caret-up"></i>&nbsp;1% THAN DAILY AVERAGE</span>
			</div>
		</div>
		</div>
			<div class="row">
				<div class="col-md-6" style="border-right: 2px solid #367fa9;">
					<div class="row row-no-margin">
						<div class="col-md-12 bb-color text-center">
							<h4>CITY PERFORMANCE TODAY</h4>
						</div>
					</div>
					<br>
					<table class="table table-bordered table-striped">
      	<thead><tr class="text-center info">
      		<th class="text-center alert-font-small" style="width:20%">Location</th><th class="text-center alert-font-small" style="width:40%">% Orders Delivered At Right Temperature Today</th><th class="text-center alert-font-small" style="width:40%">% Orders Without Temperature Excursions Today</th></tr>
      	</thead>
      	<tbody>
      		<tr><td>DC 1</td><td class="text-center text-green"><div class="progress">
  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="98" aria-valuemin="0" aria-valuemax="100" style="width: 98%;">
    98%
  </div>
</div></td><td class="text-center"><div class="progress">
  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="98" aria-valuemin="0" aria-valuemax="100" style="width: 98%;">
    98%
  </div>
</div></td></tr>
      		<tr><td>Hub 1</td><td class="text-center text-green"><div class="progress">
  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="97" aria-valuemin="0" aria-valuemax="100" style="width: 97%;">
    97%
  </div>
</div></td><td class="text-center"><div class="progress">
  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="96" aria-valuemin="0" aria-valuemax="100" style="width: 96%;">
    96%
  </div>
</div></td></tr>
      		<tr><td>Hub 2</td><td class="text-center text-green"><div class="progress">
  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="99" aria-valuemin="0" aria-valuemax="100" style="width: 99%;">
    99%
  </div>
</div></td><td class="text-center"><div class="progress">
  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="96" aria-valuemin="0" aria-valuemax="100" style="width: 96%;">
    96%
  </div>
</div></td></tr>
      		<tr><td>DC 2</td><td class="text-center text-green"><div class="progress">
  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="94" aria-valuemin="0" aria-valuemax="100" style="width: 94%;">
    94%
  </div>
</div></td><td class="text-center"><div class="progress">
  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="94" aria-valuemin="0" aria-valuemax="100" style="width: 94%;">
    94%
  </div>
</div></td></tr>
      		<tr><td>Hub 3</td><td class="text-center text-green"><div class="progress">
  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="94" aria-valuemin="0" aria-valuemax="100" style="width: 94%;">
    94%
  </div>
</div></td><td class="text-center"><div class="progress">
  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="96" aria-valuemin="0" aria-valuemax="100" style="width: 96%;">
    96%
  </div>
</div></td></tr>
      		
      	</tbody>
      </table>
				</div>
				<div class="col-md-6">
				<div class="row row-no-margin">
						<div class="col-md-12 bb-color">
							<h4>PRODUCT CATEGORY PERFORMANCE TODAY</h4>
						</div>
					</div>
					<br>
					<table class="table table-bordered table-striped">
      	<thead><tr class="text-center info">
      		<th class="text-center alert-font-small" style="width:20%">Product Category</th><th class="text-center alert-font-small" style="width:40%">% Orders Delivered At Right Temperature Today</th><th class="text-center alert-font-small" style="width:40%">% Orders Without Temperature Excursions Today</th></tr>
      	</thead>
      	<tbody>
      		<tr class="text-center"><td><a href="#" data-toggle="modal" data-target="#myModal1" style="text-decoration:underline;color:blue">CFV</a></td><td><div class="progress">
  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="96" aria-valuemin="0" aria-valuemax="100" style="width: 96%;">
    96%
  </div>
</div></td><td><div class="progress">
  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="97" aria-valuemin="0" aria-valuemax="100" style="width: 97%;">
    97%
  </div>
</div></td></tr>
      		<tr class="text-center"><td><a href="#">CV</a></td><td><div class="progress">
  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="98" aria-valuemin="0" aria-valuemax="100" style="width: 98%;">
    98%
  </div>
</div></td><td><div class="progress">
  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="99" aria-valuemin="0" aria-valuemax="100" style="width: 99%;">
    99%
  </div>
</div></td></tr>
      		<tr class="text-center"><td><a href="#">CNV</a></td><td><div class="progress">
  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="96" aria-valuemin="0" aria-valuemax="100" style="width: 96%;">
    96%
  </div>
</div></td><td><div class="progress">
  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="97" aria-valuemin="0" aria-valuemax="100" style="width: 97%;">
    97%
  </div>
</div></td></tr>
      		<tr class="text-center"><td><a href="#">FV</a></td><td><div class="progress">
  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="97" aria-valuemin="0" aria-valuemax="100" style="width: 97%;">
    97%
  </div>
</div></td><td><div class="progress">
  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="98" aria-valuemin="0" aria-valuemax="100" style="width: 98%;">
    98%
  </div>
</div></td></tr>
      		<tr class="text-center"><td><a href="#">FNV</a></td><td><div class="progress">
  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="98" aria-valuemin="0" aria-valuemax="100" style="width: 98%;">
    98%
  </div>
</div></td><td><div class="progress">
  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="96" aria-valuemin="0" aria-valuemax="100" style="width: 96%;">
    96%
  </div>
</div></td></tr>
      	</tbody>
      </table>
				</div>
				
			</div><br>
			<div class="row text-center"><div class="col-md-12 text-center">
					<table class="table table-striped table-bordered">
							<thead><tr><td colspan="4" class="bb-color text-center">PERFORMANCE OF BANGALORE IN LAST WEEK </td></tr>
							</thead>
						</table>
						</div>
						</div>
			<div class="row">
				<div class="col-md-6">
				<div class="row row-no-margin text-center">
						<div class="col-md-12">
							<h4>PERFORMANCE TREND FOR LAST WEEK</h4>
						</div>
					</div>
					<br>
				<div class="text-center">
			<div class="box-body">
				<div class="chart">
						<canvas id="chart1"
							style="height: 175px; width: 100px"></canvas>
					</div>
			</div>
		</div>
				</div>
				<div class="col-md-6" style="border-left: 2px solid #367fa9;">
				<div class="row row-no-margin text-center">
						<div class="col-md-12">
							<h4>EXCURSIONS MAP FOR LAST WEEK</h4>
						</div>
					</div>
				<div class="text-center">
					<div id="chart_div"></div>
			
		</div>
				</div>
			</div>
			<br>
			<div id="div1" class="row wow slideInUp">
				<div class="col-md-12">
					<div class="well white-background">
						
							<div class="row text-center"><div class="col-md-12 text-center">
					<table class="table table-striped table-bordered">
							<thead><tr><td colspan="4" class="bb-color text-center">COLD BOX COUNT STATUS</td></tr>
							<tr class="text-center info">
      		<th class="text-center alert-font-small">Location</th><th class="text-center alert-font-small">Total Cold Boxes Inventory</th><th class="text-center alert-font-small">Missing Cold Boxes Till Date</th><th class="text-center alert-font-small">Missing Cold Boxes In Past Week</th></tr>
      	</thead>
      	<tbody>
      	<tr><td><span class="alert-font">Bangalore</span></td><td><span class="success-text alert-font">1000</span></td><td><span class="alert-text alert-font">70</span>&nbsp;&nbsp;&nbsp;&nbsp;<button class="btn btn-active" data-toggle="modal" data-target="#MissingBoxes">VIEW DETAILS</button></td><td><span class="alert-text alert-font">10</span></td></tr>
      	</tbody>
						</table>
						</div>
						</div>
						<br><Br>
						<div class="row text-center"><div class="col-md-6 text-center">
			<table class="table table-bordered table-striped">
      	<thead><tr class="text-center info">
      		<th class="text-center alert-font-small" style="width:20%">DC</th><th class="text-center alert-font-small" style="width:30%">Cold Boxes Missing Till Date</th><th class="text-center alert-font-small" style="width:40%">Cold Boxes Missing Past Week</th></tr>
      	</thead>
      	<tbody>
      		<tr><td><a href="MissingBoxesDC.jsp" style="text-decoration:underline;color:blue">DC 1</a></td><td><a href="#" class="alert-text alert-font" data-toggle="modal" data-target="#myModal">10</a></td><td><a href="#" class="alert-text alert-font" data-toggle="modal" data-target="#myModal">02</a></td></tr>
      		<tr><td><a href="#">DC 2</a></td><td><a href="#" class="alert-text alert-font" data-toggle="modal" data-target="#myModal">15</a></td><td><a href="#" class="alert-text alert-font" data-toggle="modal" data-target="#myModal">02</a></td></tr>
      	</tbody>
      </table>
      </div>
      <div class="col-md-6 text-center">
      <table class="table table-bordered table-striped">
      	<thead><tr class="text-center info">
      		<th class="text-center alert-font-small" style="width:20%">Hub</th><th class="text-center alert-font-small" style="width:30%">Cold Boxes Missing Till Date</th><th class="text-center alert-font-small" style="width:40%">Cold Boxes Missing Past Week</th></tr>
      	</thead>
      	<tbody>
      		<tr><td><a href="MissingBoxesHub.jsp" style="text-decoration:underline;color:blue">Hub 1</a></td><td><a href="#" class="alert-text alert-font">10</a></td><td><a href="#" class="alert-text alert-font" data-toggle="modal" data-target="#myModal">03</a></td></tr>
      		<tr><td><a href="#">Hub 2</a></td><td><a href="#" class="alert-text alert-font">40</a></td><td><a href="#" class="alert-text alert-font" data-toggle="modal" data-target="#myModal">01</a></td></tr>
      		<tr><td><a href="#" id="div1" >Hub 3</a></td><td><a href="#" class="alert-text alert-font">30</a></td><td><a href="#" class="alert-text alert-font" data-toggle="modal" data-target="#myModal">02</a></td></tr>
      	</tbody>
      </table>
      </div>
						</div>
						</div>
					</div>
					</div>
				
	</section>
	</div></div>
	
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Product Category Details</h4>
      </div>
      <div class="modal-body">
      <table class="table table-bordered table-black-border">
      	<thead><tr class="text-center bb-color">
      		<th class="text-center" style="width:25%">Location</th><th class="text-center" style="width:45%">% Orders Delivered At Right Temperature Today</th><th style="width:30%">&nbsp;</th></tr>
      	</thead>
      	<tbody>
      	<tr class="warning"><td>CFV</td><td class="text-center"><h4 style="color:green">97%</h4></td><td class="text-center"><a href="RunningShipmentInfo.jsp" class="btn btn-warning btn-sm" style="width:60%">View Orders</a></td></tr>
      		<tr class="info"><td>&nbsp;&nbsp;&nbsp;DC 1</td><td class="text-center"><h4 style="color:green">97%</h4></td><td class="text-center"><a href="RunningShipmentInfo.jsp" class="btn btn-warning btn-sm" style="width:60%">View Orders</a></td></tr>
      		<tr class="info"><td>&nbsp;&nbsp;&nbsp;DC 2</td><td class="text-center"><h4 style="color:green">97%</h4></td><td class="text-center"><a href="RunningShipmentInfo.jsp" class="btn btn-warning btn-sm" style="width:60%">View Orders</a></td></tr>
      		
      	</tbody>
      </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
    
<div class="modal fade" tabindex="-1" role="dialog" id="ProductTrends">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Product Category Performance Trends</h4>
      </div>
      <div class="modal-body text-center">
      <span class="fa-color-blue">% ORDERS DELIVERED AT RIGHT TEMPERATURE IN THE PAST WEEK</span>
		<div class="row">
			<div class="col-md-4">
		<div class="box" style="border-top-color:#fff">
			<div class="box-header with-border bb-color">
				&nbsp;CFV
			</div>
			<div class="box-body">
				<div class="chart">
						<canvas id="chart30"
							style="height: 175px; width: 100px"></canvas>
					</div>
			</div>
		</div>
			</div>
			<div class="col-md-4">
		<div class="box" style="border-top-color:#fff">
			<div class="box-header with-border bb-color">
				&nbsp;CV
			</div>
			<div class="box-body">
				<div class="chart">
						<canvas id="chart31"
							style="height: 175px; width: 100px"></canvas>
					</div>
			</div>
		</div>
			</div>
			<div class="col-md-4">
		<div class="box" style="border-top-color:#fff">
			<div class="box-header with-border bb-color">
				&nbsp;CNV
			</div>
			<div class="box-body">
				<div class="chart">
						<canvas id="chart32"
							style="height: 175px; width: 100px"></canvas>
					</div>
			</div>
		</div>
			</div>
		</div>
		<div class="row">
		<div class="col-md-4">
		<div class="box" style="border-top-color:#fff">
			<div class="box-header with-border bb-color">
				&nbsp;FV
			</div>
			<div class="box-body">
				<div class="chart">
						<canvas id="chart33"
							style="height: 175px; width: 100px"></canvas>
					</div>
			</div>
		</div>
			</div>
			<div class="col-md-4">
		<div class="box" style="border-top-color:#fff">
			<div class="box-header with-border bb-color">
				&nbsp;FNV
			</div>
			<div class="box-body">
				<div class="chart">
						<canvas id="chart34"
							style="height: 175px; width: 100px"></canvas>
					</div>
			</div>
		</div>
			</div>
			</div>
    </div>
		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
		<!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0
			</div>
			<strong>Copyright &copy; 2017 <a href="http://tagbox.in">TagBox</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Control Sidebar -->
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->
	
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
	<script src="../../bootstrap/js/bootstrap.min.js"></script>
	<script src="../../dist/js/app.min.js"></script>
	<script src="../../dist/js/demo.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
	<script>

Chart.defaults.global.legend.display = false;


var barOptions_stacked = {
		responsive: true,
   	 maintainAspectRatio: false,
	    tooltips: {
	        enabled: true
	    },
	    legend:{
	        display:false
	    },
	    elements: {
            point:{
                radius: 1
            }
        },
	    scales: {
	        xAxes: [{
	        	scaleLabel: {
					labelString: 'Date',
					display: true
		    	},
	                    gridLines: {
	                        display:false
	                    },
	                    ticks: {
	                        autoSkip: true,
	                        maxTicksLimit: 20
	                    }
	                }],
	        yAxes: [{
	        	scaleLabel: {
					labelString: '% Orders',
					display: true
		    	},
	        			
	                    gridLines: {
	                        display:false
	                    },
	        			ticks: {
	        				autoSkip: true,
	                        maxTicksLimit: 20,
	                        suggestedMin: 92
	        			}
	                }]
	        },
	        fill: false,
            lineTension: 0,
            backgroundColor: "rgba(75,192,192,0.4)",
            borderColor: "rgba(75,192,192,1)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            spanGaps: false,
	};
	
	var ctx2 = document.getElementById("chart1");
	var myChart2 = new Chart(ctx2, {
	    type: 'line',
	    data: {
	    	labels : ['02/08/2017', '03/08/2017', '04/08/2017', '05/08/2017', '06/08/2017', '07/08/2017', '08/08/2017'],
			datasets : [ {
				label : ["% Orders Delivered"],
				fill: false,
				borderColor: "#a6cee3",
				data : ['95', '95', '94', '98', '94', '94', '96']
			}]
	    },
	    options: barOptions_stacked
	});
	
	
</script>
	</body>
	<script type="text/javascript">
google.charts.load('current', {packages:["orgchart"]});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {
  var data = new google.visualization.DataTable();
  data.addColumn('string', 'Name');
  data.addColumn('string', 'Location');
  data.addColumn('string', 'ToolTip');

  // For each orgchart box, provide the name, manager, and tooltip to show.
  data.addRows([
    [{v:'DC1', f:'DC 1<div style="color:red; font-style:italic">02</div>'},
     '', 'DC 1'],
    [{v:'Hub1', f:'Hub 1<div style="color:red; font-style:italic">03</div><div class="label">02</div>'},
     'DC1', 'Hub 1'],
    [{v:'Hub2', f:'Hub 2<div style="color:red; font-style:italic">01</div><div class="label">01</div>'},
     'DC1', 'Hub 2'],
    [{v:'EC1', f:'End_Cust<div style="color:green; font-style:italic">00</div><div class="label">04</div>'},
      'Hub1', 'End Customer'],
    [{v:'EC2', f:'End_Cust<div style="color:red; font-style:italic">02</div><div class="label">06</div>'},
     'Hub2', 'End Customer'],
    [{v:'DC2', f:'DC 2<div style="color:red; font-style:italic">02</div>'},
      '', 'DC 2'],
    [{v:'Hub3', f:'Hub 3<div style="color:red; font-style:italic">02</div><div class="label">03</div>'},
      'DC2', 'Hub 3'],
    [{v:'EC3', f:'End_Cust<div style="color:green; font-style:italic">00</div><div class="label">01</div>'},
      'Hub3', 'End Customer']
  ]);

  // Create the chart.
  var chart = new google.visualization.OrgChart(document.getElementById('chart_div'));
  // Draw the chart, setting the allowHtml option to true for the tooltips.
  chart.draw(data, {allowHtml:true, allowCollapse:true});
}
</script>
</html>
