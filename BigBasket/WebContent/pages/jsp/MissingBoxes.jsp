<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>

<%
response.setHeader("Cache-Control","no-cache"); 
response.setHeader("Pragma","no-cache"); 
response.setDateHeader ("Expires", -1); 
%>
<%!
static final Logger logger = LoggerFactory.getLogger("MissingBoxes");
%>
<%
String sUsername = (String) session.getAttribute("username");
if(sUsername == null || sUsername.equals("")) {
	session.setAttribute("callingPage", "MissingBoxes.jsp");
	response.sendRedirect("../../index.html");
}
	java.util.Date dt = new java.util.Date();
	java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd-MM-yyyy HH:mm");
sdf.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
String currentTime = sdf.format(dt);
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Missing Boxes</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- Add these below two lines for autocomplete and the javascript function at the end  -->
<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="../../bootstrap/css/bootstrap.css">
   
<!-- Font Awesome -->
<script src="https://use.fontawesome.com/37ba1872d2.js"></script>
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../../dist/css/tagbox.css">
<link href="../../dist/css/select2.css" rel="stylesheet" />
<script src="../../dist/js/select2.js"></script>
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
<link href="https://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
  body {
	font-family:  Lato, sans-serif;
	font-weight: 600;
}
</style>
<script>
$(document).ready(function() {
	$("#City").select2();
	$("#DC").select2();
	$("#Hub").select2();
	$("#Date").select2();
});
</script>
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
	<div class="wrapper">

		<header class="main-header">
			<!-- Logo -->
			<div>
				<a href="#" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
					<span class="logo-mini"><img src="../../dist/img/BigBasketLogo.png" style="margin-top:14px"
						alt="User Image"></span> <!-- logo for regular state and mobile devices -->
					<span class="logo-lg"><img src="../../dist/img/BigBasketLogo.png" 
						alt="User Image"></span>
				</a>
			</div>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">

				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
					role="button"> <span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
				</a>
				<label class="navbar-form pull-left" role="search">
					<div class="form-group">
						<b style="padding-right: 115px; font-size: 20px;color:#fff;" class="pull-right">Missing Boxes Dashboard</b><br><b style="padding-right: 15px;color:#fff;"><%= currentTime %></b>
					</div>
				</label>

				<div class="navbar-custom-menu">
					<img src="../../dist/img/Logo.png" class="logo-class pull-right"
						alt="User Image">
				</div>
			</nav>

		</header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->


				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<br>
				<ul class="sidebar-menu">
					<li class="header">MAIN NAVIGATION</li>
						<li class="treeview active"><a href="#"> <i
							class="fa fa-home"></i> <span>Home</span>
					</a>
					<ul class="treeview-menu">
							<li><a href="HomePage.jsp"><i class="fa fa-circle-o"></i>
									Cold Chain<br> Dashboard</a></li>
							<li><a href="CityPage.jsp"><i class="fa fa-circle-o"></i>
									City Performance</a></li>
							<li><a href="RunningShipmentInfo.jsp"><i class="fa fa-circle-o"></i>
									Shipment Detail</a></li>
							<li class="active"><a href="MissingBoxes.jsp"><i class="fa fa-circle-o"></i>
									Missing Boxes <br>Dashboard</a></li>
							
						
									</ul>
									</li>
						<li class="treeview"><a href="#"> <i
							class="fa fa-bar-chart"></i> <span>Reports</span>
					</a>
					<ul class="treeview-menu">
							<li><a href="Reports.jsp"><i class="fa fa-circle-o"></i>
									Shipment Report</a></li>
									</ul>
									</li>
							
						
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper" style="background:white">
			<!-- Content Header (Page header) -->
			
		<section id="mainSection" class="content">
		<div class="well">
		<div class="row">
		<div class="col-md-3 text-center">
					CITY&nbsp;&nbsp;&nbsp;
					<select id="City" name="City" class="form-control-no-background pull-center">
					<option>All</option>
					<option selected>Bangalore</option>
					<option>Chennai</option>
					<option>Goa</option>
					<option>Mumbai</option>
					<option>Pune</option>
					</select>
					</div>
					<div class="col-md-3 text-center">
					DC&nbsp;&nbsp;&nbsp;
					<select id="DC" name="DC" class="form-control-no-background pull-center">
					<option>All</option>
					<option>DC 1</option>
					<option>DC 2</option>
					<option>DC 3</option>
					</select>
					
					</div>
					<div class="col-md-3 text-center">
					Hub&nbsp;&nbsp;&nbsp;
					<select id="Hub" name="Hub" class="form-control-no-background pull-center">
					<option>All</option>
					<option>Hub 1</option>
					<option>Hub 2</option>
					<option>Hub 3</option>
					</select>
					
					</div>
					<div class="col-md-3 text-center">
					Date&nbsp;&nbsp;&nbsp;
					<select id="Date" name="Date" class="form-control-no-background pull-center">
					<option selected>Last Week</option>
					<option>Today</option>
					<option>Yesterday</option>
					<option>02/08/2017</option>
					<option>03/08/2017</option>
					<option>04/08/2017</option>
					<option>05/08/2017</option>
					<option>06/08/2017</option>
					<option>07/08/2017</option>
					<option>08/08/2017</option>
					</select>
					
					</div>
					</div>
					<div class="row">
					<div class="col-md-12 text-center"><br>
					<button type="button" class="btn bb-color">GO</button>
					</div>
					</div>
					</div><br>
					<div class="row text-center"><div class="col-md-offset-2 col-md-10 text-center">
					<table class="table table-striped table-bordered" style="width:80%">
							<thead><tr><td colspan="4" class="bb-color text-center">COLD BOX COUNT STATUS</td></tr>
							<tr class="text-center info">
      		<th class="text-center alert-font-small">Location</th><th class="text-center alert-font-small">Total Cold Boxes Inventory</th><th class="text-center alert-font-small">Missing Cold Boxes Till Date</th><th class="text-center alert-font-small">Missing Cold Boxes In Past Week</th></tr>
      	</thead>
      	<tbody>
      	<tr><td><span class="alert-font">Bangalore</span></td><td><span class="success-text alert-font">1000</span></td><td><span class="alert-text alert-font">70</span>&nbsp;&nbsp;&nbsp;&nbsp;<button class="btn btn-active" data-toggle="modal" data-target="#MissingBoxes">VIEW DETAILS</button></td><td><span class="alert-text alert-font">10</span></td></tr>
      	</tbody>
						</table>
						</div>
						</div>
						<br><Br>
						<div class="row text-center"><div class="col-md-offset-2 col-md-4 text-center">
			<table class="table table-bordered table-striped">
      	<thead><tr class="text-center info">
      		<th class="text-center alert-font-small" style="width:20%">DC</th><th class="text-center alert-font-small" style="width:30%">Cold Boxes Missing Till Date</th><th class="text-center alert-font-small" style="width:40%">Cold Boxes Missing Past Week</th></tr>
      	</thead>
      	<tbody>
      		<tr><td><a href="MissingBoxesDC.jsp" style="text-decoration:underline;color:blue">DC 1</a></td><td><a href="#" class="alert-text alert-font" data-toggle="modal" data-target="#myModal">10</a></td><td><a href="#" class="alert-text alert-font" data-toggle="modal" data-target="#myModal">02</a></td></tr>
      		<tr><td><a href="#">DC 2</a></td><td><a href="#" class="alert-text alert-font" data-toggle="modal" data-target="#myModal">15</a></td><td><a href="#" class="alert-text alert-font" data-toggle="modal" data-target="#myModal">02</a></td></tr>
      	</tbody>
      </table>
      </div>
      <div class="col-md-4 text-center">
      <table class="table table-bordered table-striped">
      	<thead><tr class="text-center info">
      		<th class="text-center alert-font-small" style="width:20%">Hub</th><th class="text-center alert-font-small" style="width:30%">Cold Boxes Missing Till Date</th><th class="text-center alert-font-small" style="width:40%">Cold Boxes Missing Past Week</th></tr>
      	</thead>
      	<tbody>
      		<tr><td><a href="MissingBoxesHub.jsp" style="text-decoration:underline;color:blue">Hub 1</a></td><td><a href="#" class="alert-text alert-font">10</a></td><td><a href="#" class="alert-text alert-font" data-toggle="modal" data-target="#myModal">03</a></td></tr>
      		<tr><td><a href="#">Hub 2</a></td><td><a href="#" class="alert-text alert-font">40</a></td><td><a href="#" class="alert-text alert-font" data-toggle="modal" data-target="#myModal">01</a></td></tr>
      		<tr><td><a href="#" id="div1" >Hub 3</a></td><td><a href="#" class="alert-text alert-font">30</a></td><td><a href="#" class="alert-text alert-font" data-toggle="modal" data-target="#myModal">02</a></td></tr>
      	</tbody>
      </table>
      </div>
						</div>
	</section>
	
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Missing Boxes Details</h4>
      </div>
      <div class="modal-body">
      <div class="btn-group pull-right">
							<button class="btn bb-color btn-sm dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Export Data</button>
							<ul class="dropdown-menu " role="menu">
								<!-- <li><a href="#" onClick ="$('#temp1_table').tableExport({type:'json',escape:'false'});"> <img src='../../dist/img/json.png' width='24px'> JSON</a></li>
								<li><a href="#" onClick ="$('#temp1_table').tableExport({type:'sql',escape:'false'});"> <img src='../../dist/img/sql.png' width='24px'> SQL</a></li>
								<li class="divider"></li>
								<li><a href="#" onClick ="$('#temp1_table').tableExport({type:'xml',escape:'false'});"> <img src='../../dist/img/xml.png' width='24px'> XML</a></li> -->
								<li><a href="#" onClick ="$('#temp1_table').tableExport({type:'csv',escape:'false'});"> <img src='../../dist/img/csv.png' width='24px'> CSV</a></li>
								<li><a href="#" onClick ="$('#temp1_table').tableExport({type:'excel',escape:'false'});"> <img src='../../dist/img/xls.png' width='24px'> XLS</a></li>
								<li class="divider"></li>				
								<li><a href="#" onClick ="$('#temp1_table').tableExport({type:'txt',escape:'false'});"> <img src='../../dist/img/txt.png' width='24px'> TXT</a></li>
								<!-- <li><a href="#" onClick ="$('#temp1_table').tableExport({type:'doc',escape:'false'});"> <img src='../../dist/img/word.png' width='24px'> Word</a></li>
								<li class="divider"></li> -->
								<!-- <li><a href="#" onClick ="$('#temp1_table').tableExport({type:'pdf',pdfFontSize:'7',escape:'false'});"> <img src='../../dist/img/pdf.png' width='15px'> PDF</a></li> -->
								<li><a href="#" onClick ="exportData()"> <img src='../../dist/img/pdf.png' width='15px'> PDF</a></li>
							</ul>
						</div><br>
      <table class="table table-bordered">
      	<thead><tr class="text-center bb-color">
      		<th class="text-center">Box Id</th><th class="text-center">Last Seen Location</th><th class="text-center">Last Seen<br> Time</th><th class="text-center">Last Order<br> Id</th>
      		<th class="text-center">DC</th><th class="text-center">Hub</th><th class="text-center">Delivery Person</th><th class="text-center">Delivery Vehicle</th><th class="text-center">Customer Name</th>
      		<th class="text-center">Customer<br> Address</th></tr>
      	</thead>
      	<tbody>
      		<tr class="text-center"><td>1</td><td class="text-center">Hub 1</td><td class="text-center">04/08/2017 11:00AM</td>
      		<td>XX123-ABC87-87634-7SD34</td><td class="text-center">DC 1</td><td class="text-center">Hub 1</td>
      		<td>Robert</td><td class="text-center">KA 03 ML 1234</td><td class="text-center">Peter</td><td class="text-center">123 Street, ABC Lane, Bangalore</td></tr>
      		<tr class="text-center"><td>2</td><td class="text-center">Hub 2</td><td class="text-center">04/08/2017 12:00PM</td>
      		<td>23434-AB342-09987-OI34D</td><td class="text-center">DC 1</td><td class="text-center">Hub 1</td>
      		<td>Naresh</td><td class="text-center">KA 04 ML 4567</td><td class="text-center">Anna</td><td class="text-center">123 Street, ABC Lane, Bangalore</td></tr>
      		<tr class="text-center"><td>3</td><td class="text-center">Hub 1</td><td class="text-center">04/08/2017 11:30AM</td>
      		<td>LJKL0-34322-KJWE9-42322</td><td class="text-center">DC 1</td><td class="text-center">Hub 1</td>
      		<td>Vijay</td><td class="text-center">KA 03 ML 8787</td><td class="text-center">Kavita</td><td class="text-center">123 Street, ABC Lane, Bangalore</td></tr>
      		<tr class="text-center"><td>4</td><td class="text-center">Hub 2</td><td class="text-center">04/08/2017 11:20AM</td>
      		<td>8734A-K2342-B9009-K3423</td><td class="text-center">DC 1</td><td class="text-center">Hub 1</td>
      		<td>Suresh</td><td class="text-center">KA 03 ML 1111</td><td class="text-center">Deepti</td><td class="text-center">123 Street, ABC Lane, Bangalore</td></tr>
      		<tr class="text-center"><td>5</td><td class="text-center">Hub 2</td><td class="text-center">04/08/2017 04:00PM</td>
      		<td>XX123-ABC87-87634-7SD34</td><td class="text-center">DC 1</td><td class="text-center">Hub 1</td>
      		<td>George</td><td class="text-center">KA 03 ML 9865</td><td class="text-center">Preeti</td><td class="text-center">123 Street, ABC Lane, Bangalore</td></tr>
      		<tr class="text-center"><td>6</td><td class="text-center">Hub 1</td><td class="text-center">04/08/2017 10:00AM</td>
      		<td>XX123-ABC87-87634-7SD34</td><td class="text-center">DC 1</td><td class="text-center">Hub 1</td>
      		<td>David</td><td class="text-center">KA 03 ML 5666</td><td class="text-center">Pushpa</td><td class="text-center">123 Street, ABC Lane, Bangalore</td></tr>
      	</tbody>
      </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
    
    
				</div>
				
				<div class="modal fade" id="MissingBoxes" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Missing Cold Boxes Details</h4>
      </div>
      <div class="modal-body">
      <table class="table table-bordered table-striped">
      <thead><tr><td colspan="9" class="bb-color text-center"># OF COLD BOXES MISSING IN BANGALORE TILL DATE</td></tr></thead>
      	<thead><tr class="text-center info">
      		<th class="text-center">Box Id</th><th class="text-center">Last Seen Location</th><th class="text-center">Last Seen<br> Time</th><th class="text-center">Last Order<br> Id</th>
      		<th class="text-center">Hub</th><th class="text-center">Delivery Person</th><th class="text-center">Delivery Vehicle</th><th class="text-center">Customer Name</th>
      		<th class="text-center">Customer<br> Address</th></tr>
      	</thead>
      	<tbody>
      		<tr class="text-center warning"><td>1</td><td class="text-center">Hub 1</td><td class="text-center">07/08/2017 11:00AM</td>
      		<td>XX123-ABC87-87634-7SD34</td><td class="text-center">Hub 1</td>
      		<td>Robert</td><td class="text-center">KA 03 ML 1234</td><td class="text-center">Peter</td><td class="text-center">123 Street, ABC Lane, Bangalore</td></tr>
      		<tr class="text-center warning"><td>2</td><td class="text-center">Hub 2</td><td class="text-center">06/08/2017 12:00PM</td>
      		<td>23434-AB342-09987-OI34D</td><td class="text-center">Hub 1</td>
      		<td>Naresh</td><td class="text-center">KA 04 ML 4567</td><td class="text-center">Anna</td><td class="text-center">123 Street, ABC Lane, Bangalore</td></tr>
      		<tr class="text-center warning"><td>3</td><td class="text-center">Hub 1</td><td class="text-center">06/08/2017 11:30AM</td>
      		<td>LJKL0-34322-KJWE9-42322</td><td class="text-center">Hub 1</td>
      		<td>Vijay</td><td class="text-center">KA 03 ML 8787</td><td class="text-center">Kavita</td><td class="text-center">123 Street, ABC Lane, Bangalore</td></tr>
      		<tr class="text-center warning"><td>4</td><td class="text-center">Hub 2</td><td class="text-center">05/08/2017 11:20AM</td>
      		<td>8734A-K2342-B9009-K3423</td><td class="text-center">Hub 1</td>
      		<td>Suresh</td><td class="text-center">KA 03 ML 1111</td><td class="text-center">Deepti</td><td class="text-center">123 Street, ABC Lane, Bangalore</td></tr>
      		<tr class="text-center warning"><td>5</td><td class="text-center">Hub 2</td><td class="text-center">05/08/2017 04:00PM</td>
      		<td>XX123-ABC87-87634-7SD34</td><td class="text-center">Hub 1</td>
      		<td>George</td><td class="text-center">KA 03 ML 9865</td><td class="text-center">Preeti</td><td class="text-center">123 Street, ABC Lane, Bangalore</td></tr>
      		<tr class="text-center warning"><td>6</td><td class="text-center">Hub 1</td><td class="text-center">04/08/2017 10:00AM</td>
      		<td>XX123-ABC87-87634-7SD34</td><td class="text-center">Hub 1</td>
      		<td>David</td><td class="text-center">KA 03 ML 5666</td><td class="text-center">Pushpa</td><td class="text-center">123 Street, ABC Lane, Bangalore</td></tr>
      		<tr class="text-center warning"><td>7</td><td class="text-center">Hub 1</td><td class="text-center">04/08/2017 11:00AM</td>
      		<td>XX123-ABC87-87634-7SD34</td><td class="text-center">Hub 1</td>
      		<td>Robert</td><td class="text-center">KA 03 ML 1234</td><td class="text-center">Peter</td><td class="text-center">123 Street, ABC Lane, Bangalore</td></tr>
      		<tr class="text-center warning"><td>8</td><td class="text-center">Hub 2</td><td class="text-center">03/08/2017 12:00PM</td>
      		<td>23434-AB342-09987-OI34D</td><td class="text-center">Hub 1</td>
      		<td>Naresh</td><td class="text-center">KA 04 ML 4567</td><td class="text-center">Anna</td><td class="text-center">123 Street, ABC Lane, Bangalore</td></tr>
      		<tr class="text-center warning"><td>9</td><td class="text-center">Hub 1</td><td class="text-center">03/08/2017 11:30AM</td>
      		<td>LJKL0-34322-KJWE9-42322</td><td class="text-center">Hub 1</td>
      		<td>Vijay</td><td class="text-center">KA 03 ML 8787</td><td class="text-center">Kavita</td><td class="text-center">123 Street, ABC Lane, Bangalore</td></tr>
      		<tr class="text-center warning"><td>10</td><td class="text-center">Hub 2</td><td class="text-center">03/08/2017 11:20AM</td>
      		<td>8734A-K2342-B9009-K3423</td><td class="text-center">Hub 1</td>
      		<td>Suresh</td><td class="text-center">KA 03 ML 1111</td><td class="text-center">Deepti</td><td class="text-center">123 Street, ABC Lane, Bangalore</td></tr>
      		<tr class="text-center"><td>5</td><td class="text-center">Hub 2</td><td class="text-center">28/07/2017 04:00PM</td>
      		<td>XX123-ABC87-87634-7SD34</td><td class="text-center">Hub 1</td>
      		<td>George</td><td class="text-center">KA 03 ML 9865</td><td class="text-center">Preeti</td><td class="text-center">123 Street, ABC Lane, Bangalore</td></tr>
      		<tr class="text-center"><td>6</td><td class="text-center">Hub 1</td><td class="text-center">20/07/2017 10:00AM</td>
      		<td>XX123-ABC87-87634-7SD34</td><td class="text-center">Hub 1</td>
      		<td>David</td><td class="text-center">KA 03 ML 5666</td><td class="text-center">Pushpa</td><td class="text-center">123 Street, ABC Lane, Bangalore</td></tr>
      		<tr class="text-center"><td>1</td><td class="text-center">Hub 1</td><td class="text-center">18/06/2017 11:00AM</td>
      		<td>XX123-ABC87-87634-7SD34</td><td class="text-center">Hub 1</td>
      		<td>Robert</td><td class="text-center">KA 03 ML 1234</td><td class="text-center">Peter</td><td class="text-center">123 Street, ABC Lane, Bangalore</td></tr>
      		<tr class="text-center"><td>2</td><td class="text-center">Hub 2</td><td class="text-center">28/06/2017 12:00PM</td>
      		<td>23434-AB342-09987-OI34D</td><td class="text-center">Hub 1</td>
      		<td>Naresh</td><td class="text-center">KA 04 ML 4567</td><td class="text-center">Anna</td><td class="text-center">123 Street, ABC Lane, Bangalore</td></tr>
      		<tr class="text-center"><td>3</td><td class="text-center">Hub 1</td><td class="text-center">21/05/2017 11:30AM</td>
      		<td>LJKL0-34322-KJWE9-42322</td><td class="text-center">Hub 1</td>
      		<td>Vijay</td><td class="text-center">KA 03 ML 8787</td><td class="text-center">Kavita</td><td class="text-center">123 Street, ABC Lane, Bangalore</td></tr>
      		<tr class="text-center"><td>4</td><td class="text-center">Hub 2</td><td class="text-center">22/04/2017 11:20AM</td>
      		<td>8734A-K2342-B9009-K3423</td><td class="text-center">Hub 1</td>
      		<td>Suresh</td><td class="text-center">KA 03 ML 1111</td><td class="text-center">Deepti</td><td class="text-center">123 Street, ABC Lane, Bangalore</td></tr>
      		<tr class="text-center"><td>5</td><td class="text-center">Hub 2</td><td class="text-center">07/03/2017 04:00PM</td>
      		<td>XX123-ABC87-87634-7SD34</td><td class="text-center">Hub 1</td>
      		<td>George</td><td class="text-center">KA 03 ML 9865</td><td class="text-center">Preeti</td><td class="text-center">123 Street, ABC Lane, Bangalore</td></tr>
      		<tr class="text-center"><td>6</td><td class="text-center">Hub 1</td><td class="text-center">09/02/2017 10:00AM</td>
      		<td>XX123-ABC87-87634-7SD34</td><td class="text-center">Hub 1</td>
      		<td>David</td><td class="text-center">KA 03 ML 5666</td><td class="text-center">Pushpa</td><td class="text-center">123 Street, ABC Lane, Bangalore</td></tr>
      	</tbody>
      </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
		<!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0
			</div>
			<strong>Copyright &copy; 2017 <a href="http://tagbox.in">TagBox</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Control Sidebar -->
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->
	
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
	<script src="../../bootstrap/js/bootstrap.min.js"></script>
	<script src="../../dist/js/app.min.js"></script>
	<script src="../../dist/js/demo.js"></script>

	</body>
	
</html>
