<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>

<%
response.setHeader("Cache-Control","no-cache"); 
response.setHeader("Pragma","no-cache"); 
response.setDateHeader ("Expires", -1); 
%>
<%!
static final Logger logger = LoggerFactory.getLogger("HomePage");
%>
<%
String sUsername = (String) session.getAttribute("username");
if(sUsername == null || sUsername.equals("")) {
	session.setAttribute("callingPage", "HomePage.jsp");
	response.sendRedirect("../../index.html");
}
	java.util.Date dt = new java.util.Date();
	java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd-MM-yyyy HH:mm");
sdf.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
String currentTime = sdf.format(dt);
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Today's Cold Chain Performance</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- Add these below two lines for autocomplete and the javascript function at the end  -->
<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="../../bootstrap/css/bootstrap.css">
   
<!-- Font Awesome -->
<script src="https://use.fontawesome.com/37ba1872d2.js"></script>
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../../dist/css/tagbox.css">
<link href="../../dist/css/select2.css" rel="stylesheet" />
<script src="../../dist/js/select2.js"></script>
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
<link href="https://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
  body {
	font-family:  Lato, sans-serif;
	font-weight: 600;
}
</style>

</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
	<div class="wrapper">

		<header class="main-header">
			<!-- Logo -->
			<div>
				<a href="#" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
					<span class="logo-mini"><img src="../../dist/img/BigBasketLogo.png" style="margin-top:14px"
						alt="User Image"></span> <!-- logo for regular state and mobile devices -->
					<span class="logo-lg"><img src="../../dist/img/BigBasketLogo.png" 
						alt="User Image"></span>
				</a>
			</div>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">

				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
					role="button"> <span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
				</a>
				<label class="navbar-form pull-left" role="search">
					<div class="form-group">
						<b style="padding-right: 115px; font-size: 20px;color:#fff;" class="pull-right">Today's Cold Chain Performance</b><br> <b style="padding-right: 15px;color:#fff;"><%= currentTime %></b>
					</div>
				</label>

				<div class="navbar-custom-menu">
					<img src="../../dist/img/Logo.png" class="logo-class pull-right"
						alt="User Image">
				</div>
			</nav>

		</header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->


				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<br>
				<ul class="sidebar-menu">
					<li class="header">MAIN NAVIGATION</li>
						<li class="treeview active"><a href="#"> <i
							class="fa fa-home"></i> <span>Home</span>
					</a>
					<ul class="treeview-menu">
							<li class="active"><a href="HomePage.jsp"><i class="fa fa-circle-o"></i>
									Cold Chain<br> Dashboard</a></li>
							<li><a href="CityPage.jsp"><i class="fa fa-circle-o"></i>
									City Performance</a></li>
							<li><a href="RunningShipmentInfo.jsp"><i class="fa fa-circle-o"></i>
									Shipment Detail</a></li>
							<li><a href="MissingBoxesDC.jsp"><i class="fa fa-circle-o"></i>
									Missing Boxes <br>Dashboard</a></li>
							
						
									</ul>
									</li>
						<li class="treeview"><a href="#"> <i
							class="fa fa-bar-chart"></i> <span>Reports</span>
					</a>
					<ul class="treeview-menu">
							<li><a href="Reports.jsp"><i class="fa fa-circle-o"></i>
									Shipment Report</a></li>
									</ul>
									</li>
							
						
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper" style="background:white">
			<!-- Content Header (Page header) -->
			
		<section id="mainSection" class="content">
			<div class="well">
		<div class="row">
			<div class="col-md-4 text-center">
				<h4>TOTAL COLD CHAIN ORDERS TODAY<br><br><br><span data-toggle="tooltip" class="badge badge-big bb-color">3650</span></h4><i class="fa fa-caret-up"></i>&nbsp;5% THAN DAILY AVERAGE
			</div>
			<div class="col-md-4 text-center">
			<h4>% ORDERS DELIVERED AT RIGHT TEMPERATURE TODAY<br><br><span data-toggle="tooltip" class="badge badge-big bg-red">72%</span></h4><span class="description-percentage text-green"><i class="fa fa-caret-up"></i>&nbsp;2% THAN DAILY AVERAGE</span>
			</div>
			<div class="col-md-4 text-center">
			<h4>% ORDERS WITHOUT TEMPERATURE EXCURSIONS TODAY<br><br><span data-toggle="tooltip" class="badge badge-big bg-red">70%</span></h4><span class="description-percentage text-green"><i class="fa fa-caret-up"></i>&nbsp;1% THAN DAILY AVERAGE</span>
			</div>
		</div>
		</div>
			<div class="row text-center">
				<div class="col-md-6">
						<div class="row row-no-margin">
							<div class="col-md-12 bb-color">
								<h4>CITY PERFORMANCE TODAY</h4>
							</div>
						</div>
						<h4>% ORDERS DELIVERED AT RIGHT TEMPERATURE TODAY</h4>
						<div class="row">
							<div class="col-md-12">
								<div role="tabpanel">
									<ul class="nav nav-tabs" role="tablist">
										<li role="presentation" class="active"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">ALL</a></li>
										<li role="presentation"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">TIER 1</a></li>
										<li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">TIER 2</a></li>
									</ul>
									<div class="tab-content">
										<div role="tabpanel" class="tab-pane" id="home">
											<div class="row">
												<div class="col-md-4">
													<h4 style="color:green">&gt;= 95%</h4>
													<div class="well well-sm white-background text-center" style="border-color:green;border-width:3px">
														<div class="scroll-area1">
															<span class="pull-left"><a href="#">Hyderabad</a></span><b class="pull-right">98%&nbsp;&nbsp;</b><br><hr class="hr-no-padding">
															<span class="pull-left"><a href="#">Bangalore DC1</a></span><b class="pull-right">96%&nbsp;&nbsp;</b><br><hr class="hr-no-padding">
															<span class="pull-left"><a href="#">Chennai</a></span><b class="pull-right">95%&nbsp;&nbsp;</b><br><hr class="hr-no-padding">
															<span class="pull-left"><a href="#">Mumbai</a></span><b class="pull-right">95%&nbsp;&nbsp;</b><br><hr class="hr-no-padding">
														</div>
													</div>
												</div>
												<div class="col-md-4">
													<h4 style="color:#ff851b">75-95%</h4>
													<div class="well well-sm white-background text-center" style="border-color:orange;border-width:3px">
														<div class="scroll-area1">
															<span class="pull-left"><a href="#">Pune</a></span><b class="pull-right">92%&nbsp;&nbsp;</b><br><hr class="hr-no-padding">
															<span class="pull-left"><a href="#">Delhi</a></span><b class="pull-right">90%&nbsp;&nbsp;</b><br><hr class="hr-no-padding">
															<span class="pull-left"><a href="#">Noida</a></span><b class="pull-right">89%&nbsp;&nbsp;</b><br><hr class="hr-no-padding">
														</div>
													</div>
												</div>
												<div class="col-md-4">
													<h4 style="color:red">&lt; 75%</h4>
													<div class="well well-sm white-background text-center" style="border-color:red;border-width:3px">
														<div class="scroll-area1">
															<span class="pull-left"><a href="#">Ahmedabad</a></span><b class="pull-right">70%&nbsp;&nbsp;</b><br><hr class="hr-no-padding">
															<span class="pull-left"><a href="#">Bangalore DC2</a></span><b class="pull-right">95%&nbsp;&nbsp;</b><br><hr class="hr-no-padding">
															<span class="pull-left"><a href="#">Kolkata</a></span><b class="pull-right">60%&nbsp;&nbsp;</b><br><hr class="hr-no-padding">
														</div>
													</div>
												</div>
											</div>
										</div>
										<div role="tabpanel" class="tab-pane" id="profile">
											<div class="row">
												<div class="col-md-4">
													<h4 style="color:green">&gt;= 95%</h4>
													<div class="well well-sm white-background text-center" style="border-color:green;border-width:3px">
														<div class="scroll-area1">
															<span class="pull-left"><a href="#">Agra</a></span><b class="pull-right">98%&nbsp;&nbsp;</b><br><hr class="hr-no-padding">
															<span class="pull-left"><a href="#">Ahmedabad</a></span><b class="pull-right">96%&nbsp;&nbsp;</b><br><hr class="hr-no-padding">
															<span class="pull-left"><a href="#">Baroda</a></span><b class="pull-right">95%&nbsp;&nbsp;</b><br><hr class="hr-no-padding">
														</div>
													</div>
												</div>
												<div class="col-md-4">
													<h4 style="color:#ff851b">75-95%</h4>
													<div class="well well-sm white-background text-center" style="border-color:orange;border-width:3px">
														<div class="scroll-area1">
															<span class="pull-left"><a href="#">Bhopal</a></span><b class="pull-right">92%&nbsp;&nbsp;</b><br><hr class="hr-no-padding">
															<span class="pull-left"><a href="#">Jaipur</a></span><b class="pull-right">90%&nbsp;&nbsp;</b><br><hr class="hr-no-padding">
															<span class="pull-left"><a href="#">Gurgaon</a></span><b class="pull-right">89%&nbsp;&nbsp;</b><br><hr class="hr-no-padding">
														</div>
													</div>
												</div>
												<div class="col-md-4">
													<h4 style="color:red">&lt; 75%</h4>
													<div class="well well-sm white-background text-center" style="border-color:red;border-width:3px">
														<div class="scroll-area1">
															<span class="pull-left"><a href="#">Tirupati</a></span><b class="pull-right">70%&nbsp;&nbsp;</b><br><hr class="hr-no-padding">
															<span class="pull-left"><a href="#">Ranchi</a></span><b class="pull-right">65%&nbsp;&nbsp;</b><br><hr class="hr-no-padding">
															<span class="pull-left"><a href="#">Patna</a></span><b class="pull-right">60%&nbsp;&nbsp;</b><br><hr class="hr-no-padding">
														</div>
													</div>
												</div>
											</div>
										</div>
										<div role="tabpanel" class="tab-pane active" id="messages">
											<div class="row">
												<div class="col-md-4">
													<h4 style="color:green">&gt;= 95%</h4>
													<div class="well well-sm white-background text-center" style="border-color:green;border-width:3px">
														<div class="scroll-area1">
															<span class="pull-left"><a href="#">Hyderabad</a></span><b class="pull-right">98%&nbsp;&nbsp;</b><br><hr class="hr-no-padding">
															<span class="pull-left"><a href="CityPage.jsp" style="text-decoration:underline;color:blue">Bangalore</a></span><b class="pull-right">96%&nbsp;&nbsp;</b><br><hr class="hr-no-padding">
															<span class="pull-left"><a href="#">Chennai</a></span><b class="pull-right">95%&nbsp;&nbsp;</b><br><hr class="hr-no-padding">
															<span class="pull-left"><a href="#">Mumbai</a></span><b class="pull-right">95%&nbsp;&nbsp;</b><br><hr class="hr-no-padding">
														</div>
													</div>
												</div>
												<div class="col-md-4">
													<h4 style="color:#ff851b">75-95%</h4>
													<div class="well well-sm white-background text-center" style="border-color:orange;border-width:3px">
														<div class="scroll-area1">
															<span class="pull-left"><a href="#">Pune</a></span><b class="pull-right">92%&nbsp;&nbsp;</b><br><hr class="hr-no-padding">
															<span class="pull-left"><a href="#">Delhi</a></span><b class="pull-right">90%&nbsp;&nbsp;</b><br><hr class="hr-no-padding">
															<span class="pull-left"><a href="#">Noida</a></span><b class="pull-right">89%&nbsp;&nbsp;</b><br><hr class="hr-no-padding">
														</div>
													</div>
												</div>
												<div class="col-md-4">
													<h4 style="color:red">&lt; 75%</h4>
													<div class="well well-sm white-background text-center" style="border-color:red;border-width:3px">
														<div class="scroll-area1">
															<span class="pull-left"><a href="#">Ahmedabad</a></span><b class="pull-right">70%&nbsp;&nbsp;</b><br><hr class="hr-no-padding">
															<span class="pull-left"><a href="#">Bangalore</a></span><b class="pull-right">95%&nbsp;&nbsp;</b><br><hr class="hr-no-padding">
															<span class="pull-left"><a href="#">Kolkata</a></span><b class="pull-right">60%&nbsp;&nbsp;</b><br><hr class="hr-no-padding">
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<a href="#" data-toggle="modal" data-target="#CityTrends" style="text-decoration:underline;color:blue">View All Cities Performance Trends</a>
						<br>
					</div>
				<div class="col-md-6">
				<div class="row row-no-margin">
						<div class="col-md-12 bb-color">
							<h4>PRODUCT CATEGORY PERFORMANCE TODAY</h4>
						</div>
					</div>
					<br>
					<table class="table table-bordered table-striped">
      	<thead><tr class="text-center info">
      		<th class="text-center alert-font-small" style="width:20%">Product Category</th><th class="text-center alert-font-small" style="width:40%">% Orders Delivered At Right Temperature Today</th><th class="text-center alert-font-small" style="width:40%">% Orders Without Temperature Excursions Today</th></tr>
      	</thead>
      	<tbody>
      		<tr class="text-center"><td><a href="#" data-toggle="modal" data-target="#myModal1" style="text-decoration:underline;color:blue">CFV</a></td><td><div class="progress">
  <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="72" aria-valuemin="0" aria-valuemax="100" style="width: 72%;">
    72%
  </div>
</div></td><td><div class="progress">
  <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%;">
    70%
  </div>
</div></td></tr>
      		<tr class="text-center"><td><a href="#">CV</a></td><td><div class="progress">
  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="98" aria-valuemin="0" aria-valuemax="100" style="width: 98%;">
    98%
  </div>
</div></td><td><div class="progress">
  <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 85%;">
    85%
  </div>
</div></td></tr>
      		<tr class="text-center"><td><a href="#">CNV</a></td><td><div class="progress">
  <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100" style="width: 65%;">
    65%
  </div>
</div></td><td><div class="progress">
  <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="87" aria-valuemin="0" aria-valuemax="100" style="width: 87%;">
    87%
  </div>
</div></td></tr>
      		<tr class="text-center"><td><a href="#">FV</a></td><td><div class="progress">
  <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="87" aria-valuemin="0" aria-valuemax="100" style="width: 87%;">
    87%
  </div>
</div></td><td><div class="progress">
  <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="88" aria-valuemin="0" aria-valuemax="100" style="width: 88%;">
    88%
  </div>
</div></td></tr>
      		<tr class="text-center"><td><a href="#">FNV</a></td><td><div class="progress">
  <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="78" aria-valuemin="0" aria-valuemax="100" style="width: 78%;">
    78%
  </div>
</div></td><td><div class="progress">
  <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%;">
    70%
  </div>
</div></td></tr>
      	</tbody>
      </table>
					<a href="#" data-toggle="modal" data-target="#ProductTrends" style="text-decoration:underline;color:blue">View All Product Categories Performance Trends</a>
				</div>
				
			</div><br>
			<div class="row">
				<div class="col-md-12">
					<div class="well white-background">
						<div class="row row-no-margin">
						<div class="text-center col-md-12 bb-color">
							<h4>COLD BOX COUNT SUMMARY</h4>
							</div></div><br>
							<div class="row text-center"><div class="col-md-12 text-center">
							<table class="table table-bordered table-striped">
      	<thead><tr class="text-center info">
      		<th class="text-center alert-font-small">Total Cold Boxes Inventory</th><th class="text-center alert-font-small">Missing Cold Boxes Till Date</th><th class="text-center alert-font-small">Missing Cold Boxes In Past Week</th></tr>
      	</thead>
      	<tbody>
      	<tr><td><span class="success-text alert-font">3650</span></td><td><span class="alert-text alert-font">520</span></td><td><span class="alert-text alert-font">151</span></td></tr>
      	</tbody>
      </table>
      <div class="row text-center">
				<div class="col-md-12">
					<div class="row row-no-margin">
						<div class="col-md-12 bb-color">
							<h4>MISSING COLD BOXES IN PAST WEEK ACROSS CITIES</h4>
						</div>
					</div>
					<br>
					<div class="row">
					<div class="col-md-12">
      <div class="chart">
						<canvas id="chart35"
							style="height: 300px; width: 100px"></canvas>
					</div>
					</div></div>
				</div>
				
				
			</div>
      	
      </div></div>
						</div>
					</div>
					</div>
				
	</section>
	</div></div>
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">City Details</h4>
      </div>
      <div class="modal-body">
      <table class="table table-bordered table-black-border">
      	<thead><tr class="text-center bb-color">
      		<th class="text-center" style="width:25%">Location</th><th class="text-center" style="width:45%">% Orders Delivered At Right Temperature Today</th><th style="width:30%">&nbsp;</th></tr>
      	</thead>
      	<tbody>
      		<tr class="warning"><td>Bangalore</td><td class="text-center text-green"><h4>96%</h4></td><td class="text-center"><a href="RunningShipmentInfo.jsp" class="btn btn-warning btn-sm" style="width:60%">View Orders</a></td></tr>
      		<tr class="success"><td>&nbsp;&nbsp;&nbsp;DC 1</td><td class="text-center text-green"><h4>98%</h4></td><td class="text-center"><a href="RunningShipmentInfo.jsp" class="btn btn-warning btn-sm" style="width:60%">View Orders</a></td></tr>
      		<tr class="info"><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Hub 1</td><td class="text-center text-green"><h4>97%</h4></td><td class="text-center"><a href="RunningShipmentInfo.jsp" class="btn btn-warning btn-sm" style="width:60%">View Orders</a></td></tr>
      		<tr class="info"><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Hub 2</td><td class="text-center text-green"><h4>99%</h4></td><td class="text-center"><a href="RunningShipmentInfo.jsp" class="btn btn-warning btn-sm" style="width:60%">View Orders</a></td></tr>
      		<tr class="success"><td>&nbsp;&nbsp;&nbsp;DC 2</td><td class="text-center text-green"><h4>94%</h4></td><td class="text-center"><a href="RunningShipmentInfo.jsp" class="btn btn-warning btn-sm" style="width:60%">View Orders</a></td></tr>
      		<tr class="info"><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Hub 3</td><td class="text-center text-green"><h4>94%</h4></td><td class="text-center"><a href="RunningShipmentInfo.jsp" class="btn btn-warning btn-sm" style="width:60%">View Orders</a></td></tr>
      		
      	</tbody>
      </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Product Category Details</h4>
      </div>
      <div class="modal-body">
      <table class="table table-bordered table-black-border">
      	<thead><tr class="text-center bb-color">
      		<th class="text-center" style="width:25%">Location</th><th class="text-center" style="width:45%">% Orders Delivered At Right Temperature Today</th><th style="width:30%">&nbsp;</th></tr>
      	</thead>
      	<tbody>
      	<tr class="warning"><td>CFV</td><td class="text-center"><h4 style="color:red">72%</h4></td><td class="text-center"><a href="RunningShipmentInfo.jsp" class="btn btn-warning btn-sm" style="width:60%">View Orders</a></td></tr>
      		<tr class="info"><td>&nbsp;&nbsp;&nbsp;Bangalore</td><td class="text-center"><h4 style="color:red">72%</h4></td><td class="text-center"><a href="RunningShipmentInfo.jsp" class="btn btn-warning btn-sm" style="width:60%">View Orders</a></td></tr>
      		<tr class="info"><td>&nbsp;&nbsp;&nbsp;Pune</td><td class="text-center"><h4 style="color:red">71%</h4></td><td class="text-center"><a href="RunningShipmentInfo.jsp" class="btn btn-warning btn-sm" style="width:60%">View Orders</a></td></tr>
      		<tr class="info"><td>&nbsp;&nbsp;&nbsp;Chennai</td><td class="text-center"><h4 style="color:red">73%</h4></td><td class="text-center"><a href="RunningShipmentInfo.jsp" class="btn btn-warning btn-sm" style="width:60%">View Orders</a></td></tr>
      		
      	</tbody>
      </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
    
    <div class="modal fade" tabindex="-1" role="dialog" id="CityTrends">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">City Performance Trends</h4>
      </div>
      <div class="modal-body text-center">
      <span class="fa-color-blue">% ORDERS DELIVERED AT RIGHT TEMPERATURE IN THE PAST WEEK</span>
		<div class="row">
			<div class="col-md-4">
		<div class="box" style="border-top-color:#fff">
			<div class="box-header with-border bb-color">
				&nbsp;Agartala
			</div>
			<div class="box-body">
				<div class="chart">
						<canvas id="chart1"
							style="height: 175px; width: 100px"></canvas>
					</div>
			</div>
		</div>
			</div>
			<div class="col-md-4">
		<div class="box" style="border-top-color:#fff">
			<div class="box-header with-border bb-color">
				&nbsp;Agra
			</div>
			<div class="box-body">
				<div class="chart">
						<canvas id="chart2"
							style="height: 175px; width: 100px"></canvas>
					</div>
			</div>
		</div>
			</div>
			<div class="col-md-4">
		<div class="box" style="border-top-color:#fff">
			<div class="box-header with-border bb-color">
				&nbsp;Ahmedabad
			</div>
			<div class="box-body">
				<div class="chart">
						<canvas id="chart3"
							style="height: 175px; width: 100px"></canvas>
					</div>
			</div>
		</div>
			</div>
		</div>
		<div class="row">
		<div class="col-md-4">
		<div class="box" style="border-top-color:#fff">
			<div class="box-header with-border bb-color">
				&nbsp;Allahabad
			</div>
			<div class="box-body">
				<div class="chart">
						<canvas id="chart4"
							style="height: 175px; width: 100px"></canvas>
					</div>
			</div>
		</div>
			</div>
			<div class="col-md-4">
		<div class="box" style="border-top-color:#fff">
			<div class="box-header with-border bb-color">
				&nbsp;Amritsar
			</div>
			<div class="box-body">
				<div class="chart">
						<canvas id="chart5"
							style="height: 175px; width: 100px"></canvas>
					</div>
			</div>
		</div>
			</div>
			<div class="col-md-4">
		<div class="box" style="border-top-color:#fff">
			<div class="box-header with-border bb-color">
				&nbsp;Bangalore
			</div>
			<div class="box-body">
				<div class="chart">
						<canvas id="chart6"
							style="height: 175px; width: 100px"></canvas>
					</div>
			</div>
		</div>
			</div>
			
			
		</div>
		<div class="row">
		<div class="col-md-4">
		<div class="box" style="border-top-color:#fff">
			<div class="box-header with-border bb-color">
				&nbsp;Baroda
			</div>
			<div class="box-body">
				<div class="chart">
						<canvas id="chart7"
							style="height: 175px; width: 100px"></canvas>
					</div>
			</div>
		</div>
			</div>
			<div class="col-md-4">
		<div class="box" style="border-top-color:#fff">
			<div class="box-header with-border bb-color">
				&nbsp;Bhopal
			</div>
			<div class="box-body">
				<div class="chart">
						<canvas id="chart8"
							style="height: 175px; width: 100px"></canvas>
					</div>
			</div>
		</div>
			</div>
			<div class="col-md-4">
		<div class="box" style="border-top-color:#fff">
			<div class="box-header with-border bb-color">
				&nbsp;Chennai
			</div>
			<div class="box-body">
				<div class="chart">
						<canvas id="chart9"
							style="height: 175px; width: 100px"></canvas>
					</div>
			</div>
		</div>
			</div>
			</div>
			<div class="row">
			<div class="col-md-4">
		<div class="box" style="border-top-color:#fff">
			<div class="box-header with-border bb-color">
				&nbsp;Coimbatore
			</div>
			<div class="box-body">
				<div class="chart">
						<canvas id="chart10"
							style="height: 175px; width: 100px"></canvas>
					</div>
			</div>
		</div>
			</div>
			<div class="col-md-4">
		<div class="box" style="border-top-color:#fff">
			<div class="box-header with-border bb-color">
				&nbsp;Delhi
			</div>
			<div class="box-body">
				<div class="chart">
						<canvas id="chart11"
							style="height: 175px; width: 100px"></canvas>
					</div>
			</div>
		</div>
			</div>
			<div class="col-md-4">
		<div class="box" style="border-top-color:#fff">
			<div class="box-header with-border bb-color">
				&nbsp;Gurgaon
			</div>
			<div class="box-body">
				<div class="chart">
						<canvas id="chart12"
							style="height: 175px; width: 100px"></canvas>
					</div>
			</div>
		</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
		<div class="box" style="border-top-color:#fff">
			<div class="box-header with-border bb-color">
				&nbsp;Hyderabad
			</div>
			<div class="box-body">
				<div class="chart">
						<canvas id="chart13"
							style="height: 175px; width: 100px"></canvas>
					</div>
			</div>
		</div>
			</div>
			<div class="col-md-4">
		<div class="box" style="border-top-color:#fff">
			<div class="box-header with-border bb-color">
				&nbsp;Indore
			</div>
			<div class="box-body">
				<div class="chart">
						<canvas id="chart14"
							style="height: 175px; width: 100px"></canvas>
					</div>
			</div>
		</div>
			</div>
			<div class="col-md-4">
		<div class="box" style="border-top-color:#fff">
			<div class="box-header with-border bb-color">
				&nbsp;Jaipur
			</div>
			<div class="box-body">
				<div class="chart">
						<canvas id="chart15"
							style="height: 175px; width: 100px"></canvas>
					</div>
			</div>
		</div>
			</div>
		</div>
		<div class="row">
		<div class="col-md-4">
		<div class="box" style="border-top-color:#fff">
			<div class="box-header with-border bb-color">
				&nbsp;Kanpur
			</div>
			<div class="box-body">
				<div class="chart">
						<canvas id="chart16"
							style="height: 175px; width: 100px"></canvas>
					</div>
			</div>
		</div>
			</div>
			<div class="col-md-4">
		<div class="box" style="border-top-color:#fff">
			<div class="box-header with-border bb-color">
				&nbsp;Kolkata
			</div>
			<div class="box-body">
				<div class="chart">
						<canvas id="chart17"
							style="height: 175px; width: 100px"></canvas>
					</div>
			</div>
		</div>
			</div>
			<div class="col-md-4">
		<div class="box" style="border-top-color:#fff">
			<div class="box-header with-border bb-color">
				&nbsp;Ludhiana
			</div>
			<div class="box-body">
				<div class="chart">
						<canvas id="chart18"
							style="height: 175px; width: 100px"></canvas>
					</div>
			</div>
		</div>
			</div>
			</div>
			<div class="row">
			<div class="col-md-4">
		<div class="box" style="border-top-color:#fff">
			<div class="box-header with-border bb-color">
				&nbsp;Lucknow
			</div>
			<div class="box-body">
				<div class="chart">
						<canvas id="chart19"
							style="height: 175px; width: 100px"></canvas>
					</div>
			</div>
		</div>
			</div>
			<div class="col-md-4">
		<div class="box" style="border-top-color:#fff">
			<div class="box-header with-border bb-color">
				&nbsp;Mumbai
			</div>
			<div class="box-body">
				<div class="chart">
						<canvas id="chart20"
							style="height: 175px; width: 100px"></canvas>
					</div>
			</div>
		</div>
			</div>
			<div class="col-md-4">
		<div class="box" style="border-top-color:#fff">
			<div class="box-header with-border bb-color">
				&nbsp;Mysore
			</div>
			<div class="box-body">
				<div class="chart">
						<canvas id="chart21"
							style="height: 175px; width: 100px"></canvas>
					</div>
			</div>
		</div>
			</div>
			</div>
			<div class="row">
			<div class="col-md-4">
		<div class="box" style="border-top-color:#fff">
			<div class="box-header with-border bb-color">
				&nbsp;Patna
			</div>
			<div class="box-body">
				<div class="chart">
						<canvas id="chart22"
							style="height: 175px; width: 100px"></canvas>
					</div>
			</div>
		</div>
			</div>
			<div class="col-md-4">
		<div class="box" style="border-top-color:#fff">
			<div class="box-header with-border bb-color">
				&nbsp;Pune
			</div>
			<div class="box-body">
				<div class="chart">
						<canvas id="chart23"
							style="height: 175px; width: 100px"></canvas>
					</div>
			</div>
		</div>
			</div>
			<div class="col-md-4">
		<div class="box" style="border-top-color:#fff">
			<div class="box-header with-border bb-color">
				&nbsp;Ranchi
			</div>
			<div class="box-body">
				<div class="chart">
						<canvas id="chart24"
							style="height: 175px; width: 100px"></canvas>
					</div>
			</div>
		</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
		<div class="box" style="border-top-color:#fff">
			<div class="box-header with-border bb-color">
				&nbsp;Surat
			</div>
			<div class="box-body">
				<div class="chart">
						<canvas id="chart25"
							style="height: 175px; width: 100px"></canvas>
					</div>
			</div>
		</div>
			</div>
			<div class="col-md-4">
		<div class="box" style="border-top-color:#fff">
			<div class="box-header with-border bb-color">
				&nbsp;Tirupati
			</div>
			<div class="box-body">
				<div class="chart">
						<canvas id="chart26"
							style="height: 175px; width: 100px"></canvas>
					</div>
			</div>
		</div>
			</div>
			<div class="col-md-4">
		<div class="box" style="border-top-color:#fff">
			<div class="box-header with-border bb-color">
				&nbsp;Udaipur
			</div>
			<div class="box-body">
				<div class="chart">
						<canvas id="chart27"
							style="height: 175px; width: 100px"></canvas>
					</div>
			</div>
		</div>
			</div>
			</div>
			<div class="row">
			<div class="col-md-4">
		<div class="box" style="border-top-color:#fff">
			<div class="box-header with-border bb-color">
				&nbsp;Vijayawada
			</div>
			<div class="box-body">
				<div class="chart">
						<canvas id="chart28"
							style="height: 175px; width: 100px"></canvas>
					</div>
			</div>
		</div>
			</div>
			<div class="col-md-4">
		<div class="box" style="border-top-color:#fff">
			<div class="box-header with-border bb-color">
				&nbsp;Vishakhapatnam
			</div>
			<div class="box-body">
				<div class="chart">
						<canvas id="chart29"
							style="height: 175px; width: 100px"></canvas>
					</div>
			</div>
		</div>
			</div>
			
		</div>
		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" tabindex="-1" role="dialog" id="ProductTrends">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Product Category Performance Trends</h4>
      </div>
      <div class="modal-body text-center">
      <span class="fa-color-blue">% ORDERS DELIVERED AT RIGHT TEMPERATURE IN THE PAST WEEK</span>
		<div class="row">
			<div class="col-md-4">
		<div class="box" style="border-top-color:#fff">
			<div class="box-header with-border bb-color">
				&nbsp;CFV
			</div>
			<div class="box-body">
				<div class="chart">
						<canvas id="chart30"
							style="height: 175px; width: 100px"></canvas>
					</div>
			</div>
		</div>
			</div>
			<div class="col-md-4">
		<div class="box" style="border-top-color:#fff">
			<div class="box-header with-border bb-color">
				&nbsp;CV
			</div>
			<div class="box-body">
				<div class="chart">
						<canvas id="chart31"
							style="height: 175px; width: 100px"></canvas>
					</div>
			</div>
		</div>
			</div>
			<div class="col-md-4">
		<div class="box" style="border-top-color:#fff">
			<div class="box-header with-border bb-color">
				&nbsp;CNV
			</div>
			<div class="box-body">
				<div class="chart">
						<canvas id="chart32"
							style="height: 175px; width: 100px"></canvas>
					</div>
			</div>
		</div>
			</div>
		</div>
		<div class="row">
		<div class="col-md-4">
		<div class="box" style="border-top-color:#fff">
			<div class="box-header with-border bb-color">
				&nbsp;FV
			</div>
			<div class="box-body">
				<div class="chart">
						<canvas id="chart33"
							style="height: 175px; width: 100px"></canvas>
					</div>
			</div>
		</div>
			</div>
			<div class="col-md-4">
		<div class="box" style="border-top-color:#fff">
			<div class="box-header with-border bb-color">
				&nbsp;FNV
			</div>
			<div class="box-body">
				<div class="chart">
						<canvas id="chart34"
							style="height: 175px; width: 100px"></canvas>
					</div>
			</div>
		</div>
			</div>
			</div>
    </div>
		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
				
		<!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0
			</div>
			<strong>Copyright &copy; 2017 <a href="http://tagbox.in">TagBox</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Control Sidebar -->
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->
	
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
	<script src="../../bootstrap/js/bootstrap.min.js"></script>
	<script src="../../dist/js/app.min.js"></script>
	<script src="../../dist/js/demo.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
	<script>

Chart.defaults.global.legend.display = false;


var barOptions_stacked = {
	    tooltips: {
	        enabled: true
	    },
	    legend:{
	        display:false
	    },
	    elements: {
            point:{
                radius: 1
            }
        },
	    scales: {
	        xAxes: [{
	        	scaleLabel: {
					labelString: 'Date',
					display: true
		    	},
	                    gridLines: {
	                        display:false
	                    },
	                    ticks: {
	                        autoSkip: true,
	                        maxTicksLimit: 20
	                    }
	                }],
	        yAxes: [{
	        	scaleLabel: {
					labelString: '% Orders',
					display: true
		    	},
	                    gridLines: {
	                        display:false
	                    },
	        			ticks: {
	        				autoSkip: true,
	                        maxTicksLimit: 20
	        			}
	                }]
	        },
	        fill: false,
            lineTension: 0,
            backgroundColor: "rgba(75,192,192,0.4)",
            borderColor: "rgba(75,192,192,1)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            spanGaps: false,
	};
	
	var ctx2 = document.getElementById("chart1");
	var myChart2 = new Chart(ctx2, {
	    type: 'line',
	    data: {
	    	labels : ['02-Aug', '03-Aug', '04-Aug', '05-Aug', '06-Aug', '07-Aug', '08-Aug'],
			datasets : [ {
				label : ["% Orders Delivered"],
				fill: false,
				borderColor: "black",
				data : ['75', '73', '72', '70', '76', '78', '73']
			}]
	    },
	    options: barOptions_stacked
	});
	var ctx2 = document.getElementById("chart2");
	var myChart2 = new Chart(ctx2, {
	    type: 'line',
	    data: {
	    	labels : ['02-Aug', '03-Aug', '04-Aug', '05-Aug', '06-Aug', '07-Aug', '08-Aug'],
			datasets : [ {
				label : ["% Orders Delivered"],
				fill: false,
				borderColor: "black",
				data : ['70', '73', '75', '70', '76', '78', '75']
			}]
	    },
	    options: barOptions_stacked
	});
	var ctx2 = document.getElementById("chart3");
	var myChart2 = new Chart(ctx2, {
	    type: 'line',
	    data: {
	    	labels : ['02-Aug', '03-Aug', '04-Aug', '05-Aug', '06-Aug', '07-Aug', '08-Aug'],
			datasets : [ {
				label : ["% Orders Delivered"],
				fill: false,
				borderColor: "black",
				data : ['72', '71', '72', '70', '76', '70', '73']
			}]
	    },
	    options: barOptions_stacked
	});
	var ctx2 = document.getElementById("chart4");
	var myChart2 = new Chart(ctx2, {
	    type: 'line',
	    data: {
	    	labels : ['02-Aug', '03-Aug', '04-Aug', '05-Aug', '06-Aug', '07-Aug', '08-Aug'],
			datasets : [ {
				label : ["% Orders Delivered"],
				fill: false,
				borderColor: "black",
				data : ['71', '75', '72', '70', '79', '78', '73']
			}]
	    },
	    options: barOptions_stacked
	});
	var ctx2 = document.getElementById("chart5");
	var myChart2 = new Chart(ctx2, {
	    type: 'line',
	    data: {
	    	labels : ['02-Aug', '03-Aug', '04-Aug', '05-Aug', '06-Aug', '07-Aug', '08-Aug'],
			datasets : [ {
				label : ["% Orders Delivered"],
				fill: false,
				borderColor: "black",
				data : ['75', '74', '72', '73', '76', '79', '73']
			}]
	    },
	    options: barOptions_stacked
	});
	var ctx2 = document.getElementById("chart6");
	var myChart2 = new Chart(ctx2, {
	    type: 'line',
	    data: {
	    	labels : ['02-Aug', '03-Aug', '04-Aug', '05-Aug', '06-Aug', '07-Aug', '08-Aug'],
			datasets : [ {
				label : ["% Orders Delivered"],
				fill: false,
				borderColor: "black",
				data : ['75', '73', '72', '70', '76', '78', '73']
			}]
	    },
	    options: barOptions_stacked
	});
	var ctx2 = document.getElementById("chart7");
	var myChart2 = new Chart(ctx2, {
	    type: 'line',
	    data: {
	    	labels : ['02-Aug', '03-Aug', '04-Aug', '05-Aug', '06-Aug', '07-Aug', '08-Aug'],
			datasets : [ {
				label : ["% Orders Delivered"],
				fill: false,
				borderColor: "black",
				data : ['75', '73', '72', '70', '76', '78', '73']
			}]
	    },
	    options: barOptions_stacked
	});
	var ctx2 = document.getElementById("chart8");
	var myChart2 = new Chart(ctx2, {
	    type: 'line',
	    data: {
	    	labels : ['02-Aug', '03-Aug', '04-Aug', '05-Aug', '06-Aug', '07-Aug', '08-Aug'],
			datasets : [ {
				label : ["% Orders Delivered"],
				fill: false,
				borderColor: "black",
				data : ['75', '73', '72', '70', '76', '78', '73']
			}]
	    },
	    options: barOptions_stacked
	});
	var ctx2 = document.getElementById("chart9");
	var myChart2 = new Chart(ctx2, {
	    type: 'line',
	    data: {
	    	labels : ['02-Aug', '03-Aug', '04-Aug', '05-Aug', '06-Aug', '07-Aug', '08-Aug'],
			datasets : [ {
				label : ["% Orders Delivered"],
				fill: false,
				borderColor: "black",
				data : ['75', '73', '72', '70', '76', '78', '73']
			}]
	    },
	    options: barOptions_stacked
	});
	var ctx2 = document.getElementById("chart10");
	var myChart2 = new Chart(ctx2, {
	    type: 'line',
	    data: {
	    	labels : ['02-Aug', '03-Aug', '04-Aug', '05-Aug', '06-Aug', '07-Aug', '08-Aug'],
			datasets : [ {
				label : ["% Orders Delivered"],
				fill: false,
				borderColor: "black",
				data : ['75', '73', '72', '70', '76', '78', '73']
			}]
	    },
	    options: barOptions_stacked
	});
	var ctx2 = document.getElementById("chart11");
	var myChart2 = new Chart(ctx2, {
	    type: 'line',
	    data: {
	    	labels : ['02-Aug', '03-Aug', '04-Aug', '05-Aug', '06-Aug', '07-Aug', '08-Aug'],
			datasets : [ {
				label : ["% Orders Delivered"],
				fill: false,
				borderColor: "black",
				data : ['75', '73', '72', '70', '76', '78', '73']
			}]
	    },
	    options: barOptions_stacked
	});
	var ctx2 = document.getElementById("chart12");
	var myChart2 = new Chart(ctx2, {
	    type: 'line',
	    data: {
	    	labels : ['02-Aug', '03-Aug', '04-Aug', '05-Aug', '06-Aug', '07-Aug', '08-Aug'],
			datasets : [ {
				label : ["% Orders Delivered"],
				fill: false,
				borderColor: "black",
				data : ['75', '73', '72', '70', '76', '78', '73']
			}]
	    },
	    options: barOptions_stacked
	});
	var ctx2 = document.getElementById("chart13");
	var myChart2 = new Chart(ctx2, {
	    type: 'line',
	    data: {
	    	labels : ['02-Aug', '03-Aug', '04-Aug', '05-Aug', '06-Aug', '07-Aug', '08-Aug'],
			datasets : [ {
				label : ["% Orders Delivered"],
				fill: false,
				borderColor: "black",
				data : ['75', '73', '72', '70', '76', '78', '73']
			}]
	    },
	    options: barOptions_stacked
	});
	var ctx2 = document.getElementById("chart14");
	var myChart2 = new Chart(ctx2, {
	    type: 'line',
	    data: {
	    	labels : ['02-Aug', '03-Aug', '04-Aug', '05-Aug', '06-Aug', '07-Aug', '08-Aug'],
			datasets : [ {
				label : ["% Orders Delivered"],
				fill: false,
				borderColor: "black",
				data : ['75', '73', '72', '70', '76', '78', '73']
			}]
	    },
	    options: barOptions_stacked
	});
	var ctx2 = document.getElementById("chart15");
	var myChart2 = new Chart(ctx2, {
	    type: 'line',
	    data: {
	    	labels : ['02-Aug', '03-Aug', '04-Aug', '05-Aug', '06-Aug', '07-Aug', '08-Aug'],
			datasets : [ {
				label : ["% Orders Delivered"],
				fill: false,
				borderColor: "black",
				data : ['75', '73', '72', '70', '76', '78', '73']
			}]
	    },
	    options: barOptions_stacked
	});
	var ctx2 = document.getElementById("chart16");
	var myChart2 = new Chart(ctx2, {
	    type: 'line',
	    data: {
	    	labels : ['02-Aug', '03-Aug', '04-Aug', '05-Aug', '06-Aug', '07-Aug', '08-Aug'],
			datasets : [ {
				label : ["% Orders Delivered"],
				fill: false,
				borderColor: "black",
				data : ['75', '73', '72', '70', '76', '78', '73']
			}]
	    },
	    options: barOptions_stacked
	});
	var ctx2 = document.getElementById("chart17");
	var myChart2 = new Chart(ctx2, {
	    type: 'line',
	    data: {
	    	labels : ['02-Aug', '03-Aug', '04-Aug', '05-Aug', '06-Aug', '07-Aug', '08-Aug'],
			datasets : [ {
				label : ["% Orders Delivered"],
				fill: false,
				borderColor: "black",
				data : ['75', '73', '72', '70', '76', '78', '73']
			}]
	    },
	    options: barOptions_stacked
	});
	var ctx2 = document.getElementById("chart18");
	var myChart2 = new Chart(ctx2, {
	    type: 'line',
	    data: {
	    	labels : ['02-Aug', '03-Aug', '04-Aug', '05-Aug', '06-Aug', '07-Aug', '08-Aug'],
			datasets : [ {
				label : ["% Orders Delivered"],
				fill: false,
				borderColor: "black",
				data : ['50', '54', '34', '28', '30', '44', '35']
			}]
	    },
	    options: barOptions_stacked
	});
	var ctx2 = document.getElementById("chart19");
	var myChart2 = new Chart(ctx2, {
	    type: 'line',
	    data: {
	    	labels : ['02-Aug', '03-Aug', '04-Aug', '05-Aug', '06-Aug', '07-Aug', '08-Aug'],
			datasets : [ {
				label : ["% Orders Delivered"],
				fill: false,
				borderColor: "black",
				data : ['50', '54', '34', '28', '30', '44', '35']
			}]
	    },
	    options: barOptions_stacked
	});
	var ctx2 = document.getElementById("chart20");
	var myChart2 = new Chart(ctx2, {
	    type: 'line',
	    data: {
	    	labels : ['02-Aug', '03-Aug', '04-Aug', '05-Aug', '06-Aug', '07-Aug', '08-Aug'],
			datasets : [ {
				label : ["% Orders Delivered"],
				fill: false,
				borderColor: "black",
				data : ['50', '54', '34', '28', '30', '44', '35']
			}]
	    },
	    options: barOptions_stacked
	});
	var ctx2 = document.getElementById("chart21");
	var myChart2 = new Chart(ctx2, {
	    type: 'line',
	    data: {
	    	labels : ['02-Aug', '03-Aug', '04-Aug', '05-Aug', '06-Aug', '07-Aug', '08-Aug'],
			datasets : [ {
				label : ["% Orders Delivered"],
				fill: false,
				borderColor: "black",
				data : ['50', '54', '34', '28', '30', '44', '35']
			}]
	    },
	    options: barOptions_stacked
	});
	var ctx2 = document.getElementById("chart22");
	var myChart2 = new Chart(ctx2, {
	    type: 'line',
	    data: {
	    	labels : ['02-Aug', '03-Aug', '04-Aug', '05-Aug', '06-Aug', '07-Aug', '08-Aug'],
			datasets : [ {
				label : ["% Orders Delivered"],
				fill: false,
				borderColor: "black",
				data : ['50', '54', '34', '28', '30', '44', '35']
			}]
	    },
	    options: barOptions_stacked
	});
	var ctx2 = document.getElementById("chart23");
	var myChart2 = new Chart(ctx2, {
	    type: 'line',
	    data: {
	    	labels : ['02-Aug', '03-Aug', '04-Aug', '05-Aug', '06-Aug', '07-Aug', '08-Aug'],
			datasets : [ {
				label : ["% Orders Delivered"],
				fill: false,
				borderColor: "black",
				data : ['50', '54', '34', '28', '30', '44', '35']
			}]
	    },
	    options: barOptions_stacked
	});
	var ctx2 = document.getElementById("chart24");
	var myChart2 = new Chart(ctx2, {
	    type: 'line',
	    data: {
	    	labels : ['02-Aug', '03-Aug', '04-Aug', '05-Aug', '06-Aug', '07-Aug', '08-Aug'],
			datasets : [ {
				label : ["% Orders Delivered"],
				fill: false,
				borderColor: "black",
				data : ['50', '54', '34', '28', '30', '44', '35']
			}]
	    },
	    options: barOptions_stacked
	});
	var ctx2 = document.getElementById("chart25");
	var myChart2 = new Chart(ctx2, {
	    type: 'line',
	    data: {
	    	labels : ['02-Aug', '03-Aug', '04-Aug', '05-Aug', '06-Aug', '07-Aug', '08-Aug'],
			datasets : [ {
				label : ["% Orders Delivered"],
				fill: false,
				borderColor: "black",
				data : ['50', '54', '34', '28', '30', '44', '35']
			}]
	    },
	    options: barOptions_stacked
	});
	var ctx2 = document.getElementById("chart26");
	var myChart2 = new Chart(ctx2, {
	    type: 'line',
	    data: {
	    	labels : ['02-Aug', '03-Aug', '04-Aug', '05-Aug', '06-Aug', '07-Aug', '08-Aug'],
			datasets : [ {
				label : ["% Orders Delivered"],
				fill: false,
				borderColor: "black",
				data : ['50', '54', '34', '28', '30', '44', '35']
			}]
	    },
	    options: barOptions_stacked
	});
	var ctx2 = document.getElementById("chart27");
	var myChart2 = new Chart(ctx2, {
	    type: 'line',
	    data: {
	    	labels : ['02-Aug', '03-Aug', '04-Aug', '05-Aug', '06-Aug', '07-Aug', '08-Aug'],
			datasets : [ {
				label : ["% Orders Delivered"],
				fill: false,
				borderColor: "black",
				data : ['50', '54', '34', '28', '30', '44', '35']
			}]
	    },
	    options: barOptions_stacked
	});
	var ctx2 = document.getElementById("chart28");
	var myChart2 = new Chart(ctx2, {
	    type: 'line',
	    data: {
	    	labels : ['02-Aug', '03-Aug', '04-Aug', '05-Aug', '06-Aug', '07-Aug', '08-Aug'],
			datasets : [ {
				label : ["% Orders Delivered"],
				fill: false,
				borderColor: "black",
				data : ['50', '54', '34', '28', '30', '44', '35']
			}]
	    },
	    options: barOptions_stacked
	});
	var ctx2 = document.getElementById("chart29");
	var myChart2 = new Chart(ctx2, {
	    type: 'line',
	    data: {
	    	labels : ['02-Aug', '03-Aug', '04-Aug', '05-Aug', '06-Aug', '07-Aug', '08-Aug'],
			datasets : [ {
				label : ["% Orders Delivered"],
				fill: false,
				borderColor: "black",
				data : ['50', '54', '34', '28', '30', '44', '35']
			}]
	    },
	    options: barOptions_stacked
	});
	var ctx2 = document.getElementById("chart30");
	var myChart2 = new Chart(ctx2, {
	    type: 'line',
	    data: {
	    	labels : ['02-Aug', '03-Aug', '04-Aug', '05-Aug', '06-Aug', '07-Aug', '08-Aug'],
			datasets : [ {
				label : ["% Orders Delivered"],
				fill: false,
				borderColor: "black",
				data : ['75', '73', '72', '70', '76', '78', '73']
			}]
	    },
	    options: barOptions_stacked
	});
	var ctx2 = document.getElementById("chart31");
	var myChart2 = new Chart(ctx2, {
	    type: 'line',
	    data: {
	    	labels : ['02-Aug', '03-Aug', '04-Aug', '05-Aug', '06-Aug', '07-Aug', '08-Aug'],
			datasets : [ {
				label : ["% Orders Delivered"],
				fill: false,
				borderColor: "black",
				data : ['70', '73', '75', '70', '76', '78', '75']
			}]
	    },
	    options: barOptions_stacked
	});
	var ctx2 = document.getElementById("chart32");
	var myChart2 = new Chart(ctx2, {
	    type: 'line',
	    data: {
	    	labels : ['02-Aug', '03-Aug', '04-Aug', '05-Aug', '06-Aug', '07-Aug', '08-Aug'],
			datasets : [ {
				label : ["% Orders Delivered"],
				fill: false,
				borderColor: "black",
				data : ['72', '71', '72', '70', '76', '70', '73']
			}]
	    },
	    options: barOptions_stacked
	});
	var ctx2 = document.getElementById("chart33");
	var myChart2 = new Chart(ctx2, {
	    type: 'line',
	    data: {
	    	labels : ['02-Aug', '03-Aug', '04-Aug', '05-Aug', '06-Aug', '07-Aug', '08-Aug'],
			datasets : [ {
				label : ["% Orders Delivered"],
				fill: false,
				borderColor: "black",
				data : ['71', '75', '72', '70', '79', '78', '73']
			}]
	    },
	    options: barOptions_stacked
	});
	var ctx2 = document.getElementById("chart34");
	var myChart2 = new Chart(ctx2, {
	    type: 'line',
	    data: {
	    	labels : ['02-Aug', '03-Aug', '04-Aug', '05-Aug', '06-Aug', '07-Aug', '08-Aug'],
			datasets : [ {
				label : ["% Orders Delivered"],
				fill: false,
				borderColor: "black",
				data : ['75', '74', '72', '73', '76', '79', '73']
			}]
	    },
	    options: barOptions_stacked
	});
	
	var ctx2 = document.getElementById("chart35");
	var myChart2 = new Chart(ctx2, {
	    type: 'bar',
	    data: {
	    	labels : ['Bangalore', 'Mumbai', 'Ahmedabad', 'Hyderabad', 'Chennai', 'Delhi', 'Kanpur', 'Lucknow', 'Mumbai', 'Pune', 'Vijayawada', 'Agartala', 'Allahabad', 'Amritsar', 'Baroda', 'Bhopal', 'Coimbatore', 'Gurgaon', 'Agra', 'Indore', 'Jaipur', 'Ludhiana', 'Mysore', 'Kolkata', 'Patna', 'Pune', 'Ranchi', 'Surat', 'Tirupati', 'Udaipur', 'Vishakhapatnam'],
			datasets : [ {
				label : ["% Of Cold Boxes Missing"],
				fill: false,
				borderColor: "black",
				data : ['10', '10', '09', '09', '08', '08', '08', '08', '07', '06', '06', '06', '06', '05', '05', '05', '04', '04', '04', '03', '03', '02', '02', '02', '02', '02', '02', '02', '01', '01', '01'],
				backgroundColor: [
					'rgba(54, 162, 235, 0.2)',
					'rgba(54, 162, 235, 0.2)',
					'rgba(54, 162, 235, 0.2)',
					'rgba(54, 162, 235, 0.2)',
					'rgba(54, 162, 235, 0.2)',
					'rgba(54, 162, 235, 0.2)',
					'rgba(54, 162, 235, 0.2)',
					'rgba(54, 162, 235, 0.2)',
					'rgba(54, 162, 235, 0.2)',
					'rgba(54, 162, 235, 0.2)',
					'rgba(54, 162, 235, 0.2)',
					'rgba(54, 162, 235, 0.2)',
					'rgba(54, 162, 235, 0.2)',
					'rgba(54, 162, 235, 0.2)',
					'rgba(54, 162, 235, 0.2)',
					'rgba(54, 162, 235, 0.2)',
					'rgba(54, 162, 235, 0.2)',
					'rgba(54, 162, 235, 0.2)',
					'rgba(54, 162, 235, 0.2)',
					'rgba(54, 162, 235, 0.2)',
					'rgba(54, 162, 235, 0.2)',
					'rgba(54, 162, 235, 0.2)',
					'rgba(54, 162, 235, 0.2)',
					'rgba(54, 162, 235, 0.2)',
					'rgba(54, 162, 235, 0.2)',
					'rgba(54, 162, 235, 0.2)',
					'rgba(54, 162, 235, 0.2)',
					'rgba(54, 162, 235, 0.2)',
					'rgba(54, 162, 235, 0.2)',
					'rgba(54, 162, 235, 0.2)',
					'rgba(54, 162, 235, 0.2)'
				],
				borderColor: [
					'rgba(54, 162, 235, 1)',
					'rgba(54, 162, 235, 1)',
					'rgba(54, 162, 235, 1)',
					'rgba(54, 162, 235, 1)',
					'rgba(54, 162, 235, 1)',
					'rgba(54, 162, 235, 1)',
					'rgba(54, 162, 235, 1)',
					'rgba(54, 162, 235, 1)',
					'rgba(54, 162, 235, 1)',
					'rgba(54, 162, 235, 1)',
					'rgba(54, 162, 235, 1)',
					'rgba(54, 162, 235, 1)',
					'rgba(54, 162, 235, 1)',
					'rgba(54, 162, 235, 1)',
					'rgba(54, 162, 235, 1)',
					'rgba(54, 162, 235, 1)',
					'rgba(54, 162, 235, 1)',
					'rgba(54, 162, 235, 1)',
					'rgba(54, 162, 235, 1)',
					'rgba(54, 162, 235, 1)',
					'rgba(54, 162, 235, 1)',
					'rgba(54, 162, 235, 1)',
					'rgba(54, 162, 235, 1)',
					'rgba(54, 162, 235, 1)',
					'rgba(54, 162, 235, 1)',
					'rgba(54, 162, 235, 1)',
					'rgba(54, 162, 235, 1)',
					'rgba(54, 162, 235, 1)',
					'rgba(54, 162, 235, 1)',
					'rgba(54, 162, 235, 1)',
					'rgba(54, 162, 235, 1)'
				   
				],
				borderWidth: 1
			}]
	    },
	    options: {
	    	 responsive: true,
	    	 maintainAspectRatio: false,
	        scales: {
	        	xAxes: [{
	        		scaleLabel: {
						labelString: 'Location',
						display: true
			    	},
                    gridLines: {
                        display:false
                    }
                }],
	        	yAxes: [{
	        		scaleLabel: {
						labelString: '# Of Missing Cold Boxes',
						display: true
			    	},
                    gridLines: {
                        display:false
                    },
        			ticks: {
        				beginAtZero: true,
        				max: 20
        			}
                }]
	        },
	        tooltips: {
		        enabled: false
		    },
		    hover :{
		        animationDuration:0
		    },
		    onClick: graphClickEvent,
	        animation: {
		        onComplete: function () {
		            var chartInstance = this.chart;
		            var ctx = chartInstance.ctx;
		            ctx.textAlign = "center";
		            ctx.textBaseline = "bottom";
		            ctx.font = "15px Open Sans";
		            ctx.fillStyle = "red";

		            Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
		                var meta = chartInstance.controller.getDatasetMeta(i);
		                Chart.helpers.each(meta.data.forEach(function (bar, index) {
		                    data = dataset.data[index];
		                   // alert(index + ":" + data + ":" +  bar._model.x + ":" + bar._model.y);
		                        ctx.fillText(data, bar._model.x, bar._model.y);
		                    
		                }),this);
		            }),this);
					
				}
		    }
	    }
	});
	
	function graphClickEvent(event, array){
		window.location.href = "CityPage.jsp#div1";
	}
</script>
	</body>
</html>
