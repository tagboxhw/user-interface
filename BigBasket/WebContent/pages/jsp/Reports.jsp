<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>

<%
response.setHeader("Cache-Control","no-cache"); 
response.setHeader("Pragma","no-cache"); 
response.setDateHeader ("Expires", -1); 
%>
<%!
static final Logger logger = LoggerFactory.getLogger("Reports");
%>
<%
String sUsername = (String) session.getAttribute("username");
if(sUsername == null || sUsername.equals("")) {
	session.setAttribute("callingPage", "Reports.jsp");
	response.sendRedirect("../../index.html");
}
	java.util.Date dt = new java.util.Date();
	java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd-MM-yyyy HH:mm");
sdf.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
String currentTime = sdf.format(dt);
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Reports</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- Add these below two lines for autocomplete and the javascript function at the end  -->
<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="../../bootstrap/css/bootstrap.css">
   
<!-- Font Awesome -->
<script src="https://use.fontawesome.com/37ba1872d2.js"></script>
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../../dist/css/tagbox.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
<link href="https://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet">
<link rel="stylesheet" href="../../dist/css/bootstrap-switch.css">
<script src="../../dist/js/bootstrap-switch1.js"></script>
<link href="../../dist/css/select2.css" rel="stylesheet" />
<script src="../../dist/js/select2.js"></script>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
  body {
	font-family:  Lato, sans-serif;
	font-weight: 600;
}
</style>
<script>
$(document).ready(function() {
	$("#City").select2();
	$("#DC").select2();
	$("#Hub").select2();
	$("#Category").select2();
	$("#Status").select2();
	$("#Filter").select2();
});
</script>
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
	<div class="wrapper">

		<header class="main-header">
			<!-- Logo -->
			<div>
				<a href="#" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
					<span class="logo-mini"><img src="../../dist/img/BigBasketLogo.png" style="margin-top:14px"
						alt="User Image"></span> <!-- logo for regular state and mobile devices -->
					<span class="logo-lg"><img src="../../dist/img/BigBasketLogo.png" 
						alt="User Image"></span>
				</a>
			</div>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">

				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
					role="button"> <span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
				</a>
				<label class="navbar-form pull-left" role="search">
					<div class="form-group">
						<b style="padding-right: 115px; font-size: 20px;color:#fff;" class="pull-right">Shipments Report</b><br> <b style="padding-right: 15px;color:#fff;"><%= currentTime %></b>
					</div>
				</label>

				<div class="navbar-custom-menu">
					<img src="../../dist/img/Logo.png" class="logo-class pull-right"
						alt="User Image">
				</div>
			</nav>

		</header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->


				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<br>
				<ul class="sidebar-menu">
					<li class="header">MAIN NAVIGATION</li>
						<li class="treeview "><a href="#"> <i
							class="fa fa-home"></i> <span>Home</span>
					</a>
					<ul class="treeview-menu">
							<li><a href="HomePage.jsp"><i class="fa fa-circle-o"></i>
									Cold Chain<br> Dashboard</a></li>
							<li><a href="CityPage.jsp"><i class="fa fa-circle-o"></i>
									City Performance</a></li>
							<li><a href="RunningShipmentInfo.jsp"><i class="fa fa-circle-o"></i>
									Shipment Detail</a></li>
							<li><a href="MissingBoxesDC.jsp"><i class="fa fa-circle-o"></i>
									Missing Boxes <br>Dashboard</a></li>
							
						
									</ul>
									</li>
						<li class="treeview active"><a href="#"> <i
							class="fa fa-bar-chart"></i> <span>Reports</span>
					</a>
					<ul class="treeview-menu">
							<li class="active"><a href="Reports.jsp"><i class="fa fa-circle-o"></i>
									Shipment Report</a></li>
									</ul>
									</li>
							
						
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper" style="background:white">
			<!-- Content Header (Page header) -->
			
		<section id="mainSection" class="content">
		<div class="row"><div class="col-md-12">
							<div class="well">
		<div class="row">
		<div class="col-md-6 text-center">
		<div class="row">
			<div class="col-md-4">
			CITY<br>
					<select id="City" name="City" class="form-control-no-background pull-center">
					<option>All</option>
					<option>Bangalore</option>
					<option>Chennai</option>
					<option>Goa</option>
					<option>Mumbai</option>
					<option>Pune</option>
					</select>
			</div>
			
			<div class="col-md-4">
			DC<br>
					<select id="DC" name="DC" class="form-control-no-background pull-center">
					<option>All</option>
					<option>DC 1</option>
					<option>DC 2</option>
					<option>DC 3</option>
					</select>
					</div>
					
					<div class="col-md-4 text-center">
					HUB<br>
					<select id="Hub" name="Hub" class="form-control-no-background pull-center">
					<option>All</option>
					<option>Hub 1</option>
					<option>Hub 2</option>
					<option>Hub 3</option>
					<option>Hub 4</option>
					<option>Hub 5</option>
					</select>
					</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-4 text-center">
						PRODUCT CATEGORY<br>
					<select id="Category" name="Category" class="form-control-no-background pull-center">
					<option>All</option>
					<option>CFV</option>
					<option>CNV</option>
					<option>CV</option>
					<option>FNV</option>
					<option>FV</option>
					</select>
						</div>
						<div class="col-md-4 text-center">
						STATUS<br>
					<select id="Status" name="Status" class="form-control-no-background pull-center">
					<option>All</option>
					<option>At DC</option>
					<option>DC -> Hub Transit</option>
					<option>At Hub</option>
					<option>Out For Delivery</option>
					<option>Delivered</option>
					<option>Cancelled</option>
					<option>Customer Retained Box</option>
					<option>Returned</option>
					<option>Shifted Order</option>
					</select>
						</div>
						
					</div>
					</div>
					<div class="col-md-6">
					<div class="row">
						<div class="col-md-offset-2 col-md-6 text-center">
						<label><b>TIME RANGE</b></label><br><label class="slider-time"></label> - <label class="slider-time2"></label>
    <div class="sliders_step1">
        <div id="slider-range"></div>
    </div>
    <input type="hidden" name="from_date" id="from_date" value="">
    <input type="hidden" name="to_date" id="to_date" value="">
						</div>
						</div>
					</div>
					</div>
					<div class="row"><div class="col-md-12 text-center"><br>
					<button type="button" class="btn bb-color">GO</button></div>
					</div>
					</div></div></div>
					<br>
		<div class="row">
					
					</div>
		<div>
						<!-- <span id="csvLink1"></span>&nbsp;&nbsp;&nbsp;&nbsp;<span id="xlsLink1"></span>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" onClick ="$('#temp1_table').tableExport({type:'pdf',escape:'false'});" style="text-decoration:underline">Export to PDF</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" style="text-decoration:underline">Email</a> -->
						<div class="btn-group pull-right">
							<button class="btn bb-color btn-sm dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Export Data</button>
							<ul class="dropdown-menu " role="menu">
								<!-- <li><a href="#" onClick ="$('#temp1_table').tableExport({type:'json',escape:'false'});"> <img src='../../dist/img/json.png' width='24px'> JSON</a></li>
								<li><a href="#" onClick ="$('#temp1_table').tableExport({type:'sql',escape:'false'});"> <img src='../../dist/img/sql.png' width='24px'> SQL</a></li>
								<li class="divider"></li>
								<li><a href="#" onClick ="$('#temp1_table').tableExport({type:'xml',escape:'false'});"> <img src='../../dist/img/xml.png' width='24px'> XML</a></li> -->
								<li><a href="#" onClick ="$('#temp1_table').tableExport({type:'csv',escape:'false'});"> <img src='../../dist/img/csv.png' width='24px'> CSV</a></li>
								<li><a href="#" onClick ="$('#temp1_table').tableExport({type:'excel',escape:'false'});"> <img src='../../dist/img/xls.png' width='24px'> XLS</a></li>
								<li class="divider"></li>				
								<li><a href="#" onClick ="$('#temp1_table').tableExport({type:'txt',escape:'false'});"> <img src='../../dist/img/txt.png' width='24px'> TXT</a></li>
								<!-- <li><a href="#" onClick ="$('#temp1_table').tableExport({type:'doc',escape:'false'});"> <img src='../../dist/img/word.png' width='24px'> Word</a></li>
								<li class="divider"></li> -->
								<!-- <li><a href="#" onClick ="$('#temp1_table').tableExport({type:'pdf',pdfFontSize:'7',escape:'false'});"> <img src='../../dist/img/pdf.png' width='15px'> PDF</a></li> -->
								<li><a href="#" onClick ="exportData()"> <img src='../../dist/img/pdf.png' width='15px'> PDF</a></li>
							</ul>
						</div>	
					</div>
			<div class="row text-center"><div class="col-md-12"><br>
			<table id="bb_table" name="bb_table" class="table table-striped">
				<thead>
					<tr class="info">
						<th class="text-center">
							Order ID
						</th>
						<th class="text-center">
							Box ID
						</th>
						<th class="text-center">
							DC
						</th>
						<th class="text-center">
							Hub
						</th>
						<th class="text-center">
							Product Category
						</th>
						<th class="text-center">
							Total Time Since Packing
						</th>
						<th class="text-center">
							Delivery Person
						</th>
						<th class="text-center">
							Last Update
						</th>
						<th class="text-center">
							Status
						</th>
						<th class="text-center">
							Last Known Temp (&deg;C)
						</th>
						<th class="text-center">
							Delivered Temp (&deg;C)
						</th>
						<th class="text-center">
							Total Time Spent In Current Excursion
						</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>11X34-22BS1-12YD3-ABC34</td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">1</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>CFV</td>
						<td>120 mins</td>
						<td>Robert</td>
						<td>2 mins ago</td>
						<td>DC To Hub Transit</td>
						<td>4</td>
						<td>NA</td>
						<td>10 mins</td>
					</tr>
					<tr>
						<td>237XY-HG234-U9887-4BI78</td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">2</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>CV</td>
						<td>120 mins</td>
						<td>Robert</td>
						<td>2 mins ago</td>
						<td>DC To Hub Transit</td>
						<td>4</td>
						<td>NA</td>
						<td>10 mins</td>
					</tr>
					<tr>
						<td>87SDF-8232N-876VV-WQ652</td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">3</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>CNV</td>
						<td>75 mins</td>
						<td>Paul</td>
						<td>12 mins ago</td>
						<td>Out for delivery</td>
						<td>5</td>
						<td>NA</td>
						<td>5 min</td>
					</tr>
					<tr>
						<td>11X34-22BS1-12YD3-ABC34</td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">4</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>FNV</td>
						<td>45 mins</td>
						<td>George</td>
						<td>22 mins ago</td>
						<td>Delivered</td>
						<td>3</td>
						<td>5</td>
						<td>NA</td>
					</tr>
					<tr>
						<td>237XY-HG234-U9887-4BI78</td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">5</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>FV</td>
						<td>30 mins</td>
						<td>Robert</td>
						<td>2 mins ago</td>
						<td>DC To Hub Transit</td>
						<td>4</td>
						<td>NA</td>
						<td>NA</td>
					</tr>
					<tr>
						<td>87SDF-8232N-876VV-WQ652</td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">6</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>CFV</td>
						<td>35 mins</td>
						<td>Paul</td>
						<td>12 mins ago</td>
						<td>Out for delivery</td>
						<td>5</td>
						<td>NA</td>
						<td>NA</td>
					</tr>
					<tr>
						<td>11X34-22BS1-12YD3-ABC34</td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">7</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>CFV</td>
						<td>45 mins</td>
						<td>George</td>
						<td>22 mins ago</td>
						<td>Delivered</td>
						<td>3</td>
						<td>5</td>
						<td>NA</td>
					</tr>
					<tr>
						<td>237XY-HG234-U9887-4BI78</td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">8</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>CFV</td>
						<td>30 mins</td>
						<td>Robert</td>
						<td>2 mins ago</td>
						<td>DC To Hub Transit</td>
						<td>4</td>
						<td>NA</td>
						<td>NA</td>
					</tr>
					<tr>
						<td>87SDF-8232N-876VV-WQ652</td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">9</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>CFV</td>
						<td>35 mins</td>
						<td>Paul</td>
						<td>12 mins ago</td>
						<td>Out for delivery</td>
						<td>5</td>
						<td>NA</td>
						<td>NA</td>
					</tr>
					<tr>
						<td>11X34-22BS1-12YD3-ABC34</td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">10</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>CFV</td>
						<td>45 mins</td>
						<td>George</td>
						<td>22 mins ago</td>
						<td>Delivered</td>
						<td>3</td>
						<td>5</td>
						<td>NA</td>
					</tr>
					<tr>
						<td>237XY-HG234-U9887-4BI78</td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">11</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>CFV</td>
						<td>30 mins</td>
						<td>Robert</td>
						<td>2 mins ago</td>
						<td>DC To Hub Transit</td>
						<td>4</td>
						<td>NA</td>
						<td>NA</td>
					</tr>
					<tr>
						<td>87SDF-8232N-876VV-WQ652</td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">12</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>CFV</td>
						<td>35 mins</td>
						<td>Paul</td>
						<td>12 mins ago</td>
						<td>Out for delivery</td>
						<td>5</td>
						<td>NA</td>
						<td>NA</td>
					</tr>
					<tr>
						<td>11X34-22BS1-12YD3-ABC34</td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">3</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>CFV</td>
						<td>45 mins</td>
						<td>George</td>
						<td>22 mins ago</td>
						<td>Delivered</td>
						<td>3</td>
						<td>5</td>
						<td>NA</td>
					</tr>
					<tr>
						<td>237XY-HG234-U9887-4BI78</td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">1</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>CFV</td>
						<td>30 mins</td>
						<td>Robert</td>
						<td>2 mins ago</td>
						<td>DC To Hub Transit</td>
						<td>4</td>
						<td>NA</td>
						<td>NA</td>
					</tr>
					<tr>
						<td>87SDF-8232N-876VV-WQ652</td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">2</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>CFV</td>
						<td>35 mins</td>
						<td>Paul</td>
						<td>12 mins ago</td>
						<td>Out for delivery</td>
						<td>5</td>
						<td>NA</td>
						<td>NA</td>
					</tr>
					<tr>
						<td>11X34-22BS1-12YD3-ABC34</td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">3</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>CFV</td>
						<td>45 mins</td>
						<td>George</td>
						<td>22 mins ago</td>
						<td>Delivered</td>
						<td>3</td>
						<td>5</td>
						<td>NA</td>
					</tr>
					<tr>
						<td>237XY-HG234-U9887-4BI78</td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">1</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>CFV</td>
						<td>30 mins</td>
						<td>Robert</td>
						<td>2 mins ago</td>
						<td>DC To Hub Transit</td>
						<td>4</td>
						<td>NA</td>
						<td>NA</td>
					</tr>
					<tr>
						<td>87SDF-8232N-876VV-WQ652</td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">2</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>CFV</td>
						<td>35 mins</td>
						<td>Paul</td>
						<td>12 mins ago</td>
						<td>Out for delivery</td>
						<td>5</td>
						<td>NA</td>
						<td>NA</td>
					</tr>
					<tr>
						<td>11X34-22BS1-12YD3-ABC34</td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">3</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>CFV</td>
						<td>45 mins</td>
						<td>George</td>
						<td>22 mins ago</td>
						<td>Delivered</td>
						<td>3</td>
						<td>5</td>
						<td>NA</td>
					</tr>
					<tr>
						<td>237XY-HG234-U9887-4BI78</td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">1</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>CFV</td>
						<td>30 mins</td>
						<td>Robert</td>
						<td>2 mins ago</td>
						<td>DC To Hub Transit</td>
						<td>4</td>
						<td>NA</td>
						<td>NA</td>
					</tr>
					<tr>
						<td>87SDF-8232N-876VV-WQ652</td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">2</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>CFV</td>
						<td>35 mins</td>
						<td>Paul</td>
						<td>12 mins ago</td>
						<td>Out for delivery</td>
						<td>5</td>
						<td>NA</td>
						<td>NA</td>
					</tr>
					<tr>
						<td>11X34-22BS1-12YD3-ABC34</td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">3</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>CFV</td>
						<td>45 mins</td>
						<td>George</td>
						<td>22 mins ago</td>
						<td>Delivered</td>
						<td>3</td>
						<td>5</td>
						<td>NA</td>
					</tr>
					<tr>
						<td>237XY-HG234-U9887-4BI78</td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">1</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>CFV</td>
						<td>30 mins</td>
						<td>Robert</td>
						<td>2 mins ago</td>
						<td>DC To Hub Transit</td>
						<td>4</td>
						<td>NA</td>
						<td>NA</td>
					</tr>
					<tr>
						<td>87SDF-8232N-876VV-WQ652</td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">2</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>CFV</td>
						<td>35 mins</td>
						<td>Paul</td>
						<td>12 mins ago</td>
						<td>Out for delivery</td>
						<td>5</td>
						<td>NA</td>
						<td>NA</td>
					</tr>
					<tr>
						<td>11X34-22BS1-12YD3-ABC34</td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">3</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>CFV</td>
						<td>45 mins</td>
						<td>George</td>
						<td>22 mins ago</td>
						<td>Delivered</td>
						<td>3</td>
						<td>5</td>
						<td>NA</td>
					</tr>
					<tr>
						<td>237XY-HG234-U9887-4BI78</td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">1</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>CFV</td>
						<td>30 mins</td>
						<td>Robert</td>
						<td>2 mins ago</td>
						<td>DC To Hub Transit</td>
						<td>4</td>
						<td>NA</td>
						<td>NA</td>
					</tr>
					<tr>
						<td>87SDF-8232N-876VV-WQ652</td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">2</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>CFV</td>
						<td>35 mins</td>
						<td>Paul</td>
						<td>12 mins ago</td>
						<td>Out for delivery</td>
						<td>5</td>
						<td>NA</td>
						<td>NA</td>
					</tr>
					<tr>
						<td>11X34-22BS1-12YD3-ABC34</td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">3</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>CFV</td>
						<td>45 mins</td>
						<td>George</td>
						<td>22 mins ago</td>
						<td>Delivered</td>
						<td>5</td>
						<td>5</td>
						<td>NA</td>
					</tr>
				</tbody>
			</table>
			</div></div>
	</section>
				</div>
		<!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0
			</div>
			<strong>Copyright &copy; 2017 <a href="http://tagbox.in">TagBox</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Control Sidebar -->
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->
	
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
	<script src="../../bootstrap/js/bootstrap.min.js"></script>
	<script src="../../dist/js/app.min.js"></script>
	<script src="../../dist/js/demo.js"></script>
<script>
var dt_from = "07/01/2017";
var dt_to = "07/08/2017";

$('.slider-time').html(dt_from);
$('.slider-time2').html(dt_to);
var min_val = Date.parse(dt_from)/1000;
var max_val = Date.parse(dt_to)/1000;

function zeroPad(num, places) {
  var zero = places - num.toString().length + 1;
  return Array(+(zero > 0 && zero)).join("0") + num;
}
function formatDT(__dt) {
    var year = __dt.getFullYear();
    var month = zeroPad(__dt.getMonth()+1, 2);
    var date = zeroPad(__dt.getDate(), 2);
    return date + '/' + month + '/' + year;
};


$("#slider-range").slider({
    range: true,
    min: min_val,
    max: max_val,
    step: 10,
    values: [min_val, max_val],
    slide: function (e, ui) {
        var dt_cur_from = new Date(ui.values[0]*1000); //.format("yyyy-mm-dd hh:ii:ss");
        $('.slider-time').html(formatDT(dt_cur_from));

        var dt_cur_to = new Date(ui.values[1]*1000); //.format("yyyy-mm-dd hh:ii:ss");                
        $('.slider-time2').html(formatDT(dt_cur_to));
    }
});
</script>
	</body>
</html>
