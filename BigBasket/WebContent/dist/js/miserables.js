// This file contains the weighted network of coappearances of characters in
// Victor Hugo's novel "Les Miserables". Nodes represent characters as indicated
// by the labels, and edges connect any pair of characters that appear in the
// same chapter of the book. The values on the edges are the number of such
// coappearances. The data on coappearances were taken from D. E. Knuth, The
// Stanford GraphBase: A Platform for Combinatorial Computing, Addison-Wesley,
// Reading, MA (1993).
//
// The group labels were transcribed from "Finding and evaluating community
// structure in networks" by M. E. J. Newman and M. Girvan.

var miserables = {
  nodes:[
    {nodeName:"DC 1", group:1},
    {nodeName:"DC 2", group:2},
    {nodeName:"Hub 1", group:3},
    {nodeName:"Hub 2", group:4},
    {nodeName:"Hub 3", group:5},
    {nodeName:"End Cust 1", group:6},
    {nodeName:"End Cust 2", group:7},
    {nodeName:"End Cust 3", group:8}
  ],
  links:[
    {source:1, target:3, value:2},
    {source:1, target:4, value:2},
    {source:2, target:5, value:10},
    {source:3, target:6, value:6},
    {source:4, target:7, value:1},
    {source:5, target:8, value:1}
  ]
};
