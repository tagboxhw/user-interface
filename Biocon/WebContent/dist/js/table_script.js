
function delete_leg(no)
{ 
	document.getElementById("row_leg"+no).outerHTML="";
	return; 
 
}

function delete_box(no)
{
	document.getElementById("row_tag"+no).outerHTML="";
	return;
}

function add_leg()
{	
	var no = document.getElementById("lastValue_leg").value;
	
 var table=document.getElementById("leg_table_step2");
 var table_len=(table.rows.length);
 
 var select1 = document.getElementById("Source");
 var a_source = "<select id='"+no+"_Source' name='"+no+"_Source' class='form-control-no-background pull-center' onchange='add_hops();'>" + select1.innerHTML + "</select>";
 
 select1 = document.getElementById("Destination");
 var a_dest = "<select id='"+no+"_Destination' name='"+no+"_Destination' class='form-control-no-background pull-center'>" + select1.innerHTML + "</select>";
 
 select1 = document.getElementById("0_Mode");
 var a_mode = "<select id='"+no+"_Mode' name='"+no+"_Mode' class='form-control-no-height pull-center'>" + select1.innerHTML + "</select>";
 
 
 var row = table.insertRow(table_len).outerHTML="<tr id='row_leg"+no+"'><td id='"+no+"_leg' class='text-center' colspan='2'>"+a_source+"</td>" +
 		"<td id='etd_"+no+"'><input class='form-control-no-height' type='text' id='"+no+"_etd'></td>" +
 		"<td id='eta_"+no+"'><input class='form-control-no-height' type='text' id='"+no+"_eta'></td>" +
 		"<td id='carrier_"+no+"'><input class='form-control-no-height' type='text' id='"+no+"_carrier'></td>" +
 		"<td id='mode_"+no+"' class='text-center'>"+a_mode+"</td>" +
 		"<td id='leg_"+no+"' class='text-center'><button type='button' class='add btn btn-sm btn-warning' onclick='add_leg();add_hops();'; data-toggle='tooltip' data-placement='top' title='Add Leg' value='Add Leg'><i class='fa fa-plus'></i></button>&nbsp;&nbsp;<button type='button' class='add btn btn-sm btn-danger' onclick='delete_leg("+no+");add_hops();' data-toggle='tooltip' data-placement='top' title='Remove Leg' value='Remove Leg'><i class='fa fa-minus'></i></button></td>";
 		
 
 $('#'+no+'_Source').select2();
 $('#'+no+'_Destination').select2();
 $('#'+no+'_eta').datetimepicker({
	oneLine: true,
	controlType: 'select'
 }); 
 $('#'+no+'_etd').datetimepicker({
	oneLine: true,
	controlType: 'select'
 }); 
 document.getElementById("lastValue_leg").value = (parseInt(no, 10)+1); 
}

function add_box()
{	
	
	var no = document.getElementById("lastValue_tag").value;
	
	 	
	
 var table=document.getElementById("tag_table_step3");
 var table_len=(table.rows.length);
 
 var select1 = document.getElementById("0_TagId");
 var a_source = "<select id='"+no+"_TagId' name='"+no+"_TagId' class='form-control-no-background pull-center' onchange='checkTag("+no+")' style='width: 100%'>" + select1.innerHTML + "</select>";
 
 var select1 = document.getElementById("0_Criticality");
 var a_criticality = "<select id='"+no+"_Criticality' name='"+no+"_Criticality' class='form-control-no-height pull-center'>" + select1.innerHTML + "</select>";
 
 var row = table.insertRow(table_len).outerHTML="<tr id='row_tag"+no+"'><td id='"+no+"_box' class='text-center'><input class='form-control-no-height' type='text' id='"+no+"_BoxId' name='"+no+"_BoxId' onchange='checkBox("+no+")'></td>" +
 		"<td id='tag_"+no+"' class='text-center'>"+a_source+"</td>" +
 		"<td id='product_"+no+"' class='text-center'><div class='ui-widget'><input id='"+no+"_ProductId' name='"+no+"_ProductId' class='form-control-no-height' type='text'></div></td>" +
 		"<td id='value_"+no+"'><input class='form-control-no-height' type='text' id='"+no+"_Value'></td>" +
 		"<td id='criticality_"+no+"' class='text-center'>"+a_criticality+"</td>" +
 		"<td id='tag_btn_"+no+"' class='text-center'><button type='button' class='add btn btn-sm btn-warning' onclick='add_box()'; data-toggle='tooltip' data-placement='top' title='Add Box' value='Add Box'><i class='fa fa-plus'></i></button>&nbsp;&nbsp;<button type='button' class='add btn btn-sm btn-danger' onclick='delete_box("+no+")'; data-toggle='tooltip' data-placement='top' title='Remove Box' value='Remove Box'><i class='fa fa-minus'></i></button></td>";
 		
 
 $('#'+no+'_TagId').select2(); 
 var b = $("#products_id").val();
 var a = b.split(",");
 $( "#"+no+"_ProductId" ).autocomplete({
   source: a
 });
 document.getElementById("lastValue_tag").value = (parseInt(no, 10)+1);
}

function add_hops() {
	var no = document.getElementById("lastValue_leg").value;
	var final_hops = "";
	for(var i=0;i<no;i++) {
		var b = i+"_Source";
		if(document.getElementById(b) != null) {
			final_hops = final_hops + "<b class='font-blue font-big'>" + $('#'+i+'_Source').val() + "</b>&nbsp;&nbsp;<i class='fa fa-long-arrow-right font-blue font-big'></i>&nbsp;&nbsp;";
		}
	}
	final_hops = final_hops + "<b class='font-blue font-big'>" + $('#0_Destination').val();
	$("#show_stops").html("");
	$("#show_stops").html(final_hops);
}


function loadXMLString(txt) 
{
	if (window.DOMParser)
	{
		parser=new DOMParser();
		xmlDoc=parser.parseFromString(txt,"text/xml");
	}
	else // Internet Explorer
	{
		xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
		xmlDoc.async=false;
		xmlDoc.loadXML(txt); 
	}
	return xmlDoc;
}