<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ page import="com.biocon.dao.TripScorecardDao"%>
<%@ page import="com.biocon.dao.TagAssignmentDao"%>
<%@ page import="com.biocon.bean.TripScorecardBean"%>
<%@ page import="com.biocon.bean.TagAssignmentBean"%>
<%@ page import="com.biocon.util.Utils"%>
<%@ page import="java.util.TreeMap"%>
<%@ page import="java.util.Set"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.util.Collection"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@ page import="java.util.Set"%>
<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>
<%@ page import="com.biocon.dao.TemperatureDao"%>
<%@ page import="com.biocon.bean.TemperatureBean"%>

<%!
static final Logger logger = LoggerFactory.getLogger("DownloadShipmentData");
%>
<%
String sUsername = (String) session.getAttribute("username");
if(sUsername == null || sUsername.equals("")) {
	session.setAttribute("callingPage", "DownloadShipmentData.jsp");
	response.sendRedirect("../../index.html");
}
java.util.Date dt = new java.util.Date();
java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd-MM-yyyy HH:mm");
sdf.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
String currentTime = sdf.format(dt);
SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
df.setTimeZone(java.util.TimeZone.getTimeZone("UTC"));
sdf1.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
//Temporatily using Value_INR field to save trip status to display the alert bell
String sShipmentId = request.getParameter("ShipID");
TemperatureDao td = new TemperatureDao();
Collection col = null;
if(sShipmentId != null) {
col = td.selectTemperature(Integer.parseInt(sShipmentId), "All");
}
Date d = null;
Date d2 = null;
Long newTime = null;
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Download Data</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Add these below two lines for autocomplete and the javascript function at the end  -->
<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="../../bootstrap/css/bootstrap.css">
   
<!-- Font Awesome -->
<script src="https://use.fontawesome.com/37ba1872d2.js"></script>
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../../dist/css/tagbox.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
<link href="https://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet">
<link href="../../dist/css/select2.css" rel="stylesheet" />
<script src="../../dist/js/select2.js"></script>
<script src="../../dist/js/table2download.js"></script>
<link rel="stylesheet" href="../../dist/css/bootstrap-switch.css">
<script src="../../dist/js/bootstrap-switch.js"></script>
<script type="text/javascript" src="../../plugins/tableExport/tableExport.js"></script>
<script type="text/javascript" src="../../plugins/tableExport/jquery.base64.js"></script>
<script type="text/javascript" src="../../plugins/tableExport/jspdf/libs/sprintf.js"></script>
<script type="text/javascript" src="../../plugins/tableExport/jspdf/jspdf.js"></script>
<script type="text/javascript" src="../../plugins/tableExport/jspdf/libs/base64.js"></script>
<script type="text/javascript" src="../../plugins/tableExport/html2canvas.js"></script>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
  body {
	font-family:  Lato, sans-serif;
	font-weight: 600;
}
</style>
<style>
.save
{
	display:none;
}
.no-border {
	border: none !important;
	border: 0 !important;
}
</style>
<STYLE TYPE="text/css">
			table.sortable span.sortarrow {
   				color: blue;
   				text-decoration: none;
			}
					
		</style>
<script src="../../dist/js/sorttable.js"></script>
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
	<div class="wrapper">

		<header class="main-header">
			<!-- Logo -->
			<div>
				<a href="#" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
					<span class="logo-mini"><b>Demo</b></span> <!-- logo for regular state and mobile devices -->
					<span class="logo-lg"><b>Demo</b> </span>
				</a>
			</div>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">

				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
					role="button"> <span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
				</a>
				<label class="navbar-form pull-left" role="search">
					<div class="form-group">
						<b style="padding-left: 15px; font-size: 20px;color:#fff;" class="pull-right">Download Shipment Data</b><br> <b style="padding-left: 15px;color:#fff;"><%= currentTime %></b>
					</div>
				</label>

				<div class="navbar-custom-menu">
					<img src="../../dist/img/Logo.png" class="logo-class pull-right"
						alt="User Image">
				</div>
			</nav>

		</header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->


				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<br>
				<ul class="sidebar-menu">
					<li class="header">MAIN NAVIGATION</li>
					<li class="treeview active"><a href="#"> <i
							class="fa fa-dashboard"></i> <span>Dashboard</span>
					</a>
						<ul class="treeview-menu">
							<li class="active"><a href="TripScorecard.jsp"><i
									class="fa fa-circle-o"></i> Home</a></li>
						</ul></li>
					<!-- <li class="treeview"><a href="#"> <i
							class="fa fa-database"></i> <span>Admin</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="TagAssignment.jsp"><i class="fa fa-circle-o"></i>
									Tag Assignment</a></li>
						</ul></li> -->
					<li class="treeview"><a href="#"> <i
							class="fa fa-th-large"></i> <span>Shipment</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="ShipmentCreation.jsp"><i class="fa fa-circle-o"></i>
									Create Shipment</a></li>
							<li><a href="ShipmentEdit.jsp"><i class="fa fa-circle-o"></i>
									Edit Shipment</a></li>
						</ul></li>
						<li class="treeview"><a href="#"> <i
							class="fa fa-bar-chart"></i> <span>Trip Analysis</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="ETagDashboard.jsp"><i class="fa fa-circle-o"></i>
									Trip Report</a></li>
						</ul></li>
						<!-- <li class="treeview"><a href="#"> <i
							class="fa fa-file-text-o"></i> <span>Reports</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="ETagDashboard.jsp"><i class="fa fa-circle-o"></i>
									Download Historic<br> Data</a></li>
						</ul></li> -->
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper" style="background:white">
			<!-- Content Header (Page header) -->
			
		<section id="mainSection" class="content">
		<ol class="breadcrumb">
					<li class="active"><i class="fa fa-dashboard"></i> Home</li>
				</ol>
			<form id="mainForm" method="post" action="DownloadShipmentData">
			
							
		<div class="container">
                    <div>
						<!-- <span id="csvLink1"></span>&nbsp;&nbsp;&nbsp;&nbsp;<span id="xlsLink1"></span>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" onClick ="$('#temp1_table').tableExport({type:'pdf',escape:'false'});" style="text-decoration:underline">Export to PDF</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" style="text-decoration:underline">Email</a> -->
						<div class="btn-group">
							<button class="btn btn-warning btn-sm dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Export Table Data</button>
							<ul class="dropdown-menu " role="menu">
								<!-- <li><a href="#" onClick ="$('#temp1_table').tableExport({type:'json',escape:'false'});"> <img src='../../dist/img/json.png' width='24px'> JSON</a></li>
								<li><a href="#" onClick ="$('#temp1_table').tableExport({type:'sql',escape:'false'});"> <img src='../../dist/img/sql.png' width='24px'> SQL</a></li>
								<li class="divider"></li>
								<li><a href="#" onClick ="$('#temp1_table').tableExport({type:'xml',escape:'false'});"> <img src='../../dist/img/xml.png' width='24px'> XML</a></li> -->
								<li><a href="#" onClick ="$('#temp1_table').tableExport({type:'csv',escape:'false'});"> <img src='../../dist/img/csv.png' width='24px'> CSV</a></li>
								<li><a href="#" onClick ="$('#temp1_table').tableExport({type:'excel',escape:'false'});"> <img src='../../dist/img/xls.png' width='24px'> XLS</a></li>
								<li class="divider"></li>				
								<li><a href="#" onClick ="$('#temp1_table').tableExport({type:'txt',escape:'false'});"> <img src='../../dist/img/txt.png' width='24px'> TXT</a></li>
								<!-- <li><a href="#" onClick ="$('#temp1_table').tableExport({type:'doc',escape:'false'});"> <img src='../../dist/img/word.png' width='24px'> Word</a></li>
								<li class="divider"></li> -->
								<li><a href="#" onClick ="$('#temp1_table').tableExport({type:'pdf',pdfFontSize:'7',escape:'false'});"> <img src='../../dist/img/pdf.png' width='15px'> PDF</a></li>
							</ul>
						</div>	
					</div><br>
                    <table id="temp1_table" class="table table-condensed table-bordered sortable" style="width:48%">
                    <thead><tr class="blue"><th class="text-center">Shipment ID</th><th class="text-center">Box Id</th><th class="text-center">Time</th><th class="text-center">Temperature</th><th class="text-center">Location</th></tr></thead>
                    <tbody>
                    <%
                    if(col == null || col.size() == 0) {%>
                    	 <tr><td colspan="5">No Data!</td></tr>
                    	
                    <%} else {
                    TemperatureBean tdBean = new TemperatureBean();
                    Iterator iterSt = col.iterator();
                    while(iterSt.hasNext()){ 
                    	tdBean = (TemperatureBean) iterSt.next();
                    	d = df.parse(tdBean.getTimestamp());
            			//newTime = d.getTime();
            			//newTime +=(330*60*1000);
            			//d2 = new Date(newTime);
                    %>
                    	<tr class="text-center"><td>Tag-0001</td><td><%=tdBean.getNode_Id()%></td><td><%=sdf1.format(d)%></td><td class="text-center"><%=tdBean.getTemperature() %></td><td>&nbsp;</td></tr>
                    <%}
                    }%>
                    </tbody>
                    </table>
                    	
                </div>
		</form>
		</section>
	
    
    
    
    
    
				</div>
		<!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0
			</div>
			<strong>Copyright &copy; 2017 <a href="http://tagbox.in">TagBox</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Control Sidebar -->
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->
	
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>


	<!-- Bootstrap 3.3.6 -->
	<script src="../../bootstrap/js/bootstrap.min.js"></script>
	<!-- ChartJS 1.0.1 -->
	<!-- AdminLTE App -->
	<script src="../../dist/js/app.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="../../dist/js/demo.js"></script>
<script type="text/javascript">
function doEnd(tagid) {
	$("#endTripID").val(tagid);
}

function showDownload() {
	var a = $("#endTripID").val();
	$("#"+a+"_download").show();
	$("#"+a+"_running").hide();
}
	jQuery( document ).ready(function() {
	    
	    jQuery( "#temp1_table" ).table_download({
	        format: "xls",
	        separator: ",",
	        filename: "download",
	        linkname: "Export To XLS",
	        quotes: "\"",
	        linkid: "xlsLink1"
	    });
	    
	    jQuery( "#temp1_table" ).table_download({
	        format: "csv",
	        separator: "-",
	        filename: "download",
	        linkname: "Export To CSV",
	        quotes: "\"",
	        linkid: "csvLink1"
	    });
	    
	});
</script>
	</body>
	</html>
