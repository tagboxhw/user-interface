<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>
<%@ page import="com.biocon.dao.TagAssignmentDao"%>
<%@ page import="java.text.SimpleDateFormat"%>

<%!static final Logger logger = LoggerFactory.getLogger("GetTagAssignmentData");
TagAssignmentDao ta = new TagAssignmentDao();
%>
<%
String buf = "";
String sString = "";
try {
	sString = request.getParameter("query");
	if (sString.equals("updateTagDetails")) {
		String sTagId = request.getParameter("Tag_Id");
		String sShipDate = request.getParameter("Ship_Date");
		String sShipId = request.getParameter("Ship_Id");
		String sSource = request.getParameter("Source_Id");
		String sDest = request.getParameter("Dest_Id");
		String sFlightId = request.getParameter("flight_id");
		String sCriticalityId = request.getParameter("Criticality_id");
		String sMinId = request.getParameter("Min_Id");
		String sMaxId = request.getParameter("Max_Id");
		//logger.info("AlertId: " + sAlertId + " Action: " + sAction);
		buf = "";
		//response.setContentType("text/xml");
		//buf += ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		ta.updateTagAssignment(sTagId, sShipDate, sShipId, sSource, sDest, sFlightId, sCriticalityId, sMinId, sMaxId);
		session.setAttribute("tagid", sTagId);
		session.setAttribute("flightId", sFlightId);
		session.setAttribute("min", sMinId);
		session.setAttribute("max", sMaxId);
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MMM HH:mm");
		
		buf = "<rows>";
		buf += ("<row>");
		buf += ("<Done>" + sdf1.format((df.parse(sShipDate))) + "</Done>");
		buf += ("</row>");
		buf += "</rows>";
		buf = buf.trim();
		//logger.info(buf);
		out.clear();
		out.println(buf);
	}
} catch (Exception e) {
	logger.error("Exception: " + e.getLocalizedMessage());
	e.printStackTrace();
}
%>