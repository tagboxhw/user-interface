<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ page import="com.biocon.dao.ShipmentDao"%>
<%@ page import="com.biocon.bean.ShipmentLocations"%>
<%@ page import="com.biocon.util.Utils"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.TreeMap"%>
<%@ page import="java.util.Set"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.util.Collection"%>
<%@ page import="java.util.Set"%>
<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>

<%!
static final Logger logger = LoggerFactory.getLogger("ShipmentTransitDetailing");
%>
<%
String sUsername = (String) session.getAttribute("username");
if(sUsername == null || sUsername.equals("")) {
	session.setAttribute("callingPage", "ShipmentTransitDetailing.jsp");
	response.sendRedirect("../../index.html");
}
SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MMM HH:mm");
java.util.Date dt = new java.util.Date();
java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd-MM-yyyy HH:mm");
sdf.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
String currentTime = sdf.format(dt);
ShipmentDao sd = new ShipmentDao();
Collection<ShipmentLocations> colLocs = sd.getShipmentLocations();
String sShipmentId = request.getParameter("shipment_id");
String sSource = request.getParameter("Source");
String sDestination = request.getParameter("Destination");
String sETD = request.getParameter("etd");
String sETA = request.getParameter("eta");
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Step 2 - Shipment Transit Detailing</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- Add these below two lines for autocomplete and the javascript function at the end  -->
<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="../../bootstrap/css/bootstrap.css">
   
<!-- Font Awesome -->
<script src="https://use.fontawesome.com/37ba1872d2.js"></script>
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../../dist/css/tagbox.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
<link href="https://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet">
<link href="../../dist/css/select2.css" rel="stylesheet" />
<script src="../../dist/js/select2.js"></script>
<script src="../../dist/js/tagAssignment.js"></script>
<link rel="stylesheet" href="../../dist/css/datepicker.css">
<link href="../../dist/css/select2.css" rel="stylesheet" />
<script src="../../dist/js/select2.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#Source").select2();
	$("#Destination").select2();
});
</script>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
  body {
	font-family:  Lato, sans-serif;
	font-weight: 600;
}
</style>
<style>
.save
{
	display:none;
}
.no-border {
	border: none !important;
	border: 0 !important;
}
</style>
<STYLE TYPE="text/css">
			table.sortable span.sortarrow {
   				color: blue;
   				text-decoration: none;
			}
					
		</style>
<script src="../../dist/js/sorttable.js"></script>
<script src="../../dist/js/table_script.js"></script>
<script>
  $( function() {
    var availableTags = [
		"SHP-001",
		"SHP-002",
		"SHP-003",
		"SHP-004",
		"SHP-005",
		"SHP-006",
		"SHP-007",
		"SHP-008"
    ];
    $( "#tags" ).autocomplete({
      source: availableTags
    });
  });
    </script>
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
	<div class="wrapper">

		<header class="main-header">
			<!-- Logo -->
			<div>
				<a href="#" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
					<span class="logo-mini"><b>Demo</b></span> <!-- logo for regular state and mobile devices -->
					<span class="logo-lg"><b>Demo</b> </span>
				</a>
			</div>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">

				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
					role="button"> <span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
				</a>
				<label class="navbar-form pull-left" role="search">
					<div class="form-group">
						<b style="padding-left: 15px; font-size: 20px;color:#fff;" class="pull-right">Shipment Assignment</b><br> <b style="padding-left: 15px;color:#fff;"><%= currentTime %></b>
					</div>
				</label>

				<div class="navbar-custom-menu">
					<img src="../../dist/img/Logo.png" class="logo-class pull-right"
						alt="User Image">
				</div>
			</nav>

		</header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->


				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<br>
				<ul class="sidebar-menu">
					<li class="header">MAIN NAVIGATION</li>
					<li class="treeview"><a href="#"> <i
							class="fa fa-dashboard"></i> <span>Dashboard</span>
					</a>
						<ul class="treeview-menu">
							<li class=""><a href="TripScorecard.jsp"><i
									class="fa fa-circle-o"></i> Home</a></li>
						</ul></li>
					<li class="treeview"><a href="#"> <i
							class="fa fa-database"></i> <span>Admin</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="TagAssignment.jsp"><i class="fa fa-circle-o"></i>
									Tag Assignment</a></li>
						</ul></li>
						<li class="treeview active"><a href="#"> <i
							class="fa fa-th-large"></i> <span>Shipment</span>
					</a>
						<ul class="treeview-menu">
							<li class="active"><a href="ShipmentCreation.jsp"><i class="fa fa-circle-o"></i>
									Create Shipment</a></li>
							<li><a href="ShipmentCreation.jsp"><i class="fa fa-circle-o"></i>
									Edit Shipment</a></li>
						</ul></li>
						<li class="treeview"><a href="#"> <i
							class="fa fa-bar-chart"></i> <span>Trip Analysis</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="ETagDashboard.jsp"><i class="fa fa-circle-o"></i>
									X-Tag Dashboard</a></li>
						</ul></li>
						<li class="treeview"><a href="#"> <i
							class="fa fa-file-text-o"></i> <span>Reports</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="ETagDashboard.jsp"><i class="fa fa-circle-o"></i>
									Download Historic<br> Data</a></li>
						</ul></li>
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper" style="background:white">
			<!-- Content Header (Page header) -->
			
		<section id="mainSection" class="content">
			<ul class="nav navbar-nav">
            <li><a href="ShipmentCreation.jsp" style="background:#5cb85c;color:#fff;">Step 1<span>Shipment Creation</span></a></li>
            <li><a href="ShipmentTransitDetailing.jsp" style="background:#0383c0;color:#fff;">Step 2<span>Shipment Transit Detailing</span></a></li>
            <li><a href="#">Step 3<span>Shipment Tag Assignment</span></a></li>
            <li><a href="#">Step 4<span>Shipment User Assignment</span></a></li>
          </ul>
          <form id="mainForm" method="post" action="ShipmentCreation.jsp">
          <div class="row">
          	<div class="col-md-12">
		<div class="row">
          	<div class="col-md-2">
		<div class="ui-widget">
		<Br><br>
  			<label for="tags">Shipment ID: </label>
  		<input  type="text" id="shipment_id" name="shipment_id" class='form-control-no-height' value="<%= sShipmentId %>" disabled>
  		</div></div>
  		</div>
  		<div class="row">
  			<div class="col-md-offset-1 col-md-8">
  			<br>
  				<table id="data_table_stationary" class="table table-bordered table-striped table-condensed">
					<tr class="info text-center">
						<th width="20%" class="text-center"><h4>Location Type</h4></th>
						<th width="20%" class="text-center"><h4>Name</h4></th>
						<th width="20%" class="text-center"><h4>ETA</h4></th>
						<th width="20%" class="text-center"><h4>ETD</h4></th>
						<th width="20%" class="text-center"><h4>Action</h4></th>
					</tr>
					<tr>
						<td class="text-center">Source</td>
						<td class="text-center"><select id="Source" name="Source" class="form-control-no-background pull-center">
				          	<option>Select Source</option>
				          	<%for(ShipmentLocations s: colLocs) { 
				          		if(sSource.equals(s.getShipmentLocationId())) {%>
				          			<option selected><%=s.getShipmentLocationId() %></option>
				          		<%} else { %>
				          			<option><%=s.getShipmentLocationId() %></option>
				          		<%} %>
				          	<%} %>
						</select>
						</td>
						<td class="text-center">NA</td>
						<td class="text-center"><input class='form-control-no-height' placeholder="ETD" type="text" id="etd" name="etd" value="<%=sETD%>"></td>
						<td class="text-center"><button type="button" class="add btn btn-sm btn-warning" onclick="add_row_stationary();" data-toggle="tooltip" data-placement="top" title="Add Leg" value="Add Row"><i class='fa fa-plus'></i></button></td>
					</tr>
					<tr>
						<td class="text-center">Destination</td>
						<td class="text-center"><select id="Destination" name="Destination" class="form-control-no-background pull-center">
          					<option>Select Destination</option>
				          	<%for(ShipmentLocations s: colLocs) { 
				          		if(sDestination.equals(s.getShipmentLocationId())) {%>
				          			<option selected><%=s.getShipmentLocationId() %></option>
				          		<%} else { %>
				          			<option><%=s.getShipmentLocationId() %></option>
				          		<%} %>
				          	<%} %>
						</select></td>
						<td class="text-center"><input class='form-control-no-height' placeholder="ETA" type="text" id="eta" name="eta" value="<%= sETA%>"></td>
						<td class="text-center">NA</td>
						<td class="text-center">&nbsp;</td>
					</tr>
				</table>
				<%int j = 1; %>
				<input type="hidden" id="lastValue_stationary" name="lastValue_stationary" value="<%= j %>">
				<br>
				<a onclick="showStep('1')" class="btn btn-success pull-left"><i class="fa fa-chevron-left fg-lg"></i>&nbsp;EDIT STEP 1</a>
				<a onclick="showStep('3')" class="btn btn-success pull-right">SAVE & CONTINUE&nbsp;<i class="fa fa-chevron-right fg-lg"></i></a>
			
  			</div>
  		</div>
  		<br>
  		</div></div></form>
		</section>
	
    
    
    
				</div>
		<!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0
			</div>
			<strong>Copyright &copy; 2017 <a href="http://tagbox.in">TagBox</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Control Sidebar -->
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->
	
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
<script src="../../dist/js/bootstrap-datepicker.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="../../bootstrap/js/bootstrap.min.js"></script>
	<!-- ChartJS 1.0.1 -->
	<!-- AdminLTE App -->
	<script src="../../dist/js/app.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="../../dist/js/demo.js"></script>
	<script>
	$('#eta').datepicker({
	     format: "dd/mm/yyyy"
	 }); 
	
	$('#etd').datepicker({
	     format: "dd/mm/yyyy"
	 });
	</script>

	</body>
	
</html>
