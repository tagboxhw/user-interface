<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ page import="com.biocon.dao.TagAssignmentDao"%>
<%@ page import="com.biocon.bean.TagAssignmentBean"%>
<%@ page import="com.biocon.util.Utils"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.TreeMap"%>
<%@ page import="java.util.Set"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.util.Collection"%>
<%@ page import="java.util.Set"%>
<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>

<%!
static final Logger logger = LoggerFactory.getLogger("ShipmentUserAssignment");
%>
<%
String sUsername = (String) session.getAttribute("username");
if(sUsername == null || sUsername.equals("")) {
	session.setAttribute("callingPage", "ShipmentUserAssignment.jsp");
	response.sendRedirect("../../index.html");
}
SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MMM HH:mm");
java.util.Date dt = new java.util.Date();
java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd-MM-yyyy HH:mm");
sdf.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
String currentTime = sdf.format(dt);
TagAssignmentDao taDao = new TagAssignmentDao();
TagAssignmentBean taBean = new TagAssignmentBean();
int iUpcoming = 0, iUnderway = 0, iUnassigned = 0;

int tmp = 0;
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Step 4 - Shipment User Assignment</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Add these below two lines for autocomplete and the javascript function at the end  -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="../../bootstrap/css/bootstrap.css">
   
<!-- Font Awesome -->
<script src="https://use.fontawesome.com/37ba1872d2.js"></script>
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../../dist/css/tagbox.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
<link href="https://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet">
<link href="../../dist/css/select2.css" rel="stylesheet" />
<script src="../../dist/js/select2.js"></script>
<script src="../../dist/js/tagAssignment.js"></script>
<link rel="stylesheet" href="../../dist/css/datepicker.css">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
  body {
	font-family:  Lato, sans-serif;
	font-weight: 600;
}
</style>
<style>
.save
{
	display:none;
}
.no-border {
	border: none !important;
	border: 0 !important;
}
</style>
<STYLE TYPE="text/css">
			table.sortable span.sortarrow {
   				color: blue;
   				text-decoration: none;
			}
					
		</style>
<script src="../../dist/js/sorttable.js"></script>
<script>
  $( function() {
    var availableTags = [
      "XT065D",
      "XT1BA0",
      "XT5AFD",
      "XT4D37",
      "XT065D"
    ];
    $( "#tags" ).autocomplete({
      source: availableTags
    });
  });
  </script>
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
	<div class="wrapper">

		<header class="main-header">
			<!-- Logo -->
			<div>
				<a href="#" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
					<span class="logo-mini"><b>Demo</b></span> <!-- logo for regular state and mobile devices -->
					<span class="logo-lg"><b>Demo</b> </span>
				</a>
			</div>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">

				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
					role="button"> <span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
				</a>
				<label class="navbar-form pull-left" role="search">
					<div class="form-group">
						<b style="padding-left: 15px; font-size: 20px;color:#fff;" class="pull-right">Shipment Assignment</b><br> <b style="padding-left: 15px;color:#fff;"><%= currentTime %></b>
					</div>
				</label>

				<div class="navbar-custom-menu">
					<img src="../../dist/img/Logo.png" class="logo-class pull-right"
						alt="User Image">
				</div>
			</nav>

		</header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->


				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<br>
				<ul class="sidebar-menu">
					<li class="header">MAIN NAVIGATION</li>
					<li class="treeview"><a href="#"> <i
							class="fa fa-dashboard"></i> <span>Dashboard</span>
					</a>
						<ul class="treeview-menu">
							<li class=""><a href="TripScorecard.jsp"><i
									class="fa fa-circle-o"></i> Home</a></li>
						</ul></li>
					<li class="treeview"><a href="#"> <i
							class="fa fa-database"></i> <span>Admin</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="TagAssignment.jsp"><i class="fa fa-circle-o"></i>
									Tag Assignment</a></li>
						</ul></li>
						<li class="treeview active"><a href="#"> <i
							class="fa fa-th-large"></i> <span>Shipment</span>
					</a>
						<ul class="treeview-menu">
							<li class="active"><a href="ShipmentCreation.jsp"><i class="fa fa-circle-o"></i>
									Create Shipment</a></li>
							<li><a href="ShipmentCreation.jsp"><i class="fa fa-circle-o"></i>
									Edit Shipment</a></li>
						</ul></li>
						<li class="treeview"><a href="#"> <i
							class="fa fa-bar-chart"></i> <span>Trip Analysis</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="ETagDashboard.jsp"><i class="fa fa-circle-o"></i>
									X-Tag Dashboard</a></li>
						</ul></li>
						<li class="treeview"><a href="#"> <i
							class="fa fa-file-text-o"></i> <span>Reports</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="ETagDashboard.jsp"><i class="fa fa-circle-o"></i>
									Download Historic<br> Data</a></li>
						</ul></li>
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper" style="background:white">
			<!-- Content Header (Page header) -->
			
		<section id="mainSection" class="content">
		<ul class="nav navbar-nav">
            <li><a href="ShipmentCreation.jsp" style="background:#5cb85c;color:#fff;">Step 1<span>Shipment Creation</span></a></li>
            <li><a href="ShipmentTransitDetailing.jsp" style="background:#5cb85c;color:#fff;">Step 2<span>Shipment Transit Detailing</span></a></li>
            <li><a href="ShipmentTagAssignment.jsp" style="background:#5cb85c;color:#fff;">Step 3<span>Shipment Tag Assignment</span></a></li>
            <li><a href="ShipmentUserAssignment.jsp" style="background:#0383c0;color:#fff;">Step 4<span>Shipment User Assignment</span></a></li>
          </ul>
          <form id="mainForm" method="post" action="ShipmentCreation.jsp">
          <div class="row">
          	<div class="col-md-12">
		<div class="row">
          	<div class="col-md-2">
		<div class="ui-widget">
		<Br><br>
  			<label for="tags">Shipment ID: </label>
  		<input id="tags" class='form-control-no-height' value="SHP-001" disabled>
  		</div></div>
  		</div>
  		</div></div><br>
  		<div class="row">
  			<div class="col-md-offset-1 col-md-10">
  				<div class="well white-background text-center">
  					<div class="row">
  					<div class="col-md-2">
  			<input type="button" class="btn btn-default btn-sm" onclick="colorChange('User001');" id="User001" value="User 001">
  			</div>
  			<div class="col-md-2">
  			<input type="button" class="btn btn-default btn-sm" onclick="colorChange('User002');" id="User002" value="User 002">
  			</div>
  			<div class="col-md-2">
  			<input type="button" class="btn btn-default btn-sm" onclick="colorChange('User003');" id="User003" value="User 003">
  			</div>
  			<div class="col-md-2">
  			<input type="button" class="btn btn-default btn-sm" onclick="colorChange('User004');" id="User004" value="User 004">
  			</div>
  			<div class="col-md-2">
  			<input type="button" class="btn btn-default btn-sm" onclick="colorChange('User005');" id="User005" value="User 005">
  			</div>
  			<div class="col-md-2">
  			<input type="button" class="btn btn-default btn-sm" onclick="colorChange('User006');" id="User006" value="User 006">
  			</div>
  		</div><br>
  		<div class="row">
  			<div class="col-md-2">
  			<input type="button" class="btn btn-default btn-sm" onclick="colorChange('User007');" id="User007" value="User 007">
  			</div>
  			<div class="col-md-2">
  			<input type="button" class="btn btn-default btn-sm" onclick="colorChange('User008');" id="User008" value="User 008">
  			</div>
  			<div class="col-md-2">
  			<input type="button" class="btn btn-default btn-sm" onclick="colorChange('User009');" id="User009" value="User 009">
  			</div>
  			<div class="col-md-2">
  			<input type="button" class="btn btn-default btn-sm" onclick="colorChange('User010');" id="User010" value="User 010">
  			</div>
  			<div class="col-md-2">
  			<input type="button" class="btn btn-default btn-sm" onclick="colorChange('User011');" id="User011" value="User 011">
  			</div>
  			<div class="col-md-2">
  			<input type="button" class="btn btn-default btn-sm" onclick="colorChange('User012');" id="User012" value="User 012">
  			</div>
  		</div>
  		
  		</div>	
  		<a href="ShipmentTagAssignment.jsp" class="btn btn-success pull-left"><i class="fa fa-chevron-left fg-lg"></i>&nbsp;EDIT STEP 3</a>
				<a href="ShipmentUserAssignment.jsp" class="btn btn-success pull-right">SAVE</a>
			
  		<br><br>
  					
  				</div>
  			</div>
  			
  		</form>
			
		</section>
	
    
    
    
				</div>
		<!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0
			</div>
			<strong>Copyright &copy; 2017 <a href="http://tagbox.in">TagBox</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Control Sidebar -->
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->
	
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
<script src="../../dist/js/bootstrap-datepicker.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="../../bootstrap/js/bootstrap.min.js"></script>
	<!-- ChartJS 1.0.1 -->
	<!-- AdminLTE App -->
	<script src="../../dist/js/app.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="../../dist/js/demo.js"></script>

	</body>
	<script>
	function colorChange(e) {

		if($('#'+e).hasClass('btn-default')){
			$('#' + e).removeClass('btn-default'); 
			$('#'+e).addClass('btn-warning');
		  } else {
			  $('#' + e).removeClass('btn-warning'); 
				$('#'+e).addClass('btn-default');
		  }
		
	}
	</script>
</html>
