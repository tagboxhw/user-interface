<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ page import="com.biocon.dao.TripScorecardDao"%>
<%@ page import="com.biocon.dao.TagAssignmentDao"%>
<%@ page import="com.biocon.bean.TripScorecardBean"%>
<%@ page import="com.biocon.bean.TagAssignmentBean"%>
<%@ page import="com.biocon.util.Utils"%>
<%@ page import="java.util.TreeMap"%>
<%@ page import="java.util.Set"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.util.Collection"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@ page import="java.util.Set"%>
<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>
<%@ page import="com.biocon.dao.TemperatureDao"%>
<%@ page import="com.biocon.bean.TemperatureBean"%>

<%!
static final Logger logger = LoggerFactory.getLogger("ShipmentSummary");
%>
<%
String sUsername = (String) session.getAttribute("username");
if(sUsername == null || sUsername.equals("")) {
	session.setAttribute("callingPage", "ShipmentSummary.jsp");
	response.sendRedirect("../../index.html");
}
java.util.Date dt = new java.util.Date();
java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd-MM-yyyy HH:mm");
SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MMM HH:mm");
sdf.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
String currentTime = sdf.format(dt);
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Shipment Summary</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Add these below two lines for autocomplete and the javascript function at the end  -->
<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="../../bootstrap/css/bootstrap.css">
   
<!-- Font Awesome -->
<script src="https://use.fontawesome.com/37ba1872d2.js"></script>
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../../dist/css/tagbox.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
<link href="https://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
  body {
	font-family:  Lato, sans-serif;
	font-weight: 600;
}
</style>
<style>
.save
{
	display:none;
}
.no-border {
	border: none !important;
	border: 0 !important;
}
</style>
<STYLE TYPE="text/css">
			table.sortable span.sortarrow {
   				color: blue;
   				text-decoration: none;
			}
					
		</style>
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
	<div class="wrapper">

		<header class="main-header">
			<!-- Logo -->
			<div>
				<a href="#" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
					<span class="logo-mini"><b>Demo</b></span> <!-- logo for regular state and mobile devices -->
					<span class="logo-lg"><b>Demo</b> </span>
				</a>
			</div>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">

				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
					role="button"> <span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
				</a>
				<label class="navbar-form pull-left" role="search">
					<div class="form-group">
						<b style="padding-left: 15px; font-size: 20px;color:#fff;" class="pull-right">Summary of Today's Shipments</b><br> <b style="padding-left: 15px;color:#fff;"><%= currentTime %></b>
					</div>
				</label>

				<div class="navbar-custom-menu">
					<img src="../../dist/img/Logo.png" class="logo-class pull-right"
						alt="User Image">
				</div>
			</nav>

		</header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->


				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<br>
				<ul class="sidebar-menu">
					<li class="header">MAIN NAVIGATION</li>
					<li class="treeview active"><a href="#"> <i
							class="fa fa-dashboard"></i> <span>Dashboard</span>
					</a>
						<ul class="treeview-menu">
							<li class="active"><a href="TripScorecard.jsp"><i
									class="fa fa-circle-o"></i> Home</a></li>
						</ul></li>
					<!-- <li class="treeview"><a href="#"> <i
							class="fa fa-database"></i> <span>Admin</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="TagAssignment.jsp"><i class="fa fa-circle-o"></i>
									Tag Assignment</a></li>
						</ul></li> -->
					<li class="treeview"><a href="#"> <i
							class="fa fa-th-large"></i> <span>Shipment</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="ShipmentCreation.jsp"><i class="fa fa-circle-o"></i>
									Create Shipment</a></li>
							<li><a href="ShipmentEdit.jsp"><i class="fa fa-circle-o"></i>
									Edit Shipment</a></li>
						</ul></li>
						<li class="treeview"><a href="#"> <i
							class="fa fa-bar-chart"></i> <span>Trip Analysis</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="ETagDashboard.jsp"><i class="fa fa-circle-o"></i>
									Trip Report</a></li>
						</ul></li>
						<!-- <li class="treeview"><a href="#"> <i
							class="fa fa-file-text-o"></i> <span>Reports</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="ETagDashboard.jsp"><i class="fa fa-circle-o"></i>
									Download Historic<br> Data</a></li>
						</ul></li> -->
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper" style="background:white">
			<!-- Content Header (Page header) -->
			
		<section id="mainSection" class="content">
		
			<form id="mainForm" method="post" action="ShipmentSummary.jsp">
				<div id="listView">	
				<br>
				<div class="row text-center">
					<div class="col-md-offset-1 col-md-3">
					<button id="btn1" class="btn btn-info infotext-padding" type="button" style="width:200px" onclick="showTable(1)">All Shipments</button>
					</div>
					<div class="col-md-3">
					<button id="btn2" class="btn btn-success" type="button" style="width:200px" onclick="showTable(2)">Shipments With No Issues</button>
					</div>
					<div class="col-md-3">
					<button id="btn3" class="btn btn-warning" type="button" style="width:200px" onclick="showTable(3)">Shipments Needing Attention</button>
					</div>
				</div>	
				<br><br>
				<div class="row" id="table1">
					<div class="col-md-offset-1 col-md-9">
					<table id="data_table_stationary" class="table table-condensed table-bordered table-striped">
									<thead class="btn-info">
									<th width="150" class="text-center"><b class="font-big-white">&nbsp;</b></th>
									<th width="90" class="text-center"><b class="font-big-white">S1</b></th>
									<th width="150" class="text-center"><b class="font-big-white">S2</b></th>
									<th width="100" class="text-center"><b class="font-big-white">S3</b></th>
									<th width="160" class="text-center"><b class="font-big-white">S4</b></th>
									</thead>
									<tr id="row_stationary" class="text-center"><td>D1</td><td>1</td><td>2</td><td>3</td><td>4</td></tr>
									<tr id="row_stationary" class="text-center"><td>D2</td><td>1</td><td>4</td><td>5</td><td>8</td></tr>
									<tr id="row_stationary" class="text-center"><td>D3</td><td>1</td><td>0</td><td>0</td><td>3</td></tr>
									<tr id="row_stationary" class="text-center"><td>D4</td><td>5</td><td>2</td><td>1</td><td>0</td></tr>
									</table>
					</div>
				</div>
				<div class="row" id="table2" style="display:none;">
					<div class="col-md-offset-1 col-md-9">
					<table id="data_table_stationary" class="table table-condensed table-bordered table-striped">
									<thead class="btn-success">
									<th width="150" class="text-center"><b class="font-big-white">&nbsp;</b></th>
									<th width="90" class="text-center"><b class="font-big-white">S1</b></th>
									<th width="150" class="text-center"><b class="font-big-white">S2</b></th>
									<th width="100" class="text-center"><b class="font-big-white">S3</b></th>
									<th width="160" class="text-center"><b class="font-big-white">S4</b></th>
									</thead>
									<tr id="row_stationary" class="text-center"><td>D1</td><td>1</td><td>2</td><td>3</td><td>4</td></tr>
									<tr id="row_stationary" class="text-center"><td>D2</td><td>1</td><td>4</td><td>5</td><td>8</td></tr>
									<tr id="row_stationary" class="text-center"><td>D3</td><td>1</td><td>0</td><td>0</td><td>3</td></tr>
									<tr id="row_stationary" class="text-center"><td>D4</td><td>5</td><td>2</td><td>1</td><td>0</td></tr>
									</table>
					</div>
				</div>
				<div class="row" id="table3" style="display:none;">
					<div class="col-md-offset-1 col-md-9">
					<table id="data_table_stationary" class="table table-condensed table-bordered table-striped">
									<thead class="btn-warning">
									<th width="150" class="text-center"><b class="font-big-white">&nbsp;</b></th>
									<th width="90" class="text-center"><b class="font-big-white">S1</b></th>
									<th width="150" class="text-center"><b class="font-big-white">S2</b></th>
									<th width="100" class="text-center"><b class="font-big-white">S3</b></th>
									<th width="160" class="text-center"><b class="font-big-white">S4</b></th>
									</thead>
									<tr id="row_stationary" class="text-center"><td>D1</td><td>1</td><td>2</td><td>3</td><td>4</td></tr>
									<tr id="row_stationary" class="text-center"><td>D2</td><td>1</td><td>4</td><td>5</td><td>8</td></tr>
									<tr id="row_stationary" class="text-center"><td>D3</td><td>1</td><td>0</td><td>0</td><td>3</td></tr>
									<tr id="row_stationary" class="text-center"><td>D4</td><td>5</td><td>2</td><td>1</td><td>0</td></tr>
									</table>
					</div>
				</div>
				
										
								<br>
		</div>
		</form>
		</section>
				</div>
		<!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0
			</div>
			<strong>Copyright &copy; 2017 <a href="http://tagbox.in">TagBox</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Control Sidebar -->
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->
	
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>


	<!-- Bootstrap 3.3.6 -->
	<script src="../../bootstrap/js/bootstrap.min.js"></script>
	<!-- ChartJS 1.0.1 -->
	<!-- AdminLTE App -->
	<script src="../../dist/js/app.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="../../dist/js/demo.js"></script>
	<script>
	 function showTable(no) {
		 if(no == 1){
			 $("#table1").show();
			 $("#table2").hide();
			 $("#table3").hide();
			 $("#btn1").addClass("infotext-padding");
			 $("#btn2").removeClass("infotext-padding");
			 $("#btn3").removeClass("infotext-padding");
		 } else if(no == 2) {
			 $("#table1").hide();
			 $("#table2").show();
			 $("#table3").hide();
			 $("#btn2").addClass("infotext-padding");
			 $("#btn1").removeClass("infotext-padding");
			 $("#btn3").removeClass("infotext-padding");
		 } else if(no == 3) {
			 $("#table1").hide();
			 $("#table2").hide();
			 $("#table3").show();
			 $("#btn3").addClass("infotext-padding");
			 $("#btn2").removeClass("infotext-padding");
			 $("#btn1").removeClass("infotext-padding");
		 }
	 }
	</script>
	</body>
	</html>
