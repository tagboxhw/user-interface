<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ page import="com.biocon.dao.TemperatureDao"%>
<%@ page import="com.biocon.dao.TripSummaryDao"%>
<%@ page import="com.biocon.dao.TagAssignmentDao"%>
<%@ page import="com.biocon.dao.TripScorecardDao"%>
<%@ page import="com.biocon.bean.TemperatureBean"%>
<%@ page import="com.biocon.bean.ShipmentDetailsBean"%>
<%@ page import="com.biocon.bean.ShipmentBoxInfo"%>
<%@ page import="com.biocon.bean.ETagSummaryBean"%>
<%@ page import="com.biocon.util.Constants"%>
<%@ page import="com.biocon.util.Utils"%>
<%@ page import="java.util.TreeMap"%>
<%@ page import="java.util.LinkedHashMap"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.util.Collection"%>
<%@ page import="java.util.Set"%>
<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>
<%@ page import="java.util.Date"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="com.biocon.dao.ShipmentDao"%>
<%@ page import="com.biocon.bean.ShipmentBean"%>

<%!
static final Logger logger = LoggerFactory.getLogger("ETagDashboard");
%>

<%
java.util.Date dt = new java.util.Date();
java.text.SimpleDateFormat sdf = 
     new java.text.SimpleDateFormat("dd-MM-yyyy HH:mm");
SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MMM HH:mm");
df.setTimeZone(java.util.TimeZone.getTimeZone("UTC"));
sdf1.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
sdf.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
String currentTime = sdf.format(dt);
String sUsername = (String) session.getAttribute("username");
if(sUsername == null || sUsername.equals("")) {
	session.setAttribute("callingPage", "ETagDashboard.jsp");
	response.sendRedirect("../../index.html");
}
TripScorecardDao clDao = new TripScorecardDao();
TripSummaryDao tsDao = new TripSummaryDao();
TagAssignmentDao taDao = new TagAssignmentDao();
String sShipmentId = request.getParameter("ShipID");
String sBoxId = request.getParameter("BoxID");
ShipmentDao sd = new ShipmentDao();
ShipmentBean sb = new ShipmentBean();
Collection<String> colShipmentIds = sd.selectAllShipmentIds();
Collection<ShipmentBoxInfo> colShipmentIdDetails = null;
ShipmentBoxInfo sbi = new ShipmentBoxInfo();
String sNodeId = "";
TemperatureDao td = new TemperatureDao();
TemperatureBean tdBean = new TemperatureBean();
Collection col = null;
String temperature = "", temperature1 = "";
String time = "", time1 = "";
String sMin1 = "", sMax1 = "";
Date d = null;
Date d2 = null;
Long newTime = null;
TreeMap tm = null;
if(sShipmentId != null && !sShipmentId.equalsIgnoreCase("Select Shipment")) {
	colShipmentIdDetails = taDao.selectShipmentDetails(Integer.parseInt(sShipmentId));
	if(sBoxId == null) {
		for(ShipmentBoxInfo s: colShipmentIdDetails) {
			sBoxId = s.getBoxId();
			break;
		}
	}
	tm = clDao.selectLastSyncTimeForAllSubZones(sShipmentId);
	col = td.selectTemperature(Integer.parseInt(sShipmentId), sBoxId);
	Iterator iterSt = col.iterator();
	int iColSize = col.size()/20;
	int iSize = 0;
	while(iterSt.hasNext()) {
		tdBean = (TemperatureBean) iterSt.next();
			d = df.parse(tdBean.getTimestamp());
			//newTime = d.getTime();
			//newTime +=(330*60*1000);
			//d2 = new Date(newTime);
			temperature += "\"" + tdBean.getTemperature() + "\", ";
			time += "\"" + sdf1.format(d) + "\", ";
			sMin1 += "\"2\", ";
			sMax1 += "\"8\", ";
	}
	if(temperature.equals("") || time.equals("")){
		temperature1 = "00";
		time1 = "00";
	} else {
		if(temperature.length() >= 2)	{
			temperature1 = temperature.substring(0, (temperature.length() - 2));
			sMin1 = sMin1.substring(0, (sMin1.length() - 2));
			sMax1 = sMax1.substring(0, (sMax1.length() - 2));
		}
			if(time.length() >= 2) time1 = time.substring(0, (time.length() - 2));
	}
}

%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Trip Report</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Add these below two lines for autocomplete and the javascript function at the end  -->
<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="../../bootstrap/css/bootstrap.css">
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
   
<!-- Font Awesome -->
<script src="https://use.fontawesome.com/37ba1872d2.js"></script>
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../../dist/css/tagbox.css">
<script src="../../dist/js/tagbox.js"></script>
<link href="../../dist/css/select2.css" rel="stylesheet" />
<script src="../../dist/js/select2.js"></script>
<!-- <script src="../../dist/js/google_charts.js"></script> -->
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
<link href="https://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
  body {
	font-family:  Lato, sans-serif;
	font-weight: 600;
}
</style>
<script>
$(document).ready(function() {
	$("#ShipID").select2();
	$("#BoxID").select2();
});
</script>
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
	<div class="wrapper">

		<header class="main-header">
			<!-- Logo -->
			<div>
				<a href="#" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
					<span class="logo-mini"><b>Demo</b></span> <!-- logo for regular state and mobile devices -->
					<span class="logo-lg"><b>Demo</b> </span>
				</a>
			</div>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">

				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
					role="button"> <span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
				</a>
				<form class="navbar-form pull-left" role="search">
					<div class="form-group">
						<b style="padding-left: 15px; font-size: 20px;color:#fff;">Trip Report</b><br> <b style="padding-left: 15px;color:#fff;"><%= currentTime %></b>
					</div>
				</form>

				<div class="navbar-custom-menu">
					<img src="../../dist/img/Logo.png" class="logo-class pull-right"
						alt="User Image">
				</div>
			</nav>

		</header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->


				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<br>
				<ul class="sidebar-menu">
					<li class="header">MAIN NAVIGATION</li>
					<li class="treeview"><a href="#"> <i
							class="fa fa-dashboard"></i> <span>Dashboard</span>
					</a>
						<ul class="treeview-menu">
							<li class=""><a href="TripScorecard.jsp"><i
									class="fa fa-circle-o"></i> Home</a></li>
						</ul></li>
					<!-- <li class="treeview"><a href="#"> <i
							class="fa fa-database"></i> <span>Admin</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="TagAssignment.jsp"><i class="fa fa-circle-o"></i>
									Tag Assignment</a></li>
						</ul></li> -->
						<li class="treeview"><a href="#"> <i
							class="fa fa-th-large"></i> <span>Shipment</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="ShipmentCreation.jsp"><i class="fa fa-circle-o"></i>
									Create Shipment</a></li>
							<li><a href="ShipmentEdit.jsp"><i class="fa fa-circle-o"></i>
									Edit Shipment</a></li>
						</ul></li>
						<li class="treeview active"><a href="#"> <i
							class="fa fa-bar-chart"></i> <span>Trip Analysis</span>
					</a>
						<ul class="treeview-menu">
							<li class="active"><a href="ETagDashboard.jsp"><i class="fa fa-circle-o"></i>
									Trip Report</a></li>
						</ul></li>
						<!-- <li class="treeview"><a href="#"> <i
							class="fa fa-file-text-o"></i> <span>Reports</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="ETagDashboard.jsp"><i class="fa fa-circle-o"></i>
									Download Historic<br> Data</a></li>
						</ul></li> -->
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			
		<section id="mainSection" class="content white-background">
		<ol class="breadcrumb">
					<li><a href="TripScorecard.jsp"><i class="fa fa-dashboard"></i> Home</a></li>
					<li class="active">Trip Report</li>
				</ol>
			<div class="row">
				
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-12">
						<div class="row"><div class="col-md-4">
						<form method="post" action="ETagDashboard.jsp" id="myForm">
							<Br>
							<div class="well panel-background">
							<label>Shipment ID:&nbsp;&nbsp;&nbsp;</label>
							<select id="ShipID" name="ShipID" class="form-control-no-background pull-center" style="width:50%">
							<option>Select Shipment</option>
          									<%for(String s: colShipmentIds) { 
          									if(sShipmentId != null && sShipmentId.equals(s)) {%>
          									<option selected><%=s %></option>
          									<%} else { %>
          									<option><%=s %></option>
          									<%}
          									}%>
										</select>
										&nbsp;&nbsp;<button class="btn btn-success pull-right" type="submit">GO</button></div>
							<%
							String sTagId = "";
							if(sShipmentId != null && !sShipmentId.equals("") && !sShipmentId.equalsIgnoreCase("Select Shipment")) {
								if(colShipmentIdDetails != null && colShipmentIdDetails.size() != 0) {%>
							<div class="well panel-background">
							<label>Box ID:&nbsp;&nbsp;&nbsp;</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<select id="BoxID" name="BoxID" class="form-control-no-background pull-center" style="width:50%">
          									<%for(ShipmentBoxInfo sbi1: colShipmentIdDetails) { 
          									if(sBoxId.equals(sbi1.getBoxId())) {
          									sTagId = sbi1.getTagId();%>
          									<option selected><%=sbi1.getBoxId() %></option>
          									<%} else { %>
          									<option><%=sbi1.getBoxId() %></option>
          									<%}
          									}%>
										</select>
										&nbsp;&nbsp;<button class="btn btn-success pull-right" type="button" onclick="getDetails();">GO</button></div>
										<%}} %>
										
						</form>
						</div>
						<div class="col-md-8">
						<%if(sShipmentId != null && !sShipmentId.equals("") && !sShipmentId.equalsIgnoreCase("Select Shipment")) {
								if(colShipmentIdDetails != null && colShipmentIdDetails.size() != 0) {%>
								<div id="div1" class="text-center">
														<div class="row"><div class="col-md-offset-4 col-md-4 text-center">
														<b style="color:black;font-size:25px"><%=sTagId %></b>
														</div>
														<div class="col-md-4">
														<%if(tm != null && tm.containsKey(sTagId) && !tm.get(sTagId).equals("")){%>
														<span class="pull-right">Last Sync Time: <%= sdf1.format(df.parse((String)tm.get(sTagId)))%> </span>
														<%} else { %>
														<span class="pull-right">Last Sync Time: NA</span>
														<%} %>
														</div>
														</div>
														<div>
														<canvas id="Chart3" class="chart1" height="50" width="350"></canvas>
														</div>
													</div>
						<%}
						}%>
						</div>
						</div>
						<br><br>
						<%if(sShipmentId != null && !sShipmentId.equals("") && !sShipmentId.equalsIgnoreCase("Select Shipment")) {
							if(colShipmentIdDetails == null || colShipmentIdDetails.size() == 0) {%>
								Shipment Assignment was incomplete! To complete, please <a href="ShipmentEdit.jsp?shipment_id=<%=sShipmentId %>" style="text-decoration:underline;color:blue;">click here.</a>
						<%	} else {%>
					<br>
							
						<input type="hidden" id="boxID" name="boxID" value="">
						<table id="tag_table_step3" class="table table-bordered table-striped table-condensed table-blue-border">
					<tr class="info text-center">
						<th width="10%" class="text-center"><h4>Shipment ID</h4></th>
						<th width="10%" class="text-center"><h4>Box ID</h4></th>
						<th width="10%" class="text-center"><h4>Tag ID</h4></th>
						<th width="15%" class="text-center"><h4>Assign Timestamp</h4></th>
						<th width="15%" class="text-center"><h4>Product</h4></th>
						<th width="10%" class="text-center"><h4>Value</h4></th>
						<th width="15%" class="text-center"><h4>Criticality</h4></th>
						<th width="15%" class="text-center"><h4>Last Sync</h4></th>
					</tr>
						<%for(ShipmentBoxInfo sbi1: colShipmentIdDetails) {
            			%>
					<tr>
						<td class="text-center"><%=sbi1.getShipmentId() %></td>
						<td class="text-center"><%=sbi1.getBoxId() %></td>
						<td class="text-center"><%=sbi1.getTagId() %></td>
						<%if(sbi1.getAssignTimestamp() == null || sbi1.getAssignTimestamp().equals("")) { %>
						<td class="text-center">NA</td>
						<%} else {
							d = df.parse(sbi1.getAssignTimestamp());
			            			//newTime = d.getTime();
			            			//newTime +=(330*60*1000);
			            			//d2 = new Date(newTime); 
			            			%>
						<td class="text-center"><%=sdf1.format(d) %></td>
						<%} %>
						<td class="text-center"><%=sbi1.getProductId() %></td>
          				<td class="text-center"><%=sbi1.getValue() %></td>
          				<%if(sbi1.getCriticality().equalsIgnoreCase("High")){ %>
									<td class="text-center"><span class="badge bg-red">HIGH</span></td>
									<%} else if(sbi1.getCriticality().equalsIgnoreCase("Medium")){%>
									<td class="text-center"><span class="badge bg-orange">MEDIUM</span></td>
									<%} else if(sbi1.getCriticality().equalsIgnoreCase("Low")){%>
									<td class="text-center"><span class="badge bg-green">LOW</span></td>
									<%} else { %>
									<td class="text-center">NA</td>
									<%} %>
          				<%if(sbi1.getLastSync() == null || sbi1.getLastSync().equals("")) { %>
          				<td class="text-center">NA</td>
          				<%} else {
          					d = df.parse(sbi1.getLastSync());
            					//newTime = d.getTime();
            					//newTime +=(330*60*1000);
            					//d2 = new Date(newTime);
            					%>
          				<td class="text-center"><%=sdf1.format(d) %></td>
          				<%} %>
					</tr>
					<%} %>
						</table>
						
											<div class="row">
												
												<%Collection<ETagSummaryBean> colETag2 = tsDao.selectETagSummary(sShipmentId); %>
												<div class="col-md-12 text-center">
													<div class="blue text-center"><b class="font-big-white">Time(mins) spent by Tags in temperature zone</b></div>
													<table id="data_table_stationary" class="table table-bordered table-striped table-condensed table-blue-border">
														<tr class="info text-center">
															<td>&nbsp;</td>
															<td>Last Sync</td>
															<td><-5&deg;C</td>
															<td>-5 to 2 &deg;C</td>
															<td>2 to 8 &deg;C</td>
															<td>9 to 15 &deg;C</td>
															<td>>15&deg;C</td>
														</tr>
														<%
														int iCountRed = 0;
														int iRowColor = 1;
														for(ETagSummaryBean sd1: colETag2) {
															iCountRed = 0;
															if(iRowColor % 2 == 0){%>
														<tr class="panel-stop">
														<%} else { %>
														<tr>
														<%}
														iRowColor++;%>
															<td><%=sd1.getTagId() %></td>
															<%if(sd1.getTimestamp() != null && !sd1.getTimestamp().equals("")) { %>
															<td><%=sdf1.format(df.parse(sd1.getTimestamp())) %></td>
															<%} else { %>
															<td>NA</td>
															<%} %>
															<%if(sd1.getLMF() > 30) {
																iCountRed++;%>
															<td class="alert-text"><%=sd1.getLMF() %></td>
															<%} else { %>
															<td class="success-text"><%=sd1.getLMF() %></td>
															<%} %>
															<%if(sd1.getFTT() > 120) {
																iCountRed++;%>
															<td class="alert-text"><%=sd1.getFTT() %></td>
															<%} else { %>
															<td class="success-text"><%=sd1.getFTT() %></td>
															<%} %>
															<%if(sd1.getTTE() > 500000) {
																iCountRed++;%>
															<td class="alert-text"><%=sd1.getTTE() %></td>
															<%} else { %>
															<td class="success-text"><%=sd1.getTTE() %></td>
															<%} %>
															<%if(sd1.getETF() > 5000) {
																iCountRed++;%>
															<td class="alert-text"><%=sd1.getETF() %></td>
															<%} else { %>
															<td class="success-text"><%=sd1.getETF() %></td>
															<%} %>
															<%if(sd1.getGTF() > 2500) {
																iCountRed++;%>
															<td class="alert-text"><%=sd1.getGTF() %></td>
															<%} else { %>
															<td class="success-text"><%=sd1.getGTF() %></td>
															<%} %>
														</tr>
														<%} %>
														</table>
												</div>
											</div>
										
							<%
						}} %>
						</div>
					</div>
				</div>
			</div>
				
				</section>
				</div>
		<!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0
			</div>
			<strong>Copyright &copy; 2017 <a href="http://tagbox.in">TagBox</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Control Sidebar -->
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->
<!-- ChartJS 1.0.1 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
	

	<!-- Bootstrap 3.3.6 -->
	<script src="../../bootstrap/js/bootstrap.min.js"></script>
	<!-- ChartJS 1.0.1 -->
	<!-- AdminLTE App -->
	<script src="../../dist/js/app.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="../../dist/js/demo.js"></script>
<script>
Chart.defaults.global.legend.display = false;
	
    
var barOptions_stacked = {
	    tooltips: {
	        enabled: true
	    },
	    legend:{
	        display:false
	    },
	    elements: {
            point:{
                radius: 1
            }
        },
	    scales: {
	        xAxes: [{
	                    gridLines: {
	                        display:false
	                    },
	                    ticks: {
	                        autoSkip: true,
	                        maxTicksLimit: 20
	                    }
	                }],
	        yAxes: [{
	                    gridLines: {
	                        display:false
	                    },
	        			ticks: {
	        				stepSize: 10,
	        				suggestedMin: -10,
	        				suggestedMax: 40
	        			}
	                }]
	        },
	        fill: false,
            lineTension: 0,
            backgroundColor: "rgba(75,192,192,0.4)",
            borderColor: "rgba(75,192,192,1)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            spanGaps: false,
	};
	
	var ctx2 = document.getElementById("Chart3");
	ctx2.height = 100;
	var myChart2 = new Chart(ctx2, {
	    type: 'line',
	    data: {
	    	labels : [<%=time1%>],
			datasets : [ {
				label : ["Temperature"],
				fill: false,
				borderColor: "black",
				data : [<%=temperature1%>]
			}, 
			{
				label : ["Min Threshold"],
				backgroundColor: "rgba(54, 162, 235, 0.1)",
				data : [<%= sMin1 %>]
			}, {
				label : ["Max Threshold"],
				backgroundColor: "rgba(54, 162, 235, 0.1)",
				data : [<%= sMax1 %>]
			}]
	    },
	    options: barOptions_stacked
	});
	
function getDetails(){
	var tagid = $("#BoxID").val();
	window.location="ETagDashboard.jsp?ShipID="+$("#ShipID").val()+"&BoxID="+tagid;
}

function showPrev() {
	if($("#div1").css('display') == 'block'){
	    return;
	} else if($("#div2").css('display') == 'block'){
		$("#div2").hide();
		$("#div1").show();
		 $("#PrevBtn").hide();
		 $("#NextBtn").show();
	}  else if($("#div3").css('display') == 'block'){
		$("#div3").hide();
		$("#div2").show();
		
		 $("#PrevBtn").show();
		 $("#NextBtn").show();
	}
	else if($("#div4").css('display') == 'block'){
		$("#div4").hide();
		$("#div3").show();
		
		$("#PrevBtn").show();
		$("#NextBtn").show();
	}
}


function showNext() {
	if($("#div1").css('display') == 'block'){
		$("#div2").show();
		
		$("#div1").hide();
		$("#NextBtn").show();
		$("#PrevBtn").show();
	} else if($("#div2").css('display') == 'block'){
		$("#div3").show();
		
		$("#div2").hide();
		$("#NextBtn").show();
		$("#PrevBtn").show();
	} else if($("#div3").css('display') == 'block'){
		$("#div4").show();
		
		$("#div3").hide();
		$("#NextBtn").hide();
		$("#PrevBtn").show();
	}
}

</script>


	</body>
</html>
