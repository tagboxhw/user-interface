package com.biocon.util;

import java.io.DataInputStream;
import java.io.FileInputStream;

/**
 * @author kkumbaji Desc : This call is used to read a file from
 *         the hard disk and pass the file pointer back
 * 
 */
public class FileReader
    {

        /**
         * @Desc - The method takes the file path and the finle name as input
         *       and creates a socket connection to the actual file and passes
         *       the file pointer to the requesting program
         * 
         * @param strFileName -
         *            The path and name of the file
         * @return - The file pointer (as DataInputStream )
         */
        public DataInputStream readFile(String strFileName)
            {
                
                DataInputStream in = null;
                // args.length is equivalent to argc in C
                if (strFileName != null)
                    {
                        try
                            {
                                // Open the file that is the first
                                // command line parameter
                                FileInputStream fstream = new FileInputStream(
                                        strFileName);

                                // Convert our input stream to a
                                // DataInputStream
                                in = new DataInputStream(fstream);

                                // Continue to read lines while
                                // there are still some left to read
                              /*  while (in.available() != 0)
                                    {
                                        // Print file line to screen
                                        System.out.println(this.getClass().getName() + in.readLine());
                                    }*/

                                //in.close();
                            } catch (Exception e)
                            {
                                System.err.println(this.getClass() + ": File input error");
                            }
                    } else
                    System.out.println(this.getClass().getName() + "Invalid parameters");
                //System.out.println(this.getClass().getName() + "File pointer returned succesfully ");
                return in;
            }
    }
