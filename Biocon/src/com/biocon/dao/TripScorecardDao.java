package com.biocon.dao;

import java.util.Collection;
import java.util.Iterator;
import java.util.TreeMap;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.biocon.bean.ETagSummaryBean;
import com.biocon.bean.TripScorecardBean;
import com.biocon.util.Constants;

public class TripScorecardDao {
	private Client client; 
	static final Logger logger = LoggerFactory
			.getLogger(TripScorecardDao.class);
	
	private void init(){ 
	      this.client = ClientBuilder.newClient(); 
	   }  
	
	public Collection<TripScorecardBean> selectAllTripScorecardRecords() {
		init();
		GenericType<Collection<TripScorecardBean>> tvmBean1 = new GenericType<Collection<TripScorecardBean>>(){};
		Collection<TripScorecardBean> tvmBean = client 
		         .target(Constants.REST_SERVICE_URL + "TripScorecardService/tripscorecard") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(tvmBean1); 
		return tvmBean;
	}
	
	public TreeMap<String, String> selectLastSyncTimeForAllSubZones(String sShipmentId) {
		init();
		GenericType<Collection<TripScorecardBean>> clmBean1 = new GenericType<Collection<TripScorecardBean>>(){};
		WebTarget target = client.target(Constants.REST_SERVICE_URL + "TripScorecardService/lastsynctime").path(sShipmentId);
		Collection<TripScorecardBean> col  = target.request().get(clmBean1);
		TreeMap<String, String> tm = new TreeMap<String, String>();
		Iterator iter = col.iterator();
		TripScorecardBean clmBean = new TripScorecardBean();
		while(iter.hasNext()){
			clmBean = (TripScorecardBean) iter.next();
			System.out.println(clmBean.getTag_Id() + "," + clmBean.getLastSyncTime());
			tm.put(clmBean.getTag_Id(), clmBean.getLastSyncTime());
		}
		return tm;
	}
	
//	
//	public int updateVehicleDetails(String sVehicleId, String sSource,
//			String sDestination, String sDriverId) {
//		Connection conn = null;
//		ResultSet rs = null;
//		PreparedStatement pSelect = null;
//		String updateSql = "";
//		int iReturn = 0;
//		try {
//			TreeMap tmLocations = clmDao.selectLocations();
//			conn = DbConnection.getConnection();
//			String sSourceTemp = "", sDestTemp = "", sKey = "";
//			Set set = tmLocations.keySet();
//			Iterator iter = set.iterator();
//			while(iter.hasNext()) {
//				sKey = (String) iter.next();
//				clmBean = (ClientLocationMapBean) tmLocations.get(sKey);
//				if(sSource.equals(clmBean.getLocationName())) sSourceTemp = clmBean.getLocation_id();
//				if(sDestination.equals(clmBean.getLocationName())) sDestTemp = clmBean.getLocation_id();
//			}
//			int a = sVehicleId.indexOf("<a");
//			sVehicleId = sVehicleId.substring(0, a);
//			pSelect = conn.prepareStatement(SqlConstants.UpdateVehicleManagement);
//			System.out.println(sSource + ";" + sDestination + ";" + sDriverId + ";" + sVehicleId);
//			pSelect.setString(1, sSourceTemp);
//			pSelect.setString(2, sDestTemp);
//			pSelect.setString(3, sVehicleId);
//			//pSelect.setString(4, sVehicleId);
//			iReturn = pSelect.executeUpdate();
//		} catch (Exception e) {
//			e.printStackTrace();
//		} finally {
//			try {
//				if (rs != null)
//					rs.close();
//				DbConnection.closeConnection();
//			} catch (SQLException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//		return iReturn;
//	}
}
