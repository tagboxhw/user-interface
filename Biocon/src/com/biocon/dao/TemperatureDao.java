package com.biocon.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.Vector;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.biocon.bean.TemperatureBean;
import com.biocon.util.Constants;
import com.biocon.util.DbConnection;
import com.biocon.util.SqlConstants;

public class TemperatureDao {
	private Client client; 
	static final Logger logger = LoggerFactory
			.getLogger(TemperatureDao.class);
	
	private void init(){ 
	      this.client = ClientBuilder.newClient(); 
	   }  
	
	public Collection<TemperatureBean> getTemperatureData() {
		init();
		GenericType<Collection<TemperatureBean>> tvmBean1 = new GenericType<Collection<TemperatureBean>>(){};
		Collection<TemperatureBean> tvmBean = client 
		         .target(Constants.REST_SERVICE_URL + "TemperatureService/temperature") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(tvmBean1); 
		return tvmBean;
	}
	
	public Collection<TemperatureBean> getTemperatureData(String sNodeId) {
		init();
		Collection<TemperatureBean> col1 = new Vector();
		GenericType<Collection<TemperatureBean>> tvmBean1 = new GenericType<Collection<TemperatureBean>>(){};
		Collection<TemperatureBean> tvmBean = client 
		         .target(Constants.REST_SERVICE_URL + "TemperatureService/temperature") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(tvmBean1); 
		for(TemperatureBean tb: tvmBean) {
			if(tb.getNode_Id().equals(sNodeId)) {
				col1.add(tb);
			}
		}
		return col1;
	}
	
	public Collection<TemperatureBean> selectTemperature(int sShipmentId, String sBoxId) {
		Collection<TemperatureBean> col = new Vector<TemperatureBean>();
		Connection conn = null;
		PreparedStatement psSelect = null;
		ResultSet rs = null;
		TemperatureBean temperatureBean = new TemperatureBean();
		Statement statement = null;
		try {
			conn = DbConnection.getConnection();
			try {
				statement = conn.createStatement();
			} catch (Exception e) {
				e.printStackTrace();
			}
			psSelect = conn.prepareStatement(SqlConstants.GetTemperatureDetails);
			psSelect.setInt(1, sShipmentId);
			rs = psSelect.executeQuery();
			while (rs.next()) {
				if(rs.getString(2) != null && ((sBoxId.equals("All")) || rs.getString(2).equals(sBoxId))) {
					temperatureBean = new TemperatureBean();
					if(rs.getString(1) == null || rs.getString(1).equals("")) {
						temperatureBean.setZone_Id("");
					} else {
						temperatureBean.setZone_Id(rs.getString(1));
					}
					if(rs.getString(2) == null || rs.getString(2).equals("")) {
						temperatureBean.setNode_Id("");
					} else {
						temperatureBean.setNode_Id(rs.getString(2));
					}
					if(rs.getString(3) == null || rs.getString(3).equals("")) {
						temperatureBean.setTimestamp("");
					} else {
						temperatureBean.setTimestamp(rs.getString(3));
					}
					if(rs.getString(4) == null || rs.getString(4).equals("")) {
						temperatureBean.setTemperature("");
					} else {
						temperatureBean.setTemperature(rs.getString(4));
					}
				
				col.add(temperatureBean);
				}
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (psSelect != null)
					psSelect.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return col;
	}
	
}
