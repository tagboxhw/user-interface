package com.biocon.dao;

import java.util.Collection;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.biocon.bean.ShipmentDetailsBean;
import com.biocon.bean.ETagSummaryBean;
import com.biocon.util.Constants;

public class TripSummaryDao {
	private Client client; 
	static final Logger logger = LoggerFactory
			.getLogger(TripSummaryDao.class);
	
	private void init(){ 
	      this.client = ClientBuilder.newClient(); 
	   }  
	
	public Collection<ShipmentDetailsBean> selectAllShipmentDetails() {
		init();
		GenericType<Collection<ShipmentDetailsBean>> tvmBean1 = new GenericType<Collection<ShipmentDetailsBean>>(){};
		Collection<ShipmentDetailsBean> tvmBean = client 
		         .target(Constants.REST_SERVICE_URL + "TripSummaryService/tripsummary") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(tvmBean1); 
		return tvmBean;
	}
	
	public Collection<ETagSummaryBean> selectETagSummary() {
		init();
		GenericType<Collection<ETagSummaryBean>> tvmBean1 = new GenericType<Collection<ETagSummaryBean>>(){};
		Collection<ETagSummaryBean> tvmBean = client 
		         .target(Constants.REST_SERVICE_URL + "TripSummaryService/etagsummary") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(tvmBean1); 
		return tvmBean;
	}
	
	public Collection<ETagSummaryBean> selectETagSummary(String sShipmentId){ 
		init();
		GenericType<Collection<ETagSummaryBean>> tvmBean1 = new GenericType<Collection<ETagSummaryBean>>(){};
		WebTarget target = client.target(Constants.REST_SERVICE_URL + "TripSummaryService/etagsummary").path(sShipmentId);
		Collection<ETagSummaryBean> tvmBean  = target.request().get(tvmBean1);
		return tvmBean; 
	   } 
	
}
