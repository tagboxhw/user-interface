package com.biocon.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Vector;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.biocon.bean.ShipmentBean;
import com.biocon.util.Constants;
import com.biocon.util.DbConnection;
import com.biocon.util.SqlConstants;
import com.biocon.bean.ShipmentBoxInfo;
import com.biocon.bean.ShipmentLocations;
import com.biocon.bean.ShipmentProducts;
import com.biocon.bean.ShipmentTags;
import com.biocon.bean.ShipmentTransitInfo;
import com.biocon.bean.ShipmentUsers;
import com.biocon.bean.TemperatureBean;

public class ShipmentDao {
	private Client client; 
	static final Logger logger = LoggerFactory
			.getLogger(ShipmentDao.class);
	
	private void init(){ 
	      this.client = ClientBuilder.newClient(); 
	   }  
	
	public String getNewShipmentId() {
		init();
		GenericType<String> tvmBean1 = new GenericType<String>(){};
		String tvmBean = client 
		         .target(Constants.REST_SERVICE_URL + "ShipmentService/nextshipmentid") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(tvmBean1); 
		return tvmBean;
	}
	
	public Collection<ShipmentBean> getShipmentDetails() {
		init();
		GenericType<Collection<ShipmentBean>> tvmBean1 = new GenericType<Collection<ShipmentBean>>(){};
		Collection<ShipmentBean> tvmBean = client 
		         .target(Constants.REST_SERVICE_URL + "ShipmentService/shipmentdetails") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(tvmBean1); 
		return tvmBean;
	}
	
	public Collection<ShipmentLocations> getShipmentLocations() {
		init();
		GenericType<Collection<ShipmentLocations>> tvmBean1 = new GenericType<Collection<ShipmentLocations>>(){};
		Collection<ShipmentLocations> tvmBean = client 
		         .target(Constants.REST_SERVICE_URL + "ShipmentService/shipmentlocations") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(tvmBean1); 
		return tvmBean;
	}
	
	public Collection<ShipmentTags> getShipmentTags() {
		init();
		GenericType<Collection<ShipmentTags>> tvmBean1 = new GenericType<Collection<ShipmentTags>>(){};
		Collection<ShipmentTags> tvmBean = client 
		         .target(Constants.REST_SERVICE_URL + "ShipmentService/shipmenttags") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(tvmBean1); 
		return tvmBean;
	}
	
	public Collection<ShipmentProducts> getShipmentProducts() {
		init();
		GenericType<Collection<ShipmentProducts>> tvmBean1 = new GenericType<Collection<ShipmentProducts>>(){};
		Collection<ShipmentProducts> tvmBean = client 
		         .target(Constants.REST_SERVICE_URL + "ShipmentService/shipmentproducts") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(tvmBean1); 
		return tvmBean;
	}
	
	public Collection<ShipmentUsers> getShipmentUsers() {
		init();
		GenericType<Collection<ShipmentUsers>> tvmBean1 = new GenericType<Collection<ShipmentUsers>>(){};
		Collection<ShipmentUsers> tvmBean = client 
		         .target(Constants.REST_SERVICE_URL + "ShipmentService/shipmentusers") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(tvmBean1); 
		return tvmBean;
	}
	
	//public String insertShipmentStep1Data(String sShipmentId, String sSource, String sDestination, String sETD, String sETA){ 
	//	Form form = new Form();
	//     form.param("shipmentid", sShipmentId);
	//      form.param("source", sSource);
	//      form.param("destination", sDestination);
	//      form.param("etd", sETD);
	//      form.param("eta", sETA);
	//      init();
	//      String callResult = client
	//         .target(Constants.REST_SERVICE_URL + "ShipmentService/step1insert")
	//         .request(MediaType.APPLICATION_XML)
	//         .post(Entity.entity(form,
	//            MediaType.APPLICATION_FORM_URLENCODED_TYPE),
	//            String.class);
	//	return callResult; 
	//   } 
	
	//The above method can be uncommented instead of this method once Somesh is able to resolve the error: ERROR: cannot open INSERT query as cursor
	public String insertShipmentStep1Data(String sShipmentId, String sSource, String sDestination, String sETD, String sETA, String sInvoice) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Statement statement = null;
		String sReturn = "";
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			conn = DbConnection.getConnection();
			try {
				pstmt = conn
						.prepareStatement(SqlConstants.InsertStep1);
				df.setTimeZone(java.util.TimeZone.getTimeZone("UTC"));
				sdf.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
				Date dateObj = sdf.parse(sETD);
				pstmt.setInt(1, Integer.parseInt(sShipmentId));
				pstmt.setString(2, sSource);
				pstmt.setString(3, sDestination);
				//System.out.println(sETD);
				//Date dateObj = sdf.parse(sETD);
				//System.out.println(dateObj.getYear() + " " + dateObj.getMonth() + " " + dateObj.getDate());
				//Timestamp tstamp = new Timestamp(dateObj.getTime()); 
				//System.out.println(tstamp);
				pstmt.setTimestamp(4, java.sql.Timestamp.valueOf(df.format(dateObj)));
				//dateObj = sdf.parse(sETA);
				dateObj = sdf.parse(sETA);
				pstmt.setTimestamp(5, java.sql.Timestamp.valueOf(df.format(dateObj)));
				pstmt.setString(6, sInvoice);
			} catch (Exception e) {
				e.printStackTrace();
			}
			int iReturn = pstmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return sReturn;
	}
	
	public String insertShipmentStep2Data(String sShipmentId, String sSourceId, String sDestId, String sEtd, String sEta, String sMode, String sCarrier) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Statement statement = null;
		String sReturn = "";
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			conn = DbConnection.getConnection();
			String[] source = sSourceId.split(";");
			String[] dest = sDestId.split(";");
			String[] etd = sEtd.split(";");
			String[] eta = sEta.split(";");
			String[] mode = sMode.split(";");
			String[] carrier = sCarrier.split(";");
			try {
				pstmt = conn
						.prepareStatement(SqlConstants.InsertStep2);
				df.setTimeZone(java.util.TimeZone.getTimeZone("UTC"));
				sdf.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
				Date dateObj = null;
				for(int i = 0; i<source.length;i++) {
					if(source[i] == null || source[i].equals("")) continue;
					pstmt.setInt(1, Integer.parseInt(sShipmentId));
					pstmt.setInt(2, (i-1));
					pstmt.setString(3, source[i]);
					if(dest[i] == null || dest[i].equals("undefined")) {
						pstmt.setString(4, null);
					} else {
						pstmt.setString(4, dest[i]);
					}
					
					dateObj = sdf.parse(etd[i]);
					pstmt.setTimestamp(5, java.sql.Timestamp.valueOf(df.format(dateObj)));
					dateObj = sdf.parse(eta[i]);
					pstmt.setTimestamp(6, java.sql.Timestamp.valueOf(df.format(dateObj)));
					pstmt.setString(7, mode[i]);
					pstmt.setString(8, carrier[i]);
					int iReturn = pstmt.executeUpdate();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return sReturn;
	}
	
	public String insertShipmentStep3Data(String sShipmentId, String sBoxId, String sTagId, String sProductId, String sValue, String sCriticality) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Statement statement = null;
		String sReturn = "";
		try {
			conn = DbConnection.getConnection();
			String[] box = sBoxId.split(";");
			String[] tag = sTagId.split(";");
			String[] product = sProductId.split(";");
			String[] value = sValue.split(";");
			String[] criticality = sCriticality.split(";");
			try {
				
				for(int i = 0; i<box.length;i++) {
					if(box[i] == null || box[i].equals("")) continue;
					pstmt = conn
							.prepareStatement(SqlConstants.InsertStep3);
					pstmt.setInt(1, Integer.parseInt(sShipmentId));
					pstmt.setString(2, box[i]);
					pstmt.setString(3, tag[i]);
					pstmt.setString(4, product[i]);
					pstmt.setFloat(5, Float.parseFloat(value[i]));
					pstmt.setString(6, criticality[i]);
					int iReturn = pstmt.executeUpdate();
					pstmt = conn
							.prepareStatement(SqlConstants.InsertProduct);
						pstmt.setString(1, product[i]);
						pstmt.setString(2, product[i]);
						iReturn = pstmt.executeUpdate();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return sReturn;
	}
	
	public String insertShipmentStep4Data(String sShipmentId, String sUsers) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Statement statement = null;
		String sReturn = "";
		try {
			conn = DbConnection.getConnection();
			try {
				pstmt = conn
						.prepareStatement(SqlConstants.InsertStep4);
				pstmt.setString(1, sUsers.substring(1));
				pstmt.setInt(2, Integer.parseInt(sShipmentId));
			} catch (Exception e) {
				e.printStackTrace();
			}
			int iReturn = pstmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return sReturn;
	}
	
	public Collection<String> selectAllShipmentIds() {
		Collection<String> col = new ArrayList<String>();
		Connection conn = null;
		PreparedStatement psSelect = null;
		ResultSet rs = null;
		Statement statement = null;
		String sShipmentId = "";
		try {
			conn = DbConnection.getConnection();

			// Create and execute a SELECT SQL statement.
			// Create and execute a SELECT SQL statement.
			try {
				statement = conn.createStatement();
			} catch (Exception e) {
				e.printStackTrace();
			}
			rs = statement.executeQuery(SqlConstants.GetShipmentId);

			while (rs.next()) {
				sShipmentId = (rs.getString(1));
				col.add(sShipmentId);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (psSelect != null)
					psSelect.close();
				DbConnection.closeConnection();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return col;
	}
	
	public Collection<ShipmentBean> selectShipmentIdDetails() {
		Connection conn = null;
		PreparedStatement psSelect = null;
		ResultSet rs = null;
		Statement statement = null;
		ShipmentBean sb = new ShipmentBean();
		Collection<ShipmentBean> col = new Vector<ShipmentBean>();
		try {
			conn = DbConnection.getConnection();
			try {
				statement = conn.createStatement();
			} catch (Exception e) {
				e.printStackTrace();
			}
			psSelect = conn.prepareStatement(SqlConstants.GetShipmentIdDetails);
			rs = psSelect.executeQuery();
			while (rs.next()) {
				sb = new ShipmentBean();
				sb.setShipmentId(rs.getInt(1));
				if(rs.getString(2) == null || rs.getString(2).equals("")) {
					sb.setOrigin("");
				} else {
					sb.setOrigin(rs.getString(2));
				}
				if(rs.getString(3) == null || rs.getString(3).equals("")) {
					sb.setDestination("");
				} else {
					sb.setDestination(rs.getString(3));
				}
				if(rs.getString(4) == null || rs.getString(4).equals("")) {
					sb.setETD("");
				} else {
					sb.setETD(rs.getString(4));
				}
				if(rs.getString(5) == null || rs.getString(5).equals("")) {
					sb.setETA("");
				} else {
					sb.setETA(rs.getString(5));
				}
				if(rs.getString(6) == null || rs.getString(6).equals("")) {
					sb.setUser("");
				} else {
					sb.setUser(rs.getString(6));
				}
				if(rs.getString(7) == null || rs.getString(7).equals("")) {
					sb.setInvoiceNumber("");
				} else {
					sb.setInvoiceNumber(rs.getString(7));
				}
				col.add(sb);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (psSelect != null)
					psSelect.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return col;
	}
	
	public Collection<ShipmentTransitInfo> selectShipmentTransitDetails(String sShipmentId) {
		Connection conn = null;
		PreparedStatement psSelect = null;
		ResultSet rs = null;
		Statement statement = null;
		ShipmentTransitInfo sb = new ShipmentTransitInfo();
		Collection<ShipmentTransitInfo> col = new ArrayList<ShipmentTransitInfo>();
		try {
			conn = DbConnection.getConnection();
			try {
				statement = conn.createStatement();
			} catch (Exception e) {
				e.printStackTrace();
			}
			psSelect = conn.prepareStatement(SqlConstants.GetShipmentTransitDetails);
			psSelect.setInt(1, Integer.parseInt(sShipmentId));
			rs = psSelect.executeQuery();
			while (rs.next()) {
				sb = new ShipmentTransitInfo();
				sb.setShipmentId(rs.getInt(1));
				sb.setLegId(rs.getInt(2));
				if(rs.getString(3) == null || rs.getString(3).equals("")) {
					sb.setOrigin("");
				} else {
					sb.setOrigin(rs.getString(3));
				}
				if(rs.getString(4) == null || rs.getString(4).equals("")) {
					sb.setDestination("");
				} else {
					sb.setDestination(rs.getString(4));
				}
				if(rs.getString(5) == null || rs.getString(5).equals("")) {
					sb.setETD("");
				} else {
					sb.setETD(rs.getString(5));
				}
				if(rs.getString(6) == null || rs.getString(6).equals("")) {
					sb.setETA("");
				} else {
					sb.setETA(rs.getString(6));
				}
				if(rs.getString(11) == null || rs.getString(11).equals("")) {
					sb.setMode("");
				} else {
					sb.setMode(rs.getString(11));
				}
				if(rs.getString(12) == null || rs.getString(12).equals("")) {
					sb.setCarrier("");
				} else {
					sb.setCarrier(rs.getString(12));
				}
				col.add(sb);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (psSelect != null)
					psSelect.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return col;
	}
	
	public Collection<ShipmentBoxInfo> selectShipmentBoxDetails(String sShipmentId) {
		Connection conn = null;
		PreparedStatement psSelect = null;
		ResultSet rs = null;
		Statement statement = null;
		ShipmentBoxInfo sb = new ShipmentBoxInfo();
		Collection<ShipmentBoxInfo> col = new ArrayList<ShipmentBoxInfo>();
		try {
			conn = DbConnection.getConnection();
			try {
				statement = conn.createStatement();
			} catch (Exception e) {
				e.printStackTrace();
			}
			psSelect = conn.prepareStatement(SqlConstants.GetShipmentTagDetails);
			psSelect.setInt(1, Integer.parseInt(sShipmentId));
			rs = psSelect.executeQuery();
			while (rs.next()) {
				sb = new ShipmentBoxInfo();
				sb.setShipmentId(rs.getInt(1));
				if(rs.getString(2) == null || rs.getString(2).equals("")) {
					sb.setBoxId("");
				} else {
					sb.setBoxId(rs.getString(2));
				}
				if(rs.getString(3) == null || rs.getString(3).equals("")) {
					sb.setTagId("");
				} else {
					sb.setTagId(rs.getString(3));
				}
				if(rs.getString(5) == null || rs.getString(5).equals("")) {
					sb.setProductId("");
				} else {
					sb.setProductId(rs.getString(5));
				}
				if(rs.getString(6) == null || rs.getString(6).equals("")) {
					sb.setValue(0);
				} else {
					sb.setValue(rs.getFloat(6));
				}
				if(rs.getString(7) == null || rs.getString(7).equals("")) {
					sb.setCriticality("");
				} else {
					sb.setCriticality(rs.getString(7));
				}
				col.add(sb);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (psSelect != null)
					psSelect.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return col;
	}
	
	public String updateShipmentStep1Data(String sShipmentId, String sSource, String sDestination, String sETD, String sETA, String sInvoice) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Statement statement = null;
		String sReturn = "";
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			conn = DbConnection.getConnection();
			try {
				df.setTimeZone(java.util.TimeZone.getTimeZone("UTC"));
				sdf.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
				Date dateObj = null;
				//System.out.println(sETD + ";" + sETA);
				pstmt = conn
						.prepareStatement(SqlConstants.UpdateStep1);
				pstmt.setString(1, sSource);
				pstmt.setString(2, sDestination);
				dateObj = sdf.parse(sETD);
				pstmt.setTimestamp(3, java.sql.Timestamp.valueOf(df.format(dateObj)));
				dateObj = sdf.parse(sETA);
				pstmt.setTimestamp(4, java.sql.Timestamp.valueOf(df.format(dateObj)));
				pstmt.setString(5, sInvoice);
				pstmt.setInt(6, Integer.parseInt(sShipmentId));
			} catch (Exception e) {
				e.printStackTrace();
			}
			int iReturn = pstmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return sReturn;
	}
	
	
	public String updateShipmentStep1Data1(String sShipmentId, String sSource, String sDestination, String sETD, String sETA){ 
			Form form = new Form();
		     form.param("shipmentid", sShipmentId);
		      form.param("source", sSource);
		      form.param("destination", sDestination);
		      form.param("etd", sETD);
		      form.param("eta", sETA);
		      init();
		      String callResult = client
		         .target(Constants.REST_SERVICE_URL + "ShipmentService/step1update")
		         .request(MediaType.APPLICATION_XML)
		         .post(Entity.entity(form,
		            MediaType.APPLICATION_FORM_URLENCODED_TYPE),
		            String.class);
			return callResult; 
		   } 
	
	public String updateShipmentStep2Data(String sShipmentId, String sSourceId, String sDestId, String sEtd, String sEta, String sMode, String sCarrier) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Statement statement = null;
		String sReturn = "";
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			conn = DbConnection.getConnection();
			String[] source = sSourceId.split(";");
			String[] dest = sDestId.split(";");
			String[] etd = sEtd.split(";");
			String[] eta = sEta.split(";");
			String[] mode = sMode.split(";");
			String[] carrier = sCarrier.split(";");
			try {
				Collection col = selectShipmentTransitDetails(sShipmentId);
				df.setTimeZone(java.util.TimeZone.getTimeZone("UTC"));
				sdf.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
				Date dateObj = null;
				if(col.size() > (source.length-1)) {
					//some legs have been deleted. Delete those legs from db and update the remaining ones
					pstmt = conn
							.prepareStatement(SqlConstants.DeleteStep2);
					for(int i=(source.length-1); i<(col.size()); i++) {
						pstmt.setInt(1, Integer.parseInt(sShipmentId));
						pstmt.setInt(2, i);
						int iReturn = pstmt.executeUpdate();
					}
					pstmt = conn
							.prepareStatement(SqlConstants.UpdateStep2);
					for(int i = 0; i<source.length;i++) {
						if(source[i] == null || source[i].equals("")) continue;
						pstmt.setString(1, source[i]);
						if(dest[i] == null || dest[i].equals("undefined")) {
							pstmt.setString(2, null);
						} else {
							pstmt.setString(2, dest[i]);
						}
						dateObj = sdf.parse(etd[i]);
						pstmt.setTimestamp(3, java.sql.Timestamp.valueOf(df.format(dateObj)));
						dateObj = sdf.parse(eta[i]);
						pstmt.setTimestamp(4, java.sql.Timestamp.valueOf(df.format(dateObj)));
						pstmt.setString(5, mode[i]);
						pstmt.setString(6, carrier[i]);
						pstmt.setInt(7, Integer.parseInt(sShipmentId));
						pstmt.setInt(8, (i-1));
						int iReturn = pstmt.executeUpdate();
					}
				} else if(col.size() < (source.length-1)) {
					//some legs have been added. update the existing ones and insert the new ones
					pstmt = conn
							.prepareStatement(SqlConstants.UpdateStep2);
					for(int i = 0; i<(col.size()+1);i++) {
						if(source[i] == null || source[i].equals("")) continue;
						pstmt.setString(1, source[i]);
						if(dest[i] == null || dest[i].equals("undefined")) {
							pstmt.setString(2, null);
						} else {
							pstmt.setString(2, dest[i]);
						}
						dateObj = sdf.parse(etd[i]);
						pstmt.setTimestamp(3, java.sql.Timestamp.valueOf(df.format(dateObj)));
						dateObj = sdf.parse(eta[i]);
						pstmt.setTimestamp(4, java.sql.Timestamp.valueOf(df.format(dateObj)));
						pstmt.setString(5, mode[i]);
						pstmt.setString(6, carrier[i]);
						pstmt.setInt(7, Integer.parseInt(sShipmentId));
						pstmt.setInt(8, (i-1));
						int iReturn = pstmt.executeUpdate();
					}
					
					try {
						pstmt = conn
								.prepareStatement(SqlConstants.InsertStep2);
						for(int i = (col.size()+1); i<source.length;i++) {
							if(source[i] == null || source[i].equals("")) continue;
							pstmt.setInt(1, Integer.parseInt(sShipmentId));
							pstmt.setInt(2, (i-1));
							pstmt.setString(3, source[i]);
							if(dest[i] == null || dest[i].equals("undefined")) {
								pstmt.setString(4, null);
							} else {
								pstmt.setString(4, dest[i]);
							}
							dateObj = sdf.parse(etd[i]);
							pstmt.setTimestamp(5, java.sql.Timestamp.valueOf(df.format(dateObj)));
							dateObj = sdf.parse(eta[i]);
							pstmt.setTimestamp(6, java.sql.Timestamp.valueOf(df.format(dateObj)));
							pstmt.setString(7, mode[i]);
							pstmt.setString(8, carrier[i]);
							int iReturn = pstmt.executeUpdate();
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					//update the existing legs
					pstmt = conn
							.prepareStatement(SqlConstants.UpdateStep2);
					for(int i = 0; i<source.length;i++) {
						if(source[i] == null || source[i].equals("")) continue;
						pstmt.setString(1, source[i]);
						if(dest[i] == null || dest[i].equals("undefined")) {
							pstmt.setString(2, null);
						} else {
							pstmt.setString(2, dest[i]);
						}
						dateObj = sdf.parse(etd[i]);
						pstmt.setTimestamp(3, java.sql.Timestamp.valueOf(df.format(dateObj)));
						dateObj = sdf.parse(eta[i]);
						pstmt.setTimestamp(4, java.sql.Timestamp.valueOf(df.format(dateObj)));
						pstmt.setString(5, mode[i]);
						pstmt.setString(6, carrier[i]);
						pstmt.setInt(7, Integer.parseInt(sShipmentId));
						pstmt.setInt(8, (i-1));
						int iReturn = pstmt.executeUpdate();
					}
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return sReturn;
	}
	
	public String updateShipmentStep3Data(String sShipmentId, String sBoxId, String sTagId, String sProductId, String sValue, String sCriticality) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Statement statement = null;
		String sReturn = "";
		try {
			conn = DbConnection.getConnection();
			try {
				pstmt = conn
						.prepareStatement(SqlConstants.DeleteStep3);
					pstmt.setInt(1, Integer.parseInt(sShipmentId));
					int iReturn = pstmt.executeUpdate();
					insertShipmentStep3Data(sShipmentId, sBoxId, sTagId, sProductId, sValue, sCriticality);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return sReturn;
	}
	
	public String updateShipmentStep4Data(String sShipmentId, String sUsers) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Statement statement = null;
		String sReturn = "";
		try {
			conn = DbConnection.getConnection();
			try {
				pstmt = conn
						.prepareStatement(SqlConstants.InsertStep4);
				pstmt.setString(1, sUsers.substring(1));
				pstmt.setInt(2, Integer.parseInt(sShipmentId));
			} catch (Exception e) {
				e.printStackTrace();
			}
			int iReturn = pstmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return sReturn;
	}
	
	public String endShipment(String sShipmentId, String sEndTime) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Statement statement = null;
		String sReturn = "";
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			conn = DbConnection.getConnection();
			try {
				pstmt = conn
						.prepareStatement(SqlConstants.EndShipment);
				df.setTimeZone(java.util.TimeZone.getTimeZone("UTC"));
				sdf.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
				Date dateObj = sdf.parse(sEndTime);
				pstmt.setTimestamp(1, java.sql.Timestamp.valueOf(df.format(dateObj)));
				pstmt.setInt(2, Integer.parseInt(sShipmentId));
			} catch (Exception e) {
				e.printStackTrace();
			}
			int iReturn = pstmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return sReturn;
	}
	
	public String deleteShipment(String sShipmentId) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Statement statement = null;
		String sReturn = "";
		try {
			conn = DbConnection.getConnection();
			try {
				pstmt = conn
						.prepareStatement(SqlConstants.DeleteShipment);
				pstmt.setInt(1, Integer.parseInt(sShipmentId));
			} catch (Exception e) {
				e.printStackTrace();
			}
			int iReturn = pstmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return sReturn;
	}
	
}
