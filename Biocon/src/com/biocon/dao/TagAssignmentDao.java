package com.biocon.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeMap;
import java.util.Vector;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.biocon.util.DbConnection;
import com.biocon.util.SqlConstants;
import com.biocon.bean.ShipmentBean;
import com.biocon.bean.ShipmentBoxInfo;
import com.biocon.bean.TagAssignmentBean;
import com.biocon.util.Constants;

public class TagAssignmentDao {
	private Client client; 
	static final Logger logger = LoggerFactory
			.getLogger(TagAssignmentDao.class);
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	
	private void init(){ 
	      this.client = ClientBuilder.newClient(); 
	   }  
	
	public Collection<TagAssignmentBean> selectAllTagAssignmentRecords() {
		init();
		GenericType<Collection<TagAssignmentBean>> tvmBean1 = new GenericType<Collection<TagAssignmentBean>>(){};
		Collection<TagAssignmentBean> tvmBean = client 
		         .target(Constants.REST_SERVICE_URL + "TagAssignmentService/tagassignment") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(tvmBean1); 
		return tvmBean;
	}
	
	public int updateTagAssignment(String sTagId, String sShipDate, String sShipId, String sSource, String sDest, String sFlightId, String sCriticalityId, String sMinId, String sMaxId){ 
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement pSelect = null;
		try {
			conn = DbConnection.getConnection();
			pSelect = conn
					.prepareStatement(SqlConstants.UpdateTagAssignment);
			Date dateObj = sdf.parse(sShipDate);
		    Timestamp tstamp = new Timestamp(dateObj.getTime());
			pSelect.setTimestamp(1, tstamp);
			pSelect.setString(2, sShipId);
			pSelect.setString(3, sSource);
			pSelect.setString(4, sDest);
			pSelect.setString(5, sCriticalityId);
			pSelect.setString(6, sSource);
			pSelect.setString(7, sTagId);
			int iReturn = pSelect.executeUpdate();
		} catch (SQLException e) {
			logger.error("localized message: " + e.getLocalizedMessage());
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return 0;
	   } 
	
	public Collection<ShipmentBoxInfo> selectShipmentDetails(int sShipmentId) {
		Collection<ShipmentBoxInfo> col = new Vector<ShipmentBoxInfo>();
		Connection conn = null;
		PreparedStatement psSelect = null;
		ResultSet rs = null;
		ShipmentBoxInfo sb = new ShipmentBoxInfo();
		Statement statement = null;
		try {
			conn = DbConnection.getConnection();
			try {
				statement = conn.createStatement();
			} catch (Exception e) {
				e.printStackTrace();
			}
			psSelect = conn.prepareStatement(SqlConstants.GetShipmentDetails);
			psSelect.setInt(1, sShipmentId);
			rs = psSelect.executeQuery();
			while (rs.next()) {
				if(rs.getInt(1) != sShipmentId) continue;
				sb = new ShipmentBoxInfo();
				sb.setShipmentId(rs.getInt(1));
				if(rs.getString(2) == null || rs.getString(2).equals("")) {
					sb.setBoxId("");
				} else {
					sb.setBoxId(rs.getString(2));
				}
				if(rs.getString(3) == null || rs.getString(3).equals("")) {
					sb.setTagId("");
				} else {
					sb.setTagId(rs.getString(3));
				}
				if(rs.getString(4) == null || rs.getString(4).equals("")) {
					sb.setAssignTimestamp("");
				} else {
					sb.setAssignTimestamp(rs.getString(4));
				}
				if(rs.getString(5) == null || rs.getString(5).equals("")) {
					sb.setProductId("");
				} else {
					sb.setProductId(rs.getString(5));
				}
				sb.setValue(rs.getInt(6));
				if(rs.getString(7) == null || rs.getString(7).equals("")) {
					sb.setCriticality("");
				} else {
					sb.setCriticality(rs.getString(7));
				}
				if(rs.getString(8) == null || rs.getString(8).equals("")) {
					sb.setLastSync("");
				} else {
					sb.setLastSync(rs.getString(8));
				}
				col.add(sb);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (psSelect != null)
					psSelect.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return col;
	}
	
//	
//	public int updateVehicleDetails(String sVehicleId, String sSource,
//			String sDestination, String sDriverId) {
//		Connection conn = null;
//		ResultSet rs = null;
//		PreparedStatement pSelect = null;
//		String updateSql = "";
//		int iReturn = 0;
//		try {
//			TreeMap tmLocations = clmDao.selectLocations();
//			conn = DbConnection.getConnection();
//			String sSourceTemp = "", sDestTemp = "", sKey = "";
//			Set set = tmLocations.keySet();
//			Iterator iter = set.iterator();
//			while(iter.hasNext()) {
//				sKey = (String) iter.next();
//				clmBean = (ClientLocationMapBean) tmLocations.get(sKey);
//				if(sSource.equals(clmBean.getLocationName())) sSourceTemp = clmBean.getLocation_id();
//				if(sDestination.equals(clmBean.getLocationName())) sDestTemp = clmBean.getLocation_id();
//			}
//			int a = sVehicleId.indexOf("<a");
//			sVehicleId = sVehicleId.substring(0, a);
//			pSelect = conn.prepareStatement(SqlConstants.UpdateVehicleManagement);
//			System.out.println(sSource + ";" + sDestination + ";" + sDriverId + ";" + sVehicleId);
//			pSelect.setString(1, sSourceTemp);
//			pSelect.setString(2, sDestTemp);
//			pSelect.setString(3, sVehicleId);
//			//pSelect.setString(4, sVehicleId);
//			iReturn = pSelect.executeUpdate();
//		} catch (Exception e) {
//			e.printStackTrace();
//		} finally {
//			try {
//				if (rs != null)
//					rs.close();
//				DbConnection.closeConnection();
//			} catch (SQLException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//		return iReturn;
//	}
}
