package com.biocon.bean;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "shipmentBoxInfo")
public class ShipmentBoxInfo implements Serializable{

	/**
	 * Generated serial version ID
	 */
	private static final long serialVersionUID = -1874474658549948076L;
	private int iShipmentId;
	private String sBoxId;
	private String sTagId;
	private String sAssignTimestamp;
	private String sProductId;
	private float sValue;
	private String sCriticality;
	private String sLastSync;
	
	public ShipmentBoxInfo(){
		super();
	}
	
	public void init() {
		this.iShipmentId = 0;
		this.sBoxId = "";
		this.sTagId = "";
		this.sAssignTimestamp = "";
		this.sProductId = "";
		this.sValue = 0;
		this.sCriticality = "";
		this.sLastSync = "";
	}
	
	public String toString(){
		String buf = null;
		buf = "Shipment Box object: \n";
		buf = buf + "Shipment Id: " + iShipmentId + "\n";
		buf = buf + "sBoxId: " + sBoxId + "\n";
		buf = buf + "sTagId: " + sTagId + "\n";
		buf = buf + "sAssignTimestamp: " + sAssignTimestamp + "\n";
		buf = buf + "sProductId: " + sProductId + "\n";
		buf = buf + "sValue: " + sValue + "\n";
		buf = buf + "sCriticality: " + sCriticality + "\n";
		buf = buf + "sLastSync: " + sLastSync + "\n";
		
		return buf;
	}

	public int getShipmentId() {
		return iShipmentId;
	}

	public void setShipmentId(int iShipmentId) {
		this.iShipmentId = iShipmentId;
	}

	public String getBoxId() {
		return sBoxId;
	}

	public void setBoxId(String sBoxId) {
		this.sBoxId = sBoxId;
	}

	public String getTagId() {
		return sTagId;
	}

	public void setTagId(String sTagId) {
		this.sTagId = sTagId;
	}

	public String getAssignTimestamp() {
		return sAssignTimestamp;
	}

	public void setAssignTimestamp(String sAssignTimestamp) {
		this.sAssignTimestamp = sAssignTimestamp;
	}

	public String getProductId() {
		return sProductId;
	}

	public void setProductId(String sProductId) {
		this.sProductId = sProductId;
	}

	public float getValue() {
		return sValue;
	}

	public void setValue(float sValue) {
		this.sValue = sValue;
	}

	public String getCriticality() {
		return sCriticality;
	}

	public void setCriticality(String sCriticality) {
		this.sCriticality = sCriticality;
	}
	
	public String getLastSync() {
		return sLastSync;
	}

	public void setLastSync(String sLastSync) {
		this.sLastSync = sLastSync;
	}


}
