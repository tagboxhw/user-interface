package com.biocon.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "shipmentBean")
public class ShipmentBean implements Serializable{

	/**
	 * Generated serial version ID
	 */
	private static final long serialVersionUID = -1874474658549948076L;
	private int iShipmentId;
	private String sOrigin;
	private String sDestination;
	private String sETA;
	private String sETD;
	private String sUser;
	private String sInvoiceNumber;
	
	public ShipmentBean(){
		super();
	}
	
	public void init() {
		this.iShipmentId = 0;
		this.sOrigin = "";
		this.sDestination = "";
		this.sETA = "";
		this.sETD = "";
		this.sUser = "";
		this.sInvoiceNumber = "";
	}
	
	public String toString(){
		String buf = null;
		buf = "Shipment Bean object: \n";
		buf = buf + "Shipment Id: " + iShipmentId + "\n";
		buf = buf + "sOrigin: " + sOrigin + "\n";
		buf = buf + "sDestination: " + sDestination + "\n";
		buf = buf + "sETA: " + sETA + "\n";
		buf = buf + "sETD: " + sETD + "\n";
		buf = buf + "sUser: " + sUser + "\n";
		buf = buf + "sInvoiceNumber: " + sInvoiceNumber + "\n";
		return buf;
	}

	public String getInvoiceNumber() {
		return sInvoiceNumber;
	}

	public void setInvoiceNumber(String sInvoiceNumber) {
		this.sInvoiceNumber = sInvoiceNumber;
	}

	public int getShipmentId() {
		return iShipmentId;
	}

	public void setShipmentId(int iShipmentId) {
		this.iShipmentId = iShipmentId;
	}

	public String getOrigin() {
		return sOrigin;
	}

	public void setOrigin(String sOrigin) {
		this.sOrigin = sOrigin;
	}

	public String getDestination() {
		return sDestination;
	}

	public void setDestination(String sDestination) {
		this.sDestination = sDestination;
	}

	public String getETA() {
		return sETA;
	}

	public void setETA(String sETA) {
		this.sETA = sETA;
	}

	public String getETD() {
		return sETD;
	}

	public void setETD(String sETD) {
		this.sETD = sETD;
	}
	
	public String getUser() {
		return sUser;
	}

	public void setUser(String sUser) {
		this.sUser = sUser;
	}

}
