package com.biocon.bean;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "shipmentLocations")
public class ShipmentLocations implements Serializable{

	/**
	 * Generated serial version ID
	 */
	private static final long serialVersionUID = -1874474658549948076L;
	private String sLocationId;
	
	public ShipmentLocations(){
		super();
	}
	
	public void init() {
		this.sLocationId = "";
		
	}
	
	public String toString(){
		String buf = null;
		buf = "Shipment Locations object: \n";
		buf = buf + "Shipment Location Id: " + sLocationId + "\n";
		return buf;
	}

	public String getShipmentLocationId() {
		return sLocationId;
	}

	public void setShipmentLocationId(String sLocationId) {
		this.sLocationId = sLocationId;
	}
}
