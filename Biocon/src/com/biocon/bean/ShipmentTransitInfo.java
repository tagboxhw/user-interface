package com.biocon.bean;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "shipmentTransitInfo")
public class ShipmentTransitInfo implements Serializable{

	/**
	 * Generated serial version ID
	 */
	private static final long serialVersionUID = -1874474658549948076L;
	private int iShipmentId;
	private int iLegId;
	private String sOrigin;
	private String sDestination;
	private String sETD;
	private String sETA;
	private String sMode;
	private String sCarrier;
	
	public ShipmentTransitInfo(){
		super();
	}
	
	public void init() {
		this.iShipmentId = 0;
		this.sOrigin = "";
		this.sDestination = "";
		this.sETA = "";
		this.sETD = "";
		this.sMode = "";
		this.sCarrier = "";
		this.iLegId = 0;
	}
	
	public String toString(){
		String buf = null;
		buf = "Shipment Transit object: \n";
		buf = buf + "Shipment Id: " + iShipmentId + "\n";
		buf = buf + "sOrigin: " + sOrigin + "\n";
		buf = buf + "sDestination: " + sDestination + "\n";
		buf = buf + "sETA: " + sETA + "\n";
		buf = buf + "sETD: " + sETD + "\n";
		buf = buf + "sMode: " + sMode + "\n";
		buf = buf + "sCarrier: " + sCarrier + "\n";
		buf = buf + "iLegId: " + iLegId + "\n";
		return buf;
	}

	public String getMode() {
		return sMode;
	}

	public void setMode(String sMode) {
		this.sMode = sMode;
	}

	public String getCarrier() {
		return sCarrier;
	}

	public void setCarrier(String sCarrier) {
		this.sCarrier = sCarrier;
	}

	public int getShipmentId() {
		return iShipmentId;
	}

	public void setShipmentId(int iShipmentId) {
		this.iShipmentId = iShipmentId;
	}

	public String getOrigin() {
		return sOrigin;
	}

	public void setOrigin(String sOrigin) {
		this.sOrigin = sOrigin;
	}

	public String getDestination() {
		return sDestination;
	}

	public void setDestination(String sDestination) {
		this.sDestination = sDestination;
	}

	public String getETA() {
		return sETA;
	}

	public void setETA(String sETA) {
		this.sETA = sETA;
	}

	public String getETD() {
		return sETD;
	}

	public void setETD(String sETD) {
		this.sETD = sETD;
	}
	
	public int getLegId() {
		return iLegId;
	}

	public void setLegId(int iLegId) {
		this.iLegId = iLegId;
	}
}
