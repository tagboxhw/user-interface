<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>

<%
response.setHeader("Cache-Control","no-cache"); 
response.setHeader("Pragma","no-cache"); 
response.setDateHeader ("Expires", -1); 
%>
<%!
static final Logger logger = LoggerFactory.getLogger("Distribution");
%>
<%
String sUsername = (String) session.getAttribute("username");
if(sUsername == null || sUsername.equals("")) {
	session.setAttribute("callingPage", "Distribution.jsp");
	response.sendRedirect("../../index.html");
}
	java.util.Date dt = new java.util.Date();
	java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd-MM-yyyy HH:mm");
sdf.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
String currentTime = sdf.format(dt);
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Distribution</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- Add these below two lines for autocomplete and the javascript function at the end  -->
<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="../../bootstrap/css/bootstrap.css">
   
<!-- Font Awesome -->
<script src="https://use.fontawesome.com/37ba1872d2.js"></script>
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../../dist/css/tagbox.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
<link href="https://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
  body {
	font-family:  Lato, sans-serif;
	font-weight: 600;
}
</style>
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
	<div class="wrapper">

		<header class="main-header">
			<!-- Logo -->
			<div>
				<a href="#" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
					<span class="logo-mini"><b>Demo</b></span> <!-- logo for regular state and mobile devices -->
					<span class="logo-lg"><b>Demo</b> </span>
				</a>
			</div>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">

				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
					role="button"> <span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
				</a>
				<label class="navbar-form pull-left" role="search">
					<div class="form-group">
						<b style="padding-right: 115px; font-size: 20px;color:#fff;" class="pull-right">Distribution of Cold Boxes</b><br> <b style="padding-right: 15px;color:#fff;"><%= currentTime %></b>
					</div>
				</label>

				<div class="navbar-custom-menu">
					<img src="../../dist/img/Logo.png" class="logo-class pull-right"
						alt="User Image">
				</div>
			</nav>

		</header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->


				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<br>
				<ul class="sidebar-menu">
					<li class="header">MAIN NAVIGATION</li>
						<li class="treeview active"><a href="#"> <i
							class="fa fa-home"></i> <span>Home</span>
					</a>
					<ul class="treeview-menu">
							<li><a href="HomePage.jsp"><i class="fa fa-circle-o"></i>
									Dashboard</a></li>
							<li><a href="RunningShipmentInfo.jsp"><i class="fa fa-circle-o"></i>
									Summary Of Today's <br>Shipments</a></li>
							<li><a href="MissingBoxes.jsp"><i class="fa fa-circle-o"></i>
									Missing Boxes <br>Details</a></li>
							<li class="active"><a href="Distribution.jsp"><i class="fa fa-circle-o"></i>
									Distribution Of <br>Cold Boxes</a></li>
						
									</ul>
									</li>
						<li class="treeview"><a href="#"> <i
							class="fa fa-bar-chart"></i> <span>Reports</span>
					</a>
					<ul class="treeview-menu">
							<li><a href="Reports.jsp"><i class="fa fa-circle-o"></i>
									Shipments Report</a></li>
									</ul>
									</li>
							
						
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper" style="background:white">
			<!-- Content Header (Page header) -->
			
		<section id="mainSection" class="content">
			<div class="row text-center">
				<div class="col-md-12 pull-right">
					<h4>Any boxes located in last <input class='form-control-no-height errorborder' style="width:50px" type="text" name="hours_txt" id="hours_txt" value="6"> hours are not considered missing</h4>
				</div>
				
			</div>
			<br>
			<div class="row text-center">
				<div class="col-md-12">
					<div class="btn bb-color" style="color:#fff">
						<h5>Total Fitted Boxes<br>1200</h5>
					</div>
				</div>
				</div><div class="row text-center">
				<div class="col-md-6" style="border-right: 2px solid #367fa9;">
					<div class="btn bb-color" style="color:#fff">
						<h5>Located Boxes<br>1000</h5>
					</div>
					<br><br>
					<div class="row"><div class="col-md-offset-1 col-md-10 text-center">
					<div class="scroll-area">
					<table id="data_table_stationary" class="table table-condensed table-striped">
				<thead><tr class="bb-color">
				<th class="text-center"><b class="font-big-white">Current Location</b></th>
				<th class="text-center"><b class="font-big-white">Count</b></th>
				</tr></thead>
				<tbody>
				<tr><td>Mahadevapura DC</td><td>500</td></tr>
				<tr><td>Sarjapur DC</td><td>300</td></tr>
				<tr><td>Hub 1</td><td>100</td></tr>
				<tr><td>Hub 2</td><td>100</td></tr>
				</tbody>
			</table>
					</div></div></div>
					<br>
					</div>
				<div class="col-md-6 text-center"><div class="btn bb-color" style="color:#fff">
						<h5>Unaccounted Boxes<br>200</h5>
					</div>
					<br><br>
					<div class="row"><div class="col-md-offset-1 col-md-10 text-center">
					<div class="scroll-area">
					<table id="data_table_stationary" class="table table-condensed table-striped">
				<thead><tr class="bb-color">
				<th class="text-center"><b class="font-big-white">Current Location</b></th>
				<th class="text-center"><b class="font-big-white">Count</b></th>
				</tr></thead>
				<tbody>
				<tr><td>Mahadevapura DC</td><td>50</td></tr>
				<tr><td>Sarjapur DC</td><td>60</td></tr>
				<tr><td>Hub 1</td><td>40</td></tr>
				<tr><td>Hub 2</td><td>50</td></tr>
				</tbody>
			</table>
					</div></div></div>
					<br>
					</div>
					<div class="row"><div class="col-md-12 text-center"><a href="MissingBoxes.jsp" style="text-decoration:underline">Movement Details</a></div></div>
			</div>
			
			
			
        
	</section>
	
    
    
    
				</div>
		<!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0
			</div>
			<strong>Copyright &copy; 2017 <a href="http://tagbox.in">TagBox</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Control Sidebar -->
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->
	
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
	<script src="../../bootstrap/js/bootstrap.min.js"></script>
	<script src="../../dist/js/app.min.js"></script>
	<script src="../../dist/js/demo.js"></script>

	</body>
</html>
