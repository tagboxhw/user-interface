<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>

<%
response.setHeader("Cache-Control","no-cache"); 
response.setHeader("Pragma","no-cache"); 
response.setDateHeader ("Expires", -1); 
%>
<%!
static final Logger logger = LoggerFactory.getLogger("RunningShipmentInfo");
%>
<%
String sUsername = (String) session.getAttribute("username");
if(sUsername == null || sUsername.equals("")) {
	session.setAttribute("callingPage", "RunningShipmentInfo.jsp");
	response.sendRedirect("../../index.html");
}
	java.util.Date dt = new java.util.Date();
	java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd-MM-yyyy HH:mm");
sdf.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
String currentTime = sdf.format(dt);
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Today's Cold Chain Shipment Detail</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- Add these below two lines for autocomplete and the javascript function at the end  -->
<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="../../bootstrap/css/bootstrap.css">
   
<!-- Font Awesome -->
<script src="https://use.fontawesome.com/37ba1872d2.js"></script>
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../../dist/css/tagbox.css">
<link href="../../dist/css/select2.css" rel="stylesheet" />
<script src="../../dist/js/select2.js"></script>
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
<link href="https://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
  body {
	font-family:  Lato, sans-serif;
	font-weight: 600;
}
</style>
<STYLE TYPE="text/css">
			table.sortable span.sortarrow {
   				color: white;
   				text-decoration: none;
			}
					
		</style>
		
<script src="../../dist/js/sorttable.js"></script>
<script>
function blinker() {
	$('.blinking').fadeOut(500);
	$('.blinking').fadeIn(500);

}
setInterval(blinker, 1000);
$(document).ready(function() {
	$("#City").select2();
	$("#DC").select2();
	$("#Hub").select2();
	$("#Category").select2();
	$("#Status").select2();
	$("#Filter").select2();
});
</script>
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
	<div class="wrapper">

		<header class="main-header">
			<!-- Logo -->
			<div>
				<a href="#" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
					<span class="logo-mini"><b>Demo</b></span> <!-- logo for regular state and mobile devices -->
					<span class="logo-lg"><b>Demo</b> </span>
				</a>
			</div>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">

				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
					role="button"> <span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
				</a>
				<label class="navbar-form pull-left" role="search">
					<div class="form-group">
						<b style="padding-right: 115px; font-size: 20px;color:#fff;" class="pull-right">Today's Cold Chain Shipment Detail</b><br> <b style="padding-right: 15px;color:#fff;"><%= currentTime %></b>
					</div>
				</label>

				<div class="navbar-custom-menu">
					<img src="../../dist/img/Logo.png" class="logo-class pull-right"
						alt="User Image">
				</div>
			</nav>

		</header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->


				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<br>
				<ul class="sidebar-menu">
					<li class="header">MAIN NAVIGATION</li>
						<li class="treeview active"><a href="#"> <i
							class="fa fa-home"></i> <span>Home</span>
					</a>
					<ul class="treeview-menu">
							<li><a href="HomePage.jsp"><i class="fa fa-circle-o"></i>
									Cold Chain<br> Dashboard</a></li>
							<li class="active"><a href="RunningShipmentInfo.jsp"><i class="fa fa-circle-o"></i>
									Shipment Detail</a></li>
							<li><a href="MissingBoxes.jsp"><i class="fa fa-circle-o"></i>
									Missing Boxes <br>Dashboard</a></li>
							
						
									</ul>
									</li>
						<li class="treeview"><a href="#"> <i
							class="fa fa-bar-chart"></i> <span>Reports</span>
					</a>
					<ul class="treeview-menu">
							<li><a href="Reports.jsp"><i class="fa fa-circle-o"></i>
									Shipment Report</a></li>
									</ul>
									</li>
							
						
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper" style="background:white">
			<!-- Content Header (Page header) -->
			
		<section id="mainSection" class="content">
		<div class="row"><div class="col-md-12">
			<div class="well">
		<div class="row">
		<div class="col-md-2 text-center">
					CITY<br>
					<select id="City" name="City" class="form-control-no-background pull-center">
					<option>All</option>
					<option selected>Bangalore</option>
					<option>Chennai</option>
					<option>Goa</option>
					<option>Mumbai</option>
					<option>Pune</option>
					</select>
					</div>
					<div class="col-md-2 text-center">
					DC<br>
					<select id="DC" name="DC" class="form-control-no-background pull-center">
					<option>All</option>
					<option selected>DC 1</option>
					<option>DC 2</option>
					<option>DC 3</option>
					</select>
					</div>
					<div class="col-md-2 text-center">
					HUB<br>
					<select id="Hub" name="Hub" class="form-control-no-background pull-center">
					<option>All</option>
					<option selected>Hub 1</option>
					<option>Hub 2</option>
					<option>Hub 3</option>
					<option>Hub 4</option>
					<option>Hub 5</option>
					</select>
					</div>
					<div class="col-md-2 text-center">
					PRODUCT CATEGORY<br>
					<select id="Category" name="Category" class="form-control-no-background pull-center">
					<option selected>All</option>
					<option>Prod_Cat_1</option>
					<option>Prod_Cat_2</option>
					<option>Prod_Cat_3</option>
					<option>Prod_Cat_4</option>
					<option>Prod_Cat_5</option>
					</select>
					</div>
					<div class="col-md-2 text-center">
					STATUS<br>
					<select id="Status" name="Status" class="form-control-no-background pull-center">
					<option>All</option>
					<option>At DC</option>
					<option>DC -> Hub Transit</option>
					<option>At Hub</option>
					<option>Out For Delivery</option>
					<option>Delivered</option>
					<option>Cancelled</option>
					<option>Customer Retained Box</option>
					<option>Returned</option>
					<option>Shifted Order</option>
					</select>
					</div>
					<div class="col-md-2 text-center">
					PERFORMANCE<br>
					<select id="Filter" name="Filter" class="form-control-no-background pull-center">
					<option>All</option>
					<option>Orders Not Delivered At Right Temperature</option>
					<option>Orders With Temp Excursions</option>
					</select>
					</div>
					</div>
					<div class="row">
					<div class="col-md-12 text-center">
					<br>
					<button type="button" class="btn bb-color">GO</button></div>
					</div>
					</div>
					
					
		</div>
		</div>	
					
					<div class="row">
						<div class="col-md-12 alert-text text-center">
								<ol class="breadcrumb">
		  <li class="alert-font-small">Bangalore</li>
		  <li class="alert-font-small">DC 1</li>
		  <li class="alert-font-small">Hub 1</li>
		</ol>
									
							</div>
						</div>
						
						<div class="row"><div class="col-md-12">
							<div class="well white-background">
							
						<div class="row">
			<div class="col-md-4 text-center">
				<h4>TOTAL ORDERS TODAY<br><br><br><span data-toggle="tooltip" class="badge badge-big bb-color">500</span></h4><i class="fa fa-caret-up"></i>&nbsp;5% THAN DAILY AVERAGE
			</div>
			<div class="col-md-4 text-center">
			<h4>% ORDERS DELIVERED AT RIGHT TEMPERATURE TODAY<br><br><span data-toggle="tooltip" class="badge badge-big bg-green">97%</span></h4><span class="description-percentage text-green"><i class="fa fa-caret-up"></i>&nbsp;2% THAN DAILY AVERAGE</span>
			</div>
			<div class="col-md-4 text-center">
			<h4>% ORDERS WITHOUT TEMPERATURE EXCURSIONS TODAY<br><br><span data-toggle="tooltip" class="badge badge-big bg-green">94%</span></h4><span class="description-percentage text-green"><i class="fa fa-caret-up"></i>&nbsp;1% THAN DAILY AVERAGE</span>
			</div>
		</div></div>
						</div></div>
			<div class="row text-center"><div class="col-md-12">
			<span class="font-big">TODAY'S ORDERS</span><BR><span class="blinking alert-text">3 Excursions To Be Addressed</span><br>
			<div class="btn-group pull-right">
							<button class="btn bb-color btn-sm dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Export Data</button>
							<ul class="dropdown-menu " role="menu">
								<!-- <li><a href="#" onClick ="$('#temp1_table').tableExport({type:'json',escape:'false'});"> <img src='../../dist/img/json.png' width='24px'> JSON</a></li>
								<li><a href="#" onClick ="$('#temp1_table').tableExport({type:'sql',escape:'false'});"> <img src='../../dist/img/sql.png' width='24px'> SQL</a></li>
								<li class="divider"></li>
								<li><a href="#" onClick ="$('#temp1_table').tableExport({type:'xml',escape:'false'});"> <img src='../../dist/img/xml.png' width='24px'> XML</a></li> -->
								<li><a href="#" onClick ="$('#temp1_table').tableExport({type:'csv',escape:'false'});"> <img src='../../dist/img/csv.png' width='24px'> CSV</a></li>
								<li><a href="#" onClick ="$('#temp1_table').tableExport({type:'excel',escape:'false'});"> <img src='../../dist/img/xls.png' width='24px'> XLS</a></li>
								<li class="divider"></li>				
								<li><a href="#" onClick ="$('#temp1_table').tableExport({type:'txt',escape:'false'});"> <img src='../../dist/img/txt.png' width='24px'> TXT</a></li>
								<!-- <li><a href="#" onClick ="$('#temp1_table').tableExport({type:'doc',escape:'false'});"> <img src='../../dist/img/word.png' width='24px'> Word</a></li>
								<li class="divider"></li> -->
								<!-- <li><a href="#" onClick ="$('#temp1_table').tableExport({type:'pdf',pdfFontSize:'7',escape:'false'});"> <img src='../../dist/img/pdf.png' width='15px'> PDF</a></li> -->
								<li><a href="#" onClick ="exportData()"> <img src='../../dist/img/pdf.png' width='15px'> PDF</a></li>
							</ul>
						</div><br><br>
						
			<table id="bb_table" name="bb_table" class="table  table-striped sortable">
				<thead>
					<tr class="background-tagbox-blue">
						<th class="text-center">
							Order ID
						</th>
						<th class="text-center">
							Box ID
						</th>
						<th class="text-center">
							DC
						</th>
						<th class="text-center">
							Hub
						</th>
						<th class="text-center">
							Product Category
						</th>
						<th class="text-center">
							Total Time Since Packing
						</th>
						<th class="text-center">
							Delivery Person
						</th>
						<th class="text-center">
							Last Update
						</th>
						<th class="text-center">
							Status
						</th>
						<th class="text-center">
							Last Known Temp (&deg;C)
						</th>
						<th class="text-center">
							Delivered Temp (&deg;C)
						</th>
						<th class="text-center">
							Total Time Spent In Current Excursion
						</th>
						<th class="text-center">
							&nbsp;
						</th>
						<th class="text-center">
							&nbsp;
						</th>
					</tr>
				</thead>
				<tbody>
					<tr style="background-color:rgba(255,0,0,0.7);color:white">
						<td><span data-toggle="tooltip" data-placement="top" title="11X34-22BS1-12YD3-ABC34">11X34-...</span></td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">1</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>Prod_Cat_1</td>
						<td>120 mins</td>
						<td>Robert</td>
						<td>2 mins ago</td>
						<td>DC To Hub Transit</td>
						<td>4</td>
						<td>NA</td>
						<td>10 mins</td>
						<td><a class="btn" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye" style="color:white"></i></a></td>
						<td style="background-color:white;"><a id="id1" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#myModal1">Take Action</a><a id="id2" class="btn btn-sm btn-success" style="display:none">Action Taken</a></td>
					</tr>
					<tr style="background-color:rgba(255,0,0,0.7);color:white">
						<td><span data-toggle="tooltip" data-placement="top" title="11X34-22BS1-12YD3-ABC34">11X34-...</span></td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">2</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>Prod_Cat_3</td>
						<td>120 mins</td>
						<td>Robert</td>
						<td>2 mins ago</td>
						<td>DC To Hub Transit</td>
						<td>4</td>
						<td>NA</td>
						<td>10 mins</td>
						<td><a class="btn" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye" style="color:white"></i></a></td>
						<td style="background-color:white;"><a id="id1" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#myModal1">Take Action</a><a id="id2" class="btn btn-sm btn-success" style="display:none">Action Taken</a></td>
					</tr>
					<tr style="background-color:rgba(255,165,0,0.7);color:white">
						<td><span data-toggle="tooltip" data-placement="top" title="237XY-HG234-U9887-4BI78">237XY-...</span></td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">3</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>CNV</td>
						<td>75 mins</td>
						<td>Paul</td>
						<td>12 mins ago</td>
						<td>Out for delivery</td>
						<td>5</td>
						<td>NA</td>
						<td>5 min</td>
						<td><a class="btn" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye" style="color:white"></i></a></td>
						<td style="background-color:white;"><a class="btn btn-sm btn-warning">Take Action</a></td>
					</tr>
					<tr>
						<td><span data-toggle="tooltip" data-placement="top" title="87SDF-8232N-876VV-WQ652">87SDF-...</span></td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">4</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>Prod_Cat_4</td>
						<td>45 mins</td>
						<td>George</td>
						<td>22 mins ago</td>
						<td>Delivered</td>
						<td>3</td>
						<td>5</td>
						<td>NA</td>
						<td><a class="btn" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye fa-color-blue"></i></a></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td><span data-toggle="tooltip" data-placement="top" title="11X34-22BS1-12YD3-ABC34">11X34-...</span></td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">5</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>Prod_Cat_5</td>
						<td>30 mins</td>
						<td>Robert</td>
						<td>2 mins ago</td>
						<td>DC To Hub Transit</td>
						<td>4</td>
						<td>NA</td>
						<td>NA</td>
						<td><a class="btn" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye fa-color-blue"></i></a></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td><span data-toggle="tooltip" data-placement="top" title="237XY-HG234-U9887-4BI78">237XY-...</span></td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">6</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>Prod_Cat_1</td>
						<td>35 mins</td>
						<td>Paul</td>
						<td>12 mins ago</td>
						<td>Out for delivery</td>
						<td>5</td>
						<td>NA</td>
						<td>NA</td>
						<td><a class="btn" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye fa-color-blue"></i></a></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td><span data-toggle="tooltip" data-placement="top" title="87SDF-8232N-876VV-WQ652">87SDF-...</span></td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">7</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>Prod_Cat_1</td>
						<td>45 mins</td>
						<td>George</td>
						<td>22 mins ago</td>
						<td>Delivered</td>
						<td>3</td>
						<td>5</td>
						<td>NA</td>
						<td><a class="btn" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye fa-color-blue"></i></a></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td><span data-toggle="tooltip" data-placement="top" title="11X34-22BS1-12YD3-ABC34">11X34-...</span></td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">8</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>Prod_Cat_1</td>
						<td>30 mins</td>
						<td>Robert</td>
						<td>2 mins ago</td>
						<td>DC To Hub Transit</td>
						<td>4</td>
						<td>NA</td>
						<td>NA</td>
						<td><a class="btn" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye fa-color-blue"></i></a></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td><span data-toggle="tooltip" data-placement="top" title="237XY-HG234-U9887-4BI78">237XY-...</span></td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">9</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>Prod_Cat_1</td>
						<td>35 mins</td>
						<td>Paul</td>
						<td>12 mins ago</td>
						<td>Out for delivery</td>
						<td>5</td>
						<td>NA</td>
						<td>NA</td>
						<td><a class="btn" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye fa-color-blue"></i></a></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td><span data-toggle="tooltip" data-placement="top" title="87SDF-8232N-876VV-WQ652">87SDF-...</span></td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">10</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>Prod_Cat_1</td>
						<td>45 mins</td>
						<td>George</td>
						<td>22 mins ago</td>
						<td>Delivered</td>
						<td>3</td>
						<td>5</td>
						<td>NA</td>
						<td><a class="btn" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye fa-color-blue"></i></a></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td><span data-toggle="tooltip" data-placement="top" title="11X34-22BS1-12YD3-ABC34">11X34-...</span></td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">11</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>Prod_Cat_1</td>
						<td>30 mins</td>
						<td>Robert</td>
						<td>2 mins ago</td>
						<td>DC To Hub Transit</td>
						<td>4</td>
						<td>NA</td>
						<td>NA</td>
						<td><a class="btn" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye fa-color-blue"></i></a></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td><span data-toggle="tooltip" data-placement="top" title="237XY-HG234-U9887-4BI78">237XY-...</span></td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">12</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>Prod_Cat_1</td>
						<td>35 mins</td>
						<td>Paul</td>
						<td>12 mins ago</td>
						<td>Out for delivery</td>
						<td>5</td>
						<td>NA</td>
						<td>NA</td>
						<td><a class="btn" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye fa-color-blue"></i></a></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td><span data-toggle="tooltip" data-placement="top" title="87SDF-8232N-876VV-WQ652">87SDF-...</span></td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">3</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>Prod_Cat_1</td>
						<td>45 mins</td>
						<td>George</td>
						<td>22 mins ago</td>
						<td>Delivered</td>
						<td>3</td>
						<td>5</td>
						<td>NA</td>
						<td><a class="btn" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye fa-color-blue"></i></a></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td><span data-toggle="tooltip" data-placement="top" title="11X34-22BS1-12YD3-ABC34">11X34-...</span></td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">1</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>Prod_Cat_1</td>
						<td>30 mins</td>
						<td>Robert</td>
						<td>2 mins ago</td>
						<td>DC To Hub Transit</td>
						<td>4</td>
						<td>NA</td>
						<td>NA</td>
						<td><a class="btn" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye fa-color-blue"></i></a></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td><span data-toggle="tooltip" data-placement="top" title="237XY-HG234-U9887-4BI78">237XY-...</span></td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">2</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>Prod_Cat_1</td>
						<td>35 mins</td>
						<td>Paul</td>
						<td>12 mins ago</td>
						<td>Out for delivery</td>
						<td>5</td>
						<td>NA</td>
						<td>NA</td>
						<td><a class="btn" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye fa-color-blue"></i></a></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td><span data-toggle="tooltip" data-placement="top" title="87SDF-8232N-876VV-WQ652">87SDF-...</span></td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">3</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>Prod_Cat_1</td>
						<td>45 mins</td>
						<td>George</td>
						<td>22 mins ago</td>
						<td>Delivered</td>
						<td>3</td>
						<td>5</td>
						<td>NA</td>
						<td><a class="btn" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye fa-color-blue"></i></a></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td><span data-toggle="tooltip" data-placement="top" title="11X34-22BS1-12YD3-ABC34">11X34-...</span></td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">1</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>Prod_Cat_1</td>
						<td>30 mins</td>
						<td>Robert</td>
						<td>2 mins ago</td>
						<td>DC To Hub Transit</td>
						<td>4</td>
						<td>NA</td>
						<td>NA</td>
						<td><a class="btn" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye fa-color-blue"></i></a></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td><span data-toggle="tooltip" data-placement="top" title="237XY-HG234-U9887-4BI78">237XY-...</span></td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">2</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>Prod_Cat_1</td>
						<td>35 mins</td>
						<td>Paul</td>
						<td>12 mins ago</td>
						<td>Out for delivery</td>
						<td>5</td>
						<td>NA</td>
						<td>NA</td>
						<td><a class="btn" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye fa-color-blue"></i></a></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td><span data-toggle="tooltip" data-placement="top" title="87SDF-8232N-876VV-WQ652">87SDF-...</span></td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">3</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>Prod_Cat_1</td>
						<td>45 mins</td>
						<td>George</td>
						<td>22 mins ago</td>
						<td>Delivered</td>
						<td>3</td>
						<td>5</td>
						<td>NA</td>
						<td><a class="btn" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye fa-color-blue"></i></a></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td><span data-toggle="tooltip" data-placement="top" title="11X34-22BS1-12YD3-ABC34">11X34-...</span></td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">1</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>Prod_Cat_1</td>
						<td>30 mins</td>
						<td>Robert</td>
						<td>2 mins ago</td>
						<td>DC To Hub Transit</td>
						<td>4</td>
						<td>NA</td>
						<td>NA</td>
						<td><a class="btn" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye fa-color-blue"></i></a></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td><span data-toggle="tooltip" data-placement="top" title="237XY-HG234-U9887-4BI78">237XY-...</span></td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">2</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>Prod_Cat_1</td>
						<td>35 mins</td>
						<td>Paul</td>
						<td>12 mins ago</td>
						<td>Out for delivery</td>
						<td>5</td>
						<td>NA</td>
						<td>NA</td>
						<td><a class="btn" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye fa-color-blue"></i></a></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td><span data-toggle="tooltip" data-placement="top" title="87SDF-8232N-876VV-WQ652">87SDF-...</span></td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">3</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>Prod_Cat_1</td>
						<td>45 mins</td>
						<td>George</td>
						<td>22 mins ago</td>
						<td>Delivered</td>
						<td>3</td>
						<td>5</td>
						<td>NA</td>
						<td><a class="btn" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye fa-color-blue"></i></a></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td><span data-toggle="tooltip" data-placement="top" title="11X34-22BS1-12YD3-ABC34">11X34-...</span></td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">1</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>Prod_Cat_1</td>
						<td>30 mins</td>
						<td>Robert</td>
						<td>2 mins ago</td>
						<td>DC To Hub Transit</td>
						<td>4</td>
						<td>NA</td>
						<td>NA</td>
						<td><a class="btn" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye fa-color-blue"></i></a></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td><span data-toggle="tooltip" data-placement="top" title="237XY-HG234-U9887-4BI78">237XY-...</span></td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">2</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>Prod_Cat_1</td>
						<td>35 mins</td>
						<td>Paul</td>
						<td>12 mins ago</td>
						<td>Out for delivery</td>
						<td>5</td>
						<td>NA</td>
						<td>NA</td>
						<td><a class="btn" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye fa-color-blue"></i></a></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td><span data-toggle="tooltip" data-placement="top" title="87SDF-8232N-876VV-WQ652">87SDF-...</span></td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">3</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>Prod_Cat_1</td>
						<td>45 mins</td>
						<td>George</td>
						<td>22 mins ago</td>
						<td>Delivered</td>
						<td>3</td>
						<td>5</td>
						<td>NA</td>
						<td><a class="btn" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye fa-color-blue"></i></a></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td><span data-toggle="tooltip" data-placement="top" title="11X34-22BS1-12YD3-ABC34">11X34-...</span></td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">1</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>Prod_Cat_1</td>
						<td>30 mins</td>
						<td>Robert</td>
						<td>2 mins ago</td>
						<td>DC To Hub Transit</td>
						<td>4</td>
						<td>NA</td>
						<td>NA</td>
						<td><a class="btn" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye fa-color-blue"></i></a></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td><span data-toggle="tooltip" data-placement="top" title="237XY-HG234-U9887-4BI78">237XY-...</span></td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">2</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>Prod_Cat_1</td>
						<td>35 mins</td>
						<td>Paul</td>
						<td>12 mins ago</td>
						<td>Out for delivery</td>
						<td>5</td>
						<td>NA</td>
						<td>NA</td>
						<td><a class="btn" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye fa-color-blue"></i></a></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td><span data-toggle="tooltip" data-placement="top" title="87SDF-8232N-876VV-WQ652">87SDF-...</span></td>
						<td><span data-toggle="tooltip" data-placement="top" title="987978799, 087686887, 672342343">3</span></td>
						<td>DC 1</td>
						<td>Hub 1</td>
						<td>Prod_Cat_1</td>
						<td>45 mins</td>
						<td>George</td>
						<td>22 mins ago</td>
						<td>Delivered</td>
						<td>5</td>
						<td>5</td>
						<td>NA</td>
						<td><a class="btn" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye fa-color-blue"></i></a></td>
						<td>&nbsp;</td>
					</tr>
				</tbody>
			</table>
			
			<br>
			</div></div>
			
	</section>
				</div>
				<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Order Details</h4>
      </div>
      <div class="modal-body">
      <div class="row"><div class="col-md-12 text-center">
      <div class="btn-group">
							<button class="btn bb-color btn-sm dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Export Data</button>
							<ul class="dropdown-menu " role="menu">
								<!-- <li><a href="#" onClick ="$('#temp1_table').tableExport({type:'json',escape:'false'});"> <img src='../../dist/img/json.png' width='24px'> JSON</a></li>
								<li><a href="#" onClick ="$('#temp1_table').tableExport({type:'sql',escape:'false'});"> <img src='../../dist/img/sql.png' width='24px'> SQL</a></li>
								<li class="divider"></li>
								<li><a href="#" onClick ="$('#temp1_table').tableExport({type:'xml',escape:'false'});"> <img src='../../dist/img/xml.png' width='24px'> XML</a></li> -->
								<li><a href="#" onClick ="$('#temp1_table').tableExport({type:'csv',escape:'false'});"> <img src='../../dist/img/csv.png' width='24px'> CSV</a></li>
								<li><a href="#" onClick ="$('#temp1_table').tableExport({type:'excel',escape:'false'});"> <img src='../../dist/img/xls.png' width='24px'> XLS</a></li>
								<li class="divider"></li>				
								<li><a href="#" onClick ="$('#temp1_table').tableExport({type:'txt',escape:'false'});"> <img src='../../dist/img/txt.png' width='24px'> TXT</a></li>
								<!-- <li><a href="#" onClick ="$('#temp1_table').tableExport({type:'doc',escape:'false'});"> <img src='../../dist/img/word.png' width='24px'> Word</a></li>
								<li class="divider"></li> -->
								<!-- <li><a href="#" onClick ="$('#temp1_table').tableExport({type:'pdf',pdfFontSize:'7',escape:'false'});"> <img src='../../dist/img/pdf.png' width='15px'> PDF</a></li> -->
								<li><a href="#" onClick ="exportData()"> <img src='../../dist/img/pdf.png' width='15px'> PDF</a></li>
							</ul>
						</div>
						</div></div><br>
      <div>
      <table class="table table-bb-border">
      	<tr>
      	<td style="width:16%"><span class="font-big-black">ORDER ID</span></td><td style="width:16%;border-right: 2px solid #367fa9;">11X34-22BS1-12YD3-ABC34</td>
      	<td><span class="font-big-black">PRODUCT CATEGORY</span></td><td style="width:16%;border-right: 2px solid #367fa9;">Prod_Cat_1</td>
      	<td><span class="font-big-black">DELIVERY PERSON</span></td><td style="width:16%">Robert</td>
      	</tr>
      	<tr>
      	<td style="width:16%"><span class="font-big-black">BOX ID</span></td><td style="border-right: 2px solid #367fa9;">1</td>
      	<td><span class="font-big-black">SKU</span></td><td style="border-right: 2px solid #367fa9;">987978799, 087686887, 672342343</td>
      	<td><span class="font-big-black">LAST UPDATE</span></td><td>2 mins ago</td>
      	</tr>
      	<tr>
      	<td><span class="font-big-black">CITY</span></td><td style="border-right: 2px solid #367fa9;">Bangalore</td>
      	
      	<td><span class="font-big-black">TOTAL TIME SINCE PACKING</span></td><td style="border-right: 2px solid #367fa9;">120 mins</td>
      	<td><span class="font-big-black">LAST KNOWN TEMP</span></td><td>4 &deg;C</td>
      	</tr>
      	<tr>
      	<td style="width:16%"><span class="font-big-black">DC</span></td><td style="border-right: 2px solid #367fa9;">DC 1</td>
      	<td><span class="font-big-black">CUSTOMER NAME</span></td><td style="border-right: 2px solid #367fa9;">Peter</td>
      	<td><span class="font-big-black">ACCEPTABLE TEMP RANGE</span></td><td>1-5&deg;C</td>
      	</tr>
      	<tr>
      	<td><span class="font-big-black">HUB</span></td><td style="border-right: 2px solid #367fa9;">Hub 1</td>
      	<td><span class="font-big-black">CUSTOMER ADDRESS</span></td><td style="border-right: 2px solid #367fa9;">123 Street, ABC Lane, Bangalore</td>
      	<td><span class="font-big-black">STATUS</span></td><td>DC To Hub Transit</td>
      	</tr>
      	<tr>
      	<td><span class="font-big-black alert-text">DELIVERED TEMP</span></td><td style="border-right: 2px solid #367fa9;" class="alert-text">NA</td>
      	<td><span class="font-big-black alert-text">TOTAL # OF EXCURSIONS</span></td><td style="border-right: 2px solid #367fa9;" class="alert-text">2</td>
      	<td><span class="font-big-black alert-text">TOTAL TIME SPENT IN EXCURSIONS</span></td><td class="alert-text">10 mins</td>
      	</tr>
      </table>
      </div>
      <div class="row"><div class="col-md-6 text-center">
      <div class="scroll-area-table">
        <table id="temp_table" name="temp_table" class="table table-bordered table-striped sortable">
			<thead>
				<tr class="bb-color">
					<th class="text-center">
						Time
					</th>
					<th class="text-center">
						Location
					</th>
					<th class="text-center">
						Temperature (&deg;C)
					</th>
					<th class="text-center">
						Excursion
					</th>
				</tr>
			</thead>
			<tbody>
				<tr class="text-center"><td>04/08/2017 11:00</td><td>Bangalore</td><td>4</td><td>&nbsp;</td></tr>
				<tr class="text-center text-red"><td>04/08/2017 11:15</td><td>DC 1</td><td>7</td><td>Excursion 1</td></tr>
				<tr class="text-center"><td>04/08/2017 11:30</td><td>DC 1</td><td>4</td><td>&nbsp;</td></tr>
				<tr class="text-center"><td>04/08/2017 11:45</td><td>Hub 1</td><td>3</td><td>&nbsp;</td></tr>
				<tr class="text-center text-red"><td>04/08/2017 12:00</td><td>Hub 1</td><td>7</td><td>Excursion 2</td></tr>
				<tr class="text-center"><td>04/08/2017 12:15</td><td>Hub 1</td><td>4</td><td>&nbsp;</td></tr>
			</tbody>		
		</table>
		</div>
		</div><div class="col-md-6 text-center">
		<div class="box" style="border-top-color:#fff">
			<div class="box-header with-border bb-color">
				&nbsp;Temperature Chart
			</div>
			<div class="box-body">
				<div class="chart">
						<canvas id="chart1"
							style="height: 270px; width: 100px"></canvas>
					</div>
			</div>
		</div>
		</div></div>
		
		</div>
			</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
  
  <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Confirm Action Taken</h4>
      </div>
      <div class="modal-body">
        User Id: Demo1
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" onclick="closeModal();" data-dismiss="modal">Confirm</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
		<!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0
			</div>
			<strong>Copyright &copy; 2017 <a href="http://tagbox.in">TagBox</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Control Sidebar -->
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->
	
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
	<script src="../../bootstrap/js/bootstrap.min.js"></script>
	<script src="../../dist/js/app.min.js"></script>
	<script src="../../dist/js/demo.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
<script>

Chart.defaults.global.legend.display = false;


var barOptions_stacked = {
	    tooltips: {
	        enabled: true
	    },
	    legend:{
	        display:false
	    },
	    elements: {
            point:{
                radius: 1
            }
        },
	    scales: {
	        xAxes: [{
	                    gridLines: {
	                        display:false
	                    },
	                    ticks: {
	                        autoSkip: true,
	                        maxTicksLimit: 20
	                    }
	                }],
	        yAxes: [{
	                    gridLines: {
	                        display:false
	                    }
	                }]
	        },
	        fill: false,
            lineTension: 0,
            borderColor: "rgba(75,192,192,1)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            spanGaps: false,
	};
	
	var ctx2 = document.getElementById("chart1");
	
	var myChart2 = new Chart(ctx2, {
	    type: 'line',
	    data: {
	    	labels : ['04/08/2017 11:00', '04/08/2017 11:15', '04/08/2017 11:30', '04/08/2017 11:45', '04/08/2017 12:00', '04/08/2017 12:15'],
			datasets : [ {
				label : ["Temperature"],
				fill: false,
				borderColor: "black",
				data : ['4', '7', '4', '3', '7', '4']
			},
			{
				label : ["Upper"],
				fill: false,
				borderColor: "orange",
				data : ['5', '5', '5', '5', '5', '5']
			},
			{
				label : ["Lower"],
				fill: false,
				borderColor: "orange",
				data : ['1', '1', '1', '1', '1', '1']
			}]
	    }, 
	    options: barOptions_stacked
	});
	
	function closeModal(){
		$("#id1").hide();
		$("#id2").show();
	}
</script>
	</body>
</html>
