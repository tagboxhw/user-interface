package com.tagbox.util;

import java.sql.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DbConnection {
	static final Logger logger = LoggerFactory.getLogger(DbConnection.class);
	public static Connection connection = null;

	public static Connection getConnection() {
		try {

			/*
			 * Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			 * String connectionString =
			 * "jdbc:sqlserver://abc.database.windows.net:1433;" +
			 * "database=abc;" + "user=abc@abc;" + "password=abc;" +
			 * "encrypt=false;" + "trustServerCertificate=false;" +
			 * "hostNameInCertificate=*.database.windows.net;" +
			 * "loginTimeout=30;"; // Declare the JDBC objects.
			 */

			Class.forName("org.postgresql.Driver");
			connection = DriverManager.getConnection(

					// BIGBASKET IP ADDRESS
					"jdbc:postgresql://ip_address:port_number/db_name",
					"username", "password");

			if (connection != null) {
				System.out.println("You made it!");
			} else {
				System.out.println("Failed to make connection!");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return connection;
	}

	public static void closeConnection() {
		try {
			if (connection != null) {
				connection.close();
				connection = null;
			}
		} catch (Exception e) {
			logger.error(e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			if (connection != null) {
				connection = null;
			}
		}
	}
}