package com.tagbox.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tagbox.util.DbConnection;
import com.tagbox.util.SqlConstants;
import com.tagbox.bean.OrderBean;

public class BigBasketOrderDao {
	
	static final Logger logger = LoggerFactory.getLogger(BigBasketOrderDao.class);
	
	public Collection<OrderBean> getAllOrders() {
		Connection conn = null;
		CallableStatement pstmt = null;
		ResultSet rs = null;
		Collection<OrderBean> col = new ArrayList<OrderBean>();
		OrderBean ob = new OrderBean();
		try {
			conn = DbConnection.getConnection();
			conn.setAutoCommit(false);
			pstmt = conn.prepareCall(SqlConstants.GetAllOrders);
			pstmt.registerOutParameter(1, Types.OTHER);
			pstmt.execute();
			rs = (ResultSet) pstmt.getObject(1);
			while (rs.next()) {
				ob = new OrderBean();
				if(rs.getString(1) == null || rs.getString(1).equals("")) {
					ob.setOrderId("");
				} else {
					ob.setOrderId(rs.getString(1));
				}
				if(rs.getString(2) == null || rs.getString(2).equals("")) {
					ob.setOrderItemId("");
				} else {
					ob.setOrderItemId(rs.getString(2));
				}
				if(rs.getString(3) == null || rs.getString(3).equals("")) {
					ob.setDC("");
				} else {
					ob.setDC(rs.getString(3));
				}
				if(rs.getString(4) == null || rs.getString(4).equals("")) {
					ob.setHub("");
				} else {
					ob.setHub(rs.getString(4));
				}
				if(rs.getString(5) == null || rs.getString(5).equals("")) {
					ob.setBarCode("");
				} else {
					ob.setBarCode(rs.getString(5));
				}
				if(rs.getString(6) == null || rs.getString(6).equals("")) {
					ob.setSKU("");
				} else {
					ob.setSKU(rs.getString(6));
				}
				if(rs.getString(7) == null || rs.getString(7).equals("")) {
					ob.setCategory("");
				} else {
					ob.setCategory(rs.getString(7));
				}
				if(rs.getString(8) == null || rs.getString(8).equals("")) {
					ob.setStatus("");
				} else {
					ob.setStatus(rs.getString(8));
				}
				if(rs.getString(9) == null || rs.getString(9).equals("")) {
					ob.setStatusTimestamp("");
				} else {
					ob.setStatusTimestamp(rs.getString(9));
				}
				col.add(ob);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return col;
	}

}
