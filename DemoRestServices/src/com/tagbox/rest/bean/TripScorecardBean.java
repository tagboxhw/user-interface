package com.tagbox.rest.bean;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "tripScorecardBean")
public class TripScorecardBean implements Serializable{

	/**
	 * Generated serial version ID
	 */
	private static final long serialVersionUID = -1874474658549948076L;
	private String Ship_Id;
	private String Tag_Id;
	private String Departure_Date;
	private String Arrival_Date;
	private float Value_Inr;
	private String Origin;
	private String Destination;
	private String Status;
	private String Boxes;
	private String LastSyncTime;
	
	public TripScorecardBean(){
		super();
	}
	
	public void init() {
		this.Ship_Id = "";
		this.Tag_Id = "";
		this.Departure_Date = "";
		this.Arrival_Date = "";
		this.Value_Inr = 0;
		this.Origin = "";
		this.Destination = "";
		this.Status = "";
		this.Boxes = "";
		this.LastSyncTime = "";
	}
	
	public String toString(){
		String buf = null;
		buf = "Trip Scorecard object: \n";
		buf = buf + "Ship_Id: " + Ship_Id + "\n";
		buf = buf + "Tag_Id: " + Tag_Id + "\n";
		buf = buf + "Origin: " + Origin + "\n";
		buf = buf + "Destination: " + Destination + "\n";
		buf = buf + "Departure_Date: " + Departure_Date + "\n";
		buf = buf + "Arrival_Date: " + Arrival_Date + "\n";
		buf = buf + "Value_Inr: " + Value_Inr + "\n";
		buf = buf + "Status: " + Status + "\n";
		buf = buf + "Boxes: " + Boxes + "\n";
		buf = buf + "LastSyncTime: " + LastSyncTime + "\n";
		return buf;
	}

	public String getStatus() {
		return Status;
	}

	@XmlElement
	public void setStatus(String status) {
		Status = status;
	}

	public String getLastSyncTime() {
		return LastSyncTime;
	}

	@XmlElement
	public void setLastSyncTime(String lastSyncTime) {
		LastSyncTime = lastSyncTime;
	}

	public String getShip_Id() {
		return Ship_Id;
	}

	@XmlElement
	public void setShip_Id(String ship_Id) {
		Ship_Id = ship_Id;
	}
	
	public String getTag_Id() {
		return Tag_Id;
	}

	@XmlElement
	public void setTag_Id(String tag_Id) {
		this.Tag_Id = tag_Id;
	}

	public String getDeparture_Date() {
		return Departure_Date;
	}

	@XmlElement
	public void setDeparture_Date(String departure_Date) {
		Departure_Date = departure_Date;
	}

	public String getArrival_Date() {
		return Arrival_Date;
	}

	@XmlElement
	public void setArrival_Date(String arrival_Date) {
		Arrival_Date = arrival_Date;
	}

	public float getValue_Inr() {
		return Value_Inr;
	}

	@XmlElement
	public void setValue_Inr(float value_Inr) {
		Value_Inr = value_Inr;
	}

	public String getOrigin() {
		return Origin;
	}

	@XmlElement
	public void setOrigin(String origin) {
		Origin = origin;
	}

	public String getDestination() {
		return Destination;
	}

	@XmlElement
	public void setDestination(String destination) {
		Destination = destination;
	}

	public String getBoxes() {
		return Boxes;
	}
	
	@XmlElement
	public void setBoxes(String boxes) {
		Boxes = boxes;
	}

}
