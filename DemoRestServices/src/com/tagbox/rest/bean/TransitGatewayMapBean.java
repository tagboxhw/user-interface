package com.tagbox.rest.bean;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement (name="transitGatewayMapBean")
public class TransitGatewayMapBean implements Serializable{

	/**
	 * Generated serial version ID
	 */
	private static final long serialVersionUID = -1874474658549948076L;
	
	private String GW_Client_ID;
	private String GW_Device_ID;
	private String GW_Type;
	private String Vehicle_ID;
	private String Alert_WF_User_ID;
	private String GW_Status;
	private String Serial_Number;
	
	public TransitGatewayMapBean(){
		super();
	}
	
	public void init() {
		this.GW_Client_ID = "";
		this.GW_Device_ID = "";
		this.GW_Type = "";
		this.GW_Status = "";
		this.Vehicle_ID = "";
		this.Alert_WF_User_ID = "";
		this.Serial_Number = "";
	}
	
	public String toString(){
		String buf = null;
		buf = "Transit Gateway Map object: \n";
		buf = buf + "GW_Client_ID: " + GW_Client_ID + "\n";
		buf = buf + "GW_Device_ID: " + GW_Device_ID + "\n";
		buf = buf + "GW_Type: " + GW_Type + "\n";
		buf = buf + "GW_Status: " + GW_Status + "\n";
		buf = buf + "Vehicle_ID: " + Vehicle_ID + "\n";
		buf = buf + "Alert_WF_User_ID: " + Alert_WF_User_ID + "\n";
		buf = buf + "Serial_Number: " + Serial_Number + "\n";
		return buf;
	}

	/**
	 * @return the gW_Client_ID
	 */
	public String getGW_Client_ID() {
		return GW_Client_ID;
	}

	/**
	 * @param gW_Client_ID the gW_Client_ID to set
	 */
	@XmlElement
	public void setGW_Client_ID(String gW_Client_ID) {
		GW_Client_ID = gW_Client_ID;
	}

	/**
	 * @return the gW_Device_ID
	 */
	public String getGW_Device_ID() {
		return GW_Device_ID;
	}

	/**
	 * @param gW_Device_ID the gW_Device_ID to set
	 */
	@XmlElement
	public void setGW_Device_ID(String gW_Device_ID) {
		GW_Device_ID = gW_Device_ID;
	}

	/**
	 * @return the gW_Type
	 */
	public String getGW_Type() {
		return GW_Type;
	}

	/**
	 * @param gW_Type the gW_Type to set
	 */
	@XmlElement
	public void setGW_Type(String gW_Type) {
		GW_Type = gW_Type;
	}

	/**
	 * @return the gW_Status
	 */
	public String getGW_Status() {
		return GW_Status;
	}

	/**
	 * @param gW_Status the gW_Status to set
	 */
	@XmlElement
	public void setGW_Status(String gW_Status) {
		GW_Status = gW_Status;
	}

	/**
	 * @return the vehicle_ID
	 */
	public String getVehicle_ID() {
		return Vehicle_ID;
	}

	/**
	 * @param vehicle_ID the vehicle_ID to set
	 */
	@XmlElement
	public void setVehicle_ID(String vehicle_ID) {
		Vehicle_ID = vehicle_ID;
	}

	/**
	 * @return the Serial_Number
	 */
	public String getSerial_Number() {
		return Serial_Number;
	}

	/**
	 * @param Serial_Number the Serial_Number to set
	 */
	@XmlElement
	public void setSerial_Number(String Serial_Number) {
		this.Serial_Number = Serial_Number;
	}
	
	/**
	 * @return the Alert_WF_User_ID
	 */
	public String getAlert_WF_User_ID() {
		return Alert_WF_User_ID;
	}

	/**
	 * @param Alert_WF_User_ID the Alert_WF_User_ID to set
	 */
	@XmlElement
	public void setAlert_WF_User_ID(String Alert_WF_User_ID) {
		this.Alert_WF_User_ID = Alert_WF_User_ID;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	

}
