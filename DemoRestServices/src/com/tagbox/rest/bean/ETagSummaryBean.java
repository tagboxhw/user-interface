package com.tagbox.rest.bean;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "eTagSummaryBean")
public class ETagSummaryBean implements Serializable{

	/**
	 * Generated serial version ID
	 */
	private static final long serialVersionUID = -1874474658549948076L;
	private String TagId;
	private String Timestamp;
	private int LMF;
	private int FTT;
	private int TTE;
	private int ETF;
	private int GTF;
	
	public ETagSummaryBean(){
		super();
	}
	
	public void init() {
		this.TagId = "";
		this.Timestamp = "";
		this.LMF = 0;
		this.FTT = 0;
		this.TTE = 0;
		this.ETF = 0;
		this.GTF = 0;
	}
	
	public String toString(){
		String buf = null;
		buf = "ETagSummaryBean object: \n";
		buf = buf + "TagId: " + TagId + "\n";
		buf = buf + "Timestamp: " + Timestamp + "\n";
		buf = buf + "LMF: " + LMF + "\n";
		buf = buf + "FTT: " + FTT + "\n";
		buf = buf + "TTE: " + TTE + "\n";
		buf = buf + "ETF: " + ETF + "\n";
		buf = buf + "GTF: " + GTF + "\n";
		return buf;
	}

	public String getTagId() {
		return TagId;
	}

	@XmlElement
	public void setTagId(String tagId) {
		TagId = tagId;
	}

	public String getTimestamp() {
		return Timestamp;
	}

	@XmlElement
	public void setTimestamp(String timestamp) {
		Timestamp = timestamp;
	}

	public int getLMF() {
		return LMF;
	}

	@XmlElement
	public void setLMF(int lMF) {
		LMF = lMF;
	}

	public int getFTT() {
		return FTT;
	}

	@XmlElement
	public void setFTT(int fTT) {
		FTT = fTT;
	}

	public int getTTE() {
		return TTE;
	}

	@XmlElement
	public void setTTE(int tTE) {
		TTE = tTE;
	}

	public int getETF() {
		return ETF;
	}

	@XmlElement
	public void setETF(int eTF) {
		ETF = eTF;
	}

	public int getGTF() {
		return GTF;
	}

	@XmlElement
	public void setGTF(int gTF) {
		GTF = gTF;
	}
}
