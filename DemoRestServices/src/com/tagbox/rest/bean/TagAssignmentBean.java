package com.tagbox.rest.bean;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "tagAssignmentBean")
public class TagAssignmentBean implements Serializable{

	/**
	 * Generated serial version ID
	 */
	private static final long serialVersionUID = -1874474658549948076L;
	private String Tag_Id;
	private String Ship_Date;
	private String Inv_Id;
	private String Ship_Source;
	private String Ship_Dest;
	private String Ship_Loc;
	private String Flight_Id;
	private String Trip_Status;
	private String Criticality;
	private String Subzone_Id;
	
	public TagAssignmentBean(){
		super();
	}
	
	public void init() {
		this.Tag_Id = "";
		this.Ship_Date = "";
		this.Inv_Id = "";
		this.Ship_Source = "";
		this.Ship_Dest = "";
		this.Ship_Loc = "";
		this.Flight_Id = "";
		this.Trip_Status = "";
		this.Criticality = "";
		this.Subzone_Id = "";
	}
	
	public String toString(){
		String buf = null;
		buf = "Tag Assignment object: \n";
		buf = buf + "Tag_Id: " + Tag_Id + "\n";
		buf = buf + "Ship_Date: " + Ship_Date + "\n";
		buf = buf + "Inv_Id: " + Inv_Id + "\n";
		buf = buf + "Ship_Source: " + Ship_Source + "\n";
		buf = buf + "Ship_Dest: " + Ship_Dest + "\n";
		buf = buf + "Ship_Loc: " + Ship_Loc + "\n";
		buf = buf + "Flight_Id: " + Flight_Id + "\n";
		buf = buf + "Trip_Status: " + Trip_Status + "\n";
		buf = buf + "Criticality: " + Criticality + "\n";
		buf = buf + "Subzone_Id: " + Subzone_Id + "\n";
		
		return buf;
	}

	public String getTag_Id() {
		return Tag_Id;
	}

	@XmlElement
	public void setTag_Id(String tag_Id) {
		Tag_Id = tag_Id;
	}

	public String getShip_Date() {
		return Ship_Date;
	}

	@XmlElement
	public void setShip_Date(String ship_Date) {
		Ship_Date = ship_Date;
	}

	public String getInv_Id() {
		return Inv_Id;
	}

	@XmlElement
	public void setInv_Id(String inv_Id) {
		Inv_Id = inv_Id;
	}

	public String getShip_Source() {
		return Ship_Source;
	}

	@XmlElement
	public void setShip_Source(String ship_Source) {
		Ship_Source = ship_Source;
	}

	public String getShip_Dest() {
		return Ship_Dest;
	}

	@XmlElement
	public void setShip_Dest(String ship_Dest) {
		Ship_Dest = ship_Dest;
	}

	public String getShip_Loc() {
		return Ship_Loc;
	}

	@XmlElement
	public void setShip_Loc(String ship_Loc) {
		Ship_Loc = ship_Loc;
	}

	public String getFlight_Id() {
		return Flight_Id;
	}

	@XmlElement
	public void setFlight_Id(String flight_Id) {
		Flight_Id = flight_Id;
	}

	public String getTrip_Status() {
		return Trip_Status;
	}

	@XmlElement
	public void setTrip_Status(String trip_Status) {
		Trip_Status = trip_Status;
	}

	public String getCriticality() {
		return Criticality;
	}

	@XmlElement
	public void setCriticality(String criticality) {
		Criticality = criticality;
	}

	public String getSubzone_Id() {
		return Subzone_Id;
	}

	@XmlElement
	public void setSubzone_Id(String subzone_Id) {
		Subzone_Id = subzone_Id;
	}
	

}
