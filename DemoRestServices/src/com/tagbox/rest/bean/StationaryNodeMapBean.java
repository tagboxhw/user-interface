package com.tagbox.rest.bean;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "stationaryNodeMapBean")
public class StationaryNodeMapBean implements Serializable{

	/**
	 * Generated serial version ID
	 */
	private static final long serialVersionUID = -1874474658549948076L;
	
	private String GW_Zone_id;
	private String GW_client_id;
	private String ND_client_id;
	private String ND_device_id;
	
	private String GW_ND_Pairing;
	private String ND_subzone_type;
	private String ND_subzone_name;
	private String ND_type;
	private String ND_status;
	
	public StationaryNodeMapBean(){
		super();
	}
	
	public void init() {
		this.GW_Zone_id = "";
		this.GW_client_id = "";
		this.ND_device_id = "";
		this.ND_client_id = "";
		this.GW_ND_Pairing = "";
		this.ND_type = "";
		this.ND_status = "";
		this.ND_subzone_type = "";
		this.ND_subzone_name = "";
	}
	
	public String toString(){
		String buf = null;
		buf = "Stationary Node Map object: \n";
		buf = buf + "GW_Zone_id: " + GW_Zone_id + "\n";
		buf = buf + "GW_client_id: " + GW_client_id + "\n";
		buf = buf + "ND_client_id: " + ND_client_id + "\n";
		buf = buf + "ND_device_id: " + ND_device_id + "\n";
		buf = buf + "GW_ND_Pairing: " + GW_ND_Pairing + "\n";
		buf = buf + "ND_subzone_type: " + ND_subzone_type + "\n";
		buf = buf + "ND_subzone_name: " + ND_subzone_name + "\n";
		buf = buf + "ND_type: " + ND_type + "\n";
		buf = buf + "ND_status: " + ND_status + "\n";
		
		return buf;
	}

	/**
	 * @return the GW_Zone_id
	 */
	public String getGW_Zone_id() {
		return GW_Zone_id;
	}

	/**
	 * @param GW_Zone_id the GW_Zone_id to set
	 */
	@XmlElement
	public void setGW_Zone_id(String gW_Zone_id) {
		GW_Zone_id = gW_Zone_id;
	}

	/**
	 * @return the gW_client_id
	 */
	public String getGW_client_id() {
		return GW_client_id;
	}

	/**
	 * @param gW_client_id the gW_client_id to set
	 */
	@XmlElement
	public void setGW_client_id(String gW_client_id) {
		GW_client_id = gW_client_id;
	}

	/**
	 * @return the nD_device_id
	 */
	public String getND_device_id() {
		return ND_device_id;
	}

	/**
	 * @param nD_device_id the nD_device_id to set
	 */
	@XmlElement
	public void setND_device_id(String nD_device_id) {
		ND_device_id = nD_device_id;
	}

	/**
	 * @return the nD_client_id
	 */
	public String getND_client_id() {
		return ND_client_id;
	}

	/**
	 * @param nD_client_id the nD_client_id to set
	 */
	@XmlElement
	public void setND_client_id(String nD_client_id) {
		ND_client_id = nD_client_id;
	}

	/**
	 * @return the gW_ND_Pairing
	 */
	public String getGW_ND_Pairing() {
		return GW_ND_Pairing;
	}

	/**
	 * @param gW_ND_Pairing the gW_ND_Pairing to set
	 */
	@XmlElement
	public void setGW_ND_Pairing(String gW_ND_Pairing) {
		GW_ND_Pairing = gW_ND_Pairing;
	}

	/**
	 * @return the nD_type
	 */
	public String getND_type() {
		return ND_type;
	}

	/**
	 * @param nD_type the nD_type to set
	 */
	@XmlElement
	public void setND_type(String nD_type) {
		ND_type = nD_type;
	}

	/**
	 * @return the nD_status
	 */
	public String getND_status() {
		return ND_status;
	}

	/**
	 * @param nD_status the nD_status to set
	 */
	@XmlElement
	public void setND_status(String nD_status) {
		this.ND_status = nD_status;
	}

	
	/**
	 * @return the nD_subzone_type
	 */
	public String getND_subzone_type() {
		return ND_subzone_type;
	}

	/**
	 * @param nD_subzone_type the nD_subzone_type to set
	 */
	@XmlElement
	public void setND_subzone_type(String nD_subzone_type) {
		ND_subzone_type = nD_subzone_type;
	}

	/**
	 * @return the nD_subzone_name
	 */
	public String getND_subzone_name() {
		return ND_subzone_name;
	}

	/**
	 * @param nD_subzone_name the nD_subzone_name to set
	 */
	@XmlElement
	public void setND_subzone_name(String nD_subzone_name) {
		ND_subzone_name = nD_subzone_name;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	

}
