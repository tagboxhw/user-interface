package com.tagbox.rest.bean;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "shipmentTags")
public class ShipmentTags implements Serializable{

	/**
	 * Generated serial version ID
	 */
	private static final long serialVersionUID = -1874474658549948076L;
	private String sTagId;
	
	public ShipmentTags(){
		super();
	}
	
	public void init() {
		this.sTagId = "";
		
	}
	
	public String toString(){
		String buf = null;
		buf = "Shipment Tags object: \n";
		buf = buf + "Shipment Tag: " + sTagId + "\n";
		return buf;
	}

	public String getShipmentTagId() {
		return sTagId;
	}

	public void setShipmentTagId(String sTagId) {
		this.sTagId = sTagId;
	}
}
