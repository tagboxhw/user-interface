package com.tagbox.rest.bean;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement (name="planMapsBean")
public class PlanMapsBean implements Serializable{

	/**
	 * Generated serial version ID
	 */
	private static final long serialVersionUID = -1874474658549948076L;
	
	private String gateway_id;
	private float latitude;
	private float longitude;
	private String dcName;
	/**
	 * @return the gateway_id
	 */
	public String getGateway_id() {
		return gateway_id;
	}

	/**
	 * @param gateway_id the gateway_id to set
	 */
	@XmlElement
	public void setGateway_id(String gateway_id) {
		this.gateway_id = gateway_id;
	}

	/**
	 * @return the device_id
	 */
	public String getDCName() {
		return dcName;
	}

	/**
	 * @param device_id the device_id to set
	 */
	@XmlElement
	public void setDCName(String device_id) {
		this.dcName = device_id;
	}
	/**
	 * @return the latitude
	 */
	public float getLatitude() {
		return latitude;
	}

	/**
	 * @param latitude the latitude to set
	 */
	@XmlElement
	public void setLatitude(float latitude) {
		this.latitude = latitude;
	}

	/**
	 * @return the longitude
	 */
	public float getLongitude() {
		return longitude;
	}

	/**
	 * @param longitude the longitude to set
	 */
	@XmlElement
	public void setLongitude(float longitude) {
		this.longitude = longitude;
	}

	
	
	public PlanMapsBean(){
		super();
	}
	
	public void init() {
		this.gateway_id = "";
		this.dcName = "";
		this.latitude = 0;
		this.longitude = 0;
		
	}
	
	public String toString(){
		String buf = null;
		buf = "Planned Maps object: \n";
		buf = buf + "gateway_id: " + gateway_id + "\n";
		buf = buf + "DC Name: " + dcName + "\n";
		buf = buf + "latitude: " + latitude + "\n";
		buf = buf + "longitude: " + longitude + "\n";
		
		return buf;
	}
	

}
