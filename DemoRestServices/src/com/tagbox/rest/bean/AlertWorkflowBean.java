package com.tagbox.rest.bean;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "alertWorkflowBean")
public class AlertWorkflowBean implements Serializable{

	/**
	 * Generated serial version ID
	 */
	private static final long serialVersionUID = -1874474658549948076L;
	
	private String GW_Client_ID;
	private String ND_Client_ID;
	private String ClassType;
	private String Alert_ID;
	private String Alert_Location;
	
	private String Alert_Type;
	private String Breach_Type;
	private float Alert_Parameter_Avg;
	private String Alert_Timestamp_From;
	private String Alert_Timestamp_To;
	private String Alert_Duration;
	private String Alert_Severity;
	
	private String Current_Location_Lat;
	private String Current_Location_Long;
	private String Current_Location_Name;
	private String Route_Type;
	private String Action_Status;
	private String Reopen_Flag;
	private String Reopen_Status;
	private String Assigned_To;
	private String Reassigned_To;
	private String Assigned_To_Reopen;
	private String Reassigned_To_Reopen;
	private String Root_Cause;
	private String Preventive_Actions;
	private String Supervisor_Review;
	private String Supervisor_Review_Reopen;
		
	public AlertWorkflowBean(){
		super();
	}
	
	public void init() {
		this.GW_Client_ID = "";
		this.ND_Client_ID = "";
		this.ClassType = "";
		this.Alert_ID = "";
		this.Alert_Location = "";
		this.Alert_Type = "";
		this.Alert_Parameter_Avg = 0;
		this.Alert_Timestamp_From = "";
		this.Alert_Timestamp_To = "";
		this.Alert_Duration = "";
		this.Alert_Severity = "";
		this.Breach_Type = "";
		this.Current_Location_Lat = "";
		this.Current_Location_Long = "";
		this.Current_Location_Name = "";
		this.Route_Type = "";
		this.Action_Status = "";
		this.Reopen_Flag = "";
		this.Reopen_Status = "";
		this.Assigned_To = "";
		this.Reassigned_To = "";
		this.Assigned_To_Reopen = "";
		this.Reassigned_To_Reopen = "";
		this.Root_Cause = "";
		this.Preventive_Actions = "";
		this.Supervisor_Review = "";
		this.Supervisor_Review_Reopen = "";
	}
	
	public String toString(){
		String buf = null;
		buf = "Alert Workflow object: \n";
		buf = buf + "GW_Client_ID: " + GW_Client_ID + "\n";
		buf = buf + "ND_Client_ID: " + ND_Client_ID + "\n";
		buf = buf + "Class: " + ClassType + "\n";
		buf = buf + "Alert_ID: " + Alert_ID + "\n";
		buf = buf + "Alert_Location: " + Alert_Location + "\n";
		buf = buf + "Alert_Type: " + Alert_Type + "\n";
		buf = buf + "Alert_Parameter_Avg: " + Alert_Parameter_Avg + "\n";
		buf = buf + "Alert_Timestamp_From: " + Alert_Timestamp_From + "\n";
		buf = buf + "Alert_Timestamp_To: " + Alert_Timestamp_To + "\n";
		buf = buf + "Alert_Duration: " + Alert_Duration + "\n";
		buf = buf + "Alert_Severity: " + Alert_Severity + "\n";
		buf = buf + "Current_Location_Lat: " + Current_Location_Lat + "\n";
		buf = buf + "Current_Location_Long: " + Current_Location_Long + "\n";
		buf = buf + "Current_Location_Name: " + Current_Location_Name + "\n";
		buf = buf + "Breach_Type: " + Breach_Type + "\n";
		buf = buf + "Route_Type: " + Route_Type + "\n";
		buf = buf + "Action_Status: " + Action_Status + "\n";
		buf = buf + "Reopen_Flag: " + Reopen_Flag + "\n";
		buf = buf + "Reopen_Status: " + Reopen_Status + "\n";
		buf = buf + "Assigned_To: " + Assigned_To + "\n";
		buf = buf + "Reassigned_To: " + Reassigned_To + "\n";
		buf = buf + "Assigned_To_Reopen: " + Assigned_To_Reopen + "\n";
		buf = buf + "Reassigned_To_Reopen: " + Reassigned_To_Reopen + "\n";
		buf = buf + "Root_Cause: " + Root_Cause + "\n";
		buf = buf + "Preventive_Actions: " + Preventive_Actions + "\n";
		buf = buf + "Supervisor_Review: " + Supervisor_Review + "\n";
		buf = buf + "Supervisor_Review_Reopen: " + Supervisor_Review_Reopen + "\n";
		
		return buf;
	}

	/**
	 * @return the gW_Client_ID
	 */
	public String getGW_Client_ID() {
		return GW_Client_ID;
	}

	/**
	 * @param gW_Client_ID the gW_Client_ID to set
	 */
	@XmlElement
	public void setGW_Client_ID(String gW_Client_ID) {
		GW_Client_ID = gW_Client_ID;
	}

	/**
	 * @return the nD_Client_ID
	 */
	public String getND_Client_ID() {
		return ND_Client_ID;
	}

	/**
	 * @param nD_Client_ID the nD_Client_ID to set
	 */
	@XmlElement
	public void setND_Client_ID(String nD_Client_ID) {
		ND_Client_ID = nD_Client_ID;
	}

	/**
	 * @return the ClassType
	 */
	public String getClassType() {
		return this.ClassType;
	}

	/**
	 * @param ClassType the ClassType to set
	 */
	@XmlElement
	public void setClassType(String ClassType) {
		this.ClassType = ClassType;
	}

	/**
	 * @return the alert_ID
	 */
	public String getAlert_ID() {
		return Alert_ID;
	}

	/**
	 * @param alert_ID the alert_ID to set
	 */
	@XmlElement
	public void setAlert_ID(String alert_ID) {
		Alert_ID = alert_ID;
	}

	/**
	 * @return the alert_Location
	 */
	public String getAlert_Location() {
		return Alert_Location;
	}

	/**
	 * @param alert_Location the alert_Location to set
	 */
	@XmlElement
	public void setAlert_Location(String alert_Location) {
		Alert_Location = alert_Location;
	}

	/**
	 * @return the alert_Type
	 */
	public String getAlert_Type() {
		return Alert_Type;
	}

	/**
	 * @param alert_Type the alert_Type to set
	 */
	@XmlElement
	public void setAlert_Type(String alert_Type) {
		Alert_Type = alert_Type;
	}

	/**
	 * @return the alert_Parameter_Avg
	 */
	public float getAlert_Parameter_Avg() {
		return Alert_Parameter_Avg;
	}

	/**
	 * @param alert_Parameter_Avg the alert_Parameter_Avg to set
	 */
	@XmlElement
	public void setAlert_Parameter_Avg(float alert_Parameter_Avg) {
		Alert_Parameter_Avg = alert_Parameter_Avg;
	}

	/**
	 * @return the alert_Timestamp_From
	 */
	public String getAlert_Timestamp_From() {
		return Alert_Timestamp_From;
	}

	/**
	 * @param alert_Timestamp_From the alert_Timestamp_From to set
	 */
	@XmlElement
	public void setAlert_Timestamp_From(String alert_Timestamp_From) {
		Alert_Timestamp_From = alert_Timestamp_From;
	}

	/**
	 * @return the alert_Timestamp_To
	 */
	public String getAlert_Timestamp_To() {
		return Alert_Timestamp_To;
	}

	/**
	 * @param alert_Timestamp_To the alert_Timestamp_To to set
	 */
	@XmlElement
	public void setAlert_Timestamp_To(String alert_Timestamp_To) {
		Alert_Timestamp_To = alert_Timestamp_To;
	}

	/**
	 * @return the alert_Duration
	 */
	public String getAlert_Duration() {
		return Alert_Duration;
	}

	/**
	 * @param alert_Duration the alert_Duration to set
	 */
	@XmlElement
	public void setAlert_Duration(String alert_Duration) {
		Alert_Duration = alert_Duration;
	}

	/**
	 * @return the alert_Severity
	 */
	public String getAlert_Severity() {
		return Alert_Severity;
	}

	/**
	 * @param alert_Severity the alert_Severity to set
	 */
	@XmlElement
	public void setAlert_Severity(String alert_Severity) {
		Alert_Severity = alert_Severity;
	}

	/**
	 * @return the route_Type
	 */
	public String getRoute_Type() {
		return Route_Type;
	}

	/**
	 * @param route_Type the route_Type to set
	 */
	@XmlElement
	public void setRoute_Type(String route_Type) {
		Route_Type = route_Type;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the action_Status
	 */
	public String getAction_Status() {
		return Action_Status;
	}

	/**
	 * @param action_Status the action_Status to set
	 */
	@XmlElement
	public void setAction_Status(String action_Status) {
		Action_Status = action_Status;
	}

	/**
	 * @return the reopen_Flag
	 */
	public String getReopen_Flag() {
		return Reopen_Flag;
	}

	/**
	 * @param reopen_Flag the reopen_Flag to set
	 */
	@XmlElement
	public void setReopen_Flag(String reopen_Flag) {
		Reopen_Flag = reopen_Flag;
	}

	/**
	 * @return the reopen_Status
	 */
	public String getReopen_Status() {
		return Reopen_Status;
	}

	/**
	 * @param reopen_Status the reopen_Status to set
	 */
	@XmlElement
	public void setReopen_Status(String reopen_Status) {
		Reopen_Status = reopen_Status;
	}

	/**
	 * @return the assigned_To
	 */
	public String getAssigned_To() {
		return Assigned_To;
	}

	/**
	 * @param assigned_To the assigned_To to set
	 */
	@XmlElement
	public void setAssigned_To(String assigned_To) {
		Assigned_To = assigned_To;
	}

	/**
	 * @return the reassigned_To
	 */
	public String getReassigned_To() {
		return Reassigned_To;
	}

	/**
	 * @param reassigned_To the reassigned_To to set
	 */
	@XmlElement
	public void setReassigned_To(String reassigned_To) {
		Reassigned_To = reassigned_To;
	}

	/**
	 * @return the assigned_To_Reopen
	 */
	public String getAssigned_To_Reopen() {
		return Assigned_To_Reopen;
	}

	/**
	 * @param assigned_To_Reopen the assigned_To_Reopen to set
	 */
	@XmlElement
	public void setAssigned_To_Reopen(String assigned_To_Reopen) {
		Assigned_To_Reopen = assigned_To_Reopen;
	}

	/**
	 * @return the reassigned_To_Reopen
	 */
	public String getReassigned_To_Reopen() {
		return Reassigned_To_Reopen;
	}

	/**
	 * @param reassigned_To_Reopen the reassigned_To_Reopen to set
	 */
	@XmlElement
	public void setReassigned_To_Reopen(String reassigned_To_Reopen) {
		Reassigned_To_Reopen = reassigned_To_Reopen;
	}

	/**
	 * @return the root_Cause
	 */
	public String getRoot_Cause() {
		return Root_Cause;
	}

	/**
	 * @param root_Cause the root_Cause to set
	 */
	@XmlElement
	public void setRoot_Cause(String root_Cause) {
		Root_Cause = root_Cause;
	}

	/**
	 * @return the preventive_Actions
	 */
	public String getPreventive_Actions() {
		return Preventive_Actions;
	}

	/**
	 * @param preventive_Actions the preventive_Actions to set
	 */
	@XmlElement
	public void setPreventive_Actions(String preventive_Actions) {
		Preventive_Actions = preventive_Actions;
	}

	/**
	 * @return the supervisor_Review
	 */
	public String getSupervisor_Review() {
		return Supervisor_Review;
	}

	/**
	 * @param supervisor_Review the supervisor_Review to set
	 */
	@XmlElement
	public void setSupervisor_Review(String supervisor_Review) {
		Supervisor_Review = supervisor_Review;
	}

	/**
	 * @return the supervisor_Review_Reopen
	 */
	public String getSupervisor_Review_Reopen() {
		return Supervisor_Review_Reopen;
	}

	/**
	 * @param supervisor_Review_Reopen the supervisor_Review_Reopen to set
	 */
	@XmlElement
	public void setSupervisor_Review_Reopen(String supervisor_Review_Reopen) {
		Supervisor_Review_Reopen = supervisor_Review_Reopen;
	}

	/**
	 * @return the breach_Type
	 */
	public String getBreach_Type() {
		return Breach_Type;
	}

	/**
	 * @param breach_Type the breach_Type to set
	 */
	@XmlElement
	public void setBreach_Type(String breach_Type) {
		Breach_Type = breach_Type;
	}

	/**
	 * @return the current_Location_Lat
	 */
	public String getCurrent_Location_Lat() {
		return Current_Location_Lat;
	}

	/**
	 * @param current_Location_Lat the current_Location_Lat to set
	 */
	@XmlElement
	public void setCurrent_Location_Lat(String current_Location_Lat) {
		Current_Location_Lat = current_Location_Lat;
	}

	/**
	 * @return the current_Location_Long
	 */
	public String getCurrent_Location_Long() {
		return Current_Location_Long;
	}

	/**
	 * @param current_Location_Long the current_Location_Long to set
	 */
	@XmlElement
	public void setCurrent_Location_Long(String current_Location_Long) {
		Current_Location_Long = current_Location_Long;
	}

	/**
	 * @return the current_Location_Name
	 */
	public String getCurrent_Location_Name() {
		return Current_Location_Name;
	}

	/**
	 * @param current_Location_Name the current_Location_Name to set
	 */
	@XmlElement
	public void setCurrent_Location_Name(String current_Location_Name) {
		Current_Location_Name = current_Location_Name;
	}
}
