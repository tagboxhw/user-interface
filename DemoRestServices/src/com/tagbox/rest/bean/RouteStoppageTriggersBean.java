package com.tagbox.rest.bean;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement (name="routeStoppageTriggersBean")
public class RouteStoppageTriggersBean implements Serializable{

	/**
	 * Generated serial version ID
	 */
	private static final long serialVersionUID = -1874474658549948076L;
	
	private String GW_client_id;
	private String ND_client_id;
	private int Threshold_Window_Planned;
	private int Threshold_Window_Unplanned;
	
	public RouteStoppageTriggersBean(){
		super();
	}
	
	public void init() {
		this.GW_client_id = "";
		this.ND_client_id = "";
		this.Threshold_Window_Planned = 0;
		this.Threshold_Window_Unplanned = 0;
	}
	
	public String toString(){
		String buf = null;
		buf = "Route Stoppage Triggers object: \n";
		buf = buf + "GW_client_id: " + GW_client_id + "\n";
		buf = buf + "ND_client_id: " + ND_client_id + "\n";
		buf = buf + "Threshold_Window_Planned: " + Threshold_Window_Planned + "\n";
		buf = buf + "Threshold_Window_Unplanned: " + Threshold_Window_Unplanned + "\n";
		return buf;
	}

	/**
	 * @return the gW_client_id
	 */
	public String getGW_client_id() {
		return GW_client_id;
	}

	/**
	 * @param gW_client_id the gW_client_id to set
	 */
	@XmlElement
	public void setGW_client_id(String gW_client_id) {
		GW_client_id = gW_client_id;
	}

	/**
	 * @return the nD_client_id
	 */
	public String getND_client_id() {
		return ND_client_id;
	}

	/**
	 * @param nD_client_id the nD_client_id to set
	 */
	@XmlElement
	public void setND_client_id(String nD_client_id) {
		ND_client_id = nD_client_id;
	}

	/**
	 * @return the threshold_Window_Planned
	 */
	public int getThreshold_Window_Planned() {
		return Threshold_Window_Planned;
	}

	/**
	 * @param threshold_Window_Planned the threshold_Window_Planned to set
	 */
	@XmlElement
	public void setThreshold_Window_Planned(int threshold_Window_Planned) {
		Threshold_Window_Planned = threshold_Window_Planned;
	}

	/**
	 * @return the threshold_Window_Unplanned
	 */
	public int getThreshold_Window_Unplanned() {
		return Threshold_Window_Unplanned;
	}

	/**
	 * @param threshold_Window_Unplanned the threshold_Window_Unplanned to set
	 */
	@XmlElement
	public void setThreshold_Window_Unplanned(int threshold_Window_Unplanned) {
		Threshold_Window_Unplanned = threshold_Window_Unplanned;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
