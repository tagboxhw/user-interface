package com.tagbox.rest.bean;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="clientSubLocationMapBean")
public class ClientSubLocationMapBean implements Serializable{

	/**
	 * Generated serial version ID
	 */
	private static final long serialVersionUID = -1874474658549948076L;
	
	private String sub_location_id;
	private String location_id;
	private String sub_location_name;
	/**
	 * @return the sub_location_id
	 */
	public String getSubLocationId() {
		return sub_location_id;
	}

	/**
	 * @param sub_location_id the sub_location_id to set
	 */
	@XmlElement 
	public void setSubLocationId(String sub_location_id) {
		this.sub_location_id = sub_location_id;
	}

	/**
	 * @return the location_id
	 */
	public String getLocationId() {
		return location_id;
	}

	/**
	 * @param location_id the location_id to set
	 */
	@XmlElement 
	public void setLocationId(String location_id) {
		this.location_id = location_id;
	}

	/**
	 * @return the sub_location_name
	 */
	public String getSubLocationName() {
		return sub_location_name;
	}

	/**
	 * @param sub_location_name the sub_location_name to set
	 */
	@XmlElement 
	public void setSubLocationName(String sub_location_name) {
		this.sub_location_name = sub_location_name;
	}
		
	public ClientSubLocationMapBean(){
		super();
	}
	
	public void init() {
		this.sub_location_id = "";
		this.location_id = "";
		this.sub_location_name = "";
	}
	
	public String toString(){
		String buf = null;
		buf = "Client Sub-Location Map object: \n";
		buf = buf + "sub_location_id: " + sub_location_id + "\n";
		buf = buf + "location_id: " + location_id + "\n";
		buf = buf + "sub_location_name: " + sub_location_name + "\n";
		return buf;
	}
	

}
