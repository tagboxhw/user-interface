package com.tagbox.rest.bean;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "humidityBean")
public class HumidityBean implements Serializable{

	/**
	 * Generated serial version ID
	 */
	private static final long serialVersionUID = -1874474658549948076L;
	private String Location_id;
	private String Zone_id;
	private String Node_id;
	private String Timestamp;
	private String Humidity;
	
	public HumidityBean(){
		super();
	}
	
	public void init() {
		this.Location_id = "";
		this.Zone_id = "";
		this.Node_id = "";
		this.Timestamp = "";
		this.Humidity = "";
	}
	
	public String toString(){
		String buf = null;
		buf = "Humidity object: \n";
		buf = buf + "Location_id: " + Location_id + "\n";
		buf = buf + "Zone_id: " + Zone_id + "\n";
		buf = buf + "Node_id: " + Node_id + "\n";
		buf = buf + "Timestamp: " + Timestamp + "\n";
		buf = buf + "Humidity: " + Humidity + "\n";
		return buf;
	}

	public String getLocation_id() {
		return Location_id;
	}

	public void setLocation_id(String location_id) {
		Location_id = location_id;
	}

	/**
	 * @return the Zone_id
	 */
	public String getZone_Id() {
		return this.Zone_id;
	}

	/**
	 * @param Zone_id the Zone_id to set
	 */
	@XmlElement
	public void setZone_Id(String Zone_id) {
		this.Zone_id = Zone_id;
	}

	/**
	 * @return the Node_id
	 */
	public String getNode_Id() {
		return this.Node_id;
	}

	/**
	 * @param Node_id the Node_id to set
	 */
	@XmlElement
	public void setNode_Id(String Node_id) {
		this.Node_id = Node_id;
	}

	/**
	 * @return the timestamp
	 */
	public String getTimestamp() {
		return this.Timestamp;
	}

	/**
	 * @param timestamp the timestamp to set
	 */
	@XmlElement
	public void setTimestamp(String timestamp) {
		this.Timestamp = timestamp;
	}

	/**
	 * @return the Humidity
	 */
	public String getHumidity() {
		return this.Humidity;
	}

	/**
	 * @param Humidity the Humidity to set
	 */
	@XmlElement
	public void setHumidity(String Humidity) {
		this.Humidity = Humidity;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	

}
