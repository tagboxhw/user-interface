package com.tagbox.rest.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tagbox.rest.bean.ShipmentTransitInfo;
import com.tagbox.rest.bean.ShipmentBean;
import com.tagbox.rest.bean.ShipmentBoxInfo;
import com.tagbox.rest.bean.ShipmentLocations;
import com.tagbox.rest.bean.ShipmentProducts;
import com.tagbox.rest.bean.ShipmentTags;
import com.tagbox.rest.bean.ShipmentUsers;
import com.tagbox.rest.bean.TemperatureBean;
import com.tagbox.rest.util.DbConnection;
import com.tagbox.rest.util.SqlConstants;
import com.tagbox.rest.util.Constants;

public class ShipmentDao {
	static final Logger logger = LoggerFactory.getLogger(ShipmentDao.class);

	public String getNewShipmentId() {
		Connection conn = null;
		CallableStatement pstmt = null;
		ResultSet rs = null;
		String sReturn = "";
		try {
			conn = DbConnection.getConnection();
			conn.setAutoCommit(false);
			pstmt = conn.prepareCall(SqlConstants.GetNewShipmentId);
			pstmt.registerOutParameter(1, Types.OTHER);
			pstmt.execute();
			rs = (ResultSet) pstmt.getObject(1);
			while (rs.next()) {
				if (rs.getString(1) == null || rs.getString(1).equals("")) {
					sReturn = "";
				} else {
					sReturn = rs.getString(1);
				}
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			sReturn = Constants.ERROR + ": " + e.getLocalizedMessage();
			//logger.error("Error", e);
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return sReturn;
	}

	public Collection<ShipmentBean> getShipmentDetails() {
		Connection conn = null;
		CallableStatement pstmt = null;
		ResultSet rs = null;
		Collection<ShipmentBean> col = new ArrayList<ShipmentBean>();
		ShipmentBean shipmentBean = new ShipmentBean();
		try {
			conn = DbConnection.getConnection();
			conn.setAutoCommit(false);
			pstmt = conn.prepareCall(SqlConstants.GetShipmentDetails);
			pstmt.registerOutParameter(1, Types.OTHER);
			pstmt.execute();
			rs = (ResultSet) pstmt.getObject(1);
			while (rs.next()) {
				shipmentBean = new ShipmentBean();
				if (rs.getString(1) == null) {
					shipmentBean.setShipmentId(0);
				} else {
					shipmentBean.setShipmentId(rs.getInt(1));
				}
				if (rs.getString(2) == null || rs.getString(2).equals("")) {
					shipmentBean.setOrigin("");
				} else {
					shipmentBean.setOrigin(rs.getString(2));
				}
				if (rs.getString(3) == null || rs.getString(3).equals("")) {
					shipmentBean.setDestination("");
				} else {
					shipmentBean.setDestination(rs.getString(3));
				}
				if (rs.getString(4) == null || rs.getString(4).equals("")) {
					shipmentBean.setETD("");
				} else {
					shipmentBean.setETD(rs.getString(4));
				}
				if (rs.getString(5) == null || rs.getString(5).equals("")) {
					shipmentBean.setETA("");
				} else {
					shipmentBean.setETA(rs.getString(5));
				}
				col.add(shipmentBean);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return col;
	}
	
	public Collection<ShipmentBean> getAllShipments() {
		Connection conn = null;
		CallableStatement pstmt = null;
		ResultSet rs = null;
		Collection<ShipmentBean> col = new ArrayList<ShipmentBean>();
		ShipmentBean sb = new ShipmentBean();
		try {
			conn = DbConnection.getConnection();
			conn.setAutoCommit(false);
			pstmt = conn.prepareCall(SqlConstants.GetAllShipments);
			pstmt.registerOutParameter(1, Types.OTHER);
			pstmt.execute();
			rs = (ResultSet) pstmt.getObject(1);
			while (rs.next()) {
				sb = new ShipmentBean();
				sb.setShipmentId(rs.getInt(1));
				if(rs.getString(2) == null || rs.getString(2).equals("")) {
					sb.setOrigin("");
				} else {
					sb.setOrigin(rs.getString(2));
				}
				if(rs.getString(3) == null || rs.getString(3).equals("")) {
					sb.setDestination("");
				} else {
					sb.setDestination(rs.getString(3));
				}
				if(rs.getString(4) == null || rs.getString(4).equals("")) {
					sb.setETD("");
				} else {
					sb.setETD(rs.getString(4));
				}
				if(rs.getString(5) == null || rs.getString(5).equals("")) {
					sb.setETA("");
				} else {
					sb.setETA(rs.getString(5));
				}
				if(rs.getString(6) == null || rs.getString(6).equals("")) {
					sb.setUser("");
				} else {
					sb.setUser(rs.getString(6));
				}
				if(rs.getString(7) == null || rs.getString(7).equals("")) {
					sb.setInvoiceNumber("");
				} else {
					sb.setInvoiceNumber(rs.getString(7));
				}
				col.add(sb);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return col;
	}

	public Collection<ShipmentLocations> getShipmentLocations() {
		Connection conn = null;
		CallableStatement pstmt = null;
		ResultSet rs = null;
		Collection<ShipmentLocations> col = new ArrayList<ShipmentLocations>();
		ShipmentLocations sl = new ShipmentLocations();
		try {
			conn = DbConnection.getConnection();
			conn.setAutoCommit(false);
			pstmt = conn.prepareCall(SqlConstants.GetShipmentLocations);
			pstmt.registerOutParameter(1, Types.OTHER);
			pstmt.execute();
			rs = (ResultSet) pstmt.getObject(1);
			while (rs.next()) {
				sl = new ShipmentLocations();
				if (rs.getString(1) == null || rs.getString(1).equals("")) {
					sl.setShipmentLocationId("");
				} else {
					sl.setShipmentLocationId(rs.getString(1));
				}
				col.add(sl);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return col;
	}

	public Collection<ShipmentTags> getShipmentTags() {
		Connection conn = null;
		CallableStatement pstmt = null;
		ResultSet rs = null;
		Statement statement = null;
		String sTag = "";
		Collection<ShipmentTags> col = new ArrayList<ShipmentTags>();
		ShipmentTags sTags = new ShipmentTags();
		try {
			conn = DbConnection.getConnection();
			conn.setAutoCommit(false);
			pstmt = conn.prepareCall(SqlConstants.GetShipmentTags);
			pstmt.registerOutParameter(1, Types.OTHER);
			pstmt.execute();
			rs = (ResultSet) pstmt.getObject(1);
			while (rs.next()) {
				sTags = new ShipmentTags();
				if (rs.getString(1) == null || rs.getString(1).equals("")) {
					sTags.setShipmentTagId("");
				} else {
					sTags.setShipmentTagId(rs.getString(1));
				}
				col.add(sTags);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return col;
	}

	public Collection<ShipmentProducts> getShipmentProducts() {
		Connection conn = null;
		CallableStatement pstmt = null;
		ResultSet rs = null;
		Collection<ShipmentProducts> col = new ArrayList<ShipmentProducts>();
		ShipmentProducts sProducts = new ShipmentProducts();
		try {
			conn = DbConnection.getConnection();
			conn.setAutoCommit(false);
			pstmt = conn.prepareCall(SqlConstants.GetShipmentProducts);
			pstmt.registerOutParameter(1, Types.OTHER);
			pstmt.execute();
			rs = (ResultSet) pstmt.getObject(1);
			while (rs.next()) {
				sProducts = new ShipmentProducts();
				if (rs.getString(1) == null || rs.getString(1).equals("")) {
					sProducts.setShipmentProductId("");
				} else {
					sProducts.setShipmentProductId(rs.getString(1));
				}
				col.add(sProducts);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return col;
	}

	public Collection<ShipmentUsers> getShipmentUsers() {
		Connection conn = null;
		CallableStatement pstmt = null;
		ResultSet rs = null;
		Collection<ShipmentUsers> col = new ArrayList<ShipmentUsers>();
		ShipmentUsers su = new ShipmentUsers();
		try {
			conn = DbConnection.getConnection();
			conn.setAutoCommit(false);
			pstmt = conn.prepareCall(SqlConstants.GetShipmentUsers);
			pstmt.registerOutParameter(1, Types.OTHER);
			pstmt.execute();
			rs = (ResultSet) pstmt.getObject(1);
			while (rs.next()) {
				su = new ShipmentUsers();
				if (rs.getString(1) == null || rs.getString(1).equals("")) {
					su.setShipmentUserId("");
				} else {
					su.setShipmentUserId(rs.getString(1));
				}
				if (rs.getString(2) == null || rs.getString(2).equals("")) {
					su.setShipmentUserName("");
				} else {
					su.setShipmentUserName(rs.getString(2));
				}
				col.add(su);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return col;
	}

	public String insertShipmentStep1Data(String sShipmentId, String sSource,
			String sDestination, String sETD, String sETA, String sInvoiceNumber) {
		Connection conn = null;
		CallableStatement pstmt = null;
		ResultSet rs = null;
		String sReturn = Constants.SUCCESS;
		try {
			conn = DbConnection.getConnection();
			try {
				pstmt = conn.prepareCall(SqlConstants.InsertShipmentStep1Data);
				pstmt.registerOutParameter(1, Types.OTHER);
				int i = Integer.parseInt(sShipmentId);
				pstmt.setInt(2, i);
				pstmt.setString(3, sSource);
				pstmt.setString(4, sDestination);
				pstmt.setTimestamp(5, java.sql.Timestamp.valueOf(sETD));
				pstmt.setTimestamp(6, java.sql.Timestamp.valueOf(sETA));
				pstmt.setString(7, sInvoiceNumber);
			} catch (Exception e) {
				e.printStackTrace();
			}
			int iReturn = pstmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
			sReturn = Constants.ERROR + ": " + e.getLocalizedMessage();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return sReturn;
	}

	public String insertShipment2Data(String sShipmentId, String sSourceId,
			String sDestId, String sEtd, String sEta, String sMode,
			String sCarrier) {
		Connection conn = null;
		CallableStatement pstmt = null;
		ResultSet rs = null;
		String sReturn = Constants.SUCCESS;
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			conn = DbConnection.getConnection();
			String[] source = sSourceId.split("#");
			String[] dest = sDestId.split("#");
			String[] etd = sEtd.split("#");
			String[] eta = sEta.split("#");
			String[] mode = sMode.split("#");
			String[] carrier = sCarrier.split("#");
			df.setTimeZone(java.util.TimeZone.getTimeZone("UTC"));
			sdf.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
			Date dateObj = null;
			pstmt = conn.prepareCall(SqlConstants.InsertShipmentStep2Data);
			for (int i = 0; i < source.length; i++) {
				if (source[i] == null || source[i].equals(""))
					continue;
				pstmt.registerOutParameter(1, Types.OTHER);
				pstmt.setInt(2, Integer.parseInt(sShipmentId));
				pstmt.setInt(3, (i - 1));
				pstmt.setString(4, source[i]);
				if (dest[i] == null || dest[i].equals("undefined")) {
					pstmt.setString(5, null);
				} else {
					pstmt.setString(5, dest[i]);
				}
				dateObj = sdf.parse(etd[i]);
				pstmt.setTimestamp(6,
						java.sql.Timestamp.valueOf(df.format(dateObj)));
				dateObj = sdf.parse(eta[i]);
				pstmt.setTimestamp(7,
						java.sql.Timestamp.valueOf(df.format(dateObj)));
				pstmt.setString(8, carrier[i]);
				pstmt.setString(9, mode[i]);
				int iReturn = pstmt.executeUpdate();
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
			sReturn = Constants.ERROR + ": " + e.getLocalizedMessage();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return sReturn;
	}
	
	public String insertLeg(String sShipmentId, String sSourceId,
			String sDestId, String sEtd, String sEta, String sMode,
			String sCarrier, int iLegId) {
		Connection conn = null;
		CallableStatement pstmt = null;
		ResultSet rs = null;
		String sReturn = Constants.SUCCESS;
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			conn = DbConnection.getConnection();
			df.setTimeZone(java.util.TimeZone.getTimeZone("UTC"));
			sdf.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
			Date dateObj = null;
			pstmt = conn.prepareCall(SqlConstants.InsertShipmentStep2Data);
			
				pstmt.registerOutParameter(1, Types.OTHER);
				pstmt.setInt(2, Integer.parseInt(sShipmentId));
				pstmt.setInt(3, iLegId);
				pstmt.setString(4, sSourceId);
				if (sDestId == null || sDestId.equals("undefined")) {
					pstmt.setString(5, null);
				} else {
					pstmt.setString(5, sDestId);
				}
				dateObj = sdf.parse(sEtd);
				pstmt.setTimestamp(6,
						java.sql.Timestamp.valueOf(df.format(dateObj)));
				dateObj = sdf.parse(sEta);
				pstmt.setTimestamp(7,
						java.sql.Timestamp.valueOf(df.format(dateObj)));
				pstmt.setString(8, sCarrier);
				pstmt.setString(9, sMode);
				int iReturn = pstmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
			sReturn = Constants.ERROR + ": " + e.getLocalizedMessage();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return sReturn;
	}

	public String updateShipmentStep1Data(String sShipmentId, String sSource,
			String sDestination, String sETD, String sETA, String sInvoiceNumber) {
		Connection conn = null;
		CallableStatement pstmt = null;
		ResultSet rs = null;
		String sReturn = Constants.SUCCESS;
		try {
			conn = DbConnection.getConnection();

			pstmt = conn.prepareCall(SqlConstants.UpdateShipmentStep1Data);
			pstmt.registerOutParameter(1, Types.OTHER);
			int i = Integer.parseInt(sShipmentId);
			pstmt.setInt(2, i);
			pstmt.setString(3, sSource);
			pstmt.setString(4, sDestination);
			pstmt.setTimestamp(5, java.sql.Timestamp.valueOf(sETD));
			pstmt.setTimestamp(6, java.sql.Timestamp.valueOf(sETA));
			pstmt.setString(7, sInvoiceNumber);

			int iReturn = pstmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			sReturn = Constants.ERROR + ": " + e.getLocalizedMessage();
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return sReturn;
	}
	
	public String updateShipmentStep2Data(String sShipmentId, String sSourceId, String sDestId, String sEtd, String sEta, String sMode, String sCarrier) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Statement statement = null;
		String sReturn = "";
		
		try {
			conn = DbConnection.getConnection();
			String[] source = sSourceId.split("#");
			String[] dest = sDestId.split("#");
			String[] etd = sEtd.split("#");
			String[] eta = sEta.split("#");
			String[] mode = sMode.split("#");
			String[] carrier = sCarrier.split("#");
				Collection col = selectShipmentTransitDetails(sShipmentId);
				
				if(col.size() > (source.length-1)) {
					//some legs have been deleted. Delete those legs from db and update the remaining ones
					
					for(int i=(source.length-1); i<(col.size()); i++) {
						deleteLeg(sShipmentId, i);
					}
					for(int i = 0; i<source.length;i++) {
						if(source[i] == null || source[i].equals("")) continue;
						updateLeg(sShipmentId, source[i], dest[i], etd[i], eta[i], mode[i], carrier[i], (i-1));
					}
				} else if(col.size() < (source.length-1)) {
					//some legs have been added. update the existing ones and insert the new ones
					for(int i = 0; i<(col.size()+1);i++) {
						if(source[i] == null || source[i].equals("")) continue;
						updateLeg(sShipmentId, source[i], dest[i], etd[i], eta[i], mode[i], carrier[i], (i-1));
					}
					for(int i = (col.size()+1); i<source.length;i++) {
							if(source[i] == null || source[i].equals("")) continue;
							insertLeg(sShipmentId, source[i], dest[i], etd[i], eta[i], mode[i], carrier[i], (i-1));
						}
				} else {
					//update the existing legs
					for(int i = 0; i<source.length;i++) {
						if(source[i] == null || source[i].equals("")) continue;
						updateLeg(sShipmentId, source[i], dest[i], etd[i], eta[i], mode[i], carrier[i], (i-1));
					}
				}
		} catch(Exception e) {
			e.printStackTrace();
			return (Constants.ERROR + ": " + e.getLocalizedMessage());
		}
		finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return Constants.SUCCESS;
	}
	
	public String updateLeg(String sShipmentId, String sSourceId, String sDestId, String sEtd, String sEta, String sMode, String sCarrier, int iLegId) {
		Connection conn = null;
		CallableStatement pstmt = null;
		ResultSet rs = null;
		String sReturn = Constants.SUCCESS;
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			conn = DbConnection.getConnection();
			df.setTimeZone(java.util.TimeZone.getTimeZone("UTC"));
			sdf.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
			Date dateObj = null;
			pstmt = conn.prepareCall(SqlConstants.UpdateShipmentStep2Data);
			pstmt.registerOutParameter(1, Types.OTHER);
			int i = Integer.parseInt(sShipmentId);
			pstmt.setInt(2, i);
			pstmt.setInt(3, iLegId);
			pstmt.setString(4, sSourceId);
			if(sDestId == null || sDestId.equals("undefined")) {
				pstmt.setString(5, null);
			} else {
				pstmt.setString(5, sDestId);
			}
			dateObj = sdf.parse(sEtd);
			pstmt.setTimestamp(6, java.sql.Timestamp.valueOf(df.format(dateObj)));
			dateObj = sdf.parse(sEta);
			pstmt.setTimestamp(7, java.sql.Timestamp.valueOf(df.format(dateObj)));
			pstmt.setString(8, sCarrier);
			pstmt.setString(9, sMode);
			int iReturn = pstmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			sReturn = Constants.ERROR + ": " + e.getLocalizedMessage();
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return sReturn;
	}

	public String deleteShipment(String sShipmentId) {
		Connection conn = null;
		CallableStatement pstmt = null;
		ResultSet rs = null;
		String sReturn = Constants.SUCCESS;
		try {
			conn = DbConnection.getConnection();

			pstmt = conn.prepareCall(SqlConstants.DeleteShipment);
			pstmt.registerOutParameter(1, Types.OTHER);
			pstmt.setInt(2, Integer.parseInt(sShipmentId));

			int iReturn = pstmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
			sReturn = Constants.ERROR + ": " + e.getLocalizedMessage();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return sReturn;
	}

	public String endTrip(String sShipmentId, String sTime) {
		Connection conn = null;
		CallableStatement pstmt = null;
		ResultSet rs = null;
		String sReturn = Constants.SUCCESS;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			conn = DbConnection.getConnection();
			pstmt = conn.prepareCall(SqlConstants.EndTrip);
			pstmt.registerOutParameter(1, Types.OTHER);
			pstmt.setInt(2, Integer.parseInt(sShipmentId));
			df.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
			sdf.setTimeZone(java.util.TimeZone.getTimeZone("UTC"));
			Date dateObj = sdf.parse(sTime);
			pstmt.setTimestamp(3,
					java.sql.Timestamp.valueOf(df.format(dateObj)));

			int iReturn = pstmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
			sReturn = Constants.ERROR + ": " + e.getLocalizedMessage();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return sReturn;
	}

	public String userAssignment(String sShipmentId, String sUsers) {
		Connection conn = null;
		CallableStatement pstmt = null;
		ResultSet rs = null;
		String sReturn = Constants.SUCCESS;
		try {
			conn = DbConnection.getConnection();
			pstmt = conn.prepareCall(SqlConstants.UserAssignment);
			pstmt.registerOutParameter(1, Types.OTHER);
			pstmt.setInt(2, Integer.parseInt(sShipmentId));
			pstmt.setString(3, sUsers.substring(1));

			int iReturn = pstmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
			sReturn = Constants.ERROR + ": " + e.getLocalizedMessage();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return sReturn;
	}

	public String updateShipmentStep3Data(String sShipmentId, String sBoxId,
			String sTagId, String sProductId, String sValue, String sCriticality) {
		String callResult = Constants.SUCCESS;
		try {
			callResult = deleteBoxForShipment(sShipmentId);
			if(callResult.startsWith(Constants.ERROR)) return callResult;
			callResult = insertShipmentStep3Data(sShipmentId, sBoxId, sTagId, sProductId, sValue, sCriticality);
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
			callResult = Constants.ERROR + ": " + e.getLocalizedMessage();
		}
		return callResult;
	}
	
	public String insertShipmentStep3Data(String sShipmentId, String sBoxId,
			String sTagId, String sProductId, String sValue, String sCriticality) {
		String sReturn = Constants.SUCCESS;
		try {
			String[] box = sBoxId.split("#");
			String[] tag = sTagId.split("#");
			String[] product = sProductId.split("#");
			String[] value = sValue.split("#");
			String[] criticality = sCriticality.split("#");
			for (int i = 0; i < box.length; i++) {
				if (box[i] == null || box[i].equals("")) continue;
				insertBox(sShipmentId, box[i], tag[i], product[i], value[i], criticality[i]);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
			sReturn = Constants.ERROR + ": " + e.getLocalizedMessage();
		}
		return sReturn;
	}
	
	public String insertBox(String sShipmentId, String sBoxId,
			String sTagId, String sProductId, String sValue, String sCriticality) {
		Connection conn = null;
		CallableStatement pstmt = null;
		ResultSet rs = null;
		Statement statement = null;
		String sReturn = Constants.SUCCESS;
		try {
			conn = DbConnection.getConnection();
				pstmt = conn.prepareCall(SqlConstants.InsertShipmentStep3Data);
				pstmt.registerOutParameter(1, Types.OTHER);
				pstmt.setInt(2, Integer.parseInt(sShipmentId));
				pstmt.setString(3, sBoxId);
				pstmt.setString(4, sTagId);
				pstmt.setString(5, sProductId);
				pstmt.setFloat(6, Float.parseFloat(sValue));
				pstmt.setString(7, sCriticality);
				int iReturn = pstmt.executeUpdate();
			
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
			sReturn = Constants.ERROR + ": " + e.getLocalizedMessage();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return sReturn;
	}

	public String deleteLeg(String sShipmentId, int sLegSeqNum) {
		Connection conn = null;
		CallableStatement pstmt = null;
		ResultSet rs = null;
		String sReturn = Constants.SUCCESS;
		try {
			conn = DbConnection.getConnection();

			pstmt = conn.prepareCall(SqlConstants.DeleteStep2);
			pstmt.registerOutParameter(1, Types.OTHER);
			pstmt.setInt(2, Integer.parseInt(sShipmentId));
			pstmt.setInt(3, sLegSeqNum);
			int iReturn = pstmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
			sReturn = Constants.ERROR + ": " + e.getLocalizedMessage();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return sReturn;
	}
	
	public String deleteBoxForShipment(String sShipmentId) {
		Connection conn = null;
		CallableStatement pstmt = null;
		ResultSet rs = null;
		String sReturn = Constants.SUCCESS;
		try {
			conn = DbConnection.getConnection();

			pstmt = conn.prepareCall(SqlConstants.DeleteStep3);
			pstmt.registerOutParameter(1, Types.OTHER);
			pstmt.setInt(2, Integer.parseInt(sShipmentId));

			int iReturn = pstmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
			sReturn = Constants.ERROR + ": " + e.getLocalizedMessage();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return sReturn;
	}
	
	public Collection<TemperatureBean> getTemperatureDataForShipment(String sShipmentId, String sBoxId) {
		Connection conn = null;
		CallableStatement pstmt = null;
		ResultSet rs = null;
		TemperatureBean temperatureBean = new TemperatureBean();
		Collection<TemperatureBean> col = new Vector<TemperatureBean>();
		try {
			conn = DbConnection.getConnection();
			conn.setAutoCommit(false);
			pstmt = conn.prepareCall(SqlConstants.GetTemperatureDataForShipment);
			pstmt.registerOutParameter(1, Types.OTHER);
			pstmt.setInt(2, Integer.parseInt(sShipmentId));
			pstmt.execute();
			rs = (ResultSet) pstmt.getObject(1);
			
			while (rs.next()) {
				if(rs.getString(2) != null && ((sBoxId.equals("All")) || rs.getString(2).equals(sBoxId))) {
					temperatureBean = new TemperatureBean();
					if(rs.getString(1) == null || rs.getString(1).equals("")) {
						temperatureBean.setZone_Id("");
					} else {
						temperatureBean.setZone_Id(rs.getString(1));//This is the shipment Id
					}
					if(rs.getString(2) == null || rs.getString(2).equals("")) {
						temperatureBean.setNode_Id("");
					} else {
						temperatureBean.setNode_Id(rs.getString(2));//This is the BoxId
					}
					if(rs.getString(3) == null || rs.getString(3).equals("")) {
						temperatureBean.setTimestamp("");
					} else {
						temperatureBean.setTimestamp(rs.getString(3));
					}
					if(rs.getString(4) == null || rs.getString(4).equals("")) {
						temperatureBean.setTemperature("");
					} else {
						temperatureBean.setTemperature(rs.getString(4));
					}
				
				col.add(temperatureBean);
				}
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return col;
	}
	
	public Collection<ShipmentBoxInfo> selectShipmentDetailsForETag(String sShipmentId) {
		Connection conn = null;
		CallableStatement pstmt = null;
		ResultSet rs = null;
		ShipmentBoxInfo sb = new ShipmentBoxInfo();
		Collection<ShipmentBoxInfo> col = new Vector<ShipmentBoxInfo>();
		try {
			conn = DbConnection.getConnection();
			conn.setAutoCommit(false);
			pstmt = conn.prepareCall(SqlConstants.GetShipmentDetailForETag);
			pstmt.registerOutParameter(1, Types.OTHER);
			pstmt.setInt(2, Integer.parseInt(sShipmentId));
			pstmt.execute();
			rs = (ResultSet) pstmt.getObject(1);
			
			while (rs.next()) {
				if(rs.getInt(1) != Integer.parseInt(sShipmentId)) continue;
				sb = new ShipmentBoxInfo();
				sb.setShipmentId(rs.getInt(1));
				if(rs.getString(2) == null || rs.getString(2).equals("")) {
					sb.setBoxId("");
				} else {
					sb.setBoxId(rs.getString(2));
				}
				if(rs.getString(3) == null || rs.getString(3).equals("")) {
					sb.setTagId("");
				} else {
					sb.setTagId(rs.getString(3));
				}
				if(rs.getString(4) == null || rs.getString(4).equals("")) {
					sb.setAssignTimestamp("");
				} else {
					sb.setAssignTimestamp(rs.getString(4));
				}
				if(rs.getString(5) == null || rs.getString(5).equals("")) {
					sb.setProductId("");
				} else {
					sb.setProductId(rs.getString(5));
				}
				sb.setValue(rs.getFloat(6));
				if(rs.getString(7) == null || rs.getString(7).equals("")) {
					sb.setCriticality("");
				} else {
					sb.setCriticality(rs.getString(7));
				}
				col.add(sb);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return col;
	}
	
	public Collection<ShipmentBoxInfo> selectShipmentBoxDetails(String sShipmentId) {
		Connection conn = null;
		CallableStatement pstmt = null;
		ResultSet rs = null;
		ShipmentBoxInfo sb = new ShipmentBoxInfo();
		Collection<ShipmentBoxInfo> col = new Vector<ShipmentBoxInfo>();
		try {
			conn = DbConnection.getConnection();
			conn.setAutoCommit(false);
			pstmt = conn.prepareCall(SqlConstants.GetShipmentBoxDetails);
			pstmt.registerOutParameter(1, Types.OTHER);
			pstmt.setInt(2, Integer.parseInt(sShipmentId));
			pstmt.execute();
			rs = (ResultSet) pstmt.getObject(1);
			
			while (rs.next()) {
				sb = new ShipmentBoxInfo();
				sb.setShipmentId(rs.getInt(1));
				if(rs.getString(2) == null || rs.getString(2).equals("")) {
					sb.setBoxId("");
				} else {
					sb.setBoxId(rs.getString(2));
				}
				if(rs.getString(3) == null || rs.getString(3).equals("")) {
					sb.setTagId("");
				} else {
					sb.setTagId(rs.getString(3));
				}
				if(rs.getString(5) == null || rs.getString(5).equals("")) {
					sb.setProductId("");
				} else {
					sb.setProductId(rs.getString(5));
				}
				if(rs.getString(6) == null || rs.getString(6).equals("")) {
					sb.setValue(0);
				} else {
					sb.setValue(rs.getFloat(6));
				}
				if(rs.getString(7) == null || rs.getString(7).equals("")) {
					sb.setCriticality("");
				} else {
					sb.setCriticality(rs.getString(7));
				}
				col.add(sb);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return col;
	}
	
	public Collection<ShipmentTransitInfo> selectShipmentTransitDetails(String sShipmentId) {
		Connection conn = null;
		CallableStatement pstmt = null;
		ResultSet rs = null;
		ShipmentTransitInfo sb = new ShipmentTransitInfo();
		Collection<ShipmentTransitInfo> col = new Vector<ShipmentTransitInfo>();
		try {
			conn = DbConnection.getConnection();
			conn.setAutoCommit(false);
			pstmt = conn.prepareCall(SqlConstants.GetShipmentTransitDetails);
			pstmt.registerOutParameter(1, Types.OTHER);
			pstmt.setInt(2, Integer.parseInt(sShipmentId));
			pstmt.execute();
			rs = (ResultSet) pstmt.getObject(1);
			while (rs.next()) {
				sb = new ShipmentTransitInfo();
				sb.setShipmentId(rs.getInt(1));
				sb.setLegId(rs.getInt(2));
				if(rs.getString(3) == null || rs.getString(3).equals("")) {
					sb.setOrigin("");
				} else {
					sb.setOrigin(rs.getString(3));
				}
				if(rs.getString(4) == null || rs.getString(4).equals("")) {
					sb.setDestination("");
				} else {
					sb.setDestination(rs.getString(4));
				}
				if(rs.getString(5) == null || rs.getString(5).equals("")) {
					sb.setETD("");
				} else {
					sb.setETD(rs.getString(5));
				}
				if(rs.getString(6) == null || rs.getString(6).equals("")) {
					sb.setETA("");
				} else {
					sb.setETA(rs.getString(6));
				}
				if(rs.getString(11) == null || rs.getString(11).equals("")) {
					sb.setCarrier("");
				} else {
					sb.setCarrier(rs.getString(11));
				}
				if(rs.getString(12) == null || rs.getString(12).equals("")) {
					sb.setMode("");
				} else {
					sb.setMode(rs.getString(12));
				}
				col.add(sb);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return col;
	}
}
