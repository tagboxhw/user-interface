package com.tagbox.rest.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.TreeMap;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tagbox.rest.bean.AlertWorkflowBean;
import com.tagbox.rest.bean.TemperatureBean;
import com.tagbox.rest.bean.TodaysAlertWorkflowBean;
import com.tagbox.rest.bean.TransitVehicleMapBean;
import com.tagbox.rest.util.Constants;
import com.tagbox.rest.util.DbConnection;
import com.tagbox.rest.util.SqlConstants;
import com.tagbox.rest.util.Utils;

public class AlertsWorkflowDao {
	static final Logger logger = LoggerFactory
			.getLogger(AlertsWorkflowDao.class);
	String[] sAlerts = null;

	
	//REST Service to retrieve today's alerts
			public Collection<TodaysAlertWorkflowBean> getTodaysAlerts() {
				Connection conn = null;
				CallableStatement pstmt = null;
				ResultSet rs = null;
				Statement statement = null;
				TodaysAlertWorkflowBean todaysAlertWorkflowBean = new TodaysAlertWorkflowBean();
				Collection<TodaysAlertWorkflowBean> col = new Vector<TodaysAlertWorkflowBean>();
				try {
					conn = DbConnection.getConnection();
					conn.setAutoCommit(false);
					// Create and execute a SELECT SQL statement.
					// Create and execute a SELECT SQL statement.
					try {
						pstmt = conn.prepareCall(SqlConstants.GetTodayAlerts);
						pstmt.registerOutParameter(1, Types.OTHER);
					} catch (Exception e) {
						e.printStackTrace();
					}
					pstmt.execute();
					rs = (ResultSet) pstmt.getObject(1);
						while (rs.next()) {
							todaysAlertWorkflowBean = new TodaysAlertWorkflowBean();
							if(rs.getString(1) == null || rs.getString(1).equals("")) {
								todaysAlertWorkflowBean.setClassType("");
							} else {
								todaysAlertWorkflowBean.setClassType(rs.getString(1));
							}
							if(rs.getString(2) == null || rs.getString(2).equals("")) {
								todaysAlertWorkflowBean.setVehicle_ID("");
							} else {
								todaysAlertWorkflowBean.setVehicle_ID(rs.getString(2));
							}
							if(rs.getString(3) == null || rs.getString(3).equals("")) {
								todaysAlertWorkflowBean.setRoute_Type("");
							} else {
								todaysAlertWorkflowBean.setRoute_Type(rs.getString(3));
							}
							if(rs.getString(4) == null || rs.getString(4).equals("")) {
								todaysAlertWorkflowBean.setZone_ID("");
							} else {
								todaysAlertWorkflowBean.setZone_ID(rs.getString(4));
							}
							if(rs.getString(5) == null || rs.getString(5).equals("")) {
								todaysAlertWorkflowBean.setLocation_ID("");
							} else {
								todaysAlertWorkflowBean.setLocation_ID(rs.getString(5));
							}
							if(rs.getString(6) == null || rs.getString(6).equals("")) {
								todaysAlertWorkflowBean.setAlert_Type("");
							} else {
								todaysAlertWorkflowBean.setAlert_Type(rs.getString(6));
							}
							if(rs.getString(7) == null || rs.getString(7).equals("")) {
								todaysAlertWorkflowBean.setLocation_Name("");
							} else {
								todaysAlertWorkflowBean.setLocation_Name(rs.getString(7));
							}
							if(rs.getString(8) == null || rs.getString(8).equals("")) {
								todaysAlertWorkflowBean.setCity_Name("");
							} else {
								todaysAlertWorkflowBean.setCity_Name(rs.getString(8));
							}
							if(rs.getString(9) == null || rs.getString(9).equals("")) {
								todaysAlertWorkflowBean.setLocation_Type("");
							} else {
								todaysAlertWorkflowBean.setLocation_Type(rs.getString(9));
							}
							todaysAlertWorkflowBean.setCount_Of_Alerts(rs.getInt(10));
							if(rs.getString(11) == null || rs.getString(11).equals("")) {
								todaysAlertWorkflowBean.setAction_Status("");
							} else {
								todaysAlertWorkflowBean.setAction_Status(rs.getString(11));
							}
							todaysAlertWorkflowBean.setCurrent_Alert(rs.getInt(12));
							col.add(todaysAlertWorkflowBean);
						}
				} catch (Exception e) {
					logger.error("Exception: " + e.getLocalizedMessage());
					e.printStackTrace();
				} finally {
					try {
						if (rs != null)
							rs.close();
						if (pstmt != null)
							pstmt.close();
						DbConnection.closeConnection();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				return col;
			}
	
			//REST Service to retrieve today's alerts comparison
			public Collection<TodaysAlertWorkflowBean> getAlertsComp() {
				Connection conn = null;
				CallableStatement pstmt = null;
				ResultSet rs = null;
				Statement statement = null;
				TodaysAlertWorkflowBean todaysAlertWorkflowBean = new TodaysAlertWorkflowBean();
				Collection<TodaysAlertWorkflowBean> col = new Vector<TodaysAlertWorkflowBean>();
				try {
					conn = DbConnection.getConnection();
					conn.setAutoCommit(false);
					// Create and execute a SELECT SQL statement.
					// Create and execute a SELECT SQL statement.
					try {
						pstmt = conn.prepareCall(SqlConstants.GetAlertComp);
						pstmt.registerOutParameter(1, Types.OTHER);
					} catch (Exception e) {
						e.printStackTrace();
					}
					pstmt.execute();
					rs = (ResultSet) pstmt.getObject(1);
						while (rs.next()) {
							todaysAlertWorkflowBean = new TodaysAlertWorkflowBean();
							if(rs.getString(1) == null || rs.getString(1).equals("")) {
								todaysAlertWorkflowBean.setClassType("");
							} else {
								todaysAlertWorkflowBean.setClassType(rs.getString(1));
							}
							if(rs.getString(2) == null || rs.getString(2).equals("")) {
								todaysAlertWorkflowBean.setVehicle_ID("");
							} else {
								todaysAlertWorkflowBean.setVehicle_ID(rs.getString(2));
							}
							if(rs.getString(3) == null || rs.getString(3).equals("")) {
								todaysAlertWorkflowBean.setRoute_Type("");
							} else {
								todaysAlertWorkflowBean.setRoute_Type(rs.getString(3));
							}
							if(rs.getString(4) == null || rs.getString(4).equals("")) {
								todaysAlertWorkflowBean.setZone_ID("");
							} else {
								todaysAlertWorkflowBean.setZone_ID(rs.getString(4));
							}
							if(rs.getString(5) == null || rs.getString(5).equals("")) {
								todaysAlertWorkflowBean.setLocation_ID("");
							} else {
								todaysAlertWorkflowBean.setLocation_ID(rs.getString(5));
							}
							if(rs.getString(6) == null || rs.getString(6).equals("")) {
								todaysAlertWorkflowBean.setAlert_Type("");
							} else {
								todaysAlertWorkflowBean.setAlert_Type(rs.getString(6));
							}
							if(rs.getString(7) == null || rs.getString(7).equals("")) {
								todaysAlertWorkflowBean.setLocation_Name("");
							} else {
								todaysAlertWorkflowBean.setLocation_Name(rs.getString(7));
							}
							if(rs.getString(8) == null || rs.getString(8).equals("")) {
								todaysAlertWorkflowBean.setCity_Name("");
							} else {
								todaysAlertWorkflowBean.setCity_Name(rs.getString(8));
							}
							if(rs.getString(9) == null || rs.getString(9).equals("")) {
								todaysAlertWorkflowBean.setLocation_Type("");
							} else {
								todaysAlertWorkflowBean.setLocation_Type(rs.getString(9));
							}
							todaysAlertWorkflowBean.setCount_Of_Alerts(rs.getInt(10));
							col.add(todaysAlertWorkflowBean);
						}
				} catch (Exception e) {
					logger.error("Exception: " + e.getLocalizedMessage());
					e.printStackTrace();
				} finally {
					try {
						if (rs != null)
							rs.close();
						if (pstmt != null)
							pstmt.close();
						DbConnection.closeConnection();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				return col;
			}
	
			//REST Service to retrieve today's alerts reports
			public Collection<AlertWorkflowBean> getAlertsReports() {
				Connection conn = null;
				CallableStatement pstmt = null;
				ResultSet rs = null;
				Statement statement = null;
				AlertWorkflowBean alertWorkflowBean = new AlertWorkflowBean();
				Collection<AlertWorkflowBean> col = new Vector<AlertWorkflowBean>();
				try {
					conn = DbConnection.getConnection();
					conn.setAutoCommit(false);
					// Create and execute a SELECT SQL statement.
					// Create and execute a SELECT SQL statement.
					try {
						pstmt = conn.prepareCall(SqlConstants.GetAlertReport);
						pstmt.registerOutParameter(1, Types.OTHER);
					} catch (Exception e) {
						e.printStackTrace();
					}
					pstmt.execute();
					rs = (ResultSet) pstmt.getObject(1);
						while (rs.next()) {
							alertWorkflowBean = new AlertWorkflowBean();
							if(rs.getString(1) == null || rs.getString(1).equals("")) {
								alertWorkflowBean.setGW_Client_ID("");
							} else {
								alertWorkflowBean.setGW_Client_ID(rs.getString(1));
							}
							if(rs.getString(2) == null || rs.getString(2).equals("")) {
								alertWorkflowBean.setND_Client_ID("");
							} else {
								alertWorkflowBean.setND_Client_ID(rs.getString(2));
							}
							if(rs.getString(3) == null || rs.getString(3).equals("")) {
								alertWorkflowBean.setAlert_Timestamp_From("");
							} else {
								alertWorkflowBean.setAlert_Timestamp_From(rs.getString(3));
							}
							if(rs.getString(4) == null || rs.getString(4).equals("")) {
								alertWorkflowBean.setAlert_Type("");
							} else {
								alertWorkflowBean.setAlert_Type(rs.getString(4));
							}
							if(rs.getString(5) == null || rs.getString(5).equals("")) {
								alertWorkflowBean.setAlert_Duration("");
							} else {
								alertWorkflowBean.setAlert_Duration(rs.getString(5));
							}
							if(rs.getString(6) == null || rs.getString(6).equals("")) {
								alertWorkflowBean.setBreach_Type("");
							} else {
								alertWorkflowBean.setBreach_Type(rs.getString(6));
							}
							if(rs.getString(7) == null || rs.getString(7).equals("")) {
								alertWorkflowBean.setAssigned_To("");
							} else {
								alertWorkflowBean.setAssigned_To(rs.getString(7));
							}
							if(rs.getString(8) == null || rs.getString(8).equals("")) {
								alertWorkflowBean.setAction_Status("");
							} else {
								alertWorkflowBean.setAction_Status(rs.getString(8));
							}
							col.add(alertWorkflowBean);
						}
				} catch (Exception e) {
					logger.error("Exception: " + e.getLocalizedMessage());
					e.printStackTrace();
				} finally {
					try {
						if (rs != null)
							rs.close();
						if (pstmt != null)
							pstmt.close();
						DbConnection.closeConnection();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				return col;
			}
	
			public Collection<TemperatureBean> getHistoricalTemperatureData(String sStartTime, String sEndTime, String sSubZoneId) {
				Connection conn = null;
				CallableStatement pstmt = null;
				ResultSet rs = null;
				Statement statement = null;
				TemperatureBean temperatureBean = new TemperatureBean();
				Collection<TemperatureBean> col = new Vector<TemperatureBean>();
				try {
					conn = DbConnection.getConnection();
					conn.setAutoCommit(false);
					try {
						pstmt = conn.prepareCall(SqlConstants.GetHistoricalTempData);
						pstmt.registerOutParameter(1, Types.OTHER);
						pstmt.setTimestamp(2, java.sql.Timestamp.valueOf(sStartTime));
						pstmt.setTimestamp(3, java.sql.Timestamp.valueOf(sEndTime));
						pstmt.setString(4, sSubZoneId);
					} catch (Exception e) {
						e.printStackTrace();
					}
					pstmt.execute();
					rs = (ResultSet) pstmt.getObject(1);

						while (rs.next()) {
							
							temperatureBean = new TemperatureBean();
							if(rs.getString(1) == null || rs.getString(1).equals("")) {
								temperatureBean.setNode_Id("");
							} else {
								temperatureBean.setNode_Id(rs.getString(1));
							}
							if(rs.getString(2) == null || rs.getString(2).equals("")) {
								temperatureBean.setTemperature("");
							} else {
								temperatureBean.setTemperature(rs.getString(2));
							}
							if(rs.getString(3) == null || rs.getString(3).equals("")) {
								temperatureBean.setTimestamp("");
							} else {
								temperatureBean.setTimestamp(rs.getString(3));
							}
							col.add(temperatureBean);
						}
				} catch (Exception e) {
					logger.error("Exception: " + e.getLocalizedMessage());
					e.printStackTrace();
				} finally {
					try {
						if (rs != null)
							rs.close();
						if (pstmt != null)
							pstmt.close();
						DbConnection.closeConnection();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				return col;
			}
			
	
			
	
	
	
	public TreeMap<String, Integer> getRedAlertsForLocations(String sAlertType,
			String sTime) {
		Connection conn = null;
		ResultSet rs = null;
		TreeMap<String, Integer> tmMap = new TreeMap<String, Integer>();
		PreparedStatement pSelect = null;
		String sKey = "";
		try {
			conn = DbConnection.getConnection();
			double fTime = Utils.getAlertTime(sTime);
			logger.info("AlertFilter: " + sAlertType + " Time: " + fTime);
			if (sAlertType.equals("ALL")) {
				pSelect = conn
						.prepareStatement(SqlConstants.GetRedAlertsForLocations_All);
				pSelect.setDouble(1, fTime);
			} else {
				pSelect = conn
						.prepareStatement(SqlConstants.GetRedAlertsForLocations);
				sAlerts = sAlertType.split(",");
				sAlertType = "";
				int i = 0;
				for (int in = 0; in < sAlerts.length; in++) {
					i++;
					pSelect.setString(i, sAlerts[in]);
				}
				i++;
				while (i <= 6) {
					pSelect.setString(i, "NULL");
					i++;
				}
				pSelect.setDouble(7, fTime);
			}
			try {
				rs = pSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}
			int total = 0;
			while (rs.next()) {
				logger.info("getRedAlertsForLocations(): " + rs.getString(1)
						+ " " + rs.getString(2) + " " + rs.getInt(3));
				sKey = rs.getString(1);
				if (tmMap.containsKey(sKey)) {
					logger.info("3: " + rs.getInt(3));
					total = rs.getInt(3) + (Integer) tmMap.get(sKey);
					tmMap.put(sKey, total);
					logger.info("5: " + total);
				} else {
					logger.info("4: " + rs.getInt(3));
					tmMap.put(sKey, rs.getInt(3));
				}
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return tmMap;
	}

	public TreeMap<String, Integer> getGreenAlertsForLocations(
			String sAlertType, String sTime) {
		Connection conn = null;
		ResultSet rs = null;
		TreeMap<String, Integer> tmMap = new TreeMap<String, Integer>();
		PreparedStatement pSelect = null;
		String sKey = "";
		try {
			conn = DbConnection.getConnection();
			double fTime = Utils.getAlertTime(sTime);
			if (sAlertType.equals("ALL")) {
				pSelect = conn
						.prepareStatement(SqlConstants.GetGreenAlertsForLocations_All);
				pSelect.setDouble(1, fTime);
			} else {
				pSelect = conn
						.prepareStatement(SqlConstants.GetGreenAlertsForLocations);
				sAlerts = sAlertType.split(",");
				sAlertType = "";
				int i = 0;
				for (int in = 0; in < sAlerts.length; in++) {
					i++;
					pSelect.setString(i, sAlerts[in]);
				}
				i++;
				while (i <= 6) {
					pSelect.setString(i, "NULL");
					i++;
				}
				pSelect.setDouble(7, fTime);
			}
			try {
				rs = pSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}
			int total = 0;
			while (rs.next()) {
				sKey = rs.getString(1);
				if (tmMap.containsKey(sKey)) {
					total = rs.getInt(3) + (Integer) tmMap.get(sKey);
					tmMap.put(sKey, total);
				} else {
					tmMap.put(sKey, rs.getInt(3));
				}
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return tmMap;
	}

	public TreeMap<String, Comparable> getZoneColor(String sAlertType,
			String sTime) {
		Connection conn = null;
		ResultSet rs = null;
		TreeMap<String, Comparable> tmMap = new TreeMap<String, Comparable>();
		PreparedStatement pSelect = null;
		String sKey = "";
		try {
			conn = DbConnection.getConnection();
			double fTime = Utils.getAlertTime(sTime);
			logger.info("AlertFilter: " + sAlertType + " Time: " + fTime);
			if (sAlertType.equals("ALL")) {
				pSelect = conn.prepareStatement(SqlConstants.GetZoneColor_All);
				pSelect.setDouble(1, fTime);
			} else {
				pSelect = conn.prepareStatement(SqlConstants.GetZoneColor);
				sAlerts = sAlertType.split(",");
				sAlertType = "";
				int i = 0;
				for (int in = 0; in < sAlerts.length; in++) {
					i++;
					pSelect.setString(i, sAlerts[in]);
				}
				i++;
				while (i <= 6) {
					pSelect.setString(i, "NULL");
					i++;
				}
				pSelect.setDouble(7, fTime);
			}
			try {
				rs = pSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}
			int total = 0;
			while (rs.next()) {
				logger.info("zoneId: " + rs.getString(1) + " loc_type: "
						+ rs.getString(2) + " loc_id: " + rs.getString(3)
						+ " color: " + rs.getString(4) + " alerts: "
						+ rs.getInt(5));
				if (rs.getString(1) != null) {
					sKey = rs.getString(1);
					tmMap.put(sKey, rs.getString(4));
				}
				if (rs.getString(2) != null) {
					sKey = rs.getString(2).toUpperCase() + rs.getString(4);
					if (tmMap.containsKey(sKey)) {
						total = 1 + (Integer) tmMap.get(sKey);
						tmMap.put(sKey, total);
					} else {

						tmMap.put(sKey, 1);
					}
				}
				if (rs.getString(3) != null) {
					sKey = rs.getString(3);
					if (tmMap.containsKey(sKey)) {
						total = rs.getInt(5) + (Integer) tmMap.get(sKey);
						tmMap.put(sKey, total);
					} else {
						tmMap.put(sKey, rs.getInt(5));
					}
					sKey = rs.getString(3) + rs.getString(4);
					if (tmMap.containsKey(sKey)) {
						total = 1 + (Integer) tmMap.get(sKey);
						tmMap.put(sKey, total);
					} else {

						tmMap.put(sKey, 1);
					}

				}
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return tmMap;
	}

	public TreeMap<String, String> getCurrentAlerts() {
		Connection conn = null;
		ResultSet rs = null;
		Statement statement = null;
		TreeMap<String, String> tmMap = new TreeMap<String, String>();
		String sKey = "";
		try {
			conn = DbConnection.getConnection();

			try {
				statement = conn.createStatement();

				rs = statement.executeQuery(SqlConstants.GetCurrentAlerts);
			} catch (Exception e) {
				e.printStackTrace();
			}
			while (rs.next()) {
				sKey = rs.getString(1);
				tmMap.put(sKey, sKey);
			}

		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return tmMap;
	}

	//Not using this method anymore
	public TreeMap<String, Integer> getRedAlertsForTransit(String sAlertType,
			String sTime) {
		Connection conn = null;
		ResultSet rs = null;
		TreeMap<String, Integer> tmMap = new TreeMap<String, Integer>();
		PreparedStatement pSelect = null;
		String sKey = "";
		try {
			conn = DbConnection.getConnection();
			double fTime = Utils.getAlertTime(sTime);
			logger.info("AlertFilter: " + sAlertType + " Time: " + fTime);
			if (sAlertType.equals("ALL")) {
				// There is no route_type in transit_gateway_map. Should that be
				// transit_vehicle_map. If yes, then there is no client_id in
				// transit_vehicle_map
				pSelect = conn
						.prepareStatement(SqlConstants.GetRedAlertsForTransit_All);
				pSelect.setDouble(1, fTime);
			} else {
				pSelect = conn
						.prepareStatement(SqlConstants.GetRedAlertsForTransit);
				sAlerts = sAlertType.split(",");
				sAlertType = "";
				int i = 0;
				for (int in = 0; in < sAlerts.length; in++) {
					i++;
					pSelect.setString(i, sAlerts[in]);
				}
				i++;
				while (i <= 6) {
					pSelect.setString(i, "NULL");
					i++;
				}
				pSelect.setDouble(7, fTime);
			}
			try {
				rs = pSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}

			while (rs.next()) {
				logger.info(rs.getString(1) + " " + rs.getString(2));
				sKey = rs.getString(1);
				tmMap.put(sKey, rs.getInt(2));
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return tmMap;
	}

	public TreeMap<String, Integer> getAllAlertsForTransit(String sAlertType,
			String sTime) {
		Connection conn = null;
		ResultSet rs = null;
		TreeMap<String, Integer> tmMap = new TreeMap<String, Integer>();
		PreparedStatement pSelect = null;
		String sKey = "";
		try {
			conn = DbConnection.getConnection();
			double fTime = Utils.getAlertTime(sTime);
			logger.info("AlertFilter: " + sAlertType + " Time: " + fTime);
			if (sAlertType.equals("ALL")) {
				// There is no route_type in transit_gateway_map. Should that be
				// transit_vehicle_map. If yes, then there is no client_id in
				// transit_vehicle_map
				pSelect = conn
						.prepareStatement(SqlConstants.GetAllAlertsForTransit_All);
				pSelect.setDouble(1, fTime);
			} else {
				pSelect = conn
						.prepareStatement(SqlConstants.GetAllAlertsForTransit);
				sAlerts = sAlertType.split(",");
				sAlertType = "";
				pSelect.setDouble(1, fTime);
				int i = 1;
				for (int in = 0; in < sAlerts.length; in++) {
					i++;
					pSelect.setString(i, sAlerts[in]);
				}
				i++;
				while (i <= 7) {
					pSelect.setString(i, "NULL");
					i++;
				}
			}
			try {
				rs = pSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}
			int total = 0;

			while (rs.next()) {
				total = 0;
				logger.info(rs.getString(1) + " " + rs.getString(2) + " "
						+ rs.getString(3) + " " + rs.getString(4));
				sKey = rs.getString(2) + rs.getString(5);
				if (tmMap.containsKey(sKey)) {
					total = 1 + (Integer) tmMap.get(sKey);
					tmMap.put(sKey, total);
				} else {
					tmMap.put(sKey, 1);
				}
				total = 0;
				if (rs.getInt(6) > 0) {
					if (tmMap.containsKey(rs.getString(2))) {
						total = rs.getInt(6)
								+ (Integer) tmMap.get(rs.getString(2));
						tmMap.put(rs.getString(2), total);
					} else {
						tmMap.put(rs.getString(2), rs.getInt(6));
					}
				}
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return tmMap;
	}

	public TreeMap<String, Collection<TransitVehicleMapBean>> getLatLong(
			String sAlertType, String sTime) {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement pSelect = null;
		TreeMap<String, Collection<TransitVehicleMapBean>> tAll = new TreeMap<String, Collection<TransitVehicleMapBean>>();
		try {
			conn = DbConnection.getConnection();
			double fTime = Utils.getAlertTime(sTime);
			logger.info("AlertFilter: " + sAlertType + " Time: " + fTime);
			if (sAlertType.equals("ALL")) {
				// There is no route_type in transit_gateway_map. Should that be
				// transit_vehicle_map. If yes, then there is no client_id in
				// transit_vehicle_map
				pSelect = conn.prepareStatement(SqlConstants.GetLatLong_All);
				pSelect.setDouble(1, fTime);
			} else {
				pSelect = conn.prepareStatement(SqlConstants.GetLatLong);
				sAlerts = sAlertType.split(",");
				sAlertType = "";
				pSelect.setDouble(1, fTime);
				int i = 1;
				for (int in = 0; in < sAlerts.length; in++) {
					i++;
					pSelect.setString(i, sAlerts[in]);
				}
				i++;
				while (i <= 7) {
					pSelect.setString(i, "NULL");
					i++;
				}
			}
			try {
				rs = pSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}
			Collection<TransitVehicleMapBean> col1 = new Vector<TransitVehicleMapBean>();
			Collection<TransitVehicleMapBean> col2 = new Vector<TransitVehicleMapBean>();
			Collection<TransitVehicleMapBean> col3 = new Vector<TransitVehicleMapBean>();
			TransitVehicleMapBean tv = new TransitVehicleMapBean();
			while (rs.next()) {
				logger.info(rs.getString(1) + " " + rs.getString(2) + " "
						+ rs.getString(3) + " " + rs.getString(4) + " "
						+ rs.getString(5));
				tv = new TransitVehicleMapBean();
				tv.setVehicle_ID(rs.getString(1));
				tv.setRoute_Type(rs.getString(2));
				tv.setSource(rs.getString(3));
				tv.setDestination(rs.getString(4));
				tv.setVehicle_Status(rs.getString(5));
				if (rs.getString(2) == null)
					col1.add(tv);
				else if (rs.getString(2).equals(Constants.LONG_HAUL)) {
					col1.add(tv);
				} else if (rs.getString(2).equals(Constants.SHORT_HAUL)) {
					col2.add(tv);
				} else if (rs.getString(2).equals(Constants.LOCAL_RUN)) {
					col3.add(tv);
				}
			}
			tAll.put(Constants.LONG_HAUL, col1);
			tAll.put(Constants.SHORT_HAUL, col2);
			tAll.put(Constants.LOCAL_RUN, col3);
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return tAll;
	}

	public LinkedHashMap<String, AlertWorkflowBean> getAllAlertsForZoneId(
			String sZoneId, String sAlertType, String sTime) {
		Connection conn = null;
		ResultSet rs = null;
		LinkedHashMap<String, AlertWorkflowBean> tmMap = new LinkedHashMap<String, AlertWorkflowBean>();
		PreparedStatement pSelect = null;
		String sKey = "";
		try {
			conn = DbConnection.getConnection();
			double fTime = Utils.getAlertTime(sTime);
			if (sAlertType.equals("ALL")) {
				// There is no route_type in transit_gateway_map. Should that be
				// transit_vehicle_map. If yes, then there is no client_id in
				// transit_vehicle_map
				pSelect = conn
						.prepareStatement(SqlConstants.GetAllAlertsForZoneId_All);
				pSelect.setString(1, sZoneId);
				pSelect.setDouble(2, fTime);
			} else {
				pSelect = conn
						.prepareStatement(SqlConstants.GetAllAlertsForZoneId);
				pSelect.setString(1, sZoneId);
				sAlerts = sAlertType.split(",");
				sAlertType = "";
				int i = 1;
				for (int in = 0; in < sAlerts.length; in++) {
					i++;
					pSelect.setString(i, sAlerts[in]);
				}
				i++;
				while (i <= 7) {
					pSelect.setString(i, "NULL");
					i++;
				}
				pSelect.setDouble(8, fTime);
			}
			try {
				rs = pSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}

			while (rs.next()) {
				sKey = rs.getString(1);
				AlertWorkflowBean ab = new AlertWorkflowBean();
				ab.setAlert_ID(rs.getString(1));
				ab.setAlert_Location(rs.getString(2));
				ab.setAlert_Type(rs.getString(3));
				ab.setBreach_Type(rs.getString(4));
				ab.setAlert_Parameter_Avg(rs.getFloat(5));
				ab.setAlert_Timestamp_From(rs.getString(6));
				ab.setAlert_Timestamp_To(rs.getString(7));
				if (rs.getString(8) == null || rs.getString(8).equals("")) {
					ab.setAlert_Duration("");
				} else {
					ab.setAlert_Duration(rs.getString(8));
				}
				if (rs.getString(9) == null || rs.getString(9).equals("")) {
					ab.setAlert_Severity("");
				} else {
					ab.setAlert_Severity(rs.getString(9));
				}
				if (rs.getString(10) == null || rs.getString(10).equals("")) {
					ab.setCurrent_Location_Name("");
				} else {
					ab.setCurrent_Location_Name(rs.getString(10));
				}
				if (rs.getString(11) == null || rs.getString(11).equals("")) {
					ab.setAction_Status("Open");
				} else {
					ab.setAction_Status(rs.getString(11));
				}
				if (rs.getString(12) == null || rs.getString(12).equals("")) {
					ab.setRoot_Cause("");
				} else {
					ab.setRoot_Cause(rs.getString(12));
				}
				if (rs.getString(13) == null || rs.getString(13).equals("")) {
					ab.setPreventive_Actions("");
				} else {
					ab.setPreventive_Actions(rs.getString(13));
				}
				
				tmMap.put(sKey, ab);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return tmMap;
	}

	public LinkedHashMap<String, AlertWorkflowBean> getAllAlertsForVehicleId(
			String sVehicleId, String sAlertType, String sTime) {
		Connection conn = null;
		ResultSet rs = null;
		LinkedHashMap<String, AlertWorkflowBean> tmMap = new LinkedHashMap<String, AlertWorkflowBean>();
		PreparedStatement pSelect = null;
		String sKey = "";
		try {
			conn = DbConnection.getConnection();
			double fTime = Utils.getAlertTime(sTime);
			if (sAlertType.equals("ALL")) {
				// There is no route_type in transit_gateway_map. Should that be
				// transit_vehicle_map. If yes, then there is no client_id in
				// transit_vehicle_map
				pSelect = conn
						.prepareStatement(SqlConstants.GetAllAlertsForVehicleId_All);
				pSelect.setString(1, sVehicleId);
				pSelect.setDouble(2, fTime);
			} else {
				pSelect = conn
						.prepareStatement(SqlConstants.GetAllAlertsForVehicleId);
				pSelect.setString(1, sVehicleId);
				sAlerts = sAlertType.split(",");
				sAlertType = "";
				int i = 1;
				for (int in = 0; in < sAlerts.length; in++) {
					i++;
					pSelect.setString(i, sAlerts[in]);
				}
				i++;
				while (i <= 7) {
					pSelect.setString(i, "NULL");
					i++;
				}
				pSelect.setDouble(8, fTime);
			}
			try {
				rs = pSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}

			while (rs.next()) {
				logger.info(rs.getString(1) + " " + rs.getString(2));
				sKey = rs.getString(1);
				AlertWorkflowBean ab = new AlertWorkflowBean();
				ab.setAlert_ID(rs.getString(1));
				ab.setAlert_Location(rs.getString(2));
				ab.setAlert_Type(rs.getString(3));
				ab.setAlert_Timestamp_From(rs.getString(4));
				ab.setAlert_Timestamp_To(rs.getString(5));
				if (rs.getString(6) == null || rs.getString(6).equals("")) {
					ab.setAlert_Duration("");
				} else {
					ab.setAlert_Duration(rs.getString(6));
				}
				if (rs.getString(7) == null || rs.getString(7).equals("")) {
					ab.setAlert_Severity("");
				} else {
					ab.setAlert_Severity(rs.getString(7));
				}
				if (rs.getString(8) == null || rs.getString(8).equals("")) {
					ab.setCurrent_Location_Name("");
				} else {
					ab.setCurrent_Location_Name(rs.getString(8));
				}
				if (rs.getString(9) == null || rs.getString(9).equals("")) {
					ab.setAction_Status("Open");
				} else {
					ab.setAction_Status(rs.getString(9));
				}
				if (rs.getString(10) == null || rs.getString(10).equals("")) {
					ab.setRoot_Cause("");
				} else {
					ab.setRoot_Cause(rs.getString(10));
				}
				if (rs.getString(11) == null || rs.getString(11).equals("")) {
					ab.setPreventive_Actions("");
				} else {
					ab.setPreventive_Actions(rs.getString(11));
				}
				if (rs.getString(12) == null || rs.getString(12).equals("")) {
					ab.setRoute_Type("");
				} else {
					ab.setRoute_Type(rs.getString(12));
				}
				if (rs.getString(13) == null || rs.getString(13).equals("")) {
					ab.setBreach_Type("");
				} else {
					ab.setBreach_Type(rs.getString(13));
				}
				ab.setCurrent_Location_Lat(rs.getString(14));
				ab.setCurrent_Location_Long(rs.getString(15));
				tmMap.put(sKey, ab);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return tmMap;
	}

	public LinkedHashMap<String, AlertWorkflowBean> getAllAlertsForStationary(
			String sAlertType, String sTime) {
		Connection conn = null;
		ResultSet rs = null;
		LinkedHashMap<String, AlertWorkflowBean> tmMap = new LinkedHashMap<String, AlertWorkflowBean>();
		PreparedStatement pSelect = null;
		String sKey = "";
		try {
			conn = DbConnection.getConnection();
			double fTime = Utils.getAlertTime(sTime);
			if (sAlertType.equals("ALL")) {
				// There is no route_type in transit_gateway_map. Should that be
				// transit_vehicle_map. If yes, then there is no client_id in
				// transit_vehicle_map
				pSelect = conn
						.prepareStatement(SqlConstants.GetAllAlertsForStationary_All);
				pSelect.setDouble(1, fTime);
			} else {
				pSelect = conn
						.prepareStatement(SqlConstants.GetAllAlertsForStationary);
				sAlerts = sAlertType.split(",");
				sAlertType = "";
				int i = 0;
				for (int in = 0; in < sAlerts.length; in++) {
					i++;
					pSelect.setString(i, sAlerts[in]);
				}
				i++;
				while (i <= 6) {
					pSelect.setString(i, "NULL");
					i++;
				}
				pSelect.setDouble(7, fTime);
			}
			try {
				rs = pSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}

			while (rs.next()) {
				sKey = rs.getString(1);
				AlertWorkflowBean ab = new AlertWorkflowBean();
				ab.setAlert_ID(rs.getString(1));
				ab.setAlert_Location(rs.getString(2));
				ab.setAlert_Type(rs.getString(3));
				ab.setBreach_Type(rs.getString(4));
				ab.setAlert_Parameter_Avg(rs.getFloat(5));
				ab.setAlert_Timestamp_From(rs.getString(6));
				ab.setAlert_Timestamp_To(rs.getString(7));
				if (rs.getString(8) == null || rs.getString(8).equals("")) {
					ab.setAlert_Duration("");
				} else {
					ab.setAlert_Duration(rs.getString(8));
				}
				if (rs.getString(9) == null || rs.getString(9).equals("")) {
					ab.setAlert_Severity("");
				} else {
					ab.setAlert_Severity(rs.getString(9));
				}
				if (rs.getString(10) == null || rs.getString(10).equals("")) {
					ab.setCurrent_Location_Name("");
				} else {
					ab.setCurrent_Location_Name(rs.getString(10));
				}
				if (rs.getString(11) == null || rs.getString(11).equals("")) {
					ab.setAction_Status("Open");
				} else {
					ab.setAction_Status(rs.getString(11));
				}
				if (rs.getString(12) == null || rs.getString(12).equals("")) {
					ab.setRoot_Cause("");
				} else {
					ab.setRoot_Cause(rs.getString(12));
				}
				if (rs.getString(13) == null || rs.getString(13).equals("")) {
					ab.setPreventive_Actions("");
				} else {
					ab.setPreventive_Actions(rs.getString(13));
				}
				tmMap.put(sKey, ab);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return tmMap;
	}

	public LinkedHashMap<String, AlertWorkflowBean> allAlertsForTransit(
			String sAlertType, String sTime) {
		Connection conn = null;
		ResultSet rs = null;
		LinkedHashMap<String, AlertWorkflowBean> tmMap = new LinkedHashMap<String, AlertWorkflowBean>();
		PreparedStatement pSelect = null;
		String sKey = "";
		try {
			conn = DbConnection.getConnection();
			double fTime = Utils.getAlertTime(sTime);
			if (sAlertType.equals("ALL")) {
				// There is no route_type in transit_gateway_map. Should that be
				// transit_vehicle_map. If yes, then there is no client_id in
				// transit_vehicle_map
				pSelect = conn
						.prepareStatement(SqlConstants.AllAlertsForTransit_All);
				pSelect.setDouble(1, fTime);
			} else {
				pSelect = conn
						.prepareStatement(SqlConstants.AllAlertsForTransit);
				sAlerts = sAlertType.split(",");
				sAlertType = "";
				int i = 0;
				for (int in = 0; in < sAlerts.length; in++) {
					i++;
					pSelect.setString(i, sAlerts[in]);
				}
				i++;
				while (i <= 6) {
					pSelect.setString(i, "NULL");
					i++;
				}
				pSelect.setDouble(7, fTime);
			}
			try {
				rs = pSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}

			while (rs.next()) {
				sKey = rs.getString(1);
				AlertWorkflowBean ab = new AlertWorkflowBean();
				ab.setAlert_ID(rs.getString(1));
				ab.setAlert_Location(rs.getString(2));
				ab.setAlert_Type(rs.getString(3));
				ab.setAlert_Timestamp_From(rs.getString(4));
				ab.setAlert_Timestamp_To(rs.getString(5));
				if (rs.getString(6) == null || rs.getString(6).equals("")) {
					ab.setAlert_Duration("");
				} else {
					ab.setAlert_Duration(rs.getString(6));
				}
				if (rs.getString(7) == null || rs.getString(7).equals("")) {
					ab.setAlert_Severity("");
				} else {
					ab.setAlert_Severity(rs.getString(7));
				}
				if (rs.getString(7) == null || rs.getString(7).equals("")) {
					ab.setAlert_Severity("");
				} else {
					ab.setAlert_Severity(rs.getString(7));
				}
				if (rs.getString(8) == null || rs.getString(8).equals("")) {
					ab.setCurrent_Location_Name("");
				} else {
					ab.setCurrent_Location_Name(rs.getString(8));
				}
				if (rs.getString(9) == null || rs.getString(9).equals("")) {
					ab.setAction_Status("Open");
				} else {
					ab.setAction_Status(rs.getString(9));
				}
				if (rs.getString(10) == null || rs.getString(10).equals("")) {
					ab.setRoot_Cause("");
				} else {
					ab.setRoot_Cause(rs.getString(10));
				}
				if (rs.getString(11) == null || rs.getString(11).equals("")) {
					ab.setPreventive_Actions("");
				} else {
					ab.setPreventive_Actions(rs.getString(11));
				}
				if (rs.getString(12) == null || rs.getString(12).equals("")) {
					ab.setRoute_Type("");
				} else {
					ab.setRoute_Type(rs.getString(12));
				}
				
				if (rs.getString(13) == null || rs.getString(13).equals("")) {
					ab.setBreach_Type("");
				} else {
					ab.setBreach_Type(rs.getString(13));
				}
				tmMap.put(sKey, ab);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return tmMap;
	}

	//Not using this method anymore
	public TreeMap<String, Integer> getGreenAlertsForTransit(String sAlertType,
			String sTime) {
		Connection conn = null;
		ResultSet rs = null;
		TreeMap<String, Integer> tmMap = new TreeMap<String, Integer>();
		PreparedStatement pSelect = null;
		String sKey = "";
		try {
			conn = DbConnection.getConnection();
			double fTime = Utils.getAlertTime(sTime);
			logger.info("AlertFilter: " + sAlertType + " Time: " + fTime);
			if (sAlertType.equals("ALL")) {
				// There is no route_type in transit_gateway_map. Should that be
				// transit_vehicle_map. If yes, then there is no client_id in
				// transit_vehicle_map
				pSelect = conn
						.prepareStatement(SqlConstants.GetGreenAlertsForTransit_All);
				pSelect.setDouble(1, fTime);
			} else {
				pSelect = conn
						.prepareStatement(SqlConstants.GetGreenAlertsForTransit);
				sAlerts = sAlertType.split(",");
				sAlertType = "";
				int i = 0;
				for (int in = 0; in < sAlerts.length; in++) {
					i++;
					pSelect.setString(i, sAlerts[in]);
				}
				i++;
				while (i <= 6) {
					pSelect.setString(i, "NULL");
					i++;
				}
				pSelect.setDouble(7, fTime);
			}
			try {
				rs = pSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}

			while (rs.next()) {
				logger.info(rs.getString(1) + " " + rs.getString(2));
				sKey = rs.getString(1);
				tmMap.put(sKey, rs.getInt(2));
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return tmMap;
	}

	public void updateAction(String sAction, String sAlertId) {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement pSelect = null;
		try {
			conn = DbConnection.getConnection();
			logger.info("action: " + sAction + " id: " + sAlertId);
			// There is no route_type in transit_gateway_map. Should that be
			// transit_vehicle_map. If yes, then there is no client_id in
			// transit_vehicle_map
			pSelect = conn.prepareStatement(SqlConstants.UpdateAction);
			pSelect.setString(1, sAlertId);
			pSelect.setString(2, sAction);
			int iReturn = pSelect.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void updatePreventiveActionRootCause(String sRootCause, String sAssignedTo,
			String sPreventiveAction, String sAlertId) {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement pSelect = null;
		try {
			conn = DbConnection.getConnection();
			logger.info("root: " + sRootCause + " pre: " + sPreventiveAction
					+ " alert: " + sAlertId);
			// There is no route_type in transit_gateway_map. Should that be
			// transit_vehicle_map. If yes, then there is no client_id in
			// transit_vehicle_map
			pSelect = conn
					.prepareStatement(SqlConstants.UpdatePreventiveActionRootCause);
			pSelect.setString(1, sAssignedTo);
			pSelect.setString(2, sRootCause);
			pSelect.setString(3, sPreventiveAction);
			pSelect.setString(4, sAlertId);
			int iReturn = pSelect.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
