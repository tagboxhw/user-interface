package com.tagbox.rest.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.Collection;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tagbox.rest.bean.TripScorecardBean;
import com.tagbox.rest.util.DbConnection;
import com.tagbox.rest.util.SqlConstants;

public class TripScorecardDao {
	static final Logger logger = LoggerFactory
			.getLogger(TripScorecardDao.class);

	public Collection<TripScorecardBean> selectAllTripScorecardRecords() {
		Collection<TripScorecardBean> col = new Vector<TripScorecardBean>();
		Connection conn = null;
		CallableStatement pstmt = null;
		ResultSet rs = null;
		TripScorecardBean tvBean = new TripScorecardBean();
		Statement statement = null;
		try {
			conn = DbConnection.getConnection();
			conn.setAutoCommit(false);
			try {
				pstmt = conn.prepareCall(SqlConstants.GetTripScorecardData);
				pstmt.registerOutParameter(1, Types.OTHER);
			} catch (Exception e) {
				e.printStackTrace();
			}
			pstmt.execute();
			rs = (ResultSet) pstmt.getObject(1);
			while (rs.next()) {
				// logger.info(rs.getString(1) + " " + rs.getString(2) + " "
				// +rs.getString(3) + " " + rs.getString(4));
				tvBean = new TripScorecardBean();
				if(rs.getString(1) == null || rs.getString(1).equals("")) {
					tvBean.setShip_Id("");
				} else {
					tvBean.setShip_Id(rs.getString(1));
				}
				if(rs.getString(2) == null || rs.getString(2).equals("")) {
					tvBean.setOrigin("");
				} else {
					tvBean.setOrigin(rs.getString(2));
				}
				if(rs.getString(3) == null || rs.getString(3).equals("")) {
					tvBean.setDestination("");
				} else {
					tvBean.setDestination(rs.getString(3));
				}
				if(rs.getString(4) == null || rs.getString(4).equals("")) {
					tvBean.setDeparture_Date("");
				} else {
					tvBean.setDeparture_Date(rs.getString(4));
				}
				if(rs.getString(5) == null || rs.getString(5).equals("")) {
					tvBean.setArrival_Date("");
				} else {
					tvBean.setArrival_Date(rs.getString(5));
				}
				if(rs.getString(6) == null || rs.getString(6).equals("")) {
					tvBean.setStatus("");
				} else {
					tvBean.setStatus(rs.getString(6));
				}
				if(rs.getString(7) == null || rs.getString(7).equals("")) {
					tvBean.setBoxes("");
				} else {
					tvBean.setBoxes(rs.getString(7));
				}
				if(rs.getString(8) == null || rs.getString(8).equals("")) {
					tvBean.setValue_Inr(0);
				} else {
					tvBean.setValue_Inr(rs.getFloat(8));
				}
				col.add(tvBean);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return col;
	}
	
	
	public Collection<TripScorecardBean> selectLastSyncTimeForAllSubZones(String sShipmentId) {
		Collection<TripScorecardBean> col = new Vector<TripScorecardBean>();
		Connection conn = null;
		CallableStatement pstmt = null;
		ResultSet rs = null;
		TripScorecardBean tvBean = new TripScorecardBean();
		Statement statement = null;
		try {
			conn = DbConnection.getConnection();
			conn.setAutoCommit(false);
			try {
				pstmt = conn.prepareCall(SqlConstants.GetLastSyncTimeForSubzones);
				pstmt.registerOutParameter(1, Types.OTHER);
				pstmt.setInt(2, Integer.parseInt(sShipmentId));
			} catch (Exception e) {
				e.printStackTrace();
			}
			pstmt.execute();
			rs = (ResultSet) pstmt.getObject(1);
			// logger.info("start");
			while (rs.next()) {
				// System.out.println(rs.getString(1) + " " + rs.getString(2) + " "
				// +rs.getString(3) + " " + rs.getString(4));
				tvBean = new TripScorecardBean();
				if(rs.getString(1) == null || rs.getString(1).equals("")) {
					tvBean.setShip_Id("");
				} else {
					tvBean.setShip_Id(rs.getString(1));
				}
				if(rs.getString(2) == null || rs.getString(2).equals("")) {
					tvBean.setTag_Id("");
				} else {
					tvBean.setTag_Id(rs.getString(2));
				}
				if(rs.getString(4) == null || rs.getString(4).equals("")) {
					tvBean.setLastSyncTime("");
				} else {
					tvBean.setLastSyncTime(rs.getString(4));
				}
				//System.out.println(tvBean.toString());
				//System.out.println(tvBean.getTag_Id() + "," + tvBean.getLastSyncTime());
				col.add(tvBean);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return col;
	}
	
	
	public int updateVehicleDetails(String sVehicleId, String sSource,
			String sDestination, String sDriverId) {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement pSelect = null;
		String updateSql = "";
		int iReturn = 0;
		try {
			conn = DbConnection.getConnection();
			pSelect = conn.prepareStatement(SqlConstants.UpdateVehicleManagement);
			pSelect.setString(1, sSource);
			pSelect.setString(2, sDestination);
			pSelect.setString(3, sDriverId);
			pSelect.setString(4, sVehicleId);
			iReturn = pSelect.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return iReturn;
	}
	
}
