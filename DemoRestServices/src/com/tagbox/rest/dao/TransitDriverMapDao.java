package com.tagbox.rest.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tagbox.rest.bean.TransitDriverMapBean;
import com.tagbox.rest.util.DbConnection;
import com.tagbox.rest.util.SqlConstants;

public class TransitDriverMapDao {
	static final Logger logger = LoggerFactory.getLogger(TransitDriverMapDao.class);
	
	//REST Service to retrieve Driver details
		public Collection<TransitDriverMapBean> selectDriver() {
			Connection conn = null;
			PreparedStatement psSelect = null;
			ResultSet rs = null;
			Statement statement = null;
			TransitDriverMapBean locBean = new TransitDriverMapBean();
			Collection<TransitDriverMapBean> col = new Vector<TransitDriverMapBean>();
			try {
				conn = DbConnection.getConnection();

				// Create and execute a SELECT SQL statement.
				// Create and execute a SELECT SQL statement.
				try {
					statement = conn.createStatement();
				} catch (Exception e) {
					e.printStackTrace();
				}
				rs = statement.executeQuery(SqlConstants.SelectDrivers);
				while (rs.next()) {
					locBean = new TransitDriverMapBean();
					if(rs.getString(1) == null || rs.getString(1).equals("")) {
						locBean.setDriver_ID("");
					} else {
						locBean.setDriver_ID(rs.getString(1));
					}
					if(rs.getString(2) == null || rs.getString(2).equals("")) {
						locBean.setDriver_DL_ID("");
					} else {
						locBean.setDriver_DL_ID(rs.getString(2));
					}
					if(rs.getString(3) == null || rs.getString(3).equals("")) {
						locBean.setDriver_Name("");
					} else {
						locBean.setDriver_Name(rs.getString(3));
					}
					if(rs.getString(4) == null || rs.getString(4).equals("")) {
						locBean.setDriver_Phone_Num("");
					} else {
						locBean.setDriver_Phone_Num(rs.getString(4));
					}
					
					col.add(locBean);
				}
			} catch (Exception e) {
				logger.error("Exception: " + e.getLocalizedMessage());
				e.printStackTrace();
			} finally {
				try {
					if (rs != null)
						rs.close();
					if (psSelect != null)
						psSelect.close();
					DbConnection.closeConnection();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
			return col;
		}
}
