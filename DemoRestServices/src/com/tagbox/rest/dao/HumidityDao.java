package com.tagbox.rest.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tagbox.rest.bean.HumidityBean;
import com.tagbox.rest.bean.HumidityTriggersBean;
import com.tagbox.rest.bean.TemperatureTriggersBean;
import com.tagbox.rest.util.DbConnection;
import com.tagbox.rest.util.SqlConstants;

public class HumidityDao {
	static final Logger logger = LoggerFactory
			.getLogger(HumidityDao.class);
	
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	
	//REST Service to retrieve humidity data
	public Collection<HumidityBean> getHumidityData() {
		Connection conn = null;
		CallableStatement pstmt = null;
		ResultSet rs = null;
		Statement statement = null;
		HumidityBean humidityBean = new HumidityBean();
		Collection<HumidityBean> col = new Vector<HumidityBean>();
		try {
			conn = DbConnection.getConnection();
			conn.setAutoCommit(false);
			// Create and execute a SELECT SQL statement.
			// Create and execute a SELECT SQL statement.
			try {
				pstmt = conn.prepareCall(SqlConstants.GetHumidityData);
				pstmt.registerOutParameter(1, Types.OTHER);
			} catch (Exception e) {
				e.printStackTrace();
			}
			pstmt.execute();
			rs = (ResultSet) pstmt.getObject(1);
				while (rs.next()) {
					humidityBean = new HumidityBean();
					//if(rs.getString(1).equals("SZ-SY-DMO-000042")) System.out.println(rs.getString(1));
					if(rs.getString(1) == null || rs.getString(1).equals("")) {
						humidityBean.setNode_Id("");
					} else {
						humidityBean.setNode_Id(rs.getString(1));
					}
					if(rs.getString(2) == null || rs.getString(2).equals("")) {
						humidityBean.setTimestamp("");
					} else {
						humidityBean.setTimestamp(rs.getString(2));
					}
					if(rs.getString(3) == null || rs.getString(3).equals("")) {
						humidityBean.setHumidity("");
					} else {
						humidityBean.setHumidity(rs.getString(3));
					}
					if(rs.getString(4) == null || rs.getString(4).equals("")) {
						humidityBean.setZone_Id("");
					} else {
						humidityBean.setZone_Id(rs.getString(4));
					}
					col.add(humidityBean);
				}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return col;
	}
	
	public Collection<HumidityTriggersBean> getHumidityThresholds() {
		Connection conn = null;
		CallableStatement pstmt = null;
		ResultSet rs = null;
		Statement statement = null;
		HumidityTriggersBean humTriggersBean = new HumidityTriggersBean();
		Collection<HumidityTriggersBean> col = new Vector<HumidityTriggersBean>();
		try {
			conn = DbConnection.getConnection();
			conn.setAutoCommit(false);
			// Create and execute a SELECT SQL statement.
			// Create and execute a SELECT SQL statement.
			try {
				pstmt = conn.prepareCall(SqlConstants.GetHumidityThresholds);
				pstmt.registerOutParameter(1, Types.OTHER);
			} catch (Exception e) {
				e.printStackTrace();
			}
			pstmt.execute();
			rs = (ResultSet) pstmt.getObject(1);
			
				while (rs.next()) {
					humTriggersBean = new HumidityTriggersBean();
					if(rs.getString(1) == null || rs.getString(1).equals("")) {
						humTriggersBean.setND_client_id("");
					} else {
						humTriggersBean.setND_client_id(rs.getString(1));
					}
					if(rs.getString(2) == null || rs.getString(2).equals("")) {
						humTriggersBean.setThreshold_UL(0);
					} else {
						humTriggersBean.setThreshold_UL(rs.getInt(2));
					}
					if(rs.getString(3) == null || rs.getString(3).equals("")) {
						humTriggersBean.setThreshold_LL(0);
					} else {
						humTriggersBean.setThreshold_LL(rs.getInt(3));
					}
					if(rs.getString(4) == null || rs.getString(4).equals("")) {
						humTriggersBean.setGW_client_id("");
					} else {
						humTriggersBean.setGW_client_id(rs.getString(4));
					}
					col.add(humTriggersBean);
				}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return col;
	}
	
	//REST Service to retrieve average humidity data for each zone
		public Collection<HumidityBean> getHumidityAverageData() {
			Connection conn = null;
			CallableStatement pstmt = null;
			ResultSet rs = null;
			Statement statement = null;
			HumidityBean humidityBean = new HumidityBean();
			Collection<HumidityBean> col = new Vector<HumidityBean>();
			try {
				conn = DbConnection.getConnection();
				conn.setAutoCommit(false);
				// Create and execute a SELECT SQL statement.
				// Create and execute a SELECT SQL statement.
				try {
					pstmt = conn.prepareCall(SqlConstants.GetHumidityAverageData);
					pstmt.registerOutParameter(1, Types.OTHER);
				} catch (Exception e) {
					e.printStackTrace();
				}
				pstmt.execute();
				rs = (ResultSet) pstmt.getObject(1);
					while (rs.next()) {
						humidityBean = new HumidityBean();
						if(rs.getString(1) == null || rs.getString(1).equals("")) {
							humidityBean.setZone_Id("");
						} else {
							humidityBean.setZone_Id(rs.getString(1));
						}
						
						if(rs.getString(2) == null || rs.getString(2).equals("")) {
							humidityBean.setHumidity("");
						} else {
							humidityBean.setHumidity(rs.getString(2));
						}
						col.add(humidityBean);
					}
			} catch (Exception e) {
				logger.error("Exception: " + e.getLocalizedMessage());
				e.printStackTrace();
			} finally {
				try {
					if (rs != null)
						rs.close();
					if (pstmt != null)
						pstmt.close();
					DbConnection.closeConnection();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return col;
		}
		
		//REST Service to retrieve week humidity data
		public Collection<HumidityBean> getHumidityDataForWeek() {
			Connection conn = null;
			CallableStatement pstmt = null;
			ResultSet rs = null;
			Statement statement = null;
			HumidityBean humidityBean = new HumidityBean();
			Collection<HumidityBean> col = new Vector<HumidityBean>();
			try {
				conn = DbConnection.getConnection();
				conn.setAutoCommit(false);
				// Create and execute a SELECT SQL statement.
				// Create and execute a SELECT SQL statement.
				try {
					pstmt = conn.prepareCall(SqlConstants.GetHumidityDataWeek);
					pstmt.registerOutParameter(1, Types.OTHER);
				} catch (Exception e) {
					e.printStackTrace();
				}
				pstmt.execute();
				rs = (ResultSet) pstmt.getObject(1);
					while (rs.next()) {
						humidityBean = new HumidityBean();
						if(rs.getString(1) == null || rs.getString(1).equals("")) {
							humidityBean.setNode_Id("");
						} else {
							humidityBean.setNode_Id(rs.getString(1));
						}
						if(rs.getString(2) == null || rs.getString(2).equals("")) {
							humidityBean.setTimestamp("");
						} else {
							humidityBean.setTimestamp(rs.getString(2));
						}
						if(rs.getString(3) == null || rs.getString(3).equals("")) {
							humidityBean.setHumidity("");
						} else {
							humidityBean.setHumidity(rs.getString(3));
						}
						if(rs.getString(4) == null || rs.getString(4).equals("")) {
							humidityBean.setZone_Id("");
						} else {
							humidityBean.setZone_Id(rs.getString(4));
						}
						if(rs.getString(5) == null || rs.getString(5).equals("")) {
							humidityBean.setLocation_id("");
						} else {
							humidityBean.setLocation_id(rs.getString(5));
						}
						col.add(humidityBean);
					}
			} catch (Exception e) {
				logger.error("Exception: " + e.getLocalizedMessage());
				e.printStackTrace();
			} finally {
				try {
					if (rs != null)
						rs.close();
					if (pstmt != null)
						pstmt.close();
					DbConnection.closeConnection();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return col;
		}
		
		//REST Service to retrieve average humidity data for all sub zones under a zone Id. This is used when "All" is selected in the dropdown
		//in ParameterSummary page
		public Collection<HumidityBean> getHumidityDataForAllSubzones(String sStartTime, String sEndTime) {
			Connection conn = null;
			CallableStatement pstmt = null;
			ResultSet rs = null;
			Statement statement = null;
			HumidityBean humidityBean = new HumidityBean();
			Collection<HumidityBean> col = new Vector<HumidityBean>();
			try {
				conn = DbConnection.getConnection();
				conn.setAutoCommit(false);
				// Create and execute a SELECT SQL statement.
				// Create and execute a SELECT SQL statement.
				try {
					pstmt = conn.prepareCall(SqlConstants.GetZoneAverageHumidity);
					pstmt.registerOutParameter(1, Types.OTHER);
					Date dateObj = sdf.parse(sStartTime);
				    Timestamp tstamp = new Timestamp(dateObj.getTime());
					pstmt.setTimestamp(2, tstamp);
					dateObj = sdf.parse(sEndTime);
				    tstamp = new Timestamp(dateObj.getTime());
					pstmt.setTimestamp(3, tstamp);
				} catch (Exception e) {
					e.printStackTrace();
				}
				pstmt.execute();
				rs = (ResultSet) pstmt.getObject(1);
					while (rs.next()) {
						humidityBean = new HumidityBean();
						if(rs.getString(1) == null || rs.getString(1).equals("")) {
							humidityBean.setZone_Id("");
						} else {
							humidityBean.setZone_Id(rs.getString(1));
						}
						if(rs.getString(2) == null || rs.getString(2).equals("")) {
							humidityBean.setTimestamp("");
						} else {
							humidityBean.setTimestamp(rs.getString(2));
						}
						if(rs.getString(3) == null || rs.getString(3).equals("")) {
							humidityBean.setHumidity("");
						} else {
							humidityBean.setHumidity(rs.getString(3));
						}
						
						col.add(humidityBean);
					}
			} catch (Exception e) {
				logger.error("Exception: " + e.getLocalizedMessage());
				e.printStackTrace();
			} finally {
				try {
					if (rs != null)
						rs.close();
					if (pstmt != null)
						pstmt.close();
					DbConnection.closeConnection();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return col;
		}
}
