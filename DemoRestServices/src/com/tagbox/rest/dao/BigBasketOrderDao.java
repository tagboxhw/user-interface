package com.tagbox.rest.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Vector;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;






import com.tagbox.rest.util.Constants;
import com.tagbox.rest.util.DbConnection;
import com.tagbox.rest.util.SqlConstants;
import com.tagbox.rest.bean.OrderBean;

public class BigBasketOrderDao {
	
	private Client client; 
	static final Logger logger = LoggerFactory
			.getLogger(BigBasketOrderDao.class);
	
	private void init(){ 
	      this.client = ClientBuilder.newClient(); 
	   }  
	
	public void getAllOrders() {
		Collection<OrderBean> tvmBean = null;
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			init();
			GenericType<Collection<OrderBean>> tvmBean1 = new GenericType<Collection<OrderBean>>(){};
			tvmBean = client 
			         .target(Constants.BIG_BASKET_REST_SERVICE_URL + "OrderService/allorders") 
			         .request(MediaType.APPLICATION_XML) 
			         .get(tvmBean1); 
		} catch (Exception e) {
			e.printStackTrace();
		}
		df.setTimeZone(java.util.TimeZone.getTimeZone("UTC"));
		sdf.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
		tvmBean = new Vector();
		Connection conn = null;
		CallableStatement pstmt = null;
		ResultSet rs = null;
		String sReturn = Constants.SUCCESS;
		OrderBean ob1 = new OrderBean();
		ob1.setOrderId("1");
		ob1.setOrderItemId("1");
		ob1.setDC("DC 1");
		ob1.setHub("Hub 1");
		ob1.setBarCode("1");
		ob1.setSKU("1");
		ob1.setCategory("CFV");
		ob1.setStatus("Shipped");
		ob1.setStatusTimestamp("08-08-2017 10:00");
		tvmBean.add(ob1);
		ob1 = new OrderBean();
		ob1.setOrderId("2");
		ob1.setOrderItemId("2");
		ob1.setDC("DC 2");
		ob1.setHub("Hub 2");
		ob1.setBarCode("2");
		ob1.setSKU("2");
		ob1.setCategory("FV");
		ob1.setStatus("Delivered");
		ob1.setStatusTimestamp("10-10-2017 08:00");
		tvmBean.add(ob1);
		try {
			conn = DbConnection.getConnection();
			for(OrderBean ob: tvmBean) {
				try {
					pstmt = conn.prepareCall(SqlConstants.InsertBBOrder);
					pstmt.registerOutParameter(1, Types.OTHER);
					pstmt.setString(2, ob.getOrderId());
					pstmt.setString(3, ob.getOrderItemId());
					pstmt.setString(4, ob.getDC());
					pstmt.setString(5, ob.getHub());
					pstmt.setString(6, ob.getBarCode());
					pstmt.setString(7, ob.getSKU());
					pstmt.setString(8, ob.getCategory());
					pstmt.setString(9, ob.getStatus());
					System.out.println(ob.getStatusTimestamp());
					pstmt.setTimestamp(10, java.sql.Timestamp.valueOf(df.format(sdf.parse(ob.getStatusTimestamp()))));
				} catch (Exception e) {
					e.printStackTrace();
				}
				int iReturn = pstmt.executeUpdate();
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
			sReturn = Constants.ERROR + ": " + e.getLocalizedMessage();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return;
	}

}
