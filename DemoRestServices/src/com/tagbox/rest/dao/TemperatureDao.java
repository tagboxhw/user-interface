package com.tagbox.rest.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tagbox.rest.bean.HumidityBean;
import com.tagbox.rest.bean.TemperatureBean;
import com.tagbox.rest.bean.TemperatureTriggersBean;
import com.tagbox.rest.util.DbConnection;
import com.tagbox.rest.util.SqlConstants;

public class TemperatureDao {
	static final Logger logger = LoggerFactory
			.getLogger(TemperatureDao.class);
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	
	//REST Service to retrieve temperature data
	public Collection<TemperatureBean> getTemperatureData() {
		Connection conn = null;
		CallableStatement pstmt = null;
		ResultSet rs = null;
		Statement statement = null;
		TemperatureBean temperatureBean = new TemperatureBean();
		Collection<TemperatureBean> col = new Vector<TemperatureBean>();
		try {
			conn = DbConnection.getConnection();
			conn.setAutoCommit(false);
			// Create and execute a SELECT SQL statement.
			// Create and execute a SELECT SQL statement.
			try {
				pstmt = conn.prepareCall(SqlConstants.GetTemperatureData);
				pstmt.registerOutParameter(1, Types.OTHER);
			} catch (Exception e) {
				e.printStackTrace();
			}
			pstmt.execute();
			rs = (ResultSet) pstmt.getObject(1);
			
				while (rs.next()) {
					temperatureBean = new TemperatureBean();
					if(rs.getString(1) == null || rs.getString(1).equals("")) {
						temperatureBean.setNode_Id("");
					} else {
						temperatureBean.setNode_Id(rs.getString(1));
					}
					if(rs.getString(2) == null || rs.getString(2).equals("")) {
						temperatureBean.setTimestamp("");
					} else {
						temperatureBean.setTimestamp(rs.getString(2));
					}
					if(rs.getString(3) == null || rs.getString(3).equals("")) {
						temperatureBean.setTemperature("");
					} else {
						temperatureBean.setTemperature(rs.getString(3));
					}
					if(rs.getString(4) == null || rs.getString(4).equals("")) {
						temperatureBean.setZone_Id("");
					} else {
						temperatureBean.setZone_Id(rs.getString(4));
					}
					col.add(temperatureBean);
				}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return col;
	}
	
	public Collection<TemperatureTriggersBean> getTemperatureThresholds() {
		Connection conn = null;
		CallableStatement pstmt = null;
		ResultSet rs = null;
		Statement statement = null;
		TemperatureTriggersBean tempTriggersBean = new TemperatureTriggersBean();
		Collection<TemperatureTriggersBean> col = new Vector<TemperatureTriggersBean>();
		try {
			conn = DbConnection.getConnection();
			conn.setAutoCommit(false);
			// Create and execute a SELECT SQL statement.
			// Create and execute a SELECT SQL statement.
			try {
				pstmt = conn.prepareCall(SqlConstants.GetTemperatureThresholds);
				pstmt.registerOutParameter(1, Types.OTHER);
			} catch (Exception e) {
				e.printStackTrace();
			}
			pstmt.execute();
			rs = (ResultSet) pstmt.getObject(1);
			
				while (rs.next()) {
					tempTriggersBean = new TemperatureTriggersBean();
					if(rs.getString(1) == null || rs.getString(1).equals("")) {
						tempTriggersBean.setND_client_id("");
					} else {
						tempTriggersBean.setND_client_id(rs.getString(1));
					}
					if(rs.getString(2) == null || rs.getString(2).equals("")) {
						tempTriggersBean.setThreshold_UL(0);
					} else {
						tempTriggersBean.setThreshold_UL(rs.getInt(2));
					}
					if(rs.getString(3) == null || rs.getString(3).equals("")) {
						tempTriggersBean.setThreshold_LL(0);
					} else {
						tempTriggersBean.setThreshold_LL(rs.getInt(3));
					}
					if(rs.getString(4) == null || rs.getString(4).equals("")) {
						tempTriggersBean.setGW_client_id("");
					} else {
						tempTriggersBean.setGW_client_id(rs.getString(4));
					}
					col.add(tempTriggersBean);
				}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return col;
	}
	
	public Collection<TemperatureBean> getVibrationData() {
		Connection conn = null;
		CallableStatement pstmt = null;
		ResultSet rs = null;
		Statement statement = null;
		TemperatureBean temperatureBean = new TemperatureBean();
		Collection<TemperatureBean> col = new Vector<TemperatureBean>();
		try {
			conn = DbConnection.getConnection();
			conn.setAutoCommit(false);
			// Create and execute a SELECT SQL statement.
			// Create and execute a SELECT SQL statement.
			try {
				pstmt = conn.prepareCall(SqlConstants.GetVibrationData);
				pstmt.registerOutParameter(1, Types.OTHER);
			} catch (Exception e) {
				e.printStackTrace();
			}
			pstmt.execute();
			rs = (ResultSet) pstmt.getObject(1);
			
				while (rs.next()) {
					temperatureBean = new TemperatureBean();
					if(rs.getString(1) == null || rs.getString(1).equals("")) {
						temperatureBean.setNode_Id("");
					} else {
						temperatureBean.setNode_Id(rs.getString(1));
					}
					if(rs.getString(2) == null || rs.getString(2).equals("")) {
						temperatureBean.setTimestamp("");
					} else {
						temperatureBean.setTimestamp(rs.getString(2));
					}
					if(rs.getString(3) == null || rs.getString(3).equals("")) {
						temperatureBean.setTemperature("");
					} else {
						temperatureBean.setTemperature(rs.getString(3));
					}
					
					col.add(temperatureBean);
				}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return col;
	}
	
	//REST Service to retrieve average temperature data for each zone
		public Collection<TemperatureBean> getTemperatureAverageData() {
			Connection conn = null;
			CallableStatement pstmt = null;
			ResultSet rs = null;
			Statement statement = null;
			TemperatureBean temperatureBean = new TemperatureBean();
			Collection<TemperatureBean> col = new Vector<TemperatureBean>();
			try {
				conn = DbConnection.getConnection();
				conn.setAutoCommit(false);
				// Create and execute a SELECT SQL statement.
				// Create and execute a SELECT SQL statement.
				try {
					pstmt = conn.prepareCall(SqlConstants.GetTemperatureAverageData);
					pstmt.registerOutParameter(1, Types.OTHER);
				} catch (Exception e) {
					e.printStackTrace();
				}
				pstmt.execute();
				rs = (ResultSet) pstmt.getObject(1);
				
					while (rs.next()) {
						temperatureBean = new TemperatureBean();
						if(rs.getString(1) == null || rs.getString(1).equals("")) {
							temperatureBean.setZone_Id("");
						} else {
							temperatureBean.setZone_Id(rs.getString(1));
						}
						if(rs.getString(2) == null || rs.getString(2).equals("")) {
							temperatureBean.setTemperature("");
						} else {
							temperatureBean.setTemperature(rs.getString(2));
						}
						col.add(temperatureBean);
					}
			} catch (Exception e) {
				logger.error("Exception: " + e.getLocalizedMessage());
				e.printStackTrace();
			} finally {
				try {
					if (rs != null)
						rs.close();
					if (pstmt != null)
						pstmt.close();
					DbConnection.closeConnection();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return col;
		}
		
		//REST Service to retrieve week temperature data
		public Collection<TemperatureBean> getTemperatureDataForWeek() {
			Connection conn = null;
			CallableStatement pstmt = null;
			ResultSet rs = null;
			Statement statement = null;
			TemperatureBean temperatureBean = new TemperatureBean();
			Collection<TemperatureBean> col = new Vector<TemperatureBean>();
			try {
				conn = DbConnection.getConnection();
				conn.setAutoCommit(false);
				// Create and execute a SELECT SQL statement.
				// Create and execute a SELECT SQL statement.
				try {
					pstmt = conn.prepareCall(SqlConstants.GetTemperatureDataWeek);
					pstmt.registerOutParameter(1, Types.OTHER);
				} catch (Exception e) {
					e.printStackTrace();
				}
				pstmt.execute();
				rs = (ResultSet) pstmt.getObject(1);
				
					while (rs.next()) {
						temperatureBean = new TemperatureBean();
						if(rs.getString(1) == null || rs.getString(1).equals("")) {
							temperatureBean.setNode_Id("");
						} else {
							temperatureBean.setNode_Id(rs.getString(1));
						}
						if(rs.getString(2) == null || rs.getString(2).equals("")) {
							temperatureBean.setTimestamp("");
						} else {
							temperatureBean.setTimestamp(rs.getString(2));
						}
						if(rs.getString(3) == null || rs.getString(3).equals("")) {
							temperatureBean.setTemperature("");
						} else {
							temperatureBean.setTemperature(rs.getString(3));
						}
						if(rs.getString(4) == null || rs.getString(4).equals("")) {
							temperatureBean.setZone_Id("");
						} else {
							temperatureBean.setZone_Id(rs.getString(4));
						}
						if(rs.getString(5) == null || rs.getString(5).equals("")) {
							temperatureBean.setLocation_id("");
						} else {
							temperatureBean.setLocation_id(rs.getString(5));
						}
						col.add(temperatureBean);
					}
			} catch (Exception e) {
				logger.error("Exception: " + e.getLocalizedMessage());
				e.printStackTrace();
			} finally {
				try {
					if (rs != null)
						rs.close();
					if (pstmt != null)
						pstmt.close();
					DbConnection.closeConnection();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return col;
		}
		
		//REST Service to retrieve average temperature data for all sub zones under a zone Id. This is used when "All" is selected in the dropdown
		//in ParameterSummary page
				public Collection<TemperatureBean> getTemperatureDataForAllSubzones(String sStartTime, String sEndTime) {
					Connection conn = null;
					CallableStatement pstmt = null;
					ResultSet rs = null;
					Statement statement = null;
					TemperatureBean temperatureBean = new TemperatureBean();
					Collection<TemperatureBean> col = new Vector<TemperatureBean>();
					try {
						conn = DbConnection.getConnection();
						conn.setAutoCommit(false);
						// Create and execute a SELECT SQL statement.
						// Create and execute a SELECT SQL statement.
						try {
							pstmt = conn.prepareCall(SqlConstants.GetZoneAverageTemperature);
							pstmt.registerOutParameter(1, Types.OTHER);
							Date dateObj = sdf.parse(sStartTime);
						    Timestamp tstamp = new Timestamp(dateObj.getTime());
							pstmt.setTimestamp(2, tstamp);
							dateObj = sdf.parse(sEndTime);
						    tstamp = new Timestamp(dateObj.getTime());
							pstmt.setTimestamp(3, tstamp);
						} catch (Exception e) {
							e.printStackTrace();
						}
						pstmt.execute();
						rs = (ResultSet) pstmt.getObject(1);
						
							while (rs.next()) {
								temperatureBean = new TemperatureBean();
								if(rs.getString(1) == null || rs.getString(1).equals("")) {
									temperatureBean.setZone_Id("");
								} else {
									temperatureBean.setZone_Id(rs.getString(1));
								}
								if(rs.getString(2) == null || rs.getString(2).equals("")) {
									temperatureBean.setTimestamp("");
								} else {
									temperatureBean.setTimestamp(rs.getString(2));
								}
								if(rs.getString(3) == null || rs.getString(3).equals("")) {
									temperatureBean.setTemperature("");
								} else {
									temperatureBean.setTemperature(rs.getString(3));
								}
								col.add(temperatureBean);
							}
					} catch (Exception e) {
						logger.error("Exception: " + e.getLocalizedMessage());
						e.printStackTrace();
					} finally {
						try {
							if (rs != null)
								rs.close();
							if (pstmt != null)
								pstmt.close();
							DbConnection.closeConnection();
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					return col;
				}
}
