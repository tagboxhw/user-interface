package com.tagbox.rest.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tagbox.rest.bean.AlertSummaryBean;
import com.tagbox.rest.bean.HumidityBean;
import com.tagbox.rest.bean.ParameterSummaryBean;
import com.tagbox.rest.util.DbConnection;
import com.tagbox.rest.util.SqlConstants;

public class AlertSummaryDao {
	static final Logger logger = LoggerFactory
			.getLogger(AlertSummaryDao.class);
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	
	//REST Service to retrieve AlertSummarydata
	public Collection<AlertSummaryBean> getAlertSummaryData(String sStartTime, String sEndTime) {
		Connection conn = null;
		CallableStatement pstmt = null;
		ResultSet rs = null;
		Statement statement = null;
		AlertSummaryBean alertSummaryBean = new AlertSummaryBean();
		Collection<AlertSummaryBean> col = new Vector<AlertSummaryBean>();
		try {
			conn = DbConnection.getConnection();
			conn.setAutoCommit(false);
			// Create and execute a SELECT SQL statement.
			// Create and execute a SELECT SQL statement.
			try {
				pstmt = conn.prepareCall(SqlConstants.GetAlertSummary);
				pstmt.registerOutParameter(1, Types.OTHER);
				Date dateObj = sdf.parse(sStartTime);
			    Timestamp tstamp = new Timestamp(dateObj.getTime());
				pstmt.setTimestamp(2, tstamp);
				dateObj = sdf.parse(sEndTime);
			    tstamp = new Timestamp(dateObj.getTime());
				pstmt.setTimestamp(3, tstamp);
			} catch (Exception e) {
				e.printStackTrace();
			}
			pstmt.execute();
			rs = (ResultSet) pstmt.getObject(1);

				while (rs.next()) {
					alertSummaryBean = new AlertSummaryBean();
					if(rs.getString(1) == null || rs.getString(1).equals("")) {
						alertSummaryBean.setsLocationName("");
					} else {
						alertSummaryBean.setsLocationName(rs.getString(1));
					}
					if(rs.getString(2) == null || rs.getString(2).equals("")) {
						alertSummaryBean.setsZoneName("");
					} else {
						alertSummaryBean.setsZoneName(rs.getString(2));
					}
					if(rs.getString(3) == null) {
						alertSummaryBean.setsAlertSeverity(0);
					} else {
						alertSummaryBean.setsAlertSeverity(rs.getInt(3));
					}
					if(rs.getString(4) == null) {
						alertSummaryBean.setResolution(0);
					} else {
						alertSummaryBean.setResolution(rs.getInt(4));
					}
					if(rs.getString(5) == null) {
						alertSummaryBean.setsCount(0);
					} else {
						alertSummaryBean.setsCount(rs.getInt(5));
					}
					
					col.add(alertSummaryBean);
				}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return col;
	}
	
}
