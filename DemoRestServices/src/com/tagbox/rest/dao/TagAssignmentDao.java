package com.tagbox.rest.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tagbox.rest.bean.ParameterSummaryBean;
import com.tagbox.rest.bean.TagAssignmentBean;
import com.tagbox.rest.util.DbConnection;
import com.tagbox.rest.util.SqlConstants;

public class TagAssignmentDao {
	static final Logger logger = LoggerFactory
			.getLogger(TagAssignmentDao.class);
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	
	public Collection<TagAssignmentBean> selectAllTagAssignmentRecords() {
		Collection<TagAssignmentBean> col = new Vector<TagAssignmentBean>();
		Connection conn = null;
		CallableStatement pstmt = null;
		ResultSet rs = null;
		TagAssignmentBean tvBean = new TagAssignmentBean();
		Statement statement = null;
		try {
			conn = DbConnection.getConnection();
			conn.setAutoCommit(false);
			try {
				pstmt = conn.prepareCall(SqlConstants.GetTagAssignmentData);
				pstmt.registerOutParameter(1, Types.OTHER);
			} catch (Exception e) {
				e.printStackTrace();
			}
			pstmt.execute();
			rs = (ResultSet) pstmt.getObject(1);
			// logger.info("start");
			while (rs.next()) {
				// logger.info(rs.getString(1) + " " + rs.getString(2) + " "
				// +rs.getString(3) + " " + rs.getString(4));
				tvBean = new TagAssignmentBean();
				if(rs.getString(1) == null || rs.getString(1).equals("")) {
					tvBean.setTag_Id("");
				} else {
					tvBean.setTag_Id(rs.getString(1));
				}
				if(rs.getString(2) == null || rs.getString(2).equals("")) {
					tvBean.setShip_Date("");
				} else {
					tvBean.setShip_Date(rs.getString(2));
				}
				if(rs.getString(3) == null || rs.getString(3).equals("")) {
					tvBean.setInv_Id("");
				} else {
					tvBean.setInv_Id(rs.getString(3));
				}
				if(rs.getString(4) == null || rs.getString(4).equals("")) {
					tvBean.setShip_Source("");
				} else {
					tvBean.setShip_Source(rs.getString(4));
				}
				if(rs.getString(5) == null || rs.getString(5).equals("")) {
					tvBean.setShip_Dest("");
				} else {
					tvBean.setShip_Dest(rs.getString(5));
				}
				if(rs.getString(6) == null || rs.getString(6).equals("")) {
					tvBean.setShip_Loc("");
				} else {
					tvBean.setShip_Loc(rs.getString(6));
				}
				if(rs.getString(7) == null || rs.getString(7).equals("")) {
					tvBean.setFlight_Id("");
				} else {
					tvBean.setFlight_Id(rs.getString(7));
				}
				if(rs.getString(8) == null || rs.getString(8).equals("")) {
					tvBean.setTrip_Status("");
				} else {
					tvBean.setTrip_Status(rs.getString(8));
				}
				if(rs.getString(9) == null || rs.getString(9).equals("")) {
					tvBean.setCriticality("");
				} else {
					tvBean.setCriticality(rs.getString(9));
				}
				if(rs.getString(10) == null || rs.getString(10).equals("")) {
					tvBean.setSubzone_Id("");
				} else {
					tvBean.setSubzone_Id(rs.getString(10));
				}
				col.add(tvBean);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return col;
	}
	

	
	//REST Service to retrieve ParameterSummary Humidity data
	public int updateTagAssignment(String sTagId, String sShipDate, String sShipId, String sSource, String sDest, String sCriticalityId){
		Connection conn = null;
		CallableStatement pstmt = null;
		ResultSet rs = null;
		Statement statement = null;
		try {
			conn = DbConnection.getConnection();
			conn.setAutoCommit(false);
			// Create and execute a SELECT SQL statement.
			// Create and execute a SELECT SQL statement.
			try {
				pstmt = conn.prepareCall(SqlConstants.UpdateTagAssignment);
				pstmt.registerOutParameter(1, Types.OTHER);
				pstmt.setString(2, sTagId);
			    Date dateObj = sdf.parse(sShipDate);
			    Timestamp tstamp = new Timestamp(dateObj.getTime());
				pstmt.setTimestamp(3, tstamp);
				pstmt.setString(4, sShipId);
				pstmt.setString(5, sSource);
				pstmt.setString(6, sDest);
				pstmt.setString(7, sCriticalityId);
			} catch (Exception e) {
				e.printStackTrace();
			}
			pstmt.execute();
			rs = (ResultSet) pstmt.getObject(1);

				while (rs.next()) {
					
				}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return 0;
	}
	
	public int updateVehicleDetails(String sVehicleId, String sSource,
			String sDestination, String sDriverId) {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement pSelect = null;
		String updateSql = "";
		int iReturn = 0;
		try {
			conn = DbConnection.getConnection();
			pSelect = conn.prepareStatement(SqlConstants.UpdateVehicleManagement);
			pSelect.setString(1, sSource);
			pSelect.setString(2, sDestination);
			pSelect.setString(3, sDriverId);
			pSelect.setString(4, sVehicleId);
			iReturn = pSelect.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return iReturn;
	}
	
}
