package com.tagbox.rest.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.Collection;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tagbox.rest.bean.ETagSummaryBean;
import com.tagbox.rest.bean.ShipmentDetailsBean;
import com.tagbox.rest.util.DbConnection;
import com.tagbox.rest.util.SqlConstants;

public class TripSummaryDao {
	static final Logger logger = LoggerFactory
			.getLogger(TripSummaryDao.class);

	public Collection<ShipmentDetailsBean> selectAllShipmentDetails() {
		Collection<ShipmentDetailsBean> col = new Vector<ShipmentDetailsBean>();
		Connection conn = null;
		CallableStatement pstmt = null;
		ResultSet rs = null;
		ShipmentDetailsBean tvBean = new ShipmentDetailsBean();
		Statement statement = null;
		try {
			conn = DbConnection.getConnection();
			conn.setAutoCommit(false);
			try {
				pstmt = conn.prepareCall(SqlConstants.GetETagSummary1);
				pstmt.registerOutParameter(1, Types.OTHER);
			} catch (Exception e) {
				e.printStackTrace();
			}
			pstmt.execute();
			rs = (ResultSet) pstmt.getObject(1);
			// logger.info("start");
			while (rs.next()) {
				// logger.info(rs.getString(1) + " " + rs.getString(2) + " "
				// +rs.getString(3) + " " + rs.getString(4));
				tvBean = new ShipmentDetailsBean();
				if(rs.getString(1) == null || rs.getString(1).equals("")) {
					tvBean.setTTE("");
				} else {
					tvBean.setTTE(rs.getString(1));
				}
				if(rs.getString(2) == null || rs.getString(2).equals("")) {
					tvBean.setMaxT("");
				} else {
					tvBean.setMaxT(rs.getString(2));
				}
				if(rs.getString(3) == null || rs.getString(3).equals("")) {
					tvBean.setMinT("");
				} else {
					tvBean.setMinT(rs.getString(3));
				}
				col.add(tvBean);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return col;
	}
	
	
	public Collection<ETagSummaryBean> selectETagSummary(String sShipmentId) {
		Collection<ETagSummaryBean> col = new Vector<ETagSummaryBean>();
		Connection conn = null;
		CallableStatement pstmt = null;
		ResultSet rs = null;
		ETagSummaryBean tvBean = new ETagSummaryBean();
		Statement statement = null;
		try {
			conn = DbConnection.getConnection();
			conn.setAutoCommit(false);
			try {
				pstmt = conn.prepareCall(SqlConstants.GetETagSummary2);
				pstmt.registerOutParameter(1, Types.OTHER);
				pstmt.setInt(2, Integer.parseInt(sShipmentId));
			} catch (Exception e) {
				e.printStackTrace();
			}
			pstmt.execute();
			rs = (ResultSet) pstmt.getObject(1);
			// logger.info("start");
			while (rs.next()) {
				// logger.info(rs.getString(1) + " " + rs.getString(2) + " "
				// +rs.getString(3) + " " + rs.getString(4));
				tvBean = new ETagSummaryBean();
				if(rs.getString(1) == null || rs.getString(1).equals("")) {
					tvBean.setTagId("");
				} else {
					tvBean.setTagId(rs.getString(1));
				}
				if(rs.getString(2) == null || rs.getString(2).equals("")) {
					tvBean.setTimestamp("");
				} else {
					tvBean.setTimestamp(rs.getString(2));
				}
				if(rs.getString(3) == null) {
					tvBean.setLMF(0);
				} else {
					tvBean.setLMF(rs.getInt(3));
				}
				if(rs.getString(4) == null) {
					tvBean.setFTT(0);
				} else {
					tvBean.setFTT(rs.getInt(4));
				}
				if(rs.getString(5) == null) {
					tvBean.setTTE(0);
				} else {
					tvBean.setTTE(rs.getInt(5));
				}
				if(rs.getString(6) == null) {
					tvBean.setETF(0);
				} else {
					tvBean.setETF(rs.getInt(6));
				}
				if(rs.getString(7) == null) {
					tvBean.setGTF(0);
				} else {
					tvBean.setGTF(rs.getInt(7));
				}
				col.add(tvBean);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return col;
	}
		
}
