package com.tagbox.rest.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.TreeMap;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tagbox.rest.bean.TemperatureBean;
import com.tagbox.rest.bean.TransitTemperatureBean;
import com.tagbox.rest.util.DbConnection;
import com.tagbox.rest.util.SqlConstants;
import com.tagbox.rest.util.Utils;

public class TransitTemperatureDao {

	static final Logger logger = LoggerFactory
			.getLogger(TransitTemperatureDao.class);

	public TreeMap getChartData(String sVehicleId, String sTime) {
		Collection col = new Vector();
		Connection conn = null;
		ResultSet rs = null;
		TransitTemperatureBean stBean = new TransitTemperatureBean();
		TreeMap tm = new TreeMap();
		try {
			double fTime = Utils.getAlertTime(sTime);
			// logger.info("fTime: " + fTime);
			conn = DbConnection.getConnection();
			PreparedStatement pSelect = conn
					.prepareStatement(SqlConstants.GetChartData_Transit_Temperature);
			pSelect.setString(1, sVehicleId);
			pSelect.setDouble(2, fTime);
			rs = pSelect.executeQuery();
			while (rs.next()) {
				// logger.info(rs.getString(1) + " " + rs.getString(2) + " "
				// +rs.getString(3) + " " + rs.getString(4));
				stBean = new TransitTemperatureBean();
				stBean.setGW_client_id(rs.getString(1));
				stBean.setND_client_id(rs.getString(2));
				stBean.setTimestamp(rs.getString(3));
				stBean.setTemperature(rs.getString(4));
				if (tm.containsKey(stBean.getND_client_id())) {
					col = (Collection) tm.get(stBean.getND_client_id());
					col.add(stBean);
				} else {
					col.add(stBean);
					tm.put(stBean.getND_client_id(), col);
				}
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return tm;
	}

	//Not using this method anymore
	public Collection selectAllStationaryTemperatureRecords() {
		Collection col = new Vector();
		Connection conn = null;
		ResultSet rs = null;
		TemperatureBean stBean = new TemperatureBean();
		Statement statement = null;
		try {
			conn = DbConnection.getConnection();
			try {
				statement = conn.createStatement();
				rs = statement
						.executeQuery(SqlConstants.SelectAllStationaryTemperatureRecords);
			} catch (Exception e) {
				e.printStackTrace();
			}
			// logger.info("start");
			while (rs.next()) {
				// logger.info(rs.getString(1) + " " + rs.getString(2) + " "
				// +rs.getString(3) + " " + rs.getString(4));
				stBean = new TemperatureBean();
				stBean.setZone_Id(rs.getString(1));
				stBean.setNode_Id(rs.getString(2));
				stBean.setTimestamp(rs.getString(3));
				stBean.setTemperature(rs.getString(4));
				col.add(stBean);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return col;
	}

	//Not using this method anymore
	public Collection getChartData(String sGWClientId, String sNDClientId,
			String sTime) {
		Collection col = new Vector();
		Connection conn = null;
		ResultSet rs = null;
		TemperatureBean stBean = new TemperatureBean();
		try {
			double fTime = Utils.getAlertTime(sTime);
			// logger.info("fTime: " + fTime);
			conn = DbConnection.getConnection();
			PreparedStatement pSelect = conn
					.prepareStatement(SqlConstants.GetChartData_Temperature_WithClient);
			pSelect.setString(1, sGWClientId);
			pSelect.setString(2, sNDClientId);
			pSelect.setDouble(3, fTime);
			// logger.info("1: " + System.currentTimeMillis());
			rs = pSelect.executeQuery();
			while (rs.next()) {
				// logger.info(rs.getString(1) + " " + rs.getString(2) + " "
				// +rs.getString(3) + " " + rs.getString(4));
				stBean = new TemperatureBean();
				stBean.setZone_Id(sGWClientId);
				stBean.setNode_Id(sNDClientId);
				stBean.setTimestamp(rs.getString(3));
				stBean.setTemperature(rs.getString(4));
				col.add(stBean);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return col;
	}

}
