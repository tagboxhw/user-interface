package com.tagbox.rest.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tagbox.rest.bean.HumidityBean;
import com.tagbox.rest.bean.ParameterSummaryBean;
import com.tagbox.rest.util.DbConnection;
import com.tagbox.rest.util.SqlConstants;

public class ParameterSummaryDao {
	static final Logger logger = LoggerFactory
			.getLogger(ParameterSummaryDao.class);
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	
	//REST Service to retrieve ParameterSummary Humidity data
	public Collection<ParameterSummaryBean> getParameterSummaryHumidityData(String sStartTime, String sEndTime) {
		Connection conn = null;
		CallableStatement pstmt = null;
		ResultSet rs = null;
		Statement statement = null;
		ParameterSummaryBean parameterSummaryBean = new ParameterSummaryBean();
		Collection<ParameterSummaryBean> col = new Vector<ParameterSummaryBean>();
		try {
			conn = DbConnection.getConnection();
			conn.setAutoCommit(false);
			// Create and execute a SELECT SQL statement.
			// Create and execute a SELECT SQL statement.
			try {
				pstmt = conn.prepareCall(SqlConstants.GetParameterSummaryHumidity);
				pstmt.registerOutParameter(1, Types.OTHER);
			    Date dateObj = sdf.parse(sStartTime);
			    Timestamp tstamp = new Timestamp(dateObj.getTime());
				pstmt.setTimestamp(2, tstamp);
				dateObj = sdf.parse(sEndTime);
			    tstamp = new Timestamp(dateObj.getTime());
				pstmt.setTimestamp(3, tstamp);
			} catch (Exception e) {
				e.printStackTrace();
			}
			pstmt.execute();
			rs = (ResultSet) pstmt.getObject(1);

				while (rs.next()) {
					
					parameterSummaryBean = new ParameterSummaryBean();
					if(rs.getString(1) == null || rs.getString(1).equals("")) {
						parameterSummaryBean.setLocation_ID("");
					} else {
						parameterSummaryBean.setLocation_ID(rs.getString(1));
					}
					if(rs.getString(2) == null || rs.getString(2).equals("")) {
						parameterSummaryBean.setZone_ID("");
					} else {
						parameterSummaryBean.setZone_ID(rs.getString(2));
					}
					if(rs.getString(3) == null || rs.getString(3).equals("")) {
						parameterSummaryBean.setMax_Value("");
					} else {
						parameterSummaryBean.setMax_Value(rs.getString(3));
					}
					if(rs.getString(4) == null || rs.getString(4).equals("")) {
						parameterSummaryBean.setMin_Value("");
					} else {
						parameterSummaryBean.setMin_Value(rs.getString(4));
					}
					if(rs.getString(5) == null || rs.getString(5).equals("")) {
						parameterSummaryBean.setMean_Value("");
					} else {
						parameterSummaryBean.setMean_Value(rs.getString(5));
					}
					if(rs.getString(6) == null || rs.getString(6).equals("")) {
						parameterSummaryBean.setVolatility("");
					} else {
						parameterSummaryBean.setVolatility(rs.getString(6));
					}
					if(rs.getString(7) == null || rs.getString(7).equals("")) {
						parameterSummaryBean.setTotals("");
					} else {
						parameterSummaryBean.setTotals(rs.getString(7));
					}
					if(rs.getString(8) == null || rs.getString(8).equals("")) {
						parameterSummaryBean.setExcursion("");
					} else {
						parameterSummaryBean.setExcursion(rs.getString(8));
					}
					if(rs.getString(9) == null || rs.getString(9).equals("")) {
						parameterSummaryBean.setVol_Ap("");
					} else {
						parameterSummaryBean.setVol_Ap(rs.getString(9));
					}
					if(rs.getString(10) == null || rs.getString(10).equals("")) {
						parameterSummaryBean.setSub_Zone_ID("");
					} else {
						parameterSummaryBean.setSub_Zone_ID(rs.getString(10));
					}
					
					col.add(parameterSummaryBean);
				}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return col;
	}
	
	//REST Service to retrieve ParameterSummary Temperature data
		public Collection<ParameterSummaryBean> getParameterSummaryTemperatureData(String sStartTime, String sEndTime) {
			Connection conn = null;
			CallableStatement pstmt = null;
			ResultSet rs = null;
			Statement statement = null;
			ParameterSummaryBean parameterSummaryBean = new ParameterSummaryBean();
			Collection<ParameterSummaryBean> col = new Vector<ParameterSummaryBean>();
			try {
				conn = DbConnection.getConnection();
				conn.setAutoCommit(false);
				// Create and execute a SELECT SQL statement.
				// Create and execute a SELECT SQL statement.
				try {
					pstmt = conn.prepareCall(SqlConstants.GetParameterSummaryTemperature);
					pstmt.registerOutParameter(1, Types.OTHER);
					Date dateObj = sdf.parse(sStartTime);
				    Timestamp tstamp = new Timestamp(dateObj.getTime());
					pstmt.setTimestamp(2, tstamp);
					dateObj = sdf.parse(sEndTime);
				    tstamp = new Timestamp(dateObj.getTime());
					pstmt.setTimestamp(3, tstamp);
				} catch (Exception e) {
					e.printStackTrace();
				}
				pstmt.execute();
				rs = (ResultSet) pstmt.getObject(1);
				
					while (rs.next()) {
						parameterSummaryBean = new ParameterSummaryBean();
						if(rs.getString(1) == null || rs.getString(1).equals("")) {
							parameterSummaryBean.setLocation_ID("");
						} else {
							parameterSummaryBean.setLocation_ID(rs.getString(1));
						}
						if(rs.getString(2) == null || rs.getString(2).equals("")) {
							parameterSummaryBean.setZone_ID("");
						} else {
							parameterSummaryBean.setZone_ID(rs.getString(2));
						}
						if(rs.getString(3) == null || rs.getString(3).equals("")) {
							parameterSummaryBean.setMax_Value("");
						} else {
							parameterSummaryBean.setMax_Value(rs.getString(3));
						}
						if(rs.getString(4) == null || rs.getString(4).equals("")) {
							parameterSummaryBean.setMin_Value("");
						} else {
							parameterSummaryBean.setMin_Value(rs.getString(4));
						}
						if(rs.getString(5) == null || rs.getString(5).equals("")) {
							parameterSummaryBean.setMean_Value("");
						} else {
							parameterSummaryBean.setMean_Value(rs.getString(5));
						}
						if(rs.getString(6) == null || rs.getString(6).equals("")) {
							parameterSummaryBean.setVolatility("");
						} else {
							parameterSummaryBean.setVolatility(rs.getString(6));
						}
						if(rs.getString(7) == null || rs.getString(7).equals("")) {
							parameterSummaryBean.setTotals("");
						} else {
							parameterSummaryBean.setTotals(rs.getString(7));
						}
						if(rs.getString(8) == null || rs.getString(8).equals("")) {
							parameterSummaryBean.setExcursion("");
						} else {
							parameterSummaryBean.setExcursion(rs.getString(8));
						}
						if(rs.getString(9) == null || rs.getString(9).equals("")) {
							parameterSummaryBean.setVol_Ap("");
						} else {
							parameterSummaryBean.setVol_Ap(rs.getString(9));
						}
						if(rs.getString(10) == null || rs.getString(10).equals("")) {
							parameterSummaryBean.setSub_Zone_ID("");
						} else {
							parameterSummaryBean.setSub_Zone_ID(rs.getString(10));
						}
						
						col.add(parameterSummaryBean);
					}
			} catch (Exception e) {
				logger.error("Exception: " + e.getLocalizedMessage());
				e.printStackTrace();
			} finally {
				try {
					if (rs != null)
						rs.close();
					if (pstmt != null)
						pstmt.close();
					DbConnection.closeConnection();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return col;
		}
}
