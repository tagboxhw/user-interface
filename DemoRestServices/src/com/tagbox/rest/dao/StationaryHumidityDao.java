package com.tagbox.rest.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.Collection;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tagbox.rest.bean.PlanMapsBean;
import com.tagbox.rest.bean.HumidityBean;
import com.tagbox.rest.util.DbConnection;
import com.tagbox.rest.util.SqlConstants;
import com.tagbox.rest.util.Utils;

public class StationaryHumidityDao {

	static final Logger logger = LoggerFactory
			.getLogger(StationaryHumidityDao.class);

	public Collection getChartData(String sNodeId, String sTime) {
		Collection col = new Vector();
		Connection conn = null;
		ResultSet rs = null;
		HumidityBean stBean = new HumidityBean();
		try {
			double fTime = Utils.getAlertTime(sTime);
			// logger.info("fTime: " + fTime);
			conn = DbConnection.getConnection();
			PreparedStatement pSelect = conn
					.prepareStatement(SqlConstants.GetChartData_Humidity);
			pSelect.setString(1, sNodeId);
			pSelect.setDouble(2, fTime);
			rs = pSelect.executeQuery();
			while (rs.next()) {
				// logger.info(rs.getString(1) + " " + rs.getString(2) + " "
				// +rs.getString(3) + " " + rs.getString(4));
				stBean = new HumidityBean();
				stBean.setZone_Id(rs.getString(1));
				stBean.setNode_Id(sNodeId);
				stBean.setTimestamp(rs.getString(3));
				stBean.setHumidity(rs.getString(4));
				col.add(stBean);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return col;
	}

	//Not using this method anymore
	public Collection selectAllStationaryHumidityRecords() {
		Collection col = new Vector();
		Calendar rightNow = Calendar.getInstance();
		Connection conn = null;
		ResultSet rs = null;
		HumidityBean stBean = new HumidityBean();
		Statement statement = null;
		try {
			conn = DbConnection.getConnection();
			/*
			 * String selectSql =
			 * "select gw_client_id, nd_client_id, timestamp, humidity from LEF.stationary_humidity where nd_client_id = ? order by timestamp asc"
			 * ; PreparedStatement pSelect = conn.prepareStatement(selectSql);
			 * pSelect.setString(1, "F7:59:BC:D2:9F:C3"); rs =
			 * pSelect.executeQuery();
			 */

			try {
				statement = conn.createStatement();
				rs = statement
						.executeQuery(SqlConstants.SelectAllStationaryHumidityRecords);
			} catch (Exception e) {
				e.printStackTrace();
			}

			while (rs.next()) {
				// logger.info(rs.getString(1) + " " + rs.getString(2) + " "
				// +rs.getString(3) + " " + rs.getString(4));
				stBean = new HumidityBean();
				stBean.setZone_Id(rs.getString(1));
				stBean.setNode_Id(rs.getString(2));
				stBean.setTimestamp(rs.getString(3));
				stBean.setHumidity(rs.getString(4));
				col.add(stBean);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return col;
	}

	//Not using this method anymore
	public Collection getChartData(String sGWClientId, String sNDClientId,
			String sTime) {
		Collection col = new Vector();
		Connection conn = null;
		ResultSet rs = null;
		HumidityBean stBean = new HumidityBean();
		try {
			double fTime = Utils.getAlertTime(sTime);
			// logger.info("fTime: " + fTime);
			conn = DbConnection.getConnection();
			PreparedStatement pSelect = conn
					.prepareStatement(SqlConstants.GetChartData_WithClient);
			pSelect.setString(1, sGWClientId);
			pSelect.setString(2, sNDClientId);
			pSelect.setDouble(3, fTime);
			rs = pSelect.executeQuery();
			while (rs.next()) {
				// logger.info(rs.getString(1) + " " + rs.getString(2) + " "
				// +rs.getString(3) + " " + rs.getString(4));
				stBean = new HumidityBean();
				stBean.setZone_Id(sGWClientId);
				stBean.setNode_Id(sNDClientId);
				stBean.setTimestamp(rs.getString(3));
				stBean.setHumidity(rs.getString(4));
				col.add(stBean);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return col;
	}

	//Not using this method anymore
	public Collection selectPlanAllRecords() {
		Collection col = new Vector();
		Connection conn = null;
		PreparedStatement psSelect = null;
		ResultSet rs = null;
		PlanMapsBean mapsBean = new PlanMapsBean();
		Statement statement = null;
		try {
			conn = DbConnection.getConnection();

			// Create and execute a SELECT SQL statement.
			// Create and execute a SELECT SQL statement.
			try {
				statement = conn.createStatement();
			} catch (Exception e) {
				e.printStackTrace();
			}
			rs = statement.executeQuery(SqlConstants.SelectPlanAllRecords);

			while (rs.next()) {
				mapsBean = new PlanMapsBean();
				mapsBean.setGateway_id(rs.getString(1));
				mapsBean.setLatitude(rs.getFloat(2));
				mapsBean.setLongitude(rs.getFloat(3));
				mapsBean.setDCName(rs.getString(4));
				col.add(mapsBean);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (psSelect != null)
					psSelect.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return col;
	}

}
