package com.tagbox.rest.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.Collection;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tagbox.rest.bean.DoorActivityBean;
import com.tagbox.rest.util.DbConnection;
import com.tagbox.rest.util.SqlConstants;

public class DoorActivityDao {
	static final Logger logger = LoggerFactory
			.getLogger(DoorActivityDao.class);
	
	//REST Service to retrieve today's alerts
	public Collection<DoorActivityBean> getDoorActivityData() {
		Connection conn = null;
		CallableStatement pstmt = null;
		ResultSet rs = null;
		Statement statement = null;
		DoorActivityBean doorBean = new DoorActivityBean();
		Collection<DoorActivityBean> col = new Vector<DoorActivityBean>();
		try {
			conn = DbConnection.getConnection();
			conn.setAutoCommit(false);
			// Create and execute a SELECT SQL statement.
			// Create and execute a SELECT SQL statement.
			try {
				pstmt = conn.prepareCall(SqlConstants.GetDoorActivityData);
				pstmt.registerOutParameter(1, Types.OTHER);
			} catch (Exception e) {
				e.printStackTrace();
			}
			pstmt.execute();
			rs = (ResultSet) pstmt.getObject(1);
				while (rs.next()) {
					doorBean = new DoorActivityBean();
					if(rs.getString(1) == null || rs.getString(1).equals("")) {
						doorBean.setND_client_id("");
					} else {
						doorBean.setND_client_id(rs.getString(1));
					}
					if(rs.getString(2) == null || rs.getString(2).equals("")) {
						doorBean.setTimestamp("");
					} else {
						doorBean.setTimestamp(rs.getString(2));
					}
					if(rs.getString(3) == null || rs.getString(3).equals("")) {
						doorBean.setDoorActivity("");
					} else {
						doorBean.setDoorActivity(rs.getString(3));
					}
					if(rs.getString(4) == null || rs.getString(4).equals("")) {
						doorBean.setGW_client_id("");
					} else {
						doorBean.setGW_client_id(rs.getString(4));
					}
					col.add(doorBean);
				}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return col;
	}
}
