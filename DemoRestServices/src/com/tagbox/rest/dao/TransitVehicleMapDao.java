package com.tagbox.rest.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tagbox.rest.bean.TransitVehicleMapBean;
import com.tagbox.rest.util.DbConnection;
import com.tagbox.rest.util.SqlConstants;

public class TransitVehicleMapDao {
	static final Logger logger = LoggerFactory
			.getLogger(TransitVehicleMapDao.class);

		//REST Service to retrieve Transit Vehicle details 
		public Collection<TransitVehicleMapBean> selectAllTransitVehicleMapRecords() {
			Collection<TransitVehicleMapBean> col = new Vector<TransitVehicleMapBean>();
			Connection conn = null;
			ResultSet rs = null;
			TransitVehicleMapBean tvBean = new TransitVehicleMapBean();
			Statement statement = null;
			try {
				conn = DbConnection.getConnection();
				try {
					statement = conn.createStatement();
					rs = statement
							.executeQuery(SqlConstants.SelectAllTransitVehicleMapRecords);
				} catch (Exception e) {
					e.printStackTrace();
				}
				// logger.info("start");
				while (rs.next()) {
					// logger.info(rs.getString(1) + " " + rs.getString(2) + " "
					// +rs.getString(3) + " " + rs.getString(4));
					tvBean = new TransitVehicleMapBean();
					if(rs.getString(1) == null || rs.getString(1).equals("")) {
						tvBean.setVehicle_ID("");
					} else {
						tvBean.setVehicle_ID(rs.getString(1));
					}
					if(rs.getString(2) == null || rs.getString(2).equals("")) {
						tvBean.setVehicle_Status("");
					} else {
						tvBean.setVehicle_Status(rs.getString(2));
					}
					if(rs.getString(3) == null || rs.getString(3).equals("")) {
						tvBean.setAssigned_Date("");
					} else {
						tvBean.setAssigned_Date(rs.getString(3));
					}
					if(rs.getString(4) == null || rs.getString(4).equals("")) {
						tvBean.setVehicle_Type("");
					} else {
						tvBean.setVehicle_Type(rs.getString(4));
					}
					if(rs.getString(5) == null || rs.getString(5).equals("")) {
						tvBean.setDriver_DL_ID("");
					} else {
						tvBean.setDriver_DL_ID(rs.getString(5));
					}
					if(rs.getString(6) == null || rs.getString(6).equals("")) {
						tvBean.setRoute_Type("");
					} else {
						tvBean.setRoute_Type(rs.getString(6));
					}
					if(rs.getString(7) == null || rs.getString(7).equals("")) {
						tvBean.setSource("");
					} else {
						tvBean.setSource(rs.getString(7));
					}
					if(rs.getString(8) == null || rs.getString(8).equals("")) {
						tvBean.setDestination("");
					} else {
						tvBean.setDestination(rs.getString(8));
					}
					if(rs.getString(9) == null || rs.getString(9).equals("")) {
						tvBean.setPlanned_Stops("");
					} else {
						tvBean.setPlanned_Stops(rs.getString(9));
					}
					if(rs.getString(10) == null || rs.getString(10).equals("")) {
						tvBean.setGW_Status("");
					} else {
						tvBean.setGW_Status(rs.getString(10));
					}
					if(rs.getString(11) == null || rs.getString(11).equals("")) {
						tvBean.setTrip_Status("");
					} else {
						tvBean.setTrip_Status(rs.getString(11));
					}
					if(rs.getString(12) == null || rs.getString(12).equals("")) {
						tvBean.setHandover_To("");
					} else {
						tvBean.setHandover_To(rs.getString(12));
					}
					if(rs.getString(13) == null || rs.getString(13).equals("")) {
						tvBean.setDriver_Change_To("");
					} else {
						tvBean.setDriver_Change_To(rs.getString(13));
					}
					if(rs.getString(14) == null || rs.getString(14).equals("")) {
						tvBean.setLatitude("");
					} else {
						tvBean.setLatitude(rs.getString(14));
					}
					if(rs.getString(15) == null || rs.getString(15).equals("")) {
						tvBean.setLongitude("");
					} else {
						tvBean.setLongitude(rs.getString(15));
					}
					col.add(tvBean);
				}
			} catch (Exception e) {
				logger.error("Exception: " + e.getLocalizedMessage());
				e.printStackTrace();
			} finally {
				try {
					if (rs != null)
						rs.close();
					DbConnection.closeConnection();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
			return col;
		}
		
		public int updateVehicleDetails(String sVehicleId, String sSource,
				String sDestination, String sDriverId) {
			Connection conn = null;
			ResultSet rs = null;
			PreparedStatement pSelect = null;
			String updateSql = "";
			int iReturn = 0;
			try {
				conn = DbConnection.getConnection();
				pSelect = conn.prepareStatement(SqlConstants.UpdateVehicleManagement);
				pSelect.setString(1, sSource);
				pSelect.setString(2, sDestination);
				pSelect.setString(3, sDriverId);
				pSelect.setString(4, sVehicleId);
				iReturn = pSelect.executeUpdate();
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					if (rs != null)
						rs.close();
					DbConnection.closeConnection();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return iReturn;
		}

}
