package com.tagbox.rest.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.TreeMap;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tagbox.rest.bean.AlertWorkflowBean;
import com.tagbox.rest.bean.AllLocationDataBean;
import com.tagbox.rest.bean.AllVehicleDataBean;
import com.tagbox.rest.bean.TransitDriverMapBean;
import com.tagbox.rest.bean.TransitLatLongBean;
import com.tagbox.rest.bean.TransitNodeMapBean;
import com.tagbox.rest.bean.TransitVehicleMapBean;
import com.tagbox.rest.bean.ZoneBean;
import com.tagbox.rest.util.Constants;
import com.tagbox.rest.util.DbConnection;
import com.tagbox.rest.util.SqlConstants;
import com.tagbox.rest.util.Utils;

public class TransitDao {
	static final Logger logger = LoggerFactory.getLogger(TransitDao.class);

	
	//REST Service to retrieve all Vehicles details
		public Collection<AllVehicleDataBean> getAllVehiclesData() {
			Connection conn = null;
			CallableStatement pstmt = null;
			ResultSet rs = null;
			Statement statement = null;
			AllVehicleDataBean allVehicleDataBean = new AllVehicleDataBean();
			Collection<AllVehicleDataBean> col = new Vector<AllVehicleDataBean>();
			try {
				conn = DbConnection.getConnection();
				conn.setAutoCommit(false);
				// Create and execute a SELECT SQL statement.
				// Create and execute a SELECT SQL statement.
				try {
					pstmt = conn.prepareCall(SqlConstants.GetAllVehiclesData);
					pstmt.registerOutParameter(1, Types.OTHER);
				} catch (Exception e) {
					e.printStackTrace();
				}
				pstmt.execute();
				rs = (ResultSet) pstmt.getObject(1);
					while (rs.next()) {
						allVehicleDataBean = new AllVehicleDataBean();
						if(rs.getString(1) == null || rs.getString(1).equals("")) {
							allVehicleDataBean.setVehicle_Id("");
						} else {
							allVehicleDataBean.setVehicle_Id(rs.getString(1));
						}
						if(rs.getString(2) == null || rs.getString(2).equals("")) {
							allVehicleDataBean.setRoute_Type("");
						} else {
							allVehicleDataBean.setRoute_Type(rs.getString(2));
						}
						if(rs.getString(3) == null || rs.getString(3).equals("")) {
							allVehicleDataBean.setSource_Location_Id("");
						} else {
							allVehicleDataBean.setSource_Location_Id(rs.getString(3));
						}
						if(rs.getString(4) == null || rs.getString(4).equals("")) {
							allVehicleDataBean.setDest_Location_Id("");
						} else {
							allVehicleDataBean.setDest_Location_Id(rs.getString(4));
						}
						if(rs.getString(5) == null || rs.getString(5).equals("")) {
							allVehicleDataBean.setSource_Location_Name("");
						} else {
							allVehicleDataBean.setSource_Location_Name(rs.getString(5));
						}
						if(rs.getString(6) == null || rs.getString(6).equals("")) {
							allVehicleDataBean.setDest_Location_Name("");
						} else {
							allVehicleDataBean.setDest_Location_Name(rs.getString(6));
						}
						col.add(allVehicleDataBean);
					}
			} catch (Exception e) {
				logger.error("Exception: " + e.getLocalizedMessage());
				e.printStackTrace();
			} finally {
				try {
					if (rs != null)
						rs.close();
					if (pstmt != null)
						pstmt.close();
					DbConnection.closeConnection();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return col;
		}
		
	public TreeMap getAlertsForTransit(String sAlertType, String sTime) {
		Connection conn = null;
		ResultSet rs = null;
		TreeMap tmMap = new TreeMap();
		PreparedStatement pSelect = null;
		String sKey = "";
		try {
			conn = DbConnection.getConnection();
			double fTime = Utils.getAlertTime(sTime);
			logger.info("AlertFilter: " + sAlertType + " Time: " + fTime);
			if (sAlertType.equals("ALL")) {
				pSelect = conn
						.prepareStatement(SqlConstants.GetAlertsForTransit_All);
				pSelect.setDouble(1, fTime);
			} else {
				pSelect = conn
						.prepareStatement(SqlConstants.GetAlertsForTransit);
				String[] sAlerts = sAlertType.split(",");
				sAlertType = "";
				int i = 0;
				for (int in = 0; in < sAlerts.length; in++) {
					i++;
					pSelect.setString(i, sAlerts[in]);
				}
				i++;
				while (i <= 6) {
					pSelect.setString(i, "NULL");
					i++;
				}
				pSelect.setDouble(7, fTime);
			}
			try {
				rs = pSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}
			int total = 0;
			while (rs.next()) {
				logger.info(rs.getString(1) + " " + rs.getString(2));
				sKey = rs.getString(1);
				tmMap.put(sKey, rs.getInt(2));
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return tmMap;
	}

	public TreeMap getAlertVehicles(String sAlertType, String sTime) {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement pSelect = null;
		TreeMap tAll = new TreeMap();
		try {
			conn = DbConnection.getConnection();
			double fTime = Utils.getAlertTime(sTime);
			logger.info("AlertFilter: " + sAlertType + " Time: " + fTime);
			if (sAlertType.equals("ALL")) {
				pSelect = conn
						.prepareStatement(SqlConstants.GetAlertVehicles_All);
				pSelect.setDouble(1, fTime);
				pSelect.setDouble(2, fTime);
			} else {
				pSelect = conn.prepareStatement(SqlConstants.GetAlertVehicles);
				String[] sAlerts = sAlertType.split(",");
				sAlertType = "";
				int i = 0;
				for (int in = 0; in < sAlerts.length; in++) {
					i++;
					pSelect.setString(i, sAlerts[in]);
					pSelect.setString((i + 7), sAlerts[in]);
				}
				i++;
				while (i <= 6) {
					pSelect.setString(i, "NULL");
					pSelect.setString((i + 7), "NULL");
					i++;
				}
				pSelect.setDouble(7, fTime);
				pSelect.setDouble(14, fTime);
			}
			try {
				rs = pSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}
			int total = 0;
			TreeMap tLocalRun = new TreeMap();
			TreeMap tShortHaul = new TreeMap();
			TreeMap tLongHaul = new TreeMap();
			TransitVehicleMapBean tv = new TransitVehicleMapBean();
			while (rs.next()) {
				logger.info(rs.getString(1) + " " + rs.getString(2) + " "
						+ rs.getString(3) + " " + rs.getString(4) + " "
						+ rs.getString(5));
				tv = new TransitVehicleMapBean();
				tv.setRoute_Type(rs.getString(1));
				tv.setVehicle_ID(rs.getString(2));
				tv.setVehicle_Status(rs.getString(3));
				tv.setSource(rs.getString(4));
				tv.setDestination(rs.getString(5));
				if (rs.getString(1).equals(Constants.LONG_HAUL)) {
					tLongHaul.put(rs.getString(2), tv);
				} else if (rs.getString(1).equals(Constants.SHORT_HAUL)) {
					tShortHaul.put(rs.getString(2), tv);
				} else if (rs.getString(1).equals(Constants.LOCAL_RUN)) {
					tLocalRun.put(rs.getString(2), tv);
				}
			}
			tAll.put(Constants.LONG_HAUL, tLongHaul);
			tAll.put(Constants.SHORT_HAUL, tShortHaul);
			tAll.put(Constants.LOCAL_RUN, tLocalRun);
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return tAll;
	}

	public TransitVehicleMapBean getDetailsForVehicleId(String sVehicleId) {
		Connection conn = null;
		PreparedStatement psSelect = null;
		ResultSet rs = null;
		TransitVehicleMapBean tv = new TransitVehicleMapBean();
		try {
			conn = DbConnection.getConnection();

			psSelect = conn
					.prepareStatement(SqlConstants.GetDetailsForVehicleId);
			psSelect.setString(1, sVehicleId);
			try {
				rs = psSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}
			while (rs.next()) {
				tv.setVehicle_ID(rs.getString(1));
				tv.setVehicle_Status(rs.getString(2));
				tv.setAssigned_Date(rs.getString(3));
				tv.setDriver_DL_ID(rs.getString(5));
				tv.setRoute_Type(rs.getString(6));
				tv.setSource(rs.getString(7));
				tv.setDestination(rs.getString(8));
				if (rs.getString(9) == null || rs.getString(9).equals("")
						|| rs.getString(9).equals("NULL")) {
					tv.setPlanned_Stops("");
				} else {
					tv.setPlanned_Stops(rs.getString(9));
				}
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (psSelect != null)
					psSelect.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return tv;
	}

	public LinkedHashMap getMapDetailsVehicleId(String sVehicleId,
			String sAlertType, String sTime) {
		Connection conn = null;
		PreparedStatement psSelect = null;
		ResultSet rs = null;
		String sKey = "";
		String sValue = "";
		String selectSql = "";
		String num = "";
		TransitVehicleMapBean tv = new TransitVehicleMapBean();
		TransitLatLongBean tv1 = new TransitLatLongBean();
		AlertsWorkflowDao aw = new AlertsWorkflowDao();
		LinkedHashMap lhm = new LinkedHashMap();
		try {
			tv = getDetailsForVehicleId(sVehicleId);
			sKey = tv.getSource() + "," + tv.getDestination() + ","
					+ tv.getPlanned_Stops();

			String[] sLocs = sKey.split(",");
			lhm = aw.getAllAlertsForVehicleId(sVehicleId, sAlertType, sTime);
			conn = DbConnection.getConnection();
			Iterator iter = lhm.keySet().iterator();
			AlertWorkflowBean ab = new AlertWorkflowBean();
			while (iter.hasNext()) {
				sValue = (String) iter.next();
				ab = (AlertWorkflowBean) lhm.get(sValue);
				ab.setRoute_Type(Constants.LOCAL_RUNS_DETAIL);
			}
			for (int i = 0; i < sLocs.length; i++) {
				num += "?,";
			}
			num = num.substring(0, num.length() - 1);
			selectSql = "select location_id, location_name, location_lat, location_long from DEV.client_location_map where location_id in ("
					+ num + ")";
			psSelect = conn.prepareStatement(selectSql);
			for (int i = 0; i < sLocs.length; i++) {
				psSelect.setString(i + 1, sLocs[i]);
			}
			try {
				rs = psSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}
			HashMap hm = new HashMap();
			TransitLatLongBean tl = new TransitLatLongBean();
			while (rs.next()) {
				tl = new TransitLatLongBean();
				tl.setGW_client_id(rs.getString(1));
				tl.setTimestamp(rs.getString(2));
				tl.setLatitude(rs.getString(3));
				tl.setLongitude(rs.getString(4));
				hm.put(rs.getString(1), tl);
			}
			for (int i = 0; i < sLocs.length; i++) {
				if (i == 0) {
					tv1 = (TransitLatLongBean) hm.get(sLocs[i]);
					ab = new AlertWorkflowBean();
					ab.setAlert_Location(tv.getVehicle_ID());
					ab.setRoute_Type(Constants.LOCAL_RUNS_DETAIL);
					ab.setCurrent_Location_Name(tv1.getTimestamp());
					ab.setCurrent_Location_Lat(tv1.getLatitude());
					ab.setCurrent_Location_Long(tv1.getLongitude());
					ab.setAlert_Type("Source");
					lhm.put("Source", ab);
				} else if (i == 1) {
					tv1 = (TransitLatLongBean) hm.get(sLocs[i]);
					ab = new AlertWorkflowBean();
					ab.setAlert_Location(tv.getVehicle_ID());
					ab.setRoute_Type(Constants.LOCAL_RUNS_DETAIL);
					ab.setCurrent_Location_Name(tv1.getTimestamp());
					ab.setCurrent_Location_Lat(tv1.getLatitude());
					ab.setCurrent_Location_Long(tv1.getLongitude());
					ab.setAlert_Type("Destination");
					lhm.put("Destination", ab);
				} else {
					tv1 = (TransitLatLongBean) hm.get(sLocs[i]);
					ab = new AlertWorkflowBean();
					ab.setAlert_Location(tv.getVehicle_ID());
					ab.setRoute_Type(Constants.LOCAL_RUNS_DETAIL);
					ab.setCurrent_Location_Name(tv1.getTimestamp());
					ab.setCurrent_Location_Lat(tv1.getLatitude());
					ab.setCurrent_Location_Long(tv1.getLongitude());
					ab.setAlert_Type("Planned");
					num = "Planned" + i;
					lhm.put(num, ab);
				}
			}
			psSelect = conn.prepareStatement(SqlConstants.GetCurrentLocation);
			psSelect.setString(1, sVehicleId);
			try {
				rs = psSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}
			while (rs.next()) {
				ab = new AlertWorkflowBean();
				ab.setAlert_Location(sVehicleId);
				ab.setRoute_Type(Constants.LOCAL_RUNS_DETAIL);
				ab.setCurrent_Location_Lat(rs.getString(2));
				ab.setCurrent_Location_Long(rs.getString(3));
				ab.setAlert_Type("Current");
				lhm.put("Current", ab);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (psSelect != null)
					psSelect.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return lhm;
	}

	public TransitDriverMapBean getDriverDetails(String sDriverId) {
		Connection conn = null;
		PreparedStatement psSelect = null;
		ResultSet rs = null;
		TransitDriverMapBean tv = new TransitDriverMapBean();
		try {
			conn = DbConnection.getConnection();

			psSelect = conn.prepareStatement(SqlConstants.GetDriverDetails);
			psSelect.setString(1, sDriverId);
			try {
				rs = psSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}
			while (rs.next()) {
				tv.setDriver_DL_ID(rs.getString(1));
				tv.setDriver_Name(rs.getString(2));
				tv.setDriver_Phone_Num(rs.getString(3));
				// tv.setDriver_Address(rs.getString(4));
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (psSelect != null)
					psSelect.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return tv;
	}

	//Not using this method anymore
	public TreeMap getAlertsForVehicleId(String sVehicleId, String sAlertType,
			String sTime) {
		Connection conn = null;
		ResultSet rs = null;
		TreeMap tmMap = new TreeMap();
		PreparedStatement pSelect = null;
		String sKey = "";
		TransitVehicleMapBean tv = new TransitVehicleMapBean();
		try {
			conn = DbConnection.getConnection();
			double fTime = Utils.getAlertTime(sTime);
			logger.info("AlertFilter: " + sAlertType + " Time: " + fTime);
			if (sAlertType.equals("ALL")) {

				pSelect = conn
						.prepareStatement(SqlConstants.GetAlertsForVehicleId_All);
				pSelect.setString(1, sVehicleId);
				pSelect.setDouble(2, fTime);
				pSelect.setString(3, sVehicleId);
			} else {
				pSelect = conn
						.prepareStatement(SqlConstants.GetAlertsForVehicleId);
				pSelect.setString(1, sVehicleId);
				pSelect.setDouble(2, fTime);
				pSelect.setString(3, sVehicleId);
				String[] sAlerts = sAlertType.split(",");
				sAlertType = "";
				int i = 3;
				for (int in = 0; in < sAlerts.length; in++) {
					i++;
					pSelect.setString(i, sAlerts[in]);
				}
				i++;
				while (i <= 9) {
					pSelect.setString(i, "NULL");
					i++;
				}
			}
			try {
				rs = pSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}
			int total = 0;
			while (rs.next()) {
				logger.info(rs.getString(1) + " " + rs.getString(2));
				tv = new TransitVehicleMapBean();
				tv.setVehicle_ID(rs.getString(1));
				tv.setSource(rs.getString(2));
				tv.setDestination(rs.getString(3));
				tv.setVehicle_Status(rs.getString(4));
				sKey = rs.getString(1);
				tmMap.put(sKey, tv);
				if (rs.getString(1) != null) {
					sKey = rs.getString(1).toUpperCase() + rs.getString(4);
					if (tmMap.containsKey(sKey)) {
						total = 1 + (Integer) tmMap.get(sKey);
						tmMap.put(sKey, total);
					} else {
						tmMap.put(sKey, 1);
					}
				}
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return tmMap;
	}

	//Not using this method anymore
	public TreeMap getVehicleIdAlerts(String sVehicleId, String sAlertType,
			String sTime) {
		Connection conn = null;
		ResultSet rs = null;
		TreeMap tmMap = new TreeMap();
		PreparedStatement pSelect = null;
		String sKey = "";
		TransitVehicleMapBean tv = new TransitVehicleMapBean();
		try {
			conn = DbConnection.getConnection();
			double fTime = Utils.getAlertTime(sTime);
			logger.info("AlertFilter: " + sAlertType + " Time: " + fTime);
			if (sAlertType.equals("ALL")) {
				pSelect = conn
						.prepareStatement(SqlConstants.GetVehicleIdAlerts_All);
				pSelect.setDouble(1, fTime);
			} else {
				pSelect = conn
						.prepareStatement(SqlConstants.GetVehicleIdAlerts);
				pSelect.setDouble(1, fTime);
				String[] sAlerts = sAlertType.split(",");
				sAlertType = "";
				int i = 1;
				for (int in = 0; in < sAlerts.length; in++) {
					i++;
					pSelect.setString(i, sAlerts[in]);
				}
				i++;
				while (i <= 7) {
					pSelect.setString(i, "NULL");
					i++;
				}
			}
			try {
				rs = pSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}
			int total = 0;
			while (rs.next()) {
				logger.info(rs.getString(1) + " " + rs.getString(2));
				tv = new TransitVehicleMapBean();
				tv.setVehicle_ID(rs.getString(2));
				tv.setRoute_Type(rs.getString(3));
				tv.setVehicle_Status(rs.getString(4));
				sKey = rs.getString(2);
				tmMap.put(sKey, tv);
				if (rs.getString(1) != null) {
					sKey = rs.getString(1).toUpperCase() + rs.getString(5);
					if (tmMap.containsKey(sKey)) {
						total = 1 + (Integer) tmMap.get(sKey);
						tmMap.put(sKey, total);
					} else {
						tmMap.put(sKey, 1);
					}
				}
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return tmMap;
	}

	public List selectSourceVehiclesLocationCity() {
		Connection conn = null;
		PreparedStatement psSelect = null;
		ResultSet rs = null;
		Statement statement = null;
		List<String> al = new ArrayList<String>();
		try {
			conn = DbConnection.getConnection();

			try {
				statement = conn.createStatement();
			} catch (Exception e) {
				e.printStackTrace();
			}
			rs = statement
					.executeQuery(SqlConstants.SelectSourceVehiclesLocationCity);
			while (rs.next()) {
				al.add(rs.getString(1));
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (psSelect != null)
					psSelect.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return al;

	}

	public TreeMap selectSourceVehicleLocationNameId(String sSourceCityName) {
		Connection conn = null;
		PreparedStatement psSelect = null;
		ResultSet rs = null;
		TreeMap tv = new TreeMap();
		try {
			conn = DbConnection.getConnection();
			psSelect = conn
					.prepareStatement(SqlConstants.SelectSourceVehicleLocationNameId);
			psSelect.setString(1, sSourceCityName);
			try {
				rs = psSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}
			while (rs.next()) {
				tv.put(rs.getString(1), rs.getString(2));
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (psSelect != null)
					psSelect.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return tv;
	}

	public List selectDestinationVehiclesLocationCity(String sSourceLocationId) {
		Connection conn = null;
		PreparedStatement psSelect = null;
		ResultSet rs = null;
		List<String> al = new ArrayList<String>();
		try {
			conn = DbConnection.getConnection();

			psSelect = conn
					.prepareStatement(SqlConstants.SelectDestinationVehiclesLocationCity);
			psSelect.setString(1, sSourceLocationId);
			try {
				rs = psSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}
			while (rs.next()) {
				al.add(rs.getString(1));
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (psSelect != null)
					psSelect.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return al;

	}

	public TreeMap selectDestinationVehicleLocationNameId(
			String sDestinationCityName) {
		Connection conn = null;
		PreparedStatement psSelect = null;
		ResultSet rs = null;
		TreeMap tv = new TreeMap();
		try {
			conn = DbConnection.getConnection();

			psSelect = conn
					.prepareStatement(SqlConstants.SelectDestinationVehicleLocationNameId);
			psSelect.setString(1, sDestinationCityName);
			try {
				rs = psSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}
			while (rs.next()) {
				tv.put(rs.getString(1), rs.getString(2));
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (psSelect != null)
					psSelect.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return tv;
	}

	public List<String> selectVehicleIdForSourceDestination(String sSourceId,
			String sDestinationId) {
		Connection conn = null;
		PreparedStatement psSelect = null;
		ResultSet rs = null;
		List<String> al = new ArrayList<String>();
		try {
			conn = DbConnection.getConnection();

			psSelect = conn
					.prepareStatement(SqlConstants.SelectVehicleIdForSourceDestination);
			psSelect.setString(1, sSourceId);
			psSelect.setString(2, sDestinationId);
			try {
				rs = psSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}
			while (rs.next()) {
				al.add(rs.getString(1));
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (psSelect != null)
					psSelect.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return al;
	}

	public Collection selectAllTransitNodeMapRows() {
		Connection conn = null;
		ResultSet rs = null;
		Statement statement = null;
		Collection<TransitNodeMapBean> col = new ArrayList();
		TransitNodeMapBean nd = new TransitNodeMapBean();
		try {
			conn = DbConnection.getConnection();
			try {
				statement = conn.createStatement();
			} catch (Exception e) {
				e.printStackTrace();
			}
			rs = statement
					.executeQuery(SqlConstants.SelectAllTransitNodeMapRows);
			while (rs.next()) {
				nd = new TransitNodeMapBean();
				nd.setVehicle_id(rs.getString(1));
				nd.setGW_client_id(rs.getString(2));
				nd.setND_client_id(rs.getString(3));
				nd.setND_device_id(rs.getString(4));
				nd.setGW_ND_Pairing(rs.getString(5));
				nd.setND_subzone_type(rs.getString(6));
				nd.setND_subzone_name(rs.getString(7));
				nd.setND_type(rs.getString(8));
				nd.setND_status(rs.getString(9));
				col.add(nd);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return col;
	}

	public int insertTransitNodeMap(TransitNodeMapBean tn) {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement pSelect = null;
		int iReturn = 0;
		try {
			conn = DbConnection.getConnection();
			pSelect = conn.prepareStatement(SqlConstants.InsertTransitNodeMap);
			pSelect.setString(1, tn.getVehicle_id());
			pSelect.setString(2, tn.getGW_client_id());
			pSelect.setString(3, tn.getND_client_id());
			pSelect.setString(4, tn.getND_device_id());
			pSelect.setString(5, tn.getGW_ND_Pairing());
			pSelect.setString(6, tn.getND_subzone_type());
			pSelect.setString(7, tn.getND_subzone_name());
			pSelect.setString(8, tn.getND_type());
			pSelect.setString(9, tn.getND_status());

			iReturn = pSelect.executeUpdate();
		} catch (SQLException e) {
			logger.error("localized message: " + e.getLocalizedMessage());
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {

				e.printStackTrace();
			}
		}
		return iReturn;
	}

	public int updateTransitNodeMap(TransitNodeMapBean sn) {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement pSelect = null;
		int iReturn = 0;
		try {
			conn = DbConnection.getConnection();
			pSelect = conn.prepareStatement(SqlConstants.UpdateTransitNodeMap);
			pSelect.setString(1, sn.getVehicle_id());
			pSelect.setString(2, sn.getGW_client_id());
			pSelect.setString(3, sn.getND_device_id());
			pSelect.setString(4, sn.getGW_ND_Pairing());
			pSelect.setString(5, sn.getND_subzone_type());
			pSelect.setString(6, sn.getND_subzone_name());
			pSelect.setString(7, sn.getND_type());
			pSelect.setString(8, sn.getND_status());
			pSelect.setString(9, sn.getND_client_id());
			iReturn = pSelect.executeUpdate();
		} catch (SQLException e) {
			logger.error("localized message: " + e.getLocalizedMessage());
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return iReturn;
	}

	public int deleteTransitNodeMap(String sNDClientID) {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement pSelect = null;
		int iReturn = 0;
		try {
			conn = DbConnection.getConnection();
			pSelect = conn.prepareStatement(SqlConstants.DeleteTransitNodeMap);
			pSelect.setString(1, sNDClientID);
			iReturn = pSelect.executeUpdate();
		} catch (SQLException e) {
			logger.error("localized message: " + e.getLocalizedMessage());
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return iReturn;
	}
}
