package com.tagbox.rest.services;

import java.sql.Timestamp;
import java.util.Collection;

import javax.ws.rs.GET; 
import javax.ws.rs.Path; 
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces; 
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;  

import com.tagbox.rest.bean.HumidityBean;
import com.tagbox.rest.bean.ParameterSummaryBean;
import com.tagbox.rest.dao.HumidityDao;
import com.tagbox.rest.dao.ParameterSummaryDao;
	@Path("/ParameterSummaryService") 

	public class ParameterSummaryService {

	  
		ParameterSummaryDao awDao = new ParameterSummaryDao(); 
	   private static final String SUCCESS_RESULT = "<result>success</result>"; 
	   private static final String FAILURE_RESULT = "<result>failure</result>";  
	   @GET 
	   @Path("/parameterhumidity/{starttime}/{endtime}") 
	   @Produces(MediaType.APPLICATION_XML) 
	   public Collection<ParameterSummaryBean> getParameterSummaryHumidityData(@PathParam("starttime") String sStartTime,
			   @PathParam("endtime") String sEndTime){ 
	      return awDao.getParameterSummaryHumidityData(sStartTime, sEndTime);
	   } 
	   
	   @GET 
	   @Path("/parametertemperature/{starttime}/{endtime}") 
	   @Produces(MediaType.APPLICATION_XML) 
	   public Collection<ParameterSummaryBean> getParameterSummaryTemperatureData(@PathParam("starttime") String sStartTime,
			   @PathParam("endtime") String sEndTime){ 
	      return awDao.getParameterSummaryTemperatureData(sStartTime, sEndTime);
	   } 
	}
