package com.tagbox.rest.services;

import java.util.Collection;

import javax.ws.rs.GET; 
import javax.ws.rs.Path; 
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces; 
import javax.ws.rs.core.MediaType;  

import com.tagbox.rest.bean.HumidityBean;
import com.tagbox.rest.bean.HumidityTriggersBean;
import com.tagbox.rest.bean.ParameterSummaryBean;
import com.tagbox.rest.bean.TemperatureTriggersBean;
import com.tagbox.rest.dao.HumidityDao;
	@Path("/HumidityService") 

	public class HumidityService {

	  
		HumidityDao awDao = new HumidityDao(); 
	   private static final String SUCCESS_RESULT = "<result>success</result>"; 
	   private static final String FAILURE_RESULT = "<result>failure</result>";  
	   @GET 
	   @Path("/humidity") 
	   @Produces(MediaType.APPLICATION_XML) 
	   public Collection<HumidityBean> getHumidityData(){ 
	      return awDao.getHumidityData();
	   } 
	   
	   @GET 
		@Path("/thresholds") 
		@Produces(MediaType.APPLICATION_XML) 
		public Collection<HumidityTriggersBean> getHumidityThresholds(){ 
		  return awDao.getHumidityThresholds();
		} 
	   
	   @GET 
	   @Path("/humidityweek") 
	   @Produces(MediaType.APPLICATION_XML) 
	   public Collection<HumidityBean> getHumidityDataForWeek(){ 
	      return awDao.getHumidityDataForWeek();
	   } 
	   
	   @GET 
	   @Path("/averagehumidity") 
	   @Produces(MediaType.APPLICATION_XML) 
	   public Collection<HumidityBean> getHumidityAverageData(){ 
	      return awDao.getHumidityAverageData();
	   } 
	   
	   @GET 
	   @Path("/zoneaveragehumidity/{starttime}/{endtime}") 
	   @Produces(MediaType.APPLICATION_XML) 
	   public Collection<HumidityBean> getHumidityDataForAllSubzones(@PathParam("starttime") String sStartTime,
			   @PathParam("endtime") String sEndTime){ 
	      return awDao.getHumidityDataForAllSubzones(sStartTime, sEndTime);
	   } 
	}
