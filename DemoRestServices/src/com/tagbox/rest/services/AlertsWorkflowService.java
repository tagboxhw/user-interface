package com.tagbox.rest.services;

import java.util.Collection;

import javax.ws.rs.GET; 
import javax.ws.rs.Path; 
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces; 
import javax.ws.rs.core.MediaType;  

import com.tagbox.rest.bean.AlertWorkflowBean;
import com.tagbox.rest.bean.ClientLocationMapBean;
import com.tagbox.rest.bean.LocationVehicleAlertsDataBean;
import com.tagbox.rest.bean.ParameterSummaryBean;
import com.tagbox.rest.bean.TemperatureBean;
import com.tagbox.rest.bean.TodaysAlertWorkflowBean;
import com.tagbox.rest.dao.AlertsWorkflowDao;
import com.tagbox.rest.dao.ClientLocationMapDao;
	@Path("/AlertsWorkflowService") 

	public class AlertsWorkflowService {

	  
		AlertsWorkflowDao awDao = new AlertsWorkflowDao(); 
	   private static final String SUCCESS_RESULT = "<result>success</result>"; 
	   private static final String FAILURE_RESULT = "<result>failure</result>";  
	   @GET 
	   @Path("/todayalerts") 
	   @Produces(MediaType.APPLICATION_XML) 
	   public Collection<TodaysAlertWorkflowBean> getLocations(){ 
	      return awDao.getTodaysAlerts();
	   } 
	   
	   @GET 
	   @Path("/alertscomp") 
	   @Produces(MediaType.APPLICATION_XML) 
	   public Collection<TodaysAlertWorkflowBean> getAlertsComp(){ 
	      return awDao.getAlertsComp();
	   } 
	   
	   @GET 
	   @Path("/alertsreport") 
	   @Produces(MediaType.APPLICATION_XML) 
	   public Collection<AlertWorkflowBean> getAlertsReport(){ 
	      return awDao.getAlertsReports();
	   } 
	   
	   @GET 
	   @Path("/history/{starttime}/{endtime}/{subzoneid}") 
	   @Produces(MediaType.APPLICATION_XML) 
	   public Collection<TemperatureBean> getHistoricalTemperatureData(@PathParam("starttime") String sStartTime,
			   @PathParam("endtime") String sEndTime, @PathParam("subzoneid") String sSubZoneId){ 
	      return awDao.getHistoricalTemperatureData(sStartTime, sEndTime, sSubZoneId);
	   } 
}
