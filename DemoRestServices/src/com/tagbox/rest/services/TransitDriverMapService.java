package com.tagbox.rest.services;

import java.util.Collection;

import javax.ws.rs.GET; 
import javax.ws.rs.Path; 
import javax.ws.rs.Produces; 
import javax.ws.rs.core.MediaType;  

import com.tagbox.rest.bean.TransitDriverMapBean;
import com.tagbox.rest.bean.TransitVehicleMapBean;
import com.tagbox.rest.dao.TransitDriverMapDao;
import com.tagbox.rest.dao.TransitVehicleMapDao;
	@Path("/TransitDriverMapService") 

	public class TransitDriverMapService {

	  
		TransitDriverMapDao tdmDao = new TransitDriverMapDao(); 
	   private static final String SUCCESS_RESULT = "<result>success</result>"; 
	   private static final String FAILURE_RESULT = "<result>failure</result>";  
	   @GET 
	   @Path("/drivermap") 
	   @Produces(MediaType.APPLICATION_XML) 
	   public Collection<TransitDriverMapBean> getDriver(){ 
	      return tdmDao.selectDriver(); 
	   }  
}
