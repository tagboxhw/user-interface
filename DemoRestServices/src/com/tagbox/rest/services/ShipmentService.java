package com.tagbox.rest.services;

import java.util.Collection;

import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET; 
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path; 
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces; 
import javax.ws.rs.Consumes; 
import javax.ws.rs.core.MediaType;  
 










import com.tagbox.rest.bean.HumidityBean;
import com.tagbox.rest.bean.ParameterSummaryBean;
import com.tagbox.rest.bean.ShipmentBean;
import com.tagbox.rest.bean.ShipmentBoxInfo;
import com.tagbox.rest.bean.ShipmentLocations;
import com.tagbox.rest.bean.ShipmentProducts;
import com.tagbox.rest.bean.ShipmentTags;
import com.tagbox.rest.bean.ShipmentTransitInfo;
import com.tagbox.rest.bean.ShipmentUsers;
import com.tagbox.rest.bean.TemperatureBean;
import com.tagbox.rest.dao.HumidityDao;
import com.tagbox.rest.dao.ShipmentDao;
	@Path("/ShipmentService") 

	public class ShipmentService {

	  
		ShipmentDao awDao = new ShipmentDao(); 
	   
		@GET 
	   @Path("/nextshipmentid") 
	   @Produces(MediaType.APPLICATION_XML) 
	   public String getNewShipmentId(){ 
	      return awDao.getNewShipmentId();
	   } 
	   
	   @GET 
	   @Path("/allshipments") 
	   @Produces(MediaType.APPLICATION_XML) 
	   public Collection<ShipmentBean> getAllShipments(){ 
	      return awDao.getAllShipments();
	   }
	   
	   @GET 
	   @Path("/shipmentdetails") 
	   @Produces(MediaType.APPLICATION_XML) 
	   public Collection<ShipmentBean> getShipmentDetails(){ 
	      return awDao.getShipmentDetails();
	   }
	   
	   @GET 
	   @Path("/shipmentlocations") 
	   @Produces(MediaType.APPLICATION_XML) 
	   public Collection<ShipmentLocations> getShipmentLocations(){ 
	      return awDao.getShipmentLocations();
	   } 
	   
	   
	   @GET 
	   @Path("/shipmenttags") 
	   @Produces(MediaType.APPLICATION_XML) 
	   public Collection<ShipmentTags> getShipmentTags(){ 
	      return awDao.getShipmentTags();
	   } 
	   
	   @GET 
	   @Path("/shipmentproducts") 
	   @Produces(MediaType.APPLICATION_XML) 
	   public Collection<ShipmentProducts> getShipmentProducts(){ 
	      return awDao.getShipmentProducts();
	   } 
	   
	   @GET 
	   @Path("/shipmentusers") 
	   @Produces(MediaType.APPLICATION_XML) 
	   public Collection<ShipmentUsers> getShipmentUsers(){ 
	      return awDao.getShipmentUsers();
	   } 
	   
	   @PUT
	   @Path("/step1insert/{shipmentid}/{source}/{destination}/{etd}/{eta}/{invoicenumber}")
	   @Produces(MediaType.APPLICATION_XML)
	   @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	   public String insertShipmentStep1Data(@FormParam("shipmentid") String sShipmentId,
			   @FormParam("source") String sSource,@FormParam("destination") String sDestination,
			   @FormParam("etd") String sETD,@FormParam("eta") String sETA, @FormParam("invoicenumber") String sInvoiceNumber) {
		   return awDao.insertShipmentStep1Data(sShipmentId, sSource, sDestination, sETD, sETA, sInvoiceNumber);
	   }
	   
	   @POST
	   @Path("/step1update/{shipmentid}/{source}/{destination}/{etd}/{eta}/{invoicenumber}")
	   @Produces(MediaType.APPLICATION_XML)
	   @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	   public String updateShipmentStep1Data(@FormParam("shipmentid") String sShipmentId,
			   @FormParam("source") String sSource,@FormParam("destination") String sDestination,
			   @FormParam("etd") String sETD,@FormParam("eta") String sETA, @FormParam("invoicenumber") String sInvoiceNumber) {
		   return awDao.updateShipmentStep1Data(sShipmentId, sSource, sDestination, sETD, sETA, sInvoiceNumber);
	   }
	   
	   @PUT
	   @Path("/step2insert/{shipmentid}/{source}/{destination}/{etd}/{eta}/{mode}/{carrier}")
	   @Produces(MediaType.APPLICATION_XML)
	   @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	   public String insertShipmentStep2Data(@FormParam("shipmentid") String sShipmentId,
			   @FormParam("source") String sSource,@FormParam("destination") String sDestination,
			   @FormParam("etd") String sETD,@FormParam("eta") String sETA, @FormParam("mode") String sMode, @FormParam("carrier") String sCarrier) {
		   return awDao.insertShipment2Data(sShipmentId, sSource, sDestination, sETD, sETA, sMode, sCarrier);
	   }
	   
	   @POST
	   @Path("/step2update/{shipmentid}/{source}/{destination}/{etd}/{eta}/{mode}/{carrier}")
	   @Produces(MediaType.APPLICATION_XML)
	   @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	   public String updateShipmentStep2Data(@FormParam("shipmentid") String sShipmentId,
			   @FormParam("source") String sSource,@FormParam("destination") String sDestination,
			   @FormParam("etd") String sETD,@FormParam("eta") String sETA, @FormParam("mode") String sMode, @FormParam("carrier") String sCarrier) {
		   return awDao.updateShipmentStep2Data(sShipmentId, sSource, sDestination, sETD, sETA, sMode, sCarrier);
	   }
	   
	   @POST
	   @Path("/step3update/{shipmentid}/{boxid}/{tagid}/{productid}/{value}/{criticality}")
	   @Produces(MediaType.APPLICATION_XML)
	   @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	   public String updateShipmentStep2Data(@FormParam("shipmentid") String sShipmentId,
			   @FormParam("boxid") String sBoxId,@FormParam("tagid") String sTagId,
			   @FormParam("productid") String sProductId, @FormParam("value") String sValue, @FormParam("criticality") String sCriticality) {
		   return awDao.updateShipmentStep3Data(sShipmentId, sBoxId, sTagId, sProductId, sValue, sCriticality);
	   }
	   
	   @POST
	   @Path("/updateleg/{shipmentid}/{source}/{destination}/{etd}/{eta}/{mode}/{carrier}/{legid}")
	   @Produces(MediaType.APPLICATION_XML)
	   @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	   public String updateLeg(@FormParam("shipmentid") String sShipmentId,
			   @FormParam("source") String sSource,@FormParam("destination") String sDestination,
			   @FormParam("etd") String sETD,@FormParam("eta") String sETA, @FormParam("mode") String sMode, 
			   @FormParam("carrier") String sCarrier, @FormParam("legid") int iLegId) {
		   return awDao.updateLeg(sShipmentId, sSource, sDestination, sETD, sETA, sMode, sCarrier, iLegId);
	   }
	   
	   @PUT
	   @Path("/insertleg/{shipmentid}/{source}/{destination}/{etd}/{eta}/{mode}/{carrier}/{legid}")
	   @Produces(MediaType.APPLICATION_XML)
	   @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	   public String legInsert(@FormParam("shipmentid") String sShipmentId,
			   @FormParam("source") String sSource,@FormParam("destination") String sDestination,
			   @FormParam("etd") String sETD,@FormParam("eta") String sETA, @FormParam("mode") String sMode, 
			   @FormParam("carrier") String sCarrier, @FormParam("legid") int sLegId) {
		   return awDao.insertLeg(sShipmentId, sSource, sDestination, sETD, sETA, sMode, sCarrier, sLegId);
	   }
	   
	   @PUT 
	   @Path("/step3insert/{shipmentid}/{boxid}/{tagid}/{productid}/{value}/{criticality}") 
	   @Produces(MediaType.APPLICATION_XML)
	   @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	   public String insertShipmentBox(@FormParam("shipmentid") String sShipmentId, @FormParam("boxid") String sBoxId, 
			   @FormParam("tagid") String sTagId, @FormParam("productid") String sProductId, 
			   @FormParam("value") String sValue, @FormParam("criticality") String sCriticality){ 
	      return awDao.insertShipmentStep3Data(sShipmentId, sBoxId, sTagId, sProductId, sValue, sCriticality);
	   } 
	   
	   @PUT 
	   @Path("/insertbox/{shipmentid}/{boxid}/{tagid}/{productid}/{value}/{criticality}") 
	   @Produces(MediaType.APPLICATION_XML)
	   @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	   public String insertBox(@FormParam("shipmentid") String sShipmentId, @FormParam("boxid") String sBoxId, 
			   @FormParam("tagid") String sTagId, @FormParam("productid") String sProductId, 
			   @FormParam("value") String sValue, @FormParam("criticality") String sCriticality){ 
	      return awDao.insertBox(sShipmentId, sBoxId, sTagId, sProductId, sValue, sCriticality);
	   } 
	   
	   @DELETE 
	   @Path("/deleteshipment/{shipmentid}") 
	   @Produces(MediaType.APPLICATION_XML) 
	   public String deleteShipment(@PathParam("shipmentid") String sShipmentId){ 
	      return awDao.deleteShipment(sShipmentId);
	   } 
	   
	   @DELETE 
	   @Path("/deleteleg/{shipmentid}/{legid}") 
	   @Produces(MediaType.APPLICATION_XML) 
	   public String deleteLeg(@PathParam("shipmentid") String sShipmentId, @PathParam("legid") int iLegId){ 
	      return awDao.deleteLeg(sShipmentId, iLegId);
	   } 
	   
	   @POST 
	   @Path("/endtrip/{shipmentid}/{endtime}") 
	   @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	   public String endTrip(@PathParam("shipmentid") String sShipmentId, @PathParam("endtime") String sEndTime){ 
	      return awDao.endTrip(sShipmentId, sEndTime);
	   } 
	   
	   @POST 
	   @Path("/userassign/{shipmentid}/{users}") 
	   @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	   public String userAssignment(@PathParam("shipmentid") String sShipmentId, @PathParam("users") String sUsers){ 
	      return awDao.userAssignment(sShipmentId, sUsers);
	   } 
	   
	   @DELETE 
	   @Path("/deletebox/{shipmentid}") 
	   @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	   public String userAssignment(@PathParam("shipmentid") String sShipmentId){ 
	      return awDao.deleteBoxForShipment(sShipmentId);
	   } 
	   
	   @GET 
	   @Path("/temperatureshipment/{shipmentid}/{boxid}") 
	   @Produces(MediaType.APPLICATION_XML) 
	   public Collection<TemperatureBean> getTemperatureDataForShipment(@PathParam("shipmentid") String sShipmentId,
			   @PathParam("boxid") String sBoxId){ 
	      return awDao.getTemperatureDataForShipment(sShipmentId, sBoxId);
	   } 
	   
	   @GET 
	   @Path("/shipmentetagdetail/{shipmentid}") 
	   @Produces(MediaType.APPLICATION_XML) 
	   public Collection<ShipmentBoxInfo> selectShipmentDetailsForETag(@PathParam("shipmentid") String sShipmentId){ 
	      return awDao.selectShipmentDetailsForETag(sShipmentId);
	   } 
	   
	   @GET 
	   @Path("/shipmentboxdetail/{shipmentid}") 
	   @Produces(MediaType.APPLICATION_XML) 
	   public Collection<ShipmentBoxInfo> selectShipmentBoxDetails(@PathParam("shipmentid") String sShipmentId){ 
	      return awDao.selectShipmentBoxDetails(sShipmentId);
	   } 
	   
	   @GET 
	   @Path("/shipmenttransitinfo/{shipmentid}") 
	   @Produces(MediaType.APPLICATION_XML) 
	   public Collection<ShipmentTransitInfo> selectShipmentTransitDetails(@PathParam("shipmentid") String sShipmentId){ 
	      return awDao.selectShipmentTransitDetails(sShipmentId);
	   } 
	}
