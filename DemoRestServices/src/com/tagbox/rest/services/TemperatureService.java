package com.tagbox.rest.services;

import java.util.Collection;

import javax.ws.rs.GET; 
import javax.ws.rs.Path; 
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces; 
import javax.ws.rs.core.MediaType;  

import com.tagbox.rest.bean.HumidityBean;
import com.tagbox.rest.bean.TemperatureBean;
import com.tagbox.rest.bean.TemperatureTriggersBean;
import com.tagbox.rest.dao.HumidityDao;
import com.tagbox.rest.dao.TemperatureDao;
	@Path("/TemperatureService") 

	public class TemperatureService {

	  
		TemperatureDao awDao = new TemperatureDao(); 
	   
	
		@GET 
	   @Path("/temperature") 
	   @Produces(MediaType.APPLICATION_XML) 
	   public Collection<TemperatureBean> getTemperatureData(){ 
	      return awDao.getTemperatureData();
	   } 
		
		@GET 
		@Path("/thresholds") 
		@Produces(MediaType.APPLICATION_XML) 
		public Collection<TemperatureTriggersBean> getTemperatureThresholds(){ 
		  return awDao.getTemperatureThresholds();
		} 
	   
	   @GET 
	   @Path("/vibration") 
	   @Produces(MediaType.APPLICATION_XML) 
	   public Collection<TemperatureBean> getVibrationData(){ 
	      return awDao.getVibrationData();
	   } 
	   
	   @GET 
	   @Path("/averagetemperature") 
	   @Produces(MediaType.APPLICATION_XML) 
	   public Collection<TemperatureBean> getTemperatureAverageData(){ 
	      return awDao.getTemperatureAverageData();
	   }
	   
	   @GET 
	   @Path("/temperatureweek") 
	   @Produces(MediaType.APPLICATION_XML) 
	   public Collection<TemperatureBean> getTemperatureDataForWeek(){ 
	      return awDao.getTemperatureDataForWeek();
	   } 
	   
	   @GET 
	   @Path("/zoneaveragetemperature/{starttime}/{endtime}") 
	   @Produces(MediaType.APPLICATION_XML) 
	   public Collection<TemperatureBean> getTemperatureDataForAllSubzones(@PathParam("starttime") String sStartTime,
			   @PathParam("endtime") String sEndTime){ 
	      return awDao.getTemperatureDataForAllSubzones(sStartTime, sEndTime);
	   } 
	}
