package com.tagbox.rest.services;

import java.util.Collection;

import javax.ws.rs.GET; 
import javax.ws.rs.Path; 
import javax.ws.rs.Produces; 
import javax.ws.rs.core.MediaType;  

import com.tagbox.rest.bean.AllVehicleDataBean;
import com.tagbox.rest.dao.TransitDao;
	@Path("/TransitService") 

	public class TransitService {

	  
		TransitDao clmDao = new TransitDao(); 
	   private static final String SUCCESS_RESULT = "<result>success</result>"; 
	   private static final String FAILURE_RESULT = "<result>failure</result>";  
	 
	   @GET 
	   @Path("/allvehicles") 
	   @Produces(MediaType.APPLICATION_XML) 
	   public Collection<AllVehicleDataBean> getAllVehiclesData(){ 
	      return clmDao.getAllVehiclesData();
	   }  
}
