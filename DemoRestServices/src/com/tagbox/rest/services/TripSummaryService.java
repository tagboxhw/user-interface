package com.tagbox.rest.services;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.tagbox.rest.bean.ParameterSummaryBean;
import com.tagbox.rest.bean.ShipmentDetailsBean;
import com.tagbox.rest.bean.ETagSummaryBean;
import com.tagbox.rest.dao.TripSummaryDao;

@Path("/TripSummaryService") 
public class TripSummaryService {

	TripSummaryDao tvmDao = new TripSummaryDao(); 
   private static final String SUCCESS_RESULT = "<result>success</result>"; 
   private static final String FAILURE_RESULT = "<result>failure</result>";  
   @GET 
   @Path("/tripsummary") 
   @Produces(MediaType.APPLICATION_XML) 
   public Collection<ShipmentDetailsBean> selectAllShipmentDetails(){ 
      return tvmDao.selectAllShipmentDetails(); 
   } 
   
   @GET 
   @Path("/etagsummary/{shipmentid}") 
   @Produces(MediaType.APPLICATION_XML) 
   public Collection<ETagSummaryBean> selectETagSummary(@PathParam("shipmentid") String sShipmentId){ 
	   return tvmDao.selectETagSummary(sShipmentId); 
   }
   
}
