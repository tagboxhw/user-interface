package com.tagbox.rest.services;

import java.util.Collection;

import javax.ws.rs.GET; 
import javax.ws.rs.Path; 
import javax.ws.rs.Produces; 
import javax.ws.rs.core.MediaType;  

import com.tagbox.rest.bean.DoorActivityBean;
import com.tagbox.rest.dao.DoorActivityDao;
	@Path("/DoorActivityService") 

	public class DoorActivityService {

	  
		DoorActivityDao awDao = new DoorActivityDao(); 
	   private static final String SUCCESS_RESULT = "<result>success</result>"; 
	   private static final String FAILURE_RESULT = "<result>failure</result>";  
	   @GET 
	   @Path("/dooractivity") 
	   @Produces(MediaType.APPLICATION_XML) 
	   public Collection<DoorActivityBean> getDoorActivityData(){ 
	      return awDao.getDoorActivityData();
	   } 
	}
