package com.tagbox.rest.services;

import java.util.Collection;

import javax.ws.rs.GET; 
import javax.ws.rs.Path; 
import javax.ws.rs.Produces; 
import javax.ws.rs.core.MediaType;  

import com.tagbox.rest.bean.AllLocationDataBean;
import com.tagbox.rest.bean.ClientLocationMapBean;
import com.tagbox.rest.bean.ClientSubLocationMapBean;
import com.tagbox.rest.dao.ClientLocationMapDao;
	@Path("/ClientLocationMapService") 

	public class ClientLocationMapService {

	  
		ClientLocationMapDao clmDao = new ClientLocationMapDao(); 
	  
	   @GET 
	   @Path("/locationmap") 
	   @Produces(MediaType.APPLICATION_XML) 
	   public Collection<ClientLocationMapBean> getLocations(){ 
	      return clmDao.selectLocations();
	   }  
	   
	   @GET 
	   @Path("/alllocations") 
	   @Produces(MediaType.APPLICATION_XML) 
	   public Collection<AllLocationDataBean> getAllLocationsData(){ 
	      return clmDao.getAllLocationsData();
	   } 
	   
	   @GET 
	   @Path("/subzones") 
	   @Produces(MediaType.APPLICATION_XML) 
	   public Collection<ClientSubLocationMapBean> selectSubZones(){ 
	      return clmDao.selectSubZones();
	   } 
}
