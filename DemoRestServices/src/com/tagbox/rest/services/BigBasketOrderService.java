package com.tagbox.rest.services;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.tagbox.rest.dao.BigBasketOrderDao;;

@Path("/OrderService") 
public class BigBasketOrderService {
	BigBasketOrderDao awDao = new BigBasketOrderDao(); 
	
	@GET 
	   @Path("/allorders") 
	   @Produces(MediaType.APPLICATION_XML) 
	   public void getAllOrders(){ 
	      awDao.getAllOrders();
	   } 
}
