package com.tagbox.rest.services;

import java.util.Collection;

import javax.ws.rs.GET; 
import javax.ws.rs.Path; 
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces; 
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;  

import com.tagbox.rest.bean.AlertSummaryBean;
import com.tagbox.rest.dao.AlertSummaryDao;

	@Path("/AlertSummaryService") 

	public class AlertSummaryService {

	  
		AlertSummaryDao awDao = new AlertSummaryDao(); 
	   private static final String SUCCESS_RESULT = "<result>success</result>"; 
	   private static final String FAILURE_RESULT = "<result>failure</result>";  
	   
	   
	   @GET 
	   @Path("/alertssummary/{starttime}/{endtime}") 
	   @Produces(MediaType.APPLICATION_XML) 
	   public Collection<AlertSummaryBean> getAlertSummaryData(@PathParam("starttime") String sStartTime,
			   @PathParam("endtime") String sEndTime){ 
	      return awDao.getAlertSummaryData(sStartTime, sEndTime);
	   } 
	   
	}
