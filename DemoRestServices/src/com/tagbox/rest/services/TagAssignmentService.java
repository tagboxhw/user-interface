package com.tagbox.rest.services;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.tagbox.rest.bean.ParameterSummaryBean;
import com.tagbox.rest.bean.TagAssignmentBean;
import com.tagbox.rest.dao.TagAssignmentDao;

@Path("/TagAssignmentService") 
public class TagAssignmentService {

	TagAssignmentDao tvmDao = new TagAssignmentDao(); 
	
   @GET 
   @Path("/tagassignment") 
   @Produces(MediaType.APPLICATION_XML) 
   public Collection<TagAssignmentBean> selectAllTagAssignmentRecords(){ 
      return tvmDao.selectAllTagAssignmentRecords(); 
   }  
   
//   
//   @POST 
//   @Path("/tagassignment")  
//   @Produces(MediaType.APPLICATION_XML)
//   @Consumes(MediaType.APPLICATION_FORM_URLENCODED) 
//   public int updateUser(@FormParam("VehicleID") String sVehicleId, 
//      @FormParam("Source") String sSource, 
//      @FormParam("Destination") String sDestination,
//      @FormParam("DriverId") String sDriverId,
//      @Context HttpServletResponse servletResponse) throws IOException{ 
//	   return tvmDao.updateVehicleDetails(sVehicleId, sSource, sDestination, sDriverId); 
//   }  
   
}
