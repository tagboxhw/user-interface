package com.tagbox.rest.util;
// Use the JDBC driver  
    import java.sql.*;  

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
      
        public class DbConnection {  
        	static final Logger logger = LoggerFactory.getLogger(DbConnection.class);
        	public static Connection connection = null;
        	
    
            // Connect to your database.  
            // Replace server name, username, and password with your credentials  
            public static Connection getConnection() {
            	try {
            		
            		/*
            	Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
                String connectionString =  
                    "jdbc:sqlserver://tbox-dbsrv-1.database.windows.net:1433;" 
                    + "database=tbox_db_dev;"  
                    + "user=tbox_db1_admin@tbox-dbsrv-1;"  
                    + "password=gatxob_321;"  
                    + "encrypt=false;"  
                    + "trustServerCertificate=false;"  
                    + "hostNameInCertificate=*.database.windows.net;"  
                    + "loginTimeout=30;";  
                // Declare the JDBC objects.  
                
                   */
            		
            		Class.forName("org.postgresql.Driver");
            		connection = DriverManager.getConnection(
            		         //"jdbc:postgresql://dbhost:port/dbname", "user", "dbpass");
            				
            				//STELLAR IP ADDRESS
            		       // "jdbc:postgresql://52.187.79.46:5432/somesh", "somesh", "Tagbox_123456");
            		
            				//BIOCON
            				//"jdbc:postgresql://52.187.24.249:5432/somesh", "somesh", "Tagbox_123456"); //NEW
            		         
            		//COLDMAN IP ADDRESS
            		//"jdbc:postgresql://52.187.79.46:5432/somesh", "somesh", "Tagbox_123456");
            				
            		//LICIOUS IP ADDRESS
            		"jdbc:postgresql://52.187.22.72:5432/somesh", "somesh", "Tagbox_123456");
            		
            		//DEV or UNILEVER or DEMO1 or DEMO2 IP ADDRESS
            		//"jdbc:postgresql://104.215.248.40:5432/somesh", "somesh", "Tagbox_123456");
            		
            		//BIGBASKET IP ADDRESS
            		//"jdbc:postgresql://52.187.26.28:5432/bigbasket", "bigbasket", "Bigbasket_123456");
            		
                       // connection = DriverManager.getConnection(connectionString);  
          
                        // Create and execute a SELECT SQL statement.  
                       /* String selectSql = "SELECT TOP 10 * from uni_master_events";  
                        statement = connection.createStatement();  
                        resultSet = statement.executeQuery(selectSql);  
          
                        // Print results from select statement  
                        while (resultSet.next())   
                        {  
                            System.out.println(this.getClass().getName() + resultSet.getString(2) + " "  
                                + resultSet.getString(3));  
                        }  */
            		
            		  if (connection != null) {
            	 			System.out.println("You made it!");
            	 		} else {
            	 			System.out.println("Failed to make connection!");
            	 		}
                    }  
                    catch (Exception e) {  
                        e.printStackTrace();  
                    }  
                    finally {  
                        // Close the connections after the data has been handled.  
                         
                    }  
                //}  

             return connection;
          }
  public static void closeConnection()
  {
      try
          {
              if(connection != null) {
            	  connection.close();
            	  connection = null;
              }

          } catch (Exception e)
          {
              logger.error(e.getLocalizedMessage());
              e.printStackTrace();
          } finally {
          	if(connection != null) {
          		connection = null;
              }

          }

  }
        } 