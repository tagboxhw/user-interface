package com.tagbox.rest.util;

/**
 
 * @author kkumbaji
 * @version 1.0
 */
public class Constants {
	public static final String ALL_ALERTS = "ALL";
	public static final String TEMPERATURE = "Temperature";
    public static final String HUMIDITY = "Humidity";
    public static final String DOOR_ACTIVITY = "Door Activity";
    public static final String ENERGY = "Energy";
    public static final String DETOUR = "Detour";
    public static final String STOPPAGE = "Stoppage";
    public static final String POWER_CONSUMPTION = "Power Consumption";
    public static final String[][] TIME_FILTER = 
            { {"15", "Last 15 Minutes"}, {"30", "Last 30 Minutes"},
    		  {"60", "Last One Hour"}, {"12", "Last 12 Hours"},
    		  {"24", "Today"}, {"7", "This Week"}, {"31", "This Month"}, {"3", "This Quarter"}
            };
        
    public static final String THRESHOLD_TEMPERTURE_MIN = "-23";
    public static final String THRESHOLD_TEMPERTURE_MAX = "40";
    public static final String ENABLE = "enable";
    public static final String DISABLE = "disable";
    
    public static final String COLOR_RED = "Red";
    public static final String COLOR_GREEN = "Green";
    public static final String COLOR_ORANGE = "Orange";
    public static final String COLOR_BLACK = "Black";
    
    public static final String AMBIENT_NODE = "Ambient";
    public static final String POWER_NODE = "Power";
    public static final String DOOR_NODE = "Door";
    
    public static final String LONG_HAUL = "Long Haul";
    public static final String SHORT_HAUL = "Short Haul";
    public static final String LOCAL_RUN = "Local Run";
    public static final String LOCAL_RUNS_DETAIL = "LocalRunsDetail";
    public static final String NO_SUB_LOCATION = "nothing";
    public static final String ERROR = "ERROR";
    public static final String SUCCESS = "SUCCESS"; 
    
    public static String BIG_BASKET_REST_SERVICE_URL = "http://portal.tagbox.in/demorest/";
}
