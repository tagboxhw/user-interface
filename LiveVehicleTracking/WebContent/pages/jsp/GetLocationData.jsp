<%@ page errorPage="Error.jsp" %>
<%@ page import="com.live.action.AlertAction"%>
<%@ page import="java.util.TreeMap"%>
<%@ page import="java.util.Vector"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.util.Collection"%>
<%@ page import="java.util.Date"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Calendar"%>
<%@ page import="java.util.GregorianCalendar"%>
<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>

<!-- <?xml version="1.0" encoding="ISO-8859-1"?> -->

<%!static final Logger logger = LoggerFactory.getLogger("GetLocationData");
	AlertAction alertAction = new AlertAction();
	String buf = "";
	String sString = "";
	String sErrorMessage = null;
	String sId = null;
	String sRowId = null;
	String sSubLocId = null;
	String temperature = "", temperature1 = "", temperature2 = "",
			temperature3 = "", time = "", time1 = "", time2 = "", time3 = "";
	TreeMap tm = new TreeMap();
	List<String> al = new ArrayList<String>();%>
<%
	try {
		sString = request.getParameter("query");
		if (sString.equals("Action")) {
			String sAlertId = request.getParameter("alertId");
			String sAction = request.getParameter("action");
			//logger.info("AlertId: " + sAlertId + " Action: " + sAction);
			buf = "";
			//response.setContentType("text/xml");
			//buf += ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			alertAction.updateAction(sAction, sAlertId);

			buf = "<rows>";
			buf += ("<row>");
			buf += ("<Done>Success</Done>");
			buf += ("</row>");
			buf += "</rows>";
			buf = buf.trim();
			//logger.info(buf);
			out.clear();
			out.println(buf);
		} else if (sString.equals("RootCause")) {
			String sAlertId = request.getParameter("alertId");
			String sRoot = request.getParameter("root");
			String sPreventive = request.getParameter("preventive");
			//logger.info("AlertId: " + sAlertId + " root: " + sRoot + " pre: " + sPreventive);
			buf = "";
			//response.setContentType("text/xml");
			//buf += ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			alertAction.updateRoot(sRoot, sPreventive, sAlertId);
			buf = "<rows>";

			buf += ("<row>");
			buf += ("<Done>Success</Done>");
			buf += ("</row>");

			buf += "</rows>";
			buf = buf.trim();
			//logger.info(buf);
			out.clear();
			out.println(buf);
		}
	} catch (Exception e) {
		logger.error("Exception: " + e.getLocalizedMessage());
		e.printStackTrace();
	}
%>