<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page errorPage="Error.jsp" %>

<%@ page import="java.util.TreeMap"%>
<%@ page import="java.util.LinkedHashMap"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.util.Collection"%>
<%@ page import="java.util.LinkedHashMap"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="java.util.Date"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Calendar"%>
<%@ page import="java.util.GregorianCalendar"%>
<%@ page import="java.util.Set"%>
<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>
<%@ page import="com.live.dao.TransitDao"%>
<%@ page import="com.live.bean.TransitDriverMapBean"%>
<%@ page import="com.live.bean.TransitVehicleMapBean"%>
<%@ page import="com.live.bean.AllVehicleDataBean"%>
<%@ page import="com.live.bean.AlertWorkflowBean"%>
<%@ page import="com.live.dao.AlertsWorkflowDao"%>
<%@ page import="com.live.dao.TemperatureDao"%>
<%@ page import="com.live.dao.HumidityDao"%>
<%@ page import="com.live.bean.TemperatureBean"%>
<%@ page import="com.live.bean.HumidityBean"%>
<%@ page import="com.live.dao.TransitDao"%>
<%@ page import="com.live.util.Constants"%>
<%@ page import="com.live.dao.TransitVehicleMapDao"%>

<%!
static final Logger logger = LoggerFactory.getLogger("LiveVehicleTracking");
%>
<%
String sTimeValue = "";
String sAlertType = "";
int iColSize = 0;
String sNodeId = "";
String sUsername = (String) session.getAttribute("username");
if(sUsername == null || sUsername.equals("")) {
	session.setAttribute("callingPage", "LiveVehicleTracking.jsp");
	response.sendRedirect("../../index.html");
}
java.util.Date dt = new java.util.Date();
java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd-MM-yyyy HH:mm");
TransitDao trDao = new TransitDao();
AllVehicleDataBean alBean1 = new AllVehicleDataBean();
String sVehicleId = request.getParameter("VehicleId");
if(sVehicleId == null || sVehicleId.equals("")) sVehicleId = "KA03-DG1989";
if(trDao.colAllVehicles == null || trDao.colAllVehicles.size() == 0) trDao.getAllVehiclesData();
for(AllVehicleDataBean alBean: trDao.colAllVehicles){
	if(sVehicleId.equals(alBean.getVehicle_Id())) {
		alBean1 = alBean;
		break;
	}
}
TransitVehicleMapDao tvmDao = new TransitVehicleMapDao();
TransitVehicleMapBean tvBean1 = new TransitVehicleMapBean();
Collection<TransitVehicleMapBean> col1 = tvmDao.selectVehicles();
for(TransitVehicleMapBean tvBean: col1){
	if(sVehicleId.equals(tvBean.getVehicle_ID())) {
		tvBean1 = tvBean;
		break;
	}
}
SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
Date d = null;
Date d2 = null;
Long newTime = null;
TransitVehicleMapBean tv = new TransitVehicleMapBean();
TransitDriverMapBean driver = new TransitDriverMapBean();

	sAlertType = "";
	sTimeValue = "";
	sNodeId = "";
	if(sAlertType == null) sAlertType = "ALL";
	sTimeValue = "60";
	
	AlertsWorkflowDao alertsWorkflowDao = new AlertsWorkflowDao();
	java.text.SimpleDateFormat sdf1 = 
	     new java.text.SimpleDateFormat("dd-MM-yyyy HH:mm");
	sdf1.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
	String currentTime = sdf1.format(dt);
	

	int iTemperatureVehCount = 0, iHumidityVehCount = 0, iDoorOpenVehCount = 0, iStoppageVehCount = 0;
	LinkedHashMap lhm = alertsWorkflowDao.getAllAlertsForVehicleId(sVehicleId, sAlertType, sTimeValue);
	Iterator iter = lhm.keySet().iterator();
	AlertWorkflowBean ab = new AlertWorkflowBean();
	String sKey = "";
	if(lhm.size() != 0) {
		while(iter.hasNext()){
			sKey = (String) iter.next();
			ab = (AlertWorkflowBean) lhm.get(sKey);
			if(ab.getAlert_Type().equals(Constants.TEMPERATURE)) iTemperatureVehCount++;
			else if(ab.getAlert_Type().equals(Constants.HUMIDITY)) iHumidityVehCount++;
			else if(ab.getAlert_Type().equals(Constants.DOOR_ACTIVITY)) iDoorOpenVehCount++;
			else if(ab.getAlert_Type().equals(Constants.STOPPAGE)) iStoppageVehCount++;
		}
	}
	
	SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	SimpleDateFormat df3 = new SimpleDateFormat("dd-MMM HH:mm");
	df2.setTimeZone(java.util.TimeZone.getTimeZone("UTC"));
	df3.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
	sNodeId = request.getParameter("Node");
	if(sNodeId == null || sNodeId.equals("")) sNodeId = "SZ-SY-DMO-000042";
	String temperature = "", temperature1 = "", temperature2 = "", temperature3 = "";
	String time = "", time1 = "", time2 = "", time3 = "";	
	HumidityDao hd = new HumidityDao();
	HumidityBean hdBean = new HumidityBean();
	TemperatureDao td = new TemperatureDao();
	TemperatureBean tdBean = new TemperatureBean();
	Collection col = td.getTemperatureData(sNodeId);
	Iterator iterSt = col.iterator();
	iColSize = 0;
	if(col.size() < 20) {
		iColSize = 1;
	} else if(col.size() < 100) {
		iColSize = 5;
	} else if(col.size() < 5000) {
		iColSize = 100;
	} else if(col.size() >= 5000) {
		iColSize = 1000;
	}
	int iSize = 0;
	while(iterSt.hasNext()) {
		tdBean = (TemperatureBean) iterSt.next();
		
		if(iColSize != 0 && iSize%iColSize == 0){
			d = df2.parse(tdBean.getTimestamp());
			temperature += "\"" + tdBean.getTemperature() + "\", ";
			time += "\"" + df3.format(d) + "\", ";
		} else {
			if(iSize % 100 == 0){
				d = df2.parse(tdBean.getTimestamp());
				temperature += "\"" + tdBean.getTemperature() + "\", ";
				time += "\"\", ";
			}
		//	} else{
		//		temperature += "\"" + tdBean.getTemperature() + "\", ";
		//		time += "\"\", ";
		//	}
		}
		iSize++;
	}
	if(temperature.equals("") || time.equals("")){
		temperature1 = "00";
		time1 = "00";
	} else {
		if(temperature.length() >= 2)	temperature1 = temperature.substring(0, (temperature.length() - 2));
			if(time.length() >= 2) time1 = time.substring(0, (time.length() - 2));
	}
		temperature = ""; time = "";
		//Enter GW_ClientId and ND_Client Id for Chart 2
		col = hd.getHumidityData(sNodeId);
		Iterator iterSh = col.iterator();
		if(col.size() < 20) {
			iColSize = 1;
		} else if(col.size() < 100) {
			iColSize = 5;
		} else if(col.size() < 5000) {
			iColSize = 100;
		} else if(col.size() >= 5000) {
			iColSize = 1000;
		}
		iSize = 0;
		while(iterSh.hasNext()) {
			hdBean = (HumidityBean) iterSh.next();
			if(iColSize != 0 && iSize%iColSize == 0){
				d = df2.parse(hdBean.getTimestamp());
				temperature += "\"" + hdBean.getHumidity() + "\", ";
				time += "\"" + df3.format(d) + "\", ";
			} else {
				if(iSize % 100 == 0){
					d = df2.parse(hdBean.getTimestamp());
					temperature += "\"" + hdBean.getHumidity() + "\", ";
					time += "\"\", ";
				}
			//	 else{
			//		temperature += "\"" + hdBean.getHumidity() + "\", ";
			//		time += "\"\", ";
			//	}
			}
			iSize++;
		}
		if(temperature.equals("") || time.equals("")){
			temperature2 = "00";
			time2 = "00";
		} else {
			if(temperature.length() >= 2)	temperature2 = temperature.substring(0, (temperature.length() - 2));
				if(time.length() >= 2) time2 = time.substring(0, (time.length() - 2));
		}
		
		temperature = ""; time = "";
		col = td.getVibrationData(sNodeId);
		iterSt = col.iterator();
		if(col.size() < 20) {
			iColSize = 1;
		} else if(col.size() < 100) {
			iColSize = 5;
		} else if(col.size() < 5000) {
			iColSize = 100;
		} else if(col.size() >= 5000) {
			iColSize = 1000;
		}
		iSize = 0;
		while(iterSt.hasNext()) {
			tdBean = (TemperatureBean) iterSt.next();
			
			if(iColSize != 0 && iSize%iColSize == 0){
				d = df2.parse(tdBean.getTimestamp());
				temperature += "\"" + tdBean.getTemperature() + "\", ";
				time += "\"" + df3.format(d) + "\", ";
			} else {
				if(iSize % 100 == 0){
					d = df2.parse(tdBean.getTimestamp());
					temperature += "\"" + tdBean.getTemperature() + "\", ";
					time += "\"\", ";
				}
			//	else{
			//		temperature += "\"" + tdBean.getTemperature() + "\", ";
			//		time += "\"\", ";
			//	}
			}
			iSize++;
		}
		if(temperature.equals("") || time.equals("")){
			temperature3 = "00";
			time3 = "00";
		} else {
			if(temperature.length() >= 2)	temperature3 = temperature.substring(0, (temperature.length() - 2));
				if(time.length() >= 2) time3 = time.substring(0, (time.length() - 2));
		}
		
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Live Vehicle Tracking</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Add these below two lines for autocomplete and the javascript function at the end  -->
<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="../../bootstrap/css/bootstrap.css">
   
<!-- Font Awesome -->
<script src="https://use.fontawesome.com/37ba1872d2.js"></script>
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../../dist/css/tagbox.css">

<script src="../../dist/js/tagbox.js"></script>
<script src="../../dist/js/location.js"></script>
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
<link href="https://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet">

<link rel="stylesheet" href="../../dist/css/bootstrap-switch.css">
<script src="../../dist/js/bootstrap-switch1.js"></script>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <!-- Add this for autocomplete and dropdown combo -->
<script
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD53TZnxuAsBNWiVjlw8VbXewqG58QXVnE"
	type="text/javascript"></script>
<script src="../../dist/js/mapLiveVehicleTrackingData.js"></script>
<script type="text/javascript">
	function submitTime() {
		window.location = "LiveVehicleTracking.jsp";
	}
	setInterval(submitTime, (30000));
</script>
<link rel="stylesheet" href="../../dist/css/mapAlert.css">
  <style>
  body {
	font-family:  Lato, sans-serif;
	font-weight: 600;
}
</style>
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
	<div class="wrapper">

		<header class="main-header">
			<!-- Logo -->
			<div>
				<a href="#" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
					<span class="logo-mini"><b>Demo</b></span> <!-- logo for regular state and mobile devices -->
					<span class="logo-lg"><b>Demo</b> </span>
				</a>
			</div>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">

				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
					role="button"> <span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
				</a>
				
				<form class="navbar-form pull-left" role="search">
					<div class="form-group">
						<b style="padding-left: 15px; font-size: 20px;color:#fff;" class="pull-right">Live Vehicle Tracking</b><br> <b style="padding-left: 15px;color:#fff;"><%= currentTime %></b>
					</div>
				</form>

				<div class="navbar-custom-menu">
					<img src="../../dist/img/Logo.png" class="logo-class pull-right"
						alt="User Image">
				</div>
			</nav>

		</header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->
				<br>

				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<ul class="sidebar-menu">
					<li class="header">MAIN NAVIGATION</li>
					<li class="treeview"><a href="#"> <i
							class="fa fa-home fa-lg"></i> <span>Dashboard</span>
					</a>
						<ul class="treeview-menu">
							<li class=""><a href="LiveVehicleTracking.jsp"><i
									class="fa fa-circle-o"></i> Home</a></li>
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<!-- Main content -->
			<section class="content">
				<div class="row">
					<input type="hidden" id="CatID" name="CatID"
						value="ALL"> <input type="hidden"
						id="VehicleId" name="VehicleId" value="<%=sVehicleId%>"> <input
						type="hidden" id="RouteType" name="RouteType"
						value="<%=alBean1.getRoute_Type()%>">
						 <input
						type="hidden" id="alertsFilter" name="alertsFilter"
						value="24">
					
				</div>
				<%System.out.println(alBean1.getRoute_Type()); %>
				<div class="row">
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-12">
								<div class="bg-teal">
									<h3 class="header-style">
										<b>&nbsp;<%=alBean1.getRoute_Type()%>: <%=sVehicleId%></b>
									</h3>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="well well-sm">
<%
if(tv.getAssigned_Date() == null) {
	tv.setAssigned_Date("07/07/2017 10:00");
}
d = df.parse(tv.getAssigned_Date());
newTime = d.getTime();
//newTime +=(330*60*1000);
d2 = new Date(newTime);
newTime +=(3*24*60*60*1000);

logger.info("tv.getSource(): " + tv.getSource());
	if(tv.getSource() == null) tv.setSource("LO-DMO-000001"); 
							driver = trDao.getDriverDetails(tvBean1.getDriver_DL_ID());
%>
									<div class="row">
										<div class="col-md-offset-2 col-md-3">
											<div class="form-group">
												 <div class="form-group">
												 <label style="width:50px">Start</label>&nbsp;&nbsp;<input
													class="btn btn-default" class="form-control-no-height"
													type="button" value="<%=sdf.format(d2)%>" style="width:140px">
												
											</div>
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
											<label style="width:50px">Source</label>&nbsp;&nbsp;<input
													class="btn btn-default" class="form-control-no-height"
													type="button" value="<%=alBean1.getSource_Location_Name()%>" style="width:140px">
												
											</div>
										</div>
										<div class="col-md-3">
											<%
											logger.info("tv.getSource(): " + tv.getSource());
												if(tv.getDestination() == null) tv.setDestination("LO-DMO-000005"); 
																		d2 = new Date(newTime);
																		
											%>
											<div class="form-group">
												<label style="width:90px">Destination</label>&nbsp;&nbsp;<input
													class="btn btn-default" class="form-control-no-height"
													type="button" value="<%=alBean1.getDest_Location_Name()%>" style="width:140px">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-offset-2 col-md-3">
											<div class="form-group">
											
												<label style="width:50px">Driver</label>&nbsp;&nbsp;<input
													class="btn btn-default" class="form-control-no-height"
													type="button" value="<%=driver.getDriver_Name()%>" style="width:140px">
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
											
												<label style="width:50px">Phone</label>&nbsp;&nbsp;<input
													class="btn btn-default" class="form-control-no-height"
													type="button" value="<%=driver.getDriver_Phone_Num()%>" style="width:140px">
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
													<label style="width:90px">Trip Time</label>&nbsp;&nbsp;<input
													class="btn btn-default" class="form-control-no-height"
													type="button" value="3 Days" style="width:140px">
													
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-offset-2 col-md-3">
											<div class="form-group">
												<label style="width:50px">Status</label>&nbsp;&nbsp;<span data-toggle="tooltip" class="badge bg-red">DELAYED</span>
													
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
											<label style="width:50px">ETA</label>&nbsp;&nbsp;<input
													class="btn btn-default" class="form-control-no-height"
													type="button" value="<%=sdf.format(d2) %>" style="width:140px">
												
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
											<label style="width:90px">Shipment Risk</label>&nbsp;&nbsp;<span data-toggle="tooltip" class="badge bg-red">HIGH</span>
												
											</div>
										</div>
									</div>
									
									<div class="row">
				<div class="col-md-12 text-center">
				<div class="well well-sm white-background">
				<div class="text-center">
				<div class="panel-coldroom text-center">
					<b>TOTAL NUMBER OF ALERTS THIS TRIP: <span class="font-big" style="color:black"><%= (iTemperatureVehCount+iHumidityVehCount+iDoorOpenVehCount+iStoppageVehCount) %></span></b>
					</div>
					</div><br>
					<div class="row">
						<div class="col-md-offset-1 col-md-10">
							
								<div class="row">
								<div class="col-md-3">
								TEMPERATURE
								<div class="row">
									<div class="col-md-offset-1 col-md-3">
									<img src="../../dist/img/temperature.png">
									</div>
									<div class="col-md-offset-1 col-md-7 text-left">
								<b class="font-new"><%= iTemperatureVehCount %></b>
									</div>
								</div>
								</div>
								<div class="col-md-3">
								HUMIDITY
								<div class="row">
									<div class="col-md-offset-1 col-md-3">
									<img src="../../dist/img/humidity.png">
									</div>
									<div class="col-md-offset-1 col-md-7 text-left">
								<b class="font-new text-left"><%= iHumidityVehCount %></b>
									</div>
								</div>
								</div>
							<div class="col-md-3">
								VEHICLE DOOR OPEN
								<div class="row">
									<div class="col-md-offset-1 col-md-3">
									<img src="../../dist/img/vehdooropen.png">
									</div>
									<div class="col-md-offset-1 col-md-7 text-left">
								<b class="font-new text-left"><%= iDoorOpenVehCount %></b>
									</div>
									
								</div>
								</div>
								<div class="col-md-3">
								VEHICLE STOPPAGE
								<div class="row">
									<div class="col-md-offset-1 col-md-3">
									<img src="../../dist/img/vehiclestop.png">
									</div>
									<div class="col-md-offset-1 col-md-6 text-left">
								<b class="font-new text-left"><%= iStoppageVehCount %></b>
									</div>
								
								</div>
								</div>
								</div>
					</div>
					</div>
					</div>
							
						</div>
					</div>
									<br>
									<div id="map2" style="height: 450px; border: 2px solid #3872ac;"></div>
									<br><br>
									
<div class="row">
									
				<div class="col-md-12 text-center white">
				<div class="well well-sm white-background">
				<b>TRENDS</b><br><br>
					<div class="row">
					<!-- <select class="form-control-no-height pull-center" style="width:50%">
  														<option>Last One Hour</option>
  														<option>Last 12 Hours</option>
  														<option>Today</option>
													</select>
													<br><br> -->
						<div class="col-md-4">
						
							<div class="box box-danger">
							<div class="box-header with-border">
														<h3 class="box-title">&nbsp;Temperature (&deg; C)</h3>
														<!-- <div class="box-tools pull-right">
															<button type="button" class="btn btn-box-tool"
																data-widget="collapse">
																<i class="fa fa-minus"></i>
															</button>
															<button type="button" class="btn btn-box-tool"
																data-widget="remove">
																<i class="fa fa-times"></i>
															</button>
														</div> -->
													</div>
													<div class="box-body">
														<%
															if(temperature1.equals("00")){
														%>
														No Data!
														<%
															} else {
														%>
														<div class="chart">
															<canvas id="lineChart1"
																style="height: 270px; width: 100px"></canvas>
														</div>
														<%
															}
														%>

													</div>
												</div>
							
							
						</div>
						<div class="col-md-4">
							<div class="box box-danger">
							<div class="box-header with-border">
														<h3 class="box-title">&nbsp;Relative Humidity (%)</h3>
														<!-- <div class="box-tools pull-right">
															<button type="button" class="btn btn-box-tool"
																data-widget="collapse">
																<i class="fa fa-minus"></i>
															</button>
															<button type="button" class="btn btn-box-tool"
																data-widget="remove">
																<i class="fa fa-times"></i>
															</button>
														</div> -->
													</div>
													<div class="box-body">
														<%
															if(temperature2.equals("00")){
														%>
														No Data!
														<%
															} else {
														%>
														<div class="chart">
															<canvas id="lineChart2"
																style="height: 270px; width: 100px"></canvas>
														</div>
														<%
															}
														%>

													</div>
												</div>
							
							
						</div>
						<div class="col-md-4">
						
							<div class="box box-danger">
							<div class="box-header with-border">
														<h3 class="box-title">&nbsp;Vibration (gs)</h3>
														<!-- <div class="box-tools pull-right">
															<button type="button" class="btn btn-box-tool"
																data-widget="collapse">
																<i class="fa fa-minus"></i>
															</button>
															<button type="button" class="btn btn-box-tool"
																data-widget="remove">
																<i class="fa fa-times"></i>
															</button>
														</div> -->
													</div>
													<div class="box-body">
														<%
															if(temperature3.equals("00")){
														%>
														No Data!
														<%
															} else {
														%>
														<div class="chart">
															<canvas id="lineChart3"
																style="height: 270px; width: 100px"></canvas>
														</div>
														<%
															}
														%>

													</div>
												</div>
							
							
						</div>
					</div>
					</div>
					</div>
					</div>
					<div class="row">
					<div class="col-md-12 text-center">
				<div class="well well-sm white-background">
				<b>TODAY'S ALERTS</b><br><br>
				<br>
				
				<!-- <select class="form-control-no-height pull-center" style="width:25%">
  														<option>All Alerts</option>
  														<option>Temperature Alerts</option>
  														<option>Humidity Alerts</option>
													</select> &nbsp;
													<select class="form-control-no-height pull-center" style="width:35%">
  														<option>Last One Hour</option>
  														<option>Last 12 Hours</option>
  														<option>Today</option>
													</select> &nbsp;
													<select class="form-control-no-height pull-center" style="width:35%">
  														<option>Open Status</option>
  														<option>Closed Status</option>
  														<option>In Progress Status</option>
													</select>
													<br><br>  -->
					<div class="row">
									<div class="col-md-12">
										<div class="panel-group" id="accordion" role="tablist"
											aria-multiselectable="true">
											<%
											
											
											if(lhm.size() == 0){%>
												No Alerts!
											<%}
												iter = lhm.keySet().iterator();
												ab = new AlertWorkflowBean();
												sKey = "";
												while(iter.hasNext()){
													sKey = (String) iter.next();
													ab = (AlertWorkflowBean) lhm.get(sKey);
													if(ab.getAlert_Type().equals(Constants.TEMPERATURE)){
											%>
											<div class="panel panel-warning">
												<div class="panel-heading" role="tab"
													id="<%=ab.getAlert_ID()%>">
													<h4 class="panel-title">
														<div class="row" class="collapsed" role="button"
															data-toggle="collapse" data-parent="#accordion"
															href="#header<%=ab.getAlert_ID()%>" aria-expanded="false"
															aria-controls="header<%=ab.getAlert_ID()%>">
															<%
																if(ab.getAlert_Severity().equals("4") || ab.getAlert_Severity().equals("3")){
															%>
															<div class="col-md-1 alert-text text-center">
																<i class="fa fa-thermometer-three-quarters"></i>
															</div>
															<div class="col-md-1 alert-text"><%=ab.getAlert_ID()%></div>
															<div class="col-md-5 alert-text">
																Temperature
																has breached
																<%=ab.getBreach_Type()%>
																limit for over
																<%=ab.getAlert_Duration()%>
																minutes
															</div>
															<div class="col-md-2 alert-text">Adarsh Kumar</div>
															<%
																} else {
															%>
															<div class="col-md-1 text-center">
																<i class="fa fa-thermometer-three-quarters"></i>
															</div>
															<div class="col-md-1"><%=ab.getAlert_ID()%></div>
															<div class="col-md-5">
																Temperature
																has breached
																<%=ab.getBreach_Type()%>
																 for over
																<%=ab.getAlert_Duration()%>
																minutes
															</div>
															<div class="col-md-2">Adarsh Kumar</div>
															<%
																}
															%>
															<div class="col-md-2 pull-right">
																<input id="btn<%=ab.getAlert_ID()%>" type="button"
																	class="btn btn-default"
																	value="<%=ab.getAction_Status()%>">
															</div>
														</div>
													</h4>
												</div>
											</div>
											<%
												} else if(ab.getAlert_Type().equals(Constants.HUMIDITY)) {
											%>
											<div class="panel panel-default">
												<div class="panel-heading" role="tab"
													id="<%=ab.getAlert_ID()%>">
													<h4 class="panel-title">
														<div class="row" class="collapsed" role="button"
															data-toggle="collapse" data-parent="#accordion"
															href="#header<%=ab.getAlert_ID()%>" aria-expanded="false"
															aria-controls="header<%=ab.getAlert_ID()%>">
															<%
																if(ab.getAlert_Severity().equals("4") || ab.getAlert_Severity().equals("3")){
															%>
															<div class="col-md-1 alert-text text-center">
																<span class="glyphicon glyphicon glyphicon-tint"></span>
															</div>
															<div class="col-md-1 alert-text"><%=ab.getAlert_ID()%></div>
															<div class="col-md-5 alert-text">
																Humidity
																has breached
																<%=ab.getBreach_Type()%>
																limit for over
																<%=ab.getAlert_Duration()%>
																minutes
															</div>
															<div class="col-md-2 alert-text">Adarsh Kumar</div>
															<%
																} else {
															%>
															<div class="col-md-1 text-center">
																<span class="glyphicon glyphicon glyphicon-tint"></span>
															</div>
															<div class="col-md-1"><%=ab.getAlert_ID()%></div>
															<div class="col-md-5">
																Humidity
																has breached
																<%=ab.getBreach_Type()%>
																limit for over
																<%=ab.getAlert_Duration()%>
																minutes
															</div>
															<div class="col-md-2">Adarsh Kumar</div>
															<%
																}
															%>
															<div class="col-md-2 pull-right">
																<input id="btn<%=ab.getAlert_ID()%>" type="button"
																	class="btn btn-default"
																	value="<%=ab.getAction_Status()%>">
															</div>
														</div>
													</h4>
												</div>
											</div>
											<%
												} else if(ab.getAlert_Type().equals(Constants.STOPPAGE)) {
													%>
													<div class="panel panel-stop">
														<div class="panel-heading" role="tab"
															id="<%=ab.getAlert_ID()%>">
															<h4 class="panel-title">
																<div class="row" class="collapsed" role="button"
																	data-toggle="collapse" data-parent="#accordion"
																	href="#header<%=ab.getAlert_ID()%>" aria-expanded="false"
																	aria-controls="header<%=ab.getAlert_ID()%>">
																	<%
																		if(ab.getAlert_Severity().equals("4") || ab.getAlert_Severity().equals("3")){
																	%>
																	<div class="col-md-1 alert-text text-center">
																		<i class="fa fa-clock-o"></i>
																	</div>
																	<div class="col-md-1 alert-text"><%=ab.getAlert_ID()%></div>
																	<div class="col-md-5 alert-text">
																		Vehicle <%=sVehicleId %> 
																		has stopped 
																		for over
																		<%=ab.getAlert_Duration()%>
																		minutes
																	</div>
																	<div class="col-md-2 alert-text">Adarsh Kumar</div>
																	<%
																		} else {
																	%>
																	<div class="col-md-1 text-center">
																		<i class="fa fa-clock-o"></i>
																	</div>
																	<div class="col-md-1"><%=ab.getAlert_ID()%></div>
																	<div class="col-md-5">
																		Vehicle <%=sVehicleId %> 
																		has stopped 
																		for over
																		<%=ab.getAlert_Duration()%>
																		minutes
																	</div>
																	<div class="col-md-2">Adarsh Kumar</div>
																	<%
																		}
																	%>
																	<div class="col-md-2 pull-right">
																		<input id="btn<%=ab.getAlert_ID()%>" type="button"
																			class="btn btn-default"
																			value="<%=ab.getAction_Status()%>">
																	</div>
																</div>
															</h4>
														</div>
													</div>
													<%
														} else if(ab.getAlert_Type().equals(Constants.DOOR_ACTIVITY)) {
											%>
											<div class="panel panel-door">
												<div class="panel-heading" role="tab"
													id="<%=ab.getAlert_ID()%>">
													<h4 class="panel-title">
														<div class="row" class="collapsed" role="button"
															data-toggle="collapse" data-parent="#accordion"
															href="#header<%=ab.getAlert_ID()%>" aria-expanded="false"
															aria-controls="header<%=ab.getAlert_ID()%>">
															<%
																if(ab.getAlert_Severity().equals("4") || ab.getAlert_Severity().equals("3")){
															%>
															<div class="col-md-1 alert-text text-center">
																<i class="fa fa-columns"></i>
															</div>
															<div class="col-md-1 alert-text"><%=ab.getAlert_ID()%></div>
															<div class="col-md-5 alert-text">
																Door of <%=sVehicleId %> 
																has been opened for over
																<%=ab.getAlert_Duration()%>
																minutes
															</div>
															<div class="col-md-2 alert-text">Adarsh Kumar</div>
															<%
																} else {
															%>
															<div class="col-md-1 text-center">
																<i class="fa fa-columns"></i>
															</div>
															<div class="col-md-1"><%=ab.getAlert_ID()%></div>
															<div class="col-md-5">
																Door of <%=sVehicleId %> 
																has been opened for over
																<%=ab.getAlert_Duration()%>
																minutes
															</div>
															<div class="col-md-2">Adarsh Kumar</div>
															<%
																}
															%>
															<div class="col-md-2 pull-right">
																<input id="btn<%=ab.getAlert_ID()%>" type="button"
																	class="btn btn-default"
																	value="<%=ab.getAction_Status()%>">
															</div>
														</div>
													</h4>
												</div>
											</div>
											<%
												}
											%>
											<br>
											<%
												if(ab.getAlert_Type().equals(Constants.TEMPERATURE)) {
											%>
											<div id="header<%=ab.getAlert_ID()%>"
												class="panel-collapse collapse" role="tabpanel"
												aria-labelledby="<%=ab.getAlert_ID()%>">
												<div class="panel-body">
													<div class="well">
															<div class="row">
																<div class="col-md-12">
																<div class="row">
																		<div class="col-md-6 text-center">
																	<div class="form-group">
																		<label>Average Event Temperature</label><br> <input
																			class="btn btn-default"
																			class="form-control-no-height" type="button"
																			value="<%=ab.getAlert_Parameter_Avg()%>C">
																	</div>
																		</div>
																		<div class="col-md-6">
																		<div class="form-group">

																		<label>Event Duration</label><br> <input
																			class="btn btn-default" type="button"
																			value="<%=ab.getAlert_Duration()%> minutes">
																	</div>
																		</div>
																	</div>
																	<div class="row">
																		<div class="col-md-6">
																	<div class="form-group">
																		<label>Assigned To</label><br> <input type="text"
																			class="form-control-no-height" style="width:50%" value="Adarsh Kumar">
																	</div>
																	</div>
																	<div class="col-md-6">
																	<div class="form-group">
																		<label>Supervisor</label><br> <input type="text"
																			class="form-control-no-height" style="width:50%" value="Saumitra Singh">
																	</div>
																	</div>
																	</div>
																	
																	<input class="btn btn-success" type="button"
																		value="In Progress" style="width:120px"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'In Progress')">
																	<input class="btn btn-success" type="button"
																		value="On Hold" style="width:120px"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'On Hold')">
																	<input class="btn btn-success" type="button"
																		value="Close" style="width:120px"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'Closed')">
																	<br> <br>
																	
																	<input class="btn btn-success" type="button"
																		value="Escalate" style="width:120px"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'Escalated')">
																	<input class="btn btn-success" type="button"
																		value="Reassign" style="width:120px"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'Reassigned')">
																	<input class="btn btn-success" type="button"
																		value="Reopen" style="width:120px"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'Reopened')">
																</div>
																</div>
																<div class="row">
																<div class="col-md-12">
																	<br>
																	<div class="row">
																		<div class="col-md-6 text-center">
																		<div class="form-group">
																		<label>Alert Root Cause</label>
																		<textarea class="form-control" rows="3"
																			id="txtRoot<%=ab.getAlert_ID()%>"><%=ab.getRoot_Cause()%></textarea>
																	</div>
																		</div>
																		<div class="col-md-6 text-center">
																		<div class="form-group">
																		<label>Preventive Actions</label><br>
																		<textarea class="form-control" rows="3"
																			id="txtPre<%=ab.getAlert_ID()%>"><%=ab.getPreventive_Actions()%></textarea>
																	</div>
																		</div>
																	</div>
																	<div class="form-group text-center">
																		<input class="btn btn-success" type="button"
																			value="Save"
																			onclick="updateRoot('<%=ab.getAlert_ID()%>')"
																			data-toggle="modal" data-target="#myModal">
																	</div>
																</div>
															</div>
													
													</div>
												</div>
											</div>
											<%
												}  else if(ab.getAlert_Type().equals(Constants.HUMIDITY)) {
											%>
											<div id="header<%=ab.getAlert_ID()%>"
												class="panel-collapse collapse" role="tabpanel"
												aria-labelledby="<%=ab.getAlert_ID()%>">
												<div class="panel-body">
													<div class="well">
															<div class="row">
																<div class="col-md-12">
																	<div class="row">
																		<div class="col-md-6">
																	<div class="form-group">
																		<label>Average Humidity</label><br> <input
																			class="btn btn-default"
																			class="form-control-no-height" type="button"
																			value="<%=ab.getAlert_Parameter_Avg()%>%">
																	</div>	
																		</div>
																		<div class="col-md-6">
																		<div class="form-group">

																		<label>Event Duration</label><br> <input
																			class="btn btn-default" type="button"
																			value="<%=ab.getAlert_Duration()%> minutes">
																	</div>
																		</div>
																	</div>
																	<div class="row">
																		<div class="col-md-6">
																	<div class="form-group">
																		<label>Assigned To</label><br> <input type="text"
																			class="form-control-no-height" style="width:50%" value="Adarsh Kumar">
																	</div>
																	</div>
																	<div class="col-md-6">
																	<div class="form-group">
																		<label>Supervisor</label><br> <input type="text"
																			class="form-control-no-height" style="width:50%" value="Saumitra Singh">
																	</div>
																	</div>
																	</div>
																	<input class="btn btn-success" type="button"
																		value="In Progress" style="width:120px"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'In Progress')">
																	<input class="btn btn-success" type="button"
																		value="On Hold" style="width:120px"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'On Hold')">
																	<input class="btn btn-success" type="button"
																		value="Close" style="width:120px"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'Closed')">
																	<br> <br>
																	
																	<input class="btn btn-success" type="button"
																		value="Escalate" style="width:120px"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'Escalated')">
																	<input class="btn btn-success" type="button"
																		value="Reassign" style="width:120px"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'Reassigned')">
																	<input class="btn btn-success" type="button"
																		value="Reopen" style="width:120px"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'Reopened')">
																</div>
																</div>
																<div class="row">
																<div class="col-md-12">
																<br>
																	<div class="row">
																		<div class="col-md-6 text-center">
																		<div class="form-group">
																		<label>Alert Root Cause</label><br>
																		<textarea class="form-control" rows="3"
																			id="txtRoot<%=ab.getAlert_ID()%>"><%=ab.getRoot_Cause()%></textarea>
																	</div>
																		</div>
																		<div class="col-md-6 text-center">
																		<div class="form-group">
																		<label>Preventive Actions</label><br>
																		<textarea class="form-control" rows="3"
																			id="txtPre<%=ab.getAlert_ID()%>"><%=ab.getPreventive_Actions()%></textarea>
																	</div>
																		</div>
																	</div>
																	
																	
																	<div class="form-group text-center">
																		<input class="btn btn-success" type="button"
																			value="Save"
																			onclick="updateRoot('<%=ab.getAlert_ID()%>')"
																			data-toggle="modal" data-target="#myModal">
																	</div>
																</div>
															</div>
													</div>
												</div>
											</div>
											<%
												} else if(ab.getAlert_Type().equals(Constants.STOPPAGE)) {
													%>
													<div id="header<%=ab.getAlert_ID()%>"
														class="panel-collapse collapse" role="tabpanel"
														aria-labelledby="<%=ab.getAlert_ID()%>">
														<div class="panel-body">
															<div class="well">
																	<div class="row">
																		<div class="col-md-12">
																			<div class="row">
																				<div class="col-md-6">
																			<div class="form-group">
																				<label>Number of Unplanned Stops Today</label><br> <input
																					class="btn btn-default"
																					class="form-control-no-height" type="button"
																					value="4">
																			</div>	
																				</div>
																				<div class="col-md-6">
																				<div class="form-group">
																				<label>Stoppage Time</label><br> <input
																						class="btn btn-default" type="button"
																						value="<%=ab.getAlert_Duration()%> minutes">
																			</div>
																				</div>
																			</div>
																			<div class="row">
																				<div class="col-md-6">
																			<div class="form-group">
																				<label>Assigned To</label><br> <input type="text"
																					class="form-control-no-height" style="width:50%" value="Adarsh Kumar">
																			</div>
																			</div>
																			<div class="col-md-6">
																			<div class="form-group">
																				<label>Supervisor</label><br> <input type="text"
																					class="form-control-no-height" style="width:50%" value="Saumitra Singh">
																			</div>
																			</div>
																			</div>
																			<input class="btn btn-success" type="button"
																				value="In Progress" style="width:120px"
																				onclick="updateAction('<%=ab.getAlert_ID()%>', 'In Progress')">
																			<input class="btn btn-success" type="button"
																				value="On Hold" style="width:120px"
																				onclick="updateAction('<%=ab.getAlert_ID()%>', 'On Hold')">
																			<input class="btn btn-success" type="button"
																				value="Close" style="width:120px"
																				onclick="updateAction('<%=ab.getAlert_ID()%>', 'Closed')">
																			<br> <br>
																			
																			<input class="btn btn-success" type="button"
																				value="Escalate" style="width:120px"
																				onclick="updateAction('<%=ab.getAlert_ID()%>', 'Escalated')">
																			<input class="btn btn-success" type="button"
																				value="Reassign" style="width:120px"
																				onclick="updateAction('<%=ab.getAlert_ID()%>', 'Reassigned')">
																			<input class="btn btn-success" type="button"
																				value="Reopen" style="width:120px"
																				onclick="updateAction('<%=ab.getAlert_ID()%>', 'Reopened')">
																		</div>
																		</div>
																		<div class="row">
																		<div class="col-md-12">
																		<br>
																			<div class="row">
																				<div class="col-md-6 text-center">
																				
																			<div class="form-group">
																					<label>Alert Root Cause</label><br>
																					<textarea class="form-control" rows="3"
																						id="txtRoot<%=ab.getAlert_ID()%>"><%=ab.getRoot_Cause()%></textarea>
																				</div>
																				</div>
																				<div class="col-md-6 text-center">
																				<div class="form-group">
																				<label>Preventive Actions</label><br>
																				<textarea class="form-control" rows="3"
																					id="txtPre<%=ab.getAlert_ID()%>"><%=ab.getPreventive_Actions()%></textarea>
																			</div>
																				</div>
																			</div>
																			
																			
																			<div class="form-group text-center">
																				<input class="btn btn-success" type="button"
																					value="Save"
																					onclick="updateRoot('<%=ab.getAlert_ID()%>')"
																					data-toggle="modal" data-target="#myModal">
																			</div>
																		</div>
																	</div>
															</div>
														</div>
													</div>
													<%
														} else if(ab.getAlert_Type().equals(Constants.DOOR_ACTIVITY)) {
											%>
											<div id="header<%=ab.getAlert_ID()%>"
												class="panel-collapse collapse" role="tabpanel"
												aria-labelledby="<%=ab.getAlert_ID()%>">
												<div class="panel-body">
													<div class="well">
															<div class="row">
																<div class="col-md-12">
																<div class="row">
																		<div class="col-md-12">
																	<div class="form-group">
																		<label>Door Open Duration</label><br> <input
																			class="btn btn-default" type="button"
																			value="<%=ab.getAlert_Duration()%> minutes">
																	</div>
																		</div>
																		
																	</div>
																	<div class="row">
																		<div class="col-md-6">
																	<div class="form-group">
																		<label>Assigned To</label><br> <input type="text"
																			class="form-control-no-height" style="width:50%" value="Adarsh Kumar">
																	</div>
																	</div>
																	<div class="col-md-6">
																	<div class="form-group">
																		<label>Supervisor</label><br> <input type="text"
																			class="form-control-no-height" style="width:50%" value="Saumitra Singh">
																	</div>
																	</div>
																	</div>
																	
																	<input class="btn btn-success" type="button"
																		value="In Progress" style="width:120px"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'In Progress')">
																	<input class="btn btn-success" type="button"
																		value="On Hold" style="width:120px"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'On Hold')">
																	<input class="btn btn-success" type="button"
																		value="Close" style="width:120px"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'Closed')">
																	<br> <br>
																	
																	<input class="btn btn-success" type="button"
																		value="Escalate" style="width:120px"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'Escalated')">
																	<input class="btn btn-success" type="button"
																		value="Reassign" style="width:120px"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'Reassigned')">
																	<input class="btn btn-success" type="button"
																		value="Reopen" style="width:120px"
																		onclick="updateAction('<%=ab.getAlert_ID()%>', 'Reopened')">
																</div>
																</div>
																<div class="row">
																<div class="col-md-12">
																	<div class="row">
																		<div class="col-md-6 text-center">
																		<div class="form-group">
																		<label>Alert Root Cause</label><br>
																		<textarea class="form-control" rows="3"
																			id="txtRoot<%=ab.getAlert_ID()%>"><%=ab.getRoot_Cause()%></textarea>
																	</div>
																		</div>
																		<div class="col-md-6 text-center">
																		<div class="form-group">
																		<label>Preventive Actions</label><br>
																		<textarea class="form-control" rows="3"
																			id="txtPre<%=ab.getAlert_ID()%>"><%=ab.getPreventive_Actions()%></textarea>
																	</div>
																		</div>
																	</div>
																	
																	
																	<div class="form-group text-center">
																		<input class="btn btn-success" type="button"
																			value="Save"
																			onclick="updateRoot('<%=ab.getAlert_ID()%>')"
																			data-toggle="modal" data-target="#myModal">
																	</div>
																</div>
														</div>
													</div>
												</div>
											</div>
											<%
												}
											}
											%>
										</div>
									</div>
								</div>
					</div>
					</div>
				</div>

								</div>
							</div>
						</div>


					</div>

				</div>
				<!-- /.row -->
				<br>

			</section>
			
			
		</div>
		<!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0
			</div>
			<strong>Copyright &copy; 2017 <a href="http://tagbox.in">TagBox</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Control Sidebar -->
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->

	<!-- Bootstrap 3.3.6 -->
	<script src="../../bootstrap/js/bootstrap.min.js"></script>
	<script src="../../plugins/fastclick/fastclick.js"></script>
		<script src="../../plugins/chartjs/Chart.min.js"></script>
	<!-- AdminLTE App -->
	<script src="../../dist/js/app.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="../../dist/js/demo.js"></script>
	<script type="text/javascript">
	var areaChartData1 = {
			labels : [<%=time1%>],
			      		datasets : [
					{
						label : "Temperature",
						fill: false,
						data : [<%=temperature1%>]
					}
				
		
		 ]
	};
	var areaChartData2 = {
			labels : [<%=time2%>],
		datasets : [ 
		             {
			label : "Humidity",
			fill: false,
			data : [<%=temperature2%>]
		}
		]
	};
	var areaChartData3 = {
			labels : [<%= time3%>],
		datasets : [ 
		             {
			label : "Hertz",
			fill: false,
			data : [<%= temperature3%>]
		}
		]
	};

	var areaChartOptions = {
		scales : {
			xAxes : [ {
				ticks : {
					maxRotation : 90,
					minRotation : 90
				}
			} ]
		},

		//Boolean - If we should show the scale at all
		showScale : true,
		//Boolean - Whether grid lines are shown across the chart
		scaleShowGridLines : false,
		//String - Colour of the grid lines
		scaleGridLineColor : "rgba(0,0,0,.05)",
		//Number - Width of the grid lines
		scaleGridLineWidth : 1,
		//Boolean - Whether to show horizontal lines (except X axis)
		scaleShowHorizontalLines : true,
		//Boolean - Whether to show vertical lines (except Y axis)
		scaleShowVerticalLines : true,
		//Boolean - Whether the line is curved between points
		bezierCurve : true,
		//Number - Tension of the bezier curve between points
		bezierCurveTension : 0.3,
		//Boolean - Whether to show a dot for each point
		pointDot : true,
	//Number - Radius of each point dot in pixels
	pointDotRadius : 1,
	//Number - Pixel width of point dot stroke
	pointDotStrokeWidth : 1,
	//Number - amount extra to add to the radius to cater for hit detection outside the drawn point !!!important to display only one point
	pointHitDetectionRadius : 0,
	//Boolean - Whether to show a stroke for datasets
	datasetStroke : true,
	//Number - Pixel width of dataset stroke
	datasetStrokeWidth : 1,
		//Boolean - Whether to fill the dataset with a color
		datasetFill : false,
		fill: false,
		//String - A legend template
		//Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
		maintainAspectRatio : true,
		//Boolean - whether to make the chart responsive to window resizing
		responsive : true

	};

	//-------------
	//- LINE CHART -
	//--------------
	areaChartOptions.datasetFill = false;
	//$('a[data-toggle=tab').on('shown.bs.tab', function(e) {
	//	window.dispatchEvent(new Event('resize'));
<%if(!temperature1.equals("")) {%>
var lineChartCanvas1 = $("#lineChart1").get(0).getContext("2d");
var lineChart1 = new Chart(lineChartCanvas1);
lineChart1.Line(areaChartData1, areaChartOptions);
<%}%>
<%if(!temperature2.equals("")) {%>
var lineChartCanvas2 = $("#lineChart2").get(0).getContext("2d");
var lineChart2 = new Chart(lineChartCanvas2);
lineChart2.Line(areaChartData2, areaChartOptions);
<%}%>
<%if(!temperature3.equals("")) {%>
var lineChartCanvas3 = $("#lineChart3").get(0).getContext("2d");
var lineChart3 = new Chart(lineChartCanvas3);
lineChart3.Line(areaChartData3, areaChartOptions);
<%}%>


<%
//System.out.println(";"+temperature4+";");
//if(!temperature4.equals("00")){%>
//var lineChartCanvas4 = $("#lineChart4").get(0).getContext("2d");
//var lineChart4 = new Chart(lineChartCanvas4);
//lineChart4.Line(areaChartData4, areaChartOptions);
<%//}
%>
  
//});
</script>
	<!-- page script -->
		<script type="text/javascript">google.maps.event.addDomListener(window, 'load', initialize);  $('.infotext').tooltip();</script>
	</body>
</html>
