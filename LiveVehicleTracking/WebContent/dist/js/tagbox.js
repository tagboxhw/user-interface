$(document).ready(function(){
	 // Defining the local dataset
	
		$('#all_btn').click(function(){
        	$('#content_mixed').show();
        	$('#content_route').hide();
        	$('#content_stop').hide();
        	$('#content_temp').hide();
        	$('#content_door').hide();
        });
		$('#route_btn').click(function(){
        	$('#content_mixed').hide();
        	$('#content_route').show();
        	$('#content_stop').hide();
        	$('#content_temp').hide();
        	$('#content_door').hide();
        });
		$('#stop_btn').click(function(){
        	$('#content_mixed').hide();
        	$('#content_route').hide();
        	$('#content_stop').show();
        	$('#content_temp').hide();
        	$('#content_door').hide();
        });
		$('#door_btn').click(function(){
        	$('#content_mixed').hide();
        	$('#content_route').hide();
        	$('#content_stop').hide();
        	$('#content_temp').hide();
        	$('#content_door').show();
        });
		$('#temp_btn').click(function(){
        	$('#content_mixed').hide();
        	$('#content_route').hide();
        	$('#content_stop').hide();
        	$('#content_temp').show();
        	$('#content_door').hide();
        });
		if($("#AlertType").val() == "Cold Rooms") {
			document.getElementById("VehicleType").disabled = true;
		}
});

function onAlertTypeChange(){
	if($("#AlertType").val() == "Cold Rooms") {
		document.getElementById("VehicleType").disabled = true;
	}
	if($("#AlertType").val() == "Vehicles") {
		document.getElementById("VehicleType").disabled = false;
	}
}

function onColdAlertTypeChange(){
	if($("#coldAlert").val() == "Manufacturing Unit") {
		$("#unit").show();
		$("#warehouse").hide();
		
	}
	if($("#coldAlert").val() == "Warehouse") {
		$("#unit").hide();
		$("#warehouse").show();
	}
}

function onSKUTypeChange(){
	if($("#SKUAlertType").val() == "SKU 1") {
		$("#sku1").show();
		$("#sku2").hide();
		$("#sku3").hide();
		$("#sku4").hide();
		$("#sku5").hide();
		
	}
	if($("#SKUAlertType").val() == "SKU 2") {
		$("#sku1").hide();
		$("#sku2").show();
		$("#sku3").hide();
		$("#sku4").hide();
		$("#sku5").hide();
		
	}
	if($("#SKUAlertType").val() == "SKU 3") {
		$("#sku1").hide();
		$("#sku2").hide();
		$("#sku3").show();
		$("#sku4").hide();
		$("#sku5").hide();
		
	}
	if($("#SKUAlertType").val() == "SKU 4") {
		$("#sku1").hide();
		$("#sku2").hide();
		$("#sku3").hide();
		$("#sku4").show();
		$("#sku5").hide();
		
	}
	if($("#SKUAlertType").val() == "SKU 5") {
		$("#sku1").hide();
		$("#sku2").hide();
		$("#sku3").hide();
		$("#sku4").hide();
		$("#sku5").show();
		
	}
}

var catString = [];
function UpdateCatString(inputArray) {
	if(catString.length == 0){
		//alert("splitting: " + $('#CatID').val());
		catString[0] = $('#CatID').val().split(',');
	}
	//alert(inputArray);
	var b = false;
	var k = 0;
	for(j=0; j<catString.length; j++) {
		if(inputArray == catString[j]){
			k = j;
			b = true;
			break;
		}
	}
	if (!b) {
		catString.push(inputArray);
	} else {
		catString.splice(k,1);
	}
	
  $('#CatID').val(catString);  
}