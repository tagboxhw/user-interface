
function edit_row(no)
{
 document.getElementById("edit_button"+no).style.display="none";
 document.getElementById("save_button"+no).style.display="block";
 //document.getElementById("cancel_button"+no).style.display="block";
 
 var vehicle_id=document.getElementById(no+"_Vehicle_id");
 var gw_client_id=document.getElementById(no+"_GW_client_id");
 var nd_device_id=document.getElementById(no+"_ND_device_id");
 var gw_nd_pairing=document.getElementById(no+"_GW_ND_Pairing");
 var nd_subzone_type=document.getElementById(no+"_ND_subzone_type");
 var nd_subzone_name=document.getElementById(no+"_ND_subzone_name");
 var nd_type=document.getElementById(no+"_ND_type");
 var nd_status=document.getElementById(no+"_ND_status");
 	
 var vehicle_id_data=vehicle_id.innerHTML;
 var gw_client_id_data=gw_client_id.innerHTML;
 var nd_device_id_data=nd_device_id.innerHTML;
 var gw_nd_pairing_data=gw_nd_pairing.innerHTML;
 var nd_subzone_type_data=nd_subzone_type.innerHTML;
 var nd_subzone_name_data=nd_subzone_name.innerHTML;
 var nd_type_data=nd_type.innerHTML;
 var nd_status_data=nd_status.innerHTML;
	
 vehicle_id.innerHTML="<input type='text' class='form-control-no-height' id='vehicle_id_text"+no+"' value='"+vehicle_id_data+"'>";
 gw_client_id.innerHTML="<input type='text' class='form-control-no-height' id='gw_client_id_text"+no+"' value='"+gw_client_id_data+"'>";
 nd_device_id.innerHTML="<input type='text' class='form-control-no-height' id='nd_device_id_text"+no+"' value='"+nd_device_id_data+"'>";
 gw_nd_pairing.innerHTML="<input type='text' class='form-control-no-height' id='gw_nd_pairing_text"+no+"' value='"+gw_nd_pairing_data+"'>";
 nd_subzone_type.innerHTML="<input type='text' class='form-control-no-height' id='nd_subzone_type_text"+no+"' value='"+nd_subzone_type_data+"'>";
 nd_subzone_name.innerHTML="<input type='text' class='form-control-no-height' id='nd_subzone_name_text"+no+"' value='"+nd_subzone_name_data+"'>";
 nd_type.innerHTML="<input type='text' class='form-control-no-height' id='nd_type_text"+no+"' value='"+nd_type_data+"'>";
 nd_status.innerHTML="<input type='text' class='form-control-no-height' id='nd_status_text"+no+"' value='"+nd_status_data+"'>";
}

function save_row(no)
{
 var vehicle_id_val=document.getElementById("vehicle_id_text"+no).value;
 var gw_client_id_val=document.getElementById("gw_client_id_text"+no).value;
 var nd_device_id_val=document.getElementById("nd_device_id_text"+no).value;
 var gw_nd_pairing_val=document.getElementById("gw_nd_pairing_text"+no).value;
 var nd_subzone_type_val=document.getElementById("nd_subzone_type_text"+no).value;
 var nd_subzone_name_val=document.getElementById("nd_subzone_name_text"+no).value;
 var nd_type_val=document.getElementById("nd_type_text"+no).value;
 var nd_status_val=document.getElementById("nd_status_text"+no).value;
 
 if(vehicle_id_val == "") { alert("Please enter a value for Vehicle Id"); document.getElementById("vehicle_id_text"+no).focus(); return;}
 if(gw_client_id_val == "") { alert("Please enter a value for GW_Client_Id"); document.getElementById("gw_client_id_text"+no).focus(); return;}
 if(nd_device_id_val == "") { alert("Please enter a value for ND_Device_Id"); document.getElementById("nd_device_id_text"+no).focus(); return;}
 if(gw_nd_pairing_val == "") { alert("Please enter a value for GW_ND_Pairing"); document.getElementById("gw_nd_pairing_text"+no).focus(); return;}
 if(nd_subzone_type_val == "") { alert("Please enter a value for ND_Subzone_Type"); document.getElementById("nd_subzone_type_text"+no).focus(); return;}
 if(nd_subzone_name_val == "") { alert("Please enter a value for ND_Subzone_Name"); document.getElementById("nd_subzone_name_text"+no).focus();return;}
 if(nd_type_val == "") { alert("Please enter a value for ND_Type"); document.getElementById("nd_type_text"+no).focus(); return;}
 if(nd_status_val == "") { alert("Please enter a value for ND_Status"); document.getElementById("nd_status_text"+no).focus(); return;}
 
 document.getElementById(no+"_Vehicle_id").innerHTML=vehicle_id_val;
 document.getElementById(no+"_GW_client_id").innerHTML=gw_client_id_val;
 document.getElementById(no+"_ND_device_id").innerHTML=nd_device_id_val;
 document.getElementById(no+"_GW_ND_Pairing").innerHTML=gw_nd_pairing_val;
 document.getElementById(no+"_ND_subzone_type").innerHTML=nd_subzone_type_val;
 document.getElementById(no+"_ND_subzone_name").innerHTML=nd_subzone_name_val;
 document.getElementById(no+"_ND_type").innerHTML=nd_type_val;
 document.getElementById(no+"_ND_status").innerHTML=nd_status_val;
 
 document.getElementById("edit_button"+no).style.display="block";
 document.getElementById("save_button"+no).style.display="none";
 //document.getElementById("cancel_button"+no).style.display="none";
 
 var nd_client_id=document.getElementById(no+"_ND_client_id");
 var nd_client_id_val=nd_client_id.innerHTML;
 
 	var posting = $.post("../../pages/jsp/GetNodeData.jsp",
				{
					Vehicle_Id:vehicle_id_val,
					GW_client_Id:gw_client_id_val,
					ND_device_id_val:nd_device_id_val,
					GW_nd_pairing_val:gw_nd_pairing_val,
					ND_subzone_type_val:nd_subzone_type_val,
					ND_subzone_name_val:nd_subzone_name_val,
					ND_type_val:nd_type_val,
					ND_status_val:nd_status_val,
					ND_client_id_val:nd_client_id_val,
					query:"updateTransitNodeMap"
				},
				function(data,status){
					//alert(data);
					xmlDoc = loadXMLString(data);
					if($(xmlDoc).find("rows").length == 0) {
						return;
					}
					
					$(xmlDoc).find("row").each(function(){
						var id = $(this).find("Return").text();
					});
					
				});
 
}

function cancel_row(no) {
	document.getElementById("edit_button"+no).style.display="block";
	 document.getElementById("save_button"+no).style.display="none";
	 //document.getElementById("cancel_button"+no).style.display="none";
	 
	var vehicle_id=document.getElementById(no+"_Vehicle_id");
	 var gw_client_id=document.getElementById(no+"_GW_client_id");
	 var nd_client_id=document.getElementById(no+"_ND_client_id");
	 var nd_device_id=document.getElementById(no+"_ND_device_id");
	 var gw_nd_pairing=document.getElementById(no+"_GW_ND_Pairing");
	 var nd_subzone_type=document.getElementById(no+"_ND_subzone_type");
	 var nd_subzone_name=document.getElementById(no+"_ND_subzone_name");
	 var nd_type=document.getElementById(no+"_ND_type");
	 var nd_status=document.getElementById(no+"_ND_status");
	 	
	 var vehicle_id_data=vehicle_id.innerHTML;
	 var gw_client_id_data=gw_client_id.innerHTML;
	 var nd_client_id_data=nd_client_id.innerHTML;
	 var nd_device_id_data=nd_device_id.innerHTML;
	 var gw_nd_pairing_data=gw_nd_pairing.innerHTML;
	 var nd_subzone_type_data=nd_subzone_type.innerHTML;
	 var nd_subzone_name_data=nd_subzone_name.innerHTML;
	 var nd_type_data=nd_type.innerHTML;
	 var nd_status_data=nd_status.innerHTML;
		
	 vehicle_id.innerHTML="<input type='text' class='form-control-no-height' id='vehicle_id_text"+no+"' value='"+vehicle_id_data+"'>";
	 gw_client_id.innerHTML="<input type='text' class='form-control-no-height' id='gw_client_id_text"+no+"' value='"+gw_client_id_data+"'>";
	 nd_device_id.innerHTML="<input type='text' class='form-control-no-height' id='nd_device_id_text"+no+"' value='"+nd_device_id_data+"'>";
	 gw_nd_pairing.innerHTML="<input type='text' class='form-control-no-height' id='gw_nd_pairing_text"+no+"' value='"+gw_nd_pairing_data+"'>";
	 nd_subzone_type.innerHTML="<input type='text' class='form-control-no-height' id='nd_subzone_type_text"+no+"' value='"+nd_subzone_type_data+"'>";
	 nd_subzone_name.innerHTML="<input type='text' class='form-control-no-height' id='nd_subzone_name_text"+no+"' value='"+nd_subzone_name_data+"'>";
	 nd_type.innerHTML="<input type='text' class='form-control-no-height' id='nd_type_text"+no+"' value='"+nd_type_data+"'>";
	 nd_status.innerHTML="<input type='text' class='form-control-no-height' id='nd_status_text"+no+"' value='"+nd_status_data+"'>";
}

function delete_row(no)
{ 
	
	var nd_client_id=document.getElementById(no+"_ND_client_id");
	 var nd_client_id_val=nd_client_id.innerHTML;
 var posting = $.post("../../pages/jsp/GetNodeData.jsp",
			{
				ND_client_id_val:nd_client_id_val,
				query:"deleteTransitNodeMap"
			},
			function(data,status){
				//alert(data);
				xmlDoc = loadXMLString(data);
				if($(xmlDoc).find("rows").length == 0) {
					return;
				}
				
				$(xmlDoc).find("row").each(function(){
					var id = $(this).find("Return").text();
				});
				
			});
 document.getElementById("row_"+no).outerHTML="";
}

function add_row()
{
	var no = document.getElementById("lastValue").value;
	var vehicle_id_val=document.getElementById("new_vehicle_id").value;
	 var gw_client_id_val=document.getElementById("new_GW_client_id").value;
	 var nd_client_id_val=document.getElementById("new_ND_client_id").value;
	 var nd_device_id_val=document.getElementById("new_ND_device_id").value;
	 var gw_nd_pairing_val=document.getElementById("new_GW_ND_Pairing").value;
	 var nd_subzone_type_val=document.getElementById("new_ND_subzone_type").value;
	 var nd_subzone_name_val=document.getElementById("new_ND_subzone_name").value;
	 var nd_type_val=document.getElementById("new_ND_type").value;
	 var nd_status_val=document.getElementById("new_ND_status").value;
	 
	 if(vehicle_id_val == "") { alert("Please enter a value for Vehicle Id"); document.getElementById("new_vehicle_id").focus(); return;}
	 if(gw_client_id_val == "") { alert("Please enter a value for GW_Client_Id"); document.getElementById("new_GW_client_id").focus(); return;}
	 if(nd_client_id_val == "") { alert("Please enter a value for ND_Client_Id"); document.getElementById("new_ND_client_id").focus(); return;}
	 if(nd_device_id_val == "") { alert("Please enter a value for ND_Device_Id"); document.getElementById("new_ND_device_id").focus(); return;}
	 if(gw_nd_pairing_val == "") { alert("Please enter a value for GW_ND_Pairing"); document.getElementById("new_GW_ND_Pairing").focus(); return;}
	 if(nd_subzone_type_val == "") { alert("Please enter a value for ND_Subzone_Type"); document.getElementById("new_ND_subzone_type").focus(); return;}
	 if(nd_subzone_name_val == "") { alert("Please enter a value for ND_Subzone_Name"); document.getElementById("new_ND_subzone_name").focus();return;}
	 if(nd_type_val == "") { alert("Please enter a value for ND_Type"); document.getElementById("new_ND_type").focus(); return;}
	 if(nd_status_val == "") { alert("Please enter a value for ND_Status"); document.getElementById("new_ND_status").focus(); return;}
	 
	 var posting = $.post("../../pages/jsp/GetNodeData.jsp",
				{
					Vehicle_Id:vehicle_id_val,
					GW_client_Id:gw_client_id_val,
					ND_device_id_val:nd_device_id_val,
					GW_nd_pairing_val:gw_nd_pairing_val,
					ND_subzone_type_val:nd_subzone_type_val,
					ND_subzone_name_val:nd_subzone_name_val,
					ND_type_val:nd_type_val,
					ND_status_val:nd_status_val,
					ND_client_id_val:nd_client_id_val,
					query:"insertTransitNodeMap"
				},
				function(data,status){
					//alert(data);
					xmlDoc = loadXMLString(data);
					if($(xmlDoc).find("rows").length == 0) {
						return;
					}
					
					$(xmlDoc).find("row").each(function(){
						var id = $(this).find("Return").text();
					});
					
				});
		
 var table=document.getElementById("data_table");
 var table_len=(table.rows.length)-1;
 var row = table.insertRow(table_len).outerHTML="<tr id='row_"+no+"'><td id='"+no+"_Vehicle_id'>"+vehicle_id_val+"</td>" +
 		"<td id='"+no+"_GW_client_id'>"+gw_client_id_val+"</td>" +
 		"<td id='"+no+"_ND_client_id'>"+nd_client_id_val+"</td>" +
 		"<td id='"+no+"_ND_device_id'>"+nd_device_id_val+"</td>" +
 		"<td id='"+no+"_GW_ND_Pairing'>"+gw_nd_pairing_val+"</td>" +
 		"<td id='"+no+"_ND_subzone_type'>"+nd_subzone_type_val+"</td>" +
 		"<td id='"+no+"_ND_subzone_name'>"+nd_subzone_name_val+"</td>" +
 		"<td id='"+no+"_ND_type'>"+nd_type_val+"</td>" +
 		"<td id='"+no+"_ND_status'>"+nd_status_val+"</td>" +
 		"<td><table width='100%' class='no-border'><tr class='no-border'><td class='no-border'><i id='edit_button"+no+"' class='fa fa-pencil edit fa-color-blue' onclick='edit_row(" + no + ")'></i></td><td class='no-border'><i id='save_button"+no+"' class='fa fa-floppy-o save fa-color-blue' onclick='save_row("+no+")'></i></td><td class='no-border'><!-- --></td><td class='no-border'><i class='fa fa-trash delete alert-text' onclick='delete_row("+no+")'></i></td></tr></table></td></tr>";
 		
 document.getElementById("new_vehicle_id").value = "";
 document.getElementById("new_GW_client_id").value = "";
 document.getElementById("new_ND_client_id").value = "";
 document.getElementById("new_ND_device_id").value = "";
 document.getElementById("new_GW_ND_Pairing").value = "";
 document.getElementById("new_ND_subzone_type").value = "";
 document.getElementById("new_ND_subzone_name").value = "";
 document.getElementById("new_ND_type").value = "";
 document.getElementById("new_ND_status").value = "";
 document.getElementById("lastValue").value = parseInt(no)+1;
}

function edit_row_stationary(no)
{
 document.getElementById("edit_button_stationary"+no).style.display="none";
 document.getElementById("save_button_stationary"+no).style.display="block";
 //document.getElementById("cancel_button"+no).style.display="block";
 
 var zone_id=document.getElementById(no+"_Zone_id_stationary");
 var gw_client_id=document.getElementById(no+"_GW_client_id_stationary");
 var nd_device_id=document.getElementById(no+"_ND_device_id_stationary");
 var gw_nd_pairing=document.getElementById(no+"_GW_ND_Pairing_stationary");
 var nd_subzone_type=document.getElementById(no+"_ND_subzone_type_stationary");
 var nd_subzone_name=document.getElementById(no+"_ND_subzone_name_stationary");
 var nd_type=document.getElementById(no+"_ND_type_stationary");
 var nd_status=document.getElementById(no+"_ND_status_stationary");
 	
 var zone_id_data=zone_id.innerHTML;
 var gw_client_id_data=gw_client_id.innerHTML;
 var nd_device_id_data=nd_device_id.innerHTML;
 var gw_nd_pairing_data=gw_nd_pairing.innerHTML;
 var nd_subzone_type_data=nd_subzone_type.innerHTML;
 var nd_subzone_name_data=nd_subzone_name.innerHTML;
 var nd_type_data=nd_type.innerHTML;
 var nd_status_data=nd_status.innerHTML;
	
 zone_id.innerHTML="<input type='text' class='form-control-no-height' id='zone_id_stationary_text"+no+"' value='"+zone_id_data+"'>";
 gw_client_id.innerHTML="<input type='text' class='form-control-no-height' id='gw_client_id_stationary_text"+no+"' value='"+gw_client_id_data+"'>";
 nd_device_id.innerHTML="<input type='text' class='form-control-no-height' id='nd_device_id_stationary_text"+no+"' value='"+nd_device_id_data+"'>";
 gw_nd_pairing.innerHTML="<input type='text' class='form-control-no-height' id='gw_nd_pairing_stationary_text"+no+"' value='"+gw_nd_pairing_data+"'>";
 nd_subzone_type.innerHTML="<input type='text' class='form-control-no-height' id='nd_subzone_type_stationary_text"+no+"' value='"+nd_subzone_type_data+"'>";
 nd_subzone_name.innerHTML="<input type='text' class='form-control-no-height' id='nd_subzone_name_stationary_text"+no+"' value='"+nd_subzone_name_data+"'>";
 nd_type.innerHTML="<input type='text' class='form-control-no-height' id='nd_type_stationary_text"+no+"' value='"+nd_type_data+"'>";
 nd_status.innerHTML="<input type='text' class='form-control-no-height' id='nd_status_stationary_text"+no+"' value='"+nd_status_data+"'>";
}

function save_row_stationary(no)
{
 var zone_id_val=document.getElementById("zone_id_stationary_text"+no).value;
 var gw_client_id_val=document.getElementById("gw_client_id_stationary_text"+no).value;
 var nd_device_id_val=document.getElementById("nd_device_id_stationary_text"+no).value;
 var gw_nd_pairing_val=document.getElementById("gw_nd_pairing_stationary_text"+no).value;
 var nd_subzone_type_val=document.getElementById("nd_subzone_type_stationary_text"+no).value;
 var nd_subzone_name_val=document.getElementById("nd_subzone_name_stationary_text"+no).value;
 var nd_type_val=document.getElementById("nd_type_stationary_text"+no).value;
 var nd_status_val=document.getElementById("nd_status_stationary_text"+no).value;
 
 if(zone_id_val == "") { alert("Please enter a value for Zone Id"); document.getElementById("zone_id_stationary_text"+no).focus(); return;}
 if(gw_client_id_val == "") { alert("Please enter a value for GW_Client_Id"); document.getElementById("gw_client_id_stationary_text"+no).focus(); return;}
 if(nd_device_id_val == "") { alert("Please enter a value for ND_Device_Id"); document.getElementById("nd_device_id_stationary_text"+no).focus(); return;}
 if(gw_nd_pairing_val == "") { alert("Please enter a value for GW_ND_Pairing"); document.getElementById("gw_nd_pairing_stationary_text"+no).focus(); return;}
 if(nd_subzone_type_val == "") { alert("Please enter a value for ND_Subzone_Type"); document.getElementById("nd_subzone_type_stationary_text"+no).focus(); return;}
 if(nd_subzone_name_val == "") { alert("Please enter a value for ND_Subzone_Name"); document.getElementById("nd_subzone_name_stationary_text"+no).focus();return;}
 if(nd_type_val == "") { alert("Please enter a value for ND_Type"); document.getElementById("nd_type_stationary_text"+no).focus(); return;}
 if(nd_status_val == "") { alert("Please enter a value for ND_Status"); document.getElementById("nd_status_stationary_text"+no).focus(); return;}
 
 
 document.getElementById(no+"_Zone_id_stationary").innerHTML=zone_id_val;
 document.getElementById(no+"_GW_client_id_stationary").innerHTML=gw_client_id_val;
 document.getElementById(no+"_ND_device_id_stationary").innerHTML=nd_device_id_val;
 document.getElementById(no+"_GW_ND_Pairing_stationary").innerHTML=gw_nd_pairing_val;
 document.getElementById(no+"_ND_subzone_type_stationary").innerHTML=nd_subzone_type_val;
 document.getElementById(no+"_ND_subzone_name_stationary").innerHTML=nd_subzone_name_val;
 document.getElementById(no+"_ND_type_stationary").innerHTML=nd_type_val;
 document.getElementById(no+"_ND_status_stationary").innerHTML=nd_status_val;
 
 document.getElementById("edit_button_stationary"+no).style.display="block";
 document.getElementById("save_button_stationary"+no).style.display="none";
 //document.getElementById("cancel_button"+no).style.display="none";
 
 var nd_client_id=document.getElementById(no+"_ND_client_id_stationary");
 var nd_client_id_val=nd_client_id.innerHTML;
 
 	var posting = $.post("../../pages/jsp/GetNodeData.jsp",
				{
					Zone_Id:zone_id_val,
					GW_client_Id:gw_client_id_val,
					ND_device_id_val:nd_device_id_val,
					GW_nd_pairing_val:gw_nd_pairing_val,
					ND_subzone_type_val:nd_subzone_type_val,
					ND_subzone_name_val:nd_subzone_name_val,
					ND_type_val:nd_type_val,
					ND_status_val:nd_status_val,
					ND_client_id_val:nd_client_id_val,
					query:"updateStationaryNodeMap"
				},
				function(data,status){
					//alert(data);
					xmlDoc = loadXMLString(data);
					if($(xmlDoc).find("rows").length == 0) {
						return;
					}
					
					$(xmlDoc).find("row").each(function(){
						var id = $(this).find("Return").text();
					});
					
				});
 
}

function cancel_row_stationary(no) {
	document.getElementById("edit_button_stationary"+no).style.display="block";
	 document.getElementById("save_button_stationary"+no).style.display="none";
	 //document.getElementById("cancel_button"+no).style.display="none";
	 
	var zone_id=document.getElementById(no+"_Zone_id_stationary");
	 var gw_client_id=document.getElementById(no+"_GW_client_id_stationary");
	 var nd_client_id=document.getElementById(no+"_ND_client_id_stationary");
	 var nd_device_id=document.getElementById(no+"_ND_device_id_stationary");
	 var gw_nd_pairing=document.getElementById(no+"_GW_ND_Pairing_stationary");
	 var nd_subzone_type=document.getElementById(no+"_ND_subzone_type_stationary");
	 var nd_subzone_name=document.getElementById(no+"_ND_subzone_name_stationary");
	 var nd_type=document.getElementById(no+"_ND_type_stationary");
	 var nd_status=document.getElementById(no+"_ND_status_stationary");
	 	
	 var zone_id_data=zone_id.innerHTML;
	 var gw_client_id_data=gw_client_id.innerHTML;
	 var nd_client_id_data=nd_client_id.innerHTML;
	 var nd_device_id_data=nd_device_id.innerHTML;
	 var gw_nd_pairing_data=gw_nd_pairing.innerHTML;
	 var nd_subzone_type_data=nd_subzone_type.innerHTML;
	 var nd_subzone_name_data=nd_subzone_name.innerHTML;
	 var nd_type_data=nd_type.innerHTML;
	 var nd_status_data=nd_status.innerHTML;
		
	 zone_id.innerHTML="<input type='text' class='form-control-no-height' id='zone_id_stationary_text"+no+"' value='"+zone_id_data+"'>";
	 gw_client_id.innerHTML="<input type='text' class='form-control-no-height' id='gw_client_id_stationary_text"+no+"' value='"+gw_client_id_data+"'>";
	 nd_device_id.innerHTML="<input type='text' class='form-control-no-height' id='nd_device_id_stationary_text"+no+"' value='"+nd_device_id_data+"'>";
	 gw_nd_pairing.innerHTML="<input type='text' class='form-control-no-height' id='gw_nd_pairing_stationary_text"+no+"' value='"+gw_nd_pairing_data+"'>";
	 nd_subzone_type.innerHTML="<input type='text' class='form-control-no-height' id='nd_subzone_type_stationary_text"+no+"' value='"+nd_subzone_type_data+"'>";
	 nd_subzone_name.innerHTML="<input type='text' class='form-control-no-height' id='nd_subzone_name_stationary_text"+no+"' value='"+nd_subzone_name_data+"'>";
	 nd_type.innerHTML="<input type='text' class='form-control-no-height' id='nd_type_stationary_text"+no+"' value='"+nd_type_data+"'>";
	 nd_status.innerHTML="<input type='text' class='form-control-no-height' id='nd_status_stationary_text"+no+"' value='"+nd_status_data+"'>";
}

function delete_row_stationary(no)
{ 
	
	var nd_client_id=document.getElementById(no+"_ND_client_id_stationary");
	 var nd_client_id_val=nd_client_id.innerHTML;
 var posting = $.post("../../pages/jsp/GetNodeData.jsp",
			{
				ND_client_id_val:nd_client_id_val,
				query:"deleteStationaryNodeMap"
			},
			function(data,status){
				//alert(data);
				xmlDoc = loadXMLString(data);
				if($(xmlDoc).find("rows").length == 0) {
					return;
				}
				
				$(xmlDoc).find("row").each(function(){
					var id = $(this).find("Return").text();
				});
				
			});
 document.getElementById("row_stationary"+no).outerHTML="";
}

function add_row_stationary()
{
	var no = document.getElementById("lastValue_stationary").value;
	var zone_id_val=document.getElementById("new_zone_id_stationary").value;
	 var gw_client_id_val=document.getElementById("new_GW_client_id_stationary").value;
	 var nd_client_id_val=document.getElementById("new_ND_client_id_stationary").value;
	 var nd_device_id_val=document.getElementById("new_ND_device_id_stationary").value;
	 var gw_nd_pairing_val=document.getElementById("new_GW_ND_Pairing_stationary").value;
	 var nd_subzone_type_val=document.getElementById("new_ND_subzone_type_stationary").value;
	 var nd_subzone_name_val=document.getElementById("new_ND_subzone_name_stationary").value;
	 var nd_type_val=document.getElementById("new_ND_type_stationary").value;
	 var nd_status_val=document.getElementById("new_ND_status_stationary").value;
	 
	 if(zone_id_val == "") { alert("Please enter a value for Zone Id"); document.getElementById("new_zone_id_stationary").focus(); return;}
	 if(gw_client_id_val == "") { alert("Please enter a value for GW_Client_Id"); document.getElementById("new_GW_client_id_stationary").focus(); return;}
	 if(nd_client_id_val == "") { alert("Please enter a value for ND_Client_Id"); document.getElementById("new_ND_client_id_stationary").focus(); return;}
	 if(nd_device_id_val == "") { alert("Please enter a value for ND_Device_Id"); document.getElementById("new_ND_device_id_stationary").focus(); return;}
	 if(gw_nd_pairing_val == "") { alert("Please enter a value for GW_ND_Pairing"); document.getElementById("new_GW_ND_Pairing_stationary").focus(); return;}
	 if(nd_subzone_type_val == "") { alert("Please enter a value for ND_Subzone_Type"); document.getElementById("new_ND_subzone_type_stationary").focus(); return;}
	 if(nd_subzone_name_val == "") { alert("Please enter a value for ND_Subzone_Name"); document.getElementById("new_ND_subzone_name_stationary").focus();return;}
	 if(nd_type_val == "") { alert("Please enter a value for ND_Type"); document.getElementById("new_ND_type_stationary").focus(); return;}
	 if(nd_status_val == "") { alert("Please enter a value for ND_Status"); document.getElementById("new_ND_status_stationary").focus(); return;}
	 
	
	 var posting = $.post("../../pages/jsp/GetNodeData.jsp",
				{
					Zone_Id:zone_id_val,
					GW_client_Id:gw_client_id_val,
					ND_device_id_val:nd_device_id_val,
					GW_nd_pairing_val:gw_nd_pairing_val,
					ND_subzone_type_val:nd_subzone_type_val,
					ND_subzone_name_val:nd_subzone_name_val,
					ND_type_val:nd_type_val,
					ND_status_val:nd_status_val,
					ND_client_id_val:nd_client_id_val,
					query:"insertStationaryNodeMap"
				},
				function(data,status){
					//alert(data);
					xmlDoc = loadXMLString(data);
					if($(xmlDoc).find("rows").length == 0) {
						return;
					}
					
					$(xmlDoc).find("row").each(function(){
						var id = $(this).find("Return").text();
					});
					
				});
		
 var table=document.getElementById("data_table_stationary");
 var table_len=(table.rows.length)-1;
 var row = table.insertRow(table_len).outerHTML="<tr id='row_stationary"+no+"'><td id='"+no+"_Zone_id_stationary'>"+zone_id_val+"</td>" +
 		"<td id='"+no+"_GW_client_id_stationary'>"+gw_client_id_val+"</td>" +
 		"<td id='"+no+"_ND_client_id_stationary'>"+nd_client_id_val+"</td>" +
 		"<td id='"+no+"_ND_device_id_stationary'>"+nd_device_id_val+"</td>" +
 		"<td id='"+no+"_GW_ND_Pairing_stationary'>"+gw_nd_pairing_val+"</td>" +
 		"<td id='"+no+"_ND_subzone_type_stationary'>"+nd_subzone_type_val+"</td>" +
 		"<td id='"+no+"_ND_subzone_name_stationary'>"+nd_subzone_name_val+"</td>" +
 		"<td id='"+no+"_ND_type_stationary'>"+nd_type_val+"</td>" +
 		"<td id='"+no+"_ND_status_stationary'>"+nd_status_val+"</td>" +
 		"<td><table width='100%' class='no-border'><tr class='no-border'><td class='no-border'><i id='edit_button_stationary"+no+"' class='fa fa-pencil edit fa-color-blue' onclick='edit_row_stationary(" + no + ")'></i></td><td class='no-border'><i id='save_button_stationary"+no+"' class='fa fa-floppy-o save fa-color-blue' onclick='save_row_stationary("+no+")'></i></td><td class='no-border'><!-- --></td><td class='no-border'><i class='fa fa-trash delete alert-text' onclick='delete_row_stationary("+no+")'></i></td></tr></table></td></tr>";
 		
 document.getElementById("new_zone_id_stationary").value = "";
 document.getElementById("new_GW_client_id_stationary").value = "";
 document.getElementById("new_ND_client_id_stationary").value = "";
 document.getElementById("new_ND_device_id_stationary").value = "";
 document.getElementById("new_GW_ND_Pairing_stationary").value = "";
 document.getElementById("new_ND_subzone_type_stationary").value = "";
 document.getElementById("new_ND_subzone_name_stationary").value = "";
 document.getElementById("new_ND_type_stationary").value = "";
 document.getElementById("new_ND_status_stationary").value = "";
 document.getElementById("lastValue_stationary").value = parseInt(no)+1;
}

function loadXMLString(txt) 
{
	if (window.DOMParser)
	{
		parser=new DOMParser();
		xmlDoc=parser.parseFromString(txt,"text/xml");
	}
	else // Internet Explorer
	{
		xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
		xmlDoc.async=false;
		xmlDoc.loadXML(txt); 
	}
	return xmlDoc;
}