

var xmlDoc = "";
var directionsService1 = "";
var directionsService2 = "";
var directionsService3 = "";
var directionsService4 = "";
var directionsService5 = "";
var directionsService6 = "";
var MY_MAPTYPE_ID = 'custom_style';
var directionsDisplay1;
var directionsDisplay2;
var directionsDisplay3;
var directionsDisplay4;
var directionsDisplay5;
var directionsDisplay6;
function initialize() {
	//xmlDoc = initializeData();
	directionsService1 = new google.maps.DirectionsService();
	directionsService2 = new google.maps.DirectionsService();
	directionsService3 = new google.maps.DirectionsService();
	directionsService4 = new google.maps.DirectionsService();
	directionsService5 = new google.maps.DirectionsService();
	directionsService6 = new google.maps.DirectionsService();
	var map1; var map2; var map3; var map4; var map5; var map6;
	var posting = $.post("../../pages/jsp/GetMapDashboardData.jsp",
			{
				alert_type:$('#CatID').val(),
				time: $('#alertsFilter').val(),
				type: "Dashboard"
			},
			function(data,status){
				xmlDoc = loadXMLString(data);
				if($(xmlDoc).find("rows").length == 0) {
					$("#divResults").html("No records available. Please search again.").show();
					return;
				}
			});
	//alert("xmlDoc: " + xmlDoc);
	posting.done(function() {
	try{
		 directionsDisplay1 = new google.maps.DirectionsRenderer({suppressMarkers:true,
			 polylineOptions: {
			      strokeColor: "red"
			 }
		 });
		 directionsDisplay2 = new google.maps.DirectionsRenderer({suppressMarkers:true,
			 polylineOptions: {
			      strokeColor: "gray"
			    	  }});
		 directionsDisplay3 = new google.maps.DirectionsRenderer({suppressMarkers:true});
		 directionsDisplay4 = new google.maps.DirectionsRenderer({suppressMarkers:true});
		 directionsDisplay5 = new google.maps.DirectionsRenderer({suppressMarkers:true});
		 directionsDisplay6 = new google.maps.DirectionsRenderer({suppressMarkers:true});
		    if (jQuery('#map1').length > 0) {
		        // var locations =
				// jQuery.parseJSON(MapPoints);

		        map1 = new google.maps.Map(document.getElementById('map1'), {
		            mapTypeId: google.maps.MapTypeId.ROADMAP,
		            scrollwheel: false
		        });
		        directionsDisplay1.setMap(map1);
		        infowindow = new google.maps.InfoWindow();
		        flightPlanCoordinates1 = [];
		        bounds1 = new google.maps.LatLngBounds();
		    }
		    if (jQuery('#map4').length > 0) {
		        // var locations =
				// jQuery.parseJSON(MapPoints);

		        map4 = new google.maps.Map(document.getElementById('map4'), {
		            mapTypeId: google.maps.MapTypeId.ROADMAP,
		            scrollwheel: false
		        });
		        directionsDisplay4.setMap(map4);
		        infowindow = new google.maps.InfoWindow();
		        flightPlanCoordinates4 = [];
		        bounds4 = new google.maps.LatLngBounds();
		    }
		    if (jQuery('#map2').length > 0) {
		        // var locations =
				// jQuery.parseJSON(MapPoints);

		        map2 = new google.maps.Map(document.getElementById('map2'), {
		            mapTypeId: google.maps.MapTypeId.ROADMAP,
		            scrollwheel: false
		        });
		        directionsDisplay2.setMap(map2);
		        infowindow = new google.maps.InfoWindow();
		        flightPlanCoordinates2 = [];
		        bounds2 = new google.maps.LatLngBounds();
		    }
		    if (jQuery('#map5').length > 0) {
		        // var locations =
				// jQuery.parseJSON(MapPoints);

		        map5 = new google.maps.Map(document.getElementById('map5'), {
		            mapTypeId: google.maps.MapTypeId.ROADMAP,
		            scrollwheel: false
		        });
		        directionsDisplay5.setMap(map5);
		        infowindow = new google.maps.InfoWindow();
		        flightPlanCoordinates5 = [];
		        bounds5 = new google.maps.LatLngBounds();
		    }
		    if (jQuery('#map3').length > 0) {
		        // var locations =
				// jQuery.parseJSON(MapPoints);

		        map3 = new google.maps.Map(document.getElementById('map3'), {
		            mapTypeId: google.maps.MapTypeId.ROADMAP,
		            scrollwheel: false
		        });
		        directionsDisplay3.setMap(map3);
		        infowindow = new google.maps.InfoWindow();
		        flightPlanCoordinates3 = [];
		        bounds3 = new google.maps.LatLngBounds();
		    }
		    if (jQuery('#map6').length > 0) {
		        // var locations =
				// jQuery.parseJSON(MapPoints);

		        map6 = new google.maps.Map(document.getElementById('map6'), {
		            mapTypeId: google.maps.MapTypeId.ROADMAP,
		            scrollwheel: false
		        });
		        directionsDisplay6.setMap(map6);
		        infowindow = new google.maps.InfoWindow();
		        flightPlanCoordinates6 = [];
		        bounds6 = new google.maps.LatLngBounds();
		    }
		    var image = "http://www.googlemapsmarkers.com/v1/009900";
						//alert($(xmlDoc).find("rows").length);
		    var iLongHaul = 0;
			$(xmlDoc).find("row").each(function(){
				
				var sRoute = $(this).find("RouteType").text();
				var sVehicleId = $(this).find("VehicleId").text();
				var sStatus = $(this).find("Status").text();
				var sSource = $(this).find("Source").text();
				var sDest = $(this).find("Destination").text();
				//alert(sRoute + "  " + sVehicleId + "  " + sStatus + "  " + sSource + "  "+ sDest);
				if(sSource == null || sSource == "" || sSource == "null") {
					sSource = "13.011920";
				}
				if(sDest == null || sDest == "" || sDest == "null") {
					sDest = "77.539840";
				}
				var titleMap = "";
				if(sStatus == "Red"){
					image = "../../dist/img/truck_red.png";
	        	} else {
	        		if(sStatus == "Green") {
	        			image = "../../dist/img/truck_green.png";
	        		} else {
	        			image = "http://www.googlemapsmarkers.com/v1/009900";
	        		}
	        	}
				var sLink = "LocalRunsDetail.jsp?VehicleId=" + sVehicleId;
				if(sRoute == "Long Haul") {
					marker = new google.maps.Marker({
	                    position: new google.maps.LatLng(sSource, sDest),
	                    map: map1,
	                    icon: image,
	                    url: sLink
	                });
	        	
					map1.setZoom(17);
					map1.panTo(marker.position);
	        		flightPlanCoordinates1.push(marker.getPosition());
	        	    bounds1.extend(marker.position);
	        	    google.maps.event.addListener(marker, 'click', function(){
	        	    	window.location.href = this.url; 
	        	     });
	                google.maps.event.addListener(marker, 'click', (function (marker, iLongHaul) {
	                    return function () {
	                        infowindow.setContent("Vehicle ID: " + sVehicleId);
	                        infowindow.open(map1, marker);
	                    }
	                })(marker, iLongHaul));
	                iLongHaul++;
	                marker = new google.maps.Marker({
	                    position: new google.maps.LatLng(sSource, sDest),
	                    map: map4,
	                    icon: image,
	                    url: sLink
	                });
	        	
					map4.setZoom(17);
					map4.panTo(marker.position);
	        		flightPlanCoordinates4.push(marker.getPosition());
	        	    bounds4.extend(marker.position);
	        	    google.maps.event.addListener(marker, 'click', function(){
	        	    	window.location.href = this.url; 
	        	     });
	                google.maps.event.addListener(marker, 'click', (function (marker, iLongHaul) {
	                    return function () {
	                        infowindow.setContent("Vehicle ID: " + sVehicleId);
	                        infowindow.open(map4, marker);
	                    }
	                })(marker, iLongHaul));
	                iLongHaul++;
				} else {
					if(sRoute == "Short Haul") {
					marker = new google.maps.Marker({
	                    position: new google.maps.LatLng(sSource, sDest),
	                    map: map2,
	                    icon: image,
	                    url: sLink
	                });
	        	
					map2.setZoom(17);
					map2.panTo(marker.position);
	        		flightPlanCoordinates2.push(marker.getPosition());
	        	    bounds2.extend(marker.position);
	        	    google.maps.event.addListener(marker, 'click', function(){
	        	    	window.location.href = this.url; 
	        	     });
	                google.maps.event.addListener(marker, 'click', (function (marker, iLongHaul) {
	                    return function () {
	                        infowindow.setContent("Vehicle ID: " + sVehicleId);
	                        infowindow.open(map2, marker);
	                    }
	                })(marker, iLongHaul));
	                iLongHaul++;
	                marker = new google.maps.Marker({
	                    position: new google.maps.LatLng(sSource, sDest),
	                    map: map5,
	                    icon: image,
	                    url: sLink
	                });
	        	
					map5.setZoom(17);
					map5.panTo(marker.position);
	        		flightPlanCoordinates5.push(marker.getPosition());
	        	    bounds5.extend(marker.position);
	        	    google.maps.event.addListener(marker, 'click', function(){
	        	    	window.location.href = this.url; 
	        	     });
	                google.maps.event.addListener(marker, 'click', (function (marker, iLongHaul) {
	                    return function () {
	                        infowindow.setContent("Vehicle ID: " + sVehicleId);
	                        infowindow.open(map5, marker);
	                    }
	                })(marker, iLongHaul));
	                iLongHaul++;
				} else {
					marker = new google.maps.Marker({
	                    position: new google.maps.LatLng(sSource, sDest),
	                    map: map3,
	                    icon: image,
	                    url: sLink
	                });
	        	
					map3.setZoom(17);
					map3.panTo(marker.position);
	        		flightPlanCoordinates3.push(marker.getPosition());
	        	    bounds3.extend(marker.position);
	        	    google.maps.event.addListener(marker, 'click', function(){
	        	    	window.location.href = this.url; 
	        	     });
	                google.maps.event.addListener(marker, 'click', (function (marker, iLongHaul) {
	                    return function () {
	                        infowindow.setContent("Vehicle ID: " + sVehicleId);
	                        infowindow.open(map3, marker);
	                    }
	                })(marker, iLongHaul));
	                iLongHaul++;
	                marker = new google.maps.Marker({
	                    position: new google.maps.LatLng(sSource, sDest),
	                    map: map6,
	                    icon: image,
	                    url: sLink
	                });
	        	
					map6.setZoom(17);
					map6.panTo(marker.position);
	        		flightPlanCoordinates6.push(marker.getPosition());
	        	    bounds6.extend(marker.position);
	        	    google.maps.event.addListener(marker, 'click', function(){
	        	    	window.location.href = this.url; 
	        	     });
	                google.maps.event.addListener(marker, 'click', (function (marker, iLongHaul) {
	                    return function () {
	                        infowindow.setContent("Vehicle ID: " + sVehicleId);
	                        infowindow.open(map6, marker);
	                    }
	                })(marker, iLongHaul));
	                iLongHaul++;
				}
				}
			
			});
			map1.fitBounds(bounds1);
			map2.fitBounds(bounds2);
			map3.fitBounds(bounds3);
			map4.fitBounds(bounds4);
			map5.fitBounds(bounds5);
			map6.fitBounds(bounds6);
			// directions service
			var start1 = flightPlanCoordinates1[0];
			var end1 = flightPlanCoordinates1[flightPlanCoordinates1.length - 1];
			var waypts1 = [];
			for (var i = 1; i < flightPlanCoordinates1.length - 1; i++) {
				waypts1.push({
					location: flightPlanCoordinates1[i],
				    stopover: true
				});
			}
			var start4 = flightPlanCoordinates4[0];
			var end4 = flightPlanCoordinates4[flightPlanCoordinates4.length - 1];
			var waypts4 = [];
			for (var i = 1; i < flightPlanCoordinates4.length - 1; i++) {
				waypts4.push({
					location: flightPlanCoordinates4[i],
				    stopover: true
				});
			}
			// directions service
	        var start2 = flightPlanCoordinates2[0];
	        var end2 = flightPlanCoordinates2[flightPlanCoordinates2.length - 1];
	        var waypts2 = [];
	        for (var i = 1; i < flightPlanCoordinates2.length - 1; i++) {
	            waypts2.push({
	                location: flightPlanCoordinates2[i],
	                stopover: true
	            });
	        }
	        var start5 = flightPlanCoordinates5[0];
	        var end5 = flightPlanCoordinates5[flightPlanCoordinates5.length - 1];
	        var waypts5 = [];
	        for (var i = 1; i < flightPlanCoordinates5.length - 1; i++) {
	            waypts5.push({
	                location: flightPlanCoordinates5[i],
	                stopover: true
	            });
	        }
	        var start3 = flightPlanCoordinates3[0];
	        var end3 = flightPlanCoordinates3[flightPlanCoordinates3.length - 1];
	        var waypts3 = [];
	        for (var i = 1; i < flightPlanCoordinates3.length - 1; i++) {
	        	waypts3.push({
	        		location: flightPlanCoordinates3[i],
	        		stopover: true
	        	});
	        }
	        var start6 = flightPlanCoordinates6[0];
	        var end6 = flightPlanCoordinates6[flightPlanCoordinates6.length - 1];
	        var waypts6 = [];
	        for (var i = 1; i < flightPlanCoordinates6.length - 1; i++) {
	        	waypts6.push({
	        		location: flightPlanCoordinates6[i],
	        		stopover: true
	        	});
	        }
	       // calcRoute3(start3, end3, waypts3);
	       // calcRoute2(start2, end2, waypts2);
	       // calcRoute1(start1, end1, waypts1);
		} catch (e) {
			alert(e);
		}
	});
	}

function loadXMLString(txt) 
{
	if (window.DOMParser)
	{
		parser=new DOMParser();
		xmlDoc=parser.parseFromString(txt,"text/xml");
	}
	else // Internet Explorer
	{
		xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
		xmlDoc.async=false;
		xmlDoc.loadXML(txt); 
	}
	return xmlDoc;
}

function calcRoute1(start, end, waypts) {
    var request = {
        origin: start,
        destination: end,
        waypoints: waypts,
        optimizeWaypoints: true,
        travelMode: google.maps.TravelMode.DRIVING
    };
    directionsService1.route(request, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay1.setDirections(response);
            var route = response.routes[0];
            /*var summaryPanel = document.getElementById('directions_panel');
            summaryPanel.innerHTML = '';
            // For each route, display summary information.
            for (var i = 0; i < route.legs.length; i++) {
                var routeSegment = i + 1;
                summaryPanel.innerHTML += '<b>Route Segment: ' + routeSegment + '</b><br>';
                summaryPanel.innerHTML += route.legs[i].start_address + ' to ';
                summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
                summaryPanel.innerHTML += route.legs[i].distance.text + '<br><br>';
            }*/
        }
    });
}


function calcRoute2(start, end, waypts) {
    var request = {
        origin: start,
        destination: end,
        waypoints: waypts,
        optimizeWaypoints: true,
        travelMode: google.maps.TravelMode.DRIVING
    };
    directionsService2.route(request, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay2.setDirections(response);
            /*var route = response.routes[0];
            var summaryPanel = document.getElementById('directions_panel');
            //summaryPanel.innerHTML = '';
            // For each route, display summary information.
            for (var i = 0; i < route.legs.length; i++) {
                var routeSegment = i + 1;
                summaryPanel.innerHTML += '<b>Route Segment: ' + routeSegment + '</b><br>';
                summaryPanel.innerHTML += route.legs[i].start_address + ' to ';
                summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
                summaryPanel.innerHTML += route.legs[i].distance.text + '<br><br>';
            }*/
        }
    });
}

function calcRoute3(start, end, waypts) {
    var request = {
        origin: start,
        destination: end,
        waypoints: waypts,
        optimizeWaypoints: true,
        travelMode: google.maps.TravelMode.DRIVING
    };
    directionsService3.route(request, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay3.setDirections(response);
            /*var route = response.routes[0];
            var summaryPanel = document.getElementById('directions_panel');
            //summaryPanel.innerHTML = '';
            // For each route, display summary information.
            for (var i = 0; i < route.legs.length; i++) {
                var routeSegment = i + 1;
                summaryPanel.innerHTML += '<b>Route Segment: ' + routeSegment + '</b><br>';
                summaryPanel.innerHTML += route.legs[i].start_address + ' to ';
                summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
                summaryPanel.innerHTML += route.legs[i].distance.text + '<br><br>';
            }*/
        }
    });
}










//var locations = {
//		address:[
//		         {
//		        	   "RouteType": "Local Run",
//		        	   "VehicleId": "KA123-123",
//		        	   "Status": "Green",
//		        	   "Source": "12.973860",
//		        	   "Destination": "77.641770"
//		        	 },
//		        	 {
//		        		 "RouteType": "Local Run",
//			        	   "VehicleId": "KA123-456",
//			        	   "Status": "Green",
//			        	   "Source": "12.983850",
//			        	   "Destination": "77.676620"
//		        	 },
//		        	 {
//		        		 "RouteType": "Local Run",
//			        	   "VehicleId": "KA123-789",
//			        	   "Status": "Green",
//			        	   "Source": "12.995870",
//			        	   "Destination": "77.694680"
//		        	 },
//		        	 {
//		        		 "RouteType": "Local Run",
//			        	   "VehicleId": "KA123-111",
//			        	   "Status": "Red",
//			        	   "Source": "12.991860",
//			        	   "Destination": "77.714350"
//		        	 },
//		        	 {
//		        		 "RouteType": "Local Run",
//			        	   "VehicleId": "KA123-112",
//			        	   "Status": "Red",
//			        	   "Source": "12.988290",
//			        	   "Destination": "77.731430"
//		        	 }
//		        	 ]};
//
//var locations1 = {
//		address:[
//		         {
//		        	   "RouteType": "Long Haul",
//		        	   "VehicleId": "KA123-123",
//		        	   "Status": "Green",
//		        	   "Source": "12.973860",
//		        	   "Destination": "77.641770"
//		        	 },
//		        	 {
//		        		 "RouteType": "Long Haul",
//			        	   "VehicleId": "KA123-456",
//			        	   "Status": "Red",
//			        	   "Source": "12.983850",
//			        	   "Destination": "77.676620"
//		        	 },
//		        	 {
//		        		 "RouteType": "Long Haul",
//			        	   "VehicleId": "KA123-789",
//			        	   "Status": "Green",
//			        	   "Source": "12.995870",
//			        	   "Destination": "77.694680"
//		        	 },
//		        	 {
//		        		 "RouteType": "Long Haul",
//			        	   "VehicleId": "KA123-111",
//			        	   "Status": "Green",
//			        	   "Source": "12.991860",
//			        	   "Destination": "77.714350"
//		        	 },
//		        	 {
//		        		 "RouteType": "Long Haul",
//			        	   "VehicleId": "KA123-112",
//			        	   "Status": "Green",
//			        	   "Source": "12.988290",
//			        	   "Destination": "77.731430"
//		        	 }
//		        	 ]};
//
//var locations2 = {
//		address:[
//		         {
//		        	   "RouteType": "Short Haul",
//		        	   "VehicleId": "KA123-123",
//		        	   "Status": "Green",
//		        	   "Source": "12.973860",
//		        	   "Destination": "77.641770"
//		        	 },
//		        	 {
//		        		 "RouteType": "Short Haul",
//			        	   "VehicleId": "KA123-456",
//			        	   "Status": "Red",
//			        	   "Source": "12.983850",
//			        	   "Destination": "77.676620"
//		        	 },
//		        	 {
//		        		 "RouteType": "Short Haul",
//			        	   "VehicleId": "KA123-789",
//			        	   "Status": "Green",
//			        	   "Source": "12.995870",
//			        	   "Destination": "77.694680"
//		        	 },
//		        	 {
//		        		 "RouteType": "Short Haul",
//			        	   "VehicleId": "KA123-111",
//			        	   "Status": "Green",
//			        	   "Source": "12.991860",
//			        	   "Destination": "77.714350"
//		        	 },
//		        	 {
//		        		 "RouteType": "Short Haul",
//			        	   "VehicleId": "KA123-112",
//			        	   "Status": "Red",
//			        	   "Source": "12.988290",
//			        	   "Destination": "77.731430"
//		        	 }
//		        	 ]};
//
//var xmlDoc = "";
//var directionsService1 = "";
//var directionsService2 = "";
//var directionsService3 = "";
//var MY_MAPTYPE_ID = 'custom_style';
//var directionsDisplay1;
//var directionsDisplay2;
//var directionsDisplay3;
//function initialize() {
//	//xmlDoc = initializeData();
//	
//	
//	directionsService1 = new google.maps.DirectionsService();
//	directionsService2 = new google.maps.DirectionsService();
//	directionsService3 = new google.maps.DirectionsService();
//	var map1; var map2; var map3;
//		
//	//alert("xmlDoc: " + xmlDoc);
//	try{
//		 directionsDisplay1 = new google.maps.DirectionsRenderer({suppressMarkers:true,
//			 polylineOptions: {
//			      strokeColor: "red"
//			 }
//		 });
//		 directionsDisplay2 = new google.maps.DirectionsRenderer({suppressMarkers:true,
//			 polylineOptions: {
//			      strokeColor: "gray"
//			    	  }});
//		 directionsDisplay3 = new google.maps.DirectionsRenderer({suppressMarkers:true});
//		    if (jQuery('#map1').length > 0) {
//		        // var locations =
//				// jQuery.parseJSON(MapPoints);
//
//		        map1 = new google.maps.Map(document.getElementById('map1'), {
//		            mapTypeId: google.maps.MapTypeId.ROADMAP,
//		            scrollwheel: false
//		        });
//		        directionsDisplay1.setMap(map1);
//		        infowindow = new google.maps.InfoWindow();
//		        flightPlanCoordinates1 = [];
//		        bounds1 = new google.maps.LatLngBounds();
//		    }
//		    if (jQuery('#map2').length > 0) {
//		        // var locations =
//				// jQuery.parseJSON(MapPoints);
//
//		        map2 = new google.maps.Map(document.getElementById('map2'), {
//		            mapTypeId: google.maps.MapTypeId.ROADMAP,
//		            scrollwheel: false
//		        });
//		        directionsDisplay2.setMap(map2);
//		        infowindow = new google.maps.InfoWindow();
//		        flightPlanCoordinates2 = [];
//		        bounds2 = new google.maps.LatLngBounds();
//		    }
//		    if (jQuery('#map3').length > 0) {
//		        // var locations =
//				// jQuery.parseJSON(MapPoints);
//
//		        map3 = new google.maps.Map(document.getElementById('map3'), {
//		            mapTypeId: google.maps.MapTypeId.ROADMAP,
//		            scrollwheel: false
//		        });
//		        directionsDisplay3.setMap(map3);
//		        infowindow = new google.maps.InfoWindow();
//		        flightPlanCoordinates3 = [];
//		        bounds3 = new google.maps.LatLngBounds();
//		    }
//		    var image = "http://www.googlemapsmarkers.com/v1/009900";
//						//alert($(xmlDoc).find("rows").length);
//		    var iMarker = 0;
//						
//		    var title="";							        	
//							        
//							        for (i = 0; i < locations.address.length; i++) {
//							        	if(locations.address[i].Status=="Red"){
//							        		image = "../../dist/img/truck_red.png";
//							        	} else {
//							        		image = "../../dist/img/truck_green.png";	
//							        	}
//										var sLink = "LocalRunsDetail.jsp?VehicleId=" + locations.address[i].VehicleId;
//										//if(haltAlert){
//						        		marker = new google.maps.Marker({
//						                    position: new google.maps.LatLng(locations.address[i].Source, locations.address[i].Destination),
//						                    map: map1,
//						                    icon: image,
//						                    url: sLink
//						                });
//						        	//} 
//						        	
//						        	//google.maps.event.trigger(map,'resize');
//							        map1.setZoom(17);
//						        	map1.panTo(marker.position);
//						        		 flightPlanCoordinates1.push(marker.getPosition());
//						        	     bounds1.extend(marker.position);
//						        	     title = "VehicleId: " + locations.address[i].VehicleId;
//						        	     google.maps.event.addListener(marker, 'click', function(){
//						        	    	window.location.href = this.url; 
//						        	     });
//						                 google.maps.event.addListener(marker, 'click', (function (marker, iMarker) {
//						                     return function () {
//						                         infowindow.setContent('<p style="height:300px; color:black">' + title+ '</p>');
//						                         infowindow.open(map1, marker);
//						                     }
//						                 })(marker, iMarker));
//						                 iMarker++;
//										};
//						map1.fitBounds(bounds1);
//				        /*
//						 * polyline var flightPath = new google.maps.Polyline({
//						 * map: map, path: flightPlanCoordinates, strokeColor:
//						 * "#FF0000", strokeOpacity: 1.0, strokeWeight: 2 });
//						 */
//				        // directions service
//				        var start1 = flightPlanCoordinates1[0];
//				        var end1 = flightPlanCoordinates1[flightPlanCoordinates1.length - 1];
//				        var waypts1 = [];
//				        for (var i = 1; i < flightPlanCoordinates1.length - 1; i++) {
//				            waypts1.push({
//				                location: flightPlanCoordinates1[i],
//				                stopover: true
//				            });
//				        }
//				        
//				        for (i = 0; i < locations1.address.length; i++) {
//				        	if(locations1.address[i].Status=="Red"){
//				        		image = "../../dist/img/truck_red.png";
//				        	} else {
//				        		image = "../../dist/img/truck_green.png";	
//				        	}
//				        	var sLink = "LocalRunsDetail.jsp?VehicleId=" + locations1.address[i].VehicleId;
//						//if(haltAlert){
//		        		marker = new google.maps.Marker({
//		                    position: new google.maps.LatLng(locations1.address[i].Source, locations1.address[i].Destination),
//		                    map: map2,
//		                    icon: image,
//		                    url: sLink
//		                });
//		        	//} 
//		        	
//		        	//google.maps.event.trigger(map,'resize');
//			        map2.setZoom(17);
//		        	map2.panTo(marker.position);
//		        		 flightPlanCoordinates2.push(marker.getPosition());
//		        	     bounds2.extend(marker.position);
//		        	     title = "VehicleId: " + locations1.address[i].VehicleId;
//		        	     google.maps.event.addListener(marker, 'click', function(){
//			        	    	window.location.href = this.url; 
//			        	     });
//		                 google.maps.event.addListener(marker, 'click', (function (marker, iMarker) {
//		                     return function () {
//		                         infowindow.setContent('<div style="height:300px; color:black">' + title+ '</div>');
//		                         infowindow.open(map2, marker);
//		                     }
//		                 })(marker, iMarker));
//		                 iMarker++;
//						};
//			map2.fitBounds(bounds2);
//	        /*
//			 * polyline var flightPath = new google.maps.Polyline({
//			 * map: map, path: flightPlanCoordinates, strokeColor:
//			 * "#FF0000", strokeOpacity: 1.0, strokeWeight: 2 });
//			 */
//	        // directions service
//	        var start2 = flightPlanCoordinates2[0];
//	        var end2 = flightPlanCoordinates2[flightPlanCoordinates2.length - 1];
//	        var waypts2 = [];
//	        for (var i = 1; i < flightPlanCoordinates2.length - 1; i++) {
//	            waypts2.push({
//	                location: flightPlanCoordinates2[i],
//	                stopover: true
//	            });
//	        }
//	        
//	        
//	        
//	        for (i = 0; i < locations2.address.length; i++) {
//	        	if(locations2.address[i].Status=="Red"){
//	        		image = "../../dist/img/truck_red.png";
//	        	} else {
//	        		image = "../../dist/img/truck_green.png";	
//	        	}
//	        	var sLink = "LocalRunsDetail.jsp?VehicleId=" + locations2.address[i].VehicleId;
//			//if(haltAlert){
//    		marker = new google.maps.Marker({
//                position: new google.maps.LatLng(locations2.address[i].Source, locations2.address[i].Destination),
//                map: map3,
//                icon: image,
//                url: sLink
//            });
//    	//} 
//    	
//    	//google.maps.event.trigger(map,'resize');
//        map3.setZoom(17);
//    	map3.panTo(marker.position);
//    		 flightPlanCoordinates3.push(marker.getPosition());
//    	     bounds3.extend(marker.position);
//    	     title="VehicleId: " + locations2.address[i].VehicleId;
//    	     google.maps.event.addListener(marker, 'click', function(){
//     	    	window.location.href = this.url; 
//     	     });
//             google.maps.event.addListener(marker, 'click', (function (marker, iMarker) {
//                 return function () {
//                     infowindow.setContent(title);
//                     infowindow.open(map3, marker);
//                 }
//             })(marker, iMarker));
//             iMarker++;
//			};
//map3.fitBounds(bounds3);
///*
// * polyline var flightPath = new google.maps.Polyline({
// * map: map, path: flightPlanCoordinates, strokeColor:
// * "#FF0000", strokeOpacity: 1.0, strokeWeight: 2 });
// */
//// directions service
//var start3 = flightPlanCoordinates3[0];
//var end3 = flightPlanCoordinates3[flightPlanCoordinates3.length - 1];
//var waypts3 = [];
//for (var i = 1; i < flightPlanCoordinates3.length - 1; i++) {
//    waypts3.push({
//        location: flightPlanCoordinates3[i],
//        stopover: true
//    });
//}
//	        
////calcRoute3(start3, end3, waypts3);
////	        calcRoute2(start2, end2, waypts2);
////	        calcRoute1(start1, end1, waypts1);
//	} catch (e) {
//			alert(e);
//		}
//		
//	}
//
//function loadXMLString(txt) 
//{
//	if (window.DOMParser)
//	{
//		parser=new DOMParser();
//		xmlDoc=parser.parseFromString(txt,"text/xml");
//	}
//	else // Internet Explorer
//	{
//		xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
//		xmlDoc.async=false;
//		xmlDoc.loadXML(txt); 
//	}
//	return xmlDoc;
//}
//
//function calcRoute1(start, end, waypts) {
//    var request = {
//        origin: start,
//        destination: end,
//        waypoints: waypts,
//        optimizeWaypoints: true,
//        travelMode: google.maps.TravelMode.DRIVING
//    };
//    directionsService1.route(request, function (response, status) {
//        if (status == google.maps.DirectionsStatus.OK) {
//            directionsDisplay1.setDirections(response);
//            var route = response.routes[0];
//            /*var summaryPanel = document.getElementById('directions_panel');
//            summaryPanel.innerHTML = '';
//            // For each route, display summary information.
//            for (var i = 0; i < route.legs.length; i++) {
//                var routeSegment = i + 1;
//                summaryPanel.innerHTML += '<b>Route Segment: ' + routeSegment + '</b><br>';
//                summaryPanel.innerHTML += route.legs[i].start_address + ' to ';
//                summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
//                summaryPanel.innerHTML += route.legs[i].distance.text + '<br><br>';
//            }*/
//        }
//    });
//}
//
//
//function calcRoute2(start, end, waypts) {
//    var request = {
//        origin: start,
//        destination: end,
//        waypoints: waypts,
//        optimizeWaypoints: true,
//        travelMode: google.maps.TravelMode.DRIVING
//    };
//    directionsService2.route(request, function (response, status) {
//        if (status == google.maps.DirectionsStatus.OK) {
//            directionsDisplay2.setDirections(response);
//            /*var route = response.routes[0];
//            var summaryPanel = document.getElementById('directions_panel');
//            //summaryPanel.innerHTML = '';
//            // For each route, display summary information.
//            for (var i = 0; i < route.legs.length; i++) {
//                var routeSegment = i + 1;
//                summaryPanel.innerHTML += '<b>Route Segment: ' + routeSegment + '</b><br>';
//                summaryPanel.innerHTML += route.legs[i].start_address + ' to ';
//                summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
//                summaryPanel.innerHTML += route.legs[i].distance.text + '<br><br>';
//            }*/
//        }
//    });
//}
//
//function calcRoute3(start, end, waypts) {
//    var request = {
//        origin: start,
//        destination: end,
//        waypoints: waypts,
//        optimizeWaypoints: true,
//        travelMode: google.maps.TravelMode.DRIVING
//    };
//    directionsService3.route(request, function (response, status) {
//        if (status == google.maps.DirectionsStatus.OK) {
//            directionsDisplay3.setDirections(response);
//            /*var route = response.routes[0];
//            var summaryPanel = document.getElementById('directions_panel');
//            //summaryPanel.innerHTML = '';
//            // For each route, display summary information.
//            for (var i = 0; i < route.legs.length; i++) {
//                var routeSegment = i + 1;
//                summaryPanel.innerHTML += '<b>Route Segment: ' + routeSegment + '</b><br>';
//                summaryPanel.innerHTML += route.legs[i].start_address + ' to ';
//                summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
//                summaryPanel.innerHTML += route.legs[i].distance.text + '<br><br>';
//            }*/
//        }
//    });
//}