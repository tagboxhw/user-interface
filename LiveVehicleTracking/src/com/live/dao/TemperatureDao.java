package com.live.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeMap;
import java.util.Vector;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.live.bean.TemperatureBean;
import com.live.util.Constants;
import com.live.util.DbConnection;
import com.live.util.SqlConstants;

public class TemperatureDao {
	private Client client; 
	static final Logger logger = LoggerFactory
			.getLogger(TemperatureDao.class);
	
	private void init(){ 
	      this.client = ClientBuilder.newClient(); 
	   }  
	
	public Collection<TemperatureBean> getTemperatureData() {
		init();
		GenericType<Collection<TemperatureBean>> tvmBean1 = new GenericType<Collection<TemperatureBean>>(){};
		Collection<TemperatureBean> tvmBean = client 
		         .target(Constants.REST_SERVICE_URL + "TemperatureService/temperature") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(tvmBean1); 
		return tvmBean;
	}
	
	public Collection<TemperatureBean> getTemperatureData(String sNodeId) {
		init();
		Collection<TemperatureBean> col1 = new Vector();
		GenericType<Collection<TemperatureBean>> tvmBean1 = new GenericType<Collection<TemperatureBean>>(){};
		Collection<TemperatureBean> tvmBean = client 
		         .target(Constants.REST_SERVICE_URL + "TemperatureService/temperature") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(tvmBean1); 
		for(TemperatureBean tb: tvmBean) {
			if(tb.getNode_Id().equals(sNodeId)) {
				col1.add(tb);
			}
		}
		return col1;
	}
	
	public Collection<TemperatureBean> getVibrationData(String sNodeId) {
		init();
		Collection<TemperatureBean> col1 = new Vector();
		GenericType<Collection<TemperatureBean>> tvmBean1 = new GenericType<Collection<TemperatureBean>>(){};
		Collection<TemperatureBean> tvmBean = client 
		         .target(Constants.REST_SERVICE_URL + "TemperatureService/vibration") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(tvmBean1); 
		for(TemperatureBean tb: tvmBean) {
			if(tb.getNode_Id().equals(sNodeId)) {
				col1.add(tb);
			}
		}
		return col1;
	}
	
	
	public Collection<TemperatureBean> getVibrationData1(String sNodeId) {
		Connection conn = null;
		Statement pstmt = null;
		ResultSet rs = null;
		Statement statement = null;
		TemperatureBean temperatureBean = new TemperatureBean();
		Collection<TemperatureBean> col = new Vector<TemperatureBean>();
		try {
			conn = DbConnection.getConnection();
			// Create and execute a SELECT SQL statement.
			// Create and execute a SELECT SQL statement.
			try {
				pstmt = conn.createStatement();
				rs = pstmt.executeQuery(SqlConstants.GetVibrationData);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			
				while (rs.next()) {
					temperatureBean = new TemperatureBean();
					if(!sNodeId.equals(rs.getString(1))) continue;
					if(rs.getString(1) == null || rs.getString(1).equals("")) {
						temperatureBean.setNode_Id("");
					} else {
						temperatureBean.setNode_Id(rs.getString(1));
					}
					if(rs.getString(2) == null || rs.getString(2).equals("")) {
						temperatureBean.setTimestamp("");
					} else {
						temperatureBean.setTimestamp(rs.getString(2));
					}
					if(rs.getString(3) == null || rs.getString(3).equals("")) {
						temperatureBean.setTemperature("");
					} else {
						temperatureBean.setTemperature(rs.getString(3));
					}
					col.add(temperatureBean);
				}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return col;
	}
	
	public Collection<TemperatureBean> getTemperatureData1(String sNodeId) {
		Connection conn = null;
		CallableStatement pstmt = null;
		ResultSet rs = null;
		Statement statement = null;
		TemperatureBean temperatureBean = new TemperatureBean();
		Collection<TemperatureBean> col = new Vector<TemperatureBean>();
		try {
			conn = DbConnection.getConnection();
			// Create and execute a SELECT SQL statement.
			// Create and execute a SELECT SQL statement.
			try {
				statement = conn.createStatement();
				rs = statement.executeQuery(SqlConstants.GetTemperatureData);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			
				while (rs.next()) {
					temperatureBean = new TemperatureBean();
					if(!sNodeId.equals(rs.getString(1))) continue;
					if(rs.getString(1) == null || rs.getString(1).equals("")) {
						temperatureBean.setNode_Id("");
					} else {
						temperatureBean.setNode_Id(rs.getString(1));
					}
					if(rs.getString(2) == null || rs.getString(2).equals("")) {
						temperatureBean.setTimestamp("");
					} else {
						temperatureBean.setTimestamp(rs.getString(2));
					}
					if(rs.getString(3) == null || rs.getString(3).equals("")) {
						temperatureBean.setTemperature("");
					} else {
						temperatureBean.setTemperature(rs.getString(3));
					}
					col.add(temperatureBean);
				}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return col;
	}
	
}
