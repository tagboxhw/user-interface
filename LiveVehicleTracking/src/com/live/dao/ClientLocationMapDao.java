package com.live.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.TreeMap;
import java.util.Vector;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.live.bean.AllLocationDataBean;
import com.live.bean.ClientLocationMapBean;
import com.live.util.Constants;
import com.live.util.DbConnection;
import com.live.util.SqlConstants;
import com.live.util.Utils;

public class ClientLocationMapDao {
	private Client client; 
	static final Logger logger = LoggerFactory
			.getLogger(ClientLocationMapDao.class);
	public static Collection<AllLocationDataBean> colAllLocations = new Vector<AllLocationDataBean>();

	private void init(){ 
	      this.client = ClientBuilder.newClient(); 
	   }  
	
	
	public TreeMap<String, ClientLocationMapBean> selectLocations() {
		init();
		GenericType<Collection<ClientLocationMapBean>> clmBean1 = new GenericType<Collection<ClientLocationMapBean>>(){};
		Collection<ClientLocationMapBean> col = client 
		         .target(Constants.REST_SERVICE_URL + "ClientLocationMapService/locationmap") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(clmBean1);
		TreeMap<String, ClientLocationMapBean> tm = new TreeMap<String, ClientLocationMapBean>();
		Iterator iter = col.iterator();
		ClientLocationMapBean clmBean = new ClientLocationMapBean();
		while(iter.hasNext()){
			clmBean = (ClientLocationMapBean) iter.next();
			tm.put(clmBean.getLocation_id(), clmBean);
		}
		return tm;
	}
	
	public Collection<AllLocationDataBean> getAllLocationsData() {
		init();
		GenericType<Collection<AllLocationDataBean>> clmBean1 = new GenericType<Collection<AllLocationDataBean>>(){};
		Collection<AllLocationDataBean> col = client 
		         .target(Constants.REST_SERVICE_URL + "ClientLocationMapService/alllocations") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(clmBean1);
		colAllLocations = col;
		return colAllLocations;
	}
	

	public void updateThresholdAndPhone(String sLocId, String sNodeId,
			String sThreshold, String sPhoneNum) {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement pSelect = null;
		String updateSql = "";
		try {
			conn = DbConnection.getConnection();
			String[] s = sThreshold.split(",");
			pSelect = conn.prepareStatement(SqlConstants.UpdateThreshold);
			pSelect.setString(1, s[0]);
			pSelect.setString(2, s[1]);
			pSelect.setString(3, sNodeId);
			int iReturn = pSelect.executeUpdate();
			if (sLocId.equals("LO-DMO-000001")) {
				updateSql = SqlConstants.UpdatePhoneForLoc1;
			} else if (sLocId.equals("LO-DMO-000002")) {
				updateSql = SqlConstants.UpdatePhoneForLoc2;
			}
			pSelect = conn.prepareStatement(updateSql);
			pSelect.setString(1, sPhoneNum);
			iReturn = pSelect.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void resetThreshold(String sNodeId) {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement pSelect = null;
		try {
			conn = DbConnection.getConnection();
			pSelect = conn.prepareStatement(SqlConstants.ResetThreshold);
			pSelect.setString(1, Constants.THRESHOLD_TEMPERTURE_MIN);
			pSelect.setString(2, Constants.THRESHOLD_TEMPERTURE_MAX);
			pSelect.setString(3, sNodeId);
			int iReturn = pSelect.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public int deleteStationaryNodeMap(String sNDClientID) {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement pSelect = null;
		int iReturn = 0;
		try {
			conn = DbConnection.getConnection();
			pSelect = conn
					.prepareStatement(SqlConstants.DeleteStationaryNodeMap);
			pSelect.setString(1, sNDClientID);
			iReturn = pSelect.executeUpdate();
		} catch (SQLException e) {
			logger.error("localized message: " + e.getLocalizedMessage());
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return iReturn;
	}
	

}
