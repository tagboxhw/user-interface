package com.live.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeMap;
import java.util.Vector;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.live.bean.HumidityBean;
import com.live.util.Constants;
import com.live.util.DbConnection;
import com.live.util.SqlConstants;

public class HumidityDao {
	private Client client; 
	static final Logger logger = LoggerFactory
			.getLogger(HumidityDao.class);
	
	private void init(){ 
	      this.client = ClientBuilder.newClient(); 
	   }  
	
	public Collection<HumidityBean> getHumidityData() {
		init();
		GenericType<Collection<HumidityBean>> tvmBean1 = new GenericType<Collection<HumidityBean>>(){};
		Collection<HumidityBean> tvmBean = client 
		         .target(Constants.REST_SERVICE_URL + "HumidityService/humidity") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(tvmBean1); 
		return tvmBean;
	}
	
	public Collection<HumidityBean> getHumidityData(String sNodeId) {
		init();
		Collection<HumidityBean> col1 = new Vector();
		GenericType<Collection<HumidityBean>> tvmBean1 = new GenericType<Collection<HumidityBean>>(){};
		Collection<HumidityBean> tvmBean = client 
		         .target(Constants.REST_SERVICE_URL + "HumidityService/humidity") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(tvmBean1); 
		for(HumidityBean tb: tvmBean) {
			if(tb.getNode_Id().equals(sNodeId)) {
				col1.add(tb);
			}
		}
		return col1;
	}
	
	public Collection<HumidityBean> getHumidityData1(String sNodeId) {
		Connection conn = null;
		Statement pstmt = null;
		ResultSet rs = null;
		Statement statement = null;
		HumidityBean temperatureBean = new HumidityBean();
		Collection<HumidityBean> col = new Vector<HumidityBean>();
		try {
			conn = DbConnection.getConnection();
			// Create and execute a SELECT SQL statement.
			// Create and execute a SELECT SQL statement.
			try {
				pstmt = conn.createStatement();
				rs = pstmt.executeQuery(SqlConstants.GetHumidityData);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			
				while (rs.next()) {
					temperatureBean = new HumidityBean();
					if(!sNodeId.equals(rs.getString(1))) continue;
					if(rs.getString(1) == null || rs.getString(1).equals("")) {
						temperatureBean.setNode_Id("");
					} else {
						temperatureBean.setNode_Id(rs.getString(1));
					}
					if(rs.getString(2) == null || rs.getString(2).equals("")) {
						temperatureBean.setTimestamp("");
					} else {
						temperatureBean.setTimestamp(rs.getString(2));
					}
					if(rs.getString(3) == null || rs.getString(3).equals("")) {
						temperatureBean.setHumidity("");
					} else {
						temperatureBean.setHumidity(rs.getString(3));
					}
					col.add(temperatureBean);
				}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return col;
	}
}
