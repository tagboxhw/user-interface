package com.live.action;

import java.util.ArrayList;
import java.util.Collection;
import java.util.TreeMap;

import com.live.dao.AlertsWorkflowDao;

public class AlertAction {

	public void updateAction(String sAction, String sAlertId) {
		AlertsWorkflowDao ps = new AlertsWorkflowDao();
		try {
			ps.updateAction(sAction, sAlertId);
		} catch(Exception e) {
			System.out.println(this.getClass().getName() + "Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		}
	}
	
	public void updateRoot(String sRoot, String sPreventive, String sAlertId) {
		AlertsWorkflowDao ps = new AlertsWorkflowDao();
		try {
			ps.updatePreventiveActionRootCause(sRoot, sPreventive, sAlertId);
		} catch(Exception e) {
			System.out.println(this.getClass().getName() + "Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		}
	}
	
}
