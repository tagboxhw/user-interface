<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>
<%@ page import="com.demo1.dao.ShipmentDao"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page errorPage="Error.jsp" %>
<%!static final Logger logger = LoggerFactory.getLogger("GetShipmentData");
ShipmentDao sd = new ShipmentDao();
%>
<%
String buf = "";
String sString = "";
try {
	sString = request.getParameter("query");
	if (sString.equals("getNewShipment")) {
		buf = "";
		//response.setContentType("text/xml");
		//buf += ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		String result = sd.getNewShipmentId();
		buf = "<rows>";
		buf += ("<row>");
		buf += ("<Done>"+ result + "</Done>");
		buf += ("</row>");
		buf += "</rows>";
		buf = buf.trim();
		//logger.info(buf);
		out.clear();
		out.println(buf);
	} else if (sString.equals("step1Insert")) {
		String sShipmentId = request.getParameter("Shipment_Id");
		String sSource = request.getParameter("Source_Name");
		String sDestination = request.getParameter("Destination_Name");
		String sETD = request.getParameter("ETD_Date");
		String sETA = request.getParameter("ETA_Date");
		String sInvoice = request.getParameter("Invoice_number");
		buf = "";
		//response.setContentType("text/xml");
		//buf += ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		String result = sd.insertShipmentStep1Data(sShipmentId, sSource, sDestination, sETD, sETA, sInvoice);
		
		buf = "<rows>";
		buf += ("<row>");
		buf += ("<Done>"+ result + "</Done>");
		buf += ("</row>");
		buf += "</rows>";
		buf = buf.trim();
		//logger.info(buf);
		out.clear();
		out.println(buf);
	} else if (sString.equals("step2Insert")) {
		String sShipmentId = request.getParameter("Shipment_Id");
		String sSourceId = request.getParameter("Source_Id");
		String sDestId = request.getParameter("Dest_Id");
		String sEtd = request.getParameter("Etd_Id");
		String sEta = request.getParameter("Eta_String");
		String sMode = request.getParameter("Mode_String");
		String sCarrier = request.getParameter("Carrier_String");
		buf = "";
		String result = sd.insertShipmentStep2Data(sShipmentId, sSourceId, sDestId, sEtd, sEta, sMode, sCarrier);
		
		buf = "<rows>";
		buf += ("<row>");
		buf += ("<Done>"+ result + "</Done>");
		buf += ("</row>");
		buf += "</rows>";
		buf = buf.trim();
		//logger.info(buf);
		out.clear();
		out.println(buf);
	} else if (sString.equals("step3Insert")) {
		String sShipmentId = request.getParameter("Shipment_Id");
		String sBoxId = request.getParameter("Box_Id");
		String sTagId = request.getParameter("Tag_Id");
		String sProductId = request.getParameter("Product_Id");
		String sValue = request.getParameter("Value_String");
		String sCriticality = request.getParameter("Criticality_String");
		buf = "";
		//response.setContentType("text/xml");
		//buf += ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		String result = sd.insertShipmentStep3Data(sShipmentId, sBoxId, sTagId, sProductId, sValue, sCriticality);
		
		buf = "<rows>";
		buf += ("<row>");
		buf += ("<Done>"+ result + "</Done>");
		buf += ("</row>");
		buf += "</rows>";
		buf = buf.trim();
		//logger.info(buf);
		out.clear();
		out.println(buf);
	}  else if (sString.equals("step4Insert")) {
		String sShipmentId = request.getParameter("Shipment_Id");
		String sUserId = request.getParameter("User_Id");
		buf = "";
		//response.setContentType("text/xml");
		//buf += ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		String result = sd.insertShipmentStep4Data(sShipmentId, sUserId);
		
		buf = "<rows>";
		buf += ("<row>");
		buf += ("<Done>"+ result + "</Done>");
		buf += ("</row>");
		buf += "</rows>";
		buf = buf.trim();
		//logger.info(buf);
		out.clear();
		out.println(buf);
	} else if (sString.equals("step1Update")) {
		String sShipmentId = request.getParameter("Shipment_Id");
		String sSource = request.getParameter("Source_Name");
		String sDestination = request.getParameter("Destination_Name");
		String sETD = request.getParameter("ETD_Date");
		String sETA = request.getParameter("ETA_Date");
		String sInvoice = request.getParameter("Invoice_number");
		buf = "";
		//response.setContentType("text/xml");
		//buf += ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		String result = sd.updateShipmentStep1Data(sShipmentId, sSource, sDestination, sETD, sETA, sInvoice);
		
		buf = "<rows>";
		buf += ("<row>");
		buf += ("<Done>"+ result + "</Done>");
		buf += ("</row>");
		buf += "</rows>";
		buf = buf.trim();
		//logger.info(buf);
		out.clear();
		out.println(buf);
	} else if (sString.equals("step2Update")) {
		String sShipmentId = request.getParameter("Shipment_Id");
		String sSourceId = request.getParameter("Source_Id");
		String sDestId = request.getParameter("Dest_Id");
		String sEtd = request.getParameter("Etd_Id");
		String sEta = request.getParameter("Eta_String");
		String sMode = request.getParameter("Mode_String");
		String sCarrier = request.getParameter("Carrier_String");
		buf = "";
		//response.setContentType("text/xml");
		//buf += ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		String result = sd.updateShipmentStep2Data(sShipmentId, sSourceId, sDestId, sEtd, sEta, sMode, sCarrier);
		
		buf = "<rows>";
		buf += ("<row>");
		buf += ("<Done>"+ result + "</Done>");
		buf += ("</row>");
		buf += "</rows>";
		buf = buf.trim();
		//logger.info(buf);
		out.clear();
		out.println(buf);
	} else if (sString.equals("step3Update")) {
		String sShipmentId = request.getParameter("Shipment_Id");
		String sBoxId = request.getParameter("Box_Id");
		String sTagId = request.getParameter("Tag_Id");
		String sProductId = request.getParameter("Product_Id");
		String sValue = request.getParameter("Value_String");
		String sCriticality = request.getParameter("Criticality_String");
		buf = "";
		//response.setContentType("text/xml");
		//buf += ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		String result = sd.updateShipmentStep3Data(sShipmentId, sBoxId, sTagId, sProductId, sValue, sCriticality);
		
		buf = "<rows>";
		buf += ("<row>");
		buf += ("<Done>"+ result + "</Done>");
		buf += ("</row>");
		buf += "</rows>";
		buf = buf.trim();
		//logger.info(buf);
		out.clear();
		out.println(buf);
	}  else if (sString.equals("step4Update")) {
		String sShipmentId = request.getParameter("Shipment_Id");
		String sUserId = request.getParameter("User_Id");
		buf = "";
		//response.setContentType("text/xml");
		//buf += ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		String result = sd.updateShipmentStep4Data(sShipmentId, sUserId);
		
		buf = "<rows>";
		buf += ("<row>");
		buf += ("<Done>"+ result + "</Done>");
		buf += ("</row>");
		buf += "</rows>";
		buf = buf.trim();
		//logger.info(buf);
		out.clear();
		out.println(buf);
	}   else if (sString.equals("endShipment")) {
		String sShipmentId = request.getParameter("Shipment_Id");
		String sEndTime = request.getParameter("End_time");
		buf = "";
		//response.setContentType("text/xml");
		//buf += ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sd.endTrip(sShipmentId, sEndTime);
		
		buf = "<rows>";
		buf += ("<row>");
		buf += ("<Done>SUCCESS</Done>");
		buf += ("</row>");
		buf += "</rows>";
		buf = buf.trim();
		//logger.info(buf);
		out.clear();
		out.println(buf);
	}    else if (sString.equals("deleteShipment")) {
		String sShipmentId = request.getParameter("Shipment_Id");
		buf = "";
		//response.setContentType("text/xml");
		//buf += ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sd.deleteShipment(sShipmentId);

		buf = "<rows>";
		buf += ("<row>");
		buf += ("<Done>SUCCESS</Done>");
		buf += ("</row>");
		buf += "</rows>";
		buf = buf.trim();
		//logger.info(buf);
		out.clear();
		out.println(buf);
	}
	
} catch (Exception e) {
	logger.error("Exception: " + e.getLocalizedMessage());
	e.printStackTrace();
}
%>