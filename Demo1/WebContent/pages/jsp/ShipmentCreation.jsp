<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page errorPage="Error.jsp" %>
<%@ page import="com.demo1.dao.ShipmentDao"%>
<%@ page import="com.demo1.bean.ShipmentLocations"%>
<%@ page import="com.demo1.bean.ShipmentTags"%>
<%@ page import="com.demo1.bean.ShipmentUsers"%>
<%@ page import="com.demo1.bean.ShipmentProducts"%>
<%@ page import="com.demo1.util.Utils"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.TreeMap"%>
<%@ page import="java.util.Set"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.util.Collection"%>
<%@ page import="java.util.Set"%>
<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>
<%
response.setHeader("Cache-Control","no-cache"); 
response.setHeader("Pragma","no-cache"); 
response.setDateHeader ("Expires", -1); 
%>
<%!
static final Logger logger = LoggerFactory.getLogger("ShipmentCreation");
%>
<%
String sUsername = (String) session.getAttribute("username");
if(sUsername == null || sUsername.equals("")) {
	session.setAttribute("callingPage", "ShipmentCreation.jsp");
	response.sendRedirect("../../index.html");
}
java.util.Date dt = new java.util.Date();
java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd-MM-yyyy HH:mm");
sdf.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
String currentTime = sdf.format(dt);
ShipmentDao sd = new ShipmentDao();
Collection<ShipmentLocations> colLocs = sd.getShipmentLocations();
Collection<ShipmentTags> colTags = sd.getShipmentTags();
Collection<ShipmentUsers> colUsers = sd.getShipmentUsers();
Collection<ShipmentProducts> colProducts = sd.getShipmentProducts();
String sProducts = "", sProducts1 = "", sLocations = "", sLocations1 = "";
for(ShipmentProducts s: colProducts) {
	sProducts = sProducts + ",\"" + s.getShipmentProductId() + "\"";
	sProducts1 = sProducts1 + "," + s.getShipmentProductId();
}
if(sProducts.length()>2) {
	sProducts = sProducts.substring(1);
	sProducts1 = sProducts1.substring(1);
}
for(ShipmentLocations s: colLocs) {
	sLocations = sLocations + ",\"" + s.getShipmentLocationId() + "\"";
	sLocations1 = sLocations1 + "," + s.getShipmentLocationId();
}
if(sLocations.length()>2) {
	sLocations = sLocations.substring(1);
	sLocations1 = sLocations1.substring(1);
}
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Shipment Creation</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<link rel="stylesheet" href="../../dist/css/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- Add these below two lines for autocomplete and the javascript function at the end  -->
 <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> 
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="../../bootstrap/css/bootstrap.css">
   
<!-- Font Awesome -->
<script src="https://use.fontawesome.com/37ba1872d2.js"></script>

<link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../../dist/css/tagbox.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
<link href="https://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet">
<link href="../../dist/css/select2.css" rel="stylesheet" />
<script src="../../dist/js/select2.js"></script>
<script src="../../dist/js/shipment.js"></script>
<link rel="stylesheet" href="../../dist/css/timepicker1.css">
<script type="text/javascript">
$(document).ready(function() {
	$("#0_TagId").select2();
	$(".text-uppercase").keyup(function () {
	    this.value = this.value.toLocaleUpperCase();
	});
});
</script>
<script>
  $( function() {
    var availableProducts = [
      <%= sProducts %>
    ];
    $( "#0_ProductId" ).autocomplete({
      source: availableProducts
    });
    var availableLocations = [
      <%= sLocations %>
    ];
    $( "#Source" ).autocomplete({
      source: availableLocations
    });
    $( "#Destination" ).autocomplete({
        source: availableLocations
      });
    $( "#0_Source" ).autocomplete({
        source: availableLocations
      });
    $( "#0_Destination" ).autocomplete({
        source: availableLocations
      });
  });
  
  </script>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
  body {
	font-family:  Lato, sans-serif;
	font-weight: 600;
}
</style>
<style>
.save
{
	display:none;
}
.no-border {
	border: none !important;
	border: 0 !important;
}
</style>
<STYLE TYPE="text/css">
			table.sortable span.sortarrow {
   				color: blue;
   				text-decoration: none;
			}
					
		</style>
<script src="../../dist/js/sorttable.js"></script>
<script src="../../dist/js/table_script.js"></script>
<script type="text/javascript">
	function ExitPage(link_value)
	{
		if($("#exit_value").val() == "no") {
			if(confirm("All your changes will be lost!! Are you sure you want to leave the page?")) {
				window.location = (link_value);
			}
		} else {
			window.location = (link_value);
		}
	}
	
	</script>
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
	<div class="wrapper">

		<header class="main-header">
			<!-- Logo -->
			<div>
				<a href="#" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
					<span class="logo-mini"><b>Demo</b></span> <!-- logo for regular state and mobile devices -->
					<span class="logo-lg"><b>Demo</b> </span>
				</a>
			</div>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">

				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
					role="button"> <span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
				</a>
				<label class="navbar-form pull-left" role="search">
					<div class="form-group">
						<b style="padding-left: 15px; font-size: 20px;color:#fff;" class="pull-right">Create Shipment</b><br> <b style="padding-left: 15px;color:#fff;"><%= currentTime %></b>
					</div>
				</label>

				<div class="navbar-custom-menu">
					<img src="../../dist/img/Logo.png" class="logo-class pull-right"
						alt="User Image">
				</div>
			</nav>

		</header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->


				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<br>
				<ul class="sidebar-menu">
					<li class="header">MAIN NAVIGATION</li>
					<li class="treeview"><a href="#"> <i
							class="fa fa-cubes"></i> <span>Shipment Summary</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="javascript:ExitPage('TodayShipmentSummary.jsp')";><i
									class="fa fa-circle-o"></i> Home</a></li>
						</ul></li>
					<li class="treeview"><a href="#"> <i
							class="fa fa-dashboard"></i> <span>Dashboard</span>
					</a>
						<ul class="treeview-menu">
							<li class=""><a href="javascript:ExitPage('TripScorecard.jsp');"><i
									class="fa fa-circle-o"></i> Trip Scorecard</a></li>
						</ul></li>
					<!-- <li class="treeview"><a href="#"> <i
							class="fa fa-database"></i> <span>Admin</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="TagAssignment.jsp"><i class="fa fa-circle-o"></i>
									Tag Assignment</a></li>
						</ul></li> -->
						<li class="treeview active"><a href="#"> <i
							class="fa fa-th-large"></i> <span>Shipment</span>
					</a>
						<ul class="treeview-menu">
							<li class="active"><a href="javascript:ExitPage('ShipmentCreation.jsp');"><i class="fa fa-circle-o"></i>
									Create Shipment</a></li>
							<li><a href="javascript:ExitPage('ShipmentEdit.jsp');"><i class="fa fa-circle-o"></i>
									Edit Shipment</a></li>
						</ul></li>
						<li class="treeview"><a href="#"> <i
							class="fa fa-bar-chart"></i> <span>Trip Analysis</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="javascript:ExitPage('ETagDashboard.jsp');"><i class="fa fa-circle-o"></i>
									Trip Report</a></li>
						</ul></li>
						<!-- <li class="treeview"><a href="#"> <i
							class="fa fa-file-text-o"></i> <span>Reports</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="ETagDashboard.jsp"><i class="fa fa-circle-o"></i>
									Download Historic<br> Data</a></li>
						</ul></li> -->
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper" style="background:white">
			<!-- Content Header (Page header) -->
			
		<section id="mainSection" class="content">
			<ul class="nav navbar-nav">
            <li id="li_link1"><a href="#" onclick="showStep('1')" class="current_step" id="link1">Step 1<span>Shipment Creation</span></a></li>
            <li id="li_link2"><a href="#" onclick="showStep('2')" id="link2">Step 2<span>Add Shipment Legs</span></a></li>
            <li id="li_link3"><a href="#" onclick="showStep('3')" id="link3">Step 3<span>Assign Shipment Tags</span></a></li>
            <li id="li_link4"><a href="#" onclick="showStep('4')" id="link4">Step 4<span>Assign Shipment Users</span></a></li>
          </ul>
        <div class="row">
        	<div class="col-md-12">
				<input id="shipment_id" name="shipment_id" value="" type="hidden" class='form-control-no-height'>
				<input type="hidden" id="exit_value" name="exit_value" value="no">
				<br><br><br><br><br><br>
				<div id="step1">
  					<div class="row">
  						<div class="col-md-offset-2 col-md-8">
  							<div class="alert alert-danger text-center" role="alert" id="alertFailure" style="display:none;"></div>
  							<div class="well" id="DetailsDiv">
  								<div class="row">
          							<div class="col-md-offset-1 col-md-2">
          								<label>Source</label>
          							</div>
          							<div class="col-md-3">
	  										<input id="Source" name="Source" class='form-control-no-height text-uppercase' type='text'>
          							</div>
          							<div class="col-md-2">
          								<label>Destination</label>
          							</div>
          							<div class="col-md-3">
					  						<input id="Destination" name="Destination" class='form-control-no-height text-uppercase' type='text'>
          							</div>
          						</div>
          						<br>
          						<div class="row">
          							<div class="col-md-offset-1 col-md-2">
          								<label>Departure Date</label>
          							</div>
          							<div class="col-md-3">
          								<input class='form-control-no-height' type="text" id="etd" name="etd" value="">
          							</div>
          							<div class="col-md-2">
          								<label>Arrival Date</label>
          							</div>
          							<div class="col-md-3">
          								<input class='form-control-no-height' type="text" id="eta" name="eta" value="">
          							</div>
          						</div>
          						<br>
          						<div class="row">
          							<div class="col-md-offset-1 col-md-2">
          								<label>Invoice Number</label>
          							</div>
          							<div class="col-md-3">
          								<input class='form-control-no-height' type="text" id="invoice_number" name="invoice_number" value="">
          							</div>
          						</div>
							</div>
							
							<button onclick="CheckStep1AndSubmit()" class="btn btn-success pull-right" type="button">SAVE & CONTINUE&nbsp;<i class="fa fa-chevron-right fg-lg"></i></button>
  						</div>
  					</div>
          		</div><!-- End of Step1 -->
          		<div id="step2" style="display:none;">
          		<div class="row">
  			<div class="col-md-offset-1 col-md-10">
  			<div id="show_stops" class="text-center"></div>
  			<br>
  			<div class="alert alert-danger text-center" role="alert" id="alertFailureStep2" style="display:none;"></div>
  				<table id="leg_table_step2" class="table table-bordered table-striped table-condensed">
					<tr class="info text-center">
						<th width="15%" class="text-center"><h4>Source</h4></th>
						<th width="15%" class="text-center"><h4>Destination</h4></th>
						<th width="15%" class="text-center"><h4>Departure Date</h4></th>
						<th width="15%" class="text-center"><h4>Arrival Date</h4></th>
						<th width="10%" class="text-center"><h4>Carrier Id</h4></th>
						<th width="10%" class="text-center"><h4>Mode</h4></th>
						<th width="10%" class="text-center"><h4>Add/Delete Legs</h4></th>
					</tr>
					<tr id="row_leg0" style="background:#ddd">
						<td class="text-center">
	  						<input id="0_Source" name="0_Source" class='form-control-no-height' type='text' disabled>
						</td>
						<td class="text-center">
	  						<input id="0_Destination" name="0_Destination" class='form-control-no-height' type='text' disabled>
						</td>
						<td class="text-center"><input class='form-control-no-height' placeholder="ETD" type="text" id="0_etd" name="0_etd" value="" disabled></td>
						<td class="text-center"><input class='form-control-no-height' placeholder="ETA" type="text" id="0_eta" name="0_eta" value="" disabled></td>
						<td class="text-center"><input class='form-control-no-height' placeholder="" type="text" id="0_carrier" name="0_carrier" value=""></td>
						<td class="text-center"><select class='form-control-no-height' id="0_Mode" name="0_Mode" >
							<option selected>AIR</option>
          					<option>MARINE</option>
          					<option>SURFACE</option>
          				</select></td>
						<td class="text-center"><button type="button" class="add btn btn-sm btn-warning" onclick="add_leg();add_hops();" data-toggle="tooltip" data-placement="top" title="Add Leg" value="Add Row"><i class='fa fa-plus'></i></button></td>
					</tr>
				</table>
				<%int j = 1; %>
				<input type="hidden" id="lastValue_leg" name="lastValue_leg" value="<%= j %>">
				<br>
				<a href="#" onclick="showStep('1')" class="btn btn-success pull-left"><i class="fa fa-chevron-left fg-lg"></i>&nbsp;EDIT STEP 1</a>
				<a href="#" onclick="CheckStep2AndSubmit()" class="btn btn-success pull-right">SAVE & CONTINUE&nbsp;<i class="fa fa-chevron-right fg-lg"></i></a>
			
  			</div>
  		</div>
          		</div><!-- End of Step2 -->
          		<div id="step3" style="display:none;">
          		<div class="row">
  			<div class="col-md-offset-1 col-md-10">
  			<br>
  			<input type="hidden" name="products_id" id="products_id" value='<%=sProducts1 %>'>
  			<input type="hidden" name="locations_id" id="locations_id" value='<%=sLocations1 %>'>
  			<div class="alert alert-danger text-center" role="alert" id="alertFailureStep3" style="display:none;"></div>
  				<table id="tag_table_step3" class="table table-bordered table-striped table-condensed">
					<tr class="info text-center">
						<th class="text-center"><h4>Box ID</h4></th>
						<th class="text-center"><h4>Tag ID</h4></th>
						<th class="text-center"><h4>Product</h4></th>
						<th class="text-center"><h4>Shipment Value(Lakhs INR)</h4></th>
						<th class="text-center"><h4>Criticality&nbsp;&nbsp;
						<a data-toggle="tooltip" data-placement="top" title="How critical is this shipment?">
  							<i class="fa fa-info-circle"></i>
						</a>
						</h4></th>
						<th class="text-center"><h4>Add/Delete Boxes</h4></th>
					</tr>
					<tr>
						<td class="text-center"><input class='form-control-no-height' type='text' id='0_BoxId' name="0_BoxId" value="" onchange="checkBox('0')" ></td>
						<td class="text-center"><select id="0_TagId" name="0_TagId" class="form-control-no-background pull-center" onchange="checkTag('0')" style="width: 100%">
          									<option>Select Tag</option>
          									<%for(ShipmentTags s: colTags) { %>
          									<option><%=s.getShipmentTagId() %></option>
          									<%} %>
										</select>
						</td>
						<td class="text-center">
	  						<input id="0_ProductId" name="0_ProductId" class='form-control-no-height text-uppercase' type='text'>
						</td>
						<td><input class='form-control-no-height' type='text' id='0_Value' name="0_Value" value=""></td>
						<td><select class='form-control-no-height' id="0_Criticality" name="0_Criticality" >
          		<option>LOW</option>
          		<option>MEDIUM</option>
          		<option>HIGH</option>
          	</select></td>
						<td class="text-center"><button type='button' class='add btn btn-sm btn-warning' data-toggle='tooltip' data-placement='top' title='Add Box' value='Add Row' onclick="add_box()"><i class='fa fa-plus'></i></button></td>
					</tr>
					
				</table>
				<input type="hidden" id="lastValue_tag" name="lastValue_tag" value="<%= j %>">
				<br>
				<a href="#" onclick="showStep('2')" class="btn btn-success pull-left"><i class="fa fa-chevron-left fg-lg"></i>&nbsp;EDIT STEP 2</a>
				<a href="#" onclick="CheckStep3AndSubmit()" class="btn btn-success pull-right">ASSIGN & CONTINUE&nbsp;<i class="fa fa-chevron-right fg-lg"></i></a>
			
  			</div>
  		</div>
          		</div><!-- End of Step3 -->
          		
          		<div id="step4" style="display:none;">
          		<div class="row">
  			<div class="col-md-offset-1 col-md-10 text-center">
  			<div class="alert alert-danger text-center" role="alert" id="alertFailureStep4" style="display:none;"></div>
  			<div class="alert alert-success text-center" id="alertSuccessStep4" style="display:none;"></div>
  				<input type="checkbox" name="cb_user" id="cb_user" onclick="Select_Users()">&nbsp;Select All Users
  				<div class="well white-background text-center">
  				<div class="row">
  				<%
  				for(ShipmentUsers s: colUsers) {
  					%>
  					<div class="col-md-2">
  			<input type="button" class="btn btn-default btn-sm" onclick="colorChange('user_<%=s.getShipmentUserId() %>');" id="user_<%=s.getShipmentUserId() %>" value="User <%=s.getShipmentUserId() %>: <%=s.getShipmentUserName()%>">
  			</div>
  				<%
  				} %>
  			
  		</div>
  		
  		</div>	
  		<a href="#" onclick="showStep('3')" class="btn btn-success pull-left" id="btn_goto_step3" name="btn_goto_step3"><i class="fa fa-chevron-left fg-lg"></i>&nbsp;EDIT STEP 3</a>
		<a href="#" onclick="CheckStep4AndSubmit()" class="btn btn-success pull-right" id="btn_save" name="btn_save">SAVE</a>
		<br>
		<div id="new_btns" style="display:none;" class="text-center"><a href="ShipmentCreation.jsp" class="btn btn-success pull-left">CREATE ANOTHER SHIPMENT</a>
			<a href="ShipmentEdit.jsp" class="btn btn-success pull-right">EDIT A SHIPMENT</a></div>
  		<br><br>
  				<input type="hidden" id="shipment_status" name="shipment_status" value="">	
  				</div>
  			</div>
          		</div><!-- End of Step4 -->
          	</div>
		</div>
	</section>
	
    
    
    
				</div>
		<!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0
			</div>
			<strong>Copyright &copy; 2017 <a href="javascript:ExitPage('http://tagbox.in');">TagBox</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Control Sidebar -->
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->
	
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
<script src="../../dist/js/timepicker1.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="../../bootstrap/js/bootstrap.min.js"></script>
	<!-- ChartJS 1.0.1 -->
	<!-- AdminLTE App -->
	<script src="../../dist/js/app.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="../../dist/js/demo.js"></script>

	</body>
	<script>
	//To change Now button color, update code in dist/css/jquery-ui.css: line# 410
	var a = (new Date()).getDate();
	$('#eta').datetimepicker({
		oneLine: true,
		controlType: 'select',
		minDate: 0,
		maxDate: 30,
		dateFormat: 'dd-mm-yy'
	}); 
	
	$('#etd').datetimepicker({
		oneLine: true,
		controlType: 'select',
		minDate: 0,
		maxDate: 30,
		dateFormat: 'dd-mm-yy'
	});
	$('#0_eta').datetimepicker({
		oneLine: true,
		controlType: 'select',
		minDate: 0,
		maxDate: 30,
		dateFormat: 'dd-mm-yy'
	});
	
	$('#0_etd').datetimepicker({
		oneLine: true,
		controlType: 'select',
		minDate: 0,
		maxDate: 30,
		dateFormat: 'dd-mm-yy'
	});
	
	</script>
</html>
