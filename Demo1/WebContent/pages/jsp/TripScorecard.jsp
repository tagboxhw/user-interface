<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page errorPage="Error.jsp" %>
<%@ page import="com.demo1.dao.TripScorecardDao"%>
<%@ page import="com.demo1.dao.TagAssignmentDao"%>
<%@ page import="com.demo1.bean.TripScorecardBean"%>
<%@ page import="com.demo1.bean.TagAssignmentBean"%>
<%@ page import="com.demo1.util.Utils"%>
<%@ page import="java.util.TreeMap"%>
<%@ page import="java.util.Set"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.util.Collection"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@ page import="java.util.Set"%>
<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>
<%@ page import="com.demo1.dao.TemperatureDao"%>
<%@ page import="com.demo1.bean.TemperatureBean"%>
<%
response.setHeader("Cache-Control","no-cache"); 
response.setHeader("Pragma","no-cache"); 
response.setDateHeader ("Expires", -1); 
%>
<%!
static final Logger logger = LoggerFactory.getLogger("TripScorecard");
%>
<%
String sUsername = (String) session.getAttribute("username");
if(sUsername == null || sUsername.equals("")) {
	session.setAttribute("callingPage", "TripScorecard.jsp");
	response.sendRedirect("../../index.html");
}
java.util.Date dt = new java.util.Date();
java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd-MM-yyyy HH:mm");
SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
df.setTimeZone(java.util.TimeZone.getTimeZone("UTC"));
SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MMM HH:mm");
sdf1.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
sdf.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
String currentTime = sdf.format(dt);
TripScorecardDao tvmDao = new TripScorecardDao();
TripScorecardBean tvmBean = new TripScorecardBean();
Collection<TripScorecardBean> col = tvmDao.selectAllTripScorecardRecords();
TagAssignmentDao taDao = new TagAssignmentDao();
TagAssignmentBean taBean = new TagAssignmentBean();
//Temporatily using Value_INR field to save trip status to display the alert bell

Date d = null;
Date d2 = null;
Long newTime = null;
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Trip Scorecard</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<link rel="stylesheet" href="../../dist/css/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- Add these below two lines for autocomplete and the javascript function at the end  -->
<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="../../bootstrap/css/bootstrap.css">
   
<!-- Font Awesome -->
<script src="https://use.fontawesome.com/37ba1872d2.js"></script>
<!-- Theme style -->
<link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../../dist/css/tagbox.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
<link href="https://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet">
<link href="../../dist/css/select2.css" rel="stylesheet" />
<script src="../../dist/js/select2.js"></script>
<script src="../../dist/js/table2download.js"></script>
<link rel="stylesheet" href="../../dist/css/bootstrap-switch.css">
<script src="../../dist/js/bootstrap-switch.js"></script>
<link rel="stylesheet" href="../../dist/css/timepicker1.css">
<script src="../../dist/js/timepicker1.js"></script>
<script type="text/javascript" src="../../plugins/tableExport/tableExport.js"></script>
<script type="text/javascript" src="../../plugins/tableExport/jquery.base64.js"></script>
<script type="text/javascript" src="../../plugins/tableExport/jspdf/libs/sprintf.js"></script>
<script type="text/javascript" src="../../plugins/tableExport/jspdf/jspdf.js"></script>
<script type="text/javascript" src="../../plugins/tableExport/jspdf/libs/base64.js"></script>
<script type="text/javascript" src="../../plugins/tableExport/html2canvas.js"></script>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
  body {
	font-family:  Lato, sans-serif;
	font-weight: 600;
}
</style>
<style>
.save
{
	display:none;
}
.no-border {
	border: none !important;
	border: 0 !important;
}
</style>
<STYLE TYPE="text/css">
			table.sortable span.sortarrow {
   				color: blue;
   				text-decoration: none;
			}
					
		</style>
<script src="../../dist/js/sorttable.js"></script>
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
	<div class="wrapper">

		<header class="main-header">
			<!-- Logo -->
			<div>
				<a href="#" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
					<span class="logo-mini"><b>Demo</b></span> <!-- logo for regular state and mobile devices -->
					<span class="logo-lg"><b>Demo</b> </span>
				</a>
			</div>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">

				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
					role="button"> <span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
				</a>
				<label class="navbar-form pull-left" role="search">
					<div class="form-group">
						<b style="padding-left: 15px; font-size: 20px;color:#fff;" class="pull-right">Trip Scorecard</b><br> <b style="padding-left: 15px;color:#fff;"><%= currentTime %></b>
					</div>
				</label>

				<div class="navbar-custom-menu">
					<img src="../../dist/img/Logo.png" class="logo-class pull-right"
						alt="User Image">
				</div>
			</nav>

		</header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->


				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<br>
				<ul class="sidebar-menu">
					<li class="header">MAIN NAVIGATION</li>
					<li class="treeview"><a href="#"> <i
							class="fa fa-cubes"></i> <span>Shipment Summary</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="TodayShipmentSummary.jsp"><i
									class="fa fa-circle-o"></i> Home</a></li>
						</ul></li>
					<li class="treeview active"><a href="#"> <i
							class="fa fa-dashboard"></i> <span>Dashboard</span>
					</a>
						<ul class="treeview-menu">
							<li class="active"><a href="TripScorecard.jsp"><i
									class="fa fa-circle-o"></i> Trip Scorecard</a></li>
						</ul></li>
					<!-- <li class="treeview"><a href="#"> <i
							class="fa fa-database"></i> <span>Admin</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="TagAssignment.jsp"><i class="fa fa-circle-o"></i>
									Tag Assignment</a></li>
						</ul></li> -->
					<li class="treeview"><a href="#"> <i
							class="fa fa-th-large"></i> <span>Shipment</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="ShipmentCreation.jsp"><i class="fa fa-circle-o"></i>
									Create Shipment</a></li>
							<li><a href="ShipmentEdit.jsp"><i class="fa fa-circle-o"></i>
									Edit Shipment</a></li>
						</ul></li>
						<li class="treeview"><a href="#"> <i
							class="fa fa-bar-chart"></i> <span>Trip Analysis</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="ETagDashboard.jsp"><i class="fa fa-circle-o"></i>
									Trip Report</a></li>
						</ul></li>
						<!-- <li class="treeview"><a href="#"> <i
							class="fa fa-file-text-o"></i> <span>Reports</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="ETagDashboard.jsp"><i class="fa fa-circle-o"></i>
									Download Historic<br> Data</a></li>
						</ul></li> -->
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper" style="background:white">
			<!-- Content Header (Page header) -->
			
		<section id="mainSection" class="content">
		<br>
			<form id="mainForm" method="post" action="TripScorecard.jsp">
			
							
		<div id="listView">		
									<table id="data_table_stationary" class="table table-condensed table-black-border">
									<thead class="blue">
									<th width="150" class="text-center"><b class="font-big-white">Shipment ID</b></th>
									<th width="90" class="text-center"><b class="font-big-white">Origin</b></th>
									<th width="150" class="text-center"><b class="font-big-white">Destination</b></th>
									<th width="100" class="text-center"><b class="font-big-white">Dispatch Date</b></th>
									<th width="160" class="text-center"><b class="font-big-white">Delivery Date</b></th>
									<th width="100" class="text-center"><b class="font-big-white">Boxes</b></th>
									<th width="130" class="text-center"><b class="font-big-white">Value</b></th>
									</thead>
									<%
									int iCount = 0;
									 int j=0; 
									 TripScorecardBean tdmBean = new TripScorecardBean();
									Iterator iter = col.iterator();
									while(iter.hasNext()) {
										tvmBean = (TripScorecardBean) iter.next();
										if(iCount % 2 == 0){
									%>
										<tr id="row_stationary<%= iCount%>" class="panel-stop">
										<%} else { %>
										<tr id="row_stationary<%= iCount%>">
										<%} %>
									<td class="text-center" id="<%=iCount %>_Ship_Id"><a href="ETagDashboard.jsp?ShipID=<%= tvmBean.getShip_Id() %>" class="btn btn-info" style="width:40%"><%= tvmBean.getShip_Id() %></a>
									<%if(tvmBean.getStatus() == null || tvmBean.getStatus().equals("") || tvmBean.getStatus().equalsIgnoreCase("Active")) {%>
									<a id="<%=tvmBean.getShip_Id() %>_running" data-toggle="modal" data-target="#assignModal1" style="color:red;text-decoration:underline;" href="#" onclick="doEnd('<%=tvmBean.getShip_Id() %>')">End Trip</a>
									<a id="<%=tvmBean.getShip_Id() %>_delete" data-toggle="modal" data-target="#assignModal2" style="color:red;text-decoration:underline;" href="#" onclick="doDelete('<%=tvmBean.getShip_Id() %>')">Delete Trip</a>
									<a id="<%=tvmBean.getShip_Id() %>_download"  style="color:red;text-decoration:underline;display:none;" href="DownloadShipmentData.jsp?ShipID=<%=tvmBean.getShip_Id() %>" target="new">Export Data</a>
									<%} else if(tvmBean.getStatus().equalsIgnoreCase("Completed")) {%>
									<a id="<%=tvmBean.getShip_Id() %>_download"  style="color:red;text-decoration:underline" href="DownloadShipmentData.jsp?ShipID=<%=tvmBean.getShip_Id() %>" target="new">Export Data</a>
									<%} %>
									</td>
									<td class="text-center" id="<%=iCount %>_Origin"><%= tvmBean.getOrigin() %></td>
									<td class="text-center" id="<%=iCount %>_Origin"><%= tvmBean.getDestination() %></td>
									<%d = df.parse(tvmBean.getDeparture_Date());
			            			//newTime = d.getTime();
			            			//newTime +=(330*60*1000);
			            			//d2 = new Date(newTime);
			            			%>
									<td class="text-center" id="<%=iCount %>_Origin"><%= sdf.format(d) %></td>
									<%d = df.parse(tvmBean.getArrival_Date());
			            			//newTime = d.getTime();
			            			//newTime +=(330*60*1000);
			            			//d2 = new Date(newTime); 
			            			%>
									<td class="text-center" id="<%=iCount %>_Origin"><%= sdf.format(d) %></td>
									<td class="text-center" id="<%=iCount %>_Origin"><%= tvmBean.getBoxes() %></td>
									<td class="text-center" id="<%=iCount %>_Origin"><%= tvmBean.getValue_Inr() %></td>
									</tr>
									<%
									iCount++;	
									} %>
									</table>	
									
									<input type="hidden" id="endTripID" name="endTripID" value="">
									<input type="hidden" id="deleteTripID" name="deleteTripID" value="">
								<br>
						<input type="hidden" id="lastValue_stationary" name="lastValue_stationary" value="<%= iCount%>">
		</div>
		</form>
		</section>
	<div class="modal fade" id="assignModal1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-center">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Action</h4>
      </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row">
                    
                    
                    	<div class="col-md-6 text-center">
                    	<b>This will detach all tags from the trip !
					Are you sure?</b><br><br><br>
						<div id="selectTime">
							<div class="row">
								<div class="col-md-offset-2 col-md-3 text-center">
								End Trip Time: 
								</div>
								<div class="col-md-4">
								<input class='form-control-no-height' type="text" id="0_end" name="0_end">
								</div>
							</div>
							<br>
							<div class="alert alert-danger text-center" role="alert" id="alertFailure" style="display:none;"></div>
						</div>
						<br>
                         <button type="button" class="btn btn-info" onclick="showDownload()">Yes</button>&nbsp;&nbsp;&nbsp;
                         <button type="button" class="btn btn-info" data-dismiss="modal">No</button>
                         </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer text-center">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
    </div>
    
    <div class="modal fade" id="assignModal2">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-center">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Action</h4>
      </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row">
                    
                    
                    	<div class="col-md-6 text-center">
                    	<b>This will delete the shipment!
					Are you sure?</b><br><br><br>
                         <button type="button" class="btn btn-info" data-dismiss="modal" onclick="showDelete()">Yes</button>&nbsp;&nbsp;&nbsp;
                         <button type="button" class="btn btn-info" data-dismiss="modal">No</button>
                         </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer text-center">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
    </div>
    
    
    
				</div>
		<!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0
			</div>
			<strong>Copyright &copy; 2017 <a href="http://tagbox.in">TagBox</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Control Sidebar -->
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->
	
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>


	<!-- Bootstrap 3.3.6 -->
	<script src="../../bootstrap/js/bootstrap.min.js"></script>
	<!-- ChartJS 1.0.1 -->
	<!-- AdminLTE App -->
	<script src="../../dist/js/app.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="../../dist/js/demo.js"></script>
	<script>
	$("#0_end").datetimepicker({
		oneLine: true,
		controlType: 'select',
		minDate: 0,
		maxDate: 30,
		dateFormat: 'dd-mm-yy'
	});
	$('#assignModal1').on('shown.bs.modal', function () {
		$("#alertFailure").hide();
		$("#0_end").val("");
	})
	</script>
<script type="text/javascript">

function doEnd(tagid) {
	$("#endTripID").val(tagid);
}

function doDelete(tagid) {
	$("#deleteTripID").val(tagid);
}

function showDownload() {
	$("#alertFailure").hide();
	var a = $("#endTripID").val();
	var b = $("#0_end").val();
	if(b == "") {
		$("#alertFailure").html("Please enter a time value").show();
		return;
	}
	var posting = $.post("../../pages/jsp/GetShipmentData.jsp",
			{
				Shipment_Id:a,
				End_time:b,
				query:"endShipment"
			},
			function(data,status){
				//alert(data);
				xmlDoc = loadXMLString(data);
				if($(xmlDoc).find("rows").length == 0) {
					return;
				}
				
				$(xmlDoc).find("row").each(function(){
					var id = $(this).find("Return").text();
				});
				
			});
	
	$("#"+a+"_download").show();
	$("#"+a+"_running").hide();
	$("#"+a+"_delete").hide();
	$('#assignModal1').modal('hide')
}

function showDelete() {
	var a = $("#deleteTripID").val();
	var posting = $.post("../../pages/jsp/GetShipmentData.jsp",
			{
				Shipment_Id:a,
				query:"deleteShipment"
			},
			function(data,status){
				//alert(data);
				xmlDoc = loadXMLString(data);
				if($(xmlDoc).find("rows").length == 0) {
					return;
				}
				
				$(xmlDoc).find("row").each(function(){
					var id = $(this).find("Return").text();
				});
				
			});
	
	$("#"+a+"_download").hide();
	$("#"+a+"_running").hide();
	$("#"+a+"_delete").hide();
}

function loadXMLString(txt) 
{
	if (window.DOMParser)
	{
		parser=new DOMParser();
		xmlDoc=parser.parseFromString(txt,"text/xml");
	}
	else // Internet Explorer
	{
		xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
		xmlDoc.async=false;
		xmlDoc.loadXML(txt); 
	}
	return xmlDoc;
}
	

</script>
	</body>
	</html>
