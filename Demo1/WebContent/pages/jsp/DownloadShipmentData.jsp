<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ page import="com.demo1.dao.TripScorecardDao"%>
<%@ page import="com.demo1.dao.TagAssignmentDao"%>
<%@ page import="com.demo1.bean.TripScorecardBean"%>
<%@ page import="com.demo1.bean.TagAssignmentBean"%>
<%@ page import="com.demo1.util.Utils"%>
<%@ page import="java.util.TreeMap"%>
<%@ page import="java.util.Set"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.util.Collection"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@ page import="java.util.Set"%>
<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>
<%@ page import="com.demo1.dao.TemperatureDao"%>
<%@ page import="com.demo1.bean.TemperatureBean"%>
<%@ page import="com.demo1.bean.ShipmentBean"%>
<%@ page import="com.demo1.dao.ShipmentDao"%>
<%
response.setHeader("Cache-Control","no-cache"); 
response.setHeader("Pragma","no-cache"); 
response.setDateHeader ("Expires", -1); 
%>
<%!
static final Logger logger = LoggerFactory.getLogger("DownloadShipmentData");
%>
<%
String sUsername = (String) session.getAttribute("username");
if(sUsername == null || sUsername.equals("")) {
	session.setAttribute("callingPage", "DownloadShipmentData.jsp");
	response.sendRedirect("../../index.html");
}
java.util.Date dt = new java.util.Date();
java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd-MM-yyyy HH:mm");
sdf.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
String currentTime = sdf.format(dt);
SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MMM HH:mm");
df.setTimeZone(java.util.TimeZone.getTimeZone("UTC"));
sdf1.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
sdf2.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
//Temporatily using Value_INR field to save trip status to display the alert bell
String sShipmentId = request.getParameter("ShipID");
String temperature = "", temperature1 = "";
String time = "", time1 = "";
String sMin1 = "", sMax1 = "";
TemperatureDao td = new TemperatureDao();
Collection col = null;
TemperatureBean tdBean = new TemperatureBean();
Date d = null;
ShipmentDao sd = new ShipmentDao();
ShipmentBean sb = new ShipmentBean();
Collection<ShipmentBean> colAllShipments = sd.getAllShipments();
if(sShipmentId != null) {
	col = td.selectTemperature(sShipmentId, "All");
	Iterator iterSt = col.iterator();
	for(ShipmentBean s: colAllShipments) {
		if(s.getShipmentId() == Integer.parseInt(sShipmentId)) {
			sb = s;
		}
	}
	while(iterSt.hasNext()) {
		tdBean = (TemperatureBean) iterSt.next();
		//System.out.println(tdBean.toString());
			d = df.parse(tdBean.getTimestamp());
			temperature += "\"" + tdBean.getTemperature() + "\", ";
			time += "\"" + sdf1.format(d) + "\", ";
			sMin1 += "\"2\", ";
			sMax1 += "\"8\", ";
	}
	if(temperature.equals("") || time.equals("")){
		temperature1 = "00";
		time1 = "00";
	} else {
		if(temperature.length() >= 2)	{
			temperature1 = temperature.substring(0, (temperature.length() - 2));
			sMin1 = sMin1.substring(0, (sMin1.length() - 2));
			sMax1 = sMax1.substring(0, (sMax1.length() - 2));
		}
			if(time.length() >= 2) time1 = time.substring(0, (time.length() - 2));
	}
}

Date d2 = null;
Long newTime = null;
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Download Data</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Add these below two lines for autocomplete and the javascript function at the end  -->
<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="../../bootstrap/css/bootstrap.css">
   
<!-- Font Awesome -->
<script src="https://use.fontawesome.com/37ba1872d2.js"></script>
<!-- Theme style -->
<link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../../dist/css/tagbox.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
<link href="https://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet">
<link href="../../dist/css/select2.css" rel="stylesheet" />
<script src="../../dist/js/select2.js"></script>
<script src="../../dist/js/table2download.js"></script>
<link rel="stylesheet" href="../../dist/css/bootstrap-switch.css">
<script src="../../dist/js/bootstrap-switch.js"></script>

<script type="text/javascript" src="../../plugins/tableExport/tableExport.js"></script>
<script type="text/javascript" src="../../plugins/tableExport/jquery.base64.js"></script>
<script type="text/javascript" src="../../plugins/tableExport/jspdf/libs/sprintf.js"></script>
<script type="text/javascript" src="../../plugins/tableExport/jspdf/jspdf.js"></script>
<script type="text/javascript" src="../../plugins/tableExport/jspdf/libs/base64.js"></script>
<script type="text/javascript" src="../../plugins/tableExport/html2canvas.js"></script>
<!-- <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jspdf/1.0.272/jspdf.min.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.3/jspdf.debug.js"></script>
<script src="../../dist/js/autoTable.js"></script>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
  body {
	font-family:  Lato, sans-serif;
	font-weight: 600;
}
</style>
<style>
.save
{
	display:none;
}
.no-border {
	border: none !important;
	border: 0 !important;
}
</style>
<STYLE TYPE="text/css">
			table.sortable span.sortarrow {
   				color: blue;
   				text-decoration: none;
			}
					
		</style>
<script src="../../dist/js/sorttable.js"></script>
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
	<div class="wrapper">

		<header class="main-header">
			<!-- Logo -->
			<div>
				<a href="#" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
					<span class="logo-mini"><b>Demo</b></span> <!-- logo for regular state and mobile devices -->
					<span class="logo-lg"><b>Demo</b> </span>
				</a>
			</div>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">

				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
					role="button"> <span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
				</a>
				<label class="navbar-form pull-left" role="search">
					<div class="form-group">
						<b style="padding-left: 15px; font-size: 20px;color:#fff;" class="pull-right">Download Shipment Data</b><br> <b style="padding-left: 15px;color:#fff;"><%= currentTime %></b>
					</div>
				</label>

				<div class="navbar-custom-menu">
					<img src="../../dist/img/Logo.png" class="logo-class pull-right"
						alt="User Image">
				</div>
			</nav>

		</header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->


				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<br>
				<ul class="sidebar-menu">
					<li class="header">MAIN NAVIGATION</li>
					<li class="treeview"><a href="#"> <i
							class="fa fa-cubes"></i> <span>Shipment Summary</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="TodayShipmentSummary.jsp"><i
									class="fa fa-circle-o"></i> Home</a></li>
						</ul></li>
					<li class="treeview active"><a href="#"> <i
							class="fa fa-dashboard"></i> <span>Dashboard</span>
					</a>
						<ul class="treeview-menu">
							<li class="active"><a href="TripScorecard.jsp"><i
									class="fa fa-circle-o"></i> Trip Scorecard</a></li>
						</ul></li>
					<!-- <li class="treeview"><a href="#"> <i
							class="fa fa-database"></i> <span>Admin</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="TagAssignment.jsp"><i class="fa fa-circle-o"></i>
									Tag Assignment</a></li>
						</ul></li> -->
					<li class="treeview"><a href="#"> <i
							class="fa fa-th-large"></i> <span>Shipment</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="ShipmentCreation.jsp"><i class="fa fa-circle-o"></i>
									Create Shipment</a></li>
							<li><a href="ShipmentEdit.jsp"><i class="fa fa-circle-o"></i>
									Edit Shipment</a></li>
						</ul></li>
						<li class="treeview"><a href="#"> <i
							class="fa fa-bar-chart"></i> <span>Trip Analysis</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="ETagDashboard.jsp"><i class="fa fa-circle-o"></i>
									Trip Report</a></li>
						</ul></li>
						<!-- <li class="treeview"><a href="#"> <i
							class="fa fa-file-text-o"></i> <span>Reports</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="ETagDashboard.jsp"><i class="fa fa-circle-o"></i>
									Download Historic<br> Data</a></li>
						</ul></li> -->
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper" style="background:white">
			<!-- Content Header (Page header) -->
			
		<section id="mainSection" class="content">
		
			<form id="mainForm" method="post" action="DownloadShipmentData">
			
							
		<div class="container">
                    <div>
						<!-- <span id="csvLink1"></span>&nbsp;&nbsp;&nbsp;&nbsp;<span id="xlsLink1"></span>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" onClick ="$('#temp1_table').tableExport({type:'pdf',escape:'false'});" style="text-decoration:underline">Export to PDF</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" style="text-decoration:underline">Email</a> -->
						<div class="btn-group">
							<button class="btn btn-warning btn-sm dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Export Data</button>
							<ul class="dropdown-menu " role="menu">
								<!-- <li><a href="#" onClick ="$('#temp1_table').tableExport({type:'json',escape:'false'});"> <img src='../../dist/img/json.png' width='24px'> JSON</a></li>
								<li><a href="#" onClick ="$('#temp1_table').tableExport({type:'sql',escape:'false'});"> <img src='../../dist/img/sql.png' width='24px'> SQL</a></li>
								<li class="divider"></li>
								<li><a href="#" onClick ="$('#temp1_table').tableExport({type:'xml',escape:'false'});"> <img src='../../dist/img/xml.png' width='24px'> XML</a></li> -->
								<li><a href="#" onClick ="$('#temp1_table').tableExport({type:'csv',escape:'false'});"> <img src='../../dist/img/csv.png' width='24px'> CSV</a></li>
								<li><a href="#" onClick ="$('#temp1_table').tableExport({type:'excel',escape:'false'});"> <img src='../../dist/img/xls.png' width='24px'> XLS</a></li>
								<li class="divider"></li>				
								<li><a href="#" onClick ="$('#temp1_table').tableExport({type:'txt',escape:'false'});"> <img src='../../dist/img/txt.png' width='24px'> TXT</a></li>
								<!-- <li><a href="#" onClick ="$('#temp1_table').tableExport({type:'doc',escape:'false'});"> <img src='../../dist/img/word.png' width='24px'> Word</a></li>
								<li class="divider"></li> -->
								<!-- <li><a href="#" onClick ="$('#temp1_table').tableExport({type:'pdf',pdfFontSize:'7',escape:'false'});"> <img src='../../dist/img/pdf.png' width='15px'> PDF</a></li> -->
								<li><a href="#" onClick ="exportData()"> <img src='../../dist/img/pdf.png' width='15px'> PDF</a></li>
							</ul>
						</div>	
					</div><br>
					<div>
						<canvas id="Chart3" class="chart1" height="50" width="250"></canvas>
					</div>
					<br>
					<table id="shipment_table" name="shipment_table" class="table table-bordered table-condensed" style="width:48%">
					<thead><tr class="blue"><th class="text-center">Shipment Id</th>
						<th class="text-center">Source</th>
						<th class="text-center">Destination</th>
						<th class="text-center">Departure Date</th>
						<th class="text-center">Arrival Date</th>
					</tr>
					<tbody>
					<tr class="text-center">
						<td><%=sb.getShipmentId() %></td>
						<td><%=sb.getOrigin() %></td>
						<td><%=sb.getDestination() %></td>
						<%if(sb.getETD() == null || sb.getETD().equals("")){%>
						<td>NA</td>
						<%} else { 
						d = df.parse(sb.getETD());
						%>
						<td><%=sdf.format(d) %></td>
						<%}%>
						<%if(sb.getETA() == null || sb.getETA().equals("")){%>
						<td>NA</td>
						<%} else { 
						d = df.parse(sb.getETA());
						%>
						<td><%=sdf.format(d) %></td>
						<%}%>
						</tr>
						</tbody>
					</table>
					<br>
					<div id="table_div" class="table_class" style="display:block">
                    <table id="temp1_table" name="temp1_table" class="table table-condensed table-bordered" style="width:48%">
                    <thead><tr class="blue"><th class="text-center">Shipment ID</th><th class="text-center">Box Id</th><th class="text-center">Time</th><th class="text-center">Temperature</th><th class="text-center">Location</th></tr></thead>
                    <tbody>
                    <%
                    if(col == null || col.size() == 0) {%>
                    	 <tr><td>No Data!</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
                    	
                    <%} else {
                    Iterator iterSt = col.iterator();
                    while(iterSt.hasNext()){ 
                    	tdBean = (TemperatureBean) iterSt.next();
                    	d = df.parse(tdBean.getTimestamp());
            			if(Double.parseDouble(tdBean.getTemperature()) < 2 || Double.parseDouble(tdBean.getTemperature()) > 8) {
                    %>
                    	<tr class="danger text-center"><td><%=tdBean.getZone_Id() %></td><td><%=tdBean.getNode_Id()%></td><td><%=sdf1.format(d)%></td><td class="text-center"><%=tdBean.getTemperature() %></td><td>&nbsp;</td></tr>
                    <%} else {%>
                	<tr class="text-center"><td><%=tdBean.getZone_Id() %></td><td><%=tdBean.getNode_Id()%></td><td><%=sdf1.format(d)%></td><td class="text-center"><%=tdBean.getTemperature() %></td><td>&nbsp;</td></tr>
                	<%}
                    }
                    }%>
                    
                    </tbody>
                    </table>
                    	</div>
                </div>
                
		</form>
		</section>
	
    
    
    
    
    
				</div>
		<!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0
			</div>
			<strong>Copyright &copy; 2017 <a href="http://tagbox.in">TagBox</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Control Sidebar -->
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->
	
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="../../bootstrap/js/bootstrap.min.js"></script>
	<!-- ChartJS 1.0.1 -->
	<!-- AdminLTE App -->
	<script src="../../dist/js/app.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="../../dist/js/demo.js"></script>
	<script>
Chart.defaults.global.legend.display = false;
	
    
var barOptions_stacked = {
	    tooltips: {
	        enabled: true
	    },
	    legend:{
	        display:false
	    },
	    elements: {
            point:{
                radius: 0
            }
        },
	    scales: {
	        xAxes: [{
	                    gridLines: {
	                        display:false
	                    },
	                    ticks: {
	                        autoSkip: true,
	                        maxTicksLimit: 20
	                    }
	                }],
	        yAxes: [{
	                    gridLines: {
	                        display:false
	                    },
	        			ticks: {
	        				stepSize: 10,
	        				suggestedMin: -10,
	        				suggestedMax: 40
	        			}
	                }]
	        },
	        fill: false,
            lineTension: 0,
            backgroundColor: "rgba(75,192,192,0.4)",
            borderColor: "rgba(75,192,192,1)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            spanGaps: false,
	};
	
var ctx2 = document.getElementById("Chart3");
ctx2.height = 50;
var myChart2 = new Chart(ctx2, {
    type: 'line',
    data: {
    	labels : [<%=time1%>],
		datasets : [ {
			label : ["Temperature"],
			fill: false,
			borderColor: "black",
			data : [<%=temperature1%>]
		}, 
		{
			label : ["Min Threshold"],
			backgroundColor: "rgba(54, 162, 235, 0.1)",
			data : [<%= sMin1 %>]
		}, {
			label : ["Max Threshold"],
			backgroundColor: "rgba(54, 162, 235, 0.1)",
			data : [<%= sMax1 %>]
		}]
    },
    options: barOptions_stacked
});

	</script>
<script type="text/javascript">

function doEnd(tagid) {
	$("#endTripID").val(tagid);
}

function showDownload() {
	var a = $("#endTripID").val();
	$("#"+a+"_download").show();
	$("#"+a+"_running").hide();
}

function exportData(){
    var pdf = new jsPDF('p', 'pt', 'letter');

    //Creating the image from chart and adding to the pdf
    var canvas = document.querySelector('#Chart3');
	//creates image
	var canvasImg = canvas.toDataURL("image/jpg", 1.0);
  
	//creates PDF from img
	pdf.setFontSize(20);
	pdf.text(20, 20, "Temperature");
	pdf.addImage(canvasImg, 'JPEG', 50, 50, 500, 180 ); 
    canvas = null;
    var elem = document.getElementById("shipment_table");
    var res = pdf.autoTableHtmlToJson(elem, true);
    pdf.autoTable(res.columns, res.data, {startY: 260,createdCell: function(cell, data) {
   	
    }
    });
    canvas = null;
     var elem = document.getElementById("temp1_table");
     var res = pdf.autoTableHtmlToJson(elem, true);
     pdf.autoTable(res.columns, res.data, {startY: 400,createdCell: function(cell, data) {
    	 //data.row will give the row details. data.row.index is the row number. Or we can print to console data json object to check further details
    	 //console.log(JSON.stringify(data.row))
    	 if (data.column.dataKey == "4") { 
    		 if(data.row.cells[3].text < 2 || data.row.cells[3].text > 8) {
    			 data.row.cells[0].styles.fillColor = [242,222,222];
    			 data.row.cells[1].styles.fillColor = [242,222,222];
    			 data.row.cells[2].styles.fillColor = [242,222,222];
    			 data.row.cells[3].styles.fillColor = [242,222,222];
    			 data.row.cells[4].styles.fillColor = [242,222,222];
    		 }
    	 }
     }
     });
     pdf.save('Export.pdf');
	
    //Adding the html table to the same pdf file 
//     source = $('#table_div')[0];
//     specialElementHandlers = {
//         '#bypassme': function (element, renderer) {
//             return true;
//         }
//     };
//     margins = {
//         top: 300, // Providing high top margin value to make the table below to the image
//         bottom: 60,
//         left: 80,
//         width: 400
//     };
//     pdf.fromHTML(
//     source, 
//     margins.left, 
//     margins.top, { 
//         'width': margins.width, 
//         'elementHandlers': specialElementHandlers
//     },

//     function (dispose) {
//        pdf.save('Export.pdf');
//    }, margins);
}

function ApplyCellStyle(cell, x, y) {
	
    var styles = tableCellsStyles[x + "-" + y]; // Retrieve the cell's style
    if (styles === undefined)
        return;
   // Apply the cell's style.
    cell.styles.cellPadding = styles.cellPadding
    cell.styles.fillColor = styles.fillColor;
    cell.styles.textColor = styles.textColor;
    cell.styles.font = styles.font;
    cell.styles.color = styles.color;
    cell.styles.fontStyle = styles.fontStyle;
}
</script>
	</body>
	</html>
