<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page errorPage="Error.jsp" %>
<%@ page import="com.demo1.dao.TagAssignmentDao"%>
<%@ page import="com.demo1.bean.TagAssignmentBean"%>
<%@ page import="com.demo1.util.Utils"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.TreeMap"%>
<%@ page import="java.util.Set"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.util.Collection"%>
<%@ page import="java.util.Set"%>
<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>

<%!
static final Logger logger = LoggerFactory.getLogger("TagAssignment");
%>
<%
String sUsername = (String) session.getAttribute("username");
if(sUsername == null || sUsername.equals("")) {
	session.setAttribute("callingPage", "TagAssignment.jsp");
	response.sendRedirect("../../index.html");
}
SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MMM HH:mm");
java.util.Date dt = new java.util.Date();
java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd-MM-yyyy HH:mm");
sdf.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
String currentTime = sdf.format(dt);
TagAssignmentDao taDao = new TagAssignmentDao();
TagAssignmentBean taBean = new TagAssignmentBean();
int iUpcoming = 0, iUnderway = 0, iUnassigned = 0;
Collection<TagAssignmentBean> col = taDao.selectAllTagAssignmentRecords();
String sTagId = (String) session.getAttribute("tagid");
String sFlightId = (String) session.getAttribute("flightId");
String sMin = (String) session.getAttribute("min");
String sMax = (String) session.getAttribute("max");

Iterator iter = col.iterator();
for(TagAssignmentBean tb: col) {
	if(Utils.isNumeric(tb.getTrip_Status())) {
		iUnderway++;
	} else {
		iUnassigned++;
	}
}

int tmp = 0;
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Tag Assignment</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Add these below two lines for autocomplete and the javascript function at the end  -->
<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="../../bootstrap/css/bootstrap.css">
   
<!-- Font Awesome -->
<script src="https://use.fontawesome.com/37ba1872d2.js"></script>
<!-- Theme style -->
<link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../../dist/css/tagbox.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
<link href="https://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet">
<link href="../../dist/css/select2.css" rel="stylesheet" />
<script src="../../dist/js/select2.js"></script>
<script src="../../dist/js/tagAssignment.js"></script>
<link rel="stylesheet" href="../../dist/css/datepicker.css">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
  body {
	font-family:  Lato, sans-serif;
	font-weight: 600;
}
</style>
<style>
.save
{
	display:none;
}
.no-border {
	border: none !important;
	border: 0 !important;
}
</style>
<STYLE TYPE="text/css">
			table.sortable span.sortarrow {
   				color: blue;
   				text-decoration: none;
			}
					
		</style>
<script src="../../dist/js/sorttable.js"></script>
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
	<div class="wrapper">

		<header class="main-header">
			<!-- Logo -->
			<div>
				<a href="#" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
					<span class="logo-mini"><b>Demo</b></span> <!-- logo for regular state and mobile devices -->
					<span class="logo-lg"><b>Demo</b> </span>
				</a>
			</div>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">

				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
					role="button"> <span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
				</a>
				<label class="navbar-form pull-left" role="search">
					<div class="form-group">
						<b style="padding-left: 15px; font-size: 20px;color:#fff;" class="pull-right">Tag Assignment</b><br> <b style="padding-left: 15px;color:#fff;"><%= currentTime %></b>
					</div>
				</label>

				<div class="navbar-custom-menu">
					<img src="../../dist/img/Logo.png" class="logo-class pull-right"
						alt="User Image">
				</div>
			</nav>

		</header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->


				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<br>
				<ul class="sidebar-menu">
					<li class="header">MAIN NAVIGATION</li>
					<li class="treeview"><a href="#"> <i
							class="fa fa-cubes"></i> <span>Shipment Summary</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="TodayShipmentSummary.jsp"><i
									class="fa fa-circle-o"></i> Home</a></li>
						</ul></li>
					<li class="treeview"><a href="#"> <i
							class="fa fa-dashboard"></i> <span>Dashboard</span>
					</a>
						<ul class="treeview-menu">
							<li class=""><a href="TripScorecard.jsp"><i
									class="fa fa-circle-o"></i> Trip Scorecard</a></li>
						</ul></li>
					<!-- <li class="treeview active"><a href="#"> <i
							class="fa fa-database"></i> <span>Admin</span>
					</a>
						<ul class="treeview-menu">
							<li class="active"><a href="TagAssignment.jsp"><i class="fa fa-circle-o"></i>
									Tag Assignment</a></li>
						</ul></li> -->
						<li class="treeview"><a href="#"> <i
							class="fa fa-th-large"></i> <span>Shipment</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="ShipmentCreation.jsp"><i class="fa fa-circle-o"></i>
									Create Shipment</a></li>
							<li><a href="ShipmentEdit.jsp"><i class="fa fa-circle-o"></i>
									Edit Shipment</a></li>
						</ul></li>
						<li class="treeview"><a href="#"> <i
							class="fa fa-bar-chart"></i> <span>Trip Analysis</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="ETagDashboard.jsp"><i class="fa fa-circle-o"></i>
									Trip Report</a></li>
						</ul></li>
						<!-- <li class="treeview"><a href="#"> <i
							class="fa fa-file-text-o"></i> <span>Reports</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="ETagDashboard.jsp"><i class="fa fa-circle-o"></i>
									Download Historic<br> Data</a></li>
						</ul></li> -->
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper" style="background:white">
			<!-- Content Header (Page header) -->
			
		<section id="mainSection" class="content">
		<ol class="breadcrumb">
					<li><a href="TripScorecard.jsp"><i class="fa fa-dashboard"></i> Home</a></li>
					<li class="active">Tag Assignment</li>
				</ol>
			<form id="mainForm" method="post" action="TagAssignment.jsp">
			
							<div class="row">
							
				<div class="col-md-offset-3 col-md-2 text-center">
				<label><b># Underway&nbsp;&nbsp;</b></label><br><button class="btn btn-warning"><%=iUnderway %></button>
				</div>
				<div class="col-md-2 text-center">
				<label><b># Upcoming&nbsp;&nbsp;</b></label><br><button class="btn btn-warning"><%=iUpcoming %></button>
				</div>
				<div class="col-md-2 text-center">
				<label><b># Unassigned&nbsp;&nbsp;</b></label><br><button class="btn btn-warning"><%=iUnassigned %></button>
				</div>
							
							</div>
							<br><br>
		
		<div id="listView">		
									<table id="data_table_stationary" class="table table-condensed table-black-border">
									<thead class="blue">
									<th width="100" class="text-center"><b class="font-big-white">Tag ID</b></th>
									<th width="100" class="text-center"><b class="font-big-white">Date of Ship</b></th>
									<th width="100" class="text-center"><b class="font-big-white">Ship Id</b></th>
									<th width="100" class="text-center"><b class="font-big-white">Source</b></th>
									<th width="120" class="text-center"><b class="font-big-white">Destination</b></th>
									<th width="150" class="text-center"><b class="font-big-white">Last Known Location</b></th>
									<th width="130" class="text-center"><b class="font-big-white">Flight Id/PNR</b></th>
									<th width="130" class="text-center"><b class="font-big-white">Trip Status</b></th>
									<th width="130" class="text-center"><b class="font-big-white">Load Criticality</b></th>
									<th width="50" class="text-center"><b class="font-big-white">Min</b></th>
									<th width="50" class="text-center"><b class="font-big-white">Max</b></th>
									<th width="50" class="text-center"><b class="font-big-white">Change Assignment</b></th>
									</thead>
									<%
									int iCount = 0;
									 int j=0; 
									iter = col.iterator();
									while(iter.hasNext()) {
										taBean = (TagAssignmentBean) iter.next();
									if(iCount % 2 == 0){
									%>
										<tr id="row_stationary<%= iCount%>" class="panel-stop">
										<%} else { %>
										<tr id="row_stationary<%= iCount%>">
										<%} %><td class="text-center" id="<%=iCount %>_Tag_Id"><%=taBean.getTag_Id() %></td>
									<%if(taBean.getShip_Date().equals("")){ %>
									<td class="text-center" id="<%=iCount %>_Ship_Date"></td>
									<%} else { %>
									<td class="text-center" id="<%=iCount %>_Ship_Date"><%=sdf1.format(df.parse(taBean.getShip_Date())) %></td>
									<%} %>
									<td class="text-center" id="<%=iCount %>_Inv_Id"><%=taBean.getInv_Id() %></td>
									<td class="text-center" id="<%=iCount %>_Source"><%=taBean.getShip_Source() %></td>
									<td class="text-center" id="<%=iCount %>_Destination"><%=taBean.getShip_Dest() %></td>
									<td class="text-center" id="<%=iCount %>_Location"><%=taBean.getShip_Loc() %></td>
									<% if(taBean.getTag_Id().equals(sTagId)){%>
									<td class="text-center" id="<%=iCount %>_Flight_Id"><%=sFlightId %></td>
									<%} else { 
										if(Utils.isNumeric(taBean.getTrip_Status())){
									%>
									<td class="text-center" id="<%=iCount %>_Flight_Id">SG-278/R6TS34</td>
									<%} else {%>
										<td class="text-center" id="<%=iCount %>_Flight_Id"></td>
										<%}
										} %>
									<%if(Utils.isNumeric(taBean.getTrip_Status())) { %>
									<td class="text-center" id="<%=iCount %>_Trip_Status"><div class="progress">
  										<div class="progress-bar" role="progressbar" aria-valuenow="<%=taBean.getTrip_Status() %>" aria-valuemin="0" aria-valuemax="100" style="width: <%=taBean.getTrip_Status()%>%;">
    										<%=taBean.getTrip_Status() %>%
 										 </div>
										</div></td>
									
									<%} else { %>
									<td id="<%=iCount %>_Trip_Status" class="text-center"><%=taBean.getTrip_Status() %></td>
									<%} %>
									<%if(taBean.getCriticality().equalsIgnoreCase("High")){ %>
									<td id="<%=iCount %>_Criticality" class="text-center"><span class="badge bg-red">HIGH</span></td>
									<%} else if(taBean.getCriticality().equalsIgnoreCase("Medium")){%>
									<td id="<%=iCount %>_Criticality" class="text-center"><span class="badge bg-orange">MEDIUM</span></td>
									<%} else if(taBean.getCriticality().equalsIgnoreCase("Low")){%>
									<td id="<%=iCount %>_Criticality" class="text-center"><span class="badge bg-green">LOW</span></td>
									<%} else { %>
									<td id="<%=iCount %>_Criticality" class="text-center"><span class="badge bg-green">LOW</span></td>
									<%} %>
									<% if(taBean.getTag_Id().equals(sTagId)){%>
									<td id="<%=iCount %>_Min" class="text-center"><%=sMin %></td>
									<td id="<%=iCount %>_Max" class="text-center"><%=sMax %></td>
									
									<%} else { 
										if(Utils.isNumeric(taBean.getTrip_Status())){%>
									<td id="<%=iCount %>_Min" class="text-center">-5</td>
									<td id="<%=iCount %>_Max" class="text-center">10</td>
									<%} else {%>
									<td id="<%=iCount %>_Min" class="text-center"></td>
									<td id="<%=iCount %>_Max" class="text-center"></td>
									<%}
										} %>
									<%if(!Utils.isNumeric(taBean.getTrip_Status())) { %>
									<td class="text-center">
									<table width="100%" class="no-border"><tr class="no-border"><td class="no-border"><i id="edit_button<%=iCount %>" class="fa fa-pencil edit fa-lg fa-color-blue" onclick="edit_row('<%= iCount%>')"></i></td>
									<td class="no-border"><i id="save_button<%=iCount %>" class="fa fa-floppy-o fa-lg save fa-color-blue" onclick="save_row('<%=iCount%>')"></i></td>
									</tr>
									</table>
									</td>
									<%} else { %>
									<td>&nbsp;</td>
									<%} %>
									</tr>
									<%
									iCount++;	
									} %>
									</table>	
									
								<br>
						<input type="hidden" id="lastValue_stationary" name="lastValue_stationary" value="<%= iCount%>">
		</div>
		</form>
		</section>
	
    
    
    
				</div>
		<!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0
			</div>
			<strong>Copyright &copy; 2017 <a href="http://tagbox.in">TagBox</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Control Sidebar -->
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->
	
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
<script src="../../dist/js/bootstrap-datepicker.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="../../bootstrap/js/bootstrap.min.js"></script>
	<!-- ChartJS 1.0.1 -->
	<!-- AdminLTE App -->
	<script src="../../dist/js/app.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="../../dist/js/demo.js"></script>

	</body>
	
</html>
