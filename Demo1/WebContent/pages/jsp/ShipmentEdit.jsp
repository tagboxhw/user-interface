<%@page import="com.demo1.bean.ShipmentTransitInfo"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ page import="com.demo1.dao.ShipmentDao"%>
<%@ page import="com.demo1.bean.ShipmentLocations"%>
<%@ page import="com.demo1.bean.ShipmentTags"%>
<%@ page import="com.demo1.bean.ShipmentTransitInfo"%>
<%@ page import="com.demo1.bean.ShipmentBoxInfo"%>
<%@ page import="com.demo1.bean.ShipmentBean"%>
<%@ page import="com.demo1.bean.ShipmentUsers"%>
<%@ page import="com.demo1.bean.ShipmentProducts"%>
<%@ page import="com.demo1.util.Utils"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.TreeMap"%>
<%@ page import="java.util.Set"%>
<%@ page import="java.util.Date"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.util.Collection"%>
<%@ page import="java.util.Set"%>
<%@ page import="java.util.Calendar"%>
<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>
<%
response.setHeader("Cache-Control","no-cache"); 
response.setHeader("Pragma","no-cache"); 
response.setDateHeader ("Expires", -1); 
%>
<%!
static final Logger logger = LoggerFactory.getLogger("ShipmentEdit");
%>
<%
Calendar cal = Calendar.getInstance();
String sUsername = (String) session.getAttribute("username");
if(sUsername == null || sUsername.equals("")) {
	session.setAttribute("callingPage", "ShipmentEdit.jsp");
	response.sendRedirect("../../index.html");
}
SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
df.setTimeZone(java.util.TimeZone.getTimeZone("UTC"));
Date d = null;
Date d2 = null;
Long newTime = null;
java.util.Date dt = new java.util.Date();
//System.out.println("offset: " + (cal.get(Calendar.ZONE_OFFSET) + cal.get(Calendar.DST_OFFSET)) / (60 * 1000));
java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd-MM-yyyy HH:mm");
sdf.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
String currentTime = sdf.format(dt);
String sShipmentId = (String) request.getParameter("shipment_id");
ShipmentDao sd = new ShipmentDao();
ShipmentBean sb = new ShipmentBean();
sb.init();
Collection<ShipmentBean> colAllShipments = null;
Collection<ShipmentLocations> colLocs = null;
Collection<ShipmentTags> colTags = null;
Collection<ShipmentUsers> colUsers = null;
Collection<ShipmentProducts> colProducts = null;
Collection<ShipmentTransitInfo> colTransitInfo = null;
Collection<ShipmentBoxInfo> colBoxInfo = null;
colAllShipments = sd.getAllShipments();
String[] sUsersArray = null;
String sProducts= "", sProducts1 = "", sLocations = "", sLocations1 = "";
if(sShipmentId != null) {
	colLocs = sd.getShipmentLocations();
	colTags = sd.getShipmentTags();
	colUsers = sd.getShipmentUsers();
	colProducts = sd.getShipmentProducts();
	for(ShipmentBean s: colAllShipments) {
		if(s.getShipmentId() == Integer.parseInt(sShipmentId)) {
			sb = s;
		}
	}
	colTransitInfo = sd.selectShipmentTransitDetails(sShipmentId);
	colBoxInfo = sd.selectShipmentBoxDetails(sShipmentId);
	sUsersArray = sb.getUser().split(",");
	for(ShipmentProducts s: colProducts) {
		sProducts = sProducts + ", \"" + s.getShipmentProductId() + "\"";
		sProducts1 = sProducts1 + "," + s.getShipmentProductId();
	}
	if(sProducts.length()>2) {
		sProducts = sProducts.substring(2);
		sProducts1 = sProducts1.substring(1);
	}
	for(ShipmentLocations s: colLocs) {
		sLocations = sLocations + ",\"" + s.getShipmentLocationId() + "\"";
		sLocations1 = sLocations1 + "," + s.getShipmentLocationId();
	}
	if(sLocations.length()>2) {
		sLocations = sLocations.substring(1);
		sLocations1 = sLocations1.substring(1);
	}
}
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Shipment Edit</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<link rel="stylesheet" href="../../dist/css/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- Add these below two lines for autocomplete and the javascript function at the end  -->
<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="../../bootstrap/css/bootstrap.css">
   
<!-- Font Awesome -->
<script src="https://use.fontawesome.com/37ba1872d2.js"></script>
<!-- Theme style -->
<link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../../dist/css/tagbox.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
<link href="https://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet">
<link href="../../dist/css/select2.css" rel="stylesheet" />
<script src="../../dist/js/select2.js"></script>
<script src="../../dist/js/shipmentEdit.js"></script>
<link rel="stylesheet" href="../../dist/css/timepicker1.css">
<script src="../../dist/js/timepicker1.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#0_TagId").select2();
	$(".text-uppercase").keyup(function () {
	    this.value = this.value.toLocaleUpperCase();
	});
});
</script>
<script>
  $( function() {
    var availableProducts = [
      <%= sProducts %>
    ];
    $( "#0_ProductId" ).autocomplete({
      source: availableProducts
    });
    var availableLocations = [
         <%= sLocations %>
       ];
       $( "#Source" ).autocomplete({
         source: availableLocations
       });
       $( "#Destination" ).autocomplete({
           source: availableLocations
         });
       $( "#0_Source" ).autocomplete({
           source: availableLocations
         });
       $( "#0_Destination" ).autocomplete({
           source: availableLocations
    });
  } );
  $(".text-uppercase").keyup(function () {
	    this.value = this.value.toLocaleUpperCase();
	});
  </script>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
  body {
	font-family:  Lato, sans-serif;
	font-weight: 600;
}
</style>
<style>
.save
{
	display:none;
}
.no-border {
	border: none !important;
	border: 0 !important;
}
</style>
<STYLE TYPE="text/css">
			table.sortable span.sortarrow {
   				color: blue;
   				text-decoration: none;
			}
					
		</style>
<script src="../../dist/js/sorttable.js"></script>
<script src="../../dist/js/table_script.js"></script>
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
	<div class="wrapper">

		<header class="main-header">
			<!-- Logo -->
			<div>
				<a href="#" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
					<span class="logo-mini"><b>Demo</b></span> <!-- logo for regular state and mobile devices -->
					<span class="logo-lg"><b>Demo</b> </span>
				</a>
			</div>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">

				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
					role="button"> <span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
				</a>
				<label class="navbar-form pull-left" role="search">
					<div class="form-group">
						<b style="padding-left: 15px; font-size: 20px;color:#fff;" class="pull-right">Edit Shipment</b><br> <b style="padding-left: 15px;color:#fff;"><%= currentTime %></b>
					</div>
				</label>

				<div class="navbar-custom-menu">
					<img src="../../dist/img/Logo.png" class="logo-class pull-right"
						alt="User Image">
				</div>
			</nav>

		</header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->


				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<br>
				<ul class="sidebar-menu">
					<li class="header">MAIN NAVIGATION</li>
					<li class="treeview"><a href="#"> <i
							class="fa fa-cubes"></i> <span>Shipment Summary</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="TodayShipmentSummary.jsp"><i
									class="fa fa-circle-o"></i> Home</a></li>
						</ul></li>
					<li class="treeview"><a href="#"> <i
							class="fa fa-dashboard"></i> <span>Dashboard</span>
					</a>
						<ul class="treeview-menu">
							<li class=""><a href="TripScorecard.jsp"><i
									class="fa fa-circle-o"></i> Trip Scorecard</a></li>
						</ul></li>
					<!-- <li class="treeview"><a href="#"> <i
							class="fa fa-database"></i> <span>Admin</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="TagAssignment.jsp"><i class="fa fa-circle-o"></i>
									Tag Assignment</a></li>
						</ul></li> -->
						<li class="treeview active"><a href="#"> <i
							class="fa fa-th-large"></i> <span>Shipment</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="ShipmentCreation.jsp"><i class="fa fa-circle-o"></i>
									Create Shipment</a></li>
							<li class="active"><a href="ShipmentEdit.jsp"><i class="fa fa-circle-o"></i>
									Edit Shipment</a></li>
						</ul></li>
						<li class="treeview"><a href="#"> <i
							class="fa fa-bar-chart"></i> <span>Trip Analysis</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="ETagDashboard.jsp"><i class="fa fa-circle-o"></i>
									Trip Report</a></li>
						</ul></li>
						<!-- <li class="treeview"><a href="#"> <i
							class="fa fa-file-text-o"></i> <span>Reports</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="ETagDashboard.jsp"><i class="fa fa-circle-o"></i>
									Download Historic<br> Data</a></li>
						</ul></li> -->
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper" style="background:white">
			<!-- Content Header (Page header) -->
			
		<section id="mainSection" class="content">
			<ul class="nav navbar-nav">
            <li id="li_link1"><a href="#" onclick="showStep('1')" class="current_step" id="link1">Step 1<span>Edit Shipment</span></a></li>
            <li id="li_link2"><a href="#" onclick="showStep('2')" id="link2">Step 2<span>Edit Shipment Legs</span></a></li>
            <li id="li_link3"><a href="#" onclick="showStep('3')" id="link3">Step 3<span>Edit Shipment Tag Assignment</span></a></li>
            <li id="li_link4"><a href="#" onclick="showStep('4')" id="link4">Step 4<span>Edit Shipment User Assignment</span></a></li>
          </ul>
        <div class="row">
        	<div class="col-md-12">
        	<%if(sShipmentId == null) { %>
        		<br><br>
				<div class="row">
        			<div class="col-md-offset-1 col-md-10">
        				<form method="post" action="ShipmentEdit.jsp" id="myForm"><br>
								<table id="shipment_table" class="table table-bordered table-striped table-condensed">
					<tr class="info text-center">
						<th width="20%" class="text-center"><h4>Shipment Id</h4></th>
						<th width="20%" class="text-center"><h4>Source</h4></th>
						<th width="20%" class="text-center"><h4>Destination</h4></th>
						<th width="20%" class="text-center"><h4>Departure Date</h4></th>
						<th width="20%" class="text-center"><h4>Arrival Date</h4></th>
					</tr>
					<%for(ShipmentBean s: colAllShipments){ %>
					<tr class="text-center">
						<td><a href="ShipmentEdit.jsp?shipment_id=<%=s.getShipmentId() %>" class="btn btn-info" style="width:50%" data-toggle="tooltip" data-placement="top" title="Edit Shipment Id <%= s.getShipmentId()%>"><%=s.getShipmentId() %></a></td>
						<td><%=s.getOrigin() %></td>
						<td><%=s.getDestination() %></td>
						<%if(s.getETD() == null || s.getETD().equals("")){%>
						<td>NA</td>
						<%} else { 
						d = df.parse(s.getETD());
						//newTime = d.getTime();
						//newTime +=(330*60*1000);
						//d2 = new Date(newTime);
						%>
						<td><%=sdf.format(d) %></td>
						<%}%>
						<%if(s.getETA() == null || s.getETA().equals("")){%>
						<td>NA</td>
						<%} else { 
						d = df.parse(s.getETA());
						//newTime = d.getTime();
						//newTime +=(330*60*1000);
						//d2 = new Date(newTime);
						%>
						<td><%=sdf.format(d) %></td>
						<%}%>
						</tr>
					<%} %>
					</table>
						</form>
					</div>
				</div>
				
				<%} else if(sShipmentId != null) { %>
				<br>
				<input type="hidden" name="shipment_id" id="shipment_id" value="<%= sShipmentId %>">
				<h3>Editing Shipment Id: <%= sShipmentId %></h3><a href="ShipmentEdit.jsp" style="text-decoration:underline;color:gray">Edit a different Shipment</a>
				<br><br>
				<div id="step1">
  					<div class="row">
  						<div class="col-md-offset-2 col-md-8">
  							<div class="alert alert-danger text-center" role="alert" id="alertFailure" style="display:none;"></div>
  							<div class="well" id="DetailsDiv">
  								<div class="row">
          							<div class="col-md-offset-1 col-md-2">
          								<label>Source</label>
          							</div>
          							<div class="col-md-3">
	  									<input id="Source" name="Source" class='form-control-no-height text-uppercase' type='text' value='<%=sb.getOrigin()%>'>
          									<%
          									if(sb.getInvoiceNumber() == null) sb.setInvoiceNumber("");
          									%>
          							</div>
          							<div class="col-md-2">
          								<label>Destination</label>
          							</div>
          							<div class="col-md-3">
	  									<input id="Destination" name="Destination" class='form-control-no-height text-uppercase' type='text' value='<%=sb.getDestination()%>'>
          							</div>
          						</div>
          						<br>
          						<div class="row">
          							<div class="col-md-offset-1 col-md-2">
          								<label>Departure Date</label>
          							</div>
          							<div class="col-md-3">
          							<%
          							if(sb.getETD() == null || sb.getETD().equals("")) {%>
          							<input class='form-control-no-height' placeholder="Dep Date" type="text" id="etd" name="etd" value="">
          							<% } else {
          							d = df.parse(sb.getETD());
										//newTime = d.getTime();
										//newTime +=(330*60*1000);
										//d2 = new Date(newTime);
									%>
          								<input class='form-control-no-height' placeholder="Dep Date" type="text" id="etd" name="etd" value="<%= sdf.format(d)%>">
          								<%} %>
          							</div>
          							<div class="col-md-2">
          								<label>Arrival Date</label>
          							</div>
          							<div class="col-md-3">
          							<%
          							if(sb.getETA() == null || sb.getETA().equals("")) {%>
          							<input class='form-control-no-height' placeholder="Arrival Date" type="text" id="eta" name="eta" value="">
          							<%} else {
          							d = df.parse(sb.getETA());
										//newTime = d.getTime();
										//newTime +=(330*60*1000);
										//d2 = new Date(newTime);
									%>
          								<input class='form-control-no-height' placeholder="Arrival Date" type="text" id="eta" name="eta" value="<%= sdf.format(d)%>">
          								<%} %>
          							</div>
          						</div>
          						<br>
          						<div class="row">
          							<div class="col-md-offset-1 col-md-2">
          								<label>Invoice Number</label>
          							</div>
          							<div class="col-md-3">
          								<input class='form-control-no-height' type="text" id="invoice_number" name="invoice_number" value="<%=sb.getInvoiceNumber() %>">
          							</div>
          						</div>
							</div>
							<br>
							<button onclick="CheckStep1AndSubmit()" class="btn btn-success pull-right" type="button">SAVE &amp; CONTINUE&nbsp;<i class="fa fa-chevron-right fg-lg"></i></button>
  						</div>
  					</div>
          		</div><!-- End of Step1 -->
          		<div id="step2" style="display:none;">
          		<div class="row">
  			<div class="col-md-offset-1 col-md-10">
  			<br>
  			<div id="show_stops" class="text-center"><b class="font-blue font-big"><%=sb.getOrigin() %></b>&nbsp;&nbsp;<i class="fa fa-long-arrow-right font-blue font-big"></i>&nbsp;&nbsp;<b class="font-blue font-big"><%=sb.getDestination() %></b></div>
  			<br>
  			<div class="alert alert-danger text-center" role="alert" id="alertFailureStep2" style="display:none;"></div>
  				<table id="leg_table_step2" class="table table-bordered table-striped table-condensed">
					<tr class="info text-center">
						<th width="15%" class="text-center"><h4>Source</h4></th>
						<th width="15%" class="text-center"><h4>Destination</h4></th>
						<th width="15%" class="text-center"><h4>Departure Date</h4></th>
						<th width="15%" class="text-center"><h4>Arrival Date</h4></th>
						<th width="10%" class="text-center"><h4>Carrier Id</h4></th>
						<th width="10%" class="text-center"><h4>Mode</h4></th>
						<th width="20%" class="text-center"><h4>Add/Delete Legs</h4></th>
					</tr>
					<%
					int j = 0;
					if(colTransitInfo == null || colTransitInfo.size() == 0){%>
						<tr id="row_leg0" style="background:#ddd">
						<td class="text-center">
	  									<input id="0_Source" name="0_Source" class='form-control-no-height' type='text' disabled>
						</td>
						<td class="text-center">
	  						<input id="0_Destination" name="0_Destination" class='form-control-no-height' type='text' disabled>
						</td>
						<td class="text-center"><input class='form-control-no-height' placeholder="ETD" type="text" id="0_etd" name="0_etd" value="" disabled></td>
						<td class="text-center"><input class='form-control-no-height' placeholder="ETA" type="text" id="0_eta" name="0_eta" value="" disabled></td>
						<td class="text-center"><input class='form-control-no-height' placeholder="" type="text" id="0_carrier" name="0_carrier" value=""></td>
						<td class="text-center">
						<select class='form-control-no-height' id="0_Mode" name="0_Mode" >
							<option selected>AIR</option>
          					<option>MARINE</option>
          					<option>SURFACE</option>
          				</select>
						</td>
						<td class="text-center"><button type="button" class="add btn btn-sm btn-warning" onclick="add_leg();add_hops();" data-toggle="tooltip" data-placement="top" title="Add Leg" value="Add Row"><i class='fa fa-plus'></i></button></td>
					</tr>
					<%j++;
					} else {
					for(ShipmentTransitInfo sti: colTransitInfo) {
						if(j == 0){
						%>
					
					<tr id="row_leg<%=j%>" style="background:#ddd">
						<td class="text-center">
							<input id="<%=j %>_Source" name="<%=j %>_Source" class='form-control-no-height' type='text' value='<%=sti.getOrigin()%>' disabled>
						</td>
						<td class="text-center">
							<input id="<%=j %>_Destination" name="<%=j %>_Destination" class='form-control-no-height' type='text' value='<%=sti.getDestination()%>' disabled>
						</td>
						<%
						if(sti.getETD() == null || sti.getETD().equals("")) {%>
						<td class="text-center"><input class='form-control-no-height' placeholder="ETD" type="text" id="<%=j %>_etd" name="<%=j %>_etd" value="" disabled></td>
						<%} else { 
						d = df.parse(sti.getETD());
						//newTime = d.getTime();
						//newTime +=(330*60*1000);
						//d2 = new Date(newTime);
									%>
						<td class="text-center"><input class='form-control-no-height' placeholder="ETD" type="text" id="<%=j %>_etd" name="<%=j %>_etd" value="<%=sdf.format(d) %>" disabled></td>
						<%} %>
						<%
						if(sti.getETA() == null || sti.getETA().equals("")) {%>
						<td class="text-center"><input class='form-control-no-height' placeholder="ETA" type="text" id="<%=j %>_eta" name="<%=j %>_eta" value="" disabled></td>
						<%} else {
						d = df.parse(sti.getETA());
						//newTime = d.getTime();
						//newTime +=(330*60*1000);
						//d2 = new Date(newTime);
									%>
						<td class="text-center"><input class='form-control-no-height' placeholder="ETA" type="text" id="<%=j %>_eta" name="<%=j %>_eta" value="<%=sdf.format(d) %>" disabled></td>
						<%} %>
						<td class="text-center"><input class='form-control-no-height' placeholder="" type="text" id="<%=j %>_carrier" name="<%=j %>_carrier" value="<%= sti.getCarrier()%>"></td>
						<td class="text-center"><select class='form-control-no-height' id="<%=j %>_Mode" name="<%=j %>_Mode" >
						<%if(sti.getMode() != null && sti.getMode().equalsIgnoreCase("AIR")) { %>
          					<option selected>AIR</option>
          					<option>MARINE</option>
          					<option>SURFACE</option>
          					<%} else if(sti.getMode() != null && sti.getMode().equalsIgnoreCase("MARINE")) { %>
          					<option >AIR</option>
          					<option selected>MARINE</option>
          					<option>SURFACE</option>
          					<%} else if(sti.getMode() != null && sti.getMode().equalsIgnoreCase("SURFACE")) { %>
          					<option>AIR</option>
          					<option>MARINE</option>
          					<option selected>SURFACE</option>
          					<%} else { %>
          					<option selected>AIR</option>
          					<option>MARINE</option>
          					<option>SURFACE</option>
          					<%} %>
          				</select></td>
						<td class="text-center"><button type="button" class="add btn btn-sm btn-warning" onclick="add_leg();add_hops();" data-toggle="tooltip" data-placement="top" title="Add Leg" value="Add Row"><i class='fa fa-plus'></i></button></td>
					</tr>
					<%
						} else {%>
							<tr id="row_leg<%=j%>">
						<td class="text-center" colspan="2">
							<input id="<%=j %>_Source" name="<%=j %>_Source" class='form-control-no-height text-uppercase' type='text' value='<%=sti.getOrigin()%>' onchange="add_hops();">
						</td>
						<%
						if(sti.getETD() == null || sti.getETD().equals("")) {%>
						<td class="text-center"><input class='form-control-no-height' placeholder="ETD" type="text" id="<%=j %>_etd" name="<%=j %>_etd" value=""></td>
						<%} else {
						d = df.parse(sti.getETD());
						//newTime = d.getTime();
						//newTime +=(330*60*1000);
						//d2 = new Date(newTime);
									%>
						<td class="text-center"><input class='form-control-no-height' placeholder="ETD" type="text" id="<%=j %>_etd" name="<%=j %>_etd" value="<%=sdf.format(d) %>"></td>
						<%} %>
						<%
						if(sti.getETA() == null || sti.getETA().equals("")) {%>
						<td class="text-center"><input class='form-control-no-height' placeholder="ETA" type="text" id="<%=j %>_eta" name="<%=j %>_eta" value=""></td>
						<%} else {
						d = df.parse(sti.getETA());
						//newTime = d.getTime();
						//newTime +=(330*60*1000);
						//d2 = new Date(newTime);
									%>
						<td class="text-center"><input class='form-control-no-height' placeholder="ETA" type="text" id="<%=j %>_eta" name="<%=j %>_eta" value="<%=sdf.format(d) %>"></td>
						<%} %>
						<td class="text-center"><input class='form-control-no-height' placeholder="" type="text" id="<%=j %>_carrier" name="<%=j %>_carrier" value="<%= sti.getCarrier()%>"></td>
						<td class="text-center"><select class='form-control-no-height' id="<%=j %>_Mode" name="<%=j %>_Mode" >
						<%if(sti.getMode() != null && sti.getMode().equalsIgnoreCase("AIR")) { %>
          					<option selected>AIR</option>
          					<option>MARINE</option>
          					<option>SURFACE</option>
          					<%} else if(sti.getMode() != null && sti.getMode().equalsIgnoreCase("MARINE")) { %>
          					<option >AIR</option>
          					<option selected>MARINE</option>
          					<option>SURFACE</option>
          					<%} else if(sti.getMode() != null && sti.getMode().equalsIgnoreCase("SURFACE")) { %>
          					<option>AIR</option>
          					<option>MARINE</option>
          					<option selected>SURFACE</option>
          					<%} else { %>
          					<option selected>AIR</option>
          					<option>MARINE</option>
          					<option>SURFACE</option>
          					<%} %>
          				</select></td>
						<td class="text-center"><button type="button" class="add btn btn-sm btn-warning" onclick="add_leg();add_hops();" data-toggle="tooltip" data-placement="top" title="Add Leg" value="Add Row"><i class='fa fa-plus'></i></button>&nbsp;&nbsp;<button type='button' class='add btn btn-sm btn-danger' onclick="delete_leg(<%=j %>);add_hops();" data-toggle='tooltip' data-placement='top' title='Remove Leg' value='Remove Leg'><i class='fa fa-minus'></i></button></td>
					</tr>
						<%}
						j++;
					} 
					}%>
				</table>
				<input type="hidden" id="lastValue_leg" name="lastValue_leg" value="<%= j %>">
				<br>
				<a href="#" onclick="showStep('1')" class="btn btn-success pull-left"><i class="fa fa-chevron-left fg-lg"></i>&nbsp;EDIT STEP 1</a>
				<a href="#" onclick="CheckStep2AndSubmit()" class="btn btn-success pull-right">SAVE &amp; CONTINUE&nbsp;<i class="fa fa-chevron-right fg-lg"></i></a>
			
  			</div>
  		</div>
          		</div><!-- End of Step2 -->
          		<div id="step3" style="display:none;">
          		<div class="row">
  			<div class="col-md-offset-1 col-md-10">
  			<br>
  			<input type="hidden" name="products_id" id="products_id" value='<%=sProducts1 %>'>
  			<input type="hidden" name="locations_id" id="locations_id" value='<%=sLocations1 %>'>
  			<div class="alert alert-danger text-center" role="alert" id="alertFailureStep3" style="display:none;"></div>
  				<table id="tag_table_step3" class="table table-bordered table-striped table-condensed">
					<tr class="info text-center">
						<th class="text-center"><h4>Box ID</h4></th>
						<th class="text-center"><h4>Tag ID</h4></th>
						<th class="text-center"><h4>Product</h4></th>
						<th class="text-center"><h4>Shipment Value(Lakhs INR)</h4></th>
						<th class="text-center"><h4>Criticality&nbsp;&nbsp;
						<a data-toggle="tooltip" data-placement="top" title="How critical is this shipment?">
  							<i class="fa fa-info-circle"></i>
						</a>
						</h4></th>
						<th class="text-center"><h4>Add/Delete Boxes</h4></th>
					</tr>
					<%j = 0;
					if(colBoxInfo == null | colBoxInfo.size() == 0) {%>
						<tr>
						<td class="text-center"><input class='form-control-no-height' type='text' id='0_BoxId' name="0_BoxId" value="" onchange="checkBox('0')" ></td>
						<td class="text-center"><select id="0_TagId" name="0_TagId" class="form-control-no-background pull-center" onchange="checkTag('0')" style="width: 100%">
          									<option>Select Tag</option>
          									<%for(ShipmentTags s: colTags) { %>
          									<option><%=s.getShipmentTagId() %></option>
          									<%} %>
										</select>
						</td>
						<td class="text-center">
	  						<input id="0_ProductId" name="0_ProductId" class='form-control-no-height text-uppercase' type='text'>
						</td>
						<td><input class='form-control-no-height' type='text' id='0_Value' name="0_Value" value=""></td>
						<td><select class='form-control-no-height' id="0_Criticality" name="0_Criticality" >
          		<option>LOW</option>
          		<option>MEDIUM</option>
          		<option>HIGH</option>
          	</select></td>
						<td class="text-center"><button type='button' class='add btn btn-sm btn-warning' data-toggle='tooltip' data-placement='top' title='Add Box' value='Add Row' onclick="add_box()"><i class='fa fa-plus'></i></button></td>
					</tr>
					<% j++;
					} else {
					for(ShipmentBoxInfo sbi: colBoxInfo) {%>
					<tr id="row_tag<%=j%>">
						<td class="text-center"><input class='form-control-no-height' type='text' id='<%=j%>_BoxId' name="<%=j%>_BoxId" value="<%=sbi.getBoxId()%>" onchange="checkBox('0')"></td>
						<td class="text-center">
						<div id="<%=j %>_text_div"><%=sbi.getTagId() %>&nbsp;&nbsp;<button type='button' class='btn btn-sm btn-warning' onclick="showSelectDiv(<%=j%>)">Edit</button></div>
						<div id="<%=j %>_select_div" style="display:none;"> 
						<select id="<%=j%>_TagId" name="<%=j%>_TagId" class="form-control-no-background pull-center" onchange="checkTag('0')" style="width: 100%">
          									<option>Select Tag</option>
          									<option selected><%=sbi.getTagId() %></option>
          									<%for(ShipmentTags s: colTags) {%>
          									<option><%=s.getShipmentTagId() %></option>
          									<% }%>
										</select>
										</div>
						</td>
						<td class="text-center">
	  						<input id="<%=j%>_ProductId" name="<%=j%>_ProductId" class='form-control-no-height text-uppercase' type='text' value="<%=sbi.getProductId() %>">
						</td>
						<td>
						<%if(sbi.getValue() == 0) { %>
						<input class='form-control-no-height' type='text' id='<%=j%>_Value' name="<%=j%>_Value" value="">
						<%} else { %>
						<input class='form-control-no-height' type='text' id='<%=j%>_Value' name="<%=j%>_Value" value="<%= sbi.getValue()%>">
						<%} %>
						</td>
						<td><select class='form-control-no-height' id="<%=j%>_Criticality" name="<%=j%>_Criticality" >
							<%if(sbi.getCriticality() != null && sbi.getCriticality().equalsIgnoreCase("LOW")) { %>
          					<option selected>LOW</option>
          					<option>MEDIUM</option>
          					<option>HIGH</option>
          					<%} else if(sbi.getCriticality() != null && sbi.getCriticality().equalsIgnoreCase("MEDIUM")) { %>
          					<option>LOW</option>
          					<option selected>MEDIUM</option>
          					<option>HIGH</option>
          					<%} else if(sbi.getCriticality() != null && sbi.getCriticality().equalsIgnoreCase("HIGH")) { %>
          					<option>LOW</option>
          					<option>MEDIUM</option>
          					<option selected>HIGH</option>
          					<%} else { %>
          					<option selected>LOW</option>
          					<option>MEDIUM</option>
          					<option>HIGH</option>
          					<%} %>
          	</select></td>
          				<%if(j == 0) { %>
						<td class="text-center"><button type='button' class='add btn btn-sm btn-warning' data-toggle='tooltip' data-placement='top' title='Add Box' value='Add Row' onclick="add_box()"><i class='fa fa-plus'></i></button></td>
						<%} else { %>
						<td class="text-center"><button type='button' class='add btn btn-sm btn-warning' data-toggle='tooltip' data-placement='top' title='Add Box' value='Add Row' onclick="add_box()"><i class='fa fa-plus'></i></button>&nbsp;&nbsp;<button type='button' class='add btn btn-sm btn-danger' onclick='delete_box(<%=j %>);' data-toggle='tooltip' data-placement='top' title='Remove Box' value='Remove Box'><i class='fa fa-minus'></i></button></td>
						<%} %>
					</tr>
					<%j++;} 
					}%>
				</table>
				<input type="hidden" id="lastValue_tag" name="lastValue_tag" value="<%= j %>">
				<br>
				<a href="#" onclick="showStep('2')" class="btn btn-success pull-left"><i class="fa fa-chevron-left fg-lg"></i>&nbsp;EDIT STEP 2</a>
				<a href="#" onclick="CheckStep3AndSubmit()" class="btn btn-success pull-right">ASSIGN &amp; CONTINUE&nbsp;<i class="fa fa-chevron-right fg-lg"></i></a>
			
  			</div>
  		</div>
          		</div><!-- End of Step3 -->
          		
          		<div id="step4" style="display:none;">
          		<div class="row">
  			<div class="col-md-offset-1 col-md-10 text-center">
  			<div class="alert alert-danger text-center" role="alert" id="alertFailureStep4" style="display:none;"></div>
  			<div class="alert alert-success text-center" id="alertSuccessStep4" style="display:none;"></div>
  				<input type="checkbox" name="cb_user" id="cb_user" onclick="Select_Users()">&nbsp;Select All Users
  				<div class="well white-background text-center">
  				<div class="row">
  				<%
  				for(ShipmentUsers s: colUsers) {
  					%>
  					<div class="col-md-2">
  			<input type="button" class="btn btn-default btn-sm" onclick="colorChange('user_<%=s.getShipmentUserId() %>');" id="user_<%=s.getShipmentUserId() %>" value="User <%=s.getShipmentUserId() %>: <%=s.getShipmentUserName()%>">
  			</div>
  				<%
  				} %>
  			
  		</div>
  		
  		</div>	
  		<a href="#" onclick="showStep('3')" class="btn btn-success pull-left" id="btn_goto_step3" name="btn_goto_step3"><i class="fa fa-chevron-left fg-lg"></i>&nbsp;EDIT STEP 3</a>
		<a href="#" onclick="CheckStep4AndSubmit()" class="btn btn-success pull-right" id="btn_save" name="btn_save">SAVE</a>
			<br>
		<div id="new_btns" style="display:none;" class="text-center"><a href="ShipmentCreation.jsp" class="btn btn-success pull-left">CREATE NEW SHIPMENT</a>
			<a href="ShipmentEdit.jsp" class="btn btn-success pull-right">EDIT ANOTHER SHIPMENT</a></div>
  		
  		<br><br>
  					
  				</div>
  			</div>
          		</div><!-- End of Step4 -->
          		<%} %>
          	</div>
		</div>
	</section>
	
    
    
    
				</div>
		<!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0
			</div>
			<strong>Copyright &copy; 2017 <a href="http://tagbox.in">TagBox</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Control Sidebar -->
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->
	
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="../../bootstrap/js/bootstrap.min.js"></script>
	<!-- ChartJS 1.0.1 -->
	<!-- AdminLTE App -->
	<script src="../../dist/js/app.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="../../dist/js/demo.js"></script>

	</body>
	<script>
	$('#eta').datetimepicker({
		oneLine: true,
		controlType: 'select',
		minDate: 0,
		maxDate: 30,
		dateFormat: 'dd-mm-yy'
	});
	$('#etd').datetimepicker({
		oneLine: true,
		controlType: 'select',
		minDate: 0,
		maxDate: 30,
		dateFormat: 'dd-mm-yy'
	});
	//$('#eta').datepicker({
	//     format: "yyyy-mm-dd"
	// }); 
	
	//$('#etd').datepicker({
	//     format: "yyyy-mm-dd"
	// }); 
	$('#0_eta').datetimepicker({
		oneLine: true,
		controlType: 'select',
		minDate: 0,
		maxDate: 30,
		dateFormat: 'dd-mm-yy'
	});
	
	$('#0_etd').datetimepicker({
		oneLine: true,
		controlType: 'select',
		minDate: 0,
		maxDate: 30,
		dateFormat: 'dd-mm-yy'
	});
	<%
	
	int i = 0;
	if(colTransitInfo != null) {
	for(ShipmentTransitInfo s: colTransitInfo){%>
		$('#<%=i%>_etd').datetimepicker({
			oneLine: true,
			controlType: 'select',
			minDate: 0,
			maxDate: 30,
			dateFormat: 'dd-mm-yy'
		});
		$('#<%=i%>_eta').datetimepicker({
			oneLine: true,
			controlType: 'select',
			minDate: 0,
			maxDate: 30,
			dateFormat: 'dd-mm-yy'
		});
		
	<%i++;
	}
	i = 0;
	for(ShipmentBoxInfo s: colBoxInfo){%>
	$("#<%=i%>_TagId").select2();
	<%i++;
	}
	for(int k=0;k<sUsersArray.length;k++) {
		//System.out.println(sUsersArray[k]);
		%>
		$("#user_<%=sUsersArray[k]%>").addClass("btn-warning").removeClass("btn-default");
	
	<%}
	}%>
	
	function showSelectDiv(no) {
		$("#"+no+"_select_div").show();
		$("#"+no+"_text_div").hide();
	}
	</script>
</html>
