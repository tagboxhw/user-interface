

<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>

<!-- <?xml version="1.0" encoding="ISO-8859-1"?> -->

<%!static final Logger logger = LoggerFactory.getLogger("GetUserData");
	String buf = "";
	String sString = "";
	String sErrorMessage = null;
	String sId = null;
	String sRowId = null;
	String sSubLocId = null;
%>
	<%
	try {
		sString = request.getParameter("query");
		if (sString.equals("loginAccess")) {
			sId = request.getParameter("UserID");
			String sPwd = request.getParameter("Pwd");
			buf = "";
			//response.setContentType("text/xml");
			//buf += ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			int iCount = 0;
			//logger.info("<rows total_count='100' pos='0'>");
			buf = "<rows>";
				buf += ("<row>");
				if(sId.equals("demo1")){
	        		if(sPwd.equals("tagbox_demo1")){
	        			buf += ("<Return>Success</Return>");
	        		} else {
	        			buf += ("<Return>Failure</Return>");	
	        		}
	        	} else if(sId.equals("demo2")){
	        		if(sPwd.equals("tagbox_demo2")){
	        			buf += ("<Return>Success</Return>");
	        		} else {
	        			buf += ("<Return>Failure</Return>");	
	        		}
	        	} else if(sId.equals("demo3")){
	        		if(sPwd.equals("tagbox_demo3")){
	        			buf += ("<Return>Success</Return>");
	        		} else {
	        			buf += ("<Return>Failure</Return>");	
	        		}
	        	} else {
	        		buf += ("<Return>Failure</Return>");
	        	}
				
				buf += ("</row>");
			buf += "</rows>";
			buf = buf.trim();
			//logger.info(buf);
			out.clear();
			out.println(buf);
		}
	} catch (Exception e) {
		logger.error("Exception: " + e.getLocalizedMessage());
		e.printStackTrace();
	}
%>