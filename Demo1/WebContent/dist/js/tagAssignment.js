
function edit_row(no)
{
 document.getElementById("edit_button"+no).style.display="none";
 document.getElementById("save_button"+no).style.display="block";
 //document.getElementById("cancel_button"+no).style.display="block";
 

 var ship_id = document.getElementById(no+"_Ship_Date");
 var inv_id = document.getElementById(no+"_Inv_Id");
 var criticality = document.getElementById(no+"_Criticality");
 var destination=document.getElementById(no+"_Destination");
 var source=document.getElementById(no+"_Source");	
 var min=document.getElementById(no+"_Min");	
 var max=document.getElementById(no+"_Max");
 var max=document.getElementById(no+"_Max");
 var flight_id=document.getElementById(no+"_Flight_Id");
 
 var ship_data=ship_id.innerHTML;
 var inv_data=inv_id.innerHTML;
 var criticality_data=criticality.innerHTML;
 var destination_data=destination.innerHTML;
 var source_data=source.innerHTML;
 var min_data=min.innerHTML;
 var max_data=max.innerHTML;
 var flight_id_data=flight_id.innerHTML;

 ship_id.innerHTML="<input type='text' class='form-control-no-height' id='ship_id_stationary_text"+no+"' value=''>";
 inv_id.innerHTML="<input type='text' class='form-control-no-height' id='inv_id_stationary_text"+no+"' value='"+inv_data+"'>";
 if(criticality_data.indexOf("red", 0) > 0){
	 criticality.innerHTML="<input type='text' class='form-control-no-height' id='criticality_stationary_text"+no+"' value='HIGH'>";
 } else if(criticality_data.indexOf("orange", 0) > 0){
	 criticality.innerHTML="<input type='text' class='form-control-no-height' id='criticality_stationary_text"+no+"' value='MEDIUM'>";
 } else if(criticality_data.indexOf("green", 0) > 0){
	 criticality.innerHTML="<input type='text' class='form-control-no-height' id='criticality_stationary_text"+no+"' value='LOW'>";
 } else{
	 criticality.innerHTML="<input type='text' class='form-control-no-height' id='criticality_stationary_text"+no+"' value='LOW'>";
 }
 destination.innerHTML="<input type='text' class='form-control-no-height' id='destination_stationary_text"+no+"' value='"+destination_data+"'>";
 source.innerHTML="<input type='text' class='form-control-no-height' id='source_stationary_text"+no+"' value='"+source_data+"'>";
 min.innerHTML="<input type='text' class='form-control-no-height' id='min_text"+no+"' value='"+min_data+"'>";
 max.innerHTML="<input type='text' class='form-control-no-height' id='max_text"+no+"' value='"+max_data+"'>";
 flight_id.innerHTML="<input type='text' class='form-control-no-height' id='flight_id_text"+no+"' value='"+flight_id_data+"'>";
 $('#ship_id_stationary_text'+no).datepicker({
     format: "dd/mm/yyyy"
 }); 
}

function save_row(no)
{
	var tag_id = document.getElementById(no+"_Tag_Id");
	var tag_data=tag_id.innerHTML;
	
	var ship_id=document.getElementById("ship_id_stationary_text"+no).value;
	var inv_id=document.getElementById("inv_id_stationary_text"+no).value;
	var criticality_id=document.getElementById("criticality_stationary_text"+no).value;
	var destination_id=document.getElementById("destination_stationary_text"+no).value;
	var source_id=document.getElementById("source_stationary_text"+no).value;
	var min_id=document.getElementById("min_text"+no).value;
	var max_id=document.getElementById("max_text"+no).value;
	var flight_id=document.getElementById("flight_id_text"+no).value;
	
	document.getElementById(no+"_Ship_Date").innerHTML=ship_id;
	document.getElementById(no+"_Inv_Id").innerHTML=inv_id;
	if(criticality_id.toLowerCase() == "high") {
		document.getElementById(no+"_Criticality").innerHTML="<span class='badge bg-red'>HIGH</span>";
	} else if(criticality_id.toLowerCase() == "medium") {
		document.getElementById(no+"_Criticality").innerHTML="<span class='badge bg-orange'>MEDIUM</span>";
	} else if(criticality_id.toLowerCase() == "low") {
		document.getElementById(no+"_Criticality").innerHTML="<span class='badge bg-green'>LOW</span>";
	} else {
		document.getElementById(no+"_Criticality").innerHTML="<span class='badge bg-green'>LOW</span>";
	}
	document.getElementById(no+"_Destination").innerHTML=destination_id;
	document.getElementById(no+"_Source").innerHTML=source_id;
	document.getElementById(no+"_Min").innerHTML=min_id;
	document.getElementById(no+"_Max").innerHTML=max_id;
	document.getElementById(no+"_Flight_Id").innerHTML=flight_id;
	document.getElementById(no+"_Trip_Status").innerHTML="<div class='progress'><div class='progress-bar' role='progressbar' aria-valuenow='0' aria-valuemin='0' aria-valuemax='100' style='width: 0%;'>0%</div></div>";
	document.getElementById(no+"_Location").innerHTML=source_id;
	document.getElementById("edit_button"+no).style.display="none";
	document.getElementById("save_button"+no).style.display="none";
	
	
	var posting = $.post("../../pages/jsp/GetTagAssignmentData.jsp",
			{
				Tag_Id:tag_data,
				Ship_Date:ship_id,
				Ship_Id:inv_id,
				Source_Id:source_id,
				Dest_Id:destination_id,
				flight_id:flight_id,
				Criticality_id:criticality_id,
				Min_Id:min_id,
				Max_Id:max_id,
				query:"updateTagDetails"
			},
			function(data,status){
				//alert(data);
				xmlDoc = loadXMLString(data);
				if($(xmlDoc).find("rows").length == 0) {
					return;
				}
				
				$(xmlDoc).find("row").each(function(){
					var id = $(this).find("Done").text();
					document.getElementById(no+"_Ship_Date").innerHTML = id;
				});
				
			});

	
	//Unblock this part for disabling the edit buttons
	//document.getElementById(no+"_Trip_Status").innerHTML="<div class='progress'><div class='progress-bar' role='progressbar' aria-valuenow='0' aria-valuemin='0' aria-valuemax='100' style='width: 0%;'>0%</div></div>";
	//document.getElementById("edit_button"+no).style.display="none";
 }

function loadXMLString(txt) 
{
	if (window.DOMParser)
	{
		parser=new DOMParser();
		xmlDoc=parser.parseFromString(txt,"text/xml");
	}
	else // Internet Explorer
	{
		xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
		xmlDoc.async=false;
		xmlDoc.loadXML(txt); 
	}
	return xmlDoc;
}