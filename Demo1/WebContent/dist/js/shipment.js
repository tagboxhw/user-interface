function CheckStep1AndSubmit(){
	$("#alertFailure").hide();
	if($("#Source").val() == "") {
		$("#alertFailure").html("Please select a Source").show();
		$('.alert').fadeIn(500);
        setTimeout( "$('.alert').fadeOut(1500);",3000 );
		return;
	}
	if($("#Destination").val() == "") {
		$("#alertFailure").html("Please select a Destination").show();
		$('.alert').fadeIn(500);
        setTimeout( "$('.alert').fadeOut(1500);",3000 );
		return;
	}
	var a = $("#etd").val().split(" ");
	var b = a[0].split("-");
	var c = a[1].split(":");
	var d = new Date(b[2], (b[1]-1), b[0], c[0], c[1]);
	a = $("#eta").val().split(" ");
	b = a[0].split("-");
	c = a[1].split(":");
	e = new Date(b[2], (b[1]-1), b[0], c[0], c[1]);
	if($("#etd").val() == "" || $("#etd").val() == "undefined") {
		$("#alertFailure").html("Please select Departure Date").show();
		$('.alert').fadeIn(500);
        setTimeout( "$('.alert').fadeOut(1500);",3000 );
		return;
	}
	if($("#eta").val() == "" || $("#eta").val() == "undefined") {
		$("#alertFailure").html("Please select Arrival Date").show();
		$('.alert').fadeIn(500);
        setTimeout( "$('.alert').fadeOut(1500);",3000 );
		return;
	}
	//if($("#Source").val() == $("#Destination").val()) {
	//	$("#alertFailure").html("Source and Destination cannot be the same!").show();
	//	$('.alert').fadeIn(500);
    //   setTimeout( "$('.alert').fadeOut(1500);",3000 );
	//	return;
	//}
	var mydate1 = new Date($("#etd").val());
	var mydate2 = new Date($("#eta").val());
	if(d > e) {
		$("#alertFailure").html("Arrival date cannot be before Departure date!").show();
		$('.alert').fadeIn(500);
        setTimeout( "$('.alert').fadeOut(1500);",3000 );
		return;
	}
	if(!isAlphaNumeric($("#Source").val())) {
		$("#alertFailure").html("Source has to be alphanumeric only!").show();
		$('.alert').fadeIn(500);
        setTimeout( "$('.alert').fadeOut(1500);",3000 );
		return;
	}
	if(!isAlphaNumeric($("#Destination").val())) {
		$("#alertFailure").html("Destination has to be alphanumeric only!").show();
		$('.alert').fadeIn(500);
        setTimeout( "$('.alert').fadeOut(1500);",3000 );
		return;
	}
	if(!isAlphaNumeric($("#invoice_number").val())) {
		$("#alertFailure").html("Invoice Number has to be alphanumeric only!").show();
		$('.alert').fadeIn(500);
        setTimeout( "$('.alert').fadeOut(1500);",3000 );
		return;
	}
	$("#0_Source").val($("#Source").val());
    $("#0_Destination").val($("#Destination").val());
	$("#0_eta").val($("#eta").val());
	//$("#0_eta").datetimepicker('update', $("#eta").val());  
	$("#0_etd").val($("#etd").val());
	//$("#0_etd").datetimepicker('update', $("#etd").val());
	$("#step1").hide();
	$("#step2").show();
	$("#link1").addClass("previous_step").removeClass("unused_step").removeClass("current_step");
	$("#link2").addClass("current_step").removeClass("unused_step").removeClass("previous_step");
	add_hops();
}

function CheckStep2AndSubmit(){
	$("#alertFailureStep2").hide();
	var no = document.getElementById("lastValue_leg").value;
	for(var i = 0; i<no; i++) {
		if(document.getElementById("row_leg"+i) != null) {
			if($("#"+i+"_Source").val() == "") {
				$("#alertFailureStep2").html("Please select Source names for all legs!").show();
				$('.alert').fadeIn(500);
		        setTimeout( "$('.alert').fadeOut(1500);",3000 );
				return;
			}
			if($("#"+i+"_etd").val() == "" || $("#"+i+"_etd").val() == "undefined") {
				$("#alertFailureStep2").html("Please select departure dates for all legs!").show();
				$('.alert').fadeIn(500);
		        setTimeout( "$('.alert').fadeOut(1500);",3000 );
				return;
			}
			if($("#"+i+"_eta").val() == "" || $("#"+i+"_eta").val() == "undefined") {
				$("#alertFailureStep2").html("Please select arrival dates for all legs!").show();
				$('.alert').fadeIn(500);
		        setTimeout( "$('.alert').fadeOut(1500);",3000 );
				return;
			}
			if($("#"+i+"_carrier").val() == "" || $("#"+i+"_carrier").val() == "undefined") {
				$("#alertFailureStep2").html("Please enter Carrier Id for all legs! Carrier Id could be Flight#, Vehicle Registration, Container Id, etc").show();
				$('.alert').fadeIn(500);
		        setTimeout( "$('.alert').fadeOut(1500);",7000 );
				return;
			}
			if(!isAlphaNumeric($("#"+i+"_Source").val())) {
				$("#alertFailureStep2").html("Source has to be alphanumeric only!").show();
				$('.alert').fadeIn(500);
		        setTimeout( "$('.alert').fadeOut(1500);",3000 );
				return;
			}
			if(!isAlphaNumeric($("#"+i+"_carrier").val())) {
				$("#alertFailureStep2").html("Carrier Id has to be alphanumeric only!").show();
				$('.alert').fadeIn(500);
		        setTimeout( "$('.alert').fadeOut(1500);",3000 );
				return;
			}
		}
	}
	var bDest = false;
	var bFound = false;
	for(var i = no; i>0; i--) {
		var b = i+"_eta";
		if(document.getElementById(b) != null) {
			var a = $("#"+i+"_eta").val().split(" ");
			var b = a[0].split("-");
			var c = a[1].split(":");
			var d = new Date(b[2], (b[1]-1), b[0], c[0], c[1]);
			a = $("#eta").val().split(" ");
			b = a[0].split("-");
			c = a[1].split(":");
			e = new Date(b[2], (b[1]-1), b[0], c[0], c[1]);
			if(d > e) {
				$("#alertFailureStep2").html("Arrival date of a leg cannot be after Arrival date of last leg!").show();
				var a = "0_eta";
				b = i+"_eta";
				$('#'+a).addClass("errorborder");
				$('#'+b).addClass("errorborder");
				$('.alert').fadeIn(500);
				setTimeout(function(){doTimeout(a, b);} ,5000 );
		        bDest = true;
			}
			bFound = true;
		}
		if(bFound || bDest) break;
	}
	if(bDest) return;
	for(var i = 0; i<no; i++) {
		var b = i+"_etd";
		if(document.getElementById(b) != null) {
			var a = $("#"+i+"_etd").val().split(" ");
			var b = a[0].split("-");
			var c = a[1].split(":");
			var d = new Date(b[2], (b[1]-1), b[0], c[0], c[1]);
			a = $("#"+i+"_eta").val().split(" ");
			b = a[0].split("-");
			c = a[1].split(":");
			e = new Date(b[2], (b[1]-1), b[0], c[0], c[1]);
			if(d > e) {
				$("#alertFailureStep2").html("Arrival date cannot be before Departure date!").show();
				var a = i+"_eta";
				b = i+"_etd";
				$('#'+a).addClass("errorborder");
				$('#'+b).addClass("errorborder");
				$('.alert').fadeIn(500);
				setTimeout(function(){doTimeout(a, b);} ,5000 );
				return;
			}
		}
	}
	var j = 1;
	var b = "1_etd";
	if(document.getElementById(b) != null) {
		var a = $("#1_etd").val().split(" ");
		var b = a[0].split("-");
		var c = a[1].split(":");
		var d = new Date(b[2], (b[1]-1), b[0], c[0], c[1]);
		a = $("#0_etd").val().split(" ");
		b = a[0].split("-");
		c = a[1].split(":");
		e = new Date(b[2], (b[1]-1), b[0], c[0], c[1]);
		if(d < e) {
			var a = "0_etd";
			var b = "1_etd";
			$("#alertFailureStep2").html("Departure date of a leg cannot be before Departure date of first leg!").show();
			$('#'+a).addClass("errorborder");
			$('#'+b).addClass("errorborder");
			$('.alert').fadeIn(500);
			setTimeout(function(){doTimeout(a, b);} ,5000 );
			return;
		}
		var a = $("#1_eta").val().split(" ");
		var b = a[0].split("-");
		var c = a[1].split(":");
		var d = new Date(b[2], (b[1]-1), b[0], c[0], c[1]);
		a = $("#0_eta").val().split(" ");
		b = a[0].split("-");
		c = a[1].split(":");
		e = new Date(b[2], (b[1]-1), b[0], c[0], c[1]);
		if(d > e) {
			var a = "0_eta";
			var b = "1_eta";
			$("#alertFailureStep2").html("Arrival date of a leg cannot be after Arrival date of last leg!").show();
			$('#'+a).addClass("errorborder");
			$('#'+b).addClass("errorborder");
			$('.alert').fadeIn(500);
			setTimeout(function(){doTimeout(a, b);} ,5000 );
			return;
		}
	}
	j=1;
	for(var i = 2; i<no; i++) {
		var b = i+"_etd";
		if(document.getElementById(b) != null) {
			var a = $("#"+i+"_etd").val().split(" ");
			var b = a[0].split("-");
			var c = a[1].split(":");
			var d = new Date(b[2], (b[1]-1), b[0], c[0], c[1]);
			var prev_date = j+"_eta";
			if(document.getElementById(prev_date) != null){
				a = $("#"+j+"_eta").val().split(" ");
				b = a[0].split("-");
				c = a[1].split(":");
				e = new Date(b[2], (b[1]-1), b[0], c[0], c[1]);
				if(d < e) {
					var a = j+"_eta";
					b = i+"_etd";
					$("#alertFailureStep2").html("Departure date of a leg cannot be before Arrival date of previous leg!").show();
					$('#'+a).addClass("errorborder");
					$('#'+b).addClass("errorborder");
					$('.alert').fadeIn(500);
					setTimeout(function(){doTimeout(a, b);} ,5000 );
					return;
				}
			}
			j = i;
		}
	}
	for(var i=0;i<no;i++) {
		var b = i+"_Source";
		var b_Value = $("#"+b).val();
		for(var no1=0;no1<no;no1++) {
			if(no1 == i) continue;
			var a = no1+"_Source";
			if(document.getElementById(a) != null && ($('#'+a).val() != "Select Select") && ($('#'+a).val() == b_Value)) {
				$("#alertFailureStep2").html("Two legs cannot have the same location!").show();
				$('.alert').fadeIn(500);
		        setTimeout( "$('.alert').fadeOut(1500);",3000 );
				return;
			}
		}
	}
	$("#step2").hide();
	$("#step3").show();
	$("#link2").addClass("previous_step").removeClass("unused_step").removeClass("current_step");
	$("#link3").addClass("current_step").removeClass("unused_step").removeClass("previous_step");
}

function CheckStep3AndSubmit(){
	var no1 = document.getElementById("lastValue_tag").value;
	for(var i=0;i<no1;i++) {
		var b = i+"_BoxId";
		if(document.getElementById(b) != null) {
			if($('#'+i+'_BoxId').val() == "") {
				$("#alertFailureStep3").html("Box ID cannot be null!").show();
				$('.alert').fadeIn(500);
		        setTimeout( "$('.alert').fadeOut(1500);",3000 );
				return;
			}
			if($('#'+i+'_TagId').val() == "Select Tag") {
				$("#alertFailureStep3").html("Please select a Tag Id!").show();
				$('.alert').fadeIn(500);
		        setTimeout( "$('.alert').fadeOut(1500);",3000 );
				return;
			}
			if($('#'+i+'_ProductId').val() == "") {
				$("#alertFailureStep3").html("Please select a Product Id!").show();
				$('.alert').fadeIn(500);
		        setTimeout( "$('.alert').fadeOut(1500);",3000 );
				return;
			}
			if($('#'+i+'_Value').val() == "") {
				$("#alertFailureStep3").html("Please enter a Shipment Value! For example: 45, 34.00, 123.12, etc").show();
				$('.alert').fadeIn(500);
		        setTimeout( "$('.alert').fadeOut(1500);",3000 );
				return;
			}
			if($('#'+i+'_Value').val() != "") {
				//var reg = /^\d+$/;
				var reg = /^\d+(\.\d+)?$/;
				var result = ($('#'+i+'_Value').val()).search(reg);
				if(result == -1) {
					$("#alertFailureStep3").html("Value field can have only numbers. For example: 45, 34.00, 123.12, etc").show();
					$('.alert').fadeIn(500);
			        setTimeout( "$('.alert').fadeOut(1500);",3000 );
					return;
				}
			}
			if(!isAlphaNumeric($('#'+i+'_BoxId').val())) {
				$("#alertFailureStep3").html("Box ID has to be alphanumeric only!").show();
				$('.alert').fadeIn(500);
		        setTimeout( "$('.alert').fadeOut(1500);",3000 );
				return;
			}
			if(!isAlphaNumeric($('#'+i+'_ProductId').val())) {
				$("#alertFailureStep3").html("Product has to be alphanumeric only!").show();
				$('.alert').fadeIn(500);
		        setTimeout( "$('.alert').fadeOut(1500);",3000 );
				return;
			}
		}
	}
	for(var i=0;i<no1;i++) {
		var b = i+"_TagId";
		var b_Value = $("#"+b).val();
		for(var no=0;no<no1;no++) {
			if(no == i) continue;
			var a = no+"_TagId";
			if(document.getElementById(a) != null && ($('#'+a).val() != "Select Tag") && ($('#'+a).val() == b_Value)) {
				$("#alertFailureStep3").html("Two boxes cannot have the same Tag Id!").show();
				$('#'+a).addClass("errorborder");
				$('#'+b).addClass("errorborder");
				$('.alert').fadeIn(500);
				setTimeout(function(){doTimeout(a, b);} ,3000 );
				return;
			}
			a = no+"_BoxId";
			b = i+"_BoxId";
			b_Value = $("#"+b).val();
			if(document.getElementById(a) != null && ($('#'+a).val() != "") && ($('#'+a).val() == b_Value)) {
				$("#alertFailureStep3").html("Two boxes cannot have the same Box Id!").show();
				$('#'+a).addClass("errorborder");
				$('#'+b).addClass("errorborder");
				$('.alert').fadeIn(500);
		        setTimeout(function(){doTimeout(a, b);} ,3000 );
				return;
			}
		}
	}
	
	$("#step3").hide();
	$("#step4").show();
	$("#link3").addClass("previous_step").removeClass("unused_step").removeClass("current_step");
	$("#link4").addClass("current_step").removeClass("unused_step").removeClass("previous_step");
}

function CheckStep4AndSubmit(){
	var btn_class=false;
	var btn_string = "";
	$('input:button').each(function() {
		var a = ($(this).attr('id'));
		if(a.startsWith("user")) {
			if($('#'+a).hasClass("btn-warning")) {
				btn_class=true;
				btn_string = btn_string+","+a.substring(5);
			}
		}
	});
	if(!btn_class) {
		$("#alertFailureStep4").html("Please assign atleast one user").show();
		$('.alert-danger').fadeIn(500);
        setTimeout( "$('.alert-danger').fadeOut(1500);",3000 );
		return;
	}
	$("#exit_value").val("");
	//Get New Shipment Id
	var posting = $.post("../../pages/jsp/GetShipmentData.jsp",
			{
				query:"getNewShipment"
			},
			function(data,status){
				//alert(data);
				xmlDoc = loadXMLString(data);
				if($(xmlDoc).find("rows").length == 0) {
					return;
				}
				
				$(xmlDoc).find("row").each(function(){
					var id = $(this).find("Done").text();
					if(id.startsWith("ERROR")) {
						$("#alertFailureStep4").html("There was an error generating a new Shipment Id! Please try to save again. " + id).show();
						$('#link1').addClass("disabled");
						$('#link2').addClass("disabled");
						$('#link3').addClass("disabled");
						$('#link4').addClass("disabled");
						$('#li_link1').addClass("disabled");
						$('#li_link2').addClass("disabled");
						$('#li_link3').addClass("disabled");
						$('#li_link4').addClass("disabled");
						$('#btn_goto_step3').hide();
						$('#btn_save').show();
						$("#new_btns").hide();
						$("#exit_value").val("yes");
						$("#shipment_status").val("Error");
						return;
					} else {
						$("#shipment_id").val(id);
					}
				});
				
			});
	//Save Step1:
	posting.done(function(){
		if($("#shipment_status").val() == "Error") return;
	var posting1 = $.post("../../pages/jsp/GetShipmentData.jsp",
			{
				Shipment_Id:$("#shipment_id").val(),
				Source_Name:$("#Source").val().toLocaleUpperCase(),
				Destination_Name:$("#Destination").val().toLocaleUpperCase(),
				ETD_Date:$("#etd").val(),
				ETA_Date:$("#eta").val(),
				Invoice_number:$("#invoice_number").val(),
				query:"step1Insert"
			},
			function(data,status){
				//alert(data);
				xmlDoc = loadXMLString(data);
				if($(xmlDoc).find("rows").length == 0) {
					return;
				}
				
				$(xmlDoc).find("row").each(function(){
					var id = $(this).find("Done").text();
					if(id.startsWith("ERROR")) {
						$("#alertFailureStep4").html("There was an error saving the Shipment! Please try to save again. " + id).show();
						$('#link1').addClass("disabled");
						$('#link2').addClass("disabled");
						$('#link3').addClass("disabled");
						$('#link4').addClass("disabled");
						$('#li_link1').addClass("disabled");
						$('#li_link2').addClass("disabled");
						$('#li_link3').addClass("disabled");
						$('#li_link4').addClass("disabled");
						$('#btn_goto_step3').hide();
						$('#btn_save').show();
						$("#new_btns").hide();
						$("#exit_value").val("yes");
						$("#shipment_status").val("Error");
						return;
					}
				});
				
			});
	posting1.done(function(){
		if($("#shipment_status").val() == "Error") return;
		//Save Step2:
		var no = document.getElementById("lastValue_leg").value;
		var sourceString = "";
		var destString = "";
		var etdString = "";
		var etaString = "";
		var modeString = "";
		var carrierString = "";
		for(var i=0;i<no;i++) {
			var b = i+"_etd";
			if(document.getElementById(b) != null) {
				sourceString = sourceString + "#" + $('#'+i+'_Source').val().toLocaleUpperCase();
				var c = i+'_Destination';
				if(document.getElementById(c) != null) {
					destString = destString + "#" + $('#'+i+'_Destination').val().toLocaleUpperCase();
				} else {
					destString = destString + "#" + $('#'+i+'_Destination').val();
				}
				etdString = etdString + "#" + $('#'+i+'_etd').val();
				etaString = etaString + "#" + $('#'+i+'_eta').val();
				carrierString = carrierString + "#" + $('#'+i+'_carrier').val();
				modeString = modeString + "#" + $('#'+i+'_Mode').val();
			}
		}
		var posting2 = $.post("../../pages/jsp/GetShipmentData.jsp",
				{
					Shipment_Id:$("#shipment_id").val(),
					Source_Id:sourceString,
					Dest_Id:destString,
					Etd_Id:etdString,
					Eta_String:etaString,
					Mode_String:modeString,
					Carrier_String:carrierString,
					query:"step2Insert"
				},
				function(data,status){
					//alert(data);
					xmlDoc = loadXMLString(data);
					if($(xmlDoc).find("rows").length == 0) {
						return;
					}
					
					$(xmlDoc).find("row").each(function(){
						var id = $(this).find("Done").text();
						if(id.startsWith("ERROR")) {
							$("#alertFailureStep4").html("There was an error adding legs to this Shipment! Please <a style='color:#a94442' href='ShipmentEdit.jsp?shipment_id="+$('#shipment_id').val() + "'>edit this Shipment</a>. " + id).show();
							$('#link1').addClass("disabled");
							$('#link2').addClass("disabled");
							$('#link3').addClass("disabled");
							$('#link4').addClass("disabled");
							$('#li_link1').addClass("disabled");
							$('#li_link2').addClass("disabled");
							$('#li_link3').addClass("disabled");
							$('#li_link4').addClass("disabled");
							$('#btn_goto_step3').hide();
							$('#btn_save').hide();
							$("#new_btns").hide();
							$("#exit_value").val("yes");
							$("#shipment_status").val("Error");
							return;
						}
					});
					
				});
		posting2.done(function(){
			//Save Step3:
			var no1 = document.getElementById("lastValue_tag").value;
			var boxString = "";
			var tagString = "";
			var productString = "";
			var valueString = "";
			var criticalityString = "";
			for(var i=0;i<no1;i++) {
				var b = i+"_BoxId";
				if(document.getElementById(b) != null) {
					if($('#'+i+'_Value').val() == "") {
						valueString = valueString + "#0";	
					} else {
						valueString = valueString + "#" + $('#'+i+'_Value').val();
					}
					boxString = boxString + "#" + $('#'+i+'_BoxId').val();
					tagString = tagString + "#" + $('#'+i+'_TagId').val();
					productString = productString + "#" + $('#'+i+'_ProductId').val().toLocaleUpperCase();
					criticalityString = criticalityString + "#" + $('#'+i+'_Criticality').val();
				}
			}
			var posting3 = $.post("../../pages/jsp/GetShipmentData.jsp",
					{
						Shipment_Id:$("#shipment_id").val(),
						Box_Id:boxString,
						Tag_Id:tagString,
						Product_Id:productString,
						Value_String:valueString,
						Criticality_String:criticalityString,
						query:"step3Insert"
					},
					function(data,status){
						//alert(data);
						xmlDoc = loadXMLString(data);
						if($(xmlDoc).find("rows").length == 0) {
							return;
						}
						
						$(xmlDoc).find("row").each(function(){
							var id = $(this).find("Done").text();
							if(id.startsWith("ERROR")) {
								$("#alertFailureStep4").html("There was an error assigning tags to this Shipment! Please <a style='color:#a94442' href='ShipmentEdit.jsp?shipment_id="+$('#shipment_id').val() + "'>edit this Shipment</a>. " + id).show();
								$('#link1').addClass("disabled");
								$('#link2').addClass("disabled");
								$('#link3').addClass("disabled");
								$('#link4').addClass("disabled");
								$('#li_link1').addClass("disabled");
								$('#li_link2').addClass("disabled");
								$('#li_link3').addClass("disabled");
								$('#li_link4').addClass("disabled");
								$('#btn_goto_step3').hide();
								$('#btn_save').hide();
								$("#new_btns").hide();
								$("#exit_value").val("yes");
								$("#shipment_status").val("Error");
								return;
							}
						});
						
					});
			posting3.done(function(){
				var posting4 = $.post("../../pages/jsp/GetShipmentData.jsp",
						{
							Shipment_Id:$("#shipment_id").val(),
							User_Id:btn_string,
							query:"step4Insert"
						},
						function(data,status){
							//alert(data);
							xmlDoc = loadXMLString(data);
							if($(xmlDoc).find("rows").length == 0) {
								return;
							}
							
							$(xmlDoc).find("row").each(function(){
								var id = $(this).find("Done").text();
								if(id.startsWith("ERROR")) {
									$("#alertFailureStep4").html("There was an error assigning users to this Shipment! Please <a style='color:#a94442' href='ShipmentEdit.jsp?shipment_id="+$('#shipment_id').val() + "'>edit this Shipment</a>. " + id).show();
									$('#link1').addClass("disabled");
									$('#link2').addClass("disabled");
									$('#link3').addClass("disabled");
									$('#link4').addClass("disabled");
									$('#li_link1').addClass("disabled");
									$('#li_link2').addClass("disabled");
									$('#li_link3').addClass("disabled");
									$('#li_link4').addClass("disabled");
									$('#btn_goto_step3').hide();
									$('#btn_save').hide();
									$("#new_btns").hide();
									$("#exit_value").val("yes");
									$("#shipment_status").val("Error");
									return;
								} else {
									if($("#shipment_status").val() != "Error") {
										$("#alertSuccessStep4").html("The shipment has been created successfully! New Shipment Id is: <h4>" + $("#shipment_id").val()+"</h4>").show();
										$('#link1').addClass("disabled");
										$('#link2').addClass("disabled");
										$('#link3').addClass("disabled");
										$('#link4').addClass("disabled");
										$('#li_link1').addClass("disabled");
										$('#li_link2').addClass("disabled");
										$('#li_link3').addClass("disabled");
										$('#li_link4').addClass("disabled");
										$('#btn_goto_step3').hide();
										$('#btn_save').hide();
										$("#new_btns").show();
										$("#exit_value").val("yes");
									}
								}
							});
							
						});
			});
		});
	});
	});
}

function showStep(n) {
	//if($('#li_link'+n).hasClass("disabled")) return;
	if(n == 1){
		$("#step1").show();
		$("#step2").hide();
		$("#step3").hide();
		$("#step4").hide();
		$("#link1").addClass("current_step").removeClass("unused_step").removeClass("previous_step");
		$("#link2").addClass("unused_step").removeClass("current_step").removeClass("previous_step");
		$("#link3").addClass("unused_step").removeClass("current_step").removeClass("previous_step");
		$("#link4").addClass("unused_step").removeClass("current_step").removeClass("previous_step");
		return;
	} else if(n == 2){
		if($("#link2").hasClass("unused_step")) {return;}
		$("#step1").hide();
		$("#step2").show();
		$("#step3").hide();
		$("#step4").hide();
		$("#link1").addClass("previous_step").removeClass("unused_step").removeClass("current_step");
		$("#link2").removeClass("unused_step").removeClass("previous_step").addClass("current_step");
		$("#link3").addClass("unused_step").removeClass("current_step").removeClass("previous_step");
		$("#link4").addClass("unused_step").removeClass("current_step").removeClass("previous_step");
		return;
	} else if(n == 3){
		if($("#link3").hasClass("unused_step")) {return;}
		$("#step1").hide();
		$("#step2").hide();
		$("#step3").show();
		$("#step4").hide();
		$("#link1").addClass("previous_step").removeClass("unused_step").removeClass("current_step");
		$("#link2").addClass("previous_step").removeClass("unused_step").removeClass("current_step");
		$("#link3").addClass("current_step").removeClass("unused_step").removeClass("previous_step");
		$("#link4").addClass("unused_step").removeClass("current_step").removeClass("previous_step");
		return;
	} else if(n == 4){
		if($("#link4").hasClass("unused_step")) {return;}
		$("#step1").hide();
		$("#step2").hide();
		$("#step3").hide();
		$("#step4").show();
		$("#link1").addClass("previous_step").removeClass("unused_step").removeClass("current_step");
		$("#link2").addClass("previous_step").removeClass("unused_step").removeClass("current_step");
		$("#link3").addClass("previous_step").removeClass("unused_step").removeClass("current_step");
		$("#link4").addClass("current_step").removeClass("unused_step").removeClass("previous_step");
		return;
	}
}

function colorChange(e) {

	if($('#'+e).hasClass('btn-default')){
		$('#' + e).removeClass('btn-default'); 
		$('#'+e).addClass('btn-warning');
	  } else {
		$('#' + e).removeClass('btn-warning'); 
		$('#'+e).addClass('btn-default');
		document.getElementById('cb_user').checked = false;
	  }
	
}

function checkTag(no) {
	$("#alertFailureStep3").hide();
	var no1 = document.getElementById("lastValue_tag").value;
	var b = no + "_TagId";
	var b_Value = $('#'+b).val();
	for(var i=0;i<no1;i++) {
		if(no == i) continue;
		var a = i+"_TagId";
		if(document.getElementById(a) != null && ($('#'+a).val() != "Select Tag") && ($('#'+a).val() == b_Value)) {
			$("#alertFailureStep3").html("Two boxes cannot have the same Tag Id!").show();
			$('#'+a).addClass("errorborder");
			$('#'+b).addClass("errorborder");
			$('.alert').fadeIn(500);
	        setTimeout(function(){doTimeout(a, b);} ,3000 );
			return;
		}
	}
}

function checkBox(no) {
	$("#alertFailureStep3").hide();
	var no1 = document.getElementById("lastValue_tag").value;
	var b = no + "_BoxId";
	var b_Value = $('#'+b).val();
	for(var i=0;i<no1;i++) {
		if(no == i) continue;
		var a = i+"_BoxId";
		if(document.getElementById(a) != null && ($('#'+a).val() != "") && ($('#'+a).val() == b_Value)) {
			$("#alertFailureStep3").html("Two boxes cannot have the same Box Id!").show();
			$('#'+a).addClass("errorborder");
			$('#'+b).addClass("errorborder");
			$('.alert').fadeIn(500);
	        setTimeout(function(){doTimeout(a, b);} ,3000 );
			return;
		}
	}
}

function doTimeout(a, b){
	$('.alert').fadeOut(1500);
	$('#'+a).removeClass("errorborder");
	$('#'+b).removeClass("errorborder");
}

function Select_Users(){
	    	if($("#cb_user").is(':checked')) {
	    		$('input:button').each(function() {
	    			var a = ($(this).attr('id'));
	    			if(a.startsWith("user")) {
	    				$('[id^=user]').addClass('btn-warning').removeClass('btn-default');
	    			}
	    		});
	    	} else {
	    		$('input:button').each(function() {
	    			var a = ($(this).attr('id'));
	    			if(a.startsWith("user")) {
	    				$('[id^=user]').addClass('btn-default').removeClass('btn-warning');
	    			}
	    		});
	    	}
}

function loadXMLString(txt) 
{
	if (window.DOMParser)
	{
		parser=new DOMParser();
		xmlDoc=parser.parseFromString(txt,"text/xml");
	}
	else // Internet Explorer
	{
		xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
		xmlDoc.async=false;
		xmlDoc.loadXML(txt); 
	}
	return xmlDoc;
}