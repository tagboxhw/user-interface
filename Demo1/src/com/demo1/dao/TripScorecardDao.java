package com.demo1.dao;

import java.util.Collection;
import java.util.Iterator;
import java.util.TreeMap;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.demo1.bean.ETagSummaryBean;
import com.demo1.bean.TripScorecardBean;
import com.demo1.util.Constants;

public class TripScorecardDao {
	private Client client; 
	static final Logger logger = LoggerFactory.getLogger(TripScorecardDao.class);
	
	private void init(){ 
	      this.client = ClientBuilder.newClient(); 
	   }  
	
	/*
	 * SP used: shp.trip_scsp()
	 */
	public Collection<TripScorecardBean> selectAllTripScorecardRecords() {
		init();
		GenericType<Collection<TripScorecardBean>> tvmBean1 = new GenericType<Collection<TripScorecardBean>>(){};
		Collection<TripScorecardBean> tvmBean = client 
		         .target(Constants.REST_SERVICE_URL + "TripScorecardService/tripscorecard") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(tvmBean1); 
		return tvmBean;
	}
	
	/*
	 * SP used: shp.last_sync(?)
	 */
	public TreeMap<String, String> selectLastSyncTimeForAllSubZones(String sShipmentId) {
		init();
		GenericType<Collection<TripScorecardBean>> clmBean1 = new GenericType<Collection<TripScorecardBean>>(){};
		WebTarget target = client.target(Constants.REST_SERVICE_URL + "TripScorecardService/lastsynctime").path(sShipmentId);
		Collection<TripScorecardBean> col  = target.request().get(clmBean1);
		TreeMap<String, String> tm = new TreeMap<String, String>();
		Iterator iter = col.iterator();
		TripScorecardBean clmBean = new TripScorecardBean();
		while(iter.hasNext()){
			clmBean = (TripScorecardBean) iter.next();
			//System.out.println(clmBean.getTag_Id() + "," + clmBean.getLastSyncTime());
			tm.put(clmBean.getTag_Id(), clmBean.getLastSyncTime());
		}
		return tm;
	}
	
}
