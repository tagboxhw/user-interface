package com.demo1.dao;

import java.util.Collection;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.demo1.bean.ETagSummaryBean;
import com.demo1.bean.ShipmentDetailsBean;
import com.demo1.util.Constants;

public class TripSummaryDao {
	private Client client; 
	static final Logger logger = LoggerFactory.getLogger(TripSummaryDao.class);
	
	private void init(){ 
	      this.client = ClientBuilder.newClient(); 
	   }  
	
	/**
	 * This method calls SP: shp.etag_sum_1().
	 * 
	 */
	public Collection<ShipmentDetailsBean> selectAllShipmentDetails() {
		init();
		GenericType<Collection<ShipmentDetailsBean>> tvmBean1 = new GenericType<Collection<ShipmentDetailsBean>>(){};
		Collection<ShipmentDetailsBean> tvmBean = client 
		         .target(Constants.REST_SERVICE_URL + "TripSummaryService/tripsummary") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(tvmBean1); 
		return tvmBean;
	}
	
	/*
	 *  This method calls SP: shp.etag_sum_2(shipment_id)
	 */
	public Collection<ETagSummaryBean> selectETagSummary(String sShipmentId){ 
		init();
		GenericType<Collection<ETagSummaryBean>> tvmBean1 = new GenericType<Collection<ETagSummaryBean>>(){};
		WebTarget target = client.target(Constants.REST_SERVICE_URL + "TripSummaryService/etagsummary").path(sShipmentId);
		Collection<ETagSummaryBean> tvmBean  = target.request().get(tvmBean1);
		return tvmBean; 
	   } 
	
}
