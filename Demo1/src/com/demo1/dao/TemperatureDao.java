package com.demo1.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.Vector;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.demo1.bean.TemperatureBean;
import com.demo1.util.Constants;
import com.demo1.util.DbConnection;
import com.demo1.util.SqlConstants;

public class TemperatureDao {
	private Client client; 
	static final Logger logger = LoggerFactory
			.getLogger(TemperatureDao.class);
	
	private void init(){ 
	      this.client = ClientBuilder.newClient(); 
	   }  
	
	/*
	 * SP used: tel.temp()
	 */
	public Collection<TemperatureBean> getTemperatureData() {
		init();
		GenericType<Collection<TemperatureBean>> tvmBean1 = new GenericType<Collection<TemperatureBean>>(){};
		Collection<TemperatureBean> tvmBean = client 
		         .target(Constants.REST_SERVICE_URL + "TemperatureService/temperature") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(tvmBean1); 
		return tvmBean;
	}
	
	/*
	 * SP used: tel.temp()
	 */
	public Collection<TemperatureBean> getTemperatureData(String sNodeId) {
		init();
		Collection<TemperatureBean> col1 = new Vector();
		GenericType<Collection<TemperatureBean>> tvmBean1 = new GenericType<Collection<TemperatureBean>>(){};
		Collection<TemperatureBean> tvmBean = client 
		         .target(Constants.REST_SERVICE_URL + "TemperatureService/temperature") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(tvmBean1); 
		for(TemperatureBean tb: tvmBean) {
			if(tb.getNode_Id().equals(sNodeId)) {
				col1.add(tb);
			}
		}
		return col1;
	}
	
	/*
	 * SP used: shp.temp_sel(?)
	 */
	public Collection<TemperatureBean> selectTemperature(String sShipmentId, String sBoxId) {
		Collection<TemperatureBean> tvmBean = null;
		try {
			init();
			GenericType<Collection<TemperatureBean>> tvmBean1 = new GenericType<Collection<TemperatureBean>>(){};
			WebTarget target = client.target(Constants.REST_SERVICE_URL + "ShipmentService/temperatureshipment").path(sShipmentId).path(sBoxId);
			tvmBean  = target.request().get(tvmBean1);
		}catch(Exception e) {
			e.printStackTrace();
		   }
		return tvmBean;
	}
	
}
