package com.demo1.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.TreeMap;
import java.util.Vector;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.demo1.bean.ClientLocationMapBean;
import com.demo1.bean.TripScorecardBean;
import com.demo1.util.Constants;

public class ClientLocationMapDao {
	private Client client; 
	static final Logger logger = LoggerFactory
			.getLogger(ClientLocationMapDao.class);
	
	private void init(){ 
	      this.client = ClientBuilder.newClient(); 
	   }  
	
	
	public TreeMap<String, ClientLocationMapBean> selectLocations() {
		init();
		GenericType<Collection<ClientLocationMapBean>> clmBean1 = new GenericType<Collection<ClientLocationMapBean>>(){};
		Collection<ClientLocationMapBean> col = client 
		         .target(Constants.REST_SERVICE_URL + "ClientLocationMapService/locationmap") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(clmBean1);
		TreeMap<String, ClientLocationMapBean> tm = new TreeMap<String, ClientLocationMapBean>();
		Iterator iter = col.iterator();
		ClientLocationMapBean clmBean = new ClientLocationMapBean();
		while(iter.hasNext()){
			clmBean = (ClientLocationMapBean) iter.next();
			tm.put(clmBean.getLocation_id(), clmBean);
		}
		return tm;
	}
	
	
	
}
