package com.demo1.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Vector;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.demo1.bean.ShipmentBean;
import com.demo1.bean.ShipmentBoxInfo;
import com.demo1.bean.ShipmentLocations;
import com.demo1.bean.ShipmentProducts;
import com.demo1.bean.ShipmentTags;
import com.demo1.bean.ShipmentTransitInfo;
import com.demo1.bean.ShipmentUsers;
import com.demo1.bean.TemperatureBean;
import com.demo1.util.Constants;
import com.demo1.util.DbConnection;
import com.demo1.util.SqlConstants;


public class ShipmentDao {
	private Client client; 
	static final Logger logger = LoggerFactory
			.getLogger(ShipmentDao.class);
	
	private void init(){ 
	      this.client = ClientBuilder.newClient(); 
	   }  
	
	/*
	 * SP used: shp.nxt_shp()
	 */
	public String getNewShipmentId() {
		init();
		GenericType<String> tvmBean1 = new GenericType<String>(){};
		String tvmBean = client 
		         .target(Constants.REST_SERVICE_URL + "ShipmentService/nextshipmentid") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(tvmBean1); 
		return tvmBean;
	}
	
	/*
	 * SP used: shp.ship_all()
	 */
	public Collection<ShipmentBean> getAllShipments() {
		init();
		GenericType<Collection<ShipmentBean>> tvmBean1 = new GenericType<Collection<ShipmentBean>>(){};
		Collection<ShipmentBean> tvmBean = client 
		         .target(Constants.REST_SERVICE_URL + "ShipmentService/allshipments") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(tvmBean1); 
		return tvmBean;
	}
	
	/*
	 * SP used: shp.edit_shp()
	 */
	public Collection<ShipmentBean> getShipmentDetails() {
		init();
		GenericType<Collection<ShipmentBean>> tvmBean1 = new GenericType<Collection<ShipmentBean>>(){};
		Collection<ShipmentBean> tvmBean = client 
		         .target(Constants.REST_SERVICE_URL + "ShipmentService/shipmentdetails") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(tvmBean1); 
		return tvmBean;
	}
	
	/*
	 * SP used: shp.loc_sel()
	 */
	public Collection<ShipmentLocations> getShipmentLocations() {
		init();
		GenericType<Collection<ShipmentLocations>> tvmBean1 = new GenericType<Collection<ShipmentLocations>>(){};
		Collection<ShipmentLocations> tvmBean = client 
		         .target(Constants.REST_SERVICE_URL + "ShipmentService/shipmentlocations") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(tvmBean1); 
		return tvmBean;
	}
	
	/*
	 * SP used: shp.tag_sel()
	 */
	public Collection<ShipmentTags> getShipmentTags() {
		init();
		GenericType<Collection<ShipmentTags>> tvmBean1 = new GenericType<Collection<ShipmentTags>>(){};
		Collection<ShipmentTags> tvmBean = client 
		         .target(Constants.REST_SERVICE_URL + "ShipmentService/shipmenttags") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(tvmBean1); 
		return tvmBean;
	}
	
	/*
	 * SP used: shp.prod_sel()
	 */
	public Collection<ShipmentProducts> getShipmentProducts() {
		init();
		GenericType<Collection<ShipmentProducts>> tvmBean1 = new GenericType<Collection<ShipmentProducts>>(){};
		Collection<ShipmentProducts> tvmBean = client 
		         .target(Constants.REST_SERVICE_URL + "ShipmentService/shipmentproducts") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(tvmBean1); 
		return tvmBean;
	}
	
	/*
	 * shp.user_sel()
	 */
	public Collection<ShipmentUsers> getShipmentUsers() {
		init();
		GenericType<Collection<ShipmentUsers>> tvmBean1 = new GenericType<Collection<ShipmentUsers>>(){};
		Collection<ShipmentUsers> tvmBean = client 
		         .target(Constants.REST_SERVICE_URL + "ShipmentService/shipmentusers") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(tvmBean1); 
		return tvmBean;
	}
	
	/*
	 * SP used: shp.leg_sel(?)
	 */
	public Collection<ShipmentTransitInfo> selectShipmentTransitDetails(String sShipmentId) {
		Collection<ShipmentTransitInfo> tvmBean = null;
		try {
			init();
			GenericType<Collection<ShipmentTransitInfo>> tvmBean1 = new GenericType<Collection<ShipmentTransitInfo>>(){};
			WebTarget target = client.target(Constants.REST_SERVICE_URL + "ShipmentService/shipmenttransitinfo").path(sShipmentId);
			tvmBean  = target.request().get(tvmBean1);
		}catch(Exception e) {
			e.printStackTrace();
		   }
		return tvmBean;
	}
	
	/*
	 * SP used: shp.box_sel(?)
	 */
	public Collection<ShipmentBoxInfo> selectShipmentBoxDetails(String sShipmentId) {
		Collection<ShipmentBoxInfo> tvmBean = null;
		try {
			init();
			GenericType<Collection<ShipmentBoxInfo>> tvmBean1 = new GenericType<Collection<ShipmentBoxInfo>>(){};
			WebTarget target = client.target(Constants.REST_SERVICE_URL + "ShipmentService/shipmentboxdetail").path(sShipmentId);
			tvmBean  = target.request().get(tvmBean1);
		}catch(Exception e) {
			e.printStackTrace();
		   }
		return tvmBean;
	}
	
	/*
	 * SP used: shp.ship_create(?,?,?,?,?,?)
	 */
	public String insertShipmentStep1Data(String sShipmentId, String sSource, String sDestination, String sETD, String sETA, String sInvoice){
		String callResult = Constants.SUCCESS;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			df.setTimeZone(java.util.TimeZone.getTimeZone("UTC"));
			sdf.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
			Date dateObj = sdf.parse(sETD);
			sETD = df.format(dateObj);
			dateObj = sdf.parse(sETA);
			sETA = df.format(dateObj);
			
		Form form = new Form();
	     form.param("shipmentid", sShipmentId);
	      form.param("source", sSource);
	      form.param("destination", sDestination);
	      form.param("etd", sETD);
	      form.param("eta", sETA);
	      form.param("invoicenumber", sInvoice);
	      init();
	      WebTarget target = client
					.target(Constants.REST_SERVICE_URL + "ShipmentService/step1insert").path(sShipmentId).path(sSource).path(sDestination).path(sETD).path(sETA).path(sInvoice);
	      callResult = target
	         .request()
	         .put(Entity.entity(form,
	            MediaType.APPLICATION_FORM_URLENCODED),
	            String.class);
		} catch(Exception e) {
			e.printStackTrace();
			return (Constants.ERROR + ": " + e.getLocalizedMessage());
		}
		return callResult; 
	   } 
	
	/*
	 * SP used: shp.leg_create(?,?,?,?,?,?,?,?)
	 */
	public String insertShipmentStep2Data(String sShipmentId, String sSourceId, String sDestId, String sEtd, String sEta, String sMode, String sCarrier) {
		String callResult = Constants.SUCCESS;
		try {
		Form form = new Form();
	     form.param("shipmentid", sShipmentId);
	      form.param("source", sSourceId);
	      form.param("destination", sDestId);
	      form.param("etd", sEtd);
	      form.param("eta", sEta);
	      form.param("mode", sMode);
	      form.param("carrier", sCarrier);
	      init();
	      WebTarget target = client
					.target(Constants.REST_SERVICE_URL + "ShipmentService/step2insert").path(sShipmentId).path(sSourceId).path(sDestId).path(sEtd).path(sEta).path(sMode).path(sCarrier);
	     
	      callResult = target
	         .request()
	         .put(Entity.entity(form,
	            MediaType.APPLICATION_FORM_URLENCODED),
	            String.class);
		} catch(Exception e) {
			e.printStackTrace();
			return (Constants.ERROR + ": " + e.getLocalizedMessage());
		}
		return callResult;
	   } 
	
	/*
	 * SP used: shp.box_create(?,?,?,?,?,?)
	 */
	public String insertShipmentStep3Data(String sShipmentId, String sBoxId, String sTagId, String sProductId, String sValue, String sCriticality) {
		String callResult = Constants.SUCCESS;
		try {
		Form form = new Form();
		     form.param("shipmentid", sShipmentId);
		      form.param("boxid", sBoxId);
		      form.param("tagid", sTagId);
		      form.param("productid", sProductId);
		      form.param("value", sValue);
		      form.param("criticality", sCriticality);
		      init();
		      WebTarget target = client
		         .target(Constants.REST_SERVICE_URL + "ShipmentService/step3insert").path(sShipmentId).path(sBoxId).path(sTagId).path(sProductId).path(sValue).path(sCriticality);
		      callResult = target
		         .request()
		         .put(Entity.entity(form,
		            MediaType.APPLICATION_FORM_URLENCODED),
		            String.class);
		} catch(Exception e) {
			e.printStackTrace();
			return (Constants.ERROR + ": " + e.getLocalizedMessage());
		}
		return callResult;
	}
	
	/*
	 * SP used: shp.usr_assign(?,?)
	 */
	public String insertShipmentStep4Data(String sShipmentId, String sUsers) {
		String callResult = Constants.SUCCESS;
		try {
			init();
			Form form = new Form();
			//Make sure the data does not have '/ or ;' since the REST fails with these values in the parameters. So, 
			//convert date to another format, for eg. MM-dd-YYYY and then 
			//send it to REST service
			form.param("shipmentid", sShipmentId);
			form.param("users", sUsers);
			WebTarget target = client
					.target(Constants.REST_SERVICE_URL + "ShipmentService/userassign").path(sShipmentId).path(sUsers);
			callResult  = target.request().post(Entity.entity(form,
					MediaType.APPLICATION_FORM_URLENCODED),
					String.class);
		} catch(Exception e) {
			e.printStackTrace();
			return (Constants.ERROR + ": " + e.getLocalizedMessage());
		}
		return callResult;
	}
	
	/*
	 * SP used: shp.ship_update(?,?,?,?,?,?)
	 */
	public String updateShipmentStep1Data(String sShipmentId, String sSource, String sDestination, String sETD, String sETA, String sInvoice) {
		String callResult = "";
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			df.setTimeZone(java.util.TimeZone.getTimeZone("UTC"));
			sdf.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
			Date dateObj = sdf.parse(sETD);
			sETD = df.format(dateObj);
			dateObj = sdf.parse(sETA);
			sETA = df.format(dateObj);
			
		Form form = new Form();
	     form.param("shipmentid", sShipmentId);
	      form.param("source", sSource);
	      form.param("destination", sDestination);
	      form.param("etd", sETD);
	      form.param("eta", sETA);
	      form.param("invoicenumber", sInvoice);
	      init();
	      WebTarget target = client
					.target(Constants.REST_SERVICE_URL + "ShipmentService/step1update").path(sShipmentId).path(sSource).path(sDestination).path(sETD).path(sETA).path(sInvoice);
	      callResult = target
	         .request()
	         .post(Entity.entity(form,
	            MediaType.APPLICATION_FORM_URLENCODED),
	            String.class);
		} catch(Exception e) {
			e.printStackTrace();
			return (Constants.ERROR + ": " + e.getLocalizedMessage());
		}
		return callResult; 
	   } 
	
	/*
	 * SP used: shp.leg_create(?,?,?,?,?,?,?,?), shp.leg_update(?,?,?,?,?,?,?,?), shp.del_update(?,?)
	 */
	public String updateShipmentStep2Data(String sShipmentId, String sSourceId, String sDestId, String sEtd, String sEta, String sMode, String sCarrier) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sReturn = Constants.SUCCESS;
		try {
			Form form = new Form();
		     form.param("shipmentid", sShipmentId);
		      form.param("source", sSourceId);
		      form.param("destination", sDestId);
		      form.param("etd", sEtd);
		      form.param("eta", sEta);
		      form.param("mode", sMode);
		      form.param("carrier", sCarrier);
		      init();
		      WebTarget target = client
						.target(Constants.REST_SERVICE_URL + "ShipmentService/step2update").path(sShipmentId).path(sSourceId).path(sDestId).path(sEtd).path(sEta).path(sMode).path(sCarrier);
		      sReturn = target
		         .request()
		         .post(Entity.entity(form,
		            MediaType.APPLICATION_FORM_URLENCODED),
		            String.class);
			
		} catch(Exception e) {
			e.printStackTrace();
			return (Constants.ERROR + ": " + e.getLocalizedMessage());
		}
		finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return sReturn;
	}
	
	/*
	 * SP used: shp.box_delete(?), shp.box_create(?,?,?,?,?,?)
	 */
	public String updateShipmentStep3Data(String sShipmentId, String sBoxId, String sTagId, String sProductId, String sValue, String sCriticality) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sReturn = Constants.SUCCESS;
		try {
			Form form = new Form();
			form.param("shipmentid", sShipmentId);
		      form.param("boxid", sBoxId);
		      form.param("tagid", sTagId);
		      form.param("productid", sProductId);
		      form.param("value", sValue);
		      form.param("criticality", sCriticality);
		      init();
		      WebTarget target = client
						.target(Constants.REST_SERVICE_URL + "ShipmentService/step3update").path(sShipmentId).path(sBoxId).path(sTagId).path(sProductId).path(sValue).path(sCriticality);
		      sReturn = target
		         .request()
		         .post(Entity.entity(form,
		            MediaType.APPLICATION_FORM_URLENCODED),
		            String.class);
			
		} catch(Exception e) {
			e.printStackTrace();
			return (Constants.ERROR + ": " + e.getLocalizedMessage());
		}
		finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return sReturn;
	}
	
	/*
	 * SP used: shp.usr_assign(?,?)
	 */
	public String updateShipmentStep4Data(String sShipmentId, String sUsers) {
		String callResult = Constants.SUCCESS;
		try {
			init();
			Form form = new Form();
			//Make sure the data does not have '/' since the REST uses '/' to separate parameters. So, 
			//convert date to another format, for eg. MM-dd-YYYY and then 
			//send it to REST service
			form.param("shipmentid", sShipmentId);
			form.param("users", sUsers);
			WebTarget target = client
					.target(Constants.REST_SERVICE_URL + "ShipmentService/userassign").path(sShipmentId).path(sUsers);
			callResult  = target.request().post(Entity.entity(form,
					MediaType.APPLICATION_FORM_URLENCODED),
					String.class);
		} catch(Exception e) {
			e.printStackTrace();
			return (Constants.ERROR + ": " + e.getLocalizedMessage());
		}
		return callResult;
	} 
	
	/*
	* SP used: shp.end_trip(?,?)
	*/
	public String endTrip(String sShipmentId, String sEndTime){ 
		String callResult = Constants.SUCCESS;
		try {
			init();
			Form form = new Form();
			//Make sure the data does not have '/' since the REST uses '/' to separate parameters. So, 
			//convert date to another format, for eg. MM-dd-YYYY and then 
			//send it to REST service
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			sEndTime = df.format(sdf.parse(sEndTime));
			form.param("shipmentid", sShipmentId);
			form.param("endtime", sEndTime);
			WebTarget target = client
					.target(Constants.REST_SERVICE_URL + "ShipmentService/endtrip").path(sShipmentId).path(sEndTime);
			callResult  = target.request().post(Entity.entity(form,
					MediaType.APPLICATION_FORM_URLENCODED),
					String.class);
		} catch(Exception e) {
			e.printStackTrace();
			return (Constants.ERROR + ": " + e.getLocalizedMessage());
			
		}
		return callResult; 
	} 
	
	/*
	 * SP used: shp.ship_delete(?)
	 */
	public String deleteShipment(String sShipmentId){ 
		String callResult = Constants.SUCCESS;
		try {
			init();
			GenericType<String> tvmBean1 = new GenericType<String>(){};
			WebTarget target = client.target(Constants.REST_SERVICE_URL + "ShipmentService/deleteshipment").path(sShipmentId);
			callResult  = target.request().delete(tvmBean1);
		} catch(Exception e) {
			e.printStackTrace();
			return (Constants.ERROR + ": " + e.getLocalizedMessage());
		}
		return callResult; 
	} 
	
	/*
	 * SP used: shp.ship_delete(?)
	 */
	public String deleteBox(String sShipmentId){
		String callResult = Constants.SUCCESS;
		try {
		init();
		GenericType<String> tvmBean1 = new GenericType<String>(){};
		WebTarget target = client.target(Constants.REST_SERVICE_URL + "ShipmentService/deletebox").path(sShipmentId);
		callResult = target.request().delete(tvmBean1);
		} catch(Exception e){
			e.printStackTrace();
			return (Constants.ERROR + ": " + e.getLocalizedMessage());
		}
		return callResult; 
	} 	
}