package com.demo1.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeMap;
import java.util.Vector;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.demo1.bean.ShipmentBean;
import com.demo1.bean.ShipmentBoxInfo;
import com.demo1.bean.TagAssignmentBean;
import com.demo1.bean.TemperatureBean;
import com.demo1.util.Constants;
import com.demo1.util.DbConnection;
import com.demo1.util.SqlConstants;

public class TagAssignmentDao {
	private Client client; 
	static final Logger logger = LoggerFactory
			.getLogger(TagAssignmentDao.class);
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	
	private void init(){ 
	      this.client = ClientBuilder.newClient(); 
	   }  
	
	/**
	 * 
	 * SP used: map.tag_assign_sel()
	 */
	public Collection<TagAssignmentBean> selectAllTagAssignmentRecords() {
		init();
		GenericType<Collection<TagAssignmentBean>> tvmBean1 = new GenericType<Collection<TagAssignmentBean>>(){};
		Collection<TagAssignmentBean> tvmBean = client 
		         .target(Constants.REST_SERVICE_URL + "TagAssignmentService/tagassignment") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(tvmBean1); 
		return tvmBean;
	}
	
	/**
	 * SP user: shp.ship_detail(?)
	 * 
	 */
	public Collection<ShipmentBoxInfo> selectShipmentDetails(int sShipmentId) {
		Collection<ShipmentBoxInfo> tvmBean = null;
		try {
			init();
			GenericType<Collection<ShipmentBoxInfo>> tvmBean1 = new GenericType<Collection<ShipmentBoxInfo>>(){};
			WebTarget target = client.target(Constants.REST_SERVICE_URL + "ShipmentService/shipmentetagdetail").path(""+sShipmentId);
			tvmBean  = target.request().get(tvmBean1);
		}catch(Exception e) {
			e.printStackTrace();
		   }
		return tvmBean;
	}
	

	public int updateTagAssignment(String sTagId, String sShipDate, String sShipId, String sSource, String sDest, String sFlightId, String sCriticalityId, String sMinId, String sMaxId){ 
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement pSelect = null;
		try {
			conn = DbConnection.getConnection();
			//pSelect = conn
			//		.prepareStatement(SqlConstants.UpdateTagAssignment);
			Date dateObj = sdf.parse(sShipDate);
		    Timestamp tstamp = new Timestamp(dateObj.getTime());
			pSelect.setTimestamp(1, tstamp);
			pSelect.setString(2, sShipId);
			pSelect.setString(3, sSource);
			pSelect.setString(4, sDest);
			pSelect.setString(5, sCriticalityId);
			pSelect.setString(6, sSource);
			pSelect.setString(7, sTagId);
			int iReturn = pSelect.executeUpdate();
		} catch (SQLException e) {
			logger.error("localized message: " + e.getLocalizedMessage());
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return 0;
	   } 
	
	
}
