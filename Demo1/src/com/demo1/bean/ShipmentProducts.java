package com.demo1.bean;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "shipmentProducts")
public class ShipmentProducts implements Serializable{

	/**
	 * Generated serial version ID
	 */
	private static final long serialVersionUID = -1874474658549948076L;
	private String sProductId;
	
	public ShipmentProducts(){
		super();
	}
	
	public void init() {
		this.sProductId = "";
		
	}
	
	public String toString(){
		String buf = null;
		buf = "Shipment Products object: \n";
		buf = buf + "Shipment Product: " + sProductId + "\n";
		return buf;
	}

	public String getShipmentProductId() {
		return sProductId;
	}

	public void setShipmentProductId(String sProductId) {
		this.sProductId = sProductId;
	}
}
