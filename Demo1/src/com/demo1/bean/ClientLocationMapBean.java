package com.demo1.bean;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement(name = "clientLocationMapBean") 
public class ClientLocationMapBean implements Serializable{

	/**
	 * Generated serial version ID
	 */
	private static final long serialVersionUID = -1874474658549948076L;
	
	private String location_id;
	private String location_type;
	private String location_name;
	private String location_address;
	private String location_city;
	
	private String latitude;
	private String longitude;
	
	/**
	 * @return the location_id
	 */
	public String getLocation_id() {
		return location_id;
	}

	/**
	 * @param location_id the location_id to set
	 */
	@XmlElement 
	public void setLocation_id(String location_id) {
		this.location_id = location_id;
	}

	/**
	 * @return the location_type
	 */
	public String getLocation_type() {
		return location_type;
	}

	/**
	 * @param location_type the location_type to set
	 */
	@XmlElement 
	public void setLocation_type(String location_type) {
		this.location_type = location_type;
	}

	/**
	 * @return the location_name
	 */
	public String getLocationName() {
		return location_name;
	}

	/**
	 * @param location_name the location_name to set
	 */
	@XmlElement 
	public void setLocationName(String location_name) {
		this.location_name = location_name;
	}

	/**
	 * @return the location_address
	 */
	public String getLocationAddress() {
		return location_address;
	}

	/**
	 * @param location_address the location_address to set
	 */
	@XmlElement 
	public void setLocationAddress(String location_address) {
		this.location_address = location_address;
	}

	/**
	 * @return the location_city
	 */
	public String getLocationCity() {
		return location_city;
	}

	/**
	 * @param location_city the location_city to set
	 */
	@XmlElement 
	public void setLocationCity(String location_city) {
		this.location_city = location_city;
	}

	/**
	 * @return the latitude
	 */
	public String getLatitude() {
		return latitude;
	}

	/**
	 * @param latitude the latitude to set
	 */
	@XmlElement 
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	/**
	 * @return the longitude
	 */
	public String getLongitude() {
		return longitude;
	}

	/**
	 * @param longitude the longitude to set
	 */
	@XmlElement 
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	
	public ClientLocationMapBean(){
		super();
	}
	
	public void init() {
		this.location_id = "";
		this.location_type = "";
		this.location_name = "";
		this.latitude = "";
		this.longitude = "";
		this.location_address = "";
		this.location_city = "";
	}
	
	public String toString(){
		String buf = null;
		buf = "Client Location Map object: \n";
		buf = buf + "location_id: " + location_id + "\n";
		buf = buf + "location_type: " + location_type + "\n";
		buf = buf + "location_name: " + location_name + "\n";
		buf = buf + "latitude: " + latitude + "\n";
		buf = buf + "longitude: " + longitude + "\n";
		buf = buf + "location_address: " + location_address + "\n";
		buf = buf + "location_city: " + location_city + "\n";
		return buf;
	}
	

}
