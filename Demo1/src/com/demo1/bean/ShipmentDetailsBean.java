package com.demo1.bean;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "shipmentDetailsBean")
public class ShipmentDetailsBean implements Serializable{

	/**
	 * Generated serial version ID
	 */
	private static final long serialVersionUID = -1874474658549948076L;
	private String TTE;
	private String MaxT;
	private String MinT;
	
	public ShipmentDetailsBean(){
		super();
	}
	
	public void init() {
		this.TTE = "";
		this.MaxT = "";
		this.MinT = "";
	}
	
	public String toString(){
		String buf = null;
		buf = "ShipmentDetails object: \n";
		buf = buf + "TTE: " + TTE + "\n";
		buf = buf + "MaxT: " + MaxT + "\n";
		buf = buf + "MinT: " + MinT + "\n";
		return buf;
	}

	public String getTTE() {
		return TTE;
	}

	@XmlElement
	public void setTTE(String tTE) {
		TTE = tTE;
	}

	public String getMaxT() {
		return MaxT;
	}

	@XmlElement
	public void setMaxT(String maxT) {
		MaxT = maxT;
	}

	public String getMinT() {
		return MinT;
	}
	
	@XmlElement
	public void setMinT(String minT) {
		MinT = minT;
	}

	

}
