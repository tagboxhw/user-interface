package com.demo1.bean;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "shipmentUsers")
public class ShipmentUsers implements Serializable{

	/**
	 * Generated serial version ID
	 */
	private static final long serialVersionUID = -1874474658549948076L;
	private String sUserId;
	private String sUserName;
	
	public ShipmentUsers(){
		super();
	}
	
	public void init() {
		this.sUserId = "";
		this.sUserName = "";
		
	}
	
	public String toString(){
		String buf = null;
		buf = "Shipment Users object: \n";
		buf = buf + "Shipment UserId: " + sUserId + "\n";
		buf = buf + "Shipment UserName: " + sUserName + "\n";
		return buf;
	}

	public String getShipmentUserId() {
		return sUserId;
	}

	public void setShipmentUserId(String sUserId) {
		this.sUserId = sUserId;
	}
	
	public String getShipmentUserName() {
		return sUserName;
	}

	public void setShipmentUserName(String sUserName) {
		this.sUserName = sUserName;
	}
}
