package com.demo2.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.regex.Pattern;

public class Utils {
  public final static int YYYYMMDD = 1;
  public final static String sfnCurrency = "$###,###;-$###,###";
  public final static NumberFormat currency = NumberFormat.getCurrencyInstance(Locale.US);
  public final static DecimalFormat sfnFormat = (DecimalFormat) currency;
  public Utils() {
  }

  public static void main(String[] args) {

  }

  public static String cleanUp(String inStr) {
      if (inStr == null || (inStr = inStr.trim()).equals("0"))
          inStr = "";
      return (inStr);
  }


  /**
   * convertDate - converts yyyymmdd to mm/dd/yyyy
   */
  public static String convertDate(String myDate) {
      String val = myDate;

      val = val.trim();
      if (val.equals("0"))
          val = "";
      else if (val.length() == 8) {
          val = val.substring(4, 6) + "/" + val.substring(6, 8) + "/" + val.substring(0, 4);
      }
      return val;
  }

  /**
   * formatDate - converts yyyymmdd to yyyy/mm/dd
   */

  public static String formatDate(String myDate, char separator) {
      String val = myDate;

      val = val.trim();
      if (val.equals("0"))
          val = "";
      else if (val.length() == 8) {
          val = val.substring(0, 4) + separator + val.substring(4, 6) + separator + val.substring(6, 8);
      }
      return val;
  }

  /**
   * formatYearMonth - converts yyyymm to mm/yyyy
   */
  public static String convertYearMonth(String myDate) {
      String val = myDate;

      val = val.trim();
      if (val.equals("0"))
          val = "";
      else if (val.length() == 6 && Character.isDigit(val.charAt(0))) {
          val = val.substring(4, 6) + "/" + val.substring(0, 4);
      }
      return val;
  }

  /**
   * convertYearMonthToName - converts yyyymm to month name year eg. September 2000
   */
  public static String convertYearMonthToName(String yyyymm) {
      StringBuffer buff = new StringBuffer();

      int month = Integer.parseInt(yyyymm.substring(4, 6));
      buff.append(getMonthString(month));
      buff.append(" ");
      buff.append(yyyymm.substring(0, 4));
      return (buff.toString());
  }

  /**
   * getMonthMMM - converts mm to month name year eg. Sep
   */
  public static String getMonthMMM(int mm) {
      String monthOfYear[] = { "", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
      if (mm >= 0 && mm < 13)
          return (monthOfYear[mm]);
      else
          return "";
  }

  /**
   * convertYearMonthToName - converts yyyymm to month name year eg. September2000
   */
  public static String convertYearMonthToNameNoSpace(String yyyymm) {
      StringBuffer buff = new StringBuffer();

      int month = Integer.parseInt(yyyymm.substring(4, 6));
      buff.append(getMonthString(month));
      // buff.append(" ");
      buff.append(yyyymm.substring(0, 4));
      return (buff.toString());
  }

  /**
   * rountIt - round to 2 decimal places
   */
  public static String roundIt(String value) {
      Double dd;
      String val = value;
      val = val.trim();
      try {
          dd = Double.valueOf(value);
      } catch (NumberFormatException e) {
          return value.toString();
      }
      ;

      double dd2 = dd.doubleValue();
      DecimalFormat df = new DecimalFormat("0.00");
      return df.format(dd2);
  }

  /**
   * rountIt - round to 2 decimal places
   */
  public static String roundIt(double value) {
      DecimalFormat df = new DecimalFormat("0.00");
      return df.format(value);
  }

  public static String roundToNoDecimal(double value) {
      return (Integer.toString((int) value));
      // DecimalFormat df = new DecimalFormat("0");
      // return df.format(value);

  }

  public static String roundPercent(String value) {
      Double dd;
      String val = value;
      val = val.trim();
      try {
          dd = Double.valueOf(value);
      } catch (NumberFormatException e) {
          return value.toString();
      }
      ;

      double dd2 = dd.doubleValue() * 100.00;
      DecimalFormat df = new DecimalFormat("0.00");
      return df.format(dd2);
  }

  public static String roundPercent4(String value) {
      Double dd;
      String val = value;
      val = val.trim();
      try {
          dd = Double.valueOf(value);
      } catch (NumberFormatException e) {
          return value.toString();
      }
      ;

      double dd2 = dd.doubleValue() * 100.00;
      DecimalFormat df = new DecimalFormat("##0.0000");
      // System.out.println(this.getClass().getName() + (new Double(dd2)).toString());
      // System.out.println(this.getClass().getName() + "dd2: " + df.format(dd2));
      return df.format(dd2);
  }

  public static String formatSFNCurrency(String amount) {
      if (isBlank(amount))
          return "&nbsp;";// Or $0 //TODO Check this in SFN history form
      else {
          sfnFormat.applyPattern(sfnCurrency);
          return sfnFormat.format(Long.parseLong(amount));
      }
  }

  public static String formatPhone(String phoneNumber) {
      if (phoneNumber.length() == 10) {
          phoneNumber = "(" + phoneNumber.substring(0, 3) + ") " + phoneNumber.substring(3, 6) + "-" + phoneNumber.substring(6);
      } else if ((phoneNumber.trim()).equals("0"))
          phoneNumber = "&nbsp;";
      return phoneNumber;
  }

  // 2/8/2006, ssong: for My Assistant v2.2.
  // same as formatPhone(String phoneNumber) but does not return &nbsp;
  public static String formatPhoneCSV(String phoneNumber) {
      String phone = phoneNumber.trim();
      if (phone.equals("0"))
          return "";
      if (phone.length() != 10)
          return phone;

      return "(" + phone.substring(0, 3) + ") " + phone.substring(3, 6) + "-" + phone.substring(6);
  }

  public static String formatPhone(String phone1, String phone2, String phone3) {
      String phoneNumber = "";

      if (phone1.length() > 0)
          phoneNumber = phone1 + "-" + phone2 + "-" + phone3;

      return phoneNumber;
  }

  public static String formatPipeDelimitedPhones(String inPhones) {
      String outPhones = "";
      if (isBlank(inPhones))
          return outPhones;
      else {
          String[] phones = inPhones.split("\\p{Punct}");
          for (int i = 0; i < phones.length; i++) {
              // System.out.println(this.getClass().getName() + "Phone in Utils: " + phones[i]);
              if (i == 0)
                  outPhones = outPhones.concat(Utils.formatPhone(phones[i].trim()));
              else
                  outPhones = outPhones.concat(", " + Utils.formatPhone(phones[i].trim()));
          }
      }
      return outPhones;
  }

  public static String formatZip(String zip, String zip4) {
      String result = "";

      if (!zip.trim().equals("0")) {
          result = zip;
          if (!(zip4.trim()).equals("0"))
              result = result + "-" + zip4;
      }
      return result;
  }

  public static String convertCardNumber(String cardNumber) {
      StringBuffer outStr = new StringBuffer();

      int len = cardNumber.length();
      if (cardNumber == null || cardNumber.trim().equals(""))
          return ("");
      else if (len <= 4)
          return (cardNumber);

      for (int idx = 0; idx < len - 4; idx++)
          outStr.append("*");
      outStr.append(cardNumber.substring(len - 4, len));
      return (outStr.toString());
      /*
       * else return("**" + cardNumber.substring(len-4,len));
       */
      // if (len > 9)
      // return(cardNumber.substring(0,6) + "*" +
      // else
      // return cardNumber;
  }

  public static String dayOfTheWeek(int day) {
      String weekDays[] = { "", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };
      if (day >= 0 && day < 8)
          return (weekDays[day]);
      else
          return "";
  }

  public static String getMonthString(int month) {
      String monthOfYear[] = { "", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October",
              "November", "December" };
      if (month >= 0 && month < 13)
          return (monthOfYear[month]);
      else
          return "";
  }

  public static String pad(String s, int len) {
      StringBuffer buf = new StringBuffer();
      String trimmed = s.trim();

      int slen = trimmed.length();

      // totals line separator
      if (slen > 0 && trimmed.charAt(0) == '_') {
          for (int i = 0; i < len; i++)
              buf.append("_");
      } else {
          if (len > slen) {
              int diff = len - slen;
              for (int i = 0; i < diff; i++) {
                  buf.append(" ");
              }
          }
          buf.append(trimmed);
      }

      return buf.toString();
  }

  // rightPAd - pad to the right of string with spaces
  public static String rightPad(String s, int len) {
      StringBuffer buf = new StringBuffer();
      String trimmed = s.trim();

      int slen = trimmed.length();

      // totals line separator
      buf.append(trimmed);
      if (len > slen) {
          int diff = len - slen;
          for (int i = 0; i < diff; i++)
              buf.append(" ");
      }

      return buf.toString();
  }

  /**
   * getCurrentDate - returns current date in YYYYMMDD format
   */
  public static String getCurrentDate(int format) {
      StringBuffer currDate = new StringBuffer(10);

      GregorianCalendar todaysDate = new GregorianCalendar();
      switch (format) {
      case YYYYMMDD:
          currDate.append(todaysDate.get(Calendar.YEAR));
          currDate.append(zeroPad(todaysDate.get(Calendar.MONTH) + 1, 2));
          currDate.append(zeroPad(todaysDate.get(Calendar.DAY_OF_MONTH), 2));
      }

      return (currDate.toString());
  }

  /**
   * getCurrentDate - returns current date in YYYYMMDD format
   */
  public static String getCurrentDate(String format) {
      StringBuffer currDate = new StringBuffer(10);

      GregorianCalendar todaysDate = new GregorianCalendar();
      if (format.equals("YYYYMMDD")) {
          currDate.append(todaysDate.get(Calendar.YEAR));
          currDate.append(zeroPad(todaysDate.get(Calendar.MONTH) + 1, 2));
          currDate.append(zeroPad(todaysDate.get(Calendar.DAY_OF_MONTH), 2));
      }
      if (format.equals("YYYY-MM-DD")) {
          currDate.append(todaysDate.get(Calendar.YEAR));
          currDate.append("-");
          currDate.append(zeroPad(todaysDate.get(Calendar.MONTH) + 1, 2));
          currDate.append("-");
          currDate.append(zeroPad(todaysDate.get(Calendar.DAY_OF_MONTH), 2));
      } else if (format.equals("month dd, yyyy")) {
          currDate.append(getMonthString(todaysDate.get(Calendar.MONTH) + 1));
          currDate.append(" ");
          currDate.append(todaysDate.get(Calendar.DAY_OF_MONTH));
          currDate.append(", ");
          currDate.append(todaysDate.get(Calendar.YEAR));
      }
      return (currDate.toString());
  }

  /**
   * Gets todays date in the format provided as argument.
   * 
   * @param dateFormat
   *            the format in which today's date is to be returned.
   * @return
   */
  public static String getCurrentDateFromatted(String dateFormat) {
      SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
      Calendar calendar = GregorianCalendar.getInstance();
      return sdf.format(calendar.getTime());
  }

  /**
   * addOneToMonth - add 1 to the month part of the date format YYYYMMDD
   */
  public static String addOneToMonth(String date) {
      int year = 0;
      int month = 0;
      String day = null;
      StringBuffer result = new StringBuffer(15);

      year = Integer.parseInt(date.substring(0, 4));
      month = Integer.parseInt(date.substring(4, 6));
      day = date.substring(6, 8);

      if (month == 12) {
          month = 1;
          year++;
      } else
          month++;

      result.append(year);
      result.append(zeroPad(month, 2));
      result.append(day);

      return (result.toString());
  }

  /**
   * * get today's date in mm/dd/yyyy format
   */
  public static String getDate() {
      GregorianCalendar todaysDate = new GregorianCalendar();
      return ((todaysDate.get(Calendar.MONTH) + 1) + "/" + todaysDate.get(Calendar.DAY_OF_MONTH) + "/" + todaysDate.get(Calendar.YEAR));
  }

  public static String getCurrentYearMonth() {
      GregorianCalendar todaysDate = new GregorianCalendar();
      return ("" + todaysDate.get(Calendar.YEAR) + zeroPad((todaysDate.get(Calendar.MONTH) + 1), 2));
  }

  public static int getCurrentMonth() {
      GregorianCalendar todaysDate = new GregorianCalendar();
      return (todaysDate.get(Calendar.MONTH) + 1);
  }

  public static int getCurrentDate() {
      GregorianCalendar todaysDate = new GregorianCalendar();
      return (todaysDate.get(Calendar.DATE));
  }

  public static int getCurrentYear() {
      GregorianCalendar todaysDate = new GregorianCalendar();
      return (todaysDate.get(Calendar.YEAR));
  }

  public static String getGMTDateTime() {
      GregorianCalendar todaysDate = new GregorianCalendar();
      return ("" + todaysDate.get(Calendar.YEAR) + zeroPad((todaysDate.get(Calendar.MONTH) + 1), 2));
  }

  public static String getCurrentTime() {
      GregorianCalendar todaysDate = new GregorianCalendar();
      return (todaysDate.get(Calendar.HOUR_OF_DAY) + ":" + todaysDate.get(Calendar.MINUTE) + ":" + todaysDate.get(Calendar.SECOND));
  }

  /*
   * Returns current date time in Julian date format Ex: Jan 1, 2000 noon is 2451545.0
   */
  public String getJulianDateTime() {
      GregorianCalendar date = new GregorianCalendar();
      // StringBuffer timeStamp = new StringBuffer(25);

      double y = date.get(Calendar.YEAR);
      double m = date.get(Calendar.MONTH);
      double d = date.get(Calendar.DAY_OF_MONTH);
      double uh = date.get(Calendar.HOUR_OF_DAY);
      double um = date.get(Calendar.MINUTE);
      double us = date.get(Calendar.SECOND);

      System.out.println(this.getClass().getName() + "In JulianDate method");
      System.out.println(this.getClass().getName() + "Year:" + y);
      System.out.println(this.getClass().getName() + "Month:" + m);
      System.out.println(this.getClass().getName() + "Day:" + d);
      System.out.println(this.getClass().getName() + "Hour:" + uh);
      System.out.println(this.getClass().getName() + "Minute:" + um);
      System.out.println(this.getClass().getName() + "Second:" + us);

      double extra = 100.0 * y + m - 190002.5;
      double rjd = 367.0 * y;
      rjd -= Math.floor(7.0 * (y + Math.floor((m + 9.0) / 12.0)) / 4.0);
      rjd += Math.floor(275.0 * m / 9.0);
      rjd += d;
      rjd += (uh + (um + us / 60.0) / 60.) / 24.0;
      rjd += 1721013.5;
      rjd -= 0.5 * extra / Math.abs(extra);
      rjd += 0.5;

      System.out.println(this.getClass().getName() + "JD:" + rjd);
      return String.valueOf(rjd);

      /*
       * timeStamp.append(date.get(Calendar.YEAR)); timeStamp.append("-");
       * timeStamp.append(zeroPad(date.get(Calendar.MONTH) + 1, 2)); timeStamp.append("-");
       * timeStamp.append(zeroPad(date.get(Calendar.DAY_OF_MONTH), 2)); timeStamp.append(" ");
       * timeStamp.append(zeroPad(date.get(Calendar.HOUR_OF_DAY), 2)); timeStamp.append(":");
       * timeStamp.append(zeroPad(date.get(Calendar.MINUTE), 2)); timeStamp.append(":");
       * timeStamp.append(zeroPad(date.get(Calendar.SECOND), 2)); timeStamp.append(".");
       * timeStamp.append(zeroPad(date.get(Calendar.MILLISECOND), 3));
       * 
       * return timeStamp.toString();
       */
  }

  // return a timestamp in YYYY-MM-DD hh:mm:ss.ttt -- ssong, 6/27/2006
  public static String getTimeStamp() {
      GregorianCalendar date = new GregorianCalendar();
      StringBuffer timeStamp = new StringBuffer(25);

      timeStamp.append(date.get(Calendar.YEAR));
      timeStamp.append("-");
      timeStamp.append(zeroPad(date.get(Calendar.MONTH) + 1, 2));
      timeStamp.append("-");
      timeStamp.append(zeroPad(date.get(Calendar.DAY_OF_MONTH), 2));
      timeStamp.append(" ");
      timeStamp.append(zeroPad(date.get(Calendar.HOUR_OF_DAY), 2));
      timeStamp.append(":");
      timeStamp.append(zeroPad(date.get(Calendar.MINUTE), 2));
      timeStamp.append(":");
      timeStamp.append(zeroPad(date.get(Calendar.SECOND), 2));
      timeStamp.append(".");
      timeStamp.append(zeroPad(date.get(Calendar.MILLISECOND), 3));

      return timeStamp.toString();
  }

  /*
   * Return timestamp in YYYYMMDD_hh_mm_ss format
   */
  public static String getTimeStampString() {
      GregorianCalendar date = new GregorianCalendar();
      StringBuffer timeStamp = new StringBuffer(25);

      timeStamp.append(date.get(Calendar.YEAR));
      timeStamp.append(zeroPad(date.get(Calendar.MONTH) + 1, 2));
      timeStamp.append(zeroPad(date.get(Calendar.DAY_OF_MONTH), 2));
      timeStamp.append("_");
      timeStamp.append(zeroPad(date.get(Calendar.HOUR_OF_DAY), 2));
      timeStamp.append("_");
      timeStamp.append(zeroPad(date.get(Calendar.MINUTE), 2));
      timeStamp.append("_");
      timeStamp.append(zeroPad(date.get(Calendar.SECOND), 2));

      return timeStamp.toString();
  }

  public static String getBankDepositMethod(String dbValue) {
      if (!isBlank(dbValue)) {
          if (dbValue.equals("A"))
              return "ACH";
          else if (dbValue.equals("C"))
              return "Check";
          else if (dbValue.equals("H"))
              return "Hold";
          else
              return "";
      } else
          return null;
  }

  public static String zeroPad(int num, int len) {
      String inStr;
      StringBuffer outStr = new StringBuffer(10);

      inStr = Integer.toString(num);
      if (inStr.length() < len) {
          for (int i = 0; i < len - inStr.length(); i++) {
              outStr.append("0");
          }
          outStr.append(inStr);
          return (outStr.toString());
      }
      return (inStr);

  }

  public static String zeroPad(String num, int len) {
      StringBuffer outStr = new StringBuffer(10);

      if (num.length() < len) {
          for (int i = 0; i < len - num.length(); i++) {
              outStr.append("0");
          }
          outStr.append(num);
          return (outStr.toString());
      }
      return (num);

  }

  public static String zeroPadIfNotBlank(String num, int len) {
      if (num.length() == 0)
          return (num);

      return (zeroPad(num, len));
  }

  public static String spacePad(String str, int len) {
      StringBuffer outStr = new StringBuffer(60);

      if (str == null || (str = str.trim()).equals(""))
          str = " ";

      outStr.append(str);
      for (int idx = 0; idx < len - str.length(); idx++) {
          outStr.append(" ");
      }
      return (outStr.toString());

  }

  public static String getMonthDropDown() {

      StringBuffer buf = new StringBuffer(256);

      Calendar cal = Calendar.getInstance();
      for (int idx = 1; idx <= 12; idx++) {
          buf.append("<OPTION value=\"" + cal.get(Calendar.YEAR) + zeroPad((cal.get(Calendar.MONTH) + 1), 2) + "\">"
                  + getMonthString(cal.get(Calendar.MONTH) + 1) + " " + cal.get(Calendar.YEAR) + "</OPTION>" + "\n");
          cal.add(Calendar.MONTH, -1);
      }
      return (buf.toString());
  }
  
  
  public static String getLastMonth() {
      GregorianCalendar cal = null;
      int previousMonth = 0;

      cal = new GregorianCalendar();
      cal.roll(Calendar.MONTH, false);
      previousMonth = cal.get(Calendar.MONTH);
      if (previousMonth == Calendar.DECEMBER)
          cal.roll(Calendar.YEAR, false);

      return (String.valueOf(cal.get(Calendar.YEAR)) + zeroPad(String.valueOf(cal.get(Calendar.MONTH) + 1), 2));

  }

  // convert 2003-07-27 10:52:50.089444 to 2003072710:52:50
  public static String formatTimeStamp(String timeStamp) {
      timeStamp = removeChar(timeStamp, '-'); // remove the dashes
      timeStamp = removeChar(timeStamp, ' '); // remove space
      timeStamp = timeStamp.substring(0, timeStamp.indexOf('.'));
      return (timeStamp);
  }

  /**
   * convert 2003-07-27 10:52:50.089444 to 07/27/2003
   * 
   * @param timeStmp
   * @return
   */
  public static String timeFormat(Timestamp timeStmp) {
      SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
      if (timeStmp != null) {
          // getMonth()+getDay()+getYear()
          // Use two methods for timestamp and date
          return sdf.format(timeStmp);
      }
      return "";
  }

  /**
   * convert 2003-07-27 10:52:50.089444 to the input format
   * 
   * @param timeStmp
   * @return
   */
  public static String timeFormat(String timeStmp, String timeFormat) {
      SimpleDateFormat sdf = new SimpleDateFormat(timeFormat);
      try {
          if (!isBlank(timeStmp)) {
              // getMonth()+getDay()+getYear()
              // Use two methods for timestamp and date
              if (timeStmp.equals("0"))
                  return "";
              java.sql.Timestamp inTime = Timestamp.valueOf(timeStmp);
              return sdf.format(inTime);
          }
      } catch (IllegalArgumentException e1) {
          return timeStmp;
      }
      return "";
  }

  /**
   * Convert date to MM/dd/yyyy
   * 
   * @param timeStmp
   * @return
   * @throws Exception
   */
  public static String dateFormat(Date timeStmp) {
      SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
      if (timeStmp != null) {
          // getMonth()+getDay()+getYear()
          // Use two methods for timestamp and date
          return sdf.format(timeStmp);
      }
      return "";
  }

  /**
   * Convert date to given format
   * 
   * @param dateString
   * @return
   * @throws Exception
   */
  public static String dateFormat(String dateString, String dateFormat) {
      SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
      try {
          if (!isBlank(dateString)) {
              // getMonth()+getDay()+getYear()
              // Use two methods for timestamp and date
              if (dateString.equals("0"))
                  return "";
              Date inDate = Date.valueOf(dateString);
              return sdf.format(inDate);
          }
      } catch (IllegalArgumentException e1) {
          return dateString;
      }
      return "";
  }

  // get month list in numeric months.
  public static String getMonthDropDown(String selected) {

      StringBuffer buf = new StringBuffer(256);
      int month = Integer.parseInt(selected);

      for (int idx = 1; idx <= 12; idx++) {
          if (month == idx)
              buf.append("<OPTION SELECTED value=\"" + zeroPad(idx, 2) + "\">" + zeroPad(idx, 2) + "</OPTION>" + "\n");
          else
              buf.append("<OPTION value=\"" + zeroPad(idx, 2) + "\">" + zeroPad(idx, 2) + "</OPTION>" + "\n");
      }
      return (buf.toString());
  }

  public static String getYearDropDown(String selected) {

      StringBuffer buf = new StringBuffer(256);
      int selectedYear = Integer.parseInt(selected);

      Calendar cal = Calendar.getInstance();

      if ((cal.get(Calendar.YEAR) > selectedYear || (cal.get(Calendar.YEAR) + 10) < selectedYear) && selectedYear != -1) // eg
                                                                                                                         // 1997
          buf.append("<OPTION SELECTED value=\"" + selected + "\">" + selected + "</OPTION>" + "\n");

      for (int idx = 1; idx <= 10; idx++) {
          if (cal.get(Calendar.YEAR) == selectedYear)
              buf.append("<OPTION SELECTED value=\"" + cal.get(Calendar.YEAR) + "\">" + cal.get(Calendar.YEAR) + "</OPTION>" + "\n");
          else
              buf.append("<OPTION value=\"" + cal.get(Calendar.YEAR) + "\">" + cal.get(Calendar.YEAR) + "</OPTION>" + "\n");

          cal.add(Calendar.YEAR, 1);
      }
      return (buf.toString());
  }

  /**
   * Returns the difference between two decimal values and strips pennies
   * 
   * @param operand1
   * @param operand2
   * @return
   */
  public static String getNumericDifference(String operand1, String operand2) {
      float diff = 0;
      try {
          diff = Float.parseFloat(operand1) - Float.parseFloat(operand2);
      } catch (Exception e) {
          diff = 0;
      }
      return stripPennies(String.valueOf(diff));
  }

  public static boolean isAlphaNumeric(String inStr) {
      char inChar;

      for (int idx = 0; idx < inStr.length(); idx++) {
          inChar = inStr.charAt(idx);
          if (!Character.isLetterOrDigit(inChar))
              return (false);
      }
      return (true);
  }

  public static boolean isNumeric(String inStr) {
      char inChar;

      for (int idx = 0; idx < inStr.length(); idx++) {
          inChar = inStr.charAt(idx);
          if (inChar < '0' || inChar > '9')
              return (false);
      }
      return (true);
  }

  public static boolean isNumberInRange(String inStr, int start, int end) {
      boolean result = false;

      if (isNumeric(inStr)) {
          int currNum = Integer.parseInt(inStr);
          if (currNum >= start && currNum <= end) {
              result = true;
          }
      }
      return result;
  }

  public static boolean isAlphabetic(String inStr) {
      char inChar;

      for (int idx = 0; idx < inStr.length(); idx++) {
          inChar = inStr.charAt(idx);
          if (!Character.isLowerCase(inChar) && !Character.isUpperCase(inChar))

              // if ((inChar < 'A' && inChar > 'z') || (inChar > 'Z' && inChar
              // < 'a'))
              return (false);
      }
      return (true);
  }

  public static boolean isAlphabeticOrSpace(String inStr) {
      char inChar;

      for (int idx = 0; idx < inStr.length(); idx++) {
          inChar = inStr.charAt(idx);
          if (!Character.isLowerCase(inChar) && !Character.isUpperCase(inChar) && !Character.isSpace(inChar))

              // if ((inChar < 'A' && inChar > 'z') || (inChar > 'Z' && inChar
              // < 'a'))
              return (false);
      }
      return (true);
  }

  public static boolean hasApostrophe(String inStr) {
      return ((inStr.indexOf('\'') != -1) ? true : false);
  }

  /**
   * removes quotes around a string
   */
  public static String removeQuotes(String inStr) {

      if (inStr == null)
          return inStr;

      inStr = inStr.trim();
      if (inStr.equals("") || inStr.length() == 1)
          return (inStr);
      if (inStr.startsWith("\"") && inStr.endsWith("\"")) {
          inStr = inStr.substring(inStr.indexOf("\"") + 1, inStr.lastIndexOf("\""));
      }
      return (inStr);
  }

  /**
   * what about name like O'Neal ? This method prepars string for either saving to or searching the database.
   */
  public static String prepareForApostrophe(String inStr) {

      StringBuffer outStr = new StringBuffer();

      if (inStr == null)
          return inStr;

      inStr = inStr.trim();
      if (inStr.equals("") || inStr.length() == 1) // <-- why inStr.length() == 1? ssong
          return (inStr);
      for (int idx = 0; idx < inStr.length(); idx++) {
          if (inStr.charAt(idx) == '\'')
              outStr.append(inStr.charAt(idx));
          outStr.append(inStr.charAt(idx));
      }
      return (outStr.toString());

  }

  // 1/17/2006, ssong: simpler way of handling apostrophes
  public static String handleApostrophe(String str) {
      return str.replaceAll("'", "''");
  }

  public static String saintConversionForCity(String cityName) {
      String normalizedCityName = cityName.trim().toUpperCase();
      if (normalizedCityName.equals("ST") || normalizedCityName.equals("ST."))
          return "SAINT";

      // replace st and st. at the beginning
      normalizedCityName = normalizedCityName.replaceAll("^ST\\.?\\s", "SAINT ");
      // replace st and st. in the middle
      normalizedCityName = normalizedCityName.replaceAll("\\sST\\.?\\s", " SAINT ");
      // replace st and st. at the end
      normalizedCityName = normalizedCityName.replaceAll("\\sST\\.?$", " SAINT");
      return normalizedCityName;
  }

  // nmeda 03-04-2008: Mc XXX or mc XXX to McXXX conversion
  public static String mcConversion(String name) {
      String normalizedName = name.trim().toUpperCase();

      // replace mc at the beginning
      normalizedName = normalizedName.replaceAll("^MC\\s", "MC");
      // replace mc in the middle
      normalizedName = normalizedName.replaceAll("\\sMC\\s", " MC");
      return normalizedName;
  }

  /**
   * Remove any punctuation character from the given string \p{Punct} Punctuation: One of
   * !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~
   * 
   * @param name
   * @return
   */
  public static String removePunctuation(String name) {
      String tempFile = null;
      if (name == null) {
          return null;
      } else {
          tempFile = name.replaceAll("\\p{Punct}", "");
      }
      return tempFile;
  }

  /**
   * removes a character from a string
   */
  public static String removeChar(String inStr, char inChar) {
      StringBuffer outStr = new StringBuffer();

      if (inStr == null)
          return inStr;

      inStr = inStr.trim();
      if (inStr.equals("") || inStr.length() == 1)
          return (inStr);
      for (int idx = 0; idx < inStr.length(); idx++) {
          if (inStr.charAt(idx) != inChar)
              outStr.append(inStr.charAt(idx));
      }
      return (outStr.toString());
  }

  /**
   * This regular expression will match on a real / decimal / floating point / numeric string with no more than 2
   * digits past the decimal. The negative sign (-) is allowed. No leading zeroes or commas. Matches 123 | 123.54 |
   * -.54 Non-Matches 123.543 | 0012 | 1,000.12
   * 
   * @param inStr
   * @return
   */
  public static boolean isCurrency(String inStr) {
      String REGEX = "^[-]?([1-9]{1}[0-9]{0,}([.][0-9]{0,2})?|0([.][0-9]{0,2})?|[.][0-9]{1,2})$";
      boolean matches = false;
      if (!isBlank(inStr))
          matches = Pattern.matches(REGEX, inStr);
      return matches;
  }

  /**
   * This regular expression will match on a real / decimal / floating point / numeric string with no more than 3
   * digits past the decimal. The negative sign (-) is not allowed. Leading zeroes. No commas. Matches 12 | 12.54 |
   * .54 Non-Matches 123.543 | 812 | 1,000.12
   * 
   * @param inStr
   * @return
   */
  public static boolean isDecimal(String inStr) {
      String REGEX = "^([0-9]{0,2}([.][0-9]{0,3})?)";
      boolean matches = false;
      if (!isBlank(inStr))
          matches = Pattern.matches(REGEX, inStr);
      return matches;
  }

  /**
   * This regular expression will match on a real / decimal / floating point / numeric string with no more than 2
   * digits past the decimal, 6 digits before. The negative sign (-) is not allowed. Leading zeroes. No commas.
   * Matches 12332 | 12.54 | .54 Non-Matches 123.543 | 989812 | 1,000.12
   * 
   * @param inStr
   * @return
   */
  public static boolean isGroupCapDecimal(String inStr) {
      String REGEX = "^([0-9]{0,6}([.][0-9]{0,2})?)";
      boolean matches = false;
      if (!isBlank(inStr))
          matches = Pattern.matches(REGEX, inStr);
      return matches;
  }

  /**
   * This regular expression will match on a real / decimal / floating point / numeric string with no more than 2
   * digits past the decimal, 5 digits before. The negative sign (-) is not allowed. Leading zeroes. No commas.
   * Matches 12332 | 12.54 | .54 Non-Matches 123.543 | 989812 | 1,000.12
   * 
   * @param inStr
   * @return
   */
  public static boolean isSalesLimitDecimal(String inStr) {
      String REGEX = "^([0-9]{0,5}([.][0-9]{0,2})?)";
      boolean matches = false;
      if (!isBlank(inStr))
          matches = Pattern.matches(REGEX, inStr);
      return matches;
  }

  /**
   * isUpperCase - check to see if a string is all upper case returns : true if all upercase, otherwise false
   */
  public static boolean isUpperCase(String inStr) {
      for (int idx = 0; idx < inStr.length(); idx++) {
          if (!Character.isUpperCase(inStr.charAt(idx)))
              return (false);
      }
      return (true);
  }

  /**
   * isLowerCase - check to see if a string is all lower case
   */
  public static boolean isLowerCase(String inStr) {
      for (int idx = 0; idx < inStr.length(); idx++) {
          if (!Character.isLowerCase(inStr.charAt(idx)))
              return (false);
      }
      return (true);
  }

  public static boolean isBlank(String inStr) {
      if (inStr == null || inStr.trim().equals(""))
          return true;
      else
          return false;
  }

  public static boolean isBlankOrZero(String inStr) {
      if (inStr == null || inStr.trim().equals("") || inStr.trim().equals("0"))
          return true;
      else
          return false;
  }

  // nmeda 06/08/2007: This method is not being used from any application
  // Only called from escrip.shed for testing purposes
  public static String properCase(String inStr) {
      StringBuffer inBuf = null, outBuf = null;
      if (inStr == null || inStr.trim().equals(""))
          return inStr;
      else {
          StringTokenizer st = new StringTokenizer(inStr, " '-");
          outBuf = new StringBuffer();
          while (st.hasMoreTokens()) {
              inBuf = new StringBuffer(st.nextToken());
              inBuf.setCharAt(0, Character.toUpperCase(inBuf.charAt(0)));
              // System.out.println(this.getClass().getName() + inBuf.toString());
              outBuf.append(inBuf);
          }
      }
      return (outBuf.toString());
  }

  public static String trimField(String inField) {

      String outField = "";

      if (inField != null && inField.length() > 0)
          outField = inField.trim();

      return (outField);
  }

  /**
   * Method to trim away any trailing zeroes after a decimal If last char is decimal, then it trims away that too and
   * returns an integer If input is 0, returns 0.
   */
  public static String rtrimDecimal(String inStr) {
      if (!isBlank(inStr) && inStr.equals("0.00")) {
          return "0";
      }
      return rtrim(rtrim(inStr, '0'), '.');

  }

  public static String rtrim(String inStr, char trimChar) {
      if (isBlank(inStr))
          return "";

      int len = inStr.length();
      while (inStr.charAt(len - 1) == trimChar) {
          inStr = inStr.substring(0, len - 1);
          len--;
      }
      return inStr;
  }

  /**
   * Method to remove any leading zeroes from a string
   * 
   * @author nmeda
   * @date 03/12/2008 Added method for all card entry applications and to fix bugs in CardAdd
   */
  public static String removeLeadingZeros(String str) {
      if (str == null) {
          return null;
      }
      char[] chars = str.toCharArray();
      int index = 0;
      for (; index < str.length(); index++) {
          if (chars[index] != '0') {
              break;
          }
      }
      return (index == 0) ? str : str.substring(index);
  }

  /**
   * nmeda 06/12/2007: Reusable method; remove from other classes
   */
  public static String fixAddress(String inStr) {
      String token = null;
      StringBuffer outStr = new StringBuffer(40);
      StringTokenizer st = new StringTokenizer(inStr, ".'-#, ", true);
      while (st.hasMoreTokens()) {
          token = st.nextToken();
          if (token.equals(".") || token.equals("'") || token.equals("-") || token.equals("#") || token.equals(",") || token.equals(" "))
              outStr.append(token);
          else {
              token = convertToProperCase(token);
              outStr.append(token);
          }

      }

      return (outStr.toString());
  }

  // nmeda 06/12/2007: Added saint conversion for Group City names
  public static String fixGroupCity(String inStr) {
      return fixName(saintConversionForCity(inStr));
  }

  /**
   * Fix for O'Brady'S to O'Brady's
   * 
   * @author nmeda
   * @date 06/10/2007
   */
  public static String fixName(String inStr) {
      String token = null;
      StringBuffer outStr = new StringBuffer(40);
      StringTokenizer st = new StringTokenizer(inStr, ".'- ", true);
      while (st.hasMoreTokens()) {
          token = st.nextToken();
          if (token.equals("."))
              continue;
          if (token.equals("-") || token.equals(" "))
              outStr.append(token);
          else if (token.equals("'")) {
              outStr.append(token);
              while (st.hasMoreTokens() && (token = st.nextToken()).length() == 1) {
                  if (token.equals("."))
                      continue;
                  else
                      outStr.append(token.toLowerCase());
              }
              if (token.length() != 1)
                  outStr.append(convertToProperCase(token));
          } else {
              token = convertToProperCase(token);
              outStr.append(token);
          }
      }
      return (outStr.toString());
  }

  /**
   * Group Name fix for 'PTA' or 'PTSO' instead of 'Pta' or 'Ptso' nmeda 08/03/2007: Added 'PTO' instead of 'Pto'
   * 
   * @author nmeda
   * @date 06/12/2007
   */
  public static String fixGroupName(String inStr) {
      inStr = mcConversion(inStr);
      inStr = fixName(inStr);
      String token = null;
      StringBuffer outStr = new StringBuffer(40);
      StringTokenizer st = new StringTokenizer(inStr, "'- ", true);
      while (st.hasMoreTokens()) {
          token = st.nextToken();
          if (token.toLowerCase().equals("pta") || token.toLowerCase().equals("ptso") || token.toLowerCase().equals("pto"))
              outStr.append(token.toUpperCase());
          else
              outStr.append(token);
      }
      return (outStr.toString());
  }

  public static String convertToProperCase(String inStr) {
      // 08-15-2006 sjambhekar: added the if part ot this method to check if the string is null or empty
      if (inStr == null || inStr.trim().equals(""))
          return inStr;
      else {
          StringBuffer outStr = new StringBuffer(30);
          outStr.append(inStr.toLowerCase());
          outStr.setCharAt(0, Character.toUpperCase(outStr.charAt(0)));
          return (outStr.toString());
      }
  }

  // return me a string with digits only
  public static String digitsOnly(String instr) {
      StringBuffer outStr = new StringBuffer();
      for (int idx = 0; idx < instr.length(); idx++) {
          if (instr.charAt(idx) >= '0' && instr.charAt(idx) <= '9')
              outStr.append(instr.charAt(idx));
      }
      if (outStr.length() == 0)
          outStr.append("0");

      return (outStr.toString());
  }

  // return true if a string has digits only
  public static boolean isDigitsOnly(String instr) {
      for (int idx = 0; idx < instr.length(); idx++) {
          if (instr.charAt(idx) >= '0' && instr.charAt(idx) <= '9')
              ;// Do nothing
          else
              return false;
      }
      return true;
  }

 
  /**
   * read a text file into a string
   * 
   * @param fileName
   *            name of the text file to be read
   * @return content of the text file
   * @throws IOException
   */
  public static String ReadTextFile(String fileName) throws IOException {
      String content = null;

      FileInputStream inStream = new FileInputStream(fileName);
      int available = inStream.available();
      byte byte_arr[] = new byte[available];
      inStream.read(byte_arr);
      inStream.close();

      content = new String(byte_arr);

      return content;
  }

  /**
   * write a string to a text file
   * 
   * @param buffer
   *            string to be written
   * @param fileName
   *            name of the text file
   * @throws IOException
   */
  public static void WriteTextFile(String buffer, String fileName) throws IOException {
      FileOutputStream outStream = new FileOutputStream(fileName);
      outStream.write(buffer.getBytes());

      outStream.close();
  }

  /**
   * convert possible null strings to empty strings for display
   * 
   * @param str
   *            input string
   * @return empty string if null, the string itself if not null
   */
  public static String FormatNullString(String str) {
      if (str == null) {
          return "";
      }
      return str;
  }

  /**
   * If Java 1.4 is unavailable, the following technique may be used.
   * 
   * @param aInput
   *            is the original String which may contain substring aOldPattern
   * @param aOldPattern
   *            is the non-empty substring which is to be replaced
   * @param aNewPattern
   *            is the replacement for aOldPattern
   */
  public static String oldReplace(final String aInput, final String aOldPattern, final String aNewPattern) {
      if (aOldPattern.equals("")) {
          throw new IllegalArgumentException("Old pattern must have content.");
      }

      final StringBuffer result = new StringBuffer();
      // startIdx and idxOld delimit various chunks of aInput; these
      // chunks always end where aOldPattern begins
      int startIdx = 0;
      int idxOld = 0;
      while ((idxOld = aInput.indexOf(aOldPattern, startIdx)) >= 0) {
          // grab a part of aInput which does not include aOldPattern
          result.append(aInput.substring(startIdx, idxOld));
          // add aNewPattern to take place of aOldPattern
          result.append(aNewPattern);

          // reset the startIdx to just after the current match, to see
          // if there are any further matches
          startIdx = idxOld + aOldPattern.length();
      }
      // the final chunk will go to the end of aInput
      result.append(aInput.substring(startIdx));
      return result.toString();
  }

  /**
   * Validate input date string using MM/dd/yyyy format
   * 
   * @param date
   * @return Empty string or "Invalid date"
   */
  public static String validateMercManDate(String date) {
      String dt = date;
      String fdt = "MM/dd/yyyy";
      try {
          SimpleDateFormat sdf = new SimpleDateFormat(fdt);
          sdf.setLenient(false);
          java.util.Date dt2 = sdf.parse(dt);
          return null;
      } catch (ParseException e) {
          // return (e.getMessage());
          return "Invalid date";
      } catch (IllegalArgumentException e) {
          return "Invalid date";
      }
  }

  /**
   * For any valid Date, check if it is later than 01-01-2000, else not allowed.
   * 
   * @param date
   * @return Empty string or "Termination date before 01/01/2000 is not allowed"
   */
  public static boolean validateCutOffDate(String date) {
      String cutOffDate = "01/01/2000";
      String fdt = "MM/dd/yyyy";
      try {
          SimpleDateFormat sdf = new SimpleDateFormat(fdt);
          sdf.setLenient(false);
          java.util.Date dt1 = sdf.parse(cutOffDate);
          java.util.Date dt2 = sdf.parse(date);
          if (dt2.compareTo(dt1) >= 0)
              return true;
          else
              return false;
      } catch (ParseException e) {
          // return (e.getMessage());
          return false;
      } catch (IllegalArgumentException e) {
          return false;
      }
  }

  // Validate input date string using MM/dd/yyyy format
  public static boolean greaterEndDate(String beginDate, String endDate) {
      String fdt = "MM/dd/yyyy";
      try {
          SimpleDateFormat sdf = new SimpleDateFormat(fdt);
          sdf.setLenient(false);
          java.util.Date dt1 = sdf.parse(beginDate);
          java.util.Date dt2 = sdf.parse(endDate);
          if (dt2.compareTo(dt1) >= 0)
              return true;
          else
              return false;
      } catch (ParseException e) {
          // return (e.getMessage());
          return false;
      } catch (IllegalArgumentException e) {
          return false;
      }
  }

  /*
   * return the difference between enddate-begindate in months
   */
  public static int dateDiff(String beginDate, String endDate) {
      String fdt = "MM/dd/yyyy";
      int monthDiff = 0;
      try {
          SimpleDateFormat sdf = new SimpleDateFormat(fdt);
          sdf.setLenient(false);
          java.util.Date dt1 = sdf.parse(beginDate);
          java.util.Date dt2 = sdf.parse(endDate);
          Calendar cal1 = Calendar.getInstance();
          Calendar cal2 = Calendar.getInstance();
          cal1.setTime(dt1);
          cal2.setTime(dt2);
          int yearDiff = cal2.get(Calendar.YEAR) - cal1.get(Calendar.YEAR);
          monthDiff = yearDiff * 12 + cal2.get(Calendar.MONTH) - cal1.get(Calendar.MONTH);
      } catch (ParseException e) {
          // return (e.getMessage());
          return 0;
      } catch (IllegalArgumentException e) {
          return 0;
      }
      return monthDiff;
  }

  // Check if given time is lesser than the constant time using yyyy-MM-dd hh:mm:SS.ssssss format
  public static boolean lesserTimestamp(String givenTime, String constantTime) {
      // String fdt = "yyyy-MM-dd hh:mm:SS.ssssss";
      try {

          if (Timestamp.valueOf(givenTime).compareTo(Timestamp.valueOf(constantTime)) < 0)
              return true;
          else
              return false;
      } catch (IllegalArgumentException e) {
          return false;
      }
  }

  /**
   * Check if the range contains 201209
   * 
   * @param date
   * @return Empty string or "Invalid date"
   */
  public static boolean containsSep12(String startDate, String endDate) {

      String startVal = startDate;
      String endVal = endDate;

      startVal = startVal.trim();
      endVal = endVal.trim();
      if (isBlank(startVal))
          return false;
      else if (isBlank(endVal))
          return false;
      else if (startVal.equals("201209"))
          return true;
      else if (endVal.equals("201209"))
          return true;
      else {
          String fdt = "yyyyMM";
          try {
              SimpleDateFormat sdf = new SimpleDateFormat(fdt);
              sdf.setLenient(false);
              java.util.Date sdate = sdf.parse(startVal);
              java.util.Date edate = sdf.parse(endVal);
              if (sdate.before(Date.valueOf("2012-09-01")) && edate.after(Date.valueOf("2012-09-01")))
                  return true;
              else
                  return false;
          } catch (ParseException e) {
              return false;
          } catch (IllegalArgumentException e) {
              return false;
          }
      }
  }

  /**
   * Deletes all the files in the given pathname that match the name or extension
   * 
   * @param pathname
   * @param name
   *            (filename or extension)
   */
  public static void deleteFiles(String pathname, String name) {

      ExtensionFilter filter = new ExtensionFilter(name);
      File dir = new File(pathname);

      String[] list = dir.list(filter);
      File file;
      if (list == null || list.length == 0)
          return;

      for (int i = 0; i < list.length; i++) {
          file = new File(pathname + list[i]);
          boolean isdeleted = file.delete();
      }
  }

  /**
   * @param phoneCard10
   * @return phoneCard10+space+checkdigit where check digit is generated for phoneCard10+0
   */
  public static String generateCheckDigit(String phoneCard10) {
      String phoneCard12 = null;
      String phoneCard11 = phoneCard10.concat("0");

      int sum11 = 0;
      for (int i = 0; i < phoneCard11.length(); i++) {
          if (isOdd(i + 1)) {
              sum11 += 3 * Integer.valueOf(phoneCard11.substring(i, i + 1));
          } else {
              sum11 += Integer.valueOf(phoneCard11.substring(i, i + 1));
          }
      }
      phoneCard12 = phoneCard10.concat(" ").concat(String.valueOf((10 - (sum11 % 10)) % 10));
      return phoneCard12;
  }

  /**
   * @param x
   * @return
   */
  public static boolean isOdd(int x) {
      return ((x % 2) == 1);
  }

  /*** method to strip of pennies in the price field for display. **/

  public static String stripPennies(String Value) {
      String[] fnlArr = new String[2];
      String retVal = "";

      if (!Utils.isBlank(Value)) {
          fnlArr = Value.split("\\.");
          retVal = fnlArr[0];
      }

      return retVal;

  }

  /***
   * Method to Get the remaining days from the current date for a given input date
   * 
   * 
   * @author radhika
   * 
   */

  public static String getRemainingDays(String dat) {

      Timestamp endDate = java.sql.Timestamp.valueOf(dat);
      java.util.Date date = new java.util.Date();
      Timestamp current = new Timestamp(date.getTime());
      long diffInSeconds = (endDate.getTime() - current.getTime()) / 1000;
      long diff[] = new long[] { 0, 0, 0, 0 };
      /* sec */diff[3] = (diffInSeconds >= 60 ? diffInSeconds % 60 : diffInSeconds);
      /* min */diff[2] = (diffInSeconds = (diffInSeconds / 60)) >= 60 ? diffInSeconds % 60 : diffInSeconds;
      /* hours */diff[1] = (diffInSeconds = (diffInSeconds / 60)) >= 24 ? diffInSeconds % 24 : diffInSeconds;
      /* days */diff[0] = (diffInSeconds = (diffInSeconds / 24));

      String val = (String.format("%d DAY%s, %d HOUR%s, %d MIN ", diff[0], diff[0] > 1 ? "s" : "", diff[1], diff[1] > 1 ? "s" : "",
              diff[2]

      ));

      return val;

  }

  /***
   * 
   * Method to leftpad zeroes to the zipcode if zicode length is <5
   * 
   * @author Radhika
   * 
   */

  public static String leftPadZip(String zipcode5) {
      String pad = "";
      if (zipcode5.length() < 5) {
          int len = 5 - zipcode5.length();
          for (int i = 0; i < len; i++) {
              pad = pad + "0";
          }
          zipcode5 = pad + zipcode5;
      }
      return zipcode5;
  }

  public static long formatDate(String date, String format) {
      SimpleDateFormat sdf = new SimpleDateFormat(format);
      java.util.Date parsedDate = null;
      try {
          parsedDate = sdf.parse(date);
      } catch (ParseException e) {
          e.printStackTrace();
      }
      return parsedDate.getTime();
  }

  public static String removeComma(String input) {

      return input.replaceAll("\\,", "");
  }

  public static boolean isNegative(String inStr) {
      char inChar;

      for (int idx = 0; idx < inStr.length(); idx++) {
          inChar = inStr.charAt(idx);
          if (inChar == '-')
              return (true);
      }
      return (false);

  }

  static class ExtensionFilter implements FilenameFilter {

      private String extension;

      /**
       * @param extension
       */
      public ExtensionFilter(String extension) {
          this.extension = extension;
      }

      public boolean accept(File dir, String name) {
          return (name.endsWith(extension));
      }
  }

  public static String leftTrimByLength(String inputString, int trimLength) {
      String outputString = "";

      if (inputString != null) {
          inputString = inputString.trim();
          if (inputString.length() <= trimLength) {
              outputString = inputString;
          } else {
              outputString = inputString.substring(0, trimLength);
          }
      }

      return outputString;
  }
  
  public static boolean isNDaysOld(String sEntryDate, int num) {
  	boolean bIsNDaysOld = false;
  	GregorianCalendar todaysDate = new GregorianCalendar();
      GregorianCalendar setEntryDate = new GregorianCalendar();
      setEntryDate.set(Integer.parseInt(sEntryDate.substring(0, 4)), (Integer.parseInt(sEntryDate.substring(5, 7)) - 1), Integer.parseInt(sEntryDate.substring(8, 10)));
      int diffInDays = (int)((todaysDate.getTimeInMillis() - setEntryDate.getTimeInMillis()) / (1000 * 60 * 60 * 24) );
  	if(diffInDays >= num) bIsNDaysOld = true;
  	return bIsNDaysOld;  
  }
  
  public static double getAlertTime(String sTime) {
	  int iTime = Integer.parseInt(sTime);
		double fTime = 0.00;
		switch(iTime) {
		case 15: 
			fTime = (double) iTime/(24*60);
			break;
		case 30: 
			fTime = (double) iTime/(24*60);
			break;
		case 60: 
			fTime = (double) iTime/(24*60);
			break;
		case 12: 
			fTime = (double) (iTime)/(24);
			break;
		case 24: 
			fTime = (double) (iTime)/(24);
			break;
		case 7: 
			fTime = (double) (iTime);
			break;
		case 31: 
			fTime = (double) (iTime);
			break;
		case 3: 
			fTime = (double) (3*30);
			break;
		default:
			fTime = (double) (60/(24*60));
			break;
		}
		return fTime;
  }
  
} // end of class

