package com.demo2.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.TreeMap;
import java.util.Vector;

import org.omg.CORBA.Current;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.demo2.bean.MapsBean;
import com.demo2.bean.PlanMapsBean;
import com.demo2.bean.StationaryHumidityBean;
import com.demo2.bean.TransitHumidityBean;
import com.demo2.util.DbConnection;
import com.demo2.util.SqlConstants;
import com.demo2.util.Utils;

public class TransitHumidityDao {

	static final Logger logger = LoggerFactory
			.getLogger(TransitHumidityDao.class);

	public TreeMap getChartData(String sVehicleId, String sTime) {
		Collection col = new Vector();
		Connection conn = null;
		ResultSet rs = null;
		TransitHumidityBean stBean = new TransitHumidityBean();
		TreeMap tm = new TreeMap();
		try {
			double fTime = Utils.getAlertTime(sTime);
			// logger.info("fTime: " + fTime);
			conn = DbConnection.getConnection();
			PreparedStatement pSelect = conn
					.prepareStatement(SqlConstants.HumidityTrends);
			pSelect.setString(1, sVehicleId);
			rs = pSelect.executeQuery();
			while (rs.next()) {
				// logger.info(rs.getString(1) + " " + rs.getString(2) + " "
				// +rs.getString(3) + " " + rs.getString(4));
				stBean = new TransitHumidityBean();
				//stBean.setGW_client_id(rs.getString(1));
				stBean.setND_client_id(sVehicleId);
				stBean.setTimestamp(rs.getString(2));
				stBean.setHumidity(rs.getString(3));
				if (tm.containsKey(stBean.getND_client_id())) {
					col = (Collection) tm.get(stBean.getND_client_id());
					col.add(stBean);
				} else {
					col.add(stBean);
					tm.put(stBean.getND_client_id(), col);
				}
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return tm;
	}

}
