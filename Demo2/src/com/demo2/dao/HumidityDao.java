package com.demo2.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeMap;
import java.util.Vector;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.demo2.bean.HumidityBean;
import com.demo2.bean.TemperatureBean;
import com.demo2.util.Constants;
import com.demo2.util.DbConnection;
import com.demo2.util.SqlConstants;

public class HumidityDao {
	private Client client; 
	static final Logger logger = LoggerFactory
			.getLogger(HumidityDao.class);
	
	private void init(){ 
	      this.client = ClientBuilder.newClient(); 
	   }  
	
	public Collection<HumidityBean> getHumidityData() {
		init();
		GenericType<Collection<HumidityBean>> tvmBean1 = new GenericType<Collection<HumidityBean>>(){};
		Collection<HumidityBean> tvmBean = client 
		         .target(Constants.REST_SERVICE_URL + "HumidityService/humidity") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(tvmBean1); 
		return tvmBean;
	}
	
	public Collection<HumidityBean> getHumidityData(String sNodeId) {
		init();
		Collection<HumidityBean> col1 = new Vector();
		GenericType<Collection<HumidityBean>> tvmBean1 = new GenericType<Collection<HumidityBean>>(){};
		Collection<HumidityBean> tvmBean = client 
		         .target(Constants.REST_SERVICE_URL + "HumidityService/humidity") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(tvmBean1); 
		for(HumidityBean tb: tvmBean) {
			if(tb.getNode_Id().equals(sNodeId)) {
				col1.add(tb);
			}
		}
		return col1;
	}
	
}
