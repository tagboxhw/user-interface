package com.demo2.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

import org.omg.CORBA.Current;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.demo2.bean.MapsBean;
import com.demo2.bean.PlanMapsBean;
import com.demo2.bean.StationaryDoorActivityBean;
import com.demo2.bean.StationaryHumidityBean;
import com.demo2.util.DbConnection;
import com.demo2.util.SqlConstants;
import com.demo2.util.Utils;

public class TransitDoorActivityDao {

	static final Logger logger = LoggerFactory
			.getLogger(TransitDoorActivityDao.class);

	public Collection getChartData(String sNodeId, String sTime) {
		Collection col = new Vector();
		Connection conn = null;
		ResultSet rs = null;
		StationaryDoorActivityBean stBean = new StationaryDoorActivityBean();
		try {
			double fTime = Utils.getAlertTime(sTime);
			// logger.info("fTime: " + fTime);
			conn = DbConnection.getConnection();
			PreparedStatement pSelect = conn
					.prepareStatement(SqlConstants.GetChartData_Transit_Door);
			// pSelect.setString(1, sNodeId);
			pSelect.setDouble(1, fTime);
			rs = pSelect.executeQuery();
			while (rs.next()) {
				// logger.info(rs.getString(1) + " " + rs.getString(2) + " "
				// +rs.getString(3) + " " + rs.getString(4));
				stBean = new StationaryDoorActivityBean();
				stBean.setGW_client_id(rs.getString(1));
				stBean.setND_client_id(rs.getString(2));
				stBean.setTimestamp(rs.getString(3));
				stBean.setDoorActivity(rs.getString(4));
				col.add(stBean);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return col;
	}

}
