package com.demo2.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.TreeMap;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.demo2.bean.StationaryTemperatureBean;
import com.demo2.bean.TransitTemperatureBean;
import com.demo2.util.DbConnection;
import com.demo2.util.SqlConstants;
import com.demo2.util.Utils;

public class TransitTemperatureDao {

	static final Logger logger = LoggerFactory
			.getLogger(TransitTemperatureDao.class);

	public TreeMap getChartData(String sVehicleId, String sTime) {
		Collection col = new Vector();
		Connection conn = null;
		ResultSet rs = null;
		TransitTemperatureBean stBean = new TransitTemperatureBean();
		TreeMap tm = new TreeMap();
		try {
			System.out.println("sTime: " + sTime);
			double fTime = Utils.getAlertTime(sTime);
			// logger.info("fTime: " + fTime);
			conn = DbConnection.getConnection();
			PreparedStatement pSelect = conn
					.prepareStatement(SqlConstants.TemperatureTrends);
			pSelect.setString(1, sVehicleId);
			rs = pSelect.executeQuery();
			while (rs.next()) {
				// logger.info(rs.getString(1) + " " + rs.getString(2) + " "
				// +rs.getString(3) + " " + rs.getString(4));
				stBean = new TransitTemperatureBean();
				//stBean.setGW_client_id(rs.getString(1));
				stBean.setND_client_id(rs.getString(1));
				stBean.setTimestamp(rs.getString(2));
				stBean.setTemperature(rs.getString(3));
				if (tm.containsKey(stBean.getND_client_id())) {
					col = (Collection) tm.get(stBean.getND_client_id());
					col.add(stBean);
				} else {
					col.add(stBean);
					tm.put(stBean.getND_client_id(), col);
				}
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return tm;
	}

	public Collection selectAllStationaryTemperatureRecords() {
		Collection col = new Vector();
		Connection conn = null;
		ResultSet rs = null;
		StationaryTemperatureBean stBean = new StationaryTemperatureBean();
		Statement statement = null;
		try {
			conn = DbConnection.getConnection();
			try {
				statement = conn.createStatement();
				rs = statement
						.executeQuery(SqlConstants.SelectAllStationaryTemperatureRecords);
			} catch (Exception e) {
				e.printStackTrace();
			}
			// logger.info("start");
			while (rs.next()) {
				// logger.info(rs.getString(1) + " " + rs.getString(2) + " "
				// +rs.getString(3) + " " + rs.getString(4));
				stBean = new StationaryTemperatureBean();
				stBean.setGW_client_id(rs.getString(1));
				stBean.setND_client_id(rs.getString(2));
				stBean.setTimestamp(rs.getString(3));
				stBean.setTemperature(rs.getString(4));
				col.add(stBean);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return col;
	}

	public Collection getChartData(String sGWClientId, String sNDClientId,
			String sTime) {
		Collection col = new Vector();
		Connection conn = null;
		ResultSet rs = null;
		StationaryTemperatureBean stBean = new StationaryTemperatureBean();
		try {
			double fTime = Utils.getAlertTime(sTime);
			// logger.info("fTime: " + fTime);
			conn = DbConnection.getConnection();
			PreparedStatement pSelect = conn
					.prepareStatement(SqlConstants.GetChartData_Temperature_WithClient);
			pSelect.setString(1, sGWClientId);
			pSelect.setString(2, sNDClientId);
			pSelect.setDouble(3, fTime);
			// logger.info("1: " + System.currentTimeMillis());
			rs = pSelect.executeQuery();
			while (rs.next()) {
				// logger.info(rs.getString(1) + " " + rs.getString(2) + " "
				// +rs.getString(3) + " " + rs.getString(4));
				stBean = new StationaryTemperatureBean();
				stBean.setGW_client_id(sGWClientId);
				stBean.setND_client_id(sNDClientId);
				stBean.setTimestamp(rs.getString(3));
				stBean.setTemperature(rs.getString(4));
				col.add(stBean);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return col;
	}

}
