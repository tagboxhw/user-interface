package com.demo2.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.TreeMap;
import java.util.Vector;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.demo2.bean.AlertWorkflowBean;
import com.demo2.bean.AllLocationDataBean;
import com.demo2.bean.AllVehicleDataBean;
import com.demo2.bean.TransitDriverMapBean;
import com.demo2.bean.TransitLatLongBean;
import com.demo2.bean.TransitNodeMapBean;
import com.demo2.bean.TransitVehicleMapBean;
import com.demo2.bean.ZoneBean;
import com.demo2.util.Constants;
import com.demo2.util.DbConnection;
import com.demo2.util.SqlConstants;
import com.demo2.util.Utils;

public class TransitDao {
	private Client client; 
	static final Logger logger = LoggerFactory.getLogger(TransitDao.class);
	public static Collection<AllVehicleDataBean> colAllVehicles = new Vector<AllVehicleDataBean>();
	
	private void init(){ 
	      this.client = ClientBuilder.newClient(); 
	   }  
	
	//REST call to get all Vehicles data
	public Collection<AllVehicleDataBean> getAllVehiclesData() {
		init();
		GenericType<Collection<AllVehicleDataBean>> clmBean1 = new GenericType<Collection<AllVehicleDataBean>>(){};
		Collection<AllVehicleDataBean> col = client 
		         .target(Constants.REST_SERVICE_URL + "TransitService/allvehicles") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(clmBean1);
		colAllVehicles = col;
		return colAllVehicles;
	}
	

	public TransitVehicleMapBean getDetailsForVehicleId(String sVehicleId) {
		Connection conn = null;
		PreparedStatement psSelect = null;
		ResultSet rs = null;
		TransitVehicleMapBean tv = new TransitVehicleMapBean();
		try {
			conn = DbConnection.getConnection();

			psSelect = conn
					.prepareStatement(SqlConstants.NewGetDetailsForVehicleId);
			psSelect.setString(1, sVehicleId);
			try {
				rs = psSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}
			while (rs.next()) {
				if (rs.getString(1) == null || rs.getString(1).equals("")){
					tv.setVehicle_ID("");
				} else {
					tv.setVehicle_ID(rs.getString(1));
				}
				if (rs.getString(2) == null || rs.getString(2).equals("")){
					tv.setVehicle_Status("");
				} else {
					tv.setVehicle_Status(rs.getString(2));
				}
				if (rs.getString(3) == null || rs.getString(3).equals("")){
					tv.setAssigned_Date("");
				} else {
					tv.setAssigned_Date(rs.getString(3));
				}
				if (rs.getString(5) == null || rs.getString(5).equals("")){
					tv.setDriver_DL_ID("");
				} else {
					tv.setDriver_DL_ID(rs.getString(5));
				}
				if (rs.getString(6) == null || rs.getString(6).equals("")){
					tv.setRoute_Type("");
				} else {
					tv.setRoute_Type(rs.getString(6));
				}
				if (rs.getString(7) == null || rs.getString(7).equals("")){
					tv.setSource("");
				} else {
					tv.setSource(rs.getString(7));
				}
				if (rs.getString(8) == null || rs.getString(8).equals("")){
					tv.setDestination("");
				} else {
					tv.setDestination(rs.getString(8));
				}
				
				if (rs.getString(9) == null || rs.getString(9).equals("")
						|| rs.getString(9).equals("NULL")) {
					tv.setPlanned_Stops("");
				} else {
					tv.setPlanned_Stops(rs.getString(9));
				}
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (psSelect != null)
					psSelect.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return tv;
	}

	public LinkedHashMap getMapDetailsVehicleId(String sVehicleId,
			String sAlertType, String sTime) {
		Connection conn = null;
		PreparedStatement psSelect = null;
		ResultSet rs = null;
		String sKey = "";
		String sValue = "";
		String selectSql = "";
		String num = "";
		TransitVehicleMapBean tv = new TransitVehicleMapBean();
		TransitLatLongBean tv1 = new TransitLatLongBean();
		AlertsWorkflowDao aw = new AlertsWorkflowDao();
		LinkedHashMap lhm = new LinkedHashMap();
		try {
			tv = getDetailsForVehicleId(sVehicleId);
			sKey = tv.getSource() + "," + tv.getDestination() + ","
					+ tv.getPlanned_Stops();
			System.out.println("Source: " + tv.getSource() + "Dest: " + tv.getDestination() + "Planned: " + tv.getPlanned_Stops());
			String[] sLocs = sKey.split(",");
			lhm = aw.getAllAlertsForVehicleId(sVehicleId, sAlertType, sTime);
			conn = DbConnection.getConnection();
			Iterator iter = lhm.keySet().iterator();
			AlertWorkflowBean ab = new AlertWorkflowBean();
			while (iter.hasNext()) {
				sValue = (String) iter.next();
				ab = (AlertWorkflowBean) lhm.get(sValue);
				ab.setRoute_Type(Constants.LOCAL_RUNS_DETAIL);
			}
			for (int i = 0; i < sLocs.length; i++) {
				num += "?,";
			}
			num = num.substring(0, num.length() - 1);
			
			selectSql = "select id, name, ST_X(coordinates::geometry) as lat, ST_Y(coordinates::geometry) as long from map.location where id in ("
					+ num + ")";
			psSelect = conn.prepareStatement(selectSql);
			for (int i = 0; i < sLocs.length; i++) {
				psSelect.setString(i + 1, sLocs[i]);
				System.out.println("sLocs[i]: " + sLocs[i]);
			}
			try {
				rs = psSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}
			HashMap hm = new HashMap();
			TransitLatLongBean tl = new TransitLatLongBean();
			while (rs.next()) {
				tl = new TransitLatLongBean();
				tl.setGW_client_id(rs.getString(1));
				tl.setTimestamp(rs.getString(2));
				tl.setLatitude(rs.getString(3));
				tl.setLongitude(rs.getString(4));
				hm.put(rs.getString(1), tl);
			}
			for (int i = 0; i < sLocs.length; i++) {
				if (i == 0) {
					tv1 = (TransitLatLongBean) hm.get(sLocs[i]);
					ab = new AlertWorkflowBean();
					ab.setAlert_Location(tv.getVehicle_ID());
					ab.setRoute_Type(Constants.LOCAL_RUNS_DETAIL);
					ab.setCurrent_Location_Name(tv1.getTimestamp());
					ab.setCurrent_Location_Lat(tv1.getLatitude());
					ab.setCurrent_Location_Long(tv1.getLongitude());
					ab.setAlert_Type("Source");
					lhm.put("Source", ab);
				} else if (i == 1) {
					tv1 = (TransitLatLongBean) hm.get(sLocs[i]);
					ab = new AlertWorkflowBean();
					ab.setAlert_Location(tv.getVehicle_ID());
					ab.setRoute_Type(Constants.LOCAL_RUNS_DETAIL);
					ab.setCurrent_Location_Name(tv1.getTimestamp());
					ab.setCurrent_Location_Lat(tv1.getLatitude());
					ab.setCurrent_Location_Long(tv1.getLongitude());
					ab.setAlert_Type("Destination");
					lhm.put("Destination", ab);
				} else {
					tv1 = (TransitLatLongBean) hm.get(sLocs[i]);
					ab = new AlertWorkflowBean();
					ab.setAlert_Location(tv.getVehicle_ID());
					ab.setRoute_Type(Constants.LOCAL_RUNS_DETAIL);
					ab.setCurrent_Location_Name(tv1.getTimestamp());
					ab.setCurrent_Location_Lat(tv1.getLatitude());
					ab.setCurrent_Location_Long(tv1.getLongitude());
					ab.setAlert_Type("Planned");
					num = "Planned" + i;
					lhm.put(num, ab);
				}
			}
			psSelect = conn.prepareStatement(SqlConstants.NewGetCurrentLocation);
			psSelect.setString(1, sVehicleId);
			try {
				rs = psSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}
			while (rs.next()) {
				ab = new AlertWorkflowBean();
				ab.setAlert_Location(sVehicleId);
				ab.setRoute_Type(Constants.LOCAL_RUNS_DETAIL);
				ab.setCurrent_Location_Lat(rs.getString(2));
				ab.setCurrent_Location_Long(rs.getString(3));
				ab.setAlert_Type("Current");
				lhm.put("Current", ab);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (psSelect != null)
					psSelect.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return lhm;
	}

	public TransitDriverMapBean getDriverDetails(String sDriverId) {
		Connection conn = null;
		PreparedStatement psSelect = null;
		ResultSet rs = null;
		TransitDriverMapBean tv = new TransitDriverMapBean();
		try {
			conn = DbConnection.getConnection();

			psSelect = conn.prepareStatement(SqlConstants.NewGetDriverDetails);
			psSelect.setString(1, sDriverId);
			try {
				rs = psSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}
			while (rs.next()) {
				tv.setDriver_DL_ID(rs.getString(2));
				tv.setDriver_Name(rs.getString(3));
				tv.setDriver_Phone_Num(rs.getString(4));
				// tv.setDriver_Address(rs.getString(4));
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (psSelect != null)
					psSelect.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return tv;
	}


	public List selectSourceVehiclesLocationCity() {
		Connection conn = null;
		PreparedStatement psSelect = null;
		ResultSet rs = null;
		Statement statement = null;
		List<String> al = new ArrayList<String>();
		try {
			conn = DbConnection.getConnection();

			try {
				statement = conn.createStatement();
			} catch (Exception e) {
				e.printStackTrace();
			}
			rs = statement
					.executeQuery(SqlConstants.SelectSourceVehiclesLocationCity);
			while (rs.next()) {
				al.add(rs.getString(1));
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (psSelect != null)
					psSelect.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return al;

	}

	public TreeMap selectSourceVehicleLocationNameId(String sSourceCityName) {
		Connection conn = null;
		PreparedStatement psSelect = null;
		ResultSet rs = null;
		TreeMap tv = new TreeMap();
		try {
			conn = DbConnection.getConnection();
			psSelect = conn
					.prepareStatement(SqlConstants.SelectSourceVehicleLocationNameId);
			psSelect.setString(1, sSourceCityName);
			try {
				rs = psSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}
			while (rs.next()) {
				tv.put(rs.getString(1), rs.getString(2));
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (psSelect != null)
					psSelect.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return tv;
	}

	public List selectDestinationVehiclesLocationCity(String sSourceLocationId) {
		Connection conn = null;
		PreparedStatement psSelect = null;
		ResultSet rs = null;
		List<String> al = new ArrayList<String>();
		try {
			conn = DbConnection.getConnection();

			psSelect = conn
					.prepareStatement(SqlConstants.SelectDestinationVehiclesLocationCity);
			psSelect.setString(1, sSourceLocationId);
			try {
				rs = psSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}
			while (rs.next()) {
				al.add(rs.getString(1));
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (psSelect != null)
					psSelect.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return al;

	}

	public TreeMap selectDestinationVehicleLocationNameId(
			String sDestinationCityName) {
		Connection conn = null;
		PreparedStatement psSelect = null;
		ResultSet rs = null;
		TreeMap tv = new TreeMap();
		try {
			conn = DbConnection.getConnection();

			psSelect = conn
					.prepareStatement(SqlConstants.SelectDestinationVehicleLocationNameId);
			psSelect.setString(1, sDestinationCityName);
			try {
				rs = psSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}
			while (rs.next()) {
				tv.put(rs.getString(1), rs.getString(2));
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (psSelect != null)
					psSelect.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return tv;
	}

	public List<String> selectVehicleIdForSourceDestination(String sSourceId,
			String sDestinationId) {
		Connection conn = null;
		PreparedStatement psSelect = null;
		ResultSet rs = null;
		List<String> al = new ArrayList<String>();
		try {
			conn = DbConnection.getConnection();

			psSelect = conn
					.prepareStatement(SqlConstants.SelectVehicleIdForSourceDestination);
			psSelect.setString(1, sSourceId);
			psSelect.setString(2, sDestinationId);
			try {
				rs = psSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}
			while (rs.next()) {
				al.add(rs.getString(1));
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (psSelect != null)
					psSelect.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return al;
	}

	public int insertTransitNodeMap(TransitNodeMapBean tn) {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement pSelect = null;
		int iReturn = 0;
		try {
			conn = DbConnection.getConnection();
			pSelect = conn.prepareStatement(SqlConstants.InsertTransitNodeMap);
			pSelect.setString(1, tn.getVehicle_id());
			pSelect.setString(2, tn.getGW_client_id());
			pSelect.setString(3, tn.getND_client_id());
			pSelect.setString(4, tn.getND_device_id());
			pSelect.setString(5, tn.getGW_ND_Pairing());
			pSelect.setString(6, tn.getND_subzone_type());
			pSelect.setString(7, tn.getND_subzone_name());
			pSelect.setString(8, tn.getND_type());
			pSelect.setString(9, tn.getND_status());

			iReturn = pSelect.executeUpdate();
		} catch (SQLException e) {
			logger.error("localized message: " + e.getLocalizedMessage());
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {

				e.printStackTrace();
			}
		}
		return iReturn;
	}

	public int updateTransitNodeMap(TransitNodeMapBean sn) {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement pSelect = null;
		int iReturn = 0;
		try {
			conn = DbConnection.getConnection();
			pSelect = conn.prepareStatement(SqlConstants.UpdateTransitNodeMap);
			pSelect.setString(1, sn.getVehicle_id());
			pSelect.setString(2, sn.getGW_client_id());
			pSelect.setString(3, sn.getND_device_id());
			pSelect.setString(4, sn.getGW_ND_Pairing());
			pSelect.setString(5, sn.getND_subzone_type());
			pSelect.setString(6, sn.getND_subzone_name());
			pSelect.setString(7, sn.getND_type());
			pSelect.setString(8, sn.getND_status());
			pSelect.setString(9, sn.getND_client_id());
			iReturn = pSelect.executeUpdate();
		} catch (SQLException e) {
			logger.error("localized message: " + e.getLocalizedMessage());
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return iReturn;
	}

	public int deleteTransitNodeMap(String sNDClientID) {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement pSelect = null;
		int iReturn = 0;
		try {
			conn = DbConnection.getConnection();
			pSelect = conn.prepareStatement(SqlConstants.DeleteTransitNodeMap);
			pSelect.setString(1, sNDClientID);
			iReturn = pSelect.executeUpdate();
		} catch (SQLException e) {
			logger.error("localized message: " + e.getLocalizedMessage());
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return iReturn;
	}
}
