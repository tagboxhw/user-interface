package com.demo2.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeMap;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.demo2.bean.ClientLocationMapBean;
import com.demo2.bean.TransitVehicleMapBean;
import com.demo2.util.Constants;
import com.demo2.util.DbConnection;
import com.demo2.util.SqlConstants;

public class TransitVehicleMapDao {
	private Client client; 
	ClientLocationMapDao clmDao = new ClientLocationMapDao();
	ClientLocationMapBean clmBean = new ClientLocationMapBean();
	static final Logger logger = LoggerFactory
			.getLogger(TransitVehicleMapDao.class);
	
	private void init(){ 
	      this.client = ClientBuilder.newClient(); 
	   }  
	
	public Collection<TransitVehicleMapBean> selectVehicles() {
		init();
		GenericType<Collection<TransitVehicleMapBean>> tvmBean1 = new GenericType<Collection<TransitVehicleMapBean>>(){};
		Collection<TransitVehicleMapBean> tvmBean = client 
		         .target(Constants.REST_SERVICE_URL + "TransitVehicleMapService/vehiclemap") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(tvmBean1); 
		return tvmBean;
	}
	
	public int updateVehicleDetails(String sVehicleId, String sSource,
			String sDestination, String sDriverId) {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement pSelect = null;
		String updateSql = "";
		int iReturn = 0;
		try {
			TreeMap tmLocations = clmDao.selectLocations();
			conn = DbConnection.getConnection();
			String sSourceTemp = "", sDestTemp = "", sKey = "";
			Set set = tmLocations.keySet();
			Iterator iter = set.iterator();
			while(iter.hasNext()) {
				sKey = (String) iter.next();
				clmBean = (ClientLocationMapBean) tmLocations.get(sKey);
				if(sSource.equals(clmBean.getLocationName())) sSourceTemp = clmBean.getLocation_id();
				if(sDestination.equals(clmBean.getLocationName())) sDestTemp = clmBean.getLocation_id();
			}
			int a = sVehicleId.indexOf("<a");
			sVehicleId = sVehicleId.substring(0, a);
			pSelect = conn.prepareStatement(SqlConstants.UpdateVehicleManagement);
			System.out.println(sSource + ";" + sDestination + ";" + sDriverId + ";" + sVehicleId);
			pSelect.setString(1, sSourceTemp);
			pSelect.setString(2, sDestTemp);
			pSelect.setString(3, sVehicleId);
			//pSelect.setString(4, sVehicleId);
			iReturn = pSelect.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return iReturn;
	}
}
