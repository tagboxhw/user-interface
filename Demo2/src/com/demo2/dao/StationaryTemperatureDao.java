package com.demo2.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.Collection;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.demo2.bean.PlanMapsBean;
import com.demo2.bean.StationaryTemperatureBean;
import com.demo2.util.DbConnection;
import com.demo2.util.SqlConstants;
import com.demo2.util.Utils;

public class StationaryTemperatureDao {

	static final Logger logger = LoggerFactory
			.getLogger(StationaryTemperatureDao.class);

	public Collection getChartData(String sNodeId, String sTime) {
		Collection col = new Vector();
		Connection conn = null;
		ResultSet rs = null;
		StationaryTemperatureBean stBean = new StationaryTemperatureBean();
		try {
			double fTime = Utils.getAlertTime(sTime);
			// logger.info("fTime: " + fTime);
			conn = DbConnection.getConnection();

			PreparedStatement pSelect = conn
					.prepareStatement(SqlConstants.GetChartData_Temperature);
			pSelect.setString(1, sNodeId);
			pSelect.setDouble(2, fTime);
			// logger.info("1: " + System.currentTimeMillis());
			rs = pSelect.executeQuery();
			while (rs.next()) {
				// logger.info(rs.getString(1) + " " + rs.getString(2) + " "
				// +rs.getString(3) + " " + rs.getString(4));
				stBean = new StationaryTemperatureBean();
				stBean.setGW_client_id(rs.getString(1));
				stBean.setND_client_id(sNodeId);
				stBean.setTimestamp(rs.getString(3));
				stBean.setTemperature(rs.getString(4));
				col.add(stBean);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return col;
	}

	public Collection selectAllStationaryTemperatureRecords() {
		Collection col = new Vector();
		Connection conn = null;
		ResultSet rs = null;
		StationaryTemperatureBean stBean = new StationaryTemperatureBean();
		Statement statement = null;
		try {
			conn = DbConnection.getConnection();

			try {
				statement = conn.createStatement();
				rs = statement
						.executeQuery(SqlConstants.SelectAllStationaryTemperatureRecords);
			} catch (Exception e) {
				e.printStackTrace();
			}
			// logger.info("start");
			while (rs.next()) {
				// logger.info(rs.getString(1) + " " + rs.getString(2) + " "
				// +rs.getString(3) + " " + rs.getString(4));
				stBean = new StationaryTemperatureBean();
				stBean.setGW_client_id(rs.getString(1));
				stBean.setND_client_id(rs.getString(2));
				stBean.setTimestamp(rs.getString(3));
				stBean.setTemperature(rs.getString(4));
				col.add(stBean);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return col;
	}

}
