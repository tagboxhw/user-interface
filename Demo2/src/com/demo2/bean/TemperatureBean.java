package com.demo2.bean;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "temperatureBean")
public class TemperatureBean implements Serializable{

	/**
	 * Generated serial version ID
	 */
	private static final long serialVersionUID = -1874474658549948076L;
	
	private String Zone_id;
	private String Node_id;
	private String Timestamp;
	private String Temperature;
	
	public TemperatureBean(){
		super();
	}
	
	public void init() {
		this.Zone_id = "";
		this.Node_id = "";
		this.Timestamp = "";
		this.Temperature = "";
	}
	
	public String toString(){
		String buf = null;
		buf = "Stationary Temperature object: \n";
		buf = buf + "Zone_id: " + Zone_id + "\n";
		buf = buf + "Zone_id: " + Zone_id + "\n";
		buf = buf + "Timestamp: " + Timestamp + "\n";
		buf = buf + "Temperature: " + Temperature + "\n";
		return buf;
	}

	/**
	 * @return the Zone_id
	 */
	public String getZone_Id() {
		return this.Zone_id;
	}

	/**
	 * @param Zone_id the Zone_id to set
	 */
	@XmlElement
	public void setZone_Id(String Zone_id) {
		this.Zone_id = Zone_id;
	}

	/**
	 * @return the Node_id
	 */
	public String getNode_Id() {
		return this.Node_id;
	}

	/**
	 * @param Node_id the Node_id to set
	 */
	@XmlElement
	public void setNode_Id(String Node_id) {
		this.Node_id = Node_id;
	}

	/**
	 * @return the timestamp
	 */
	public String getTimestamp() {
		return this.Timestamp;
	}

	/**
	 * @param timestamp the timestamp to set
	 */
	@XmlElement
	public void setTimestamp(String timestamp) {
		this.Timestamp = timestamp;
	}

	/**
	 * @return the temperature
	 */
	public String getTemperature() {
		return this.Temperature;
	}

	/**
	 * @param temperature the temperature to set
	 */
	@XmlElement
	public void setTemperature(String temperature) {
		this.Temperature = temperature;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	

}
