package com.demo2.bean;

import java.io.Serializable;

public class GatewayLogBean implements Serializable{

	/**
	 * Generated serial version ID
	 */
	private static final long serialVersionUID = -1874474658549948076L;
	
	
	private String GW_Device_ID;
	private String GW_Type;
	private String GW_Timestamp;
	private String GW_Status;
	private String GW_Network;
	private int GW_Battery;
	private int GW_IsPowered;
	
	public GatewayLogBean(){
		super();
	}
	
	public void init() {
		this.GW_Device_ID = "";
		this.GW_Type = "";
		this.GW_Timestamp = "";
		this.GW_Status = "";
		this.GW_Network = "";
		this.GW_Battery = 0;
		this.GW_IsPowered = 0;
	}
	
	public String toString(){
		String buf = null;
		buf = "Gateway Log object: \n";
		buf = buf + "GW_Device_ID: " + GW_Device_ID + "\n";
		buf = buf + "GW_Type: " + GW_Type + "\n";
		buf = buf + "GW_Timestamp: " + GW_Timestamp + "\n";
		buf = buf + "GW_Status: " + GW_Status + "\n";
		buf = buf + "GW_Network: " + GW_Network + "\n";
		buf = buf + "GW_Battery: " + GW_Battery + "\n";
		buf = buf + "GW_IsPowered: " + GW_IsPowered + "\n";
		return buf;
	}

	/**
	 * @return the gW_Device_ID
	 */
	public String getGW_Device_ID() {
		return GW_Device_ID;
	}

	/**
	 * @param gW_Device_ID the gW_Device_ID to set
	 */
	public void setGW_Device_ID(String gW_Device_ID) {
		GW_Device_ID = gW_Device_ID;
	}

	/**
	 * @return the gW_Type
	 */
	public String getGW_Type() {
		return GW_Type;
	}

	/**
	 * @param gW_Type the gW_Type to set
	 */
	public void setGW_Type(String gW_Type) {
		GW_Type = gW_Type;
	}

	/**
	 * @return the gW_Status
	 */
	public String getGW_Status() {
		return GW_Status;
	}

	/**
	 * @param gW_Status the gW_Status to set
	 */
	public void setGW_Status(String gW_Status) {
		GW_Status = gW_Status;
	}

	public String getGW_Timestamp() {
		return GW_Timestamp;
	}

	public void setGW_Timestamp(String gW_Timestamp) {
		GW_Timestamp = gW_Timestamp;
	}

	public String getGW_Network() {
		return GW_Network;
	}

	public void setGW_Network(String gW_Network) {
		GW_Network = gW_Network;
	}

	public int getGW_Battery() {
		return GW_Battery;
	}

	public void setGW_Battery(int gW_Battery) {
		GW_Battery = gW_Battery;
	}

	public int getGW_IsPowered() {
		return GW_IsPowered;
	}

	public void setGW_IsPowered(int gW_IsPowered) {
		GW_IsPowered = gW_IsPowered;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	

}
