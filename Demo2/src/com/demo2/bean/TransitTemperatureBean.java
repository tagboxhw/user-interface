package com.demo2.bean;

import java.io.Serializable;

public class TransitTemperatureBean implements Serializable{

	/**
	 * Generated serial version ID
	 */
	private static final long serialVersionUID = -1874474658549948076L;
	
	private String GW_client_id;
	private String ND_client_id;
	private String Timestamp;
	private String Temperature;
	
	public TransitTemperatureBean(){
		super();
	}
	
	public void init() {
		this.GW_client_id = "";
		this.ND_client_id = "";
		this.Timestamp = "";
		this.Temperature = "";
	}
	
	public String toString(){
		String buf = null;
		buf = "Transit Temperature object: \n";
		buf = buf + "GW_client_id: " + GW_client_id + "\n";
		buf = buf + "GW_client_id: " + GW_client_id + "\n";
		buf = buf + "Timestamp: " + Timestamp + "\n";
		buf = buf + "Temperature: " + Temperature + "\n";
		return buf;
	}

	/**
	 * @return the gW_client_id
	 */
	public String getGW_client_id() {
		return GW_client_id;
	}

	/**
	 * @param gW_client_id the gW_client_id to set
	 */
	public void setGW_client_id(String gW_client_id) {
		GW_client_id = gW_client_id;
	}

	/**
	 * @return the nD_client_id
	 */
	public String getND_client_id() {
		return ND_client_id;
	}

	/**
	 * @param nD_client_id the nD_client_id to set
	 */
	public void setND_client_id(String nD_client_id) {
		ND_client_id = nD_client_id;
	}

	/**
	 * @return the timestamp
	 */
	public String getTimestamp() {
		return Timestamp;
	}

	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(String timestamp) {
		Timestamp = timestamp;
	}

	/**
	 * @return the temperature
	 */
	public String getTemperature() {
		return Temperature;
	}

	/**
	 * @param temperature the temperature to set
	 */
	public void setTemperature(String temperature) {
		Temperature = temperature;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	

}
