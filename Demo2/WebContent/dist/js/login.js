var catString = [];
var locString = [];
$(document).ready(function(){

	
	$(document).keypress(function(e) {
		if(e.keyCode == 13){
			//alert("13 pressed");
			$('.log-btn').click();
		}
	});
	
	
        $('.log-btn').click(function(){
        	var posting = $.post("pages/jsp/GetUserData.jsp",
        			{
        				UserID:$('#Username').val(),
        				Pwd:$('#Password').val(),
        				query:"loginAccess"
        			},
        			function(data,status){
        				//alert(data);
        				xmlDoc = loadXMLString(data);
        				
        				$(xmlDoc).find("row").each(function(){
        					var id = $(this).find("Return").text();
        					if(id == "Success") {
        						window.location = "pages/jsp/login.jsp?uname=" + $('#Username').val();
        						return;
        					} else {
        						$('.log-status').addClass('wrong-entry');
        				           $('.alert').fadeIn(500);
        				           setTimeout( "$('.alert').fadeOut(1500);",3000 );
        					}
        				});
        				
        			});
        	
        	
        });
        $('.form-control').keypress(function(){
            $('.log-status').removeClass('wrong-entry');
        });
        
        $('#tr1').click(function(){
        	$('#tr1_1').show();
        });
        $('#tr2').click(function(){
        	$('#tr2_1').show();
        });
        $('#tr3').click(function(){
        	$('#tr3_1').show();
        });
        $('#a_1').click(function(){
        	$('#role_section').show();
        	$('#contact_section').hide();
        });
        $('#a_2').click(function(){
        	$('#role_section').show();
        	$('#contact_section').hide();
        });
        $('#a_3').click(function(){
        	$('#role_section').show();
        	$('#contact_section').hide();
        });
        $('#cancel-btn').click(function(){
        	$('#role_section').hide();
        	$('#contact_section').show();
        });
        $('#minus_1').click(function(){
        	$('#tr1_1').hide();
        });
        $('#minus_2').click(function(){
        	$('#tr2_1').hide();
        });
        $('#minus_3').click(function(){
        	$('#tr3_1').hide();
        });
    });
  
function UpdateCatString(inputArray) {
	var b = false;
	if(inputArray == null) {
		$('#CatID').html("");
		alert("Please select atleast one Category!");
		return;
	}
	if(inputArray.length == 1){
		catString = [];
		catString.push(inputArray);
		 $('#CatID').html(catString);
		return;
	}
	for(i=0; i< inputArray.length; i++){
		b = false;
		for(j=0; j<catString.length; j++) {
			if(inputArray[i] == catString[j]){
				b = true;
				break;
			}
		}
		if (!b) {
			catString.push(inputArray[i]);
		}
	}
	b = false;
	var k =0;
	for(i=0; i< catString.length; i++){
		b = false;
		for(j=0; j<inputArray.length; j++) {
			if(inputArray[j] == catString[i]){
				b = true;
				break;
			}
		}
		if (!b) {
			k = i;
			break;
		}
	}
	if(!b){
	catString.splice(k,1);
	}
  $('#CatID').html(catString);
}



function UpdateLocString(inputArray) {
	var b = false;
	if(inputArray == null) {
		$('#LocID').html("");
		alert("Please select atleast one Location!");
		return;
	}
	if(inputArray.length == 1){
		locString = [];
		locString.push(inputArray);
		 $('#LocID').html(locString);
		return;
	}
	for(i=0; i< inputArray.length; i++){
		b = false;
		for(j=0; j<locString.length; j++) {
			if(inputArray[i] == locString[j]){
				b = true;
				break;
			}
		}
		if (!b) {
			locString.push(inputArray[i]);
		}
	}
	b = false;
	var k =0;
	for(i=0; i< locString.length; i++){
		b = false;
		for(j=0; j<inputArray.length; j++) {
			if(inputArray[j] == locString[i]){
				b = true;
				break;
			}
		}
		if (!b) {
			k = i;
			break;
		}
	}
	if(!b){
	locString.splice(k,1);
	}
  $('#LocID').html(locString);
}

function loadXMLString(txt) 
{
	if (window.DOMParser)
	{
		parser=new DOMParser();
		xmlDoc=parser.parseFromString(txt,"text/xml");
	}
	else // Internet Explorer
	{
		xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
		xmlDoc.async=false;
		xmlDoc.loadXML(txt); 
	}
	return xmlDoc;
}
  