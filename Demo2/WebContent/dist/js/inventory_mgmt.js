
function edit_row_stationary(no)
{
	
 document.getElementById("edit_button_stationary"+no).style.display="none";
 document.getElementById("save_button_stationary"+no).style.display="block";
 //document.getElementById("cancel_button"+no).style.display="block";
 
 var cust_id=document.getElementById(no+"new_custSelection");
 var zone_id=document.getElementById(no+"_Zone_id_stationary");
 var gw_client_id=document.getElementById(no+"_GW_client_id_stationary");
 var nd_client_id=document.getElementById(no+"_ND_client_id_stationary");
 
 var cust_id_data=cust_id.innerHTML;
 var zone_id_data=zone_id.innerHTML;
 var gw_client_id_data=gw_client_id.innerHTML;
 var nd_client_id_data=nd_client_id.innerHTML;
	
 var select1 = document.getElementById("new_custSelection");
 var a = "<select id='new_custSelection"+no+"' name='new_custSelection"+no + "' class='form-control-no-height'>" + select1.innerHTML + "</select>";
 cust_id.innerHTML = a;
 
 
 var a = gw_client_id_data.split(" ");
 var select1 = document.getElementById("new_type");
 var a1 = "<select id='new_type"+no+"' name='new_type"+no + "' class='form-control-no-height' style='width:200px'>" + select1.innerHTML + "</select>";
 //cust_id.innerHTML = a1;
 
 
 zone_id.innerHTML="<input type='text' class='form-control-no-height' id='zone_id_stationary_text"+no+"' value='"+zone_id_data+"'>";
 gw_client_id.innerHTML="<div class='row'><div class='col-md-3'><input type='text' class='form-control-no-height' id='gw_client_id_stationary_text"+no+"' value='"+a[0]+"'></div><div class='col-md-2'>"+a1+"</div></div>";
 nd_client_id.innerHTML="<input type='text' class='form-control-no-height' id='nd_client_id_stationary_text"+no+"' value='"+nd_client_id_data+"'>";
 $('#new_type'+no).val(a[1]);
 if(document.getElementById("optionsRadios1").checked == false) {
	 document.getElementById("new_custSelection"+no).value=cust_id_data;
	 document.getElementById("new_custSelection"+no).disabled = true;
 } else {
	 $('#new_custSelection'+no).val(cust_id_data);
	 document.getElementById("new_custSelection"+no).disabled = false;
 }
 }

function save_row_stationary(no)
{
	var type_id_val=document.getElementById("new_type"+no).value;
	var cust_id_val=document.getElementById("new_custSelection"+no).value;
 var zone_id_val=document.getElementById("zone_id_stationary_text"+no).value;
 var gw_client_id_val=document.getElementById("gw_client_id_stationary_text"+no).value;
 var nd_client_id_val=document.getElementById("nd_client_id_stationary_text"+no).value;
 
 if(zone_id_val == "") { alert("Please enter a value for Product"); document.getElementById("zone_id_stationary_text"+no).focus(); return;}
 if(gw_client_id_val == "") { alert("Please enter a value for # of units"); document.getElementById("gw_client_id_stationary_text"+no).focus(); return;}
 if(nd_client_id_val == "") { alert("Please enter a value for INR value"); document.getElementById("nd_client_id_stationary_text"+no).focus(); return;}
 
 document.getElementById(no+"new_custSelection").innerHTML=cust_id_val;
 document.getElementById(no+"_Zone_id_stationary").innerHTML=zone_id_val;
 document.getElementById(no+"_GW_client_id_stationary").innerHTML=gw_client_id_val + " " + type_id_val;
 document.getElementById(no+"_ND_client_id_stationary").innerHTML=nd_client_id_val;
 
 document.getElementById("edit_button_stationary"+no).style.display="block";
 document.getElementById("save_button_stationary"+no).style.display="none";
 //document.getElementById("cancel_button"+no).style.display="none";
 
  
}

function delete_row_stationary(no)
{ 
	
	
 document.getElementById("row_stationary_modal"+no).outerHTML="";
}

function add_row_stationary()
{
	var no = document.getElementById("lastValue_stationary1").value;
	var type_id_val=document.getElementById("new_type").value;
	var cust_id_val=document.getElementById("new_custSelection").value;
	var zone_id_val=document.getElementById("new_zone_id_stationary").value;
	 var gw_client_id_val=document.getElementById("new_GW_client_id_stationary").value;
	 var nd_client_id_val=document.getElementById("new_ND_client_id_stationary").value;
	 
	 if(zone_id_val == "") { alert("Please enter a value for Product"); document.getElementById("new_zone_id_stationary").focus(); return;}
	 if(gw_client_id_val == "") { alert("Please enter a value for # of units"); document.getElementById("new_GW_client_id_stationary").focus(); return;}
	 if(nd_client_id_val == "") { alert("Please enter INR value"); document.getElementById("new_ND_client_id_stationary").focus(); return;}
		
 var table=document.getElementById("data_table_stationary1");
 var table_len=(table.rows.length)-1;

 var row = table.insertRow(table_len).outerHTML="<tr id='row_stationary_modal"+no+"'><td id='"+no+"new_custSelection'>"+cust_id_val+"</td><td id='"+no+"_Zone_id_stationary'>"+zone_id_val+"</td>" +
 		"<td id='"+no+"_GW_client_id_stationary'>"+gw_client_id_val+ " " + type_id_val + "</td>" +
 		"<td id='"+no+"_ND_client_id_stationary'>"+nd_client_id_val+"</td>" +
 		"<td><table width='100%' class='no-border'><tr class='no-border'><td class='no-border'><i id='edit_button_stationary"+no+"' class='fa fa-pencil edit fa-color-blue' onclick='edit_row_stationary(" + no + ")'></i></td><td class='no-border'><i id='save_button_stationary"+no+"' class='fa fa-floppy-o save fa-color-blue' onclick='save_row_stationary("+no+")'></i></td><td class='no-border'><!-- --></td><td class='no-border'><i class='fa fa-trash delete alert-text' onclick='delete_row_stationary("+no+")'></i></td></tr></table></td></tr>";
 		
 document.getElementById("new_type").value="Pallets";
 if(document.getElementById("optionsRadios1").checked == false) {
	 document.getElementById("new_custSelection").value=cust_id_val;
	 document.getElementById("new_custSelection").disabled = true;
 } else {
	 document.getElementById("new_custSelection").disabled = false; 
 }

 document.getElementById("new_zone_id_stationary").value = "";
 document.getElementById("new_GW_client_id_stationary").value = "";
 document.getElementById("new_ND_client_id_stationary").value = "";
 document.getElementById("lastValue_stationary1").value = parseInt(no)+1;
}

function loadXMLString(txt) 
{
	if (window.DOMParser)
	{
		parser=new DOMParser();
		xmlDoc=parser.parseFromString(txt,"text/xml");
	}
	else // Internet Explorer
	{
		xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
		xmlDoc.async=false;
		xmlDoc.loadXML(txt); 
	}
	return xmlDoc;
}