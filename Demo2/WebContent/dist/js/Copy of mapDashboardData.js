

var xmlDoc = "";
var directionsService1 = "";
var directionsService2 = "";
var directionsService3 = "";
var MY_MAPTYPE_ID = 'custom_style';
var directionsDisplay1;
var directionsDisplay2;
var directionsDisplay3;
function initialize() {
	//xmlDoc = initializeData();
	
	
	directionsService1 = new google.maps.DirectionsService();
	directionsService2 = new google.maps.DirectionsService();
	directionsService3 = new google.maps.DirectionsService();
	var map1; var map2; var map3;
	var posting = $.post("../../pages/jsp/GetMapDashboardData.jsp",
			{
			
			},
			function(data,status){
				//alert(data);
				xmlDoc = loadXMLString(data);
				if($(xmlDoc).find("rows").length == 0) {
					$("#divResults").html("No records available. Please search again.").show();
					return;
				}
			});
	//alert("xmlDoc: " + xmlDoc);
	posting.done(function() {
	try{
		 directionsDisplay1 = new google.maps.DirectionsRenderer({suppressMarkers:true,
			 polylineOptions: {
			      strokeColor: "red"
			 }
		 });
		 directionsDisplay2 = new google.maps.DirectionsRenderer({suppressMarkers:true,
			 polylineOptions: {
			      strokeColor: "gray"
			    	  }});
		 directionsDisplay3 = new google.maps.DirectionsRenderer({suppressMarkers:true});
		    if (jQuery('#map1').length > 0) {
		        // var locations =
				// jQuery.parseJSON(MapPoints);

		        map1 = new google.maps.Map(document.getElementById('map1'), {
		            mapTypeId: google.maps.MapTypeId.ROADMAP,
		            scrollwheel: false
		        });
		        directionsDisplay1.setMap(map1);
		        infowindow = new google.maps.InfoWindow();
		        flightPlanCoordinates1 = [];
		        bounds1 = new google.maps.LatLngBounds();
		    }
		    if (jQuery('#map2').length > 0) {
		        // var locations =
				// jQuery.parseJSON(MapPoints);

		        map2 = new google.maps.Map(document.getElementById('map2'), {
		            mapTypeId: google.maps.MapTypeId.ROADMAP,
		            scrollwheel: false
		        });
		        directionsDisplay2.setMap(map2);
		        infowindow = new google.maps.InfoWindow();
		        flightPlanCoordinates2 = [];
		        bounds2 = new google.maps.LatLngBounds();
		    }
		    if (jQuery('#map3').length > 0) {
		        // var locations =
				// jQuery.parseJSON(MapPoints);

		        map3 = new google.maps.Map(document.getElementById('map3'), {
		            mapTypeId: google.maps.MapTypeId.ROADMAP,
		            scrollwheel: false
		        });
		        directionsDisplay3.setMap(map3);
		        infowindow = new google.maps.InfoWindow();
		        flightPlanCoordinates3 = [];
		        bounds3 = new google.maps.LatLngBounds();
		    }
		    var image = "http://www.googlemapsmarkers.com/v1/009900";
						//alert($(xmlDoc).find("rows").length);
		    var iLongHaul = 0; var iShortHaul = 0; var iLocalRun = 0;
			$(xmlDoc).find("row").each(function(){
				var sRoute = $(this).find("RouteType").text();
				var sVehicleId = $(this).find("VehicleId").text();
				var sStatus = $(this).find("Status").text();
				var sSource = $(this).find("Source").text();
				var sDest = $(this).find("Destination").text();
				
				var titleMap = "";
				if(sStatus == "Red"){
					image = "../../dist/img/truck_red.png";
	        	} else {
	        		if(sStatus == "Green") {
	        			image = "../../dist/img/truck_green.png";
	        		} else {
	        			image = "http://www.googlemapsmarkers.com/v1/009900";
	        		}
	        	}
				var sLink = "LocalRunsDetail.jsp?VehicleId=" + sVehicleId;
				if(sRoute = "Long Haul") {
					marker = new google.maps.Marker({
	                    position: new google.maps.LatLng(sSource, sDest),
	                    map: map1,
	                    icon: image,
	                    url: sLink
	                });
	        	
					map1.setZoom(17);
					map1.panTo(marker.position);
	        		flightPlanCoordinates1.push(marker.getPosition());
	        	    bounds1.extend(marker.position);
	        	    google.maps.event.addListener(marker, 'click', function(){
	        	    	window.location.href = this.url; 
	        	     });
	                google.maps.event.addListener(marker, 'click', (function (marker, iLongHaul) {
	                    return function () {
	                        infowindow.setContent("Vehicle ID: " + sVehicleId);
	                        infowindow.open(map1, marker);
	                    }
	                })(marker, iLongHaul));
	                iLongHaul++;
				} else if(sRoute == "Short Haul") {
					marker = new google.maps.Marker({
	                    position: new google.maps.LatLng(sSource, sDest),
	                    map: map2,
	                    icon: image,
	                    url: sLink
	                });
	        	
					map2.setZoom(17);
					map2.panTo(marker.position);
	        		flightPlanCoordinates2.push(marker.getPosition());
	        	    bounds2.extend(marker.position);
	        	    google.maps.event.addListener(marker, 'click', function(){
	        	    	window.location.href = this.url; 
	        	     });
	                google.maps.event.addListener(marker, 'click', (function (marker, iShortHaul) {
	                    return function () {
	                        infowindow.setContent("Vehicle ID: " + sVehicleId);
	                        infowindow.open(map2, marker);
	                    }
	                })(marker, iShortHaul));
	                iShortHaul++;
				} else {
					marker = new google.maps.Marker({
	                    position: new google.maps.LatLng(sSource, sDest),
	                    map: map3,
	                    icon: image,
	                    url: sLink
	                });
	        	
					map3.setZoom(17);
					map3.panTo(marker.position);
	        		flightPlanCoordinates3.push(marker.getPosition());
	        	    bounds3.extend(marker.position);
	        	    google.maps.event.addListener(marker, 'click', function(){
	        	    	window.location.href = this.url; 
	        	     });
	                google.maps.event.addListener(marker, 'click', (function (marker, iLocalRun) {
	                    return function () {
	                        infowindow.setContent("Vehicle ID: " + sVehicleId);
	                        infowindow.open(map3, marker);
	                    }
	                })(marker, iLocalRun));
	                iLocalRun++;
				}
			
			});
			map1.fitBounds(bounds1);
			map2.fitBounds(bounds2);
			map3.fitBounds(bounds3);
			
			// directions service
			var start1 = flightPlanCoordinates1[0];
			var end1 = flightPlanCoordinates1[flightPlanCoordinates1.length - 1];
			var waypts1 = [];
			for (var i = 1; i < flightPlanCoordinates1.length - 1; i++) {
				waypts1.push({
					location: flightPlanCoordinates1[i],
				    stopover: true
				});
			}
			
			// directions service
	        var start2 = flightPlanCoordinates2[0];
	        var end2 = flightPlanCoordinates2[flightPlanCoordinates2.length - 1];
	        var waypts2 = [];
	        for (var i = 1; i < flightPlanCoordinates2.length - 1; i++) {
	            waypts2.push({
	                location: flightPlanCoordinates2[i],
	                stopover: true
	            });
	        }
	        
	        var start3 = flightPlanCoordinates3[0];
	        var end3 = flightPlanCoordinates3[flightPlanCoordinates3.length - 1];
	        var waypts3 = [];
	        for (var i = 1; i < flightPlanCoordinates3.length - 1; i++) {
	        	waypts3.push({
	        		location: flightPlanCoordinates3[i],
	        		stopover: true
	        	});
	        }
	        
	        //calcRoute3(start3, end3, waypts3);
	        //calcRoute2(start2, end2, waypts2);
	        //calcRoute1(start1, end1, waypts1);
		} catch (e) {
			alert(e);
		}
	});
	}

function loadXMLString(txt) 
{
	if (window.DOMParser)
	{
		parser=new DOMParser();
		xmlDoc=parser.parseFromString(txt,"text/xml");
	}
	else // Internet Explorer
	{
		xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
		xmlDoc.async=false;
		xmlDoc.loadXML(txt); 
	}
	return xmlDoc;
}

function calcRoute1(start, end, waypts) {
    var request = {
        origin: start,
        destination: end,
        waypoints: waypts,
        optimizeWaypoints: true,
        travelMode: google.maps.TravelMode.DRIVING
    };
    directionsService1.route(request, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay1.setDirections(response);
            var route = response.routes[0];
            /*var summaryPanel = document.getElementById('directions_panel');
            summaryPanel.innerHTML = '';
            // For each route, display summary information.
            for (var i = 0; i < route.legs.length; i++) {
                var routeSegment = i + 1;
                summaryPanel.innerHTML += '<b>Route Segment: ' + routeSegment + '</b><br>';
                summaryPanel.innerHTML += route.legs[i].start_address + ' to ';
                summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
                summaryPanel.innerHTML += route.legs[i].distance.text + '<br><br>';
            }*/
        }
    });
}


function calcRoute2(start, end, waypts) {
    var request = {
        origin: start,
        destination: end,
        waypoints: waypts,
        optimizeWaypoints: true,
        travelMode: google.maps.TravelMode.DRIVING
    };
    directionsService2.route(request, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay2.setDirections(response);
            /*var route = response.routes[0];
            var summaryPanel = document.getElementById('directions_panel');
            //summaryPanel.innerHTML = '';
            // For each route, display summary information.
            for (var i = 0; i < route.legs.length; i++) {
                var routeSegment = i + 1;
                summaryPanel.innerHTML += '<b>Route Segment: ' + routeSegment + '</b><br>';
                summaryPanel.innerHTML += route.legs[i].start_address + ' to ';
                summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
                summaryPanel.innerHTML += route.legs[i].distance.text + '<br><br>';
            }*/
        }
    });
}

function calcRoute3(start, end, waypts) {
    var request = {
        origin: start,
        destination: end,
        waypoints: waypts,
        optimizeWaypoints: true,
        travelMode: google.maps.TravelMode.DRIVING
    };
    directionsService3.route(request, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay3.setDirections(response);
            /*var route = response.routes[0];
            var summaryPanel = document.getElementById('directions_panel');
            //summaryPanel.innerHTML = '';
            // For each route, display summary information.
            for (var i = 0; i < route.legs.length; i++) {
                var routeSegment = i + 1;
                summaryPanel.innerHTML += '<b>Route Segment: ' + routeSegment + '</b><br>';
                summaryPanel.innerHTML += route.legs[i].start_address + ' to ';
                summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
                summaryPanel.innerHTML += route.legs[i].distance.text + '<br><br>';
            }*/
        }
    });
}