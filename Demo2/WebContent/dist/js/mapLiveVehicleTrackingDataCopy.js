

var xmlDoc = "";
var directionsService2 = "";
var MY_MAPTYPE_ID = 'custom_style';
var directionsDisplay2;
function initialize() {
	//xmlDoc = initializeData();
	directionsService2 = new google.maps.DirectionsService();
	var map2;
	var posting = $.post("../../pages/jsp/GetMapDashboardData.jsp",
			{
				alert_type:$('#CatID').val(),
				time: $('#alertsFilter').val(),
				type: "LocalRunDetails",
				route: $('#RouteType').val(),
				vehicleId: $('#VehicleId').val()
			},
			function(data,status){
				//alert(data);
				xmlDoc = loadXMLString(data);
				if($(xmlDoc).find("rows").length == 0) {
					$("#divResults").html("No records available. Please search again.").show();
					return;
				}
			});
	//alert("xmlDoc: " + xmlDoc);
	posting.done(function() {
	try{
		 directionsDisplay2 = new google.maps.DirectionsRenderer({suppressMarkers:true,
			 polylineOptions: {
			      strokeColor: "gray"
			    	  }});
		 
		    if (jQuery('#map2').length > 0) {
		        // var locations =
				// jQuery.parseJSON(MapPoints);

		        map2 = new google.maps.Map(document.getElementById('map2'), {
		            mapTypeId: google.maps.MapTypeId.ROADMAP,
		            scrollwheel: false
		        });
		        directionsDisplay2.setMap(map2);
		        infowindow = new google.maps.InfoWindow();
		        flightPlanCoordinates2 = [];
		        bounds2 = new google.maps.LatLngBounds();
		    }
		    var image = "http://www.googlemapsmarkers.com/v1/009900";
		    var imageBlue = "http://www.googlemapsmarkers.com/v1/00c0ef";
		    var imageBlackSource = "http://www.googlemapsmarkers.com/v1/S/000000/FFFFFF/000000";
		    var imageBlackDest = "http://www.googlemapsmarkers.com/v1/D/000000/FFFFFF/000000";
		    var imageTriangle = "../../dist/img/Triangle.png";
		    var imageCurrent = "../../dist/img/truck_green.png";
						//alert($(xmlDoc).find("rows").length);
		    var iLongHaul = 0;
			$(xmlDoc).find("row").each(function(){
				
				var sRoute = $(this).find("RouteType").text();
				var sVehicleId = $(this).find("VehicleId").text();
				var sStatus = $(this).find("Status").text();
				var sSource = $(this).find("Source").text();
				var sDest = $(this).find("Destination").text();
				var sAlertType = $(this).find("AlertType").text();
				var sTimeStampFrom = $(this).find("TimeStampFrom").text();
				var sTimeStampTo = $(this).find("TimeStampeTo").text();
				var sDuration = $(this).find("Duration").text();
				var sCurrentLocName = $(this).find("CurrentLocName").text();
				var sBreach = $(this).find("Breach").text();
				
				//alert(sRoute + "  " + sVehicleId + "  " + sStatus + "  " + sSource + "  "+ sDest);
				if(sSource == null || sSource == "" || sSource == "null") {
					sSource = "13.011920";
				}
				if(sDest == null || sDest == "" || sDest == "null") {
					sDest = "77.539840";
				}
				var titleMap = "";
				var sLink = "LocalRunsDetail.jsp?VehicleId=" + sVehicleId;
				if(sRoute == "LocalRunsDetail"){
				if(sAlertType == "Source" || sAlertType == "Destination") {
					if(sAlertType == "Source"){
						marker = new google.maps.Marker({
							position: new google.maps.LatLng(sSource, sDest),
							map: map2,
							icon: imageBlackSource
						});
						map2.setZoom(17);
						map2.panTo(marker.position);
						flightPlanCoordinates2.push(marker.getPosition());
						bounds2.extend(marker.position);
						google.maps.event.addListener(marker, 'click', (function (marker, iLongHaul) {
							return function () {
								infowindow.setContent("Source: " + sCurrentLocName);
								infowindow.open(map2, marker);
							}
						})(marker, iLongHaul));
					} else {
						marker = new google.maps.Marker({
							position: new google.maps.LatLng(sSource, sDest),
							map: map2,
							icon: imageBlackDest
						});
						map2.setZoom(17);
						map2.panTo(marker.position);
						flightPlanCoordinates2.push(marker.getPosition());
						bounds2.extend(marker.position);
						google.maps.event.addListener(marker, 'click', (function (marker, iLongHaul) {
							return function () {
								infowindow.setContent("Destination: " + sCurrentLocName);
								infowindow.open(map2, marker);
							}
						})(marker, iLongHaul));
					}
						
					} else if(sAlertType == "Current"){
						marker = new google.maps.Marker({
							position: new google.maps.LatLng(sSource, sDest),
							icon: {
					            path: google.maps.SymbolPath.CIRCLE,
					            scale: 10,
					            strokeColor: '#393'
					          },
							map: map2,
							url: sLink
						});
						map2.setZoom(17);
						map2.panTo(marker.position);
						flightPlanCoordinates2.push(marker.getPosition());
						bounds2.extend(marker.position);
						google.maps.event.addListener(marker, 'click', (function (marker, iLongHaul) {
							return function () {
								infowindow.setContent("Vehicle ID: " + sVehicleId);
								infowindow.open(map2, marker);
							}
						})(marker, iLongHaul));
					} else if(sAlertType == "Planned"){
						marker = new google.maps.Marker({
							position: new google.maps.LatLng(sSource, sDest),
							map: map2,
							icon: imageBlue,
							url: sLink
						});
						map2.setZoom(17);
						map2.panTo(marker.position);
						flightPlanCoordinates2.push(marker.getPosition());
						bounds2.extend(marker.position);
						google.maps.event.addListener(marker, 'click', (function (marker, iLongHaul) {
							return function () {
								infowindow.setContent("Planned Location: " + sCurrentLocName);
								infowindow.open(map2, marker);
							}
						})(marker, iLongHaul));
					} else {
						marker = new google.maps.Marker({
							position: new google.maps.LatLng(sSource, sDest),
							map: map2,
							icon: imageTriangle,
							url: sLink
						});
						map2.setZoom(17);
						map2.panTo(marker.position);
						flightPlanCoordinates2.push(marker.getPosition());
						bounds2.extend(marker.position);
						var sTitle = "";
						if(sAlertType == "Temperature"){
							sTitle = "Temperature has breached " + sBreach + " limit for over " + sDuration + " minutes";
						} else if(sAlertType == "Stoppage"){
							sTitle = "Vehicle " + sVehicleId + " has stopped for over " + sDuration + " minutes";
						} else if(sAlertType == "Door Activity"){
							sTitle = "Door of " + sVehicleId + " has been opened for over " + sDuration + " minutes";
						} else if(sAlertType == "Detour"){
							sTitle = "Vehicle " + sVehicleId + " has taken a different route than planned";
						}
						google.maps.event.addListener(marker, 'click', (function (marker, iLongHaul) {
							return function () {
								infowindow.setContent(sTitle);
								infowindow.open(map2, marker);
							}
						})(marker, iLongHaul));
					}
	                iLongHaul++;
				}
			});
			map2.fitBounds(bounds2);
			
			
			// directions service
	        var start2 = flightPlanCoordinates2[0];
	        var end2 = flightPlanCoordinates2[flightPlanCoordinates2.length - 1];
	        var waypts2 = [];
	        for (var i = 1; i < flightPlanCoordinates2.length - 1; i++) {
	            waypts2.push({
	                location: flightPlanCoordinates2[i],
	                stopover: true
	            });
	        }
	        
	       
	       // calcRoute3(start3, end3, waypts3);
	        calcRoute2(start2, end2, waypts2);
	       // calcRoute1(start1, end1, waypts1);
		} catch (e) {
			alert(e);
		}
	});
	}

function loadXMLString(txt) 
{
	if (window.DOMParser)
	{
		parser=new DOMParser();
		xmlDoc=parser.parseFromString(txt,"text/xml");
	}
	else // Internet Explorer
	{
		xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
		xmlDoc.async=false;
		xmlDoc.loadXML(txt); 
	}
	return xmlDoc;
}

function calcRoute1(start, end, waypts) {
    var request = {
        origin: start,
        destination: end,
        waypoints: waypts,
        optimizeWaypoints: true,
        travelMode: google.maps.TravelMode.DRIVING
    };
    directionsService1.route(request, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay1.setDirections(response);
            var route = response.routes[0];
            /*var summaryPanel = document.getElementById('directions_panel');
            summaryPanel.innerHTML = '';
            // For each route, display summary information.
            for (var i = 0; i < route.legs.length; i++) {
                var routeSegment = i + 1;
                summaryPanel.innerHTML += '<b>Route Segment: ' + routeSegment + '</b><br>';
                summaryPanel.innerHTML += route.legs[i].start_address + ' to ';
                summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
                summaryPanel.innerHTML += route.legs[i].distance.text + '<br><br>';
            }*/
        }
    });
}


function calcRoute2(start, end, waypts) {
    var request = {
        origin: start,
        destination: end,
        waypoints: waypts,
        optimizeWaypoints: true,
        travelMode: google.maps.TravelMode.DRIVING
    };
    directionsService2.route(request, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay2.setDirections(response);
            /*var route = response.routes[0];
            var summaryPanel = document.getElementById('directions_panel');
            //summaryPanel.innerHTML = '';
            // For each route, display summary information.
            for (var i = 0; i < route.legs.length; i++) {
                var routeSegment = i + 1;
                summaryPanel.innerHTML += '<b>Route Segment: ' + routeSegment + '</b><br>';
                summaryPanel.innerHTML += route.legs[i].start_address + ' to ';
                summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
                summaryPanel.innerHTML += route.legs[i].distance.text + '<br><br>';
            }*/
        }
    });
}

function calcRoute3(start, end, waypts) {
    var request = {
        origin: start,
        destination: end,
        waypoints: waypts,
        optimizeWaypoints: true,
        travelMode: google.maps.TravelMode.DRIVING
    };
    directionsService3.route(request, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay3.setDirections(response);
            /*var route = response.routes[0];
            var summaryPanel = document.getElementById('directions_panel');
            //summaryPanel.innerHTML = '';
            // For each route, display summary information.
            for (var i = 0; i < route.legs.length; i++) {
                var routeSegment = i + 1;
                summaryPanel.innerHTML += '<b>Route Segment: ' + routeSegment + '</b><br>';
                summaryPanel.innerHTML += route.legs[i].start_address + ' to ';
                summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
                summaryPanel.innerHTML += route.legs[i].distance.text + '<br><br>';
            }*/
        }
    });
}
