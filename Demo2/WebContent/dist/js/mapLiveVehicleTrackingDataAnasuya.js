

var xmlDoc = "";
var directionsService2 = "";
var MY_MAPTYPE_ID = 'custom_style';
var directionsDisplay2;
function initialize() {
	//xmlDoc = initializeData();
	directionsService2 = new google.maps.DirectionsService();
	var map2;
	var posting = $.post("../../pages/jsp/GetMapDashboardData.jsp",
			{
				alert_type:$('#CatID').val(),
				time: $('#alertsFilter').val(),
				type: "AnasuyaRun",
				route: $('#RouteType').val(),
				vehicleId: $('#VehicleId').val()
			},
			function(data,status){
				//alert(data);
				xmlDoc = loadXMLString(data);
				if($(xmlDoc).find("rows").length == 0) {
					$("#divResults").html("No records available. Please search again.").show();
					return;
				}
			});
	//alert("xmlDoc: " + xmlDoc);
	posting.done(function() {
	try{
		 directionsDisplay2 = new google.maps.DirectionsRenderer({suppressMarkers:true,
			 polylineOptions: {
			      strokeColor: "gray"
			    	  }});
		 
		    if (jQuery('#map2').length > 0) {
		        // var locations =
				// jQuery.parseJSON(MapPoints);

		        map2 = new google.maps.Map(document.getElementById('map2'), {
		            mapTypeId: google.maps.MapTypeId.ROADMAP,
		            scrollwheel: false
		        });
		        directionsDisplay2.setMap(map2);
		        infowindow = new google.maps.InfoWindow();
		        flightPlanCoordinates2 = [];
		        bounds2 = new google.maps.LatLngBounds();
		    }
		    var image = "http://www.googlemapsmarkers.com/v1/009900";
		    var imageBlue = "http://www.googlemapsmarkers.com/v1/00c0ef";
		    var imageBlackSource = "http://www.googlemapsmarkers.com/v1/S/000000/FFFFFF/000000";
		    var imageBlackDest = "http://www.googlemapsmarkers.com/v1/D/000000/FFFFFF/000000";
		    var imageTriangle = "../../dist/img/Triangle.png";
		    var imageCurrent = "../../dist/img/truck_green.png";
						//alert($(xmlDoc).find("rows").length);
		    var iLongHaul = 0;
			$(xmlDoc).find("row").each(function(){
				
				var sSource = $(this).find("Source").text();
				var sDest = $(this).find("Destination").text();
				
				//alert(sRoute + "  " + sVehicleId + "  " + sStatus + "  " + sSource + "  "+ sDest);
				if(sSource == null || sSource == "" || sSource == "null") {
					sSource = "13.011920";
				}
				if(sDest == null || sDest == "" || sDest == "null") {
					sDest = "77.539840";
				}
				var titleMap = "";
						
						flightPlanCoordinates2.push(new google.maps.LatLng(sSource, sDest));
			});
			// directions service
	        var start2 = flightPlanCoordinates2[0];
	        var end2 = flightPlanCoordinates2[flightPlanCoordinates2.length - 1];
	        var waypts2 = [];
	        var tempSize = 0;
	        for (var i = 1; i < flightPlanCoordinates2.length - 1; i++) {
	            waypts2.push({
	                location: flightPlanCoordinates2[i],
	                stopover: true
	            });
	            tempSize++;
	            if(tempSize % 23 == 0){
	            	//end2 = flightPlanCoordinates2[i];
	     	        
	            	calcRoute2(start2, end2, waypts2);
	            	console.log(start2 + " " + end2);
	            	waypts2 = [];
	            	start2 = flightPlanCoordinates2[i];
	            }
	        }
	        
	        
	       // calcRoute3(start3, end3, waypts3);
	        
	       // calcRoute1(start1, end1, waypts1);
		} catch (e) {
			alert(e);
		}
	});
	}

function loadXMLString(txt) 
{
	if (window.DOMParser)
	{
		parser=new DOMParser();
		xmlDoc=parser.parseFromString(txt,"text/xml");
	}
	else // Internet Explorer
	{
		xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
		xmlDoc.async=false;
		xmlDoc.loadXML(txt); 
	}
	return xmlDoc;
}

function calcRoute1(start, end, waypts) {
    var request = {
        origin: start,
        destination: end,
        waypoints: waypts,
        optimizeWaypoints: true,
        travelMode: google.maps.TravelMode.DRIVING
    };
    directionsService1.route(request, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay1.setDirections(response);
            var route = response.routes[0];
            /*var summaryPanel = document.getElementById('directions_panel');
            summaryPanel.innerHTML = '';
            // For each route, display summary information.
            for (var i = 0; i < route.legs.length; i++) {
                var routeSegment = i + 1;
                summaryPanel.innerHTML += '<b>Route Segment: ' + routeSegment + '</b><br>';
                summaryPanel.innerHTML += route.legs[i].start_address + ' to ';
                summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
                summaryPanel.innerHTML += route.legs[i].distance.text + '<br><br>';
            }*/
        }
    });
}


function calcRoute2(start, end, waypts) {
    var request = {
        origin: start,
        destination: end,
        waypoints: waypts,
        optimizeWaypoints: true,
        travelMode: google.maps.TravelMode.DRIVING
    };
    directionsService2.route(request, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay2.setDirections(response);
            /*var route = response.routes[0];
            var summaryPanel = document.getElementById('directions_panel');
            //summaryPanel.innerHTML = '';
            // For each route, display summary information.
            for (var i = 0; i < route.legs.length; i++) {
                var routeSegment = i + 1;
                summaryPanel.innerHTML += '<b>Route Segment: ' + routeSegment + '</b><br>';
                summaryPanel.innerHTML += route.legs[i].start_address + ' to ';
                summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
                summaryPanel.innerHTML += route.legs[i].distance.text + '<br><br>';
            }*/
        }
    });
}

function calcRoute3(start, end, waypts) {
    var request = {
        origin: start,
        destination: end,
        waypoints: waypts,
        optimizeWaypoints: true,
        travelMode: google.maps.TravelMode.DRIVING
    };
    directionsService3.route(request, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay3.setDirections(response);
            /*var route = response.routes[0];
            var summaryPanel = document.getElementById('directions_panel');
            //summaryPanel.innerHTML = '';
            // For each route, display summary information.
            for (var i = 0; i < route.legs.length; i++) {
                var routeSegment = i + 1;
                summaryPanel.innerHTML += '<b>Route Segment: ' + routeSegment + '</b><br>';
                summaryPanel.innerHTML += route.legs[i].start_address + ' to ';
                summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
                summaryPanel.innerHTML += route.legs[i].distance.text + '<br><br>';
            }*/
        }
    });
}
