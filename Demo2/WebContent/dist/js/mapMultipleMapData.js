var locations = {
		address:[
		         {
		        	   "gateway_id": "G007",
		        	   "lat": "12.973860",
		        	   "long": "77.641770",
		        	   "DC_Name": "DC1"
		        	 },
		        	 {
		        		 "gateway_id": "G007",
			        	   "lat": "12.983850",
			        	   "long": "77.676620",
			        	   "DC_Name": "DC2"
		        	 },
		        	 {
		        		 "gateway_id": "G007",
			        	   "lat": "12.995870",
			        	   "long": "77.694680",
			        	   "DC_Name": "DC3"
		        	 },
		        	 {
		        		 "gateway_id": "G007",
			        	   "lat": "12.991860",
			        	   "long": "77.714350",
			        	   "DC_Name": "DC4"
		        	 },
		        	 {
		        		 "gateway_id": "G007",
			        	   "lat": "12.988290",
			        	   "long": "77.731430",
			        	   "DC_Name": "DC5"
		        	 }
		        	 ]};

var xmlDoc = "";
var directionsService1 = "";
var directionsService2 = "";
var directionsService3 = "";
var MY_MAPTYPE_ID = 'custom_style';
var directionsDisplay1;
var directionsDisplay2;
var directionsDisplay3;
function initialize() {
	//xmlDoc = initializeData();
	
	
	directionsService1 = new google.maps.DirectionsService();
	directionsService2 = new google.maps.DirectionsService();
	directionsService3 = new google.maps.DirectionsService();
	var map1; var map2; var map3;
		
	//alert("xmlDoc: " + xmlDoc);
	try{
		 directionsDisplay1 = new google.maps.DirectionsRenderer({suppressMarkers:true,
			 polylineOptions: {
			      strokeColor: "red"
			 }
		 });
		 directionsDisplay2 = new google.maps.DirectionsRenderer({suppressMarkers:true,
			 polylineOptions: {
			      strokeColor: "gray"
			    	  }});
		 directionsDisplay3 = new google.maps.DirectionsRenderer({suppressMarkers:true});
		    if (jQuery('#map1').length > 0) {
		        // var locations =
				// jQuery.parseJSON(MapPoints);

		        map1 = new google.maps.Map(document.getElementById('map1'), {
		            mapTypeId: google.maps.MapTypeId.ROADMAP,
		            scrollwheel: false
		        });
		        directionsDisplay1.setMap(map1);
		        infowindow = new google.maps.InfoWindow();
		        flightPlanCoordinates1 = [];
		        bounds1 = new google.maps.LatLngBounds();
		    }
		    if (jQuery('#map2').length > 0) {
		        // var locations =
				// jQuery.parseJSON(MapPoints);

		        map2 = new google.maps.Map(document.getElementById('map2'), {
		            mapTypeId: google.maps.MapTypeId.ROADMAP,
		            scrollwheel: false
		        });
		        directionsDisplay2.setMap(map2);
		        infowindow = new google.maps.InfoWindow();
		        flightPlanCoordinates2 = [];
		        bounds2 = new google.maps.LatLngBounds();
		    }
		    if (jQuery('#map3').length > 0) {
		        // var locations =
				// jQuery.parseJSON(MapPoints);

		        map3 = new google.maps.Map(document.getElementById('map3'), {
		            mapTypeId: google.maps.MapTypeId.ROADMAP,
		            scrollwheel: false
		        });
		        directionsDisplay3.setMap(map3);
		        infowindow = new google.maps.InfoWindow();
		        flightPlanCoordinates3 = [];
		        bounds3 = new google.maps.LatLngBounds();
		    }
		    var image = "http://www.googlemapsmarkers.com/v1/009900";
						//alert($(xmlDoc).find("rows").length);
		    var i = 0;
						
							
							        	
							        
							        for (i = 0; i < locations.address.length; i++) {
							        	if(i%2 == 0){
							        		image = "../../dist/img/truck_red.png";
							        	} else {
							        		image = "../../dist/img/truck_green.png";	
							        	}
										
										//if(haltAlert){
						        		marker = new google.maps.Marker({
						                    position: new google.maps.LatLng(locations.address[i].lat, locations.address[i].long),
						                    map: map1,
						                    icon: image
						                });
						        	//} 
						        	
						        	//google.maps.event.trigger(map,'resize');
							        map1.setZoom(17);
						        	map1.panTo(marker.position);
						        		 flightPlanCoordinates1.push(marker.getPosition());
						        	     bounds1.extend(marker.position);
						        	     
						                 google.maps.event.addListener(marker, 'click', (function (marker, i) {
						                     return function () {
						                         infowindow.setContent("DC Name: " + DC_Name);
						                         infowindow.open(map1, marker);
						                     }
						                 })(marker, i));
						        		    
										};
						map1.fitBounds(bounds1);
				        /*
						 * polyline var flightPath = new google.maps.Polyline({
						 * map: map, path: flightPlanCoordinates, strokeColor:
						 * "#FF0000", strokeOpacity: 1.0, strokeWeight: 2 });
						 */
				        // directions service
				        var start1 = flightPlanCoordinates1[0];
				        var end1 = flightPlanCoordinates1[flightPlanCoordinates1.length - 1];
				        var waypts1 = [];
				        for (var i = 1; i < flightPlanCoordinates1.length - 1; i++) {
				            waypts1.push({
				                location: flightPlanCoordinates1[i],
				                stopover: true
				            });
				        }
				        
				        image = "http://www.googlemapsmarkers.com/v1/FCD9BC";
						for (i = 0; i < locations.address.length; i++) {
					
						//if(haltAlert){
		        		marker = new google.maps.Marker({
		                    position: new google.maps.LatLng(locations.address[i].lat, locations.address[i].long),
		                    map: map2,
		                    icon: image
		                });
		        	//} 
		        	
		        	//google.maps.event.trigger(map,'resize');
			        map2.setZoom(17);
		        	map2.panTo(marker.position);
		        		 flightPlanCoordinates2.push(marker.getPosition());
		        	     bounds2.extend(marker.position);
		        	     
		                 google.maps.event.addListener(marker, 'click', (function (marker, i) {
		                     return function () {
		                         infowindow.setContent("DC Name: " + DC_Name);
		                         infowindow.open(map2, marker);
		                     }
		                 })(marker, i));
		        		    
						};
			map2.fitBounds(bounds2);
	        /*
			 * polyline var flightPath = new google.maps.Polyline({
			 * map: map, path: flightPlanCoordinates, strokeColor:
			 * "#FF0000", strokeOpacity: 1.0, strokeWeight: 2 });
			 */
	        // directions service
	        var start2 = flightPlanCoordinates2[0];
	        var end2 = flightPlanCoordinates2[flightPlanCoordinates2.length - 1];
	        var waypts2 = [];
	        for (var i = 1; i < flightPlanCoordinates2.length - 1; i++) {
	            waypts2.push({
	                location: flightPlanCoordinates2[i],
	                stopover: true
	            });
	        }
	        
	        
	        
	        image = "http://www.googlemapsmarkers.com/v1/FF0000";
			for (i = 0; i < locations.address.length; i++) {
		
			//if(haltAlert){
    		marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations.address[i].lat, locations.address[i].long),
                map: map3,
                icon: image
            });
    	//} 
    	
    	//google.maps.event.trigger(map,'resize');
        map3.setZoom(17);
    	map3.panTo(marker.position);
    		 flightPlanCoordinates3.push(marker.getPosition());
    	     bounds3.extend(marker.position);
    	     
             google.maps.event.addListener(marker, 'click', (function (marker, i) {
                 return function () {
                     infowindow.setContent("DC Name: " + locations.address[i].DC_Name);
                     infowindow.open(map3, marker);
                 }
             })(marker, i));
    		    
			};
map3.fitBounds(bounds3);
/*
 * polyline var flightPath = new google.maps.Polyline({
 * map: map, path: flightPlanCoordinates, strokeColor:
 * "#FF0000", strokeOpacity: 1.0, strokeWeight: 2 });
 */
// directions service
var start3 = flightPlanCoordinates3[0];
var end3 = flightPlanCoordinates3[flightPlanCoordinates3.length - 1];
var waypts3 = [];
for (var i = 1; i < flightPlanCoordinates3.length - 1; i++) {
    waypts3.push({
        location: flightPlanCoordinates3[i],
        stopover: true
    });
}
	        
//calcRoute3(start3, end3, waypts3);
//	        calcRoute2(start2, end2, waypts2);
//	        calcRoute1(start1, end1, waypts1);
	} catch (e) {
			alert(e);
		}
		
	}

function loadXMLString(txt) 
{
	if (window.DOMParser)
	{
		parser=new DOMParser();
		xmlDoc=parser.parseFromString(txt,"text/xml");
	}
	else // Internet Explorer
	{
		xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
		xmlDoc.async=false;
		xmlDoc.loadXML(txt); 
	}
	return xmlDoc;
}

function calcRoute1(start, end, waypts) {
    var request = {
        origin: start,
        destination: end,
        waypoints: waypts,
        optimizeWaypoints: true,
        travelMode: google.maps.TravelMode.DRIVING
    };
    directionsService1.route(request, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay1.setDirections(response);
            var route = response.routes[0];
            /*var summaryPanel = document.getElementById('directions_panel');
            summaryPanel.innerHTML = '';
            // For each route, display summary information.
            for (var i = 0; i < route.legs.length; i++) {
                var routeSegment = i + 1;
                summaryPanel.innerHTML += '<b>Route Segment: ' + routeSegment + '</b><br>';
                summaryPanel.innerHTML += route.legs[i].start_address + ' to ';
                summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
                summaryPanel.innerHTML += route.legs[i].distance.text + '<br><br>';
            }*/
        }
    });
}


function calcRoute2(start, end, waypts) {
    var request = {
        origin: start,
        destination: end,
        waypoints: waypts,
        optimizeWaypoints: true,
        travelMode: google.maps.TravelMode.DRIVING
    };
    directionsService2.route(request, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay2.setDirections(response);
            /*var route = response.routes[0];
            var summaryPanel = document.getElementById('directions_panel');
            //summaryPanel.innerHTML = '';
            // For each route, display summary information.
            for (var i = 0; i < route.legs.length; i++) {
                var routeSegment = i + 1;
                summaryPanel.innerHTML += '<b>Route Segment: ' + routeSegment + '</b><br>';
                summaryPanel.innerHTML += route.legs[i].start_address + ' to ';
                summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
                summaryPanel.innerHTML += route.legs[i].distance.text + '<br><br>';
            }*/
        }
    });
}

function calcRoute3(start, end, waypts) {
    var request = {
        origin: start,
        destination: end,
        waypoints: waypts,
        optimizeWaypoints: true,
        travelMode: google.maps.TravelMode.DRIVING
    };
    directionsService3.route(request, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay3.setDirections(response);
            /*var route = response.routes[0];
            var summaryPanel = document.getElementById('directions_panel');
            //summaryPanel.innerHTML = '';
            // For each route, display summary information.
            for (var i = 0; i < route.legs.length; i++) {
                var routeSegment = i + 1;
                summaryPanel.innerHTML += '<b>Route Segment: ' + routeSegment + '</b><br>';
                summaryPanel.innerHTML += route.legs[i].start_address + ' to ';
                summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
                summaryPanel.innerHTML += route.legs[i].distance.text + '<br><br>';
            }*/
        }
    });
}