var locations = {
		address:[
		         {
		        	   "gateway_id": "G007",
		        	   "lat": "12.973860",
		        	   "long": "77.641770",
		        	   "DC_Name": "DC1"
		        	 },
		        	 {
		        		 "gateway_id": "G007",
			        	   "lat": "12.983850",
			        	   "long": "77.676620",
			        	   "DC_Name": "DC2"
		        	 },
		        	 {
		        		 "gateway_id": "G007",
			        	   "lat": "12.995870",
			        	   "long": "77.694680",
			        	   "DC_Name": "DC3"
		        	 },
		        	 {
		        		 "gateway_id": "G007",
			        	   "lat": "12.991860",
			        	   "long": "77.714350",
			        	   "DC_Name": "DC4"
		        	 },
		        	 {
		        		 "gateway_id": "G007",
			        	   "lat": "12.988290",
			        	   "long": "77.731430",
			        	   "DC_Name": "DC5"
		        	 }
		        	 ]};

var xmlDoc = "";
var directionsService1 = "";
var directionsService2 = "";
var MY_MAPTYPE_ID = 'custom_style';
var directionsDisplay1;
var directionsDisplay2;
function initialize() {
	//xmlDoc = initializeData();
	
	
	directionsService1 = new google.maps.DirectionsService();
	directionsService2 = new google.maps.DirectionsService();
	var map;
		var posting = $.post("../../pages/jsp/GetMapData.jsp",
				{
				
				},
				function(data,status){
					//alert(data);
					xmlDoc = loadXMLString(data);
					if($(xmlDoc).find("rows").length == 0) {
						$("#divResults").html("No records available. Please search again.").show();
						return;
					}
				});
		//return xmlDoc;
		posting.done(function() {
	//alert("xmlDoc: " + xmlDoc);
	try{
		 directionsDisplay1 = new google.maps.DirectionsRenderer({suppressMarkers:true});
		 directionsDisplay2 = new google.maps.DirectionsRenderer({suppressMarkers:true,
			 polylineOptions: {
			      strokeColor: "gray"
			    	  }});
		 
		    if (jQuery('#map').length > 0) {
		        // var locations =
				// jQuery.parseJSON(MapPoints);

		        map = new google.maps.Map(document.getElementById('map'), {
		            mapTypeId: google.maps.MapTypeId.ROADMAP,
		            scrollwheel: false
		        });
		        directionsDisplay1.setMap(map);
		        directionsDisplay2.setMap(map);
		        infowindow = new google.maps.InfoWindow();
		        flightPlanCoordinates = [];
		        flightPlanCoordinates1 = [];
		        bounds = new google.maps.LatLngBounds();
		        bounds1 = new google.maps.LatLngBounds();
		    }
		    var image = "http://www.googlemapsmarkers.com/v1/009900";
						//alert($(xmlDoc).find("rows").length);
		    var i = 0;
						$(xmlDoc).find("row").each(function(){
							var gatewayId = $(this).find("gatewayId").text();
							var deviceId = $(this).find("deviceId").text();
							var deviceTime = $(this).find("deviceTime").text();
							var temp = $(this).find("temp").text();
							var humidity = $(this).find("humidity").text();
							var doorOpen = $(this).find("doorOpen").text();
							var latitude = $(this).find("latitude").text();
							var longitude = $(this).find("longitude").text();
							var haltDuration = $(this).find("haltDuration").text();
							var openDuration = $(this).find("openDuration").text();
							var tempAlert = $(this).find("tempAlert").text();
							var humidityAlert = $(this).find("humidityAlert").text();
							var haltAlert = $(this).find("haltAlert").text();
							var detourAlert = $(this).find("detourAlert").text();
							var doorAlert = $(this).find("doorAlert").text();
							var titleMap = "";
							/*alert("gatewayid:" + gatewayId + " 2: " + deviceId + " 3: " + deviceTime + " 4: "
									+ temp + " 5: " + humidity + "6: " + doorOpen + "7: " + latitude + 
									"8: " + longitude + "9: " + haltDuration + "10: " + openDuration 
									+ "11: " + tempAlert + "12: " + humidityAlert + "13: " + haltAlert + "14: " + detourAlert
									+ "15: " + doorAlert);*/

							//alert(";" + parseFloat(latitude) + ";" + parseFloat(longitude) + ";");
							    
							       // var image =
									// '../../dist/img/truck_red.png';
							
							
							        if(haltAlert == "true"){
							        	image = "http://www.googlemapsmarkers.com/v1/FF0000";
							        } else {
							        	image = "http://www.googlemapsmarkers.com/v1/009900";
							        }
							        
							        
							        	//if(haltAlert){
							        		marker = new google.maps.Marker({
							                    position: new google.maps.LatLng(parseFloat(latitude), parseFloat(longitude)),
							                    map: map,
							                    icon: image
							                });
							        	//} 
							        	
							        	//google.maps.event.trigger(map,'resize');
								        map.setZoom(17);
							        	map.panTo(marker.position);
							        		 flightPlanCoordinates.push(marker.getPosition());
							        	     bounds.extend(marker.position);
							        	     
							                 google.maps.event.addListener(marker, 'click', (function (marker, i) {
							                     return function () {
							                         infowindow.setContent("Timestamp: " + deviceTime + " Duration: " + haltDuration + " minutes");
							                         infowindow.open(map, marker);
							                     }
							                 })(marker, i));
							                 i++;
							        		    
						});
						map.fitBounds(bounds);
				        /*
						 * polyline var flightPath = new google.maps.Polyline({
						 * map: map, path: flightPlanCoordinates, strokeColor:
						 * "#FF0000", strokeOpacity: 1.0, strokeWeight: 2 });
						 */
				        // directions service
				        var start = flightPlanCoordinates[0];
				        var end = flightPlanCoordinates[flightPlanCoordinates.length - 1];
				        var waypts = [];
				        for (var i = 1; i < flightPlanCoordinates.length - 1; i++) {
				            waypts.push({
				                location: flightPlanCoordinates[i],
				                stopover: true
				            });
				        }
				        
				        image = "http://www.googlemapsmarkers.com/v1/FCD9BC";
						for (i = 0; i < locations.address.length; i++) {
					
						//if(haltAlert){
		        		marker = new google.maps.Marker({
		                    position: new google.maps.LatLng(locations.address[i].lat, locations.address[i].long),
		                    map: map,
		                    icon: image
		                });
		        	//} 
		        	
		        	//google.maps.event.trigger(map,'resize');
			        map.setZoom(17);
		        	map.panTo(marker.position);
		        		 flightPlanCoordinates1.push(marker.getPosition());
		        	     bounds1.extend(marker.position);
		        	     
		                 google.maps.event.addListener(marker, 'click', (function (marker, i) {
		                     return function () {
		                         infowindow.setContent("DC Name: " + locations.address[i].DC_Name);
		                         infowindow.open(map, marker);
		                     }
		                 })(marker, i));
		        		    
	};
			map.fitBounds(bounds1);
	        /*
			 * polyline var flightPath = new google.maps.Polyline({
			 * map: map, path: flightPlanCoordinates, strokeColor:
			 * "#FF0000", strokeOpacity: 1.0, strokeWeight: 2 });
			 */
	        // directions service
	        var start1 = flightPlanCoordinates1[0];
	        var end1 = flightPlanCoordinates1[flightPlanCoordinates1.length - 1];
	        var waypts1 = [];
	        for (var i = 1; i < flightPlanCoordinates1.length - 1; i++) {
	            waypts1.push({
	                location: flightPlanCoordinates1[i],
	                stopover: true
	            });
	        }
	        
	        
	        calcRoute2(start1, end1, waypts1);
	        calcRoute1(start, end, waypts);
	} catch (e) {
			alert(e);
		}
		});
	}

function loadXMLString(txt) 
{
	if (window.DOMParser)
	{
		parser=new DOMParser();
		xmlDoc=parser.parseFromString(txt,"text/xml");
	}
	else // Internet Explorer
	{
		xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
		xmlDoc.async=false;
		xmlDoc.loadXML(txt); 
	}
	return xmlDoc;
}

function calcRoute1(start, end, waypts) {
    var request = {
        origin: start,
        destination: end,
        waypoints: waypts,
        optimizeWaypoints: true,
        travelMode: google.maps.TravelMode.DRIVING
    };
    directionsService1.route(request, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay1.setDirections(response);
            var route = response.routes[0];
            /*var summaryPanel = document.getElementById('directions_panel');
            summaryPanel.innerHTML = '';
            // For each route, display summary information.
            for (var i = 0; i < route.legs.length; i++) {
                var routeSegment = i + 1;
                summaryPanel.innerHTML += '<b>Route Segment: ' + routeSegment + '</b><br>';
                summaryPanel.innerHTML += route.legs[i].start_address + ' to ';
                summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
                summaryPanel.innerHTML += route.legs[i].distance.text + '<br><br>';
            }*/
        }
    });
}


function calcRoute2(start, end, waypts) {
    var request = {
        origin: start,
        destination: end,
        waypoints: waypts,
        optimizeWaypoints: true,
        travelMode: google.maps.TravelMode.DRIVING
    };
    directionsService2.route(request, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay2.setDirections(response);
            /*var route = response.routes[0];
            var summaryPanel = document.getElementById('directions_panel');
            //summaryPanel.innerHTML = '';
            // For each route, display summary information.
            for (var i = 0; i < route.legs.length; i++) {
                var routeSegment = i + 1;
                summaryPanel.innerHTML += '<b>Route Segment: ' + routeSegment + '</b><br>';
                summaryPanel.innerHTML += route.legs[i].start_address + ' to ';
                summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
                summaryPanel.innerHTML += route.legs[i].distance.text + '<br><br>';
            }*/
        }
    });
}