var locations = {
		address:[
		         {
		        	   "gateway_id": "G007",
		        	   "device_id": "tg002",
		        	   "device_time": "11/18/16 14:00",
		        	   "temp": "27",
		        	   "humidity": "90",
		        	   "door_open": "1",
		        	   "lat": "14.0117946",
			"long": "77.5400684",
		        	   "halt_duration": "32",
		        	   "open_duration": "17",
		        	   "temp_alert": "0",
		        	   "humidity_alert": "0",
		        	   "halt_alert": "0",
		        	   "detour_alert": "0",
		        	   "door_alert": "1"
		        	 },
		        	 {
		        	   "gateway_id": "G007",
		        	   "device_id": "tg002",
		        	   "device_time": "11/18/16 14:03",
		        	   "temp": "25",
		        	   "humidity": "90",
		        	   "door_open": "1",
		        	   "lat": "13.0117979",
		   "long": "77.5400667",
		        	   "halt_duration": "35",
		        	   "open_duration": "20",
		        	   "temp_alert": "0",
		        	   "humidity_alert": "0",
		        	   "halt_alert": "0",
		        	   "detour_alert": "0",
		        	   "door_alert": "1"
		        	 },
		        	 {
		        	   "gateway_id": "G007",
		        	   "device_id": "tg002",
		        	   "device_time": "11/18/16 14:06",
		        	   "temp": "28",
		        	   "humidity": "90",
		        	   "door_open": "1",
		        	   "lat": "13.011957",
		   "long": "77.5400251",
		        	   "halt_duration": "38",
		        	   "open_duration": "23",
		        	   "temp_alert": "0",
		        	   "humidity_alert": "0",
		        	   "halt_alert": "1",
		        	   "detour_alert": "0",
		        	   "door_alert": "1"
		        	 },
		        	 {
		        	   "gateway_id": "G007",
		        	   "device_id": "tg002",
		        	   "device_time": "11/18/16 14:09",
		        	   "temp": "25",
		        	   "humidity": "90",
		        	   "door_open": "1",
		        	   "lat": "13.264843",
		   "long": "77.720127",
		        	   "halt_duration": "41",
		        	   "open_duration": "26",
		        	   "temp_alert": "0",
		        	   "humidity_alert": "0",
		        	   "halt_alert": "1",
		        	   "detour_alert": "0",
		        	   "door_alert": "1"
		        	 },
		        	 {
		        	   "gateway_id": "G007",
		        	   "device_id": "tg002",
		        	   "device_time": "11/18/16 14:12",
		        	   "temp": "26",
		        	   "humidity": "90",
		        	   "door_open": "1",
		        	   "lat": "13.0118482",
		   "long": "77.5401324",
		        	   "halt_duration": "44",
		        	   "open_duration": "29",
		        	   "temp_alert": "0",
		        	   "humidity_alert": "0",
		        	   "halt_alert": "0",
		        	   "detour_alert": "0",
		        	   "door_alert": "1"
		        	 },
		        	 {
		        	   "gateway_id": "G007",
		        	   "device_id": "tg002",
		        	   "device_time": "11/18/16 14:15",
		        	   "temp": "28",
		        	   "humidity": "90",
		        	   "door_open": "1",
		        	   "lat": "13.0118482",
		   "long": "77.5401324",
		        	   "halt_duration": "47",
		        	   "open_duration": "32",
		        	   "temp_alert": "0",
		        	   "humidity_alert": "0",
		        	   "halt_alert": "0",
		        	   "detour_alert": "0",
		        	   "door_alert": "1"
		        	 },
		        	 {
		        	   "gateway_id": "G007",
		        	   "device_id": "tg002",
		        	   "device_time": "11/18/16 14:18",
		        	   "temp": "26",
		        	   "humidity": "90",
		        	   "door_open": "1",
		        	   "lat": "13.0118482",
		   "long": "77.5401324",
		        	   "halt_duration": "50",
		        	   "open_duration": "35",
		        	   "temp_alert": "0",
		        	   "humidity_alert": "0",
		        	   "halt_alert": "1",
		        	   "detour_alert": "0",
		        	   "door_alert": "1"
		        	 },
		        	 {
		        	   "gateway_id": "G007",
		        	   "device_id": "tg002",
		        	   "device_time": "11/18/16 14:21",
		        	   "temp": "28",
		        	   "humidity": "90",
		        	   "door_open": "1",
		        	   "lat": "13.0118482",
		   "long": "77.5401324",
		        	   "halt_duration": "53",
		        	   "open_duration": "38",
		        	   "temp_alert": "0",
		        	   "humidity_alert": "0",
		        	   "halt_alert": "1",
		        	   "detour_alert": "0",
		        	   "door_alert": "1"
		        	 }
		        	]};

//var MapPoints = '[{"address":{"address":"plac Grzybowski, Warszawa, Polska","lat":"52.2360592","lng":"21.002903599999968"},"title":"Warszawa"},{"address":{"address":"Jana Paw\u0142a II, Warszawa, Polska","lat":"52.2179967","lng":"21.222655600000053"},"title":"Wroc\u0142aw"},{"address":{"address":"Wawelska, Warszawa, Polska","lat":"52.2166692","lng":"20.993677599999955"},"title":"O\u015bwi\u0119cim"}]';

var MY_MAPTYPE_ID = 'custom_style';
var directionsDisplay;
var directionsService = new google.maps.DirectionsService();
var map;

function initialize() {
    directionsDisplay = new google.maps.DirectionsRenderer({suppressMarkers:true});

    if (jQuery('#map').length > 0) {

        //var locations = jQuery.parseJSON(MapPoints);

        map = new google.maps.Map(document.getElementById('map'), {
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scrollwheel: false
        });
        directionsDisplay.setMap(map);
        
        var infowindow = new google.maps.InfoWindow();
        var flightPlanCoordinates = [];
        var bounds = new google.maps.LatLngBounds();
       // var image = '../../dist/img/truck_red.png';
        
        
        for (i = 0; i < locations.address.length; i++) {
        	var titleMap = "Timestamp: " + locations.address[i].device_time + "\nDuration: " + locations.address[i].halt_duration;
        	if(locations.address[i].temp_alert == "1"){
        		
        		marker1 = new google.maps.Marker({
                    position: new google.maps.LatLng(locations.address[i].lat, locations.address[i].long),
                    map: map,
                    title: titleMap
                    
                });
        	} 
        	else {
        		
        		marker1 = new google.maps.Marker({
                    position: new google.maps.LatLng(locations.address[i].lat, locations.address[i].long),
                    map: map,
                    icon: "http://www.googlemapsmarkers.com/v1/009900",
                    title: titleMap
                });
        	}
        	
        		 flightPlanCoordinates.push(marker1.getPosition());
                 bounds.extend(marker1.position);

                 google.maps.event.addListener(marker1, 'click', (function (marker1, i) {
                     return function () {
                         //infowindow.setContent(titleMap);
                         infowindow.open(map, marker1);
                     }
                 })(marker1, i));
        	
        	
           
        }

        map.fitBounds(bounds);
        /* polyline
            var flightPath = new google.maps.Polyline({
                map: map,
                path: flightPlanCoordinates,
                strokeColor: "#FF0000",
                strokeOpacity: 1.0,
                strokeWeight: 2
            });
*/
        // directions service
        var start = flightPlanCoordinates[0];
        var end = flightPlanCoordinates[flightPlanCoordinates.length - 1];
        var waypts = [];
        for (var i = 1; i < flightPlanCoordinates.length - 1; i++) {
            waypts.push({
                location: flightPlanCoordinates[i],
                stopover: true
            });
        }
        calcRoute(start, end, waypts);
            }
}

function calcRoute(start, end, waypts) {
    var request = {
        origin: start,
        destination: end,
        waypoints: waypts,
        optimizeWaypoints: true,
        travelMode: google.maps.TravelMode.DRIVING
    };
    directionsService.route(request, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
            var route = response.routes[0];
            var summaryPanel = document.getElementById('directions_panel');
            summaryPanel.innerHTML = '';
            // For each route, display summary information.
            for (var i = 0; i < route.legs.length; i++) {
                var routeSegment = i + 1;
                summaryPanel.innerHTML += '<b>Route Segment: ' + routeSegment + '</b><br>';
                summaryPanel.innerHTML += route.legs[i].start_address + ' to ';
                summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
                summaryPanel.innerHTML += route.legs[i].distance.text + '<br><br>';
            }
        }
    });
}
google.maps.event.addDomListener(window, 'load', initialize);