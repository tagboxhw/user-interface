

var xmlDoc = "";
var directionsService1 = "";
var MY_MAPTYPE_ID = 'custom_style';
var directionsDisplay1;
function initialize() {
	//xmlDoc = initializeData();
	directionsService1 = new google.maps.DirectionsService();
	var map1;
		var posting = $.post("../../pages/jsp/GetMapData.jsp",
				{
				
				},
				function(data,status){
					//alert(data);
					xmlDoc = loadXMLString(data);
					if($(xmlDoc).find("rows").length == 0) {
						$("#divResults").html("No records available. Please search again.").show();
						return;
					}
				});
		//return xmlDoc;
		posting.done(function() {
	//alert("xmlDoc: " + xmlDoc);
	try{
		 directionsDisplay1 = new google.maps.DirectionsRenderer({suppressMarkers:true
		 });
		     if (jQuery('#map1').length > 0) {
		        // var locations =
				// jQuery.parseJSON(MapPoints);

		        map1 = new google.maps.Map(document.getElementById('map1'), {
		            mapTypeId: google.maps.MapTypeId.ROADMAP,
		            scrollwheel: false
		        });
		        directionsDisplay1.setMap(map1);
		        infowindow = new google.maps.InfoWindow();
		        flightPlanCoordinates1 = [];
		        bounds1 = new google.maps.LatLngBounds();
		    }
		 				$(xmlDoc).find("row").each(function(){
							var gatewayId = $(this).find("gatewayId").text();
							var deviceId = $(this).find("deviceId").text();
							var deviceTime = $(this).find("deviceTime").text();
							var temp = $(this).find("temp").text();
							var humidity = $(this).find("humidity").text();
							var doorOpen = $(this).find("doorOpen").text();
							var latitude = $(this).find("latitude").text();
							var longitude = $(this).find("longitude").text();
							var haltDuration = $(this).find("haltDuration").text();
							var openDuration = $(this).find("openDuration").text();
							var tempAlert = $(this).find("tempAlert").text();
							var humidityAlert = $(this).find("humidityAlert").text();
							var haltAlert = $(this).find("haltAlert").text();
							var detourAlert = $(this).find("detourAlert").text();
							var doorAlert = $(this).find("doorAlert").text();
							var titleMap = "";
							/*alert("gatewayid:" + gatewayId + " 2: " + deviceId + " 3: " + deviceTime + " 4: "
									+ temp + " 5: " + humidity + "6: " + doorOpen + "7: " + latitude + 
									"8: " + longitude + "9: " + haltDuration + "10: " + openDuration 
									+ "11: " + tempAlert + "12: " + humidityAlert + "13: " + haltAlert + "14: " + detourAlert
									+ "15: " + doorAlert);*/

							
							        if(haltAlert == "true"){
							        	image = "../../dist/img/truck_red.png";
							        } else {
							        	image = "../../dist/img/truck_green.png";
							        }
							        
							        
							        	//if(haltAlert){
							        		marker = new google.maps.Marker({
							                    position: new google.maps.LatLng(parseFloat(latitude), parseFloat(longitude)),
							                    map: map1,
							                    icon: image
							                });
							        	//} 
							        	
							        	//google.maps.event.trigger(map,'resize');
								        map1.setZoom(17);
							        	map1.panTo(marker.position);
							        		 flightPlanCoordinates1.push(marker.getPosition());
							        	     bounds1.extend(marker.position);
							        	     
							                 google.maps.event.addListener(marker, 'click', (function (marker, i) {
							                     return function () {
							                         infowindow.setContent("Timestamp: " + deviceTime + " Duration: " + haltDuration + " minutes");
							                         infowindow.open(map1, marker);
							                     }
							                 })(marker, i));
							                 i++;
							        		    
						});
						map1.fitBounds(bounds1);
				        /*
						 * polyline var flightPath = new google.maps.Polyline({
						 * map: map, path: flightPlanCoordinates, strokeColor:
						 * "#FF0000", strokeOpacity: 1.0, strokeWeight: 2 });
						 */
				        // directions service
				        var start1 = flightPlanCoordinates1[0];
				        var end1 = flightPlanCoordinates1[flightPlanCoordinates1.length - 1];
				        var waypts1 = [];
				        for (var i = 1; i < flightPlanCoordinates1.length - 1; i++) {
				            waypts1.push({
				                location: flightPlanCoordinates1[i],
				                stopover: true
				            });
				        }
				        
				     //calcRoute1(start1, end1, waypts1);
	} catch (e) {
			alert(e);
		}
		});
	}

function loadXMLString(txt) 
{
	if (window.DOMParser)
	{
		parser=new DOMParser();
		xmlDoc=parser.parseFromString(txt,"text/xml");
	}
	else // Internet Explorer
	{
		xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
		xmlDoc.async=false;
		xmlDoc.loadXML(txt); 
	}
	return xmlDoc;
}

function calcRoute1(start, end, waypts) {
    var request = {
        origin: start,
        destination: end,
        waypoints: waypts,
        optimizeWaypoints: true,
        travelMode: google.maps.TravelMode.DRIVING
    };
    directionsService1.route(request, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay1.setDirections(response);
            var route = response.routes[0];
            /*var summaryPanel = document.getElementById('directions_panel');
            summaryPanel.innerHTML = '';
            // For each route, display summary information.
            for (var i = 0; i < route.legs.length; i++) {
                var routeSegment = i + 1;
                summaryPanel.innerHTML += '<b>Route Segment: ' + routeSegment + '</b><br>';
                summaryPanel.innerHTML += route.legs[i].start_address + ' to ';
                summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
                summaryPanel.innerHTML += route.legs[i].distance.text + '<br><br>';
            }*/
        }
    });
}
