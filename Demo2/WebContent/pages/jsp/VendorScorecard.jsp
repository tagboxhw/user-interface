<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page errorPage="Error.jsp" %>
<%@ page import="com.demo2.dao.ClientLocationMapDao"%>
<%@ page import="com.demo2.dao.TransitVehicleMapDao"%>
<%@ page import="com.demo2.dao.TransitDriverMapDao"%>
<%@ page import="com.demo2.bean.TransitDriverMapBean"%>
<%@ page import="com.demo2.bean.TransitVehicleMapBean"%>
<%@ page import="com.demo2.bean.ClientLocationMapBean"%>
<%@ page import="com.demo2.util.Utils"%>
<%@ page import="java.util.TreeMap"%>
<%@ page import="java.util.Set"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.util.Collection"%>
<%@ page import="java.util.Set"%>
<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>

<%!
static final Logger logger = LoggerFactory.getLogger("VendorScorecard");
%>
<%
String sUsername = (String) session.getAttribute("username");
if(sUsername == null || sUsername.equals("")) {
	session.setAttribute("callingPage", "VendorScorecard.jsp");
	response.sendRedirect("../../index.html");
}
java.util.Date dt = new java.util.Date();
java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd-MM-yyyy HH:mm");
sdf.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
String currentTime = sdf.format(dt);
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Vendor Scorecard</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Add these below two lines for autocomplete and the javascript function at the end  -->
<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="../../bootstrap/css/bootstrap.css">
   
<!-- Font Awesome -->
<script src="https://use.fontawesome.com/37ba1872d2.js"></script>
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../../dist/css/tagbox.css">
<link href="../../dist/css/select2.css" rel="stylesheet" />
<script src="../../dist/js/select2.js"></script>
<script src="../../dist/js/tagbox.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#VehicleID").select2();
	$("#Source").select2();
	$("#Destination").select2();
	$("#TripStatus").select2();
	$("#Alerts").select2();
	
});
</script>
<STYLE TYPE="text/css">
			table.sortable span.sortarrow {
   				color: blue;
   				text-decoration: none;
			}
					
		</style>
<script src="../../dist/js/sorttable.js"></script>
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
<link href="https://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
  body {
	font-family:  Lato, sans-serif;
	font-weight: 600;
}
</style>
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
	<div class="wrapper">

		<header class="main-header">
			<!-- Logo -->
			<div>
				<a href="#" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
					<span class="logo-mini"><b>Demo</b></span> <!-- logo for regular state and mobile devices -->
					<span class="logo-lg"><b>Demo</b> </span>
				</a>
			</div>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">

				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
					role="button"> <span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
				</a>
				<form class="navbar-form pull-left" role="search">
					<div class="form-group">
						<b style="padding-left: 15px; font-size: 20px;color:#fff;" class="pull-right">Vendor Score Card</b><br> <b style="color:#fff;padding-left: 15px"><%= currentTime %></b>
					</div>
				</form>

				<div class="navbar-custom-menu">
					<img src="../../dist/img/Logo.png" class="logo-class pull-right"
						alt="User Image">
				</div>
			</nav>

		</header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->


				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<br>
				<ul class="sidebar-menu">
					<li class="header">MAIN NAVIGATION</li>
					<li class="treeview"><a href="#"> <i
							class="fa fa-dashboard"></i> <span>Dashboard</span>
					</a>
						<ul class="treeview-menu">
							<li class=""><a href="CMCDashboard.jsp"><i
									class="fa fa-circle-o"></i> CMC</a></li>
							<!-- <li class=""><a href="NewDashboard.jsp"><i
									class="fa fa-circle-o"></i> Vehicle Dashboard</a></li> -->
							<li class=""><a href="TodayColdChainHealth.jsp"><i
									class="fa fa-circle-o"></i> Cold Rooms Summary</a></li>
							<!--  <li><a href="VehiclePlannedRouteMap.jsp"><i class="fa fa-circle-o"></i> Vehicle Planned Route</a></li> -->
							<li class=""><a href="TodayColdChainHealthVehicle.jsp"><i
									class="fa fa-circle-o"></i> Vehicles Summary</a></li>
						</ul></li>
					<li class="treeview"><a href="#"> <i
							class="fa fa-files-o"></i> <span>Alerts</span> 
					</a>
						<ul class="treeview-menu">
							<li><a href="LiveVehicleTracking.jsp"><i
									class="fa fa-circle-o"></i> Live Vehicle Tracking</a></li>
						</ul></li>
					
					<li class="treeview active"><a href="#"> <i
							class="fa fa-database"></i> <span>Admin</span>
					</a>
						<ul class="treeview-menu">
							<li class=""><a href="VehicleManagement.jsp"><i class="fa fa-circle-o"></i>
									Vehicle Management</a></li>
							<li class="active"><a href="VendorScorecard.jsp"><i class="fa fa-circle-o"></i>
									Vehicle Score Card</a></li>
						</ul></li>
						<li class="treeview"><a href="#"> <i
							class="fa fa-bar-chart"></i> <span>Reports</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="ParameterSummary.jsp"><i class="fa fa-circle-o"></i>
									Parameter Summary</a></li>
							<li><a href="AlertsSummary.jsp"><i class="fa fa-circle-o"></i>
									Alerts Summary</a></li>
							<li><a href="AlertsReport.jsp"><i class="fa fa-circle-o"></i>
									Alerts Report</a></li>		
							
						</ul></li>
						<li class="treeview"><a href="#"> <i
							class="fa fa-pie-chart"></i> <span>Analysis</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="BlockTemplate.jsp"><i class="fa fa-circle-o"></i>
									Temperature Profile <br>Analysis</a></li>
							<li><a href="OrganizationTemplate.jsp"><i class="fa fa-circle-o"></i>
									Excursion and Breach- <br>Root Cause Analysis</a></li>
							<li><a href="SankeyTemplate.jsp"><i class="fa fa-circle-o"></i>
									Global Supply <Br>Chain Health</a></li>
						</ul></li>
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			
		<section id="mainSection" class="content">
		<div class="well well-sm white-background" style="height:700px">
							
						<br><br>
			<div class="row">
				<div class="col-md-12">
					<table id="data_table_stationary" class="table table-bordered table-striped table-condensed sortable">
									
									<tr class="info text-center">
									<th width="150">Transporter Name</th>
									<th width="150">Address</th>
									<th width="100">Spend on Vendor (Cr. INR-YTD)</th>
									<th width="100">Share of Total Spend (%)</th>
									<th width="120">Average On time(%)</th>
									<th width="120"># Of Trips YTD</th>
									<th width="100">Total Value Carried (Cr. INR-YTD) </th>
									<th width="100"># Theft Cases (YTD)</th>
									<th width="100"># Damages Cases (YTD)</th>
									</tr>
									
									<tr>
									<td>Kalyani Integrated</td>	<td>Marathalli, Bangalore</td>	<td>2.3</td>	<td>3.74</td>	<td>84</td>	<td>123</td> <td>50</td>	<td>4</td>	<td>5</td></tr>
<tr><td>Platinum Transports</td>	<td>Whitefield, Bangalore</td>	<td>4.9</td>	<td>7.98</td>	<td>86</td><td>170</td> 	<td>32</td>	<td>3</td>	<td>6</td></tr>
<tr><td>E2E Logistics</td>	<td>Chembur, Mumbai</td>	<td>4.3</td>	<td>7.00</td>	<td>86</td><td>145</td> 	<td>49</td>	<td>2</td>	<td>5</td></tr>
<tr><td>Shah Shipping</td>	<td>Nerul, Navi Mumbai</td>	<td>3.2</td>	<td>5.21</td>	<td>86</td><td>200</td> 	<td>57</td>	<td>4</td>	<td>7</td></tr>
<tr><td>Kumar Frieght Forwarders</td>	<td>Rajaji Nagar, Thane</td>	<td>4</td>	<td>6.51</td>	<td>84</td><td>223</td> 	<td>56</td>	<td>7</td>	<td>1</td></tr>
<tr><td>Krishnaveni Transports</td>	<td>Malad, Mumbai</td>	<td>4.2</td>	<td>6.84</td>	<td>81</td><td>184</td> 	<td>33</td>	<td>9</td>	<td>1</td></tr>
<tr><td>Aggarwal Logisitics Solutions</td>	<td>Karol Bagh, Delhi</td>	<td>2.3</td>	<td>3.74</td>	<td>88</td><td>195</td> 	<td>60</td>	<td>8</td>	<td>3</td></tr>
<tr><td>Cosmos Transport Line</td>	<td>Civil Lines, Agra</td>	<td>3.7</td>	<td>6.02</td>	<td>83</td><td>146</td> 	<td>30</td>	<td>7</td>	<td>3</td></tr>
<tr><td>Airavat Corporation</td>	<td>Transport Nagar, Kanpur</td>	<td>3.6</td>	<td>5.86</td>	<td>86</td><td>190</td> 	<td>53</td>	<td>9</td>	<td>3</td></tr>
<tr><td>Eagle Logistics</td>	<td>VIP Road, Amritsar</td>	<td>4.2</td>	<td>6.84</td> <td>82</td><td>110</td> 	<td>35</td>	<td>5</td>	<td>1</td></tr>
<tr><td>Karuna Logistics Solutions</td>	<td>Malleshwaram,Bangalore</td>	<td>4.2</td>	<td>6.84</td>	<td>82</td><td>129</td> 	<td>51</td>	<td>1</td>	<td>4</td></tr>
<tr><td>Durga Transport</td>	<td>Sector 17, Chandigarh</td>	<td>4.9</td>	<td>7.98</td>	<td>87</td><td>167</td> 	<td>49</td>	<td>1</td>	<td>1</td></tr>
<tr><td>Whiteline Express</td>	<td>Indirapuram, Ghaziabad</td>	<td>4.6</td>	<td>7.49</td>	<td>86</td><td>192</td> 	<td>45</td>	<td>1</td>	<td>1</td></tr>
<tr><td>Maharaja Transports</td>	<td>Bada Bazar, Kolkata</td>	<td>3.8</td>	<td>6.18</td>	<td>92</td><td>109</td> 	<td>42</td>	<td>4</td>	<td>5</td></tr>
<tr><td>Venus Integrated</td>	<td>Bellandur, Bangalore</td>	<td>4.1</td>	<td>6.67</td>	<td>90</td><td>81</td> 	<td>52</td>	<td>5</td>	<td>6</td></tr>
<tr><td>Shyam Logistics</td>	<td>Kurla, Mumbai</td>	<td>3.1</td>	<td>5.04</td>	<td>84</td><td>166</td> 	<td>36</td>	<td>1</td>	<td>4</td></tr>
									
									</table>
				</div>
						</div>
									
								</div>
						
		</section>
		</div>
							</div>
		
		<!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0
			</div>
			<strong>Copyright &copy; 2017 <a href="http://tagbox.in">TagBox</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Control Sidebar -->
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->
<!-- ChartJS 1.0.1 -->
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>


	<!-- Bootstrap 3.3.6 -->
	<script src="../../bootstrap/js/bootstrap.min.js"></script>
	<!-- ChartJS 1.0.1 -->
	<!-- AdminLTE App -->
	<script src="../../dist/js/app.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="../../dist/js/demo.js"></script>

	</body>
</html>
