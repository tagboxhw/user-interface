<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@ page errorPage="Error.jsp" %>
<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>

<%!
static final Logger logger = LoggerFactory.getLogger("ParameterSummary");
%>
<%
String sUsername = (String) session.getAttribute("username");
if(sUsername == null || sUsername.equals("")) {
	session.setAttribute("callingPage", "ParameterSummary.jsp");
	response.sendRedirect("../../index.html");
}
java.util.Date dt = new java.util.Date();
java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd-MM-yyyy HH:mm");
sdf.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
String currentTime = sdf.format(dt);
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Parameter Summary</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Add these below two lines for autocomplete and the javascript function at the end  -->
<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="../../bootstrap/css/bootstrap.css">
   
<!-- Font Awesome -->
<script src="https://use.fontawesome.com/37ba1872d2.js"></script>
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../../dist/css/tagbox.css">
<link href="../../dist/css/select2.css" rel="stylesheet" />
<script src="../../dist/js/select2.js"></script>
<script src="../../dist/js/tagbox.js"></script>
<script src="../../dist/js/table2download.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#Destination").select2();
});
</script>
<STYLE TYPE="text/css">
			table.sortable span.sortarrow {
   				color: blue;
   				text-decoration: none;
			}
					
		</style>
<script src="../../dist/js/sorttable.js"></script>
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
<link href="https://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet">

<link rel="stylesheet" href="../../dist/css/bootstrap-switch.css">
<script src="../../dist/js/bootstrap-switch1.js"></script>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
  body {
	font-family:  Lato, sans-serif;
	font-weight: 600;
}
</style>
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
	<div class="wrapper">

		<header class="main-header">
			<!-- Logo -->
			<div>
				<a href="#" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
					<span class="logo-mini"><b>Demo</b></span> <!-- logo for regular state and mobile devices -->
					<span class="logo-lg"><b>Demo</b> </span>
				</a>
			</div>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">

				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
					role="button"> <span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
				</a>
				<form class="navbar-form pull-left" role="search">
					<div class="form-group">
						<b style="padding-left: 15px; font-size: 20px;color:#fff;" class="pull-right">Parameter Summary</b><br> <b style="padding-left: 15px;color:#fff;"><%= currentTime %></b>
					</div>
				</form>

				<div class="navbar-custom-menu">
					<img src="../../dist/img/Logo.png" class="logo-class pull-right"
						alt="User Image">
				</div>
			</nav>

		</header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->


				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<br>
				<ul class="sidebar-menu">
					<li class="header">MAIN NAVIGATION</li>
					<li class="treeview"><a href="#"> <i
							class="fa fa-dashboard"></i> <span>Dashboard</span>
					</a>
						<ul class="treeview-menu">
							<li class=""><a href="CMCDashboard.jsp"><i
									class="fa fa-circle-o"></i> CMC</a></li>
							<!-- <li class=""><a href="NewDashboard.jsp"><i
									class="fa fa-circle-o"></i> Vehicle Dashboard</a></li> -->
							<li class=""><a href="TodayColdChainHealth.jsp"><i
									class="fa fa-circle-o"></i> Cold Rooms Summary</a></li>
							<!--  <li><a href="VehiclePlannedRouteMap.jsp"><i class="fa fa-circle-o"></i> Vehicle Planned Route</a></li> -->
							<li class=""><a href="TodayColdChainHealthVehicle.jsp"><i
									class="fa fa-circle-o"></i> Vehicles Summary</a></li>
						</ul></li>
					<li class="treeview"><a href="#"> <i
							class="fa fa-files-o"></i> <span>Alerts</span> 
					</a>
						<ul class="treeview-menu">
							<li><a href="LiveVehicleTracking.jsp"><i
									class="fa fa-circle-o"></i> Live Vehicle Tracking</a></li>
						</ul></li>
					
					<li class="treeview"><a href="#"> <i
							class="fa fa-database"></i> <span>Admin</span>
					</a>
						<ul class="treeview-menu">
							<li class=""><a href="VehicleManagement.jsp"><i class="fa fa-circle-o"></i>
									Vehicle Management</a></li>
							<li><a href="VendorScorecard.jsp"><i class="fa fa-circle-o"></i>
									Vehicle Score Card</a></li>
						</ul></li>
						<li class="treeview active"><a href="#"> <i
							class="fa fa-bar-chart"></i> <span>Reports</span>
					</a>
						<ul class="treeview-menu">
							<li class="active"><a href="ParameterSummary.jsp"><i class="fa fa-circle-o"></i>
									Parameter Summary</a></li>
							<li><a href="AlertsSummary.jsp"><i class="fa fa-circle-o"></i>
									Alerts Summary</a></li>
							<li><a href="AlertsReport.jsp"><i class="fa fa-circle-o"></i>
									Alerts Report</a></li>		
							
						</ul></li>
						<li class="treeview"><a href="#"> <i
							class="fa fa-pie-chart"></i> <span>Analysis</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="BlockTemplate.jsp"><i class="fa fa-circle-o"></i>
									Temperature Profile <br>Analysis</a></li>
							<li><a href="OrganizationTemplate.jsp"><i class="fa fa-circle-o"></i>
									Excursion and Breach- <br>Root Cause Analysis</a></li>
							<li><a href="SankeyTemplate.jsp"><i class="fa fa-circle-o"></i>
									Global Supply <Br>Chain Health</a></li>
						</ul></li>
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			
		<section id="mainSection" class="content">
		<div class="well well-sm white-background" style="height:500px">
							
						<br>
						<form id="myForm1" name="myForm1" method="post"
											action="ParameterSummary.jsp">
						<div class="row">
					<div class="col-md-offset-3 col-md-2">
					<label><b>View As</b></label>&nbsp;&nbsp;<input name="my-checkbox" id="my-checkbox" type="checkbox" data-size="mini" onchange="ViewChange();" checked>
					</div>
						<div class="col-md-3">
							<label><b>Parameter</b></label>&nbsp;&nbsp;
					<select id="Destination" name="Destination" class="form-control-no-background pull-center">
					<option>Temperature</option>
					<option>Humidity</option>
					<option>Door Activity</option>
					<option>Detour</option>
					</select>
						</div>
						<div class="col-md-3">
								<button type="submit" class="btn btn-success">GO</button>&nbsp;&nbsp;
								<button type="button" class="btn btn-success" onclick="resetAll();">RESET</button>
								</div>
						
					</div>
					<br>
					<div id="listView" style="display:none;" class="text-center">
					<div class="row">
					<div class="col-md-offset-9 col-md-3">
						<span id="csvLink"></span>&nbsp;&nbsp;&nbsp;&nbsp;<span id="xlsLink"></span>
					</div>
					
					</div>
					<br>
					<div class="scroll-area-table">
					<table id="data_table_stationary" class="table table-fixed sortable table-bordered table-striped table-condensed">
									<thead>
									<tr class="info text-center">
									<th class="text-center">Location</th>
									<th class="text-center">Cold Room</th>
									<th class="text-center">Max</th>
									<th class="text-center">Min</th>
									<th class="text-center">Average</th>
									<th class="text-center">Volatility</th>
									<th class="text-center">Excursion</th>
									</tr>
									</thead>
									<tbody>
									<tr><td>Bengaluru MU</td>	<td>Ground Floor</td>	<td>8.7</td>	<td>2.3</td>	<td>3.9</td>	<td>12%</td> <td>12</td></tr>
									<tr><td>Bengaluru DC</td>	<td>DC Cold Room</td>	<td>15.3</td>	<td>5.7</td>	<td>5.5</td>	<td>21%</td> <td>17</td></tr>
									<tr><td>Chennai MU</td>	<td>Dispatch</td>	<td>9.5</td>	<td>2.9</td>	<td>1.8</td>	<td>7%</td> <td>9</td></tr>
									<tr><td>Chennai DC</td>	<td>DC Cold Room</td>	<td>11.9</td>	<td>1.6</td>	<td>4.2</td>	<td>18%</td> <td>13</td></tr>
									<tr><td>Hyderabad MU</td>	<td>Storage</td>	<td>11.5</td>	<td>3.1</td>	<td>2.7</td>	<td>9%</td> <td>6</td></tr>
									<tr><td>Hyderabad DC</td>	<td>DC Cold Room</td>	<td>15.3</td>	<td>1.8</td>	<td>6.4</td>	<td>19%</td> <td>21</td></tr>
									<tr><td>Bengaluru MU</td>	<td>Ground Floor</td>	<td>8.7</td>	<td>2.3</td>	<td>3.9</td>	<td>12%</td> <td>12</td></tr>
									<tr><td>Bengaluru DC</td>	<td>DC Cold Room</td>	<td>15.3</td>	<td>5.7</td>	<td>5.5</td>	<td>21%</td> <td>17</td></tr>
									<tr><td>Chennai MU</td>	<td>Dispatch</td>	<td>9.5</td>	<td>2.9</td>	<td>1.8</td>	<td>7%</td> <td>9</td></tr>
									<tr><td>Chennai DC</td>	<td>DC Cold Room</td>	<td>11.9</td>	<td>1.6</td>	<td>4.2</td>	<td>18%</td> <td>13</td></tr>
									<tr><td>Hyderabad MU</td>	<td>Storage</td>	<td>11.5</td>	<td>3.1</td>	<td>2.7</td>	<td>9%</td> <td>6</td></tr>
									<tr><td>Hyderabad DC</td>	<td>DC Cold Room</td>	<td>15.3</td>	<td>1.8</td>	<td>6.4</td>	<td>19%</td> <td>21</td></tr>
									<tr><td>Bengaluru MU</td>	<td>Ground Floor</td>	<td>8.7</td>	<td>2.3</td>	<td>3.9</td>	<td>12%</td> <td>12</td></tr>
									<tr><td>Bengaluru DC</td>	<td>DC Cold Room</td>	<td>15.3</td>	<td>5.7</td>	<td>5.5</td>	<td>21%</td> <td>17</td></tr>
									<tr><td>Chennai MU</td>	<td>Dispatch</td>	<td>9.5</td>	<td>2.9</td>	<td>1.8</td>	<td>7%</td> <td>9</td></tr>
									<tr><td>Chennai DC</td>	<td>DC Cold Room</td>	<td>11.9</td>	<td>1.6</td>	<td>4.2</td>	<td>18%</td> <td>13</td></tr>
									<tr><td>Hyderabad MU</td>	<td>Storage</td>	<td>11.5</td>	<td>3.1</td>	<td>2.7</td>	<td>9%</td> <td>6</td></tr>
									<tr><td>Hyderabad DC</td>	<td>DC Cold Room</td>	<td>15.3</td>	<td>1.8</td>	<td>6.4</td>	<td>19%</td> <td>21</td></tr>
									</tbody></table>
									</div>
									</div>
									<br>
									<div id="mapView" class="text-center">
						
						<div class="row"><div class="col-md-offset-3 col-md-6">
						<u class="pull-right"><a id="link2" download="ChartJpg.jpg">Save As JPG</a></u><br><br>
						<div class="box box-danger">
							<div class="box-header with-border">
														<h3 class="box-title">&nbsp;Relative Temperature</h3>
														<div class="box-tools pull-right">
															<button type="button" class="btn btn-box-tool"
																data-widget="collapse">
																<i class="fa fa-minus"></i>
															</button>
															<button type="button" class="btn btn-box-tool"
																data-widget="remove">
																<i class="fa fa-times"></i>
															</button>
														</div>
													</div>
													<div class="box-body">
														
														<div class="chart">
															<canvas id="lineChart2"
																style="height: 270px; width: 100px"></canvas>
														</div>
														
													</div>
												</div>
												</div></div>
						</div>	
						
						</form>
									
						
						
						
								</div>
						
		</section>
		</div>
							</div>
		
		<!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0
			</div>
			<strong>Copyright &copy; 2017 <a href="http://tagbox.in">TagBox</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Control Sidebar -->
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->
<!-- ChartJS 1.0.1 -->
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>


	<!-- Bootstrap 3.3.6 -->
	<script src="../../bootstrap/js/bootstrap.min.js"></script>
	<!-- ChartJS 1.0.1 -->
	<!-- AdminLTE App -->
	<script src="../../dist/js/app.min.js"></script>
	<script src="../../plugins/chartjs/Chart.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="../../dist/js/demo.js"></script>
	
	<script type="text/javascript">
	jQuery( document ).ready(function() {
	    jQuery( "#data_table_stationary" ).table_download({
	        format: "xls",
	        separator: ",",
	        filename: "download",
	        linkname: "Export To XLS",
	        quotes: "\"",
	        linkid: "xlsLink"
	    });
	    
	    jQuery( "#data_table_stationary" ).table_download({
	        format: "csv",
	        separator: "-",
	        filename: "download",
	        linkname: "Export To CSV",
	        quotes: "\"",
	        linkid: "csvLink"
	    });  
	     
	    
	});



	function ViewChange() {
		var a = (document.getElementById("my-checkbox").checked);
	  	if(a == false){
	  		$('#listView').show();
	  		$('#mapView').hide();
	  	} else {
	  		$('#listView').hide();
	  		$('#mapView').show();
	  		
	  	}
	}
	
function done() {
		
        var url_base64jp = document.getElementById("lineChart2").toDataURL("image/jpg");

       // link1.href = url_base64;
        link2.href=url_base64jp;

       // var url = link1.href.replace(/^data:image\/[^;]/, 'data:application/octet-stream');

    }
var areaChartData2 = {
		labels : ["24MAR 09:40", "24MAR 10:02", "24MAR 10:23", "24MAR 10:45", "24MAR 11:06"],
	datasets : [ 
	             {
		label : "Humidity",
		fillColor : "rgba(210, 214, 222, 1)",
		strokeColor : "rgba(210, 214, 222, 1)",
		pointColor : "rgba(210, 214, 222, 1)",
		pointStrokeColor : "#c1c7d1",
		pointHighlightFill : "#fff",
		pointHighlightStroke : "rgba(220,220,220,1)",
		data : [21.4,22.3,21.71,21.03,22.91]
	}
	]
};


var areaChartOptions = {
	scales : {
		xAxes : [ {
			ticks : {
				maxRotation : 90,
				minRotation : 90
			}
		} ]
	},

	//Boolean - If we should show the scale at all
	showScale : true,
	//Boolean - Whether grid lines are shown across the chart
	scaleShowGridLines : false,
	//String - Colour of the grid lines
	scaleGridLineColor : "rgba(0,0,0,.05)",
	//Number - Width of the grid lines
	scaleGridLineWidth : 1,
	//Boolean - Whether to show horizontal lines (except X axis)
	scaleShowHorizontalLines : true,
	//Boolean - Whether to show vertical lines (except Y axis)
	scaleShowVerticalLines : true,
	//Boolean - Whether the line is curved between points
	bezierCurve : true,
	//Number - Tension of the bezier curve between points
	bezierCurveTension : 0.3,
	//Boolean - Whether to show a dot for each point
	pointDot : false,
//Number - Radius of each point dot in pixels
pointDotRadius : 1,
//Number - Pixel width of point dot stroke
pointDotStrokeWidth : 1,
//Number - amount extra to add to the radius to cater for hit detection outside the drawn point !!!important to display only one point
pointHitDetectionRadius : 0,
//Boolean - Whether to show a stroke for datasets
datasetStroke : true,
//Number - Pixel width of dataset stroke
datasetStrokeWidth : 1,
	//Boolean - Whether to fill the dataset with a color
	datasetFill : true,
	//String - A legend template
	//Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
	maintainAspectRatio : true,
	//Boolean - whether to make the chart responsive to window resizing
	responsive : true,
	onAnimationComplete: done

};

//-------------
//- LINE CHART -
//--------------
areaChartOptions.datasetFill = false;
//$('a[data-toggle=tab').on('shown.bs.tab', function(e) {
//	window.dispatchEvent(new Event('resize'));

var lineChartCanvas2 = $("#lineChart2").get(0).getContext("2d");
	var lineChart2 = new Chart(lineChartCanvas2);
	lineChart2.Line(areaChartData2, areaChartOptions);

<%
//System.out.println(";"+temperature4+";");
//if(!temperature4.equals("00")){%>
//var lineChartCanvas4 = $("#lineChart4").get(0).getContext("2d");
//var lineChart4 = new Chart(lineChartCanvas4);
//lineChart4.Line(areaChartData4, areaChartOptions);
<%//}
%>

//});
$("[name='my-checkbox']").bootstrapSwitch();
</script>
	</body>
</html>
