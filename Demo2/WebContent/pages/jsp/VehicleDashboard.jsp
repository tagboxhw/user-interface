<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page errorPage="Error.jsp" %>
<%@ page import="com.demo2.action.LocationAction"%>
<%@ page import="com.demo2.dao.ClientLocationMapDao"%>
<%@ page import="com.demo2.dao.AlertsWorkflowDao"%>
<%@ page import="com.demo2.dao.TransitDao"%>
<%@ page import="com.demo2.bean.LocationBean"%>
<%@ page import="com.demo2.bean.SubLocationBean"%>
<%@ page import="com.demo2.util.Constants"%>
<%@ page import="com.demo2.util.Utils"%>
<%@ page import="java.util.TreeMap"%>
<%@ page import="java.util.LinkedHashMap"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.util.Collection"%>
<%@ page import="java.util.Set"%>
<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>

<%!
static final Logger logger = LoggerFactory.getLogger("VehicleDashboard");


%>
<%

String sUsername = (String) session.getAttribute("username");
if(sUsername == null || sUsername.equals("")) {
	session.setAttribute("callingPage", "VehicleDashboard.jsp");
	response.sendRedirect("../../index.html");
}
java.util.Date dt = new java.util.Date();
java.text.SimpleDateFormat sdf = 
     new java.text.SimpleDateFormat("dd-MM-yyyy HH:mm");
sdf.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
String currentTime = sdf.format(dt);
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Dashboard</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Add these below two lines for autocomplete and the javascript function at the end  -->
<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="../../bootstrap/css/bootstrap.css">
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
   
<!-- Font Awesome -->
<script src="https://use.fontawesome.com/37ba1872d2.js"></script>
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../../dist/css/tagbox.css">
<script src="../../dist/js/tagbox.js"></script>
<!-- <script src="../../dist/js/google_charts.js"></script> -->
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
<link href="https://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
  body {
	font-family:  Lato, sans-serif;
	font-weight: 600;
}
</style>
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
	<div class="wrapper">

		<header class="main-header">
			<!-- Logo -->
			<div>
				<a href="#" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
					<span class="logo-mini"><b>Demo</b></span> <!-- logo for regular state and mobile devices -->
					<span class="logo-lg"><b>Demo</b> </span>
				</a>
			</div>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">

				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
					role="button"> <span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
				</a>
				<form class="navbar-form pull-left" role="search">
					<div class="form-group">
						<b style="padding-left: 15px; font-size: 20px;color:#fff;" class="pull-right">Today's Vehicle Health Summary</b><br> <b style="color:#fff;padding-left: 15px"><%= currentTime %></b>
					</div>
				</form>

				<div class="navbar-custom-menu">
					<img src="../../dist/img/Logo.png" class="logo-class pull-right"
						alt="User Image">
				</div>
			</nav>

		</header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->


				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<br>
				<ul class="sidebar-menu">
					<li class="header">MAIN NAVIGATION</li>
					<li class="treeview active"><a href="#"> <i
							class="fa fa-dashboard"></i> <span>Dashboard</span> <span
							class="pull-right-container"> <i
								class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
						<ul class="treeview-menu">
							<li class="active"><a href="VehicleDashboard.jsp"><i
									class="fa fa-circle-o"></i> CMC</a></li>
							<!--  <li><a href="VehiclePlannedRouteMap.jsp"><i class="fa fa-circle-o"></i> Vehicle Planned Route</a></li> -->
							<li><a href="TodayColdChainHealthVehicle.jsp"><i
									class="fa fa-circle-o"></i> Vehicles By Route</a></li>
						</ul></li>
					<li class="treeview"><a href="#"> <i
							class="fa fa-files-o"></i> <span>Alert</span> 
					</a>
						<ul class="treeview-menu">
							<li><a href="LiveVehicleTracking.jsp"><i
									class="fa fa-circle-o"></i> Live Vehicle Tracking</a></li>
						</ul></li>
					
					<li class="treeview"><a href="#"> <i
							class="fa fa-pie-chart"></i> <span>Admin Console</span> <span
							class="pull-right-container"> <i
								class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
						<ul class="treeview-menu">
							<li class=""><a href="VehicleManagement.jsp"><i class="fa fa-circle-o"></i>
									Vehicle Management</a></li>
							<li><a href="VendorScorecard.jsp"><i class="fa fa-circle-o"></i>
									Vendor Score Card</a></li>
						</ul></li>
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			
		<section id="mainSection" class="content white-background">
		
			<div class="row">
				
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-12">
						
							<div class="well well-sm white-background" style="height:170px">
							<div class="panel-coldroom text-center">
							<div class="row">
							<div class="col-md-offset-4 col-md-4">
						<b>VEHICLE ALERTS STATUS</b>
						</div>
						<div class="col-md-4">
						<span class="pull-right"><i>Vehicles with: &nbsp;&nbsp;
				<i class="fa fa-square alert-text alert-font"></i>&nbsp; >3 Alerts&nbsp;&nbsp;
				<i class="fa fa-square warning-text alert-font"></i>&nbsp; 1-3 Alerts&nbsp;&nbsp;
				<i class="fa fa-square success-text alert-font"></i>&nbsp; No Alerts</i>
				</span>
						</div>
						</div>
						</div>
						<br>
								<div class="text-center">
									<div class="row">
										<div class="col-md-12">
											<div class="row">
												<div class="col-md-4">
													<a href="TodayColdChainHealthVehicle.jsp">MU <i class="fa fa-long-arrow-right"></i> DC (# Of Vehicles: <b>20</b>)</a>
													<div class="graph_container">
														<div>
														<canvas id="Chart3" class="chart1" height="30" width="250"></canvas>
														<div class="row text-center">
															<div class="col-md-4"><span data-toggle="tooltip" class="badge badge-big bg-red">01</span><br><b><span class="description-percentage text-green"><i class="fa fa-caret-down"></i>&nbsp;12%</span></b></div>
															<div class="col-md-4"><span data-toggle="tooltip" class="badge badge-big orange">03</span><br><b><span class="description-percentage text-red"><i class="fa fa-caret-up"></i>&nbsp;09%</span></b></div>
															<div class="col-md-4"><span data-toggle="tooltip" class="badge badge-big bg-green">16</span></div>
														</div>
														</div>
													</div>
												</div>
											
												<div class="col-md-4">
													DC <i class="fa fa-long-arrow-right"></i> DC (# Of Vehicles: <b>45</b>)
													<div class="graph_container">
													<div>
														<canvas id="Chart4" class="chart1" height="30" width="250"></canvas>
														<div class="row text-center">
															<div class="col-md-4"><span data-toggle="tooltip" class="badge badge-big bg-red">03</span><br><b><span class="description-percentage text-green"><i class="fa fa-caret-down"></i>&nbsp;02%</span></b></div>
															<div class="col-md-4"><span data-toggle="tooltip" class="badge badge-big orange">12</span><br><b><span class="description-percentage text-green"><i class="fa fa-caret-down"></i>&nbsp;04%</span></b></div>
															<div class="col-md-4"><span data-toggle="tooltip" class="badge badge-big bg-green">30</span></div>
														</div>
														</div>
													</div>
												</div>
											
												<div class="col-md-4">
													DC <i class="fa fa-long-arrow-right"></i> End Point (# Of Vehicles: <b>80</b>)
													<div class="graph_container">
														<canvas id="Chart5" class="chart1" height="30" width="250"></canvas>
														<div class="row text-center">
															<div class="col-md-4"><span data-toggle="tooltip" class="badge badge-big bg-red">08</span><br><b><span class="description-percentage text-red"><i class="fa fa-caret-up"></i>&nbsp;03%</span></b></div>
															<div class="col-md-4"><span data-toggle="tooltip" class="badge badge-big orange">08</span><br><b><span class="description-percentage text-red"><i class="fa fa-caret-up"></i>&nbsp;07%</span></b></div>
															<div class="col-md-4"><span data-toggle="tooltip" class="badge badge-big bg-green">64</span></div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 text-center">
				<div class="well well-sm white-background">
				<div class="text-center">
				<div class="panel-coldroom text-center">
					<b>TOTAL NUMBER OF ALERTS TODAY: <B class="font-big" style="color:black">15</B> </b>
					</div>
					</div><br>
					<div class="row">
						<div class="col-md-12">
								<div class="row">
								<div class="col-md-3">
								TEMPERATURE
								<div class="row">
									<div class="col-md-offset-3 col-md-3">
									<img src="../../dist/img/temperature.png">
									</div>
									<div class="col-md-6 text-left">
								<b class="font-new">07</b>
									</div>
								</div>
								<b><span class="description-percentage text-red"><i class="fa fa-caret-up"></i>&nbsp;02%</span></b>
								</div>
								<div class="col-md-3">
								HUMIDITY
								<div class="row">
									<div class="col-md-offset-3 col-md-3">
									<img src="../../dist/img/humidity.png">
									</div>
									<div class="col-md-6 text-left">
								<b class="font-new text-left">01</b>
									</div>
								</div>
								<b><span class="description-percentage text-green"><i class="fa fa-caret-down"></i>&nbsp;04%</span></b>
								</div>
							<div class="col-md-3">
								VEHICLE DOOR OPEN
								<div class="row">
									<div class="col-md-offset-2 col-md-3">
									<img src="../../dist/img/vehdooropen.png">
									</div>
									<div class="col-md-6 text-left">
								<b class="font-new text-left">02</b>
									</div>
									
								</div>
								<b><span class="description-percentage text-green text-center"><i class="fa fa-caret-down"></i>&nbsp;06%</span></b>
								</div>
								<div class="col-md-3">
								VEHICLE STOPPAGE
								<div class="row">
									<div class="col-md-offset-2 col-md-4">
									<img src="../../dist/img/vehiclestop.png">
									</div>
									<div class="col-md-6 text-left">
								<b class="font-new text-left">05</b>
									</div>
								
								</div>
								<b><span class="description-percentage text-red"><i class="fa fa-caret-up"></i>&nbsp;05%</span></b>
								</div>
								</div>
					</div>
					
							
						</div>
					</div>
				</div>
				</div>
			<div class="row">
				<div class="col-md-12 text-center">
					<div class="well well-sm white-background" style="height:140px">
					<div class="panel-coldroom text-center">
					<div class="row">
					
						<div class="col-md-offset-4 col-md-4">
						<b>ALERTS RESOLUTION</b>
						</div>
						<div class="col-md-4">
						<span class="pull-right"><i>
				<i class="fa fa-square grey-text-color alert-font"></i>&nbsp;% Alerts Unresolved&nbsp;&nbsp;
				<i class="fa fa-square info-text-color alert-font"></i>&nbsp;% Alerts Resolved</i>
				</span>
						</div>
						</div>
					</div>
					<br>
						<div class="row">
							
							<div class="col-md-offset-1 col-md-3">
								TOTAL MU <i class="fa fa-long-arrow-right"></i> DC ALERTS: <b>7</b><br>
								<div class="graph_container" style="position: relative;">
									<canvas id="Chart8" class="chart1" width="100"></canvas>
								</div>
							</div>
							<div class="col-md-offset-1 col-md-3">
								TOTAL DC <i class="fa fa-long-arrow-right"></i> DC ALERTS: <b>2</b><br>
								<div class="graph_container" style="position: relative;">
									<canvas id="Chart9" class="chart1" width="100"></canvas>
								</div>
							</div>
							<div class="col-md-offset-1 col-md-3">
								TOTAL DC <i class="fa fa-long-arrow-right"></i> EP ALERTS: <b>6</b><br>
								<div class="graph_container" style="position: relative;">
									<canvas id="Chart10" class="chart1" width="100"></canvas>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			
				<div class="row">
				<div class="col-md-12 text-center">
				<div class="well well-sm white-background">
				<div class="text-center">
				<div class="panel-coldroom text-center">
					<b>FLEET EFFICIENCY SUMMARY</b>
					</div>
					</div><br>
					<div class="row">
						
					<div class="col-md-12 text-center">
						
							<div class="row">
								<div class="col-md-3"><br><br>
								% ON TIME
								</div>
								<div class="col-md-3">
								MU <i class="fa fa-long-arrow-right"></i> DC
								<div class="row">
									<div class="col-md-12">
								<b class="font-new">85%</b>
									</div>
								</div>
								<b><span class="description-percentage text-red"><i class="fa fa-caret-down"></i>&nbsp;05%</span></b>
								</div>
								<div class="col-md-3">
								DC <i class="fa fa-long-arrow-right"></i> DC
								<div class="row">
									<div class="col-md-12">
								<b class="font-new text-left">83%</b>
									</div>
								</div>
								<b><span class="description-percentage text-green"><i class="fa fa-caret-up"></i>&nbsp;10%</span></b>
								</div>
								<div class="col-md-3">
								DC <i class="fa fa-long-arrow-right"></i> EP
								<div class="row">
									<div class="col-md-12">
								<b class="font-new text-left">82%</b>
									</div>
								</div>
								<b><span class="description-percentage text-green"><i class="fa fa-caret-up"></i>&nbsp;08%</span></b>
								</div>
							</div>	
								
					</div>
					</div>
					</div>
					</div>
				</div>
				</section>
				</div>
		<!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0
			</div>
			<strong>Copyright &copy; 2017 <a href="http://tagbox.in">TagBox</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Control Sidebar -->
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->
<!-- ChartJS 1.0.1 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>

	<!-- Bootstrap 3.3.6 -->
	<script src="../../bootstrap/js/bootstrap.min.js"></script>
	<!-- ChartJS 1.0.1 -->
	<!-- AdminLTE App -->
	<script src="../../dist/js/app.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="../../dist/js/demo.js"></script>
<script>

Chart.defaults.global.legend.display = false;

var barOptions_stacked = {
	    tooltips: {
	        enabled: false
	    },
	    hover :{
	        animationDuration:0
	    },
	    scales: {
	        xAxes: [{
	        	display:false,
	            ticks: {
	                beginAtZero:true,
	                fontFamily: "'Open Sans Bold', sans-serif",
	                fontSize:11
	            },
	            
	            stacked: true
	        }],
	        yAxes: [{
	        	display:false,
	            ticks: {
	                fontFamily: "'Open Sans Bold', sans-serif",
	                fontSize:11
	            },
	            stacked: true
	        }]
	    },
	    legend:{
	        display:false
	    },
	    pointLabelFontFamily : "Quadon Extra Bold",
	    scaleFontFamily : "Quadon Extra Bold"
	};
	
var horizontalbarOptions_stacked = {
		responsive: true,
		maintainAspectRatio: false,
		tooltips: {
	        enabled: false
	    },
	    hover :{
	        animationDuration:0
	    },
	    scales: {
	        xAxes: [{
	        	display: false,
	            ticks: {
	                beginAtZero:true,
	                fontFamily: "'Open Sans Bold', sans-serif",
	                fontSize:11
	            },
	            scaleLabel:{
	                display:false
	            },
	            gridLines: {
	            }, 
	            stacked: true,
	            barPercentage: 0.5,
		        categoryPercentage: 0.6
	        }],
	        yAxes: [{
	        	display: false,
	            gridLines: {
	                display:false,
	                color: "#fff",
	                zeroLineColor: "#fff",
	                zeroLineWidth: 0
	            },
	            scaleLabel:{
	                display:false
	            },
	            ticks: {
	                fontFamily: "'Open Sans Bold', sans-serif",
	                fontSize:11
	            },
	            stacked: true
	        }]
	    },
	    legend:{
	        display:false
	    },
	    
	    animation: {
	        onComplete: function () {
	            var chartInstance = this.chart;
	            var ctx = chartInstance.ctx;
	            ctx.textAlign = "center";
	            ctx.font = "20px Open Sans";
	            ctx.fillStyle = "#fff";

	            Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
	                var meta = chartInstance.controller.getDatasetMeta(i);
	                Chart.helpers.each(meta.data.forEach(function (bar, index) {
	                    data = dataset.data[index];
	                   // alert(index + ":" + data + ":" +  bar._model.x + ":" + bar._model.y);
	                        ctx.fillText(data, bar._model.x-10, bar._model.y);
	                    
	                }),this)
	            }),this);
				
			}
	    },
	    pointLabelFontFamily : "Quadon Extra Bold",
	    scaleFontFamily : "Quadon Extra Bold"
	};
	
	
	var ctx2 = document.getElementById("Chart3");
	var myChart2 = new Chart(ctx2, {
	    type: 'horizontalBar',
	    data: {
	        labels: [""],
	        
	        datasets: [{
	        	label: ">3 Alerts",
	            data: [6],
	            backgroundColor: "rgba(212,19,20,1)"
	        },{
	        	label: "<3 Alerts",
	            data: [12],
	            backgroundColor: "rgba(255,169,34,1)"
	        },{
	        	label: "No Alerts",
	            data: [96],
	            backgroundColor: "rgba(49,150,8,1)"
	        }]
	    },

	    options: barOptions_stacked
	});
	var ctx3 = document.getElementById("Chart4");
	var myChart3 = new Chart(ctx3, {
	    type: 'horizontalBar',
	    data: {
	        labels: [""],
	        
	        datasets: [{
	        	label: ">3 Alerts",
	            data: [3],
	            backgroundColor: "rgba(212,19,20,1)"
	        },{
	        	label: "<3 Alerts",
	            data: [10],
	            backgroundColor: "rgba(255,169,34,1)"
	        },{
	        	label: "No Alerts",
	            data: [56],
	            backgroundColor: "rgba(49,150,8,1)"
	        }]
	    },

	    options: barOptions_stacked
	});
	var ctx4 = document.getElementById("Chart5");
	var myChart4 = new Chart(ctx4, {
	    type: 'horizontalBar',
	    data: {
	        labels: [""],
	        
	        datasets: [{
	        	label: ">3 Alerts",
	            data: [8],
	            backgroundColor: "rgba(212,19,20,1)"
	        },{
	        	label: "<3 Alerts",
	            data: [8],
	            backgroundColor: "rgba(255,169,34,1)"
	        },{
	        	label: "No Alerts",
	            data: [64],
	            backgroundColor: "rgba(49,150,8,1)"
	        }]
	    },

	    options: barOptions_stacked
	});
	
	
	
	
	// For a pie chart
	var ctx7 = document.getElementById("Chart8");
	var myPieChart2 = new Chart(ctx7,{
	    type: 'horizontalBar',
	    data: {
	        labels: [""],
	        
	        datasets: [{
	        	label: "Resolved",
	            data: [5],
	            backgroundColor: "rgb(66, 133, 244)"
	        },{
	        	label: "Unresolved",
	            data: [2],
	            backgroundColor: "rgba(66, 66, 66, 0.5)"
	        }]
	    },
	    options: horizontalbarOptions_stacked
	});
	var ctx8 = document.getElementById("Chart9");
	var myPieChart3 = new Chart(ctx8,{
	    type: 'horizontalBar',
	    data: {
	        labels: [""],
	        
	        datasets: [{
	        	label: "Resolved",
	            data: [1],
	            backgroundColor: "rgb(66, 133, 244)"
	        },{
	        	label: "Unresolved",
	            data: [1],
	            backgroundColor: "rgba(66, 66, 66, 0.5)"
	        }]
	    },
	    options: horizontalbarOptions_stacked
	});
	var ctx9 = document.getElementById("Chart10");
	var myPieChart4 = new Chart(ctx9,{
	    type: 'horizontalBar',
	    data: {
	        labels: [""],
	        
	        datasets: [{
	        	label: "Resolved",
	            data: [2],
	            backgroundColor: "rgb(66, 133, 244)"
	        },{
	        	label: "Unresolved",
	            data: [4],
	            backgroundColor: "rgba(66, 66, 66, 0.5)"
	        }]
	    },
	    options: horizontalbarOptions_stacked
	});
	
	</script>


	</body>
</html>
