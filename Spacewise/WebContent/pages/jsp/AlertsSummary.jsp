<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@ page errorPage="Error.jsp" %>
<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>

<%!
static final Logger logger = LoggerFactory.getLogger("AlertsSummary");
%>
<%
String sUsername = (String) session.getAttribute("username");
if(sUsername == null || sUsername.equals("")) {
	session.setAttribute("callingPage", "AlertsSummary.jsp");
	response.sendRedirect("../../index.html");
}
java.util.Date dt = new java.util.Date();
java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd-MM-yyyy HH:mm");
sdf.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
String currentTime = sdf.format(dt);
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Alerts Summary</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Add these below two lines for autocomplete and the javascript function at the end  -->
<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="../../bootstrap/css/bootstrap.css">
   
<!-- Font Awesome -->
<script src="https://use.fontawesome.com/37ba1872d2.js"></script>
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../../dist/css/tagbox.css">
<link href="../../dist/css/select2.css" rel="stylesheet" />
<script src="../../dist/js/select2.js"></script>
<script src="../../dist/js/tagbox.js"></script>
<script src="../../dist/js/table2download.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#VehicleID").select2();
	$("#Destination").select2();
	$("#Source").select2();
});
</script>
<STYLE TYPE="text/css">
			table.sortable span.sortarrow {
   				color: blue;
   				text-decoration: none;
			}
					
		</style>
<script src="../../dist/js/sorttable.js"></script>
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
<link href="https://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet">

<link rel="stylesheet" href="../../dist/css/bootstrap-switch.css">
<script src="../../dist/js/bootstrap-switch1.js"></script>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
  body {
	font-family:  Lato, sans-serif;
	font-weight: 600;
}
</style>
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
	<div class="wrapper">

		<header class="main-header">
			<!-- Logo -->
			<div>
				<a href="#" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
					<span class="logo-mini"><b>Demo</b></span> <!-- logo for regular state and mobile devices -->
					<span class="logo-lg"><b>Demo</b> </span>
				</a>
			</div>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">

				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
					role="button"> <span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
				</a>
				<form class="navbar-form pull-left" role="search">
					<div class="form-group">
						<b style="padding-left: 15px; font-size: 20px;color:#fff;" class="pull-right">Alerts Summary</b><br> <b style="padding-left: 15px;color:#fff"><%= currentTime %></b>
					</div>
				</form>

				<div class="navbar-custom-menu">
					<img src="../../dist/img/Logo.png" class="logo-class pull-right"
						alt="User Image">
				</div>
			</nav>

		</header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->


				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<br>
				<ul class="sidebar-menu">
					<li class="header">MAIN NAVIGATION</li>
					<li class="treeview"><a href="#"> <i
							class="fa fa-dashboard"></i> <span>Dashboard</span>
					</a>
						<ul class="treeview-menu">
							<li class=""><a href="CMCDashboard.jsp"><i
									class="fa fa-circle-o"></i> CMC</a></li>
							<!-- <li class=""><a href="NewDashboard.jsp"><i
									class="fa fa-circle-o"></i> Vehicle Dashboard</a></li> -->
							<li class=""><a href="TodayColdChainHealth.jsp"><i
									class="fa fa-circle-o"></i> Cold Rooms Summary</a></li>
							<!--  <li><a href="VehiclePlannedRouteMap.jsp"><i class="fa fa-circle-o"></i> Vehicle Planned Route</a></li> -->
							<li class=""><a href="TodayColdChainHealthVehicle.jsp"><i
									class="fa fa-circle-o"></i> Vehicles Summary</a></li>
						</ul></li>
					<li class="treeview"><a href="#"> <i
							class="fa fa-files-o"></i> <span>Alerts</span> 
					</a>
						<ul class="treeview-menu">
							<li><a href="LiveVehicleTracking.jsp"><i
									class="fa fa-circle-o"></i> Live Vehicle Tracking</a></li>
						</ul></li>
					
					<li class="treeview"><a href="#"> <i
							class="fa fa-database"></i> <span>Admin</span>
					</a>
						<ul class="treeview-menu">
							<li class=""><a href="VehicleManagement.jsp"><i class="fa fa-circle-o"></i>
									Vehicle Management</a></li>
							<li><a href="VendorScorecard.jsp"><i class="fa fa-circle-o"></i>
									Vehicle Score Card</a></li>
						</ul></li>
						<li class="treeview active"><a href="#"> <i
							class="fa fa-bar-chart"></i> <span>Reports</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="ParameterSummary.jsp"><i class="fa fa-circle-o"></i>
									Parameter Summary</a></li>
							<li class="active"><a href="AlertsSummary.jsp"><i class="fa fa-circle-o"></i>
									Alerts Summary</a></li>
							<li><a href="AlertsReport.jsp"><i class="fa fa-circle-o"></i>
									Alerts Report</a></li>		
							
						</ul></li>
						<li class="treeview"><a href="#"> <i
							class="fa fa-pie-chart"></i> <span>Analysis</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="BlockTemplate.jsp"><i class="fa fa-circle-o"></i>
									Temperature Profile <br>Analysis</a></li>
							<li><a href="OrganizationTemplate.jsp"><i class="fa fa-circle-o"></i>
									Excursion and Breach- <br>Root Cause Analysis</a></li>
							<li><a href="SankeyTemplate.jsp"><i class="fa fa-circle-o"></i>
									Global Supply <Br>Chain Health</a></li>
						</ul></li>
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			
		<section id="mainSection" class="content">
		<div class="well well-sm white-background" style="height:550px">
							
						<br>
						<form id="myForm2" name="myForm2" method="post"
											action="AlertsSummary.jsp">
											<div class="row">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-offset-2 col-md-2 text-center">
						<label><b>Parameter</b></label>&nbsp;&nbsp;
					<select id="Destination" name="Destination" class="form-control-no-background pull-center">
					<option>Temperature</option>
					<option>Humidity</option>
					<option>Door Activity</option>
					<option>Detour</option>
					</select>
					</div>
					<div class="col-md-2 text-center">
					<label><b>Location</b></label>
					<select id="VehicleID" name="VehicleID" class="form-control-no-background pull-center">
					<option>Location</option>
					<option>Bengaluru</option>
					<option>Chennai</option>
					</select>
					</div>
					<div class="col-md-2 text-center">
					<label><b>Cold Room</b></label>
					<select id="Source" name="Source" class="form-control-no-background pull-center">
					<option>Cold Room</option>
					<option>First Floor CR</option>
					<option>Second Floor CR</option>
					</select>
					</div>
					<div class="col-md-3"><br>
								<button type="submit" class="btn btn-success">GO</button>&nbsp;&nbsp;
								<button type="button" class="btn btn-success" onclick="resetAll();">RESET</button>
								</div>
					<div class="col-md-3 pull-right"><br><br>
						<span id="csvLink1"></span>&nbsp;&nbsp;&nbsp;&nbsp;<span id="xlsLink1"></span>
					</div>
					</div>
					<br>
					
					<div class="scroll-area-table">
					<div class="info-background text-center">Severity</div>
						<table id="data_table_stationary1" width="80%" class="table table-bordered table-condensed sortable">
									
									<tr class="info text-center">
									<th width="10">&nbsp;</th>
									<th width="10">&nbsp;</th>
									<th width="100" class="text-center">2</th>
									<th width="100" class="text-center">3</th>
									<th width="100" class="text-center">4</th>
									<th width="100" class="text-center">5</th>
									</tr>
									<tr><td width="10" rowspan="6" class="info text-center"><br><br>Resolution</td></tr>
									<tr><td class="info">1</td>	<td>a</td>	<td>b</td>	<td>c</td>	<td>d</td></tr>
									<tr><td class="info">2</td>	<td>d</td>	<td>c</td>	<td>b</td>	<td>a</td></tr>
									<tr><td class="info">3</td>	<td>e</td>	<td>f</td>	<td>g</td>	<td>h</td></tr>
									<tr><td class="info">4</td>	<td>h</td>	<td>g</td>	<td>f</td>	<td>e</td></tr>
									<tr><td class="info">5</td>	<td>a</td>	<td>b</td>	<td>c</td>	<td>d</td></tr>
									</table>
						
					
									</div>
				</div>
						</div>	
										</form>
						
						
						
						
						
						
						
								</div>
						
		</section>
		</div>
							</div>
		
		<!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0
			</div>
			<strong>Copyright &copy; 2017 <a href="http://tagbox.in">TagBox</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Control Sidebar -->
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->
<!-- ChartJS 1.0.1 -->
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>


	<!-- Bootstrap 3.3.6 -->
	<script src="../../bootstrap/js/bootstrap.min.js"></script>
	<!-- ChartJS 1.0.1 -->
	<!-- AdminLTE App -->
	<script src="../../dist/js/app.min.js"></script>
	<script src="../../plugins/chartjs/Chart.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="../../dist/js/demo.js"></script>
	
	<script type="text/javascript">
	jQuery( document ).ready(function() {
	    
	    jQuery( "#data_table_stationary1" ).table_download({
	        format: "xls",
	        separator: ",",
	        filename: "download",
	        linkname: "Export To XLS",
	        quotes: "\"",
	        linkid: "xlsLink1"
	    });
	    
	    jQuery( "#data_table_stationary1" ).table_download({
	        format: "csv",
	        separator: "-",
	        filename: "download",
	        linkname: "Export To CSV",
	        quotes: "\"",
	        linkid: "csvLink1"
	    });  
	    
	});


</script>
	</body>
</html>
