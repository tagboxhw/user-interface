<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page errorPage="Error.jsp" %>
<%@ page import="java.util.TreeMap"%>
<%@ page import="java.util.LinkedHashMap"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.util.Collection"%>
<%@ page import="com.spacewise.util.Utils"%>
<%@ page import="com.spacewise.util.Constants"%>
<%@ page import="java.util.Set"%>
<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>
<%@ page import="com.spacewise.dao.AlertsWorkflowDao"%>
<%@ page import="com.spacewise.dao.TransitDao"%>
<%@ page import="com.spacewise.dao.ClientLocationMapDao"%>
<%@ page import="com.spacewise.bean.HumidityBean"%>
<%@ page import="com.spacewise.dao.HumidityDao"%>
<%@ page import="com.spacewise.bean.TemperatureBean"%>
<%@ page import="com.spacewise.dao.TemperatureDao"%>
<%@ page import="com.spacewise.bean.TodaysAlertWorkflowBean"%>
<%@ page import="com.spacewise.bean.AllLocationDataBean"%>
<%@ page import="com.spacewise.bean.AllVehicleDataBean"%>

<%!
static final Logger logger = LoggerFactory.getLogger("ColdRoomTodayColdChainHealth");
ClientLocationMapDao clmDao = new ClientLocationMapDao();
TransitDao trDao = new TransitDao();
%>
<%
String sUsername = (String) session.getAttribute("username");
if(sUsername == null || sUsername.equals("")) {
	session.setAttribute("callingPage", "ColdRoomTodayColdChainHealth.jsp");
	response.sendRedirect("../../index.html");
}
java.util.Date dt = new java.util.Date();
java.text.SimpleDateFormat sdf = 
     new java.text.SimpleDateFormat("dd-MM-yyyy HH:mm");
sdf.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
String currentTime = sdf.format(dt);
String sLocationId = request.getParameter("LocationId");
if(sLocationId == null || sLocationId.equals("")) sLocationId = "Bengaluru MU";
TreeMap<String, String> tmLocs = new TreeMap<String, String>();
TreeMap<String, String> tmZoneID = new TreeMap<String, String>();
TreeMap<String, Integer> tmZoneAlerts = new TreeMap<String, Integer>();
TreeMap<String, String> tmLocID = new TreeMap<String, String>();
String sValue = "";
int iMUDCCount = 0, iDCDCCount = 0, iDCEPCount = 0;
if(clmDao.colAllLocations == null || clmDao.colAllLocations.size() == 0) clmDao.getAllLocationsData();
if(trDao.colAllVehicles == null || trDao.colAllVehicles.size() == 0) trDao.getAllVehiclesData();
for(AllLocationDataBean alBean: clmDao.colAllLocations){
	tmLocID.put(alBean.getLocation_Name(), "1");
	if(!alBean.getLocation_Name().equals(sLocationId)) continue;
	if(!alBean.getZone_Id().equals("")) tmZoneID.put(alBean.getZone_Id(), alBean.getZone_Name());
	if(tmLocs.containsKey(alBean.getLocation_Name())) {
		sValue = tmLocs.get(alBean.getLocation_Name());
		if(sValue.indexOf(alBean.getZone_Id()) >= 0) {
			
		} else {
			sValue = sValue + ";" + alBean.getZone_Id();
			tmLocs.put(alBean.getLocation_Name(), sValue);
		}
	} else {
		sValue = alBean.getZone_Id();
		if(!alBean.getZone_Id().equals("")) tmLocs.put(alBean.getLocation_Name(), sValue);
	}
}
for(AllVehicleDataBean alBean: trDao.colAllVehicles){
	if(alBean.getSource_Location_Name().equals(sLocationId) || alBean.getDest_Location_Name().equals(sLocationId)) {
		if(alBean.getRoute_Type().equals(Constants.ROUTE_TYPE_MU_DC)){
			iMUDCCount++;
		} else if(alBean.getRoute_Type().equals(Constants.ROUTE_TYPE_DC_DC)){
			iDCDCCount++;
		} else if(alBean.getRoute_Type().equals(Constants.ROUTE_TYPE_DC_EP)){
			iDCEPCount++;
		} else {
			
		}
	}
}
int iTodayRed = 0, iTodayOrange = 0, iTodayGreen = 0;
int iTodayRedMUDC = 0, iTodayOrangeMUDC = 0, iTodayGreenMUDC = 0;
int iTodayRedDCDC = 0, iTodayOrangeDCDC = 0, iTodayGreenDCDC = 0,iTodayRedDCEP = 0, iTodayOrangeDCEP = 0, iTodayGreenDCEP = 0;
int iCompRed = 0, iCompOrange = 0, iCompGreen = 0;
int iCompRedMUDC = 0, iCompOrangeMUDC = 0, iCompGreenMUDC = 0;
int iCompRedDCDC = 0, iCompOrangeDCDC = 0, iCompGreenDCDC = 0,iCompRedDCEP = 0, iCompOrangeDCEP = 0, iCompGreenDCEP = 0;
int iTemperatureCRCount = 0, iHumidityCRCount = 0, iDoorOpenCRCount = 0, iTemperatureVehCount = 0, iHumidityVehCount = 0, iDoorOpenVehCount = 0, iStoppageVehCount = 0; 
String sPower = "ON";
int iCompTemperatureCRCount = 0, iCompHumidityCRCount = 0, iCompDoorOpenCRCount = 0, iCompTemperatureVehCount = 0,
	iCompHumidityVehCount = 0, iCompDoorOpenVehCount = 0, iCompStoppageVehCount = 0;
AlertsWorkflowDao awd = new AlertsWorkflowDao();
TodaysAlertWorkflowBean tawBean = new TodaysAlertWorkflowBean();
Collection<TodaysAlertWorkflowBean> colTodayAlerts = awd.getTodaysAlerts();
Collection<TodaysAlertWorkflowBean> colAlertsComp = awd.getAlertsComp();
TreeMap<String, String> tmLocs1 = new TreeMap<String, String>();

for(TodaysAlertWorkflowBean taw: colTodayAlerts) {
	if(!taw.getLocation_Name().equals(sLocationId)) continue;
	if(tmZoneID.containsKey(taw.getZone_ID())) {
		if(tmZoneAlerts.containsKey(taw.getZone_ID())) {
			int iTemp1 = (Integer) tmZoneAlerts.get(taw.getZone_ID());
			tmZoneAlerts.put(taw.getZone_ID(), iTemp1++);
		} else {
			tmZoneAlerts.put(taw.getZone_ID(), 1);
		}
	}
	
		if(taw.getCount_Of_Alerts() > 3) iTodayRed++;
		else if(taw.getCount_Of_Alerts() <= 3 && taw.getCount_Of_Alerts() > 0) iTodayOrange++;
		else if(taw.getCount_Of_Alerts() == 0) iTodayGreen++;
		if(tmLocs1.containsKey(taw.getCity_Name())) {
			sValue = tmLocs1.get(taw.getCity_Name());
			if(sValue.indexOf(taw.getZone_ID()) >= 0) {
				
			} else {
				sValue = sValue + ";" + taw.getZone_ID();
				tmLocs1.put(taw.getCity_Name(), sValue);
			}
		} else {
			sValue = taw.getZone_ID();
			tmLocs1.put(taw.getCity_Name(), sValue);
		}
	if(taw.getRoute_Type().equals(Constants.ROUTE_TYPE_MU_DC)) {
		if(taw.getCount_Of_Alerts() > 3) iTodayRedMUDC++;
		else if(taw.getCount_Of_Alerts() <= 3 && taw.getCount_Of_Alerts() > 0) iTodayOrangeMUDC++;
		else if(taw.getCount_Of_Alerts() == 0) iTodayGreenMUDC++;
	} else if(taw.getRoute_Type().equals(Constants.ROUTE_TYPE_DC_DC)) {
		if(taw.getCount_Of_Alerts() > 3) iTodayRedDCDC++;
		else if(taw.getCount_Of_Alerts() <= 3 && taw.getCount_Of_Alerts() > 0) iTodayOrangeDCDC++;
		else if(taw.getCount_Of_Alerts() == 0) iTodayGreenDCDC++;
	} else if(taw.getRoute_Type().equals(Constants.ROUTE_TYPE_DC_EP)) {
		if(taw.getCount_Of_Alerts() > 3) iTodayRedDCEP++;
		else if(taw.getCount_Of_Alerts() <= 3 && taw.getCount_Of_Alerts() > 0) iTodayOrangeDCEP++;
		else if(taw.getCount_Of_Alerts() == 0) iTodayGreenDCEP++;
	}
	if(taw.getClass().equals(Constants.STATIONARY)) {
		if(taw.getAlert_Type().equals(Constants.TEMPERATURE)) iTemperatureCRCount++;
		else if(taw.getAlert_Type().equals(Constants.HUMIDITY)) iHumidityCRCount++;
		else if(taw.getAlert_Type().equals(Constants.DOOR_ACTIVITY)) iDoorOpenCRCount++;
	} else if(taw.getClass().equals(Constants.TRANSIT)) {
		if(taw.getAlert_Type().equals(Constants.TEMPERATURE)) iTemperatureVehCount++;
		else if(taw.getAlert_Type().equals(Constants.HUMIDITY)) iHumidityVehCount++;
		else if(taw.getAlert_Type().equals(Constants.DOOR_ACTIVITY)) iDoorOpenVehCount++;
		else if(taw.getAlert_Type().equals(Constants.STOPPAGE)) iStoppageVehCount++;
	}
}
for(TodaysAlertWorkflowBean taw: colAlertsComp) {
	if(!taw.getLocation_Name().equals(sLocationId)) continue;
	
		if(taw.getCount_Of_Alerts() > 3) iCompRed++;
		else if(taw.getCount_Of_Alerts() <= 3 && taw.getCount_Of_Alerts() > 0) iCompOrange++;
		else if(taw.getCount_Of_Alerts() == 0) iCompGreen++;
	if(taw.getRoute_Type().equals(Constants.ROUTE_TYPE_MU_DC)) {
		if(taw.getCount_Of_Alerts() > 3) iCompRedMUDC++;
		else if(taw.getCount_Of_Alerts() <= 3 && taw.getCount_Of_Alerts() > 0) iCompOrangeMUDC++;
		else if(taw.getCount_Of_Alerts() == 0) iCompGreenMUDC++;
	} else if(taw.getRoute_Type().equals(Constants.ROUTE_TYPE_DC_DC)) {
		if(taw.getCount_Of_Alerts() > 3) iCompRedDCDC++;
		else if(taw.getCount_Of_Alerts() <= 3 && taw.getCount_Of_Alerts() > 0) iCompOrangeDCDC++;
		else if(taw.getCount_Of_Alerts() == 0) iCompGreenDCDC++;
	} else if(taw.getRoute_Type().equals(Constants.ROUTE_TYPE_DC_EP)) {
		if(taw.getCount_Of_Alerts() > 3) iCompRedDCEP++;
		else if(taw.getCount_Of_Alerts() <= 3 && taw.getCount_Of_Alerts() > 0) iCompOrangeDCEP++;
		else if(taw.getCount_Of_Alerts() == 0) iCompGreenDCEP++;
	}
	if(taw.getClass().equals(Constants.STATIONARY)) {
		if(taw.getAlert_Type().equals(Constants.TEMPERATURE)) iCompTemperatureCRCount++;
		else if(taw.getAlert_Type().equals(Constants.HUMIDITY)) iCompHumidityCRCount++;
		else if(taw.getAlert_Type().equals(Constants.DOOR_ACTIVITY)) iCompDoorOpenCRCount++;
	} else if(taw.getClass().equals(Constants.TRANSIT)) {
		if(taw.getAlert_Type().equals(Constants.TEMPERATURE)) iCompTemperatureVehCount++;
		else if(taw.getAlert_Type().equals(Constants.HUMIDITY)) iCompHumidityVehCount++;
		else if(taw.getAlert_Type().equals(Constants.DOOR_ACTIVITY)) iCompDoorOpenVehCount++;
		else if(taw.getAlert_Type().equals(Constants.STOPPAGE)) iCompStoppageVehCount++;
	}
}
int iRed = 0, iOrange = 0, iRedMUDC = 0, iOrangeMUDC = 0, iRedDCDC = 0, iOrangeDCDC = 0, iRedDCEP = 0, iOrangeDCEP = 0;
if(iCompRed > 0) iRed = ((iTodayRed-iCompRed)/iCompRed)*100;
if(iCompOrange > 0) iOrange = ((iTodayOrange-iCompOrange)/iCompOrange)*100;
if(iCompRedMUDC > 0) iRedMUDC = ((iTodayRedMUDC-iCompRedMUDC)/iCompRedMUDC)*100;
if(iCompOrangeMUDC > 0) iOrangeMUDC = ((iTodayOrangeMUDC-iCompOrangeMUDC)/iCompOrangeMUDC)*100;
if(iCompRedDCDC > 0) iRedDCDC = ((iTodayRedDCDC-iCompRedDCDC)/iCompRedDCDC)*100;
if(iCompOrangeDCDC > 0) iOrangeDCDC = ((iTodayOrangeDCDC-iCompOrangeDCDC)/iCompOrangeDCDC)*100;
if(iCompRedDCEP > 0) iRedDCEP = ((iTodayRedDCEP-iCompRedDCEP)/iCompRedDCEP)*100;
if(iCompOrangeDCEP > 0) iOrangeDCEP = ((iTodayOrangeDCEP-iCompOrangeDCEP)/iCompOrangeDCEP)*100;

int iDiffTemperatureCRCount = 0, iDiffHumidityCRCount = 0, iDiffDoorOpenCRCount = 0, iDiffTemperatureVehCount = 0, iDiffHumidityVehCount = 0, 
	iDiffDoorOpenVehCount = 0, iDiffStoppageVehCount = 0;

if(iCompTemperatureCRCount > 0) iDiffTemperatureCRCount = ((iTemperatureCRCount-iCompTemperatureCRCount)/iCompTemperatureCRCount)*100;
if(iCompHumidityCRCount > 0) iDiffHumidityCRCount = ((iHumidityCRCount-iCompHumidityCRCount)/iCompHumidityCRCount)*100;
if(iCompDoorOpenCRCount > 0) iDiffDoorOpenCRCount = ((iDoorOpenCRCount-iCompDoorOpenCRCount)/iCompDoorOpenCRCount)*100;
if(iCompTemperatureVehCount > 0) iDiffTemperatureVehCount = ((iTemperatureVehCount-iCompTemperatureVehCount)/iCompTemperatureVehCount)*100;
if(iCompHumidityVehCount > 0) iDiffHumidityVehCount = ((iHumidityVehCount-iCompHumidityVehCount)/iCompHumidityVehCount)*100;
if(iCompDoorOpenVehCount > 0) iDiffDoorOpenVehCount = ((iDoorOpenVehCount-iCompDoorOpenVehCount)/iCompDoorOpenVehCount)*100;
if(iCompStoppageVehCount > 0) iDiffStoppageVehCount = ((iStoppageVehCount-iCompStoppageVehCount)/iCompStoppageVehCount)*100;

HumidityDao hd = new HumidityDao();
HumidityBean hdBean = new HumidityBean();
TemperatureDao td = new TemperatureDao();
TemperatureBean tdBean = new TemperatureBean();
Collection<TemperatureBean> colTemperature = td.getTemperatureData();
Collection<HumidityBean> colHumidity = hd.getHumidityData();
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Dashboard</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Add these below two lines for autocomplete and the javascript function at the end  -->
<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="../../bootstrap/css/bootstrap.css">
   
<!-- Font Awesome -->
<script src="https://use.fontawesome.com/37ba1872d2.js"></script>
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../../dist/css/tagbox.css">
<script src="../../dist/js/tagbox.js"></script>
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
<link href="https://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
   body {
	font-family:  Lato, sans-serif;
	font-weight: 600;
}
</style>
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
	<div class="wrapper">

		<header class="main-header">
			<!-- Logo -->
			<div>
				<a href="#" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
					<span class="logo-mini"><b>Demo</b></span> <!-- logo for regular state and mobile devices -->
					<span class="logo-lg"><b>Demo</b> </span>
				</a>
			</div>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">

				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
					role="button"> <span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
				</a>
				<form class="navbar-form pull-left" role="search">
					<div class="form-group">
						<b style="padding-left: 15px; font-size: 20px;color:#fff;" class="pull-right">Today's Cold Chain Health Summary</b><br> <b style="padding-left: 15px;color:#fff;"><%= currentTime %></b>
					</div>
				</form>

				<div class="navbar-custom-menu">
					<img src="../../dist/img/Logo.png" class="logo-class pull-right"
						alt="User Image">
				</div>
			</nav>

		</header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->


				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<br>
				<ul class="sidebar-menu">
					<li class="header">MAIN NAVIGATION</li>
					<li class="treeview active"><a href="#"> <i
							class="fa fa-dashboard"></i> <span>Dashboard</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="CMCDashboard.jsp"><i
									class="fa fa-circle-o"></i> CMC</a></li>
							<!-- <li class=""><a href="NewDashboard.jsp"><i
									class="fa fa-circle-o"></i> Vehicle Dashboard</a></li> -->
							<li class="active"><a href="TodayColdChainHealth.jsp"><i
									class="fa fa-circle-o"></i> Cold Rooms Summary</a></li>
							<!--  <li><a href="VehiclePlannedRouteMap.jsp"><i class="fa fa-circle-o"></i> Vehicle Planned Route</a></li> -->
							<li class=""><a href="TodayColdChainHealthVehicle.jsp"><i
									class="fa fa-circle-o"></i> Vehicles Summary</a></li>
						</ul></li>
					<li class="treeview"><a href="#"> <i
							class="fa fa-files-o"></i> <span>Alerts</span> 
					</a>
						<ul class="treeview-menu">
							<li><a href="LiveVehicleTracking.jsp"><i
									class="fa fa-circle-o"></i> Live Vehicle Tracking</a></li>
						</ul></li>
					
					<li class="treeview"><a href="#"> <i
							class="fa fa-database"></i> <span>Admin</span>
					</a>
						<ul class="treeview-menu">
							<li class=""><a href="VehicleManagement.jsp"><i class="fa fa-circle-o"></i>
									Vehicle Management</a></li>
							<li><a href="VendorScorecard.jsp"><i class="fa fa-circle-o"></i>
									Vehicle Score Card</a></li>
						</ul></li>
						<li class="treeview"><a href="#"> <i
							class="fa fa-bar-chart"></i> <span>Reports</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="ParameterSummary.jsp"><i class="fa fa-circle-o"></i>
									Parameter Summary</a></li>
							<li><a href="AlertsSummary.jsp"><i class="fa fa-circle-o"></i>
									Alerts Summary</a></li>
							<li><a href="AlertsReport.jsp"><i class="fa fa-circle-o"></i>
									Alerts Report</a></li>		
							
						</ul></li>
						<li class="treeview"><a href="#"> <i
							class="fa fa-pie-chart"></i> <span>Analysis</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="BlockTemplate.jsp"><i class="fa fa-circle-o"></i>
									Temperature Profile <br>Analysis</a></li>
							<li><a href="OrganizationTemplate.jsp"><i class="fa fa-circle-o"></i>
									Excursion and Breach- <br>Root Cause Analysis</a></li>
							<li><a href="SankeyTemplate.jsp"><i class="fa fa-circle-o"></i>
									Global Supply <Br>Chain Health</a></li>
						</ul></li>
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			
		<section id="mainSection" class="content">
		<b><a href="CMCDashboard.jsp">CMC</a>  <i class="fa fa-caret-right"></i>&nbsp; <%= sLocationId %></b><br> <br>
			<div class="row">
				<div class="col-md-10">
					<div class="row">
						<div class="col-md-12">
						<div class="text-center"><i>Cold Rooms With:&nbsp;&nbsp;<i class="fa fa-square alert-text alert-font"></i>&nbsp;>3 Alerts&nbsp;&nbsp;
				<i class="fa fa-square warning-text alert-font"></i>&nbsp;1-3 Alerts&nbsp;&nbsp;
				<i class="fa fa-square success-text alert-font"></i>&nbsp;No Alerts</i>
				</div><div class="panel-coldroom text-center">
								<b>&nbsp;COLD ROOMS</b>
							</div>
							<div class="well well-sm white-background" style="height:550px">
								<div class="text-center">
									<b>TOTAL <%= sLocationId.toUpperCase() %> COLD ROOMS: <%= tmZoneID.size() %></b><br>
									<div class="row">
										<div class="col-md-offset-2 col-md-7">
										<div class="graph_container">
													<canvas id="Chart1" class="chart1" height="30" width="250"></canvas>
													<div class="row text-center">
														<div class="col-md-4"><span data-toggle="tooltip" class="badge badge-big bg-red"><%= Utils.zeroPad(iTodayRed, 2) %></span><br>
														<%if(iRed == 0) { %>
														<b><span class="description-percentage">NA</span></b>
														<%} else if(iRed > 0){ %>
														<b><span class="description-percentage text-red"><i class="fa fa-caret-up"></i>&nbsp;<%=Utils.zeroPad(iRed, 2) %>%</span></b>
														<%} else { %>
														<b><span class="description-percentage text-green"><i class="fa fa-caret-down"></i>&nbsp;<%=Utils.zeroPad(Math.abs(iRed), 2) %>%</span></b>
														<%} %>
														</div>
														<div class="col-md-4"><span data-toggle="tooltip" class="badge badge-big orange"><%= Utils.zeroPad(iTodayOrange, 2) %></span><br>
														<%if(iOrange == 0) { %>
														<b><span class="description-percentage">NA</span></b>
														<%} else if(iOrange > 0){ %>
														<b><span class="description-percentage text-red"><i class="fa fa-caret-up"></i>&nbsp;<%=Utils.zeroPad(iOrange, 2) %>%</span></b>
														<%} else { %>
														<b><span class="description-percentage text-green"><i class="fa fa-caret-down"></i>&nbsp;<%=Utils.zeroPad(Math.abs(iOrange), 2) %>%</span></b>
														<%} %>
														</div>
														<div class="col-md-4"><span data-toggle="tooltip" class="badge badge-big bg-green"><%= Utils.zeroPad(iTodayGreen, 2) %></span></div>
													</div>
												</div>
												</div>
									</div>
									
									<br>
									<div class="row">
										<div class="col-md-12">
											<div class="well well-sm">
												<div class="text-center">
												<table class="table table-striped table-condensed">
							<tr><td colspan="6" class="text-center blue"><b>TODAY'S ALERTS SUMMARY</b></td></tr>
							<tr class="panel-stop"><td>&nbsp;</td><td>Current Temperature</td><td>Current Humidity</td><td>#Of Alerts Today</td><td>#Of Unresolved Alerts</td><td>New Alert</td></tr>
							<%Set set = tmZoneID.keySet();
							Iterator iter = set.iterator();
							int iZoneAlerts = 0;
							while(iter.hasNext()) {
								sValue = (String) iter.next();
								if(tmZoneAlerts.containsKey(sValue)) {
									iZoneAlerts = (Integer) tmZoneAlerts.get(sValue);
								} else {
									iZoneAlerts = 0;
								}
								int iCount = 0;
								float iTemperature = 0;
								float iHumidity = 0;
								for(TemperatureBean tb: colTemperature) {
									if(tb.getZone_Id().equals(sValue)) {
										iCount++;
										iTemperature = iTemperature + Float.parseFloat(tb.getTemperature());
									}
								}
								if(iCount > 0) iTemperature = iTemperature/iCount;
								iCount = 0;
								for(HumidityBean hb: colHumidity) {
									if(hb.getZone_Id().equals(sValue)) {
										iCount++;
										iHumidity = iHumidity + Float.parseFloat(hb.getHumidity());
									}
								}
								if(iCount > 0) iHumidity = iHumidity/iCount;
								%>
							<tr><td><a href="TodayColdChainHealthSummary.jsp?ZoneId=<%= sValue %>"><%= tmZoneID.get(sValue) %></a><td><%= Math.round(iTemperature) %></td><td><%= Math.round(iHumidity) %></td></td><td><%= iZoneAlerts %></td><td>01</td><td><span class="text-red"><i class="fa fa-bell"></i></span><small class="label pull-right bg-red">New alert!</small></td></tr>
							<%} %>
							
						</table>
												</div>
											</div>
										</div>
										
									</div>
									
								</div>
							</div>
						</div>
					</div>
				</div>
				
				
				<div class="col-md-2">
				<div class="text-left">
				<label>GO TO:</label>
				<div class="scroll-area1 white-background">
				<%set = tmLocID.keySet();
				iter = set.iterator();
				while(iter.hasNext()) {
					sValue = (String) iter.next();
					if(sValue.equals(sLocationId)){%>
					<a href="TodayColdChainHealth.jsp?LocationId=<%=sValue%>"><%= sValue.toUpperCase() %></a><br>
					<%} else { %>
					<span style="font-weight:normal"><a href="TodayColdChainHealth.jsp?LocationId=<%=sValue%>"><%= sValue %></a></span><br>
					<%}
					
				}%>
				</div>
												</div>
											</div>
											
			</div>
			
				<div class="well well-sm white-background">
				<div class="text-center panel-coldroom">
					<b>TOTAL NUMBER OF ALERTS TODAY</b>
					</div><br>
					<div class="row">
						<div class="col-md-12 text-center">
							TOTAL COLD ROOM ALERTS: <B class="font-big" style="color:black"><%= Utils.zeroPad((iTemperatureCRCount+iHumidityCRCount+iDoorOpenCRCount), 2) %></B>
							<div class="well well-sm white-background">
					<div class="row">
								<div class="col-md-3">
								TEMPERATURE
								<div class="row"><br>
									<div class="col-md-offset-1 col-md-3">
									<img src="../../dist/img/temperature.png">
									</div>
									<div class="col-md-offset-1 col-md-7 text-left">
								<b class="font-new"><%= Utils.zeroPad(iTemperatureCRCount, 2) %></b>
									</div>
								</div>
								<%if(iDiffTemperatureCRCount == 0) { %>
									<b><span class="description-percentage">NA</span></b>
								<%} else if(iDiffTemperatureCRCount > 0) { %>
									<b><span class="description-percentage text-red"><i class="fa fa-caret-up"></i>&nbsp;<%=Utils.zeroPad(iDiffTemperatureCRCount, 2) %>%</span></b>
								<%} else { %>
									<b><span class="description-percentage text-green"><i class="fa fa-caret-down"></i>&nbsp;<%=Utils.zeroPad(Math.abs(iDiffTemperatureCRCount), 2) %>%</span></b>
								<%} %>
								</div>
								<div class="col-md-3">
								HUMIDITY
								<div class="row"><br>
									<div class="col-md-offset-1 col-md-3">
									<img src="../../dist/img/humidity.png">
									</div>
									<div class="col-md-offset-1 col-md-7 text-left">
								<b class="font-new text-left"><%=Utils.zeroPad(iHumidityCRCount, 2)%></b>
									</div>
								</div>
								<%if(iDiffHumidityCRCount == 0) { %>
									<b><span class="description-percentage">NA</span></b>
								<%} else if(iDiffHumidityCRCount > 0) { %>
									<b><span class="description-percentage text-red"><i class="fa fa-caret-up"></i>&nbsp;<%=Utils.zeroPad(iDiffHumidityCRCount, 2) %>%</span></b>
								<%} else { %>
									<b><span class="description-percentage text-green"><i class="fa fa-caret-down"></i>&nbsp;<%=Utils.zeroPad(Math.abs(iDiffHumidityCRCount), 2) %>%</span></b>
								<%} %>
								</div>
								<div class="col-md-3">
								DOOR OPEN
								<div class="row"><br>
									<div class="col-md-offset-1 col-md-3">
									<img src="../../dist/img/dooropen.png">
									</div>
									<div class="col-md-offset-1 col-md-7 text-left">
								<b class="font-new text-left"><%=Utils.zeroPad(iDoorOpenCRCount, 2)%></b>
									</div>
								</div>
								<%if(iDiffDoorOpenCRCount == 0) { %>
									<b><span class="description-percentage">NA</span></b>
								<%} else if(iDiffDoorOpenCRCount > 0) { %>
									<b><span class="description-percentage text-red"><i class="fa fa-caret-up"></i>&nbsp;<%=Utils.zeroPad(iDiffDoorOpenCRCount, 2) %>%</span></b>
								<%} else { %>
									<b><span class="description-percentage text-green"><i class="fa fa-caret-down"></i>&nbsp;<%=Utils.zeroPad(Math.abs(iDiffDoorOpenCRCount), 2) %>%</span></b>
								<%} %>
								</div>
								<div class="col-md-3">
								POWER
								<div class="row">
									<br>
									<div class="col-md-12 text-center">
								<b class="font-new text-left">ON</b>
									</div>
								</div>
								</div>
								</div>
							</div>
						</div>
						
						</div>
					</div>
				
		</section>
			</div>
		
		</div>
		<!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0
			</div>
			<strong>Copyright &copy; 2017 <a href="http://tagbox.in">TagBox</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Control Sidebar -->
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->
<!-- ChartJS 1.0.1 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>


	<!-- Bootstrap 3.3.6 -->
	<script src="../../bootstrap/js/bootstrap.min.js"></script>
	<!-- ChartJS 1.0.1 -->
	<!-- AdminLTE App -->
	<script src="../../dist/js/app.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="../../dist/js/demo.js"></script>
<script>
Chart.defaults.global.legend.display = false;

var barOptions_stacked = {
	    tooltips: {
	        enabled: false
	    },
	    hover :{
	        animationDuration:0
	    },
	    scales: {
	        xAxes: [{
	        	display:false,
	            ticks: {
	                beginAtZero:true,
	                fontFamily: "'Open Sans Bold', sans-serif",
	                fontSize:11
	            },
	            
	            stacked: true
	        }],
	        yAxes: [{
	        	display:false,
	            ticks: {
	                fontFamily: "'Open Sans Bold', sans-serif",
	                fontSize:11
	            },
	            stacked: true
	        }]
	    },
	    legend:{
	        display:false
	    },
	    pointLabelFontFamily : "Quadon Extra Bold",
	    scaleFontFamily : "Quadon Extra Bold"
	};
	
var ctx = document.getElementById("Chart1");
var myChart = new Chart(ctx, {
    type: 'horizontalBar',
    data: {
        labels: [""],
        
        datasets: [{
        	label: ">3 Alerts",
            data: [<%=iTodayRed%>],
            backgroundColor: "rgba(212,19,20,1)"
        },{
        	label: "<3 Alerts",
            data: [<%=iTodayOrange%>],
            backgroundColor: "rgba(255,169,34,1)"
        },{
        	label: "No Alerts",
            data: [<%=iTodayGreen%>],
            backgroundColor: "rgba(49,150,8,1)"
        }]
    },

    options: barOptions_stacked
});
var ctx2 = document.getElementById("Chart3");
var myChart2 = new Chart(ctx2, {
    type: 'horizontalBar',
    data: {
        labels: [""],
        
        datasets: [{
        	label: ">3 Alerts",
            data: [<%=iTodayRedMUDC %>],
            backgroundColor: "rgba(212,19,20,1)"
        },{
        	label: "<3 Alerts",
            data: [<%=iTodayOrangeMUDC %>],
            backgroundColor: "rgba(255,169,34,1)"
        },{
        	label: "No Alerts",
            data: [<%=iTodayGreenMUDC %>],
            backgroundColor: "rgba(49,150,8,1)"
        }]
    },

    options: barOptions_stacked
});
var ctx3 = document.getElementById("Chart4");
var myChart3 = new Chart(ctx3, {
    type: 'horizontalBar',
    data: {
        labels: [""],
        
        datasets: [{
        	label: ">3 Alerts",
            data: [<%=iTodayRedDCDC %>],
            backgroundColor: "rgba(212,19,20,1)"
        },{
        	label: "<3 Alerts",
            data: [<%=iTodayOrangeDCDC %>],
            backgroundColor: "rgba(255,169,34,1)"
        },{
        	label: "No Alerts",
            data: [<%=iTodayGreenDCDC %>],
            backgroundColor: "rgba(49,150,8,1)"
        }]
    },

    options: barOptions_stacked
});
var ctx4 = document.getElementById("Chart5");
var myChart4 = new Chart(ctx4, {
    type: 'horizontalBar',
    data: {
        labels: [""],
        
        datasets: [{
        	label: ">3 Alerts",
            data: [<%=iTodayRedDCEP %>],
            backgroundColor: "rgba(212,19,20,1)"
        },{
        	label: "<3 Alerts",
            data: [<%=iTodayOrangeDCEP %>],
            backgroundColor: "rgba(255,169,34,1)"
        },{
        	label: "No Alerts",
            data: [<%=iTodayGreenDCEP %>],
            backgroundColor: "rgba(49,150,8,1)"
        }]
    },

    options: barOptions_stacked
});

</script>
	</body>
</html>
