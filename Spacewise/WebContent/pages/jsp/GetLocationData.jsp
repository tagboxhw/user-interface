<%@ page errorPage="Error.jsp" %>
<%@ page import="com.spacewise.action.LocationAction"%>
<%@ page import="com.spacewise.action.AlertAction"%>
<%@ page import="com.spacewise.dao.ClientLocationMapDao"%>
<%@ page import="com.spacewise.bean.SubLocationBean"%>
<%@ page import="com.spacewise.bean.StationaryNodeMapBean"%>
<%@ page import="com.spacewise.bean.ZoneBean"%>
<%@ page import="java.util.TreeMap"%>
<%@ page import="java.util.Vector"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.util.Collection"%>
<%@ page import="com.spacewise.bean.StationaryTemperatureBean"%>
<%@ page import="com.spacewise.bean.StationaryDoorActivityBean"%>
<%@ page import="com.spacewise.dao.StationaryTemperatureDao"%>
<%@ page import="com.spacewise.dao.StationaryDoorActivityDao"%>
<%@ page import="com.spacewise.bean.StationaryMeterEnergyBean"%>
<%@ page import="com.spacewise.dao.StationaryMeterEnergyDao"%>
<%@ page import="java.util.Date"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Calendar"%>
<%@ page import="java.util.GregorianCalendar"%>
<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>

<!-- <?xml version="1.0" encoding="ISO-8859-1"?> -->

<%!static final Logger logger = LoggerFactory.getLogger("GetLocationData");
	LocationAction locAction = new LocationAction();
	AlertAction alertAction = new AlertAction();
	String buf = "";
	String sString = "";
	String sErrorMessage = null;
	String sId = null;
	String sRowId = null;
	String sSubLocId = null;
	SubLocationBean subLocBean = new SubLocationBean();
	ClientLocationMapDao clientDao = new ClientLocationMapDao();
	ZoneBean zoneBean = new ZoneBean();
	StationaryNodeMapBean stationaryNodeMapBean = new StationaryNodeMapBean();
	String temperature = "", temperature1 = "", temperature2 = "",
			temperature3 = "", time = "", time1 = "", time2 = "", time3 = "";
	StationaryTemperatureBean stBean = new StationaryTemperatureBean();
	StationaryTemperatureDao stDao = new StationaryTemperatureDao();
	StationaryDoorActivityBean sdBean = new StationaryDoorActivityBean();
	StationaryDoorActivityDao sdDao = new StationaryDoorActivityDao();
	StationaryMeterEnergyBean smBean = new StationaryMeterEnergyBean();
	StationaryMeterEnergyDao smDao = new StationaryMeterEnergyDao();
	TreeMap tm = new TreeMap();
	List<String> al = new ArrayList<String>();%>
<%
	try {
		sString = request.getParameter("query");
		if (sString.equals("subLoc")) {
			sId = request.getParameter("locId");

			//logger.info("sLocId: " + sId);
			buf = "";
			//response.setContentType("text/xml");
			//buf += ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			tm = locAction.getLocationSubTypes(sId);
			Iterator<String> i = tm.keySet().iterator();
			int iCount = 0;
			//logger.info("<rows total_count='100' pos='0'>");
			buf = "<rows>";
			while (i.hasNext()) {
				sRowId = (String) i.next();
				subLocBean = (SubLocationBean) tm.get(sRowId);
				buf += ("<row id=\"" + iCount + "\">");
				buf += ("<Name>" + subLocBean.getName() + "</Name>");
				buf += ("<Id>" + subLocBean.getSubLocationId() + "</Id>");
				buf += ("</row>");
				iCount++;
			}
			buf += "</rows>";
			buf = buf.trim();
			//logger.info(buf);
			out.clear();
			out.println(buf);
		} else if (sString.equals("zone")) {
			sId = request.getParameter("locId");
			sSubLocId = request.getParameter("subLocId");
			if (sSubLocId == null || sSubLocId.equals("null"))
				sSubLocId = "0";
			//logger.info("sSubLocId: " + sSubLocId);
			buf = "";
			//response.setContentType("text/xml");
			//buf += ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			tm = locAction.getZones(sId, sSubLocId);
			Iterator<String> i = tm.keySet().iterator();
			int iCount = 0;
			//logger.info("<rows total_count='100' pos='0'>");
			buf = "<rows>";
			while (i.hasNext()) {
				sRowId = (String) i.next();
				zoneBean = (ZoneBean) tm.get(sRowId);
				buf += ("<row id=\"" + iCount + "\">");
				buf += ("<Name>" + zoneBean.getZoneName() + "</Name>");
				buf += ("<Id>" + zoneBean.getZoneId() + "</Id>");
				buf += ("</row>");
				iCount++;
			}
			buf += "</rows>";
			buf = buf.trim();
			//logger.info(buf);
			out.clear();
			out.println(buf);
		} else if (sString.equals("node")) {
			sId = request.getParameter("locId");
			//logger.info("sSubLocId: " + sSubLocId);
			buf = "";
			//response.setContentType("text/xml");
			//buf += ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			tm = clientDao.selectNodesFromZoneID(sId, "ALL", "60");
			Iterator<String> i = tm.keySet().iterator();
			int iCount = 0;
			//logger.info("<rows total_count='100' pos='0'>");
			buf = "<rows>";
			while (i.hasNext()) {
				sRowId = (String) i.next();
				stationaryNodeMapBean = (StationaryNodeMapBean) tm
						.get(sRowId);
				buf += ("<row id=\"" + iCount + "\">");
				buf += ("<Name>"
						+ stationaryNodeMapBean.getND_subzone_name() + "</Name>");
				buf += ("<Id>"
						+ stationaryNodeMapBean.getND_client_id() + "</Id>");
				buf += ("</row>");
				iCount++;
			}
			buf += "</rows>";
			buf = buf.trim();
			//logger.info(buf);
			out.clear();
			out.println(buf);
		} else if (sString.equals("Action")) {
			String sAlertId = request.getParameter("alertId");
			String sAction = request.getParameter("action");
			//logger.info("AlertId: " + sAlertId + " Action: " + sAction);
			buf = "";
			//response.setContentType("text/xml");
			//buf += ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			alertAction.updateAction(sAction, sAlertId);

			buf = "<rows>";
			buf += ("<row>");
			buf += ("<Done>Success</Done>");
			buf += ("</row>");
			buf += "</rows>";
			buf = buf.trim();
			//logger.info(buf);
			out.clear();
			out.println(buf);
		} else if (sString.equals("RootCause")) {
			String sAlertId = request.getParameter("alertId");
			String sRoot = request.getParameter("root");
			String sPreventive = request.getParameter("preventive");
			//logger.info("AlertId: " + sAlertId + " root: " + sRoot + " pre: " + sPreventive);
			buf = "";
			//response.setContentType("text/xml");
			//buf += ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			alertAction.updateRoot(sRoot, sPreventive, sAlertId);
			buf = "<rows>";

			buf += ("<row>");
			buf += ("<Done>Success</Done>");
			buf += ("</row>");

			buf += "</rows>";
			buf = buf.trim();
			//logger.info(buf);
			out.clear();
			out.println(buf);
		} else if (sString.equals("GetAllSourceLocs")) {
			buf = "";
			//response.setContentType("text/xml");
			//buf += ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			al = locAction.selectSourceVehiclesLocationCity();
			//logger.info("<rows total_count='100' pos='0'>");
			buf = "<rows>";
			for (int i = 0; i < al.size(); i++) {
				buf += ("<row id=\"" + i + "\">");
				buf += ("<City>" + al.get(i) + "</City>");
				buf += ("</row>");
			}
			buf += "</rows>";
			buf = buf.trim();
			//logger.info(buf);
			out.clear();
			out.println(buf);
		} else if (sString.equals("GetAllSourceIds")) {
			String sSource = request.getParameter("Source");
			buf = "";
			//response.setContentType("text/xml");
			//buf += ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			tm = locAction.selectSourceVehicleLocationNameId(sSource);
			Iterator<String> i = tm.keySet().iterator();
			int iCount = 0;

			buf = "<rows>";
			while (i.hasNext()) {
				sRowId = (String) i.next();
				sId = (String) tm.get(sRowId);
				buf += ("<row id=\"" + iCount + "\">");
				buf += ("<LocationId>" + sRowId + "</LocationId>");
				buf += ("<LocationName>" + sId + "</LocationName>");
				buf += ("</row>");
				iCount++;
			}
			buf += "</rows>";
			buf = buf.trim();
			//logger.info(buf);
			out.clear();
			out.println(buf);
		} else if (sString.equals("GetAllDestCity")) {
			String sLocId = request.getParameter("LocationId");
			buf = "";
			//response.setContentType("text/xml");
			//buf += ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			al = locAction
					.selectDestinationVehiclesLocationCity(sLocId);
			//logger.info("<rows total_count='100' pos='0'>");
			buf = "<rows>";
			for (int i = 0; i < al.size(); i++) {
				buf += ("<row id=\"" + i + "\">");
				buf += ("<DestCity>" + al.get(i) + "</DestCity>");
				buf += ("</row>");
			}
			buf += "</rows>";
			buf = buf.trim();
			//logger.info(buf);
			out.clear();
			out.println(buf);
		} else if (sString.equals("GetAllDestId")) {
			String sDestCity = request.getParameter("DestCity");
			buf = "";
			//response.setContentType("text/xml");
			//buf += ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			tm = locAction
					.selectDestinationVehicleLocationNameId(sDestCity);
			Iterator<String> i = tm.keySet().iterator();
			int iCount = 0;
			buf = "<rows>";
			while (i.hasNext()) {
				sRowId = (String) i.next();
				sId = (String) tm.get(sRowId);
				buf += ("<row id=\"" + iCount + "\">");
				buf += ("<LocationId>" + sRowId + "</LocationId>");
				buf += ("<LocationName>" + sId + "</LocationName>");
				buf += ("</row>");
				iCount++;
			}
			buf += "</rows>";
			buf = buf.trim();
			//logger.info(buf);
			out.clear();
			out.println(buf);
		} else if (sString.equals("GetAllVehicleIds")) {
			String sSourceId = request.getParameter("SourceId");
			String sDestId = request.getParameter("DestId");
			buf = "";
			al = locAction.selectVehicleIdForSourceDestination(
					sSourceId, sDestId);
			buf = "<rows>";
			for (int i = 0; i < al.size(); i++) {
				buf += ("<row id=\"" + i + "\">");
				buf += ("<VehicleId>" + al.get(i) + "</VehicleId>");
				buf += ("</row>");
			}
			buf += "</rows>";
			buf = buf.trim();
			//logger.info(buf);
			out.clear();
			out.println(buf);
		} else if (sString.equals("UpdateThresholdPhoneNum")) {
			String sLocId = request.getParameter("locId");
			String sNodeId = request.getParameter("nodeId");
			String sPhone = request.getParameter("phone");
			String sThreshold = request.getParameter("threshold");
			buf = "";
			clientDao.updateThresholdAndPhone(sLocId, sNodeId,
					sThreshold, sPhone);
			buf = "<rows>";
			buf += ("<row>");
			buf += ("<Done>Success</Done>");
			buf += ("</row>");
			buf += "</rows>";
			buf = buf.trim();
			//logger.info(buf);
			out.clear();
			out.println(buf);
		} else if (sString.equals("resetThreshold")) {
			String sNodeId = request.getParameter("nodeId");
			buf = "";
			clientDao.resetThreshold(sNodeId);
			buf = "<rows>";
			buf += ("<row>");
			buf += ("<Done>Success</Done>");
			buf += ("</row>");
			buf += "</rows>";
			buf = buf.trim();
			//logger.info(buf);
			out.clear();
			out.println(buf);
		} else if (sString.equals("updateCharts")) {
			String sNodeValue1 = request.getParameter("alertNodeValue");
			String sNode = request.getParameter("node");
			String stime = request.getParameter("sTime");
			Collection col = stDao.getChartData(sNode, stime);
			SimpleDateFormat df = new SimpleDateFormat(
					"yyyy-MM-dd HH:mm:ss");
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
			Date d = null;
			Calendar gc = new GregorianCalendar();
			Date d2 = null;
			Long newTime = null;
			Iterator<StationaryTemperatureBean> iterSt = col.iterator();
			int iColSize = col.size() / 20;
			int iSize = 0;
			temperature = "";
			time = "";
			while (iterSt.hasNext()) {
				stBean = (StationaryTemperatureBean) iterSt.next();
				if (iColSize != 0 && iSize % iColSize == 0) {
					d = df.parse(stBean.getTimestamp());
					newTime = d.getTime();
					newTime += (330 * 60 * 1000);
					d2 = new Date(newTime);
					temperature += stBean.getTemperature() + ", ";
					time += sdf.format(d2) + ", ";
				} else {
					if (col.size() != 0 && col.size() <= 10) {
						d = df.parse(stBean.getTimestamp());
						newTime = d.getTime();
						newTime += (330 * 60 * 1000);
						d2 = new Date(newTime);
						temperature += stBean.getTemperature() + ", ";
						time += sdf.format(d2) + ", ";
					}
				}
				iSize++;
			}
			;
			if (temperature.equals("") || time.equals("")) {
				temperature1 = "00";
				time1 = "00";
			} else {
				if (temperature.length() >= 2)
					temperature1 = temperature.substring(0,
							(temperature.length() - 2));
				if (time.length() >= 2)
					time1 = time.substring(0, (time.length() - 2));
			}

			temperature = "";
			time = "";
			//Enter GW_ClientId and ND_Client Id for Chart 3
			col = sdDao.getChartData(sNodeValue1, stime);
			Iterator<StationaryDoorActivityBean> iterSd = col
					.iterator();
			iColSize = col.size() / 20;
			iSize = 0;
			while (iterSd.hasNext()) {
				sdBean = (StationaryDoorActivityBean) iterSd.next();
				if (iColSize != 0 && iSize % iColSize == 0) {
					d = df.parse(sdBean.getTimestamp());
					newTime = d.getTime();
					newTime += (330 * 60 * 1000);
					d2 = new Date(newTime);
					temperature += sdBean.getDoorActivity() + ", ";
					time += sdf.format(d2) + ", ";
				} else {
					if (col.size() != 0 && col.size() <= 10) {
						d = df.parse(sdBean.getTimestamp());
						newTime = d.getTime();
						newTime += (330 * 60 * 1000);
						d2 = new Date(newTime);
						temperature += sdBean.getDoorActivity() + ", ";
						time += sdf.format(d2) + ", ";
					}
				}
				iSize++;
			}
			;

			if (temperature.equals("") || time.equals("")) {
				temperature2 = "00";
				time2 = "00";
			} else {
				if (temperature.length() >= 2)
					temperature2 = temperature.substring(0,
							(temperature.length() - 2));
				if (time.length() >= 2)
					time2 = time.substring(0, (time.length() - 2));
			}
			//logger.info("TEMP : " + temperature2 + " " + time2);
			temperature = "";
			time = "";
			//Enter GW_ClientId and ND_Client Id for Chart 3
			col = smDao.getChartData("ND-SY-DMO-000004", stime);
			Iterator<StationaryMeterEnergyBean> iterSm = col.iterator();
			iColSize = col.size() / 20;
			iSize = 0;
			while (iterSm.hasNext()) {
				smBean = (StationaryMeterEnergyBean) iterSm.next();
				if (iColSize != 0 && iSize % iColSize == 0) {
					d = df.parse(smBean.getTimestamp());
					newTime = d.getTime();
					newTime += (330 * 60 * 1000);
					d2 = new Date(newTime);
					temperature += smBean.getPowerFactor() + ", ";
					time += sdf.format(d2) + ", ";
				} else {
					if (col.size() != 0 && col.size() <= 10) {
						d = df.parse(smBean.getTimestamp());
						newTime = d.getTime();
						newTime += (330 * 60 * 1000);
						d2 = new Date(newTime);
						temperature += smBean.getPowerFactor() + ", ";
						time += sdf.format(d2) + ", ";
					}
				}
				iSize++;
			}
			;

			if (temperature.equals("") || time.equals("")) {
				temperature3 = "00";
				time3 = "00";
			} else {
				if (temperature.length() >= 2)
					temperature3 = temperature.substring(0,
							(temperature.length() - 2));
				if (time.length() >= 2)
					time3 = time.substring(0, (time.length() - 2));
			}
			buf = "<rows>";
			buf += ("<row>");
			buf += ("<TempLabel>" + time1 + "</TempLabel>");
			buf += ("<TempData>" + temperature1 + "</TempData>");
			buf += ("<DoorLabel>" + time2 + "</DoorLabel>");
			buf += ("<DoorData>" + temperature2 + "</DoorData>");
			buf += ("<PowerLabel>" + time3 + "</PowerLabel>");
			buf += ("<PowerData>" + temperature3 + "</PowerData>");
			buf += ("</row>");
			buf += "</rows>";
			buf = buf.trim();
			//logger.info(buf);
			out.clear();
			out.println(buf);
		}
	} catch (Exception e) {
		logger.error("Exception: " + e.getLocalizedMessage());
		e.printStackTrace();
	}
%>