<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@ page errorPage="Error.jsp" %>
<%@ page import="com.spacewise.dao.AlertsReportDao"%>
<%@ page import="com.spacewise.bean.AlertWorkflowBean"%>
<%@ page import="java.util.Collection"%>
<%@ page import="java.util.Vector"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>

<%!
static final Logger logger = LoggerFactory.getLogger("AlertsReport");
AlertsReportDao alertsReportDao = new AlertsReportDao();
AlertWorkflowBean alertWorkflowBean = new AlertWorkflowBean();
Collection<AlertWorkflowBean> col = new Vector<AlertWorkflowBean>();
%>
<%
String sUsername = (String) session.getAttribute("username");
if(sUsername == null || sUsername.equals("")) {
	session.setAttribute("callingPage", "AlertsReport.jsp");
	response.sendRedirect("../../index.html");
}
java.util.Date dt = new java.util.Date();
SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
Date d = null;
java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd-MM-yyyy HH:mm");
sdf.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
String currentTime = sdf.format(dt);
col = alertsReportDao.getAlertsReport();
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Alerts Reports</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Add these below two lines for autocomplete and the javascript function at the end  -->
<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="../../bootstrap/css/bootstrap.css">
   
<!-- Font Awesome -->
<script src="https://use.fontawesome.com/37ba1872d2.js"></script>
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../../dist/css/tagbox.css">
<link href="../../dist/css/select2.css" rel="stylesheet" />
<script src="../../dist/js/select2.js"></script>
<script src="../../dist/js/tagbox.js"></script>
<script src="../../dist/js/table2download.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#Destination").select2();
	$("#TripStatus").select2();
	$("#Alerts").select2();
	$("#AssignedTo").select2();
	$("#Status").select2();
});
</script>
<STYLE TYPE="text/css">
			table.sortable span.sortarrow {
   				color: blue;
   				text-decoration: none;
			}
					
		</style>
<script src="../../dist/js/sorttable.js"></script>
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
<link href="https://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet">

<link rel="stylesheet" href="../../dist/css/bootstrap-switch.css">
<script src="../../dist/js/bootstrap-switch1.js"></script>
<script src="../../dist/js/table2download.js"></script>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
  body {
	font-family:  Lato, sans-serif;
	font-weight: 600;
}
</style>
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
	<div class="wrapper">

		<header class="main-header">
			<!-- Logo -->
			<div>
				<a href="#" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
					<span class="logo-mini"><b>Demo</b></span> <!-- logo for regular state and mobile devices -->
					<span class="logo-lg"><b>Demo</b> </span>
				</a>
			</div>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">

				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
					role="button"> <span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
				</a>
				<form class="navbar-form pull-left" role="search">
					<div class="form-group">
						<b style="padding-left: 15px; font-size: 20px;color:#fff;" class="pull-right">Alerts Reports</b><br> <b style="padding-left: 15px;color:#fff;"><%= currentTime %></b>
					</div>
				</form>

				<div class="navbar-custom-menu">
					<img src="../../dist/img/Logo.png" class="logo-class pull-right"
						alt="User Image">
				</div>
			</nav>

		</header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->


				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<br>
				<ul class="sidebar-menu">
					<li class="header">MAIN NAVIGATION</li>
					<li class="treeview"><a href="#"> <i
							class="fa fa-dashboard"></i> <span>Dashboard</span>
					</a>
						<ul class="treeview-menu">
							<li class=""><a href="CMCDashboard.jsp"><i
									class="fa fa-circle-o"></i> CMC</a></li>
							<!-- <li class=""><a href="NewDashboard.jsp"><i
									class="fa fa-circle-o"></i> Vehicle Dashboard</a></li> -->
							<li class=""><a href="TodayColdChainHealth.jsp"><i
									class="fa fa-circle-o"></i> Cold Rooms Summary</a></li>
							<!--  <li><a href="VehiclePlannedRouteMap.jsp"><i class="fa fa-circle-o"></i> Vehicle Planned Route</a></li> -->
							<li class=""><a href="TodayColdChainHealthVehicle.jsp"><i
									class="fa fa-circle-o"></i> Vehicles Summary</a></li>
						</ul></li>
					<li class="treeview"><a href="#"> <i
							class="fa fa-files-o"></i> <span>Alerts</span> 
					</a>
						<ul class="treeview-menu">
							<li><a href="LiveVehicleTracking.jsp"><i
									class="fa fa-circle-o"></i> Live Vehicle Tracking</a></li>
						</ul></li>
					
					<li class="treeview"><a href="#"> <i
							class="fa fa-database"></i> <span>Admin</span>
					</a>
						<ul class="treeview-menu">
							<li class=""><a href="VehicleManagement.jsp"><i class="fa fa-circle-o"></i>
									Vehicle Management</a></li>
							<li><a href="VendorScorecard.jsp"><i class="fa fa-circle-o"></i>
									Vehicle Score Card</a></li>
						</ul></li>
						<li class="treeview active"><a href="#"> <i
							class="fa fa-bar-chart"></i> <span>Reports</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="ParameterSummary.jsp"><i class="fa fa-circle-o"></i>
									Parameter Summary</a></li>
							<li><a href="AlertsSummary.jsp"><i class="fa fa-circle-o"></i>
									Alerts Summary</a></li>
							<li class="active"><a href="AlertsReport.jsp"><i class="fa fa-circle-o"></i>
									Alerts Report</a></li>		
							
						</ul></li>
						<li class="treeview"><a href="#"> <i
							class="fa fa-pie-chart"></i> <span>Analysis</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="BlockTemplate.jsp"><i class="fa fa-circle-o"></i>
									Temperature Profile <br>Analysis</a></li>
							<li><a href="OrganizationTemplate.jsp"><i class="fa fa-circle-o"></i>
									Excursion and Breach- <br>Root Cause Analysis</a></li>
							<li><a href="SankeyTemplate.jsp"><i class="fa fa-circle-o"></i>
									Global Supply <Br>Chain Health</a></li>
						</ul></li>
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			
		<section id="mainSection" class="content">
		<div class="well well-sm white-background" style="height:500px">
							
						<br>
						<form id="myForm3" name="myForm3" method="post"
											action="AlertsReport.jsp">
											<div class="row">
				<div class="col-md-12">
				<div class="row">
						<div class="col-md-2 text-center">
					<label><b>Location</b></label>
					<select id="Destination" name="Destination" class="form-control-no-background pull-center">
					<option>Location</option>
					<option>Bengaluru</option>
					<option>Chennai</option>
					</select>
					</div>
					<div class="col-md-2 text-center">
					<label><b>Cold Room</b></label>
					<select id="TripStatus" name="TripStatus" class="form-control-no-background pull-center">
					<option>Cold Room</option>
					<option>First Floor CR</option>
					<option>Second Floor CR</option>
					</select>
					</div>
					
					<div class="col-md-2 text-center">
						<label><b>Alert Type</b></label>
					<select id="Alerts" name="Alerts" class="form-control-no-background pull-center">
					<option>Alert Type</option>
					<option>Door Open</option>
					<option>Temp Breach</option>
					</select>
					</div>
					<div class="col-md-2 text-center">
						<label><b>Assigned To</b></label>
					<select id="AssignedTo" name="AssignedTo" class="form-control-no-background pull-center">
					<option>Assigned To</option>
					<option>Prajesh</option>
					<option>Santhosh</option>
					</select>
					</div>
					<div class="col-md-2 text-center">
						<label><b>Status</b></label><br>
					<select id="Status" name="Status" class="form-control-no-background pull-center">
					<option>Status</option>
					<option>Closed</option>
					<option>Open</option>
					</select>
					</div>
					<div class="col-md-2"><br>
								<button type="submit" class="btn btn-success">GO</button>&nbsp;&nbsp;
								<button type="button" class="btn btn-success" onclick="resetAll();">RESET</button>
								</div>
					</div>
					<br>
					<div class="row">
					<div class="col-md-offset-9 col-md-3">
						<span id="csvLink"></span>&nbsp;&nbsp;&nbsp;&nbsp;<span id="xlsLink"></span>
					</div>
					<br><br>
					</div>
					<div class="scroll-area-table">
					<table id="data_table_stationary2" class="table table-bordered table-condensed sortable">
									
									<tr class="info text-center">
									<th width="150" class="text-center">Location</th>
									<th width="150" class="text-center">Cold Room</th>
									<th width="100" class="text-center">Time</th>
									<th width="100" class="text-center">Alert Type</th>
									<th width="120" class="text-center">Alert Duration</th>
									<th width="120" class="text-center">Breach</th>
									<th width="100" class="text-center">Assigned To</th>
									<th width="100" class="text-center">Status</th>
									</tr>
									<%
									for(AlertWorkflowBean alertWorkflowBean: col){ 
									%>
									<tr>
										<td><%= alertWorkflowBean.getGW_Client_ID()%></td>
										<td><%=alertWorkflowBean.getND_Client_ID() %></td>
										<td><%=sdf.format(df1.parse(alertWorkflowBean.getAlert_Timestamp_From())) %></td>
										<td><%=alertWorkflowBean.getAlert_Type() %></td>
										<td><%=alertWorkflowBean.getAlert_Duration() %></td>
										<td><%=alertWorkflowBean.getBreach_Type() %></td>
										<td><%=alertWorkflowBean.getAssigned_To() %></td>
										<td><%=alertWorkflowBean.getAction_Status() %></td></tr>
									<%} %>
									</table>
									</div>
				</div>
						</div>
						
																
								</form>
					
					
					
						
								</div>
						
		</section>
		</div>
							</div>
		
		<!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0
			</div>
			<strong>Copyright &copy; 2017 <a href="http://tagbox.in">TagBox</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Control Sidebar -->
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->
<!-- ChartJS 1.0.1 -->
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>


	<!-- Bootstrap 3.3.6 -->
	<script src="../../bootstrap/js/bootstrap.min.js"></script>
	<!-- ChartJS 1.0.1 -->
	<!-- AdminLTE App -->
	<script src="../../dist/js/app.min.js"></script>
	<script src="../../plugins/chartjs/Chart.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="../../dist/js/demo.js"></script>
	
	<script type="text/javascript">
	jQuery( document ).ready(function() {
	   
	    jQuery( "#data_table_stationary2" ).table_download({
	        format: "xls",
	        separator: ",",
	        filename: "download",
	        linkname: "Export To XLS",
	        quotes: "\"",
	        linkid: "xlsLink"
	    });
	    
	    jQuery( "#data_table_stationary2" ).table_download({
	        format: "csv",
	        separator: "-",
	        filename: "download",
	        linkname: "Export To CSV",
	        quotes: "\"",
	        linkid: "csvLink"
	    });  
	    
	});

</script>
	</body>
</html>
