<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page errorPage="Error.jsp" %>
<%@ page import="com.spacewise.dao.ClientLocationMapDao"%>
<%@ page import="com.spacewise.dao.TransitVehicleMapDao"%>
<%@ page import="com.spacewise.dao.TransitDriverMapDao"%>
<%@ page import="com.spacewise.bean.TransitDriverMapBean"%>
<%@ page import="com.spacewise.bean.TransitVehicleMapBean"%>
<%@ page import="com.spacewise.bean.ClientLocationMapBean"%>
<%@ page import="com.spacewise.util.Utils"%>
<%@ page import="java.util.TreeMap"%>
<%@ page import="java.util.Set"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.util.Collection"%>
<%@ page import="java.util.Set"%>
<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>

<%!
static final Logger logger = LoggerFactory.getLogger("VehicleManagement");
%>
<%
String sUsername = (String) session.getAttribute("username");
if(sUsername == null || sUsername.equals("")) {
	session.setAttribute("callingPage", "VehicleManagement.jsp");
	response.sendRedirect("../../index.html");
}
String sVehicleIdParam = "", sVehicleTypeParam = "", sSourceParam = "", sDestParam = "", sCurrentLocationParam = "", sDriverIDParam = "", sVehicleStatusParam = "", sListView = "";
sVehicleIdParam = request.getParameter("VehicleID");
if(sVehicleIdParam == null || sVehicleIdParam.equals("")) sVehicleIdParam = "ALL";
sSourceParam = request.getParameter("Source");
if(sSourceParam == null || sSourceParam.equals("")) sSourceParam = "ALL";
sDestParam = request.getParameter("Destination");
if(sDestParam == null || sDestParam.equals("")) sDestParam = "ALL";
sCurrentLocationParam = request.getParameter("CurrentLocation");
if(sCurrentLocationParam == null || sCurrentLocationParam.equals("")) sCurrentLocationParam = "ALL";
sDriverIDParam = request.getParameter("DriverID");
if(sDriverIDParam == null || sDriverIDParam.equals("")) sDriverIDParam = "ALL";
sVehicleStatusParam = request.getParameter("VehicleStatus");
if(sVehicleStatusParam == null || sVehicleStatusParam.equals("")) sVehicleStatusParam = "ALL";
sListView = request.getParameter("showView1");
System.out.println(sListView);

java.util.Date dt = new java.util.Date();
java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd-MM-yyyy HH:mm");
sdf.setTimeZone(java.util.TimeZone.getTimeZone("IST"));
String currentTime = sdf.format(dt);
ClientLocationMapDao clmDao = new ClientLocationMapDao();
TransitVehicleMapDao tvmDao = new TransitVehicleMapDao();
TransitDriverMapDao tdmDao = new TransitDriverMapDao();
TransitVehicleMapBean tvmBean = new TransitVehicleMapBean();
ClientLocationMapBean clmBean = new ClientLocationMapBean();

Collection<TransitVehicleMapBean> col = tvmDao.selectVehicles();
TreeMap tmLocations = clmDao.selectLocations();

Iterator iter = col.iterator();
String sVehicleId = "", sVehicleType = "", sSource = "", sDest = "", sCurrentLocation = "", sDriverID = "", sVehicleStatus  = "";
int iRunning = 0, iAssigned = 0, iAvailable = 0, iUnavailable = 0;
int tmp = 0;
while(iter.hasNext()) {
	tvmBean = (TransitVehicleMapBean) iter.next();
	if(tmLocations.containsKey(tvmBean.getSource())) {
		clmBean = (ClientLocationMapBean) tmLocations.get(tvmBean.getSource());
		tvmBean.setSource(clmBean.getLocationName());
	}
	if(tmLocations.containsKey(tvmBean.getDestination())) {
		clmBean = (ClientLocationMapBean) tmLocations.get(tvmBean.getDestination());
		tvmBean.setDestination(clmBean.getLocationName());
	}
	
	if(tvmBean.getLatitude() == null || tvmBean.getLatitude().equals("")) tvmBean.setLatitude("12.993709");
	if(tvmBean.getLongitude() == null || tvmBean.getLongitude().equals("")) tvmBean.setLongitude("77.681804");
	if(!sVehicleId.contains(tvmBean.getVehicle_ID())){ sVehicleId += tvmBean.getVehicle_ID() + ";";}
	//if(!sVehicleType.contains(tvmBean.getVehicle_Type())){ sVehicleType += tvmBean.getVehicle_Type() + ";";}
	if(!sSource.contains(tvmBean.getSource())){ sSource += tvmBean.getSource() + ";";}
	if(!sDest.contains(tvmBean.getDestination())){ sDest += tvmBean.getDestination() + ";";}
	if(!sCurrentLocation.contains(tvmBean.getSource())){ sCurrentLocation += tvmBean.getSource() + ";";}//This should be changed to Current Location
	//if(!sDriverID.contains(tvmBean.getDriver_DL_ID())){ sDriverID += tvmBean.getDriver_DL_ID() + ";";}
	if(!sVehicleStatus.contains(tvmBean.getVehicle_Status())){ sVehicleStatus += tvmBean.getVehicle_Status() + ";";}
	if(tvmBean.getVehicle_Status().equalsIgnoreCase("Assigned")) iAssigned++;
	else if(tvmBean.getVehicle_Status().equalsIgnoreCase("Running")) iRunning++;
	else if(tvmBean.getVehicle_Status().equalsIgnoreCase("Available")) iAvailable++;
	else if(tvmBean.getVehicle_Status().equalsIgnoreCase("Unavailable")) iUnavailable++;
	tmp++;
}
String[] sVehicleIdArray = sVehicleId.split(";");

String[] sSourceArray = sSource.split(";");
String[] sDestArray = sDest.split(";");
String[] sCurrentLocationArray = sCurrentLocation.split(";");
String[] sDriverIDArray = sDriverID.split(";");
String[] sVehicleStatusArray = sVehicleStatus.split(";");

TreeMap tmDrivers = tdmDao.selectDrivers();
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Vehicle Management</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Add these below two lines for autocomplete and the javascript function at the end  -->
<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="../../bootstrap/css/bootstrap.css">
   
<!-- Font Awesome -->
<script src="https://use.fontawesome.com/37ba1872d2.js"></script>
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../../dist/css/tagbox.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
<link href="https://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet">
<link href="../../dist/css/select2.css" rel="stylesheet" />
<script src="../../dist/js/select2.js"></script>
<script src="../../dist/js/vehicleManagement.js"></script>
<script src="../../dist/js/inventory_mgmt.js"></script>
<link rel="stylesheet" type="text/css" href="https://js.cit.api.here.com/v3/3.0/mapsjs-ui.css" />
<script src="http://js.api.here.com/v3/3.0/mapsjs-core.js" type="text/javascript" charset="utf-8"></script>
<script src="http://js.api.here.com/v3/3.0/mapsjs-service.js" type="text/javascript" charset="utf-8"></script>

<script type="text/javascript" src="https://js.cit.api.here.com/v3/3.0/mapsjs-ui.js"></script>
<script type="text/javascript" src="https://js.cit.api.here.com/v3/3.0/mapsjs-mapevents.js"></script>

<script  type="text/javascript" charset="UTF-8" >
   
/**
 * Boilerplate map initialization code starts below:
 */

//Step 1: initialize communication with the platform
var platform = new H.service.Platform({
	  app_id: 'DdeJ8sqdrenYc1k7eTH6',
	  app_code: 'DBU7IBF4dzhGgtLRCx6Sgg',
  useCIT: true,
  useHTTPS: true
});
var defaultLayers = platform.createDefaultLayers();
var platform1 = new H.service.Platform({
	  app_id: 'DdeJ8sqdrenYc1k7eTH6',
	  app_code: 'DBU7IBF4dzhGgtLRCx6Sgg',
useCIT: true,
useHTTPS: true
});
var defaultLayers1 = platform1.createDefaultLayers();

  </script>
<script>
var CurrentLoc = "";
	$(document).ready(function() {
		$("#VehicleID").select2();
		$("#Source").select2();
		$("#Destination").select2();
		$("#CurrentLocation").select2();
		//$("#DriverID").select2();
		$("#VehicleStatus").select2();
		$('#paramFilter').select2();
	});
	function blinker() {
		$('.blinking').fadeOut(500);
		$('.blinking').fadeIn(500);

	}
	setInterval(blinker, 1000);
</script>

<link rel="stylesheet" href="../../dist/css/bootstrap-switch.css">
<script src="../../dist/js/bootstrap-switch.js"></script>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
  body {
	font-family:  Lato, sans-serif;
}
</style>
<style>
.save
{
	display:none;
}
.no-border {
	border: none !important;
	border: 0 !important;
}
</style>
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
	<div class="wrapper">

		<header class="main-header">
			<!-- Logo -->
			<div>
				<a href="#" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
					<span class="logo-mini"><b>Demo</b></span> <!-- logo for regular state and mobile devices -->
					<span class="logo-lg"><b>Demo</b> </span>
				</a>
			</div>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">

				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
					role="button"> <span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
				</a>
				<label class="navbar-form pull-left" role="search">
					<div class="form-group">
						<b style="padding-left: 15px; font-size: 20px;color:#fff;" class="pull-right">Vehicle Management</b><br> <b style="color:#fff;padding-left: 15px"><%= currentTime %></b>
					</div>
				</label>

				<div class="navbar-custom-menu">
					<img src="../../dist/img/Logo.png" class="logo-class pull-right"
						alt="User Image">
				</div>
			</nav>

		</header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->


				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<br>
				<ul class="sidebar-menu">
					<li class="header">MAIN NAVIGATION</li>
					<li class="treeview"><a href="#"> <i
							class="fa fa-dashboard"></i> <span>Dashboard</span>
					</a>
						<ul class="treeview-menu">
							<li class=""><a href="CMCDashboard.jsp"><i
									class="fa fa-circle-o"></i> CMC</a></li>
							<!-- <li class=""><a href="NewDashboard.jsp"><i
									class="fa fa-circle-o"></i> Vehicle Dashboard</a></li> -->
							<li class=""><a href="TodayColdChainHealth.jsp"><i
									class="fa fa-circle-o"></i> Cold Rooms Summary</a></li>
							<!--  <li><a href="VehiclePlannedRouteMap.jsp"><i class="fa fa-circle-o"></i> Vehicle Planned Route</a></li> -->
							<li class=""><a href="TodayColdChainHealthVehicle.jsp"><i
									class="fa fa-circle-o"></i> Vehicles Summary</a></li>
						</ul></li>
					<li class="treeview"><a href="#"> <i
							class="fa fa-files-o"></i> <span>Alerts</span> 
					</a>
						<ul class="treeview-menu">
							<li><a href="LiveVehicleTracking.jsp"><i
									class="fa fa-circle-o"></i> Live Vehicle Tracking</a></li>
						</ul></li>
					
					<li class="treeview active"><a href="#"> <i
							class="fa fa-database"></i> <span>Admin</span>
					</a>
						<ul class="treeview-menu">
							<li class="active"><a href="VehicleManagement.jsp"><i class="fa fa-circle-o"></i>
									Vehicle Management</a></li>
							<li><a href="VendorScorecard.jsp"><i class="fa fa-circle-o"></i>
									Vehicle Score Card</a></li>
						</ul></li>
						<li class="treeview"><a href="#"> <i
							class="fa fa-bar-chart"></i> <span>Reports</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="ParameterSummary.jsp"><i class="fa fa-circle-o"></i>
									Parameter Summary</a></li>
							<li><a href="AlertsSummary.jsp"><i class="fa fa-circle-o"></i>
									Alerts Summary</a></li>
							<li><a href="AlertsReport.jsp"><i class="fa fa-circle-o"></i>
									Alerts Report</a></li>		
							
						</ul></li>
						<li class="treeview"><a href="#"> <i
							class="fa fa-pie-chart"></i> <span>Analysis</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="BlockTemplate.jsp"><i class="fa fa-circle-o"></i>
									Temperature Profile <br>Analysis</a></li>
							<li><a href="OrganizationTemplate.jsp"><i class="fa fa-circle-o"></i>
									Excursion and Breach- <br>Root Cause Analysis</a></li>
							<li><a href="SankeyTemplate.jsp"><i class="fa fa-circle-o"></i>
									Global Supply <Br>Chain Health</a></li>
						</ul></li>
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			
		<section id="mainSection" class="content">
			<form id="mainForm" method="post" action="VehicleManagement.jsp">
			<div class="row">
				<div class="col-md-3 text-center">
					<div class="row"><div class="col-md-offset-6 col-md-5 text-center">
					<label><b>Vehicle ID</b></label>
					<select id="VehicleID" name="VehicleID" class="form-control-no-background pull-center">
											<option value="ALL">Vehicle ID</option>
											<%for(int i=0; i<sVehicleIdArray.length; i++){ 
												if(sVehicleIdParam.equals(sVehicleIdArray[i])){
												%>
  												<option value="<%=sVehicleIdArray[i] %>" selected><%=sVehicleIdArray[i] %></option>
  												<%} else { %>
  												<option value="<%=sVehicleIdArray[i] %>"><%=sVehicleIdArray[i] %></option>
  												<%} %>
  											<%}%>
											
														
										</select>
					</div>
					
					</div>
					
												</div><div class="col-md-3 text-center">
												<div class="row"><div class="col-md-6">
													<label><b>Source</b></label><br>
													<select id="Source" name="Source" class="form-control-no-background pull-center">
  														<option value="ALL">Source</option>
  														<%for(int i=0; i<sSourceArray.length; i++){ 
  															if(sSourceParam.equals(sSourceArray[i])){
  														%>
  												<option value="<%=sSourceArray[i] %>" selected><%=sSourceArray[i] %></option>
  												<%} else { %>
  												<option value="<%=sSourceArray[i] %>"><%=sSourceArray[i] %></option>
  												<%} %>
  											<%}%>
													</select></div><div class="col-md-6">
													<label><b>Destination</b></label>
													<select id="Destination" name="Destination" class="form-control-no-background pull-center">
  														<option value="ALL">Destination</option>
  														<%for(int i=0; i<sDestArray.length; i++){ 
  															if(sDestParam.equals(sDestArray[i])){
  														%>
  												<option value="<%=sDestArray[i] %>" selected><%=sDestArray[i] %></option>
  												<%} else { %>
  												<option value="<%=sDestArray[i] %>"><%=sDestArray[i] %></option>
  												<%} %>
  											<%}%>
													</select></div>
													</div>
													</div><div class="col-md-3 text-center">
													<div class="row"><div class="col-md-6">
													<label><b>Current Location</b></label>
													<select id="CurrentLocation" name="CurrentLocation" class="form-control-no-background pull-center">
  														<option value="ALL">Current Location</option>
  														<%for(int i=0; i<sCurrentLocationArray.length; i++){ 
  														if(sCurrentLocationParam.equals(sCurrentLocationArray[i])){
  														%>
  												<option value="<%=sCurrentLocationArray[i] %>" selected><%=sCurrentLocationArray[i] %></option>
  												<%} else { %>
  												<option value="<%=sCurrentLocationArray[i] %>"><%=sCurrentLocationArray[i] %></option>
  												<%} %>
  											<%}%>
													</select></div><div class="col-md-6"><label><b>Vehicle Status</b></label>
													<select id="VehicleStatus" name="VehicleStatus" class="form-control-no-background pull-center">
  														<option value="ALL">Vehicle Status</option>
  														<%for(int i=0; i<sVehicleStatusArray.length; i++){ 
  														if(sVehicleStatusParam.equals(sVehicleStatusArray[i])){
  														%>
  												<option value="<%=sVehicleStatusArray[i] %>" selected><%=sVehicleStatusArray[i] %></option>
  												<%} else { %>
  												<option value="<%=sVehicleStatusArray[i] %>"><%=sVehicleStatusArray[i] %></option>
  												<%} %>
  											<%}%>
													</select>
													</div>
								</div>
							</div>
							<div class="col-md-3 text-center">
							<div class="row">
								
													<div class="col-md-6"><br>
								<button type="submit" class="btn btn-success">GO</button>&nbsp;&nbsp;
								<button type="button" class="btn btn-success" onclick="resetAll();">RESET</button>
								</div>
							</div>
							</div>
							</div>
							<br>
							<div class="row">
							<div class="col-md-2 text-center">
				<label><b>View As</b></label><br><input name="my-checkbox" id="my-checkbox" type="checkbox" data-size="mini" onchange="ViewChange();" checked>
				<%System.out.println("sListView: " + sListView); %>
				<input type="hidden" id="showView1" name="showView1" value="<%= sListView%>">
				</div>
				<div class="col-md-2 text-center">
				<label><b># Running&nbsp;&nbsp;</b></label><br><button class="btn btn-warning"><%=iRunning %></button>
				</div>
				<div class="col-md-2 text-center">
				<label><b># Assigned&nbsp;&nbsp;</b></label><br><button class="btn btn-warning"><%=iAssigned %></button>
				</div>
				<div class="col-md-2 text-center">
				<label><b># Available&nbsp;&nbsp;</b></label><br><button class="btn btn-warning"><%=iAvailable %></button>
				</div>
				<div class="col-md-2 text-center">
				<label><b># Unavailable&nbsp;&nbsp;</b></label><br><button class="btn btn-warning"><%=iUnavailable %></button>
				</div>				
							</div>
							<br><br>
		
		<div id="listView">		
									<table id="data_table_stationary" class="table table-bordered table-striped table-condensed">
									<thead class="success">
									<th width="150">Vehicle ID</th>
									<th width="100">Start Datetime</th>
									<th width="150">Inventory Assignment</th>
									<th width="100">Source</th>
									<th width="120">Destination</th>
									<th width="100">Current Location</th>
									<th width="130">Vehicle Status</th>
									<th width="130">Trip Status</th>
									<th width="130">Load Criticality</th>
									<th width="130">Parameters To Track</th>
									<th width="60"></th>
									</thead>
									<%
									int iCount = 0;
									 int j=0; 
									TransitDriverMapBean tdmBean = new TransitDriverMapBean();
									iter = col.iterator();
									while(iter.hasNext()) {
										tvmBean = (TransitVehicleMapBean) iter.next();
										if((sVehicleIdParam.equals("ALL") || sVehicleIdParam.equals(tvmBean.getVehicle_ID())) &&
												(sSourceParam.equals("ALL") || sSourceParam.equals(tvmBean.getSource())) &&
												(sDestParam.equals("ALL") || sDestParam.equals(tvmBean.getDestination())) &&
												//(sCurrentLocationParam.equals("ALL") || sCurrentLocationParam.equals(tvmBean.getCurrentLocation())) &&
												//(sDriverIDParam.equals("ALL") || sDriverIDParam.equals(tvmBean.getDriver_DL_ID())) &&
												(sVehicleStatusParam.equals("ALL") || sVehicleStatusParam.equals(tvmBean.getVehicle_Status()))) {
										if(tvmBean.getTrip_Status().equals("Completed")) {
									%>
										<tr id="row_stationary<%= j%>" class="blinking">
									<%} else { %>
										<tr id="row_stationary<%= iCount%>" class="danger">
									<%} %>
									<td id="<%=iCount %>_Vehicle_Id"><%=tvmBean.getVehicle_ID() %><a href="LiveVehicleTracking.jsp?VehicleId=<%= tvmBean.getVehicle_ID()%>" style="color:red">Track Live</a><a data-toggle="modal" data-target="#myModal"><i class="fa fa-camera fa-color-blue"></i></a></td>
									<td id="<%=iCount %>_Assigned_Date"><%=tvmBean.getAssigned_Date() %></td>
									<td>
									<div class="row">
										<div class="col-md-3">
											<br><a data-toggle="modal" data-target="#inventory"><i class="fa fa-truck fa-color-blue"></i></a>
											<!-- <a data-toggle="popover" data-placement="right"><i class="fa fa-eye fa-color-blue"></i></a> -->
										</div>
										<div class="col-md-9">
										Utilization<input type="text" class="form-control form-control-no-height"></td>
										</div>
									</div>
									<td id="<%=iCount %>_Source"><%=tvmBean.getSource() %></td>
									<td id="<%=iCount %>_Destination"><%=tvmBean.getDestination() %></td>
									<td id="<%=iCount %>_Current_Location"><div id="<%=iCount %>_CR" name="<%=iCount %>_CR"></div></td>
									<script type="text/javascript">
									
									$.get(
											"https://reverse.geocoder.cit.api.here.com/6.2/reversegeocode.json",
											{
												app_id: 'DdeJ8sqdrenYc1k7eTH6',
												app_code: 'DBU7IBF4dzhGgtLRCx6Sgg',
												gen:'9',
												prox:'<%=tvmBean.getLatitude()%>,<%=tvmBean.getLongitude()%>,100',
												mode:'retrieveAddresses'
											},
											function(data) {
												//console.log(data);
												$.each(data.Response, function(i, item){
													var a = (data.Response.View[0].Result[0].Location.Address.City);
													$('#<%=iCount%>_CR').html(a);
													if(CurrentLoc.indexOf(a) < 0) {
														CurrentLoc = CurrentLoc + (data.Response.View[0].Result[0].Location.Address.City) + ";"
													}
													
												});
											}
										);
									</script>
									
									<%if(iCount == 0) {%>
									<td id="<%=iCount %>_Vehicle_Status">
									<a class="btn btn-info" id="1running" data-toggle="modal" data-target="#assignModal"><%=tvmBean.getVehicle_Status() %></a>
									<a class="btn btn-success" id="1available" style="display:none;" data-toggle="modal" data-target="#assignModal3">Available</a>
									<a class="btn" id="1unavailable" style="display:none;color:white;background:black;" data-toggle="modal" data-target="#assignModal4">Unavailable</a></td>
									<%} else if(iCount == 1) { %>
									<td id="<%=iCount %>_Vehicle_Status">
									<a class="btn btn-warning" id="2assigned" data-toggle="modal" data-target="#assignModal5"><%=tvmBean.getVehicle_Status() %></a>
									<a class="btn" id="2unavailable" style="display:none;color:white;background:black;" data-toggle="modal" data-target="#assignModal6">Unavailable</a></td>
									<%} else if(iCount == 2) { %>
									<td id="<%=iCount %>_Vehicle_Status">
									<a class="btn btn-success" id="3available" data-toggle="modal" data-target="#assignModal1"><%=tvmBean.getVehicle_Status() %></a>
									<a class="btn" id="3unavailable" style="display:none;color:white;background:black;" data-toggle="modal" data-target="#assignModal2">Unavailable</a></td>
									<%} else if(iCount == 3) { %>
									<td id="<%=iCount %>_Vehicle_Status">
									<a class="btn" style="color:white;background:black;" id="4unavailable"><%=tvmBean.getVehicle_Status() %></a>
									<a class="btn btn-info" id="4available" style="display:none;">Available</a></td>
									<%} else { %>
									<td id="<%=iCount %>_Vehicle_Status">
										<%if(tvmBean.getVehicle_Status().equals("Running")){ %>
										<a class="btn btn-info"><%=tvmBean.getVehicle_Status() %></a></td>
										<%} else if(tvmBean.getVehicle_Status().equals("Available")) { %>
										<a class="btn btn-success"><%=tvmBean.getVehicle_Status() %></a></td>
										<%} else if(tvmBean.getVehicle_Status().equals("Assigned")) { %>
										<a class="btn btn-warning"><%=tvmBean.getVehicle_Status() %></a></td>
										<%} else { %>
										<a class="btn" style="color:white;background:black;"><%=tvmBean.getVehicle_Status() %></a></td>
										<%} %>
									<%} %>
									<%if(iCount % 3 == 0 && tvmBean.getVehicle_Status().equals("Running")){ %>
									<td id="<%=iCount %>_Trip_Status"><div class="progress">
  										<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
    										60%
 										 </div>
										</div></td>
									<%} else if(iCount % 2 == 0 && tvmBean.getVehicle_Status().equals("Running")) { %>
									<td id="<%=iCount %>_Trip_Status"><div class="progress">
  										<div class="progress-bar" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%;">
    										40%
 										 </div>
										</div></td>
									<%} else if(!tvmBean.getVehicle_Status().equals("Running")){ %>
									<td id="<%=iCount %>_Trip_Status"><div class="progress">
  										<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
    										0%
 										 </div>
										</div></td>
									<%} else if(tvmBean.getVehicle_Status().equals("Running")){ %>
									<td id="<%=iCount %>_Trip_Status"><div class="progress">
										<div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%;">
										80%
										 </div>
									</div></td>
									<%} %> 
									<%if(iCount % 3 == 0){ %>
									<td class="text-center"><span class="badge bg-red">HIGH</span></td>
									<%} else if(iCount % 2 == 0){%>
									<td class="text-center"><span class="badge bg-orange">MEDIUM</span></td>
									<%} else {%>
									<td class="text-center"><span class="badge bg-green">LOW</span></td>
									<%} %>
									<td><select multiple="multiple" id="paramFilter<%=iCount%>">
									<option id="Location" selected>Location</option>
									<option id="Temperature">Temperature</option>
									<option id="Humidity">Humidity</option>
									<option id="Door Activity">Door Activity</option>
									<option id="Vibration">Vibration</option>
									</td>
									<td>
									<table width="100%" class="no-border"><tr class="no-border"><td class="no-border"><i id="edit_button<%=iCount %>" class="fa fa-pencil edit fa-color-blue" onclick="edit_row('<%= iCount%>')"></i></td>
									<td class="no-border"><i id="save_button<%=iCount %>" class="fa fa-floppy-o save fa-color-blue" onclick="save_row('<%=iCount%>')"></i></td>
									<td class="no-border"><a data-target="#planRouteModal" data-toggle="modal" data-toggle="tooltip" title="Plan the route"><i id="planroute_button<%=iCount %>" class="fa fa-map-marker fa-color-blue"></i></a></td>
									</tr>
									</table>
									</td>
									</tr>
									<%}
									iCount++;	
									} %>
									</table>	
									
								<br>
						<input type="hidden" id="lastValue_stationary" name="lastValue_stationary" value="<%= iCount%>">
		</div>
		<div id="mapView" style="display:none">
		<div id="map" style="width: 100%; height: 600px; background: grey;border: 2px solid #3872ac;"></div>
		</div>
		</form>
		</section>
		<div class="modal fade" id="planRouteModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Planned Stops Selection</h4>
      </div>
            <div class="modal-body">
                <div class="container">
                
                	<div class="row">
                    	<div class="col-md-6 text-center">
                    	<div class="text-center"><button type="button" class="btn btn-info" onclick="geocode1();">Show Current Map</button><br></div>
                    	<div class="row">
                    			<div class="col-md-6 text-center">Existing Location: 
                    			<select multiple="multiple" class="form-control-no-height" id="catFilter">
                    			<%Set set = tmLocations.keySet();
                    				Iterator iter1 = set.iterator();
                    				while(iter1.hasNext()) {
                    					String sKey = (String) iter1.next();
                    					clmBean = (ClientLocationMapBean) tmLocations.get(sKey);%>
  											<option><%=clmBean.getLocationName() %></option>
  											<%} %>
										</select></div>
                    			<div class="col-md-6 text-center"><br><button type="button" class="btn btn-success" onclick="geocode1();">Add Stop</button></div>
                    		</div>
                    	<hr>
                    		<div class="row">
                    			<div class="col-md-6 text-center">Address: <input type="text" id="Address" name="Address" class="form-control-no-height" placeholder="Enter Address" value=""></div>
                    			<div class="col-md-6 text-center"><br><button type="button" onclick="geocode();" class="btn btn-success">Add Stop</button></div>
                    		</div>
                    	<hr>
                    	<div class="row">
                    			<div class="col-md-3 text-center">Latitude: <input type="text" id="Latitude" name="Latitude" class="form-control-no-height" placeholder="Enter Latitude" value="">
                    	</div><div class="col-md-3 text-center">Longitude: <input type="text" id="Longitude" name="Longitude" class="form-control-no-height" placeholder="Enter Longitude" value="">
                    	</div>
                    			<div class="col-md-6 text-center"><br><button type="button" onclick="reverseGeoCode();" class="btn btn-success">Add Stop</button></div>
                    		</div>
                    	<br>
                         <div id="map1" style="width: 100%; height: 500px; background: grey;border: 2px solid #3872ac;"></div>
                         </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer text-center">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
    </div>
		<div class="modal fade" id="assignModal">
   		 <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Action</h4>
      </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row">
                    	<div class="col-md-6 text-center">
                         <button type="button" class="btn btn-success" data-dismiss="modal" onclick="doUnassign()">Unassign</button> &nbsp;&nbsp;
                         <button type="button" class="btn btn-success" data-dismiss="modal" onclick="doUnavailable()">Unavailable</button>
                         </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer text-center">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="assignModal3">
   		 <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Action</h4>
      </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row">
                    	<div class="col-md-6 text-center">
                         <button type="button" class="btn btn-success" data-dismiss="modal" onclick="doassign()">Assign</button> &nbsp;&nbsp;
                         <button type="button" class="btn btn-success" data-dismiss="modal" onclick="doUnavailable()">Unavailable</button>
                         </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer text-center">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="assignModal4">
   		 <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Action</h4>
      </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row">
                    	<div class="col-md-6 text-center">
                         <button type="button" class="btn btn-success" data-dismiss="modal" onclick="doUnassign()">Available</button> &nbsp;&nbsp;
                         </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer text-center">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="assignModal1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Action</h4>
      </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row">
                    	<div class="col-md-6 text-center">
                         <button type="button" class="btn btn-success" data-dismiss="modal" onclick="doUnavailable1()">Unavailable</button>
                         </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer text-center">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
    </div>
    
    <div class="modal fade" id="assignModal2">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Action</h4>
      </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row">
                    	<div class="col-md-6 text-center">
                         <button type="button" class="btn btn-success" data-dismiss="modal" onclick="doavailable1()">Available</button>
                         </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer text-center">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="assignModal5">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Action</h4>
      </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row">
                    	<div class="col-md-6 text-center">
                         <button type="button" class="btn btn-success" data-dismiss="modal" onclick="doUnavailable2()">Unavailable</button>
                         </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer text-center">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="inventory">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header modal-footer-center">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Inventory Assignment</h4>
      </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row">
                    	<div class="col-md-9 text-center">
                    	<div class="row">
                    		<div class="col-md-6">
                    		<div class="radio">
 							 <label>
    							<input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
    								Partial Truck Load
  							</label>
							</div>
                    		</div>
                    		<div class="col-md-6">
                    		<div class="radio">
  							<label>
    							<input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
    								Full Truck Load
  							</label>
							</div>
                    		</div>
                    	</div>
                    	
						<div id="myPopOver">
                         <table id="data_table_stationary1"
												class="table table-bordered table-striped table-condensed"
												width="100%">
												<tr>
													<th width="20%">Customer Name</th>
													<th width="20%">Product</th>
													<th width="40%"># Of Units</th>
													<th width="20%">INR Value</th>
													
												</tr>
												<%
													int jk = 0;
													
												%>
												
												<tr id="row_stationary_modal<%=jk%>">
													<td class="pull-left"><select class="form-control-no-height" id="new_custSelection" style="width:200px">
														<option id="Tropicana">Tropicana</option>
														<option id="Vadilals">Vadilals</option>
														<option id="Cornetto">Cornetto</option>
														<option id="Cadbury">Cadbury</option>
														<option id="Nutella">Nutella</option>
													</select></td>
													<td><input id="new_zone_id_stationary" type="text"
														class="form-control-no-height"></td>
													<td><div class="row">
														<div class="col-md-3">
														<input id="new_GW_client_id_stationary"
														type="text" class="form-control-no-height">
														</div>
														<div class="col-md-2">
														<select id="new_type" style="width:200px" class="form-control-no-height">
														<option id="Pallets">Pallets</option>
														<option id="Boxes">Boxes</option></select>
														</div>
													</div>
														</td>
													<td><input id="new_ND_client_id_stationary"
														type="text" class="form-control-no-height"></td>
													
													<td><button type="button"
															class="add btn btn-sm btn-success"
															onclick="add_row_stationary();" value="Add Row">Add
															Row</button></td>
												</tr>
												<%
													jk++;
												%>

											</table>
											</div>
											<br> <input type="hidden" id="lastValue_stationary1"
												name="lastValue_stationary1" value="<%=jk%>">
                         </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer modal-footer-center">
        <button type="button" class="btn btn-success" data-dismiss="modal">Save & Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
    </div>
				</div>
		<!-- /.content-wrapper -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Current Vehicle Status</h4>
      </div>
      <div class="modal-body">
       <img src="../../dist/img/map3.png">&nbsp;&nbsp;&nbsp;
       <img src="../../dist/img/reefer.png">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0
			</div>
			<strong>Copyright &copy; 2017 <a href="http://tagbox.in">TagBox</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Control Sidebar -->
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->
	
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
<script type="text/javascript">
		$("[name='my-checkbox']").bootstrapSwitch();
	</script>

	<!-- Bootstrap 3.3.6 -->
	<script src="../../bootstrap/js/bootstrap.min.js"></script>
	<!-- ChartJS 1.0.1 -->
	<!-- AdminLTE App -->
	<script src="../../dist/js/app.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="../../dist/js/demo.js"></script>


<script type="text/javascript">


//alert(CurrentLoc);
/**
 * Adds markers to the map highlighting the locations of the captials of
 * France, Italy, Germany, Spain and the United Kingdom.
 *
 * @param  {H.Map} map      A HERE Map instance within the application
 */
 

function doMap(){
//Step 2: initialize a map - this map is centered over Europe
var map = new H.Map(document.getElementById('map'),
defaultLayers.normal.map,{
center: {lat:12.993709, lng:77.681804},
zoom: 5
});

var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));

//Create the default UI components
var ui = H.ui.UI.createDefault(map, defaultLayers);

//Now use the map as required...
<%iter = col.iterator();
while(iter.hasNext()) {
	tvmBean = (TransitVehicleMapBean) iter.next();
	if((sVehicleIdParam.equals("ALL") || sVehicleIdParam.equals(tvmBean.getVehicle_ID())) &&
			(sSourceParam.equals("ALL") || sSourceParam.equals(tvmBean.getSource())) &&
			(sDestParam.equals("ALL") || sDestParam.equals(tvmBean.getDestination())) &&
			//(sCurrentLocationParam.equals("ALL") || sCurrentLocationParam.equals(tvmBean.getCurrentLocation())) &&
			//(sDriverIDParam.equals("ALL") || sDriverIDParam.equals(tvmBean.getDriver_DL_ID())) &&
			(sVehicleStatusParam.equals("ALL") || sVehicleStatusParam.equals(tvmBean.getVehicle_Status()))) {
	%>
	var newMarker = new H.map.Marker({lat:<%= tvmBean.getLatitude()%>, lng:<%= tvmBean.getLongitude()%>});
	newMarker.setData("<%=tvmBean.getVehicle_ID() %>");
	newMarker.addEventListener('tap', function (evt) {
	    // event target is the marker itself, group is a parent event target
	    // for all objects that it contains
	    var bubble =  new H.ui.InfoBubble(evt.target.getPosition(), {
	      // read custom data
	      content: evt.target.getData()
	    });
	    // show info bubble
	   ui.addBubble(bubble);
	   
	  }, false);
	
	  map.addObject(newMarker);
<%
	}
	}%>

 }
 
 for(var i=0;i<50;i++){
	 $('#paramFilter'+i).select2();
	 
 }
</script>

	</body>
	<script type="text/javascript">
	$('[data-toggle=popover]').popover({
			container: 'body',
		   content: $('#myPopOver').html(),
		   html: true

		}).click(function() {
		   $(this).popover('show');
		});
	
	if($("#showView1").val() == null || $("#showView1").val() == "null" || $("#showView1").val() == "list"){
		$("[name='my-checkbox']").bootstrapSwitch();
		$('#listView').show();
		$('#mapView').hide();
	} else {
		$("[name='my-checkbox']").bootstrapSwitch('toggleState');
		$('#listView').hide();
  		$('#mapView').show();
  		doMap();
	}
	
	$.get(
			"https://reverse.geocoder.cit.api.here.com/6.2/reversegeocode.json",
			{
				app_id: 'DdeJ8sqdrenYc1k7eTH6',
				app_code: 'DBU7IBF4dzhGgtLRCx6Sgg',
				gen:'9',
				prox:'12.993709,77.681804,100',
				mode:'retrieveAddresses'
			},
			function(data) {
				console.log(data);
				$.each(data.Response, function(i, item){
					//console.log(data.Response.View[0].Result[0].Location.Address.City);
					
					//getVideos(playlistId, localStorage.getItem('maxResults'));
				});
			}
		);
	
	
</script>
</html>
