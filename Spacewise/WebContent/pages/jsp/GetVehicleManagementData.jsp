<%@ page errorPage="Error.jsp" %>
<%@ page import="com.spacewise.action.LocationAction"%>
<%@ page import="com.spacewise.action.AlertAction"%>
<%@ page import="com.spacewise.dao.ClientLocationMapDao"%>
<%@ page import="com.spacewise.bean.SubLocationBean"%>
<%@ page import="com.spacewise.bean.StationaryNodeMapBean"%>
<%@ page import="com.spacewise.bean.ZoneBean"%>
<%@ page import="java.util.TreeMap"%>
<%@ page import="java.util.Vector"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.util.Collection"%>
<%@ page import="com.spacewise.bean.StationaryTemperatureBean"%>
<%@ page import="com.spacewise.bean.StationaryDoorActivityBean"%>
<%@ page import="com.spacewise.dao.StationaryTemperatureDao"%>
<%@ page import="com.spacewise.dao.TransitVehicleMapDao"%>
<%@ page import="com.spacewise.bean.StationaryMeterEnergyBean"%>
<%@ page import="com.spacewise.dao.StationaryMeterEnergyDao"%>
<%@ page import="java.util.Date"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Calendar"%>
<%@ page import="java.util.GregorianCalendar"%>
<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>

<!-- <?xml version="1.0" encoding="ISO-8859-1"?> -->

<%!static final Logger logger = LoggerFactory.getLogger("GetVehicleManagementData");
	LocationAction locAction = new LocationAction();
	AlertAction alertAction = new AlertAction();
	String buf = "";
	String sString = "";
	String sErrorMessage = null;
	String sId = null;
	String sRowId = null;
	String sSubLocId = null;
	SubLocationBean subLocBean = new SubLocationBean();
	ClientLocationMapDao clientDao = new ClientLocationMapDao();
	ZoneBean zoneBean = new ZoneBean();
	StationaryNodeMapBean stationaryNodeMapBean = new StationaryNodeMapBean();
	String temperature = "", temperature1 = "", temperature2 = "",
			temperature3 = "", time = "", time1 = "", time2 = "", time3 = "";
	StationaryTemperatureBean stBean = new StationaryTemperatureBean();
	StationaryTemperatureDao stDao = new StationaryTemperatureDao();
	StationaryDoorActivityBean sdBean = new StationaryDoorActivityBean();
	TransitVehicleMapDao tvmDao = new TransitVehicleMapDao();
	StationaryMeterEnergyBean smBean = new StationaryMeterEnergyBean();
	StationaryMeterEnergyDao smDao = new StationaryMeterEnergyDao();
	TreeMap tm = new TreeMap();
	List<String> al = new ArrayList<String>();%>
<%
	try {
		sString = request.getParameter("query");
		if (sString.equals("updateVehicleDetails")) {
			
			String sVehicle_Id = request.getParameter("Vehicle_Id");
			String sSource = request.getParameter("Source");
			String sDest = request.getParameter("Destination");
			String sDriverId = request.getParameter("Driver_Id");
			//logger.info("AlertId: " + sAlertId + " Action: " + sAction);
			buf = "";
			//response.setContentType("text/xml");
			//buf += ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			tvmDao.updateVehicleDetails(sVehicle_Id, sSource, sDest, sDriverId);

			buf = "<rows>";
			buf += ("<row>");
			buf += ("<Done>Success</Done>");
			buf += ("</row>");
			buf += "</rows>";
			buf = buf.trim();
			//logger.info(buf);
			out.clear();
			out.println(buf);
		} else if (sString.equals("RootCause")) {
			String sAlertId = request.getParameter("alertId");
			String sRoot = request.getParameter("root");
			String sPreventive = request.getParameter("preventive");
			//logger.info("AlertId: " + sAlertId + " root: " + sRoot + " pre: " + sPreventive);
			buf = "";
			//response.setContentType("text/xml");
			//buf += ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			alertAction.updateRoot(sRoot, sPreventive, sAlertId);
			buf = "<rows>";

			buf += ("<row>");
			buf += ("<Done>Success</Done>");
			buf += ("</row>");

			buf += "</rows>";
			buf = buf.trim();
			//logger.info(buf);
			out.clear();
			out.println(buf);
		}
	} catch (Exception e) {
		logger.error("Exception: " + e.getLocalizedMessage());
		e.printStackTrace();
	}
%>