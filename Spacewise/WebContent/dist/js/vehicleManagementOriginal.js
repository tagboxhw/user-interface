var map1;
var geo1 = "", geo2 = "", geo3 = "";
var endMarker;
 function doMap1(lat1, lon1){
	 var imageBlackSource = "http://www.googlemapsmarkers.com/v1/S/000000/FFFFFF/000000";
	    var imageBlackDest = "http://www.googlemapsmarkers.com/v1/D/000000/FFFFFF/000000";
	    var imageBlackPlan = "http://www.googlemapsmarkers.com/v1/P/000000/FFFFFF/000000";
	    var myIcon = new H.map.Icon(imageBlackSource);
	    var myIcon1 = new H.map.Icon(imageBlackDest);
	    var myIcon2 = new H.map.Icon(imageBlackPlan);
	 if(map1 == null) {
		 map1 = new H.Map(document.getElementById('map1'),
		defaultLayers.normal.map,{
		center: {lat:lat1, lng:lon1},
		zoom: 5
		});

		var behavior1 = new H.mapevents.Behavior(new H.mapevents.MapEvents(map1));

		//Create the default UI components
		var ui1 = H.ui.UI.createDefault(map1, defaultLayers);
		//var parisMarker = new H.map.Marker({lat:lat1, lng:lon1});
		//  map1.addObject(parisMarker);
		 
		// Create the parameters for the routing request:
		var routingParameters = {
		  // The routing mode:
		  'mode': 'fastest;car',
		  // The start point of the route:
		  'waypoint0': 'geo!12.9723,77.6449',
		  // The end point of the route:
		  'waypoint1': 'geo!9.9112151,78.1320462',
		  // To retrieve the shape of the route we choose the route
		  // representation mode 'display'
		  'representation': 'display'
		};

		// Define a callback function to process the routing response:
		var onResult = function(result) {
		  var route,
		    routeShape,
		    startPoint,
		    endPoint,
		    strip;
		  if(result.response.route) {
		  // Pick the first route from the response:
		  route = result.response.route[0];
		  // Pick the route's shape:
		  routeShape = route.shape;

		  // Create a strip to use as a point source for the route line
		  strip = new H.geo.Strip();

		  // Push all the points in the shape into the strip:
		  routeShape.forEach(function(point) {
		    var parts = point.split(',');
		    strip.pushLatLngAlt(parts[0], parts[1]);
		  });

		  // Retrieve the mapped positions of the requested waypoints:
		  startPoint = route.waypoint[0].mappedPosition;
		  endPoint = route.waypoint[1].mappedPosition;

		  
		  // Create a polyline to display the route:
		  var routeLine = new H.map.Polyline(strip, {
		    style: { strokeColor: 'blue', lineWidth: 5 }
		  });

		  // Create a marker for the start point:
		  var startMarker = new H.map.Marker({
		    lat: startPoint.latitude,
		    lng: startPoint.longitude
		  }, {icon: myIcon});

		  // Create a marker for the end point:
		  var endMarker = new H.map.Marker({
		    lat: endPoint.latitude,
		    lng: endPoint.longitude
		  }, {icon: myIcon1});

		

		  // Add the route polyline and the two markers to the map:
		  map1.addObjects([routeLine, startMarker, endMarker]);
		 
		  // Set the map's viewport to make the whole route visible:
		  map1.setViewBounds(routeLine.getBounds());
		  }
		};

		// Get an instance of the routing service:
		var router = platform.getRoutingService();

		// Call calculateRoute() with the routing parameters,
		// the callback and an error callback function (called if a
		// communication error occurs):
		router.calculateRoute(routingParameters, onResult,
		  function(error) {
		    alert(error.message);
		  });
	

	 } else {
		// alert("here");
		// var parisMarker = new H.map.Marker({lat:lat1, lng:lon1});
		 // map1.addObject(parisMarker);
		
		  if(geo1 == "") {
			  geo1 = "geo!"+lat1+","+lon1;
			  //alert(geo1);
			  routingParameters = {
					  // The routing mode:
					  'mode': 'fastest;car',
					  // The start point of the route:
					  'waypoint0': 'geo!12.9723,77.6449',
					  'waypoint1':geo1,
					  // The end point of the route:
					  'waypoint2': 'geo!9.9112151,78.1320462',
					  // To retrieve the shape of the route we choose the route
					  // representation mode 'display'
					  'representation': 'display'
					};
			// Define a callback function to process the routing response:
				var onResult = function(result) {
				  var route,
				    routeShape,
				    startPoint,
				    endPoint,
				    strip;
				  if(result.response.route) {
				  // Pick the first route from the response:
				  route = result.response.route[0];
				  // Pick the route's shape:
				  routeShape = route.shape;

				  // Create a strip to use as a point source for the route line
				  strip = new H.geo.Strip();

				  // Push all the points in the shape into the strip:
				  routeShape.forEach(function(point) {
				    var parts = point.split(',');
				    strip.pushLatLngAlt(parts[0], parts[1]);
				  });

				  // Retrieve the mapped positions of the requested waypoints:
				  startPoint = route.waypoint[0].mappedPosition;
				  endPoint = route.waypoint[2].mappedPosition;
				  middlePoint = route.waypoint[1].mappedPosition;
				  
				  // Create a polyline to display the route:
				  routeLine = new H.map.Polyline(strip, {
				    style: { strokeColor: 'blue', lineWidth: 5 }
				  });

				  // Create a marker for the start point:
				  var startMarker = new H.map.Marker({
				    lat: startPoint.latitude,
				    lng: startPoint.longitude
				  }, {icon: myIcon});

				  // Create a marker for the end point:
				  var endMarker = new H.map.Marker({
				    lat: endPoint.latitude,
				    lng: endPoint.longitude
				  }, {icon: myIcon1});

				// Create a marker for the end point:
				  var middleMarker = new H.map.Marker({
				    lat: middlePoint.latitude,
				    lng: middlePoint.longitude
				  }, {icon: myIcon2});

				  // Add the route polyline and the two markers to the map:
				  map1.addObjects([routeLine, startMarker, middleMarker, endMarker]);
				  map1.addObjects([routeLine, middleMarker, endMarker]);
				  // Set the map's viewport to make the whole route visible:
				  map1.setViewBounds(routeLine.getBounds());
				  }
				};
		  }
		  else if(geo2 == "") {
			  geo2 = "geo!"+lat1+","+lon1;
			  routingParameters = {
					  // The routing mode:
					  'mode': 'fastest;car',
					  // The start point of the route:
					  'waypoint0': 'geo!12.9723,77.6449',
					  'waypoint1':geo1,
					  'waypoint2':geo2,
					  // The end point of the route:
					  'waypoint3': 'geo!9.9112151,78.1320462',
					  // To retrieve the shape of the route we choose the route
					  // representation mode 'display'
					  'representation': 'display'
					};
			// Define a callback function to process the routing response:
				var onResult = function(result) {
				  var route,
				    routeShape,
				    startPoint,
				    endPoint,
				    strip;
				  if(result.response.route) {
				  // Pick the first route from the response:
				  route = result.response.route[0];
				  // Pick the route's shape:
				  routeShape = route.shape;

				  // Create a strip to use as a point source for the route line
				  strip = new H.geo.Strip();

				  // Push all the points in the shape into the strip:
				  routeShape.forEach(function(point) {
				    var parts = point.split(',');
				    strip.pushLatLngAlt(parts[0], parts[1]);
				  });

				  // Retrieve the mapped positions of the requested waypoints:
				  startPoint = route.waypoint[0].mappedPosition;
				  endPoint = route.waypoint[3].mappedPosition;
				  middlepoint = route.waypoint[1].mappedPosition;
				  middlepoint1 = route.waypoint[2].mappedPosition;
				  // Create a polyline to display the route:
				  var routeLine = new H.map.Polyline(strip, {
				    style: { strokeColor: 'blue', lineWidth: 5 }
				  });

				  // Create a marker for the start point:
				  var startMarker = new H.map.Marker({
				    lat: startPoint.latitude,
				    lng: startPoint.longitude
				  }, {icon: myIcon});

				  // Create a marker for the end point:
				  var endMarker = new H.map.Marker({
				    lat: endPoint.latitude,
				    lng: endPoint.longitude
				  }, {icon: myIcon1});

				// Create a marker for the end point:
				  var middleMarker = new H.map.Marker({
				    lat: middlePoint.latitude,
				    lng: middlePoint.longitude
				  }, {icon: myIcon2});
				  
				// Create a marker for the end point:
				  var middleMarker1 = new H.map.Marker({
				    lat: middlePoint1.latitude,
				    lng: middlePoint1.longitude
				  }, {icon: myIcon2});
				

				  // Add the route polyline and the two markers to the map:
				  map1.addObjects([routeLine, startMarker, middleMarker, middleMarker1, endMarker]);
				 
				  // Set the map's viewport to make the whole route visible:
				  map1.setViewBounds(routeLine.getBounds());
				  }
				};
		  }
		  else if(geo3 == "") {
			  geo3 = "geo!"+lat1+","+lon1;
			  routingParameters = {
					  // The routing mode:
					  'mode': 'fastest;car',
					  // The start point of the route:
					  'waypoint0': 'geo!13.0983079,80.1685787',
					  'waypoint1':geo1,
					  'waypoint2':geo2,
					  'waypoint3':geo3,
					  // The end point of the route:
					  'waypoint4': 'geo!9.9112151,78.1320462',
					  // To retrieve the shape of the route we choose the route
					  // representation mode 'display'
					  'representation': 'display'
					};
			// Define a callback function to process the routing response:
				var onResult = function(result) {
				  var route,
				    routeShape,
				    startPoint,
				    endPoint,
				    strip;
				  if(result.response.route) {
				  // Pick the first route from the response:
				  route = result.response.route[0];
				  // Pick the route's shape:
				  routeShape = route.shape;

				  // Create a strip to use as a point source for the route line
				  strip = new H.geo.Strip();

				  // Push all the points in the shape into the strip:
				  routeShape.forEach(function(point) {
				    var parts = point.split(',');
				    strip.pushLatLngAlt(parts[0], parts[1]);
				  });

				  // Retrieve the mapped positions of the requested waypoints:
				  startPoint = route.waypoint[0].mappedPosition;
				  endPoint = route.waypoint[1].mappedPosition;

				  
				  // Create a polyline to display the route:
				  var routeLine = new H.map.Polyline(strip, {
				    style: { strokeColor: 'blue', lineWidth: 5 }
				  });

				  // Create a marker for the start point:
				  var startMarker = new H.map.Marker({
				    lat: startPoint.latitude,
				    lng: startPoint.longitude
				  }, {icon: myIcon});

				  // Create a marker for the end point:
				  var endMarker = new H.map.Marker({
				    lat: endPoint.latitude,
				    lng: endPoint.longitude
				  }, {icon: myIcon1});

				

				  // Add the route polyline and the two markers to the map:
				  map1.addObjects([routeLine, startMarker, endMarker]);
				 
				  // Set the map's viewport to make the whole route visible:
				  map1.setViewBounds(routeLine.getBounds());
				  }
				};
		  }
		 

				

				// Get an instance of the routing service:
				var router = platform.getRoutingService();

				// Call calculateRoute() with the routing parameters,
				// the callback and an error callback function (called if a
				// communication error occurs):
				router.calculateRoute(routingParameters, onResult,
				  function(error) {
				    alert(error.message);
				  });
			
	 }
		
 }
 

 function geocode() {
	  var geocoder = platform.getGeocodingService(),
	    parameters = {
	      searchtext: $('#Address').val(),
	      gen: '8'};

	  geocoder.geocode(parameters,
	    function (result) {
	      //console.log(result);
	      //console.log(result.Response.View[0].Result[0].Location.DisplayPosition.Latitude);
	      //console.log(result.Response.View[0].Result[0].Location.DisplayPosition.Longitude);
	      doMap1(result.Response.View[0].Result[0].Location.DisplayPosition.Latitude, result.Response.View[0].Result[0].Location.DisplayPosition.Longitude)
	    }, function (error) {
	      alert(error);
	    });
	}

 function geocode1() {
	  var geocoder = platform.getGeocodingService(),
	    parameters = {
	      searchtext: $('#Address').val(),
	      gen: '8'};

	  geocoder.geocode(parameters,
	    function (result) {
	      //console.log(result);
	      //console.log(result.Response.View[0].Result[0].Location.DisplayPosition.Latitude);
	      //console.log(result.Response.View[0].Result[0].Location.DisplayPosition.Longitude);
	      doMap1('11.0205629','76.9784027');
	    }, function (error) {
	      alert(error);
	    });
	}
 function reverseGeoCode(){
	 doMap1($('#Latitude').val(), $('#Longitude').val());
 }
function ViewChange(){
  	var a = (document.getElementById("my-checkbox").checked);
  	if(a == false){
  		$('#listView').hide();
  		$('#mapView').show();
  		$('#showView1').val("map");
  		doMap();
  	} else {
  		$('#listView').show();
  		$('#mapView').hide();
  		$('#showView1').val("list");
  	}
  	//alert(a);
  }
  

function edit_row(no)
{
 document.getElementById("edit_button"+no).style.display="none";
 document.getElementById("save_button"+no).style.display="block";
 //document.getElementById("cancel_button"+no).style.display="block";
 
 var destination=document.getElementById(no+"_Destination");
 var driver_id=document.getElementById(no+"_Vehicle_Type");
 var source=document.getElementById(no+"_Source");	
 var destination_data=destination.innerHTML;
 var driver_id_data=driver_id.innerHTML;
 var source_data=source.innerHTML;

 var select1 = document.getElementById("Source");
 var a = "<select id='source_text"+no+"' name='source_text"+no + "'>" + select1.innerHTML + "</select>";
 source.innerHTML = a;
 $('#source_text'+no).val(source_data);
 
 var select1 = document.getElementById("Destination");
 var a = "<select id='destination_text"+no+"' name='destination_text"+no + "'>" + select1.innerHTML + "</select>";
 destination.innerHTML = a;
 $('#destination_text'+no).val(destination_data);
 
 var select1 = document.getElementById("VehicleType");
 var a = "<select id='driver_id_text"+no+"' name='driver_id_text"+no + "'>" + select1.innerHTML + "</select>";
 driver_id.innerHTML = a;
 $('#driver_id_text'+no).val(driver_id_data);
 
}

function save_row(no)
{

	var vehicle_id=document.getElementById(no+"_Vehicle_Id");	
	 var vehicle_id_data=vehicle_id.innerHTML;
	 
var destination_val=document.getElementById("destination_text"+no).value;
 var driver_id_val=document.getElementById("driver_id_text"+no).value;
 var source_val=document.getElementById("source_text"+no).value;
 
 
 if(source_val == "ALL") { alert("Please enter a Source"); document.getElementById("source_text"+no).focus(); return;}
 if(destination_val == "ALL") { alert("Please enter a Destination"); document.getElementById("destination_text"+no).focus(); return;}
 if(driver_id_val == "ALL") { alert("Please enter a Transporter Name"); document.getElementById("driver_id_text"+no).focus(); return;}
 
 document.getElementById(no+"_Destination").innerHTML=destination_val;
 document.getElementById(no+"_Vehicle_Type").innerHTML=driver_id_val;
 document.getElementById(no+"_Source").innerHTML=source_val;
 
 document.getElementById("edit_button"+no).style.display="block";
 document.getElementById("save_button"+no).style.display="none";
 //document.getElementById("cancel_button"+no).style.display="none";
 var posting = $.post("../../pages/jsp/GetVehicleManagementData.jsp",
			{
				Vehicle_Id:vehicle_id_data,
				Destination:destination_val,
				Source:source_val,
				Driver_Id:driver_id_val,
				query:"updateVehicleDetails"
			},
			function(data,status){
				//alert(data);
				xmlDoc = loadXMLString(data);
				if($(xmlDoc).find("rows").length == 0) {
					return;
				}
				
				$(xmlDoc).find("row").each(function(){
					var id = $(this).find("Return").text();
				});
				
			});
}

function doUnassign(){
	$('#1running').hide();
	$('#1available').show();
	$('#1unavailable').hide();
}

function doassign(){
	$('#1running').show();
	$('#1available').hide();
	$('#1unavailable').hide();
}

function doUnassign4(){
	$('#1running').show();
	$('#1available').hide();
	$('#1unavailable').hide();
}

function doUnavailable4(){
	$('#1running').show();
	$('#1available').hide();
	$('#1unavailable').hide();
}

function doUnavailable(){
	$('#1running').hide();
	$('#1available').hide();
	$('#1unavailable').show();
}

function doUnavailable1(){
	$('#3available').hide();
	$('#3unavailable').show();
}

function doUnavailable2(){
	$('#2assigned').hide();
	$('#2unavailable').show();
}

function doavailable1(){
	$('#3available').show();
	$('#3unavailable').hide();
}

function resetAll(){
	$("#VehicleID").val("ALL");
	$("#VehicleType").val("ALL");
	$("#Source").val("ALL");
	$("#Destination").val("ALL");
	$("#CurrentLocation").val("ALL");
	$("#DriverID").val("ALL");
	$("#VehicleStatus").val("ALL");
	document.forms["mainForm"].submit();
}

function loadXMLString(txt) 
{
	if (window.DOMParser)
	{
		parser=new DOMParser();
		xmlDoc=parser.parseFromString(txt,"text/xml");
	}
	else // Internet Explorer
	{
		xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
		xmlDoc.async=false;
		xmlDoc.loadXML(txt); 
	}
	return xmlDoc;
}