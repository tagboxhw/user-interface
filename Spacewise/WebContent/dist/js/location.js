function reloadSubLoc(){
	$('#subLocationsFilter').empty();
	var b = false;
	var posting = $.post("../../pages/jsp/GetLocationData.jsp",
				{
					locId:$('#locationsFilter').val(),
					query:"subLoc"
				},
				function(data,status){
					//alert(data);
					xmlDoc = loadXMLString(data);
					if($(xmlDoc).find("rows").length == 0) {
						$('#subLocationsFilter').append('<option value=0>None</option>');
						return;
					}
					var i =0;
					$(xmlDoc).find("row").each(function(){
						b = true;
						var id = $(this).find("Id").text();
						var name = $(this).find("Name").text();
						if (name == "None") id = "0";
						if(i == 0){
						 $('#subLocationsFilter').append('<option value=' + id + ' selected>' + name + '</option>');
						 i++;
						} else {
							$('#subLocationsFilter').append('<option value=' + id + '>' + name + '</option>');
						}
					});
					if(!b){
						$('#subLocationsFilter').append('<option value=0>None</option>');
					}
				});
	posting.done(function() {
		reloadZone();
	});
}

function reloadZone() {
	$('#zonesFilter').empty();
	var b = false;
	var posting = $.post("../../pages/jsp/GetLocationData.jsp",
				{
					locId:$('#locationsFilter').val(),
					query:"zone",
					subLocId:$('#subLocationsFilter').val()
				},
				function(data,status){
					//alert(data);
					xmlDoc = loadXMLString(data);
					if($(xmlDoc).find("rows").length == 0) {
						$('#zonesFilter').append('<option value=0>None</option>');
						return;
					}
					var i = 0;
					$(xmlDoc).find("row").each(function(){
						b = true;
						var id = $(this).find("Id").text();
						var name = $(this).find("Name").text();
						if (name == "None") id = "0";
						if(i == 0) {
						 $('#zonesFilter').append('<option value=' + id + ' selected>' + name + '</option>');
						 i++;
						} else {
							$('#zonesFilter').append('<option value=' + id + '>' + name + '</option>');
						}
					});
					if(!b){
						$('#zonesFilter').append('<option value=0>None</option>');
					}
				});
	posting.done(function() {
		if($('#nodePresent').val() == "yes") {
			reloadNodes();
		}
	});
}

function reloadNodes() {
	$('#nodeFilter').empty();
	var b = false;
	var posting = $.post("../../pages/jsp/GetLocationData.jsp",
				{
					locId:$('#zonesFilter').val(),
					query:"node"
				},
				function(data,status){
					//alert(data);
					xmlDoc = loadXMLString(data);
					if($(xmlDoc).find("rows").length == 0) {
						$('#nodeFilter').append('<option value=0>None</option>');
						return;
					}
					var i = 0;
					$(xmlDoc).find("row").each(function(){
						b = true;
						var id = $(this).find("Id").text();
						var name = $(this).find("Name").text();
						if (name == "None") id = "0";
						
						if(i == 0){
						 $('#nodeFilter').append('<option value=' + id + ' selected>' + name + '</option>');
						 i++;
						} else {
							$('#nodeFilter').append('<option value=' + id + '>' + name + '</option>');
						}
					});
					if(!b){
						$('#nodeFilter').append('<option value=0>None</option>');
					}
				});  
	posting.done(function() {
		//updateCharts();
	});
}

function reloadZone1() {
	$('#zonesFilter').empty();
	var b = false;
	var posting = $.post("../../pages/jsp/GetLocationData.jsp",
				{
					locId:$('#locationsFilter').val(),
					query:"zone",
					subLocId:$('#subLocationsFilter').val()
				},
				function(data,status){
					//alert(data);
					xmlDoc = loadXMLString(data);
					if($(xmlDoc).find("rows").length == 0) {
						$('#zonesFilter').append('<option value=0>None</option>');
						return;
					}
					var i = 0;
					$(xmlDoc).find("row").each(function(){
						b = true;
						var id = $(this).find("Id").text();
						var name = $(this).find("Name").text();
						if (name == "None") id = "0";
						if(i == 0) {
						 $('#zonesFilter').append('<option value=' + id + ' selected>' + name + '</option>');
						 i++;
						} else {
							$('#zonesFilter').append('<option value=' + id + '>' + name + '</option>');
						}
					});
					if(!b){
						$('#zonesFilter').append('<option value=0>None</option>');
					}
				});
	posting.done(function() {
		if($('#nodePresent').val() == "yes") {
			reloadNodes1();
		}
	});
}

function reloadNodes1() {
	$('#nodeFilter').empty();
	var b = false;
	var posting = $.post("../../pages/jsp/GetLocationData.jsp",
				{
					locId:$('#zonesFilter').val(),
					query:"node"
				},
				function(data,status){
					//alert(data);
					xmlDoc = loadXMLString(data);
					if($(xmlDoc).find("rows").length == 0) {
						$('#nodeFilter').append('<option value=0>None</option>');
						return;
					}
					var i = 0;
					$(xmlDoc).find("row").each(function(){
						b = true;
						var id = $(this).find("Id").text();
						var name = $(this).find("Name").text();
						if (name == "None") id = "0";
						
						if(i == 0){
						 $('#nodeFilter').append('<option value=' + id + ' selected>' + name + '</option>');
						 i++;
						} else {
							$('#nodeFilter').append('<option value=' + id + '>' + name + '</option>');
						}
					});
					if(!b){
						$('#nodeFilter').append('<option value=0>None</option>');
					}
				});  
	posting.done(function() {
		SubmitPage();
	});
}

function reloadZone(sZone, sNode) {
	$('#zonesFilter').empty();
	var b = false;
	var posting = $.post("../../pages/jsp/GetLocationData.jsp",
				{
					locId:$('#locationsFilter').val(),
					query:"zone",
					subLocId:$('#subLocationsFilter').val()
				},
				function(data,status){
					//alert(data);
					xmlDoc = loadXMLString(data);
					if($(xmlDoc).find("rows").length == 0) {
						$('#zonesFilter').append('<option value=0>None</option>');
						return;
					}
					$(xmlDoc).find("row").each(function(){
						b = true;
						var id = $(this).find("Id").text();
						var name = $(this).find("Name").text();
						if (name == "None") id = "0";
						
						if(id == sZone) {
							$('#zonesFilter').append('<option value=' + id + ' selected>' + name + '</option>');
						} else {
							$('#zonesFilter').append('<option value=' + id + '>' + name + '</option>');
						}
					});
					if(!b){
						$('#zonesFilter').append('<option value=0>None</option>');
					}
				});
	posting.done(function() {
		if($('#nodePresent').val() == "yes") {
			reloadNodes(sNode);
		}
	});
}

function reloadNodes(sNode) {
	$('#nodeFilter').empty();
	var b = false;
	var posting = $.post("../../pages/jsp/GetLocationData.jsp",
				{
					locId:$('#zonesFilter').val(),
					query:"node"
				},
				function(data,status){
					//alert(data);
					xmlDoc = loadXMLString(data);
					if($(xmlDoc).find("rows").length == 0) {
						$('#nodeFilter').append('<option value=0>None</option>');
						return;
					}
					$(xmlDoc).find("row").each(function(){
						b = true;
						var id = $(this).find("Id").text();
						var name = $(this).find("Name").text();
						if (name == "None") id = "0";
						
						if(id == sNode) {
							$('#nodeFilter').append('<option value=' + id + ' selected>' + name + '</option>');
						} else {
							$('#nodeFilter').append('<option value=' + id + '>' + name + '</option>');
						}
					});
					if(!b){
						$('#nodeFilter').append('<option value=0>None</option>');
					}
				});  
	posting.done(function() {
		//SubmitPage();
	});
}

function getSourceLocs(){
	$('#vehicleSourceCity').empty();
	var b = false;
	var posting = $.post("../../pages/jsp/GetLocationData.jsp",
				{
					query:"GetAllSourceLocs"
				},
				function(data,status){
					//alert(data);
					xmlDoc = loadXMLString(data);
					if($(xmlDoc).find("rows").length == 0) {
						$('#vehicleSourceCity').append('<option value=0>None</option>');
						return;
					}
					
					$(xmlDoc).find("row").each(function(){
						b = true;
						var id = $(this).find("City").text();
						 $('#vehicleSourceCity').append('<option value=' + id + '>' + id + '</option>');
					});
					if(!b){
						$('#vehicleSourceCity').append('<option value=0>None</option>');
					}
				});
	posting.done(function() {
		getSourceIds();
	});
}

function getSourceIds(){
	$('#vehicleSourceId').empty();
	var b = false;
	var posting = $.post("../../pages/jsp/GetLocationData.jsp",
				{
					Source:$('#vehicleSourceCity').val(),
					query:"GetAllSourceIds"
				},
				function(data,status){
					//alert(data);
					xmlDoc = loadXMLString(data);
					if($(xmlDoc).find("rows").length == 0) {
						$('#vehicleSourceId').append('<option value=0>None</option>');
						return;
					}
					
					$(xmlDoc).find("row").each(function(){
						b = true;
						var id = $(this).find("LocationId").text();
						var name = $(this).find("LocationName").text();
						 $('#vehicleSourceId').append('<option value=' + id + '>' + name + '</option>');
					});
					if(!b){
						$('#vehicleSourceId').append('<option value=0>None</option>');
					}
				});
	posting.done(function() {
		getDestLocs();
	});
}

function getDestLocs(){
	$('#vehicleDestCity').empty();
	var b = false;
	var posting = $.post("../../pages/jsp/GetLocationData.jsp",
				{
					LocationId:$('#vehicleSourceId').val(),
					query:"GetAllDestCity"
				},
				function(data,status){
					//alert(data);
					xmlDoc = loadXMLString(data);
					if($(xmlDoc).find("rows").length == 0) {
						$('#vehicleDestCity').append('<option value=0>None</option>');
						return;
					}
					
					$(xmlDoc).find("row").each(function(){
						b = true;
						var id = $(this).find("DestCity").text();
						 $('#vehicleDestCity').append('<option value=' + id + '>' + id + '</option>');
					});
					if(!b){
						$('#vehicleDestCity').append('<option value=0>None</option>');
					}
				});
	posting.done(function() {
		getDestIds();
	});
}

function getDestIds(){
	$('#vehicleDestId').empty();
	var b = false;
	var posting = $.post("../../pages/jsp/GetLocationData.jsp",
				{
					DestCity:$('#vehicleDestCity').val(),
					query:"GetAllDestId"
				},
				function(data,status){
					//alert(data);
					xmlDoc = loadXMLString(data);
					if($(xmlDoc).find("rows").length == 0) {
						$('#vehicleDestId').append('<option value=0>None</option>');
						return;
					}
					
					$(xmlDoc).find("row").each(function(){
						b = true;
						var id = $(this).find("LocationId").text();
						var name = $(this).find("LocationName").text();
						 $('#vehicleDestId').append('<option value=' + id + '>' + name + '</option>');
					});
					if(!b){
						$('#vehicleDestId').append('<option value=0>None</option>');
					}
				});
	posting.done(function() {
		getVehicleId();
	});
}

function getVehicleId(){
	$('#e1').empty();
	var b = false;
	var posting = $.post("../../pages/jsp/GetLocationData.jsp",
				{
					SourceId:$('#vehicleSourceId').val(),
					DestId:$('#vehicleDestId').val(),
					query:"GetAllVehicleIds"
				},
				function(data,status){
					//alert(data);
					xmlDoc = loadXMLString(data);
					if($(xmlDoc).find("rows").length == 0) {
						$('#e1').append('<option value=0>None</option>');
						return;
					}
					
					$(xmlDoc).find("row").each(function(){
						b = true;
						var id = $(this).find("VehicleId").text();
						 $('#e1').append('<option value=' + id + '>' + id + '</option>');
					});
					if(!b){
						$('#e1').append('<option value=0>None</option>');
					}
				});
}

function submitVehicleId() {
	//alert($("#e1").val());
	if($("#e1").val() == "Select Vehicle" || $("#e1").val() == "0"){
		$("#error-message").html("Please select a Vehicle!");
		$("#error-message").show();
		return;
	}
	window.location = "LocalRunsDetail.jsp?VehicleId=" + $("#e1").val();
			
}


function updateAction(sAlertId, sAction) {
	var posting = $.post("../../pages/jsp/GetLocationData.jsp",
				{
					alertId:sAlertId,
					query:"Action",
					action:sAction
				},
				function(data,status){
					//alert(data);
					xmlDoc = loadXMLString(data);
					if($(xmlDoc).find("rows").length == 0) {
						return;
					}
					
					$(xmlDoc).find("row").each(function(){
						var id = $(this).find("Done").text();
					});
				});
	$('#btn'+sAlertId).val(sAction);
}


function updateRoot(sAlertId) {
	var posting = $.post("../../pages/jsp/GetLocationData.jsp",
			{
				alertId:sAlertId,
				root:$('#txtRoot'+sAlertId).val(),
				preventive:$('#txtPre'+sAlertId).val(),
				query:"RootCause"
			},
			function(data,status){
				//alert(data);
				xmlDoc = loadXMLString(data);
				if($(xmlDoc).find("rows").length == 0) {
					return;
				}
				
				$(xmlDoc).find("row").each(function(){
					var id = $(this).find("Done").text();
				});
			});
}

function updateThresholdAndPhoneNumber() {
	$('#errormsg').hide();
	$('#confirmmsg').hide();
	var ph = $('#PhoneNum').val();
	if(ph.length == 10) { ph = "0" + ph;}
	var phoneno = /^[0]{1}[1-9]{1}[0-9]{9}$/;
	if(ph.match(phoneno)) {
		//alert($('#ex2').val());
	} else {
		$('#errormsg').html("Invalid Phone Number!").show();
		return false;
	}
	var posting = $.post("../../pages/jsp/GetLocationData.jsp",
				{
					locId:$('#locationsFilter').val(),
					nodeId:$('#nodeFilter').val(),
					phone:$('#PhoneNum').val(),
					threshold:$('#ex2').val(),
					query:"UpdateThresholdPhoneNum"
				},
				function(data,status){
					//alert(data);
					xmlDoc = loadXMLString(data);
					if($(xmlDoc).find("rows").length == 0) {
						return;
					}
					
					$(xmlDoc).find("row").each(function(){
						var id = $(this).find("Done").text();
					});
					//$('#confirmmsg').html("").show();
					$('.confirmmsg').fadeIn(500);
			           setTimeout( "$('.confirmmsg').fadeOut(1500);",3000 );
			           document.getElementById("saveBtn").disabled = true;
			           document.getElementById("resetBtn").disabled = false;
			           document.getElementById("locationsFilter").disabled = true;
			           document.getElementById("zonesFilter").disabled = true;
			           document.getElementById("nodeFilter").disabled = true;
			           $('#saveState').val("disable");
				});
}


function resetThreshold(){
	var posting = $.post("../../pages/jsp/GetLocationData.jsp",
			{
				nodeId:$('#nodeFilter').val(),
				query:"resetThreshold"
			},
			function(data,status){
				//alert(data);
				xmlDoc = loadXMLString(data);
				if($(xmlDoc).find("rows").length == 0) {
					return;
				}
				
				$(xmlDoc).find("row").each(function(){
					var id = $(this).find("Done").text();
				});
				//$('#confirmmsg').html("").show();
				$('.resetmsg').fadeIn(500);
		           setTimeout( "$('.resetmsg').fadeOut(1500);",3000 );
		           document.getElementById("saveBtn").disabled = false;
		           document.getElementById("resetBtn").disabled = true;
		           document.getElementById("locationsFilter").disabled = false;
		           document.getElementById("zonesFilter").disabled = false;
		           document.getElementById("nodeFilter").disabled = false;
		           $('#saveState').val("enable");
			});

}




function updateCharts() {
	var b = false;
	var alertnode = "";
	var labeltemp1 = "";
	var datatemp1 = "";
	var labeltemp2 = "";
	var datatemp2 = "";
	var labeltemp3 = "";
	var datatemp3 = "";
	if($('#locationsFilter').val() == 'LM-DMO-000001') alertnode = "ND-SY-DMO-000005";
	else alertnode = "ND-SY-DMO-000058";
	var posting = $.post("../../pages/jsp/GetLocationData.jsp",
				{
					alertNodeValue:alertnode,
					node:$('#nodeFilter').val(),
					sTime:$('#alertsFilter').val(),
					query:"updateCharts"
				},
				function(data,status){
					xmlDoc = loadXMLString(data);
					if($(xmlDoc).find("rows").length == 0) {
						//$('#nodeFilter').append('<option value=0>None</option>');
						return;
					}
					
					$(xmlDoc).find("row").each(function(){
						b = true;
						labeltemp1 = $(this).find("TempLabel").text();
						datatemp1 = $(this).find("TempData").text();
						//alert("1: " + labeltemp1);
						//alert("2: " + datatemp1);
						labeltemp2 = $(this).find("DoorLabel").text();
						datatemp2 = $(this).find("DoorData").text();
						//alert("3: " + labeltemp2);
						//alert("4: " + datatemp2);
						labeltemp3 = $(this).find("PowerLabel").text();
						datatemp3 = $(this).find("PowerData").text();
						//alert("5: " + labeltemp3);
						//alert("6: " + datatemp3);
					});
					
				});  
	posting.done(function() {
		
		var areaChartData1 = {
				labels : labeltemp1,
				datasets : [ {
					label : "Temperature",
					fillColor : "rgba(210, 214, 222, 1)",
					strokeColor : "rgba(210, 214, 222, 1)",
					pointColor : "rgba(210, 214, 222, 1)",
					pointStrokeColor : "#c1c7d1",
					pointHighlightFill : "#fff",
					pointHighlightStroke : "rgba(220,220,220,1)",
					data : datatemp1
				} ]
			};
		var areaChartData2 = {
				labels : labeltemp2,
				datasets : [ {
					label : "Door Activity",
					fillColor : "rgba(210, 214, 222, 1)",
					strokeColor : "rgba(210, 214, 222, 1)",
					pointColor : "rgba(210, 214, 222, 1)",
					pointStrokeColor : "#c1c7d1",
					pointHighlightFill : "#fff",
					pointHighlightStroke : "rgba(220,220,220,1)",
					data : datatemp2
				} ]
			};
		var areaChartData3 = {
				labels : labeltemp3,
				datasets : [ {
					label : "Power Consumption",
					fillColor : "rgba(210, 214, 222, 1)",
					strokeColor : "rgba(210, 214, 222, 1)",
					pointColor : "rgba(210, 214, 222, 1)",
					pointStrokeColor : "#c1c7d1",
					pointHighlightFill : "#fff",
					pointHighlightStroke : "rgba(220,220,220,1)",
					data : datatemp3
				} ]
			};
		var areaChartOptions = {
				scales: {
				    xAxes: [{
				      ticks: {
				    	  maxRotation: 90,
				          minRotation: 90
				      }
				    }]
				  },
			//Boolean - If we should show the scale at all
			showScale : true,
			//Boolean - Whether grid lines are shown across the chart
			scaleShowGridLines : false,
			//String - Colour of the grid lines
			scaleGridLineColor : "rgba(0,0,0,.05)",
			//Number - Width of the grid lines
			scaleGridLineWidth : 0,
			//Boolean - Whether to show horizontal lines (except X axis)
			scaleShowHorizontalLines : false,
			//Boolean - Whether to show vertical lines (except Y axis)
			scaleShowVerticalLines : false,
			//Boolean - Whether the line is curved between points
			bezierCurve : true,
			//Number - Tension of the bezier curve between points
			bezierCurveTension : 0.3,
			//Boolean - Whether to show a dot for each point
			pointDot : true,
			//Number - Radius of each point dot in pixels
			pointDotRadius : 1,
			//Number - Pixel width of point dot stroke
			pointDotStrokeWidth : 1,
			//Number - amount extra to add to the radius to cater for hit detection outside the drawn point
			pointHitDetectionRadius : 0,
			//Boolean - Whether to show a stroke for datasets
			datasetStroke : true,
			//Number - Pixel width of dataset stroke
			datasetStrokeWidth : 2,
			//Boolean - Whether to fill the dataset with a color
			datasetFill : true,
			//String - A legend template
			//Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
			maintainAspectRatio : true,
			//Boolean - whether to make the chart responsive to window resizing
			responsive : true
			
		};

		//-------------
		//- LINE CHART -
		//--------------
		areaChartOptions.datasetFill = true;
	// Reduce the animation steps for demo clarity.
		var lineChartCanvas1 = $("#lineChart1").get(0).getContext("2d");
		var lineChart1 = new Chart(lineChartCanvas1);
		lineChart1.Line(areaChartData1, areaChartOptions);
		var lineChartCanvas2 = $("#lineChart2").get(0).getContext("2d");
		var lineChart2 = new Chart(lineChartCanvas2);
		lineChart2.Line(areaChartData2, areaChartOptions);
		var lineChartCanvas3 = $("#lineChart3").get(0).getContext("2d");
		var lineChart3 = new Chart(lineChartCanvas3);
		lineChart3.Line(areaChartData3, areaChartOptions);
		//var myLiveChart = new Chart(ctx).Line(startingData, {animationSteps: 15});
		//lineChart1.update();
		
		

});

}

function loadXMLString(txt) 
{
	if (window.DOMParser)
	{
		parser=new DOMParser();
		xmlDoc=parser.parseFromString(txt,"text/xml");
	}
	else // Internet Explorer
	{
		xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
		xmlDoc.async=false;
		xmlDoc.loadXML(txt); 
	}
	return xmlDoc;
}