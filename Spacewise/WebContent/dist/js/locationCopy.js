function reloadSubLoc(){
	$('#subLocationsFilter').empty();
	var b = false;
	var posting = $.post("../../pages/jsp/GetLocationData.jsp",
				{
					locId:$('#locationsFilter').val(),
					query:"subLoc"
				},
				function(data,status){
					//alert(data);
					xmlDoc = loadXMLString(data);
					if($(xmlDoc).find("rows").length == 0) {
						$('#subLocationsFilter').append('<option value=0>None</option>');
						return;
					}
					
					$(xmlDoc).find("row").each(function(){
						b = true;
						var id = $(this).find("Id").text();
						var name = $(this).find("Name").text();
						if (name == "None") id = "0";
						 $('#subLocationsFilter').append('<option value=' + id + '>' + name + '</option>');
					});
					if(!b){
						$('#subLocationsFilter').append('<option value=0>None</option>');
					}
				});
	posting.done(function() {
		reloadZone();
	});
}

function reloadZone() {
	$('#zonesFilter').empty();
	var b = false;
	var posting = $.post("../../pages/jsp/GetLocationData.jsp",
				{
					locId:$('#locationsFilter').val(),
					query:"zone",
					subLocId:$('#subLocationsFilter').val()
				},
				function(data,status){
					//alert(data);
					xmlDoc = loadXMLString(data);
					if($(xmlDoc).find("rows").length == 0) {
						$('#zonesFilter').append('<option value=0>None</option>');
						return;
					}
					
					$(xmlDoc).find("row").each(function(){
						b = true;
						var id = $(this).find("Id").text();
						var name = $(this).find("Name").text();
						if (name == "None") id = "0";
						 $('#zonesFilter').append('<option value=' + id + '>' + name + '</option>');
					});
					if(!b){
						$('#zonesFilter').append('<option value=0>None</option>');
					}
				});
	posting.done(function() {
		if($('#nodePresent').val() == "yes") {
			reloadNodes();
		}
	});
}

function reloadNodes() {
	$('#nodeFilter').empty();
	var b = false;
	var posting = $.post("../../pages/jsp/GetLocationData.jsp",
				{
					locId:$('#zonesFilter').val(),
					query:"node"
				},
				function(data,status){
					//alert(data);
					xmlDoc = loadXMLString(data);
					if($(xmlDoc).find("rows").length == 0) {
						$('#nodeFilter').append('<option value=0>None</option>');
						return;
					}
					
					$(xmlDoc).find("row").each(function(){
						b = true;
						var id = $(this).find("Id").text();
						var name = $(this).find("Name").text();
						if (name == "None") id = "0";
						 $('#nodeFilter').append('<option value=' + id + '>' + name + '</option>');
					});
					if(!b){
						$('#nodeFilter').append('<option value=0>None</option>');
					}
				});  
	posting.done(function() {
		updateCharts();
	});
}

function updateCharts() {
	var b = false;
	var alertnode = "ND-SY-DMO-000067";
	var labeltemp1 = "";
	var datatemp1 = "";
	var labeltemp2 = "";
	var datatemp2 = "";
	var labeltemp3 = "";
	var datatemp3 = "";
	var posting = $.post("../../pages/jsp/GetLocationData.jsp",
				{
					alertNodeValue:alertnode,
					node:$('#nodeFilter').val(),
					sTime:$('#alertsFilter').val(),
					query:"updateCharts"
				},
				function(data,status){
					xmlDoc = loadXMLString(data);
					if($(xmlDoc).find("rows").length == 0) {
						//$('#nodeFilter').append('<option value=0>None</option>');
						return;
					}
					
					$(xmlDoc).find("row").each(function(){
						b = true;
						labeltemp1 = $(this).find("TempLabel").text();
						datatemp1 = $(this).find("TempData").text();
						alert("1: " + labeltemp1);
						alert("2: " + datatemp1);
						labeltemp2 = $(this).find("DoorLabel").text();
						datatemp2 = $(this).find("DoorData").text();
						//alert("3: " + labeltemp2);
						//alert("4: " + datatemp2);
						labeltemp3 = $(this).find("PowerLabel").text();
						datatemp3 = $(this).find("PowerData").text();
						//alert("5: " + labeltemp3);
						//alert("6: " + datatemp3);
					});
					
				});  
	posting.done(function() {
		var canvas1 = document.getElementById('lineChart1');
	    var ctx1 = canvas1.getContext('2d');
	   // ctx1.destroy();
	    var startingData1 = {
	      labels: labeltemp1,
	      datasets: [
	          {
	        	  label : "Temperature",
					fillColor : "rgba(210, 214, 222, 1)",
					strokeColor : "rgba(210, 214, 222, 1)",
					pointColor : "rgba(210, 214, 222, 1)",
					pointStrokeColor : "#c1c7d1",
					pointHighlightFill : "#fff",
					pointHighlightStroke : "rgba(220,220,220,1)",
	              data: datatemp1
	          }
	      ]
	    };
		
		var myLiveChart1 = new Chart.Line(ctx1, {
			data: startingData1,
		    options: {animationSteps: 1}});
		myLiveChart1.update();
		
		var canvas2 = document.getElementById('lineChart2');
	    var ctx2 = canvas2.getContext('2d');
	    ctx2.destroy();
	    var startingData2 = {
	      labels: labeltemp2,
	      datasets: [
	          {
	        	  label : "Door Activity",
					fillColor : "rgba(210, 214, 222, 1)",
					strokeColor : "rgba(210, 214, 222, 1)",
					pointColor : "rgba(210, 214, 222, 1)",
					pointStrokeColor : "#c1c7d1",
					pointHighlightFill : "#fff",
					pointHighlightStroke : "rgba(220,220,220,1)",
	              data: datatemp2
	          }
	      ]
	    };
		
		var myLiveChart2 = new Chart.Line(ctx2, {
			data: startingData2,
		    options: {animationSteps: 15}});
		myLiveChart2.update();
		

	
	var canvas3 = document.getElementById('lineChart3');
    var ctx3 = canvas3.getContext('2d');
    ctx3.destroy();
    var startingData3 = {
      labels: labeltemp3,
      datasets: [
          {
        	  label : "Power Consumption",
				fillColor : "rgba(210, 214, 222, 1)",
				strokeColor : "rgba(210, 214, 222, 1)",
				pointColor : "rgba(210, 214, 222, 1)",
				pointStrokeColor : "#c1c7d1",
				pointHighlightFill : "#fff",
				pointHighlightStroke : "rgba(220,220,220,1)",
              data: datatemp3
          }
      ]
    };
	
	var myLiveChart3 = new Chart.Line(ctx3, {
		data: startingData3,
	    options: {animationSteps: 15}});
	myLiveChart3.update();
});
//	var canvas = document.getElementById('lineChart1');
//    var ctx = canvas.getContext('2d');
//    var startingData = {
//      labels: [1, 2, 3, 4, 5, 6, 7],
//      datasets: [
//          {
//        	  label : "Door Activity",
//				fillColor : "rgba(210, 214, 222, 1)",
//				strokeColor : "rgba(210, 214, 222, 1)",
//				pointColor : "rgba(210, 214, 222, 1)",
//				pointStrokeColor : "#c1c7d1",
//				pointHighlightFill : "#fff",
//				pointHighlightStroke : "rgba(220,220,220,1)",
//              data: [65, 59, 80, 81, 56, 55, 40]
//          }
//      ]
//    };
//	
//	var myLiveChart = new Chart.Line(ctx, {
//		data: startingData,
//	    options: {animationSteps: 15}});
	
}

function getSourceLocs(){
	$('#vehicleSourceCity').empty();
	var b = false;
	var posting = $.post("../../pages/jsp/GetLocationData.jsp",
				{
					query:"GetAllSourceLocs"
				},
				function(data,status){
					//alert(data);
					xmlDoc = loadXMLString(data);
					if($(xmlDoc).find("rows").length == 0) {
						$('#vehicleSourceCity').append('<option value=0>None</option>');
						return;
					}
					
					$(xmlDoc).find("row").each(function(){
						b = true;
						var id = $(this).find("City").text();
						 $('#vehicleSourceCity').append('<option value=' + id + '>' + id + '</option>');
					});
					if(!b){
						$('#vehicleSourceCity').append('<option value=0>None</option>');
					}
				});
	posting.done(function() {
		getSourceIds();
	});
}

function getSourceIds(){
	$('#vehicleSourceId').empty();
	var b = false;
	var posting = $.post("../../pages/jsp/GetLocationData.jsp",
				{
					Source:$('#vehicleSourceCity').val(),
					query:"GetAllSourceIds"
				},
				function(data,status){
					//alert(data);
					xmlDoc = loadXMLString(data);
					if($(xmlDoc).find("rows").length == 0) {
						$('#vehicleSourceId').append('<option value=0>None</option>');
						return;
					}
					
					$(xmlDoc).find("row").each(function(){
						b = true;
						var id = $(this).find("LocationId").text();
						var name = $(this).find("LocationName").text();
						 $('#vehicleSourceId').append('<option value=' + id + '>' + name + '</option>');
					});
					if(!b){
						$('#vehicleSourceId').append('<option value=0>None</option>');
					}
				});
	posting.done(function() {
		getDestLocs();
	});
}

function getDestLocs(){
	$('#vehicleDestCity').empty();
	var b = false;
	var posting = $.post("../../pages/jsp/GetLocationData.jsp",
				{
					LocationId:$('#vehicleSourceId').val(),
					query:"GetAllDestCity"
				},
				function(data,status){
					//alert(data);
					xmlDoc = loadXMLString(data);
					if($(xmlDoc).find("rows").length == 0) {
						$('#vehicleDestCity').append('<option value=0>None</option>');
						return;
					}
					
					$(xmlDoc).find("row").each(function(){
						b = true;
						var id = $(this).find("DestCity").text();
						 $('#vehicleDestCity').append('<option value=' + id + '>' + id + '</option>');
					});
					if(!b){
						$('#vehicleDestCity').append('<option value=0>None</option>');
					}
				});
	posting.done(function() {
		getDestIds();
	});
}

function getDestIds(){
	$('#vehicleDestId').empty();
	var b = false;
	var posting = $.post("../../pages/jsp/GetLocationData.jsp",
				{
					DestCity:$('#vehicleDestCity').val(),
					query:"GetAllDestId"
				},
				function(data,status){
					//alert(data);
					xmlDoc = loadXMLString(data);
					if($(xmlDoc).find("rows").length == 0) {
						$('#vehicleDestId').append('<option value=0>None</option>');
						return;
					}
					
					$(xmlDoc).find("row").each(function(){
						b = true;
						var id = $(this).find("LocationId").text();
						var name = $(this).find("LocationName").text();
						 $('#vehicleDestId').append('<option value=' + id + '>' + name + '</option>');
					});
					if(!b){
						$('#vehicleDestId').append('<option value=0>None</option>');
					}
				});
	posting.done(function() {
		getVehicleId();
	});
}

function getVehicleId(){
	$('#e1').empty();
	var b = false;
	var posting = $.post("../../pages/jsp/GetLocationData.jsp",
				{
					SourceId:$('#vehicleSourceId').val(),
					DestId:$('#vehicleDestId').val(),
					query:"GetAllVehicleIds"
				},
				function(data,status){
					//alert(data);
					xmlDoc = loadXMLString(data);
					if($(xmlDoc).find("rows").length == 0) {
						$('#e1').append('<option value=0>None</option>');
						return;
					}
					
					$(xmlDoc).find("row").each(function(){
						b = true;
						var id = $(this).find("VehicleId").text();
						 $('#e1').append('<option value=' + id + '>' + id + '</option>');
					});
					if(!b){
						$('#e1').append('<option value=0>None</option>');
					}
				});
}

function submitVehicleId() {
	//alert($("#e1").val());
	if($("#e1").val() == "Select Vehicle" || $("#e1").val() == "0"){
		$("#error-message").html("Please select a Vehicle!");
		$("#error-message").show();
		return;
	}
	window.location = "LocalRunsDetail.jsp?VehicleId=" + $("#e1").val();
			
}


function updateAction(sAlertId, sAction) {
	var posting = $.post("../../pages/jsp/GetLocationData.jsp",
				{
					alertId:sAlertId,
					query:"Action",
					action:sAction
				},
				function(data,status){
					//alert(data);
					xmlDoc = loadXMLString(data);
					if($(xmlDoc).find("rows").length == 0) {
						return;
					}
					
					$(xmlDoc).find("row").each(function(){
						var id = $(this).find("Done").text();
					});
				});
	$('#btn'+sAlertId).val(sAction);
}


function updateRoot(sAlertId) {
	var posting = $.post("../../pages/jsp/GetLocationData.jsp",
			{
				alertId:sAlertId,
				root:$('#txtRoot'+sAlertId).val(),
				preventive:$('#txtPre'+sAlertId).val(),
				query:"RootCause"
			},
			function(data,status){
				//alert(data);
				xmlDoc = loadXMLString(data);
				if($(xmlDoc).find("rows").length == 0) {
					return;
				}
				
				$(xmlDoc).find("row").each(function(){
					var id = $(this).find("Done").text();
				});
			});
}

function updateThresholdAndPhoneNumber() {
	$('#errormsg').hide();
	$('#confirmmsg').hide();
	var ph = $('#PhoneNum').val();
	if(ph.length == 10) { ph = "0" + ph;}
	var phoneno = /^[0]{1}[1-9]{1}[0-9]{9}$/;
	if(ph.match(phoneno)) {
		//alert($('#ex2').val());
	} else {
		$('#errormsg').html("Invalid Phone Number!").show();
		return false;
	}
	var posting = $.post("../../pages/jsp/GetLocationData.jsp",
				{
					locId:$('#locationsFilter').val(),
					nodeId:$('#nodeFilter').val(),
					phone:$('#PhoneNum').val(),
					threshold:$('#ex2').val(),
					query:"UpdateThresholdPhoneNum"
				},
				function(data,status){
					//alert(data);
					xmlDoc = loadXMLString(data);
					if($(xmlDoc).find("rows").length == 0) {
						return;
					}
					
					$(xmlDoc).find("row").each(function(){
						var id = $(this).find("Done").text();
					});
					//$('#confirmmsg').html("").show();
					$('.confirmmsg').fadeIn(500);
			           setTimeout( "$('.confirmmsg').fadeOut(1500);",3000 );
				});
}

function loadXMLString(txt) 
{
	if (window.DOMParser)
	{
		parser=new DOMParser();
		xmlDoc=parser.parseFromString(txt,"text/xml");
	}
	else // Internet Explorer
	{
		xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
		xmlDoc.async=false;
		xmlDoc.loadXML(txt); 
	}
	return xmlDoc;
}