package com.spacewise.action;


import java.util.Collection;
import java.util.Vector;

import com.spacewise.bean.MapsBean;
import com.spacewise.bean.PlanMapsBean;
import com.spacewise.bean.StationaryTemperatureBean;
import com.spacewise.dao.ProcessMapDataDao;
import com.spacewise.dao.StationaryTemperatureDao;


public class MapAction {

	public Collection<StationaryTemperatureBean> getAllStationaryTemperatureRows() {
		StationaryTemperatureDao ps = new StationaryTemperatureDao();
		Collection<StationaryTemperatureBean> col = new Vector<StationaryTemperatureBean>();
		try {
			col = ps.selectAllStationaryTemperatureRecords();
		} catch(Exception e) {
			System.out.println(this.getClass().getName() + "Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		}
		return col;
	}
	
	public Collection<MapsBean> getAllRows() {
		ProcessMapDataDao ps = new ProcessMapDataDao();
		Collection<MapsBean> col = new Vector<MapsBean>();
		try {
			col = ps.selectAllRecords();
		} catch(Exception e) {
			System.out.println(this.getClass().getName() + "Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		}
		return col;
	}
	
	public Collection<PlanMapsBean> getPlanAllRows() {
		ProcessMapDataDao ps = new ProcessMapDataDao();
		Collection<PlanMapsBean> col = new Vector<PlanMapsBean>();
		try {
			col = ps.selectPlanAllRecords();
		} catch(Exception e) {
			System.out.println(this.getClass().getName() + "Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		}
		return col;
	}
}
