package com.spacewise.util;
// Use the JDBC driver  
    import java.sql.*;  

import com.microsoft.sqlserver.jdbc.SQLServerDriver;  
      
        public class DbConnection {  
        	public static Connection connection = null;
        	
    
            // Connect to your database.  
            // Replace server name, username, and password with your credentials  
            public static Connection getConnection() {
            	Statement statement = null;   
            	  ResultSet resultSet = null;
            	try {
            		
            		/* For Anasuya:
            		 * 
                   	Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
                    String connectionString =  
                        "jdbc:sqlserver://tbox-ansrv1.database.windows.net:1433;" 
                        + "database=tbox_db_dev;"  
                        + "user=tbox_db1_admin@tbox-ansrv1;"  
                        + "password=gatxob_321;"  
                        + "encrypt=false;"  
                        + "trustServerCertificate=false;"  
                        + "hostNameInCertificate=*.database.windows.net;"  
                        + "loginTimeout=30;";  
                    // Declare the JDBC objects.  
                    
                    /*
                     * For Creamline:
            	Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
                String connectionString =  
                    "jdbc:sqlserver://tbox-clsrv1.database.windows.net:1433;" 
                    + "database=tbox_db_dev;"  
                    + "user=tbox_db1_admin@tbox-clsrv1;"  
                    + "password=gatxob_321;"  
                    + "encrypt=false;"  
                    + "trustServerCertificate=false;"  
                    + "hostNameInCertificate=*.database.windows.net;"  
                    + "loginTimeout=30;";  
                // Declare the JDBC objects.  
                
                   */
                //        connection = DriverManager.getConnection(connectionString);
            		Class.forName("org.postgresql.Driver");
            		connection = DriverManager.getConnection(
            		         //"jdbc:postgresql://dbhost:port/dbname", "user", "dbpass");
            		         "jdbc:postgresql://104.215.248.40:5432/somesh", "somesh", "Tagbox_123456");
          
                        // Create and execute a SELECT SQL statement.  
                       /* String selectSql = "SELECT TOP 10 * from uni_master_events";  
                        statement = connection.createStatement();  
                        resultSet = statement.executeQuery(selectSql);  
          
                        // Print results from select statement  
                        while (resultSet.next())   
                        {  
                            System.out.println(this.getClass().getName() + resultSet.getString(2) + " "  
                                + resultSet.getString(3));  
                        }  */
                    }  
                    catch (Exception e) {  
                        e.printStackTrace();  
                    }  
                    finally {  
                        // Close the connections after the data has been handled.  
                         
                    }  
                //}  

             return connection;
          }
  public static void closeConnection()
  {
      try
          {
              if(connection != null) {
            	  connection.close();
            	  connection = null;
              }

          } catch (Exception e)
          {
              System.out.println("com.tagbox.util.DbConnection: Exception: getConnection():" + e.getLocalizedMessage());
              e.printStackTrace();
          } finally {
          	if(connection != null) {
          		connection = null;
              }

          }

  }
        } 