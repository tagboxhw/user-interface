package com.spacewise.bean;

import java.io.Serializable;

public class HumidityTriggersBean implements Serializable{

	/**
	 * Generated serial version ID
	 */
	private static final long serialVersionUID = -1874474658549948076L;
	
	private String GW_client_id;
	private String ND_client_id;
	private int Threshold_UL;
	private int Threshold_LL;
	private int Threshold_Window;
	
	public HumidityTriggersBean(){
		super();
	}
	
	public void init() {
		this.GW_client_id = "";
		this.ND_client_id = "";
		this.Threshold_UL = 0;
		this.Threshold_LL = 0;
		this.Threshold_Window = 0;
	}
	
	public String toString(){
		String buf = null;
		buf = "Humidity Triggers object: \n";
		buf = buf + "GW_client_id: " + GW_client_id + "\n";
		buf = buf + "ND_client_id: " + ND_client_id + "\n";
		buf = buf + "Threshold_UL: " + Threshold_UL + "\n";
		buf = buf + "Threshold_LL: " + Threshold_LL + "\n";
		buf = buf + "Threshold_Window: " + Threshold_Window + "\n";
		return buf;
	}

	/**
	 * @return the gW_client_id
	 */
	public String getGW_client_id() {
		return GW_client_id;
	}

	/**
	 * @param gW_client_id the gW_client_id to set
	 */
	public void setGW_client_id(String gW_client_id) {
		GW_client_id = gW_client_id;
	}

	/**
	 * @return the nD_client_id
	 */
	public String getND_client_id() {
		return ND_client_id;
	}

	/**
	 * @param nD_client_id the nD_client_id to set
	 */
	public void setND_client_id(String nD_client_id) {
		ND_client_id = nD_client_id;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the threshold_UL
	 */
	public int getThreshold_UL() {
		return Threshold_UL;
	}

	/**
	 * @param threshold_UL the threshold_UL to set
	 */
	public void setThreshold_UL(int threshold_UL) {
		Threshold_UL = threshold_UL;
	}

	/**
	 * @return the threshold_LL
	 */
	public int getThreshold_LL() {
		return Threshold_LL;
	}

	/**
	 * @param threshold_LL the threshold_LL to set
	 */
	public void setThreshold_LL(int threshold_LL) {
		Threshold_LL = threshold_LL;
	}

	/**
	 * @return the threshold_Window
	 */
	public int getThreshold_Window() {
		return Threshold_Window;
	}

	/**
	 * @param threshold_Window the threshold_Window to set
	 */
	public void setThreshold_Window(int threshold_Window) {
		Threshold_Window = threshold_Window;
	}
	

}
