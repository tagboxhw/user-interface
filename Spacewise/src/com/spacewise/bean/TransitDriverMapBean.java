package com.spacewise.bean;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement (name="transitDriverMapBean")
public class TransitDriverMapBean implements Serializable{

	/**
	 * Generated serial version ID
	 */
	private static final long serialVersionUID = -1874474658549948076L;
	
	private String Driver_Name;
	private String Driver_Phone_Num;
	private String Driver_ID;
	private String Driver_DL_ID;
	
	public TransitDriverMapBean(){
		super();
	}
	
	public void init() {
		this.Driver_Name = "";
		this.Driver_Phone_Num = "";
		this.Driver_ID = "";
		this.Driver_DL_ID = "";
	}
	
	public String toString(){
		String buf = null;
		buf = "Transit Driver Map object: \n";
		buf = buf + "Driver_Name: " + Driver_Name + "\n";
		buf = buf + "Driver_Phone_Num: " + Driver_Phone_Num + "\n";
		buf = buf + "Driver_ID: " + Driver_ID + "\n";
		buf = buf + "Driver_DL_ID: " + Driver_DL_ID + "\n";
		return buf;
	}

	/**
	 * @return the driver_Name
	 */
	public String getDriver_Name() {
		return Driver_Name;
	}

	/**
	 * @param driver_Name the driver_Name to set
	 */
	@XmlElement
	public void setDriver_Name(String driver_Name) {
		this.Driver_Name = driver_Name;
	}

	/**
	 * @return the driver_Phone_Num
	 */
	public String getDriver_Phone_Num() {
		return Driver_Phone_Num;
	}

	/**
	 * @param driver_Phone_Num the driver_Phone_Num to set
	 */
	@XmlElement
	public void setDriver_Phone_Num(String driver_Phone_Num) {
		this.Driver_Phone_Num = driver_Phone_Num;
	}

	/**
	 * @return the driver_ID
	 */
	public String getDriver_ID() {
		return Driver_ID;
	}

	/**
	 * @param driver_ID the driver_ID to set
	 */
	@XmlElement
	public void setDriver_ID(String driver_ID) {
		this.Driver_ID = driver_ID;
	}

	/**
	 * @return the driver_DL_ID
	 */
	public String getDriver_DL_ID() {
		return Driver_DL_ID;
	}

	/**
	 * @param driver_DL_ID the driver_DL_ID to set
	 */
	@XmlElement
	public void setDriver_DL_ID(String driver_DL_ID) {
		this.Driver_DL_ID = driver_DL_ID;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	

}
