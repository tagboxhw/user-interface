package com.spacewise.bean;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "todaysAlertWorkflowBean")
public class TodaysAlertWorkflowBean implements Serializable{

	/**
	 * Generated serial version ID
	 */
	private static final long serialVersionUID = -1874474658549948076L;
	
	
	private String ClassType;
	private String Vehicle_ID;
	private String Route_Type;
	private String Zone_ID;
	private String Location_ID;
	private String Alert_Type;
	private String Location_Name;
	private String City_Name;
	private String Location_Type;
	private int Count_Of_Alerts;
	private String Action_Status;
	private int Current_Alert;
		
	public TodaysAlertWorkflowBean(){
		super();
	}
	
	public void init() {
		this.ClassType = "";
		this.Vehicle_ID = "";
		this.Route_Type = "";
		this.Zone_ID = "";
		this.Location_ID = "";
		this.Alert_Type = "";
		this.Location_Name = "";
		this.City_Name = "";
		this.Location_Type = "";
		this.Count_Of_Alerts = 0;
		this.Action_Status = "";
		this.Current_Alert = 0;
	}
	
	public String toString(){
		String buf = null;
		buf = "Today's Alert Workflow object: \n";
		buf = buf + "Class Type: " + ClassType + "\n";
		buf = buf + "Vehicle ID: " + Vehicle_ID + "\n";
		buf = buf + "Route_Type: " + Route_Type + "\n";
		buf = buf + "Zone_ID: " + Zone_ID + "\n";
		buf = buf + "Location_ID: " + Location_ID + "\n";
		buf = buf + "Alert_Type: " + Alert_Type + "\n";
		buf = buf + "Location_Name: " + Location_Name + "\n";
		buf = buf + "City_Name: " + City_Name + "\n";
		buf = buf + "Location_Type: " + Location_Type + "\n";
		buf = buf + "Count_Of_Alerts: " + Count_Of_Alerts + "\n";
		buf = buf + "Action_Status: " + Action_Status + "\n";
		buf = buf + "Current_Alert: " + Current_Alert + "\n";
		
		return buf;
	}

	/**
	 * @return the ClassType
	 */
	public String getClassType() {
		return this.ClassType;
	}

	/**
	 * @param ClassType the ClassType to set
	 */
	@XmlElement
	public void setClassType(String ClassType) {
		this.ClassType = ClassType;
	}
	
	/**
	 * @return the Vehicle_ID
	 */
	public String getVehicle_ID() {
		return this.Vehicle_ID;
	}

	/**
	 * @param Vehicle_ID the Vehicle_ID to set
	 */
	@XmlElement
	public void setVehicle_ID(String Vehicle_ID) {
		this.Vehicle_ID = Vehicle_ID;
	}

	/**
	 * @return the route_Type
	 */
	public String getRoute_Type() {
		return this.Route_Type;
	}

	/**
	 * @param route_Type the route_Type to set
	 */
	@XmlElement
	public void setRoute_Type(String route_Type) {
		this.Route_Type = route_Type;
	}


	/**
	 * @return the Zone_ID
	 */
	public String getZone_ID() {
		return this.Zone_ID;
	}

	/**
	 * @param Zone_ID the Zone_ID to set
	 */
	@XmlElement
	public void setZone_ID(String Zone_ID) {
		this.Zone_ID = Zone_ID;
	}

	/**
	 * @return the Location_ID
	 */
	public String getLocation_ID() {
		return this.Location_ID;
	}

	/**
	 * @param Location_ID the Location_ID to set
	 */
	@XmlElement
	public void setLocation_ID(String Location_ID) {
		this.Location_ID = Location_ID;
	}

	/**
	 * @return the alert_Type
	 */
	public String getAlert_Type() {
		return this.Alert_Type;
	}

	/**
	 * @param alert_Type the alert_Type to set
	 */
	@XmlElement
	public void setAlert_Type(String alert_Type) {
		this.Alert_Type = alert_Type;
	}

	/**
	 * @return the Location_Name
	 */
	public String getLocation_Name() {
		return this.Location_Name;
	}

	/**
	 * @param Location_Name the Location_Name to set
	 */
	@XmlElement
	public void setLocation_Name(String Location_Name) {
		this.Location_Name = Location_Name;
	}

	/**
	 * @return the City_Name
	 */
	public String getCity_Name() {
		return this.City_Name;
	}

	/**
	 * @param City_Name the City_Name to set
	 */
	@XmlElement
	public void setCity_Name(String City_Name) {
		this.City_Name = City_Name;
	}

	/**
	 * @return the Location_Type
	 */
	public String getLocation_Type() {
		return this.Location_Type;
	}

	/**
	 * @param Location_Type the Location_Type to set
	 */
	@XmlElement
	public void setLocation_Type(String Location_Type) {
		this.Location_Type = Location_Type;
	}

	/**
	 * @return the Count_Of_Alerts
	 */
	public int getCount_Of_Alerts() {
		return this.Count_Of_Alerts;
	}

	/**
	 * @param Count_Of_Alerts the Count_Of_Alerts to set
	 */
	@XmlElement
	public void setCount_Of_Alerts(int Count_Of_Alerts) {
		this.Count_Of_Alerts = Count_Of_Alerts;
	}

	public String getAction_Status() {
		return Action_Status;
	}

	@XmlElement
	public void setAction_Status(String action_Status) {
		Action_Status = action_Status;
	}

	public int getCurrent_Alert() {
		return Current_Alert;
	}

	@XmlElement
	public void setCurrent_Alert(int current_Alert) {
		Current_Alert = current_Alert;
	}
}
