package com.spacewise.bean;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "alertSummaryBean")
public class AlertSummaryBean implements Serializable{

	/**
	 * Generated serial version ID
	 */
	private static final long serialVersionUID = -1874474658549948076L;
	private String Resolution;
	private String One;
	private String Two;
	private String Three;
	private String Four;
	private String Five;
	
	public AlertSummaryBean(){
		super();
	}
	
	public void init() {
		this.Resolution = "";
		this.One = "";
		this.Two = "";
		this.Three = "";
		this.Four = "";
		this.Five = "";
	}
	
	public String toString(){
		String buf = null;
		buf = "Alert Summary object: \n";
		buf = buf + "Resolution: " + Resolution + "\n";
		buf = buf + "One: " + One + "\n";
		buf = buf + "Two: " + Two + "\n";
		buf = buf + "Three: " + Three + "\n";
		buf = buf + "Four: " + Four + "\n";
		buf = buf + "Five: " + Five + "\n";
		
		return buf;
	}

	public String getResolution() {
		return Resolution;
	}

	@XmlElement
	public void setResolution(String Resolution) {
		this.Resolution = Resolution;
	}

	public String getOne() {
		return One;
	}
	
	@XmlElement
	public void setOne(String One) {
		this.One = One;
	}

	public String getTwo() {
		return Two;
	}
	
	@XmlElement
	public void setTwo(String Two) {
		this.Two = Two;
	}

	public String getThree() {
		return Three;
	}

	@XmlElement
	public void setThree(String Three) {
		this.Three = Three;
	}

	public String getFour() {
		return Four;
	}

	@XmlElement
	public void setFour(String Four) {
		this.Four = Four;
	}

	public String getFive() {
		return Five;
	}

	@XmlElement
	public void setFive(String Five) {
		this.Five = Five;
	}

}
