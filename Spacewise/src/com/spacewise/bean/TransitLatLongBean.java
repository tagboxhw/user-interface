package com.spacewise.bean;

import java.io.Serializable;

public class TransitLatLongBean implements Serializable{

	/**
	 * Generated serial version ID
	 */
	private static final long serialVersionUID = -1874474658549948076L;
	private String GW_client_id;
	private String latitude;
	private String Timestamp;
	private String longitude;
	
	public TransitLatLongBean(){
		super();
	}
	
	public void init() {
		this.GW_client_id = "";
		this.latitude = "";
		this.Timestamp = "";
		this.longitude = "";
	}
	
	public String toString(){
		String buf = null;
		buf = "Transit LatLong Activity object: \n";
		buf = buf + "GW_client_id: " + GW_client_id + "\n";
		buf = buf + "Latitude: " + latitude + "\n";
		buf = buf + "Longitude: " + longitude + "\n";
		buf = buf + "Timestamp: " + Timestamp + "\n";
		return buf;
	}

	/**
	 * @return the gW_client_id
	 */
	public String getGW_client_id() {
		return GW_client_id;
	}

	/**
	 * @param gW_client_id the gW_client_id to set
	 */
	public void setGW_client_id(String gW_client_id) {
		GW_client_id = gW_client_id;
	}

	/**
	 * @return the latitude
	 */
	public String getLatitude() {
		return this.latitude;
	}

	/**
	 * @param latitude the latitude to set
	 */
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	/**
	 * @return the timestamp
	 */
	public String getTimestamp() {
		return Timestamp;
	}

	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(String timestamp) {
		Timestamp = timestamp;
	}

	

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the longitude
	 */
	public String getLongitude() {
		return longitude;
	}

	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	
	

}
