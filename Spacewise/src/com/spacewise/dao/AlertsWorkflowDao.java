package com.spacewise.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.TreeMap;
import java.util.Vector;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.spacewise.bean.AlertWorkflowBean;
import com.spacewise.bean.LocationVehicleAlertsDataBean;
import com.spacewise.bean.TodaysAlertWorkflowBean;
import com.spacewise.bean.TransitVehicleMapBean;
import com.spacewise.util.Constants;
import com.spacewise.util.DbConnection;
import com.spacewise.util.SqlConstants;
import com.spacewise.util.Utils;

public class AlertsWorkflowDao {
	static final Logger logger = LoggerFactory
			.getLogger(AlertsWorkflowDao.class);
	String[] sAlerts = null;
	private Client client; 
	
	private void init(){ 
	      this.client = ClientBuilder.newClient(); 
	   }  
	
	public Collection<TodaysAlertWorkflowBean> getTodaysAlerts() {
		init();
		GenericType<Collection<TodaysAlertWorkflowBean>> tvmBean1 = new GenericType<Collection<TodaysAlertWorkflowBean>>(){};
		Collection<TodaysAlertWorkflowBean> tvmBean = client 
		         .target(Constants.REST_SERVICE_URL + "AlertsWorkflowService/todayalerts") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(tvmBean1); 
		return tvmBean;
	}

	public Collection<TodaysAlertWorkflowBean> getAlertsComp() {
		init();
		GenericType<Collection<TodaysAlertWorkflowBean>> tvmBean1 = new GenericType<Collection<TodaysAlertWorkflowBean>>(){};
		Collection<TodaysAlertWorkflowBean> tvmBean = client 
		         .target(Constants.REST_SERVICE_URL + "AlertsWorkflowService/alertscomp") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(tvmBean1); 
		return tvmBean;
	}
	
	public TreeMap<String, Collection<TransitVehicleMapBean>> getLatLong(
			String sAlertType, String sTime) {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement pSelect = null;
		TreeMap<String, Collection<TransitVehicleMapBean>> tAll = new TreeMap<String, Collection<TransitVehicleMapBean>>();
		try {
			conn = DbConnection.getConnection();
			double fTime = Utils.getAlertTime(sTime);
			logger.info("AlertFilter: " + sAlertType + " Time: " + fTime);
				// There is no route_type in transit_gateway_map. Should that be
				// transit_vehicle_map. If yes, then there is no client_id in
				// transit_vehicle_map
				pSelect = conn.prepareStatement(SqlConstants.GetLatLong_All);
				pSelect.setDouble(1, fTime);
			try {
				rs = pSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}
			Collection<TransitVehicleMapBean> col1 = new Vector<TransitVehicleMapBean>();
			Collection<TransitVehicleMapBean> col2 = new Vector<TransitVehicleMapBean>();
			Collection<TransitVehicleMapBean> col3 = new Vector<TransitVehicleMapBean>();
			TransitVehicleMapBean tv = new TransitVehicleMapBean();
			while (rs.next()) {
				logger.info(rs.getString(1) + " " + rs.getString(2) + " "
						+ rs.getString(3) + " " + rs.getString(4) + " "
						+ rs.getString(5));
				tv = new TransitVehicleMapBean();
				tv.setVehicle_ID(rs.getString(1));
				tv.setRoute_Type(rs.getString(2));
				tv.setSource(rs.getString(3));
				tv.setDestination(rs.getString(4));
				tv.setVehicle_Status(rs.getString(5));
				if (rs.getString(2) == null)
					col1.add(tv);
				else if (rs.getString(2).equals(Constants.LONG_HAUL)) {
					col1.add(tv);
				} else if (rs.getString(2).equals(Constants.SHORT_HAUL)) {
					col2.add(tv);
				} else if (rs.getString(2).equals(Constants.LOCAL_RUN)) {
					col3.add(tv);
				}
			}
			tAll.put(Constants.LONG_HAUL, col1);
			tAll.put(Constants.SHORT_HAUL, col2);
			tAll.put(Constants.LOCAL_RUN, col3);
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return tAll;
	}

	public TreeMap<String, Collection<TransitVehicleMapBean>> getLatLongForAnasuya(
			String sAlertType, String sTime) {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement pSelect = null;
		TreeMap<String, Collection<TransitVehicleMapBean>> tAll = new TreeMap<String, Collection<TransitVehicleMapBean>>();
		try {
			conn = DbConnection.getConnection();
			double fTime = Utils.getAlertTime(sTime);
			logger.info("AlertFilter: " + sAlertType + " Time: " + fTime);
			
				pSelect = conn.prepareStatement("select * from DEV.transit_lat_long where vehicle_id=? order by timestamp asc");
				pSelect.setString(1, "KA03-KH5282");
			try {
				rs = pSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}
			Collection<TransitVehicleMapBean> col1 = new Vector<TransitVehicleMapBean>();
			TransitVehicleMapBean tv = new TransitVehicleMapBean();
			String sSource = "", sDest = "";
			while (rs.next()) {
				logger.info(rs.getString(1) + " " + rs.getString(2) + " "
						+ rs.getString(3) + " " + rs.getString(4));
				tv = new TransitVehicleMapBean();
				tv.setVehicle_ID(rs.getString(1));
				tv.setSource(rs.getString(3));
				tv.setDestination(rs.getString(4));
				if(sSource.equals(rs.getString(3)) && sDest.equals(rs.getString(4))) {
					continue;
				}
				sSource = rs.getString(3);
				sDest = rs.getString(4);
				col1.add(tv);
			}
			System.out.println("Size of Anasuya: " + col1.size());
			tAll.put(Constants.LONG_HAUL, col1);
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return tAll;
	}


	public LinkedHashMap<String, AlertWorkflowBean> getAllAlertsForVehicleId(
			String sVehicleId, String sAlertType, String sTime) {
		Connection conn = null;
		ResultSet rs = null;
		LinkedHashMap<String, AlertWorkflowBean> tmMap = new LinkedHashMap<String, AlertWorkflowBean>();
		PreparedStatement pSelect = null;
		String sKey = "";
		try {
			conn = DbConnection.getConnection();
			double fTime = Utils.getAlertTime(sTime);
				// There is no route_type in transit_gateway_map. Should that be
				// transit_vehicle_map. If yes, then there is no client_id in
				// transit_vehicle_map
				pSelect = conn
						.prepareStatement(SqlConstants.AlertsForVehicleId);
				pSelect.setString(1, sVehicleId);
			try {
				rs = pSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}

			while (rs.next()) {
				//logger.info(rs.getString(1) + " " + rs.getString(2));
				sKey = rs.getString(3);
				AlertWorkflowBean ab = new AlertWorkflowBean();
				ab.setAlert_ID(rs.getString(3));
				ab.setAlert_Location(rs.getString(4));
				ab.setAlert_Type(rs.getString(5));
				ab.setAlert_Timestamp_From(rs.getString(8));
				ab.setAlert_Timestamp_To(rs.getString(9));
				if (rs.getString(10) == null || rs.getString(10).equals("")) {
					ab.setAlert_Duration("");
				} else {
					ab.setAlert_Duration(rs.getString(10));
				}
				if (rs.getString(11) == null || rs.getString(11).equals("")) {
					ab.setAlert_Severity("");
				} else {
					ab.setAlert_Severity(rs.getString(11));
				}
				if (rs.getString(13) == null || rs.getString(13).equals("")) {
					ab.setCurrent_Location_Name("");
				} else {
					ab.setCurrent_Location_Name(rs.getString(13));
				}
				if (rs.getString(15) == null || rs.getString(15).equals("")) {
					ab.setAction_Status("Open");
				} else {
					ab.setAction_Status(rs.getString(15));
				}
				if (rs.getString(22) == null || rs.getString(22).equals("")) {
					ab.setRoot_Cause("");
				} else {
					ab.setRoot_Cause(rs.getString(22));
				}
				if (rs.getString(23) == null || rs.getString(23).equals("")) {
					ab.setPreventive_Actions("");
				} else {
					ab.setPreventive_Actions(rs.getString(23));
				}
				if (rs.getString(14) == null || rs.getString(14).equals("")) {
					ab.setRoute_Type("");
				} else {
					ab.setRoute_Type(rs.getString(14));
				}
				if (rs.getString(6) == null || rs.getString(6).equals("")) {
					ab.setBreach_Type("");
				} else {
					ab.setBreach_Type(rs.getString(6));
				}
				ab.setCurrent_Location_Lat(rs.getString(28));
				ab.setCurrent_Location_Long(rs.getString(29));
				tmMap.put(sKey, ab);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return tmMap;
	}


	public void updateAction(String sAction, String sAlertId) {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement pSelect = null;
		try {
			conn = DbConnection.getConnection();
			logger.info("action: " + sAction + " id: " + sAlertId);
			// There is no route_type in transit_gateway_map. Should that be
			// transit_vehicle_map. If yes, then there is no client_id in
			// transit_vehicle_map
			pSelect = conn.prepareStatement(SqlConstants.UpdateAction);
			pSelect.setString(1, sAction);
			pSelect.setString(2, sAlertId);
			int iReturn = pSelect.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void updatePreventiveActionRootCause(String sRootCause,
			String sPreventiveAction, String sAlertId) {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement pSelect = null;
		try {
			conn = DbConnection.getConnection();
			logger.info("root: " + sRootCause + " pre: " + sPreventiveAction
					+ " alert: " + sAlertId);
			// There is no route_type in transit_gateway_map. Should that be
			// transit_vehicle_map. If yes, then there is no client_id in
			// transit_vehicle_map
			pSelect = conn
					.prepareStatement(SqlConstants.UpdatePreventiveActionRootCause);
			pSelect.setString(1, sRootCause);
			pSelect.setString(2, sPreventiveAction);
			pSelect.setString(3, sAlertId);
			int iReturn = pSelect.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
