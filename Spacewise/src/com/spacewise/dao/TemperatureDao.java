package com.spacewise.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeMap;
import java.util.Vector;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.spacewise.bean.ClientLocationMapBean;
import com.spacewise.bean.TemperatureBean;
import com.spacewise.bean.TransitVehicleMapBean;
import com.spacewise.util.Constants;
import com.spacewise.util.DbConnection;
import com.spacewise.util.SqlConstants;

public class TemperatureDao {
	private Client client; 
	ClientLocationMapDao clmDao = new ClientLocationMapDao();
	ClientLocationMapBean clmBean = new ClientLocationMapBean();
	static final Logger logger = LoggerFactory
			.getLogger(TemperatureDao.class);
	
	private void init(){ 
	      this.client = ClientBuilder.newClient(); 
	   }  
	
	public Collection<TemperatureBean> getTemperatureData() {
		init();
		GenericType<Collection<TemperatureBean>> tvmBean1 = new GenericType<Collection<TemperatureBean>>(){};
		Collection<TemperatureBean> tvmBean = client 
		         .target(Constants.REST_SERVICE_URL + "TemperatureService/temperature") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(tvmBean1); 
		return tvmBean;
	}
	
	public Collection<TemperatureBean> getTemperatureData(String sNodeId) {
		init();
		Collection<TemperatureBean> col1 = new Vector();
		GenericType<Collection<TemperatureBean>> tvmBean1 = new GenericType<Collection<TemperatureBean>>(){};
		Collection<TemperatureBean> tvmBean = client 
		         .target(Constants.REST_SERVICE_URL + "TemperatureService/temperature") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(tvmBean1); 
		for(TemperatureBean tb: tvmBean) {
			if(tb.getNode_Id().equals(sNodeId)) {
				col1.add(tb);
			}
		}
		return col1;
	}
	
	public Collection<TemperatureBean> getVibrationData(String sNodeId) {
		init();
		Collection<TemperatureBean> col1 = new Vector();
		GenericType<Collection<TemperatureBean>> tvmBean1 = new GenericType<Collection<TemperatureBean>>(){};
		Collection<TemperatureBean> tvmBean = client 
		         .target(Constants.REST_SERVICE_URL + "TemperatureService/vibration") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(tvmBean1); 
		for(TemperatureBean tb: tvmBean) {
			if(tb.getNode_Id().equals(sNodeId)) {
				col1.add(tb);
			}
		}
		return col1;
	}
}
