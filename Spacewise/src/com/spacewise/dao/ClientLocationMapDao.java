package com.spacewise.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.TreeMap;
import java.util.Vector;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.spacewise.bean.AllLocationDataBean;
import com.spacewise.bean.ClientLocationMapBean;
import com.spacewise.bean.GatewayLogBean;
import com.spacewise.bean.LocationBean;
import com.spacewise.bean.PlanMapsBean;
import com.spacewise.bean.StationaryNodeMapBean;
import com.spacewise.bean.SubLocationBean;
import com.spacewise.bean.TransitVehicleMapBean;
import com.spacewise.bean.ZoneBean;
import com.spacewise.util.Constants;
import com.spacewise.util.DbConnection;
import com.spacewise.util.SqlConstants;
import com.spacewise.util.Utils;

public class ClientLocationMapDao {
	private Client client; 
	static final Logger logger = LoggerFactory
			.getLogger(ClientLocationMapDao.class);
	public static Collection<AllLocationDataBean> colAllLocations = new Vector<AllLocationDataBean>();
	public static TreeMap<String, ClientLocationMapBean> tmAllLocationsOrderByName = new TreeMap<String, ClientLocationMapBean>();
	public static TreeMap<String, SubLocationBean> tmAllSubLocations = new TreeMap<String, SubLocationBean>();
	public static LinkedHashMap<String, Collection> lhmAllDashboardData = new LinkedHashMap<String, Collection>();

	private void init(){ 
	      this.client = ClientBuilder.newClient(); 
	   }  
	
	
	public TreeMap<String, ClientLocationMapBean> selectLocations() {
		init();
		GenericType<Collection<ClientLocationMapBean>> clmBean1 = new GenericType<Collection<ClientLocationMapBean>>(){};
		Collection<ClientLocationMapBean> col = client 
		         .target(Constants.REST_SERVICE_URL + "ClientLocationMapService/locationmap") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(clmBean1);
		TreeMap<String, ClientLocationMapBean> tm = new TreeMap<String, ClientLocationMapBean>();
		Iterator iter = col.iterator();
		ClientLocationMapBean clmBean = new ClientLocationMapBean();
		while(iter.hasNext()){
			clmBean = (ClientLocationMapBean) iter.next();
			tm.put(clmBean.getLocation_id(), clmBean);
		}
		return tm;
	}
	
	public Collection<AllLocationDataBean> getAllLocationsData() {
		init();
		GenericType<Collection<AllLocationDataBean>> clmBean1 = new GenericType<Collection<AllLocationDataBean>>(){};
		Collection<AllLocationDataBean> col = client 
		         .target(Constants.REST_SERVICE_URL + "ClientLocationMapService/alllocations") 
		         .request(MediaType.APPLICATION_XML) 
		         .get(clmBean1);
		colAllLocations = col;
		return colAllLocations;
	}
	
	public TreeMap selectZones(String sLocId, String sSubLocId) {
		Connection conn = null;
		PreparedStatement psSelect = null;
		ResultSet rs = null;
		ZoneBean zoneBean = new ZoneBean();
		TreeMap tm = new TreeMap();
		try {
			conn = DbConnection.getConnection();

			if (!sSubLocId.equals("0")) {
				psSelect = conn
						.prepareStatement(SqlConstants.SelectZones_WithSubLoc);
				psSelect.setString(1, sLocId);
				psSelect.setString(2, sSubLocId);
			} else {
				psSelect = conn.prepareStatement(SqlConstants.SelectZones);
				psSelect.setString(1, sLocId);
			}
			try {
				rs = psSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}

			while (rs.next()) {
				zoneBean = new ZoneBean();
				zoneBean.setLocationId(rs.getString(1));
				zoneBean.setSubLocationId(rs.getString(2));
				zoneBean.setZoneId(rs.getString(3));
				zoneBean.setZoneName(rs.getString(4));
				tm.put(zoneBean.getZoneName(), zoneBean);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (psSelect != null)
					psSelect.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return tm;
	}

	public TreeMap selectNodesFromZoneID(String sZoneId, String sAlertType,
			String sTime) {
		Connection conn = null;
		PreparedStatement psSelect = null;
		ResultSet rs = null;
		StationaryNodeMapBean stationaryNodeMapBean = new StationaryNodeMapBean();
		TreeMap tm = new TreeMap();
		try {
			conn = DbConnection.getConnection();
			double fTime = Utils.getAlertTime(sTime);
			if (sAlertType.equals("ALL")) {
				psSelect = conn
						.prepareStatement(SqlConstants.SelectNodesFromZoneID_All);
				psSelect.setDouble(1, fTime);
				psSelect.setString(2, sZoneId);
			} else {
				psSelect = conn
						.prepareStatement(SqlConstants.SelectNodesFromZoneID);
				psSelect.setDouble(1, fTime);
				String[] sAlerts = sAlertType.split(",");
				sAlertType = "";
				int i = 1;
				for (int in = 0; in < sAlerts.length; in++) {
					i++;
					psSelect.setString(i, sAlerts[in]);
				}
				i++;
				while (i <= 7) {
					psSelect.setString(i, "NULL");
					i++;
				}
				psSelect.setString(8, sZoneId);
			}
			try {
				rs = psSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}
			while (rs.next()) {
				stationaryNodeMapBean = new StationaryNodeMapBean();
				stationaryNodeMapBean.setND_client_id(rs.getString(1));
				stationaryNodeMapBean.setND_subzone_name(rs.getString(2));
				stationaryNodeMapBean.setND_type(rs.getString(3));
				stationaryNodeMapBean.setND_subzone_type(rs.getString(5));
				stationaryNodeMapBean.setND_status(rs.getString(6));
				tm.put(stationaryNodeMapBean.getND_client_id(),
						stationaryNodeMapBean);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (psSelect != null)
					psSelect.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return tm;
	}

	public TreeMap selectSubLocation(String sLocId) {
		Connection conn = null;
		PreparedStatement psSelect = null;
		ResultSet rs = null;
		SubLocationBean subLocBean = new SubLocationBean();
		TreeMap tm = new TreeMap();
		try {
			conn = DbConnection.getConnection();
			psSelect = conn.prepareStatement(SqlConstants.SelectSubLocation);
			psSelect.setString(1, sLocId);
			try {
				rs = psSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}
			while (rs.next()) {

				subLocBean = new SubLocationBean();
				subLocBean.setSubLocationId(rs.getString(1));
				subLocBean.setName(rs.getString(2));
				tm.put(subLocBean.getName(), subLocBean);
			}
			psSelect = conn
					.prepareStatement(SqlConstants.SelectSubLocation_WithSubLocNull);
			psSelect.setString(1, sLocId);
			try {
				rs = psSelect.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}
			while (rs.next()) {
				subLocBean = new SubLocationBean();
				subLocBean.setSubLocationId(rs.getString(1));
				subLocBean.setName("None");
				tm.put(subLocBean.getName(), subLocBean);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (psSelect != null)
					psSelect.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return tm;
	}

	

	public void updateThresholdAndPhone(String sLocId, String sNodeId,
			String sThreshold, String sPhoneNum) {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement pSelect = null;
		String updateSql = "";
		try {
			conn = DbConnection.getConnection();
			String[] s = sThreshold.split(",");
			pSelect = conn.prepareStatement(SqlConstants.UpdateThreshold);
			pSelect.setString(1, s[0]);
			pSelect.setString(2, s[1]);
			pSelect.setString(3, sNodeId);
			int iReturn = pSelect.executeUpdate();
			if (sLocId.equals("LO-DMO-000001")) {
				updateSql = SqlConstants.UpdatePhoneForLoc1;
			} else if (sLocId.equals("LO-DMO-000002")) {
				updateSql = SqlConstants.UpdatePhoneForLoc2;
			}
			pSelect = conn.prepareStatement(updateSql);
			pSelect.setString(1, sPhoneNum);
			iReturn = pSelect.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void resetThreshold(String sNodeId) {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement pSelect = null;
		try {
			conn = DbConnection.getConnection();
			pSelect = conn.prepareStatement(SqlConstants.ResetThreshold);
			pSelect.setString(1, Constants.THRESHOLD_TEMPERTURE_MIN);
			pSelect.setString(2, Constants.THRESHOLD_TEMPERTURE_MAX);
			pSelect.setString(3, sNodeId);
			int iReturn = pSelect.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	public int insertStationaryNodeMap(StationaryNodeMapBean sn) {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement pSelect = null;
		int iReturn = 0;
		try {
			conn = DbConnection.getConnection();
			pSelect = conn
					.prepareStatement(SqlConstants.InsertStationaryNodeMap);
			pSelect.setString(1, sn.getGW_Zone_id());
			pSelect.setString(2, sn.getGW_client_id());
			pSelect.setString(3, sn.getND_client_id());
			pSelect.setString(4, sn.getND_device_id());
			pSelect.setString(5, sn.getGW_ND_Pairing());
			pSelect.setString(6, sn.getND_subzone_type());
			pSelect.setString(7, sn.getND_subzone_name());
			pSelect.setString(8, sn.getND_type());
			pSelect.setString(9, sn.getND_status());

			iReturn = pSelect.executeUpdate();
		} catch (SQLException e) {
			logger.error("localized message: " + e.getLocalizedMessage());
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return iReturn;
	}

	public int updateStationaryNodeMap(StationaryNodeMapBean sn) {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement pSelect = null;
		int iReturn = 0;
		try {
			conn = DbConnection.getConnection();
			pSelect = conn
					.prepareStatement(SqlConstants.UpdateStationaryNodeMap);
			pSelect.setString(1, sn.getGW_Zone_id());
			pSelect.setString(2, sn.getGW_client_id());
			pSelect.setString(3, sn.getND_device_id());
			pSelect.setString(4, sn.getGW_ND_Pairing());
			pSelect.setString(5, sn.getND_subzone_type());
			pSelect.setString(6, sn.getND_subzone_name());
			pSelect.setString(7, sn.getND_type());
			pSelect.setString(8, sn.getND_status());
			pSelect.setString(9, sn.getND_client_id());
			iReturn = pSelect.executeUpdate();
		} catch (SQLException e) {
			logger.error("localized message: " + e.getLocalizedMessage());
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return iReturn;
	}

	public int deleteStationaryNodeMap(String sNDClientID) {
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement pSelect = null;
		int iReturn = 0;
		try {
			conn = DbConnection.getConnection();
			pSelect = conn
					.prepareStatement(SqlConstants.DeleteStationaryNodeMap);
			pSelect.setString(1, sNDClientID);
			iReturn = pSelect.executeUpdate();
		} catch (SQLException e) {
			logger.error("localized message: " + e.getLocalizedMessage());
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return iReturn;
	}
	
}
