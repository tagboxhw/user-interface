package com.spacewise.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.Collection;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.spacewise.bean.PlanMapsBean;
import com.spacewise.bean.StationaryDoorActivityBean;
import com.spacewise.bean.StationaryHumidityBean;
import com.spacewise.util.DbConnection;
import com.spacewise.util.SqlConstants;
import com.spacewise.util.Utils;

public class StationaryDoorActivityDao {
	static final Logger logger = LoggerFactory
			.getLogger(StationaryDoorActivityDao.class);

	public Collection getChartData(String sNodeId, String sTime) {
		Collection col = new Vector();
		Connection conn = null;
		ResultSet rs = null;
		StationaryDoorActivityBean stBean = new StationaryDoorActivityBean();
		try {
			double fTime = Utils.getAlertTime(sTime);
			// logger.info("fTime: " +
			// fTime);
			conn = DbConnection.getConnection();
			PreparedStatement pSelect = conn
					.prepareStatement(SqlConstants.GetChartData_DoorActivity);
			pSelect.setString(1, sNodeId);
			pSelect.setDouble(2, fTime);
			rs = pSelect.executeQuery();
			while (rs.next()) {
				// logger.info(
				// rs.getString(1) + " " + rs.getString(2) + " "
				// +rs.getString(3) + " " + rs.getString(4));
				stBean = new StationaryDoorActivityBean();
				stBean.setGW_client_id(rs.getString(1));
				stBean.setND_client_id(sNodeId);
				stBean.setTimestamp(rs.getString(3));
				stBean.setDoorActivity(rs.getString(4));
				col.add(stBean);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getLocalizedMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				DbConnection.closeConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return col;
	}
}
